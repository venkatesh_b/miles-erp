<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

/**
 * @controller Name Branch
 * @category        Controller
 * @author          Abhilash
 */
class Batch extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Batch_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    /**
     * Function get List of all batches
     * 
     * @return - array
     * @created date - 12 Mar 2016
     * @author : Abhilash
     */
    function getAllBatches_get() {
        $id = $this->get('branch_id');
        if (is_numeric($id)) {
            $id = encode($id);
        }
        $name = $this->get('time');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id, 'name' => $name));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
            $resp = array("status" => $status, "message" => $message, "data" => $result);
            $this->set_response($resp);
        } else {
            $this->response($this->Batch_model->getBatchDataTableDetails($id, $name), REST_Controller::HTTP_OK);
        }
    }

    /**
     * Function get List of all batches related to same branch
     * 
     * @return - array
     * @created date - 12 Mar 2016
     * @author : Abhilash
     */
    function getBatchById_get() {
        $id = $this->get('batch_id');
        $branchId = $this->get('branch_id');
        if (is_numeric($branchId)) {
            $branchId = encode($branchId);
        }
        $branchId = decode($branchId);

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');
//        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Batch_model->getBatchById($id, $branchId);
            $status = true;
            $message = "";
            $result = "";

            if ($list == 'badrequest') {
                $message = 'No batch found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $message = "Batch Info";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No batch found';
                $status = false;
            }
        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($resp);
    }
    
    /**
     * To get listi of students
     */
    function getStudentListByBatch_get() {
        $id = $this->get('batch_id');
        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Batch_model->getStudentListByBatch($id);
            if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            }
            else if ($list=='nodata') {
                $message = "Student List";
                $status = false;
                $result = [];
            }else if ($list) {
                $message = "Student List";
                $status = true;
                $result = $list;
            }else {
                /* No data found */
                $message = 'No batch found';
                $status = false;
            }
        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($resp);
    }

    /**
     * Function get details of student by Batch
     * 
     * @return - array
     * @author : Abhilash
     */
    function getStudentDetailsByBatch_get() {
        $batch_id = $this->get('batch_id');
        $branchId = $this->get('branch_id');
        $courseId = $this->get('courseId');
        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $batch_id));
        $this->form_validation->set_rules('id', "ID", 'required');
//        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $details = $this->Batch_model->getStudentDetailsByBatch($batch_id, $branchId, $courseId);
            $status = true;
            $message = "";
            $result = "";

            if ($details == 'badrequest') {
                $message = 'No batch found';
                $status = false;
            } else if (isset($details['error']) && $details['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $details['msg'];
            } else if ($details) {
                $message = "Batch Info";
                $status = true;
                $result = $details;
            } else {
                /* No data found */
                $message = 'No batch found';
                $status = false;
            }
        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($resp);
    }

    /**
     * Function for adding new batch
     * 
     * @return - 
     * @created date - 12 Mar 2016
     * @author : Abhilash
     */
    function addBatch_post() {
        $status = true;
        $message = "";
        $result = "";
        $config = array(
            array(
                'field' => 'aed',
                'label' => 'acedemic end date',
                'rules' => 'required'
            ),
            array(
                'field' => 'asd',
                'label' => 'acedemic start date',
                'rules' => 'required',
            ),
            array(
                'field' => 'med',
                'label' => 'marketing end date',
                'rules' => 'required',
            ),
            array(
                'field' => 'msd',
                'label' => 'marketing start date',
                'rules' => 'required',
            ),
            array(
                'field' => 'target',
                'label' => 'target',
                'rules' => 'required|callback_num_check',
            ),
            array(
                'field' => 'visitorTarget',
                'label' => 'visitorTarget',
                'rules' => 'required|callback_num_check',
            ),
            array(
                'field' => 'course',
                'label' => 'course',
                'rules' => 'required',
            ),
            array(
                'field' => 'userId',
                'label' => 'user Id',
                'rules' => 'required',
            ),
                /* array(
                  'field' => 'sequence',
                  'label' => 'Sequence',
                  'rules' => 'required|regex_match[/^[0-9]+$/]',
                  'errors' => array(
                  'regex_match' => 'Invalid %s',
                  ),
                  ), */
                /* array(
                  'field' => 'code',
                  'label' => 'code',
                  'rules' => 'required|is_unique[`batch`.`code`]',
                  ), */
        );

        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $error = true;
            $dt = DateTime::createFromFormat('d M, Y', $this->input->post('msd'));
            $dt = $dt->format("Y-m-d");
            $msd = strtotime($dt);
            $dt = DateTime::createFromFormat('d M, Y', $this->input->post('med'));
            $dt = $dt->format("Y-m-d");
            $med = strtotime($dt);
            $dt = DateTime::createFromFormat('d M, Y', $this->input->post('asd'));
            $dt = $dt->format("Y-m-d");
            $asd = strtotime($dt);
            $dt = DateTime::createFromFormat('d M, Y', $this->input->post('aed'));
            $dt = $dt->format("Y-m-d");
            $aed = strtotime($dt);

            if ($med > $msd) {
                $error = false;
            } else {
                $result = array('med' => 'invalid', 'msd' => 'invalid',);
                $message = "Validation Error - Marketing End date should be greater than Marketing Start Date";
                $status = false;
                $resp = array("status" => $status, "message" => $message, "data" => $result);
                $this->response($resp);
            }

            if ($aed > $asd) {
                $error = false;
            } else {
                $result = array('aed' => 'invalid', 'asd' => 'invalid',);
                $message = "Validation Error - Academic End date should be greater than Academic Start Date";
                $status = false;
                $resp = array("status" => $status, "message" => $message, "data" => $result);
                $this->response($resp);
            }

            if ($msd <= $asd) {
                $error = false;
            } else {
                $result = array('msd' => 'invalid', 'asd' => 'invalid',);
                $message = "Marketting Start date should be greater than or equal to Academic Start Date";
                $status = false;
                $resp = array("status" => $status, "message" => $message, "data" => $result);
                $this->response($resp);
            }

            if ($msd <= $asd && $asd < $med) {
                $error = false;
            } else {
                $result = array('msd' => 'invalid', 'asd' => 'invalid', 'med' => 'invalid');
                $message = "Academic Start date should not be greater than or equal to Marketing Start Date";
                $status = false;
                $resp = array("status" => $status, "message" => $message, "data" => $result);
                $this->response($resp);
            }

            if (!$error) {
                $isAdded = $this->Batch_model->addBatch($this->input->post());
                if (isset($isAdded['error']) && $isAdded['error'] == 'dberror') {
                    $message = 'Database error';
                    $status = false;
                    $results = $isAdded['msg'];
                } else if (isset($isAdded['error']) && $isAdded['error'] == 'alreadypresent') {
                    $message = $isAdded['msg'];
                    $status = false;
//                    $results = array('course' => $result['msg']);
                    $results = [];
                } else if ($isAdded === 'nocourse') {
                    $result = array('course' => 'Course is invalid');
                    $message = "Course is invalid";
                    $status = false;
                } elseif ($isAdded === 'nobranch') {
                    $result = [];
                    $message = "Branch is invalid";
                    $status = false;
                } elseif ($isAdded === 'nobranchActive') {
                    $result = [];
                    $message = "Branch is inactive, Batch can't be created.";
                    $status = false;
                } elseif ($isAdded === 'nouser') {
                    $result = [];
                    $message = "User unautherised";
                    $status = false;
                } elseif ($isAdded) {
                    $result = [];
                    $message = "Batch Created successfully";
                    $status = true;
                }
            }
        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    /**
     * Function editing batch details
     * 
     * @return - array
     * @created date - 12 Mar 2016
     * @author : Abhilash
     */
    function editBatch_post() {
        $id = $this->get('batch_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');
//        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $config = array(
                array(
                    'field' => 'aed',
                    'label' => 'acedemic end date',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'asd',
                    'label' => 'acedemic start date',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'med',
                    'label' => 'marketing end date',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'msd',
                    'label' => 'marketing start date',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'target',
                    'label' => 'target',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'course',
                    'label' => 'course',
                    'rules' => 'required',
                ),
                /* array(
                  'field' => 'code',
                  'label' => 'code',
                  'rules' => 'required',
                  ), */
                array(
                    'field' => 'userId',
                    'label' => 'user Id',
                    'rules' => 'required',
                ),
                    /* array(
                      'field' => 'sequence',
                      'label' => 'Sequence',
                      'rules' => 'required|regex_match[/^[0-9]+$/]',
                      'errors' => array(
                      'regex_match' => 'Invalid %s',
                      ),
                      ), */
            );

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message = "Validation Error";
                $status = false;
                $result = $this->form_validation->error_array();
            } else {
                $error = true;
                $dt = DateTime::createFromFormat('d M, Y', $this->input->post('msd'));
                $dt = $dt->format("Y-m-d");
                $msd = strtotime($dt);
                $dt = DateTime::createFromFormat('d M, Y', $this->input->post('med'));
                $dt = $dt->format("Y-m-d");
                $med = strtotime($dt);
                $dt = DateTime::createFromFormat('d M, Y', $this->input->post('asd'));
                $dt = $dt->format("Y-m-d");
                $asd = strtotime($dt);
                $dt = DateTime::createFromFormat('d M, Y', $this->input->post('aed'));
                $dt = $dt->format("Y-m-d");
                $aed = strtotime($dt);

                if ($med > $msd) {
                    $error = false;
                } else {
                    $result = array('med' => 'invalid', 'msd' => 'invalid',);
                    $message = "Validation Error - Marketing End date should be greater than Marketing Start Date";
                    $status = false;
                    $resp = array("status" => $status, "message" => $message, "data" => $result);
                    $this->response($resp);
                }

                if ($aed > $asd) {
                    $error = false;
                } else {
                    $result = array('aed' => 'invalid', 'asd' => 'invalid',);
                    $message = "Validation Error - Academic End date should be greater than Academic Start Date";
                    $status = false;
                    $resp = array("status" => $status, "message" => $message, "data" => $result);
                    $this->response($resp);
                }

                if ($msd <= $asd) {
                    $error = false;
                } else {
                    $result = array('msd' => 'invalid', 'asd' => 'invalid',);
                    $message = "Marketting Start date should be greater than or equal to Academic Start Date";
                    $status = false;
                    $resp = array("status" => $status, "message" => $message, "data" => $result);
                    $this->response($resp);
                }

                if ($msd <= $asd && $asd < $med) {
                    $error = false;
                } else {
                    $result = array('msd' => 'invalid', 'asd' => 'invalid', 'med' => 'invalid');
                    $message = "Academic Start date should not be greater than or equal to Marketing Start Date";
                    $status = false;
                    $resp = array("status" => $status, "message" => $message, "data" => $result);
                    $this->response($resp);
                }

                if (!$error) {
                    $isEdited = $this->Batch_model->editBatch($this->input->post(), $id);
                    //            print_r($isAdded);die;
                    if ($isEdited === 'nocourse') {
                        $result = array('course' => 'Course is invalid');
                        $message = "Validation Error - Course is invalid";
                        $status = false;
                    } elseif ($isEdited === 'nobranchActive') {
                        $result = [];
                        $message = "Validation Error - Branch is inactive";
                        $status = false;
                    } elseif ($isEdited === 'nobranch') {
                        $result = [];
                        $message = "Validation Error - Branch is invalid";
                        $status = false;
                    } elseif ($isEdited === 'nouser') {
                        $result = [];
                        $message = "Validation Error - User unautherised";
                        $status = false;
                    } else if ($isEdited) {
                        $result = [];
                        $message = "Batch Updated successfully";
                        $status = true;
                    }
                }
            }
        }

        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    /**
     * Function to get batch lead counts
     * 
     * @return - array
     * @author : Abhilash
     */
    function getLeadStageCounts_get() {
        $id = $this->get('branchId');
        $batchId = $this->get('batchId');
        if (is_numeric($id)) {
            $id = encode($id);
        }
        $id = decode($id);

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $param['branchId'] = $id;
            $param['batchId'] = $batchId;
            $param['userId'] = $_SERVER['HTTP_USER'];
            $resp = $this->Batch_model->getLeadStageCounts($param);
            $final_response['response'] = [
                'status' => $status,
                'message' => $message,
                'data' => $resp,
            ];
        }

        $final_response['responsehttpcode'] = REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    /**
     * Function active/inactive batch
     * 
     * @return - bool
     * @author : Abhilash
     */
    function changeStatus_delete() {
        $code = $this->get('code');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('code' => $code));
        $this->form_validation->set_rules('code', "code", 'required');
//        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $isDeleted = $this->Batch_model->changeStatus($code);
            if ($isDeleted === 'nodata') {
                $result = [];
                $message = "Validation Error - Batch not found";
                $status = false;
            } else if ($isDeleted === 'active') {
                $message = "Branch Activated successfully";
                $status = true;
            } else if ($isDeleted === 'deactivated') {
                $message = "Branch De-Activated successfully";
                $status = true;
            } else {
                $message = 'Batch can\'t be deleted';
                $status = false;
            }
        }

        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    /**
     * Function to save branch subject details
     */
    function saveSubjectForBatch_post() {
        $status = true;
        $message = "";
        $result = "";

        $config = array(
            array(
                'field' => 'batchId',
                'label' => 'Batch Id',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'subjectDate',
                'label' => 'Date',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'subject',
                'label' => 'Subject',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'topic',
                'label' => 'Topic',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'faculty',
                'label' => 'Faculty',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'venue',
                'label' => 'Venue',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            )
        );

        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $isAdded = $this->Batch_model->saveSubjectForBatch($this->input->post());
            if (isset($isAdded['error']) && $isAdded['error'] === 'dberror') {
                $message = 'Database error';
                $status = false;
                $results = $isAdded['msg'];
            } else if ($isAdded === 'nobatch') {
                $result = [];
                $message = "No Batch found";
                $status = false;
            } else if ($isAdded === 'noid') {
                $result = [];
                $message = "No id found";
                $status = false;
            } else if ($isAdded === 'samedate') {
                $result = [];
                $message = "Class already scheduled for this date";
                $status = false;
            } else if ($isAdded === 'datanotinrandg') {
                $result = array('subjectDate' => 'Selected date should be between academic start and end date');
                $message = "Selected date should be between academic start and end date";
                $status = false;
            } else if ($isAdded === 'nosubject') {
                $result = array('subject' => 'No Subject found');
                $message = "No Subject found";
                $status = false;
            } else if ($isAdded === 'nofaculty') {
                $result = array('faculty' => 'No Faculty found');
                $message = "No Faculty found";
                $status = false;
            } else if ($isAdded === 'novenue') {
                $result = array('venue' => 'No Venue found');
                $message = "No Venue found";
                $status = false;
            } else if ($isAdded) {
                $result = [];
                $message = "Saved";
                $status = true;
            } else {
                $result = [];
                $message = "No Batch found";
                $status = false;
            }
        }

        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    /**
     * Function to save branch subject details
     */
    function editSubjectForBatch_post() {

        $status = true;
        $message = "";
        $result = "";
        $id = $this->get('batch_xref_subject_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "id", 'required');
//        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $config = array(
                array(
                    'field' => 'batchId',
                    'label' => 'Batch Id',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'subjectDate',
                    'label' => 'Date',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'subject',
                    'label' => 'Subject',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'topic',
                    'label' => 'Topic',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'faculty',
                    'label' => 'Faculty',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'venue',
                    'label' => 'Venue',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                )
            );

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message = "Validation Error";
                $status = false;
                $result = $this->form_validation->error_array();
            } else {
                $isUpdated = $this->Batch_model->editSubjectForBatch($this->input->post(), $id);
                if (isset($isUpdated['error']) && $isUpdated['error'] === 'dberror') {
                    $message = 'Database error';
                    $status = false;
                    $results = $isUpdated['msg'];
                } else if ($isUpdated === 'nobatch') {
                    $result = [];
                    $message = "No batch found";
                    $status = false;
                }  else if ($isUpdated === 'datanotinrandg') {
                    $result = array('subjectDate' => 'Selected date should be between academic start and end date');
                    $message = "Selected date should be between academic start and end date";
                    $status = false;
                } else if ($isUpdated === 'noid') {
                    $result = [];
                    $message = "No id found";
                    $status = false;
                } else if ($isUpdated === 'samedate') {
                    $result = [];
                    $message = "Class already scheduled for this date";
                    $status = false;
                } else if ($isUpdated === 'nosubject') {
                    $result = array('subject' => 'No subject found');
                    $message = "No subject found";
                    $status = false;
                } else if ($isUpdated === 'nofaculty') {
                    $result = array('faculty' => 'No faculty found');
                    $message = "No faculty found";
                    $status = false;
                } else if ($isUpdated === 'novenue') {
                    $result = array('venue' => 'No venue found');
                    $message = "No venue found";
                    $status = false;
                } else if ($isUpdated) {
                    $result = [];
                    $message = "Saved";
                    $status = true;
                } else {
                    $result = [];
                    $message = "No batch found";
                    $status = false;
                }
            }
        }

        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    /**
     * Get vbatch subject by id
     */
    function getSubjectForBatch_get() {
        $status = true;
        $message = "";
        $result = "";
        $id = $this->get('batch_xref_subject_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "id", 'required');
//        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $resp = $this->Batch_model->getSubjectForBatch($id);
            if (isset($resp['error']) && $resp['error'] === 'dberror') {
                $message = 'Database error';
                $status = false;
                $results = $resp['msg'];
            } else if ($resp === 'nodata') {
                $result = [];
                $message = "No record found";
                $status = false;
            } else if ($resp) {
                $result = $resp;
                $message = "Data";
                $status = true;
            } else {
                $result = [];
                $message = "No record found";
                $status = false;
            }
        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    /**
     * Get datatables for Batch subject
     */
    function getBatchSubjectList_get() {
        $id = $this->get('batch_id');
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "id", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $this->response($this->Batch_model->getBatchSubjectList($id), REST_Controller::HTTP_OK);
        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    function deleteBatchSubject_post() {
        $id = $this->get('batch_xref_subject_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "id", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $isDeleted = $this->Batch_model->deleteBatchSubject($id);
            if ($isDeleted === 'nodata') {
                $result = [];
                $message = "Validation Error - Topic not found";
                $status = false;
            } else if ($isDeleted === 'active') {
                $message = "Topic is active";
                $status = true;
            } else if ($isDeleted === 'deactivated') {
                $message = "Topic is inactive";
                $status = true;
            } else {
                $message = 'Topic can\'t be deleted';
                $status = false;
            }
        }

        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    public function BulkAddClass_post() {
        
        $id = $this->input->post('batch');
        
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "id", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {

            $config['upload_path'] = './uploads/';
            $config['file_name'] = 'file_class_' . date('U');
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '0';
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $message = $this->upload->display_errors('', '');
                $status = false;
                $result = [];
            } else {
                $resp = $this->upload->data();
                if(!$this->Batch_model->checkBatchPresent($id)){
                    $message = 'No batch found';
                    $status = false;
                    $result = [];
                }else{
                    $this->load->library('csvreader');
                    $result = $this->csvreader->parse_file($resp['full_path']);
                    $fullPath = $resp['full_path'];
                    if (!isset($result[1]['Date(dd-mm-yyyy)'])) {
                        $message = "No Date Column found in file";
                        $status = false;
                        $result = [];
                    } elseif (!isset($result[1]['Subject'])) {
                        $message = "No Subject Column found in file";
                        $status = false;
                        $result = [];
                    } elseif (!isset($result[1]['Topic'])) {
                        $message = "No Topic Column found in file";
                        $status = false;
                        $result = [];
                    } elseif (!isset($result[1]['Faculty'])) {
                        $message = "No Faculty Column found in file";
                        $status = false;
                        $result = [];
                    } elseif (!isset($result[1]['Venue'])) {
                        $message = "No Venue Column found in file";
                        $status = false;
                        $result = [];
                    } elseif (!isset($result[1]['PlatesUsed'])) {
                        $message = "No Plates Used Column found in file";
                        $status = false;
                        $result = [];
                    } elseif (!isset($result[1]['CupsUsed'])) {
                        $message = "No Cups Used Column found in file";
                        $status = false;
                        $result = [];
                    } else {
                        $excelCount = 0;
                        foreach ($result as $k_csv => $v_csv) {
                            if (trim($v_csv['Date(dd-mm-yyyy)']) == '' 
                                && trim($v_csv['Subject']) == '' 
                                && trim($v_csv['Topic']) == '' 
                                && trim($v_csv['Faculty']) == '' 
                                && trim($v_csv['Venue']) == '') 
                            {
                                unset($result[$k_csv]);
                            } else {
                                $excelCount++;
                            }
                        }
                        if ($excelCount > 0) {
                            $resp['Rows'] = array_values($result);
                            $resp = $this->Batch_model->BulkClass($resp);
                            if (isset($resp['error']) && $resp['error'] === 'dberror') {
                                $message = 'Database error';
                                $status = false;
                                $results = $resp['msg'];
                            } else if (isset($resp['error']) && $resp['error'] === 'error') {
                                $message = 'Error';
                                $status = false;
                                $results = $resp['msg'];
                            } else if (isset($resp['error']) && $resp['error'] === 'loaderror') {
                                $message = 'Upload Error';
                                $status = false;
                                $results = $resp['msg'];
                            } else if ($resp === 'norefrence') {
                                $result = [];
                                $message = "No Refrence type found";
                                $status = false;
                            } else if($resp['status']){
                                $file_upload_id = $resp['data']['uploadId'];
                                $result = $this->Batch_model->SaveBulkClass($file_upload_id, $id);
                                if (isset($resp['error']) && $resp['error'] === 'dberror') {
                                    $message = 'Database error';
                                    $status = false;
                                    $results = $resp['msg'];
                                } else if (isset($resp['error']) && $resp['error'] === 'error') {
                                    $message = 'Error';
                                    $status = false;
                                    $results = $resp['msg'];
                                } else if($resp){
                                    $message = 'Import data';
                                    $status = true;
                                    $results = $resp;
                                } else{
                                    $message = 'Error';
                                    $status = false;
                                    $results = [];
                                }
                            }
                            unlink($fullPath);
                            unset($fullPath);
                        } else {
                            $result = [];
                            $message = "No data found";
                            $status = false;
                        }
                    }
                }

                
            }
        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }
    
    function getBatchGraphData_get() {
        $id = $this->get('batch_id');
        
        if (is_numeric($id)) {
            $id = encode($id);
        }
        $id = decode($id);

        $status=true;$message="";$result="";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Batch_model->getBatchGraphData($id);
//            print_r($list);die;
            if (is_array($list) && isset($list['error']) && $list['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $result=$list['msg'];
            } else if ($list === 'nobatch') {
                $message="Batch Not found";
                $status=false;
                $result=[];
            } else if ($list === 'nodata') {
                $message="No data found";
                $status=false;
                $result=[];
            } else if($list) {
                $message="Batch chart details";
                $status=true;
                $result=$list;
            } else {
                $message="Something went wrong. Please try after sometime.";
                $status=false;
                $result=[];
            }
        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    /**
     * Function for checking number(REGEX)
     * 
     * @args 
     *  "string"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    function getTotalClasses_get() {
        $id = $this->get('batch_id');
        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $result = $this->Batch_model->getTotalClasses($id);

        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($resp);
    }

}
