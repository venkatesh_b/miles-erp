<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Branch
 * @category        Controller
 * @author          Abhilash
 */
class Branch extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Branch_model");
        $this->load->model("References_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('Put_method_extenstion');   /* method PUT */
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    /**
     * Function for geting Dashboard related function
     *
     * @arg
     * No args
     *
     * @return - array
     * @date modified - 6 Feb 2016
     * @author : Abhilash
     */
    function getBranchDashboardWidget_get() {

        $result = $this->Branch_model->getDashboardWidgetsData($_SERVER['HTTP_USER']);
        $status=true;$message="";$results="";

        if (isset($result['error']) && $result['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $results = $result['msg'];
        }
        else if ($result)
        {
            $message="Branch Widget details";
            $status=true;
            $results=$result;
        }
        else
        {
            $message='No branch found';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }
    
    /**
     * Function get Fee due widget for branch
     * 
     * @return - array
     * @author : Abhilash
     */
    function feeDueWidget_get() {

        $result = $this->Branch_model->feeDueWidget($_SERVER['HTTP_USER']);
        $status=true;$message="";$results="";

        if (isset($result['error']) && $result['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $results = $result['msg'];
        }
        else if ($result)
        {
            $message="Branch Widget details";
            $status=true;
            $results=$result;
        }
        else
        {
            $message='No branch found';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }
    
    /**
     * Function get Student widget for branch
     * 
     * @return - array
     * @author : Abhilash
     */
    function studentTypeWidget_get() {

        $result = $this->Branch_model->studentTypeWidget($_SERVER['HTTP_USER']);
        $status=true;$message="";$results="";

        if (isset($result['error']) && $result['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $results = $result['msg'];
        }
        else if ($result)
        {
            $message="Branch Widget details";
            $status=true;
            $results=$result;
        }
        else
        {
            $message='No branch found';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    /**
     * Function get users for specific branch
     * 
     * @return - array
     * @author : Abhilash
     */
    function getBranchUsers_get(){
        $results = '';
        $id = decode($this->get('branch_id'));
        $search = $this->get('search');
        $userList = $this->Branch_model->getBranchUsers($id, $search);
        $status=true;$message="";$result="";
        if(is_array($userList) && isset($userList['error']) && $userList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $userList['msg'];
        }
        else if(!$userList || count($userList)<=0)
        {
            $message='No user Found';
            $status=false;
            $results = [];
        }
        else if($userList)
        {
            $result = $userList;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function getBranchUsersDashboard_get(){
        $results = '';
        $id = $this->get('branch_id');
        $search = $this->get('search');
        $userList = $this->Branch_model->getBranchUsers($id, $search);
        $status=true;$message="";$result="";
        if(is_array($userList) && isset($userList['error']) && $userList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $userList['msg'];
        }
        else if(!$userList || count($userList)<=0)
        {
            $message='No user Found';
            $status=false;
            $results = [];
        }
        else if($userList)
        {
            $result = $userList;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }

    /**
     * Function get branch datatables
     * 
     * @return - array
     * @author : Abhilash
     */
    function getBranchesList_get()
    {
        $this->response($this->Branch_model->getBranchDataTableDetails($_SERVER['HTTP_USER']),REST_Controller::HTTP_OK);
    }

    /**
     * Function get all branch head
     * 
     * @return - array
     * @author : Abhilash
     */
    function getBranchHead_get()
    {
        $branch = $this->Branch_model->getBranchHead();
        $status=true;$message="";$results="";
        if(is_array($branch) && isset($branch['error']) && $branch['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $results = $branch['msg'];
        }
        else if(count($branch)>0)
        {
            $result = $branch;
        }
        else if(count($branch)<=0)
        {
            $message='No Branch Found';
            $status=false;
            $results = [];
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }

    /**
     * Function get primary city of the branch
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getPrimaryCity_get() {
        $results = '';
        $id = $this->get('country_id');
        $cityList = $this->References_model->getReferenceValuesList(3);
        $country = [];
        foreach ($cityList['data'] as $key => $value) {
            if($value->Id3 == $id)
            {
                $country[] = $value;
            }
        }

        if(is_array($cityList) && isset($cityList['error']) && $cityList['$response'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $results = $response['msg'];
        }
        else if($cityList == 'nodata')
        {
            $message='City list';
            $status=false;
        }
        else if(count($cityList)>0)
        {
            $message = 'City list';
            $status=true;
//            $results['city'] = array_reverse($city);
            $results = array_reverse($country);
        }
        else
        {
            $message='City list';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    /**
     * Function for only list of branches
     *
     * @arg
     * No args
     *
     * @return - array
     * @date modified - 10 Feb 2016
     * @author : Sam
     */
    
    function getBranchList_get(){
        $branchList = $this->Branch_model->getBranchList();
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no branches';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList;
        }
        else
        {
            /*No data found*/
            $message='There are no branches';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    
    /**
     * Function for only list of branches acccording to logged in User
     *
     * @arg
     * No args
     *
     * @return - array
     * @date modified - 10 Feb 2016
     * @author : Sam
     */
    function getBranchListByUserId_get(){
        $branchList = $this->Branch_model->getBranchListByUserId($_SERVER['HTTP_USER']);
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no branches';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList;
        }
        else
        {
            /*No data found*/
            $message='There are no branches';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }

    /**
     * Function for geting branch list and specific configuration settings)
     *
     * @arg
     * id - (optional) id of branch
     *
     * @return - array
     * @date modified - 28 Jan 2016
     * @author : Sam
     * **** No Used ****
     */
    function getAllBranches_get() {

        $branchList = $this->Branch_model->getAllBranch();

        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no branches';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="All Branches list";
            $status=true;
            $result=array_values($branchList);
        }
        else
        {
            /*No data found*/
            $message='There are no branches';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }

    /**
     * Function for geting branch list and specific configuration settings)
     *
     * @arg
     * id - (optional) id of branch
     *
     * @return - array
     * @date modified - 28 Jan 2016
     * @author : Sam
     */
    function getBranchById_get() {
        $id = $this->get('branch_id');
        if(is_numeric($id)){ $id = encode($id); }
        $id = decode($id);
        
        $status=true;$message="";$result="";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        }
        else
        {
            $branchList = $this->Branch_model->getBranch($id);
            if ($branchList == 'badrequest')
            {
                $message="No Branches Found";
                $status=false;
                $result=array();
            }
            else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
            {
                $message='Database error';
                $status=false;
                $result = $branchList['msg'];
            }
            else if ($branchList)
            {
                $message="Branch Detail";
                $status=true;
                $result=$branchList;
            }
            else
            {
                $message="No Branches found";
                $status=false;
                $result=array();
            }
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    
    /**
     * Function for creating a new branch
     *
     * @return - boolean
     * @date modified - 28 Jan 2016
     * @author : Sam
     */
    function createBranch_post() {
        $max_fileUpload_size = 2048;    //For Fmax filr upload (in KB)
        $status=true;$message="";$result="";

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|callback_name_check|is_unique[`branch`.`name`]'
            ),
            array(
                'field' => 'zip',
                'label' => 'Zip code',
                'rules' => 'required|regex_match[/^[0-9]{6}$/]',
                'errors' => array(
                    'regex_match' => 'Invalid %s',
                ),
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email',
                'errors' => array(
                    'valid_email' => 'Invalid %s',
                ),
            ),
            array(
                'field' => 'lsequence',
                'label' => 'Lead Sequence',
                'rules' => 'required|regex_match[/^[0-9]+$/]',
                'errors' => array(
                    'regex_match' => 'Invalid %s',
                ),
            ),
            array(
                'field' => 'bsequence',
                'label' => 'Branch Sequence',
                'rules' => 'required|regex_match[/^[0-9]+$/]',
                'errors' => array(
                    'regex_match' => 'Invalid %s',
                ),
            ),
            array(
                'field' => 'code',
                'label' => 'Branch Code',
                'rules' => 'required|regex_match[/^[a-zA-Z0-9]+$/]|is_unique[`branch`.`code`]',
                'errors' => array(
                    'required' => '%s Required',
                    'regex_match' => 'Invalid %s',
                    'is_unique' => '%s already exist',
                ),
            ),
            array(
                'field' => 'city',
                'label' => 'City',
                'rules' => 'required'
            ),
            array(
                'field' => 'country',
                'label' => 'Country',
                'rules' => 'required'
            ),
            array(
                'field' => 'state',
                'label' => 'State',
                'rules' => 'required'
            ),
            array(
                'field' => 'course',
                'label' => 'Course',
                'rules' => 'required'
            ),
            array(
                'field' => 'primaryCity',
                'label' => 'Primary City',
                'rules' => 'required'
            ),
            array(
                'field' => 'secondaryCity',
                'label' => 'Secondary City',
                'rules' => ''
            ),
            array(
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'required|callback_checkPhone'
            ),
            array(
                'field' => 'userId',
                'label' => 'User ID',
                'rules' => 'required'
            ),
            array(
                'field' => 'branchHead',
                'label' => 'Branch Head option',
                'rules' => ''
            ),
            array(
                'field' => 'desc',
                'label' => 'Description',
                'rules' => 'regex_match[/[a-zA-Z0-9. ]+$/]',
                'errors' => array(
                    'regex_match' => 'Invalid %s, may contain alphabets, numbers , dots '.', spaces',
                ),
            )
        );

        /**/
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }

        /**/
        
        if((count($_FILES) > 0) && (($_FILES['file']['size']/1024)/1024) > $max_fileUpload_size){
            $message='Upload Error';
            $status=false;
            $result = array('imageUpload' => 'File can\'t be more than 2Mb');
            $data=array("status"=>$status,"message"=>$message,"data"=>$result);
            $this->response($data);
        }
        if(count($_FILES) > 0){
            preg_match_all('/\.[0-9a-z]+$/', $_FILES['file']['name'],$res); /*For uploaded Image extension*/
        }
        $data = [];
        foreach ($this->input->post() as $key => $value) {
            if($key == 'branchHead')
            {
//                $data[$key] = decode($value);
                $data[$key] = $value;
            }
            else if($key == 'primaryCity' || $key == 'secondaryCity'  || $key == 'course')
            {
                $tmp = explode(',', $value);
                $temp = '';
                foreach ($tmp as $k => $v) {
//                    $temp[] = decode($v);
                    $temp[] = $v;
                }
                $data[$key] = '';
                $data[$key] = implode(',',$temp);
            }
            else if($key == 'image'){
                $data[$key] = $value.$res[0][0];
            }
            else
            {
                $data[$key] = $value;
            }
        }
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($data);

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } else {
            if($this->checkPrimaryCity($data['primaryCity'], $data['secondaryCity']))
            {
                $message='Primary and Secondary City duplicacy not allowed';
                $status=false;
                $result = [
                    'primaryCity'=>'Primary and Secondary City duplicacy not allowed',
                    'secondaryCity'=>'Primary and Secondary City duplicacy not allowed',
                ];
            }
            else
            {
                if(count($_FILES) > 0){
                    $this->load->helper(array('form', 'url'));

                    /*$config['upload_path'] = './../application/uploads';
                    $config['file_name'] = $data['image'];
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size']	= '2000';
                    $config['max_width']  = '1024';
                    $config['max_height']  = '768';*/
                    
                    $config['upload_path'] = FILE_UPLOAD_LOCATION.'branches';
                    $config['allowed_types'] = IMAGE_EXTENSION;
                    $config['max_size']	= DOCUMENT_MAX_SIZE;
                    $config['file_name'] = $data['image'];
                    $config['max_width']  = '1024';
                    $config['max_height']  = '768';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('file'))
                    {
                        $message='Upload Error';
                        $status=false;
                        $result = $this->upload->display_errors();
                        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
                        $this->set_response($data);
                    }

                }
                $isAdded = $this->Branch_model->addBranch($data);
                if ($isAdded === 'nocity')
                {
                    $result = array('city'=>'City is invalid');
                    $message="Validation Error - City is invalid";
                    $status=false;
                }
                else if ($isAdded === 'usedCity')
                {
                    $result = array('city'=>'City is already used');
                    $message="Validation Error - City is already assigned to branch";
                    $status=false;
                }
                else if ($isAdded === 'usedPrimary')
                {
                    $result = array('primaryCity'=>'City is already used');
                    $message="Validation Error - City is already assigned as Primary";
                    $status=false;
                }
                else if ($isAdded === 'usedPrimarySecondary')
                {
                    $result = array('secondaryCity'=>'City is already assigned');
                    $message="Validation Error - City is already assigned as Primary to another branch";
                    $status=false;
                }
                else if ($isAdded === 'noprimaryCity')
                {
                    $result = array('primaryCity'=>'Primary City is invalid');
                    $message="Validation Error - Primary City is invalid";
                    $status=false;
                }
                else if ($isAdded === 'usedCity')
                {
                    $result = array('city'=>'City is already used');
                    $message="Validation Error - City is already used";
                    $status=false;
                }
                else if ($isAdded === 'nosecondaryCity')
                {
                    $result = array('secondaryCity'=>'Secondary City is invalid');
                    $message="Validation Error - Secondary City is invalid";
                    $status=false;
                }
                else if ($isAdded === 'nostate')
                {
                    $result = array('state'=>'State is invalid');
                    $message="Validation Error - State is invalid";
                    $status=false;
                }
                else if ($isAdded === 'nocountry')
                {
                    $result = array('country'=>'Country is invalid');
                    $message="Validation Error - Country is invalid";
                    $status=false;
                }
                else if ($isAdded === 'nocourse')
                {
                    $result = array('course'=>'Course is invalid');
                    $message="Validation Error - Course is invalid";
                    $status=false;
                }
                else if ($isAdded === 'nouser')
                {
                    $result = array('branchHead'=>'Branch Head is wrong');
                    $message="Validation Error - Branch Head is wrong";
                    $status=false;
                }

                else if ($isAdded['error'] === 'dberror')
                {
                    $message='Database error';
                    $status=false;
                    $result = $branchList['msg'];
                }
                else if ($isAdded)
                {
                    $message="Branch Created successfully";
                    $status=true;
                }
                else
                {
                    $message="No branch created";
                    $status=false;
                }
            }
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->response($data);
    }

    /**
     * Function for modifying branch
     *
     * @return - boolean
     * @date modified - 28 Jan 2016
     * @author : Sam
     */
    function updateBranch_post() {

        $status=true;$message="";$result="";

        $id = decode($this->get('branch_id'));

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } else {
//            $this->put = file_get_contents('php://input');
//            $this->put_method_extenstion->_parse_request($this->put);

            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required|callback_name_check'
                ),
                array(
                    'field' => 'zip',
                    'label' => 'Zip code',
                    'rules' => 'required|regex_match[/^[0-9]{6}$/]',
                    'errors' => array(
                        'regex_match' => 'Invalid %s',
                    ),
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required|valid_email',
                    'errors' => array(
                        'valid_email' => 'Invalid %s',
                    ),
                ),
                array(
                    'field' => 'code',
                    'label' => 'code',
                    'rules' => 'required|regex_match[/^[a-zA-Z0-9]+$/]'
                ),
                array(
                    'field' => 'city',
                    'label' => 'City',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'country',
                    'label' => 'Country',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'state',
                    'label' => 'State',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'course',
                    'label' => 'Course',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'primaryCity',
                    'label' => 'Primary City',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'secondaryCity',
                    'label' => 'Secondary City',
                    'rules' => ''
                ),
                array(
                    'field' => 'phone',
                    'label' => 'Phone',
                    'rules' => 'required|callback_checkPhone'
                ),
                array(
                    'field' => 'userId',
                    'label' => 'User ID',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'branchHead',
                    'label' => 'Branch Head option',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'desc',
                    'label' => 'Description',
                    'rules' => 'regex_match[/^[a-zA-Z0-9. ]+$/]',
                    'errors' => array(
                        'regex_match' => 'Invalid %s, may contain alphabets, numbers , dots '.', spaces',
                    ),
                ),
                array(
                    'field' => 'address',
                    'label' => 'Address',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'lsequence',
                    'label' => 'Lead Sequence',
                    'rules' => 'required|regex_match[/^[0-9]+$/]',
                    'errors' => array(
                        'regex_match' => 'Invalid %s',
                    ),
                ),
                array(
                    'field' => 'bsequence',
                    'label' => 'Branch Sequence',
                    'rules' => 'required|regex_match[/^[0-9]+$/]',
                    'errors' => array(
                        'regex_match' => 'Invalid %s',
                    ),
                ),
            );

            if(count($_FILES) > 0){
                preg_match_all('/\.[0-9a-z]+$/', $_FILES['file']['name'],$res); /*For uploaded Image extension*/
            }
            $data = [];
            foreach ($this->input->post() as $key => $value) {
                if($key == 'branchHead')
                {
                    //                $data[$key] = decode($value);
                    $data[$key] = $value;
                }
                else if($key == 'primaryCity' || $key == 'secondaryCity'  || $key == 'course')
                {
                    $tmp = explode(',', $value);
                    $temp = '';
                    foreach ($tmp as $k => $v) {
                        //                    $temp[] = decode($v);
                        $temp[] = $v;
                    }
                    $data[$key] = '';
                    $data[$key] = implode(',',$temp);
                }
                else if($key == 'image' && count($_FILES) > 0){
                    $data[$key] = $value.$res[0][0];
                }
                else
                {
                    $data[$key] = $value;
                }
            }

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($data);

            if ($this->form_validation->run() == FALSE) {
                $message="Validation Error";
                $status=false;
                $result=$this->form_validation->error_array();
            } else {
                if($this->checkPrimaryCity($data['primaryCity'], $data['secondaryCity']))
                {
                    $message='Primary and Secondary City duplicacy not allowed';
                    $status=false;
                    $result = [
                        'primaryCity'=>'Primary and Secondary City duplicacy not allowed',
                        'secondaryCity'=>'Primary and Secondary City duplicacy not allowed',
                    ];
                }
                else
                {
                    if(count($_FILES) > 0){
                        $this->load->helper(array('form', 'url'));

                        $config['upload_path'] = FILE_UPLOAD_LOCATION.'branches';
                        $config['allowed_types'] = IMAGE_EXTENSION;
                        $config['max_size']	= DOCUMENT_MAX_SIZE;
                        $config['file_name'] = $data['image'];
                        $config['max_width']  = '1024';
                        $config['max_height']  = '768';

                        $this->load->library('upload', $config);

                        if ( ! $this->upload->do_upload('file'))
                        {
                            $message='Upload Error';
                            $status=false;
                            $result = $this->upload->display_errors();
                            $data=array("status"=>$status,"message"=>$message,"data"=>$result);
                            $this->set_response($data);
                        }

                    }

                    $isEdited = $this->Branch_model->editBranch($id, $data);

                    if ($isEdited === 'nocity')
                    {
                        $result = array('city'=>'City is invalid');
                        $message="Validation Error - City is invalid";
                        $status=false;
                    }
                    else if ($isEdited === 'usedCity')
                    {
                        $result = array('city'=>'City is already used');
                        $message="Validation Error - City is already assigned to branch";
                        $status=false;
                    }
                    else if ($isEdited === 'usedsequence')
                    {
                        $result = array('lsequence'=>'Sequence is already started');
                        $message="Sequence is already started";
                        $status=false;
                    }
                    else if ($isEdited === 'usedbatchsequence')
                    {
                        $result = array('bsequence'=>'Batch Sequence is already started');
                        $message="Batch Sequence is already started";
                        $status=false;
                    }
                    else if ($isEdited === 'usedPrimary')
                    {
                        $result = array('primaryCity'=>'City is already user');
                        $message="Validation Error - City is already assigned as Primary";
                        $status=false;
                    }
                    else if ($isEdited === 'usedPrimarySecondary')
                    {
                        $result = array('secondaryCity'=>'City is already assigned');
                        $message="Validation Error - City is already assigned as Primary to another branch";
                        $status=false;
                    }
                    else if ($isEdited === 'noprimaryCity')
                    {
                        $result = array('primaryCity'=>'Primary City is invalid');
                        $message="Validation Error - Primary City is invalid";
                        $status=false;
                    }
                    else if ($isEdited === 'branchcode')
                    {
                        $result = array('code'=>'Branch Code is invalid');
                        $message="Validation Error - Branch Code already exist";
                        $status=false;
                    }
                    else if ($isEdited === 'nosecondaryCity')
                    {
                        $result = array('secondaryCity'=>'Secondary City is invalid');
                        $message="Validation Error - Secondary City is invalid";
                        $status=false;
                    }
                    else if ($isEdited === 'nostate')
                    {
                        $result = array('state'=>'State is invalid');
                        $message="Validation Error - State is invalid";
                        $status=false;
                    }
                    else if ($isEdited === 'nocountry')
                    {
                        $result = array('country'=>'Country is invalid');
                        $message="Validation Error - Country is invalid";
                        $status=false;
                    }
                    else if ($isEdited === 'nocourse')
                    {
                        $result = array('course'=>'Course is invalid');
                        $message="Validation Error - Course is invalid";
                        $status=false;
                    }
                    else if ($isEdited === 'usedsequence')
                    {
                        $result = array('sequence'=>'Sequence is already used');
                        $message="Sequence is already used";
                        $status=false;
                    }
                    else if ($isEdited === 'nouser')
                    {
                        $message="Branch Head is incorrect";
                        $status=false;
                        $result=[];
                    }
                    else if (isset($isEdited['error']) && $isEdited['error'] === 'dberror')
                    {
                        $message="Database Error";
                        $status=false;
                        $result=$result['msg'];
                    }
                    else if ($isEdited === "badrequest")
                    {
                        $message="No Id found";
                        $status=false;
                    }
                    else if ($isEdited)
                    {
                        $message="Date modified successfully";
                        $status=true;
                    }
                    /*else
                    {
                        $message="Data can\'t be Updated";
                        $status=false;
                    }*/
                }
            }
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }

    /**
     * Function for activating/dectivating branch
     *
     * @return - boolean
     * @date modified - 12 Mar 2016
     * @author : Sam
     */
    public function changeBranchStatus_delete()
    {
        $status=true;$message="";$results='';

        $id = decode($this->get('branch_id'));

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "id", 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        }
        else
        {
            $result = $this->Branch_model->changeStatus($id);
            if (is_array($result) && $result['error'] === 'dberror')
            {
                $message="Database Error";
                $status=false;
                $results=$result['msg'];
            }
            else if ($result === 'active')
            {
                $message="Branch Activated successfully";
                $status=true;
            }
            else if ($result === 'deactivated')
            {
                $message="Branch De-Activated successfully";
                $status=true;
            }
            else if ($result === 'nodata')
            {
                $message= 'Branch not found';
                $status=false;
            }
            else
            {
                $message= 'Branch can\'t be deleted';
                $status=false;
            }
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    /**
     * Function for activating/dectivating branch courses
     *
     * @return - boolean
     * @date modified - 12 Mar 2016
     * @author : Sam
     */
    public function changeBranchCourseStatus_delete()
    {
        $status=true;$message="";$results='';

        $branch_id = decode($this->get('branch_id'));
        $course_id = decode($this->get('course_id'));


        $this->form_validation->set_data(array('branch_id' => $branch_id, 'course_id' => $course_id ));
        $this->form_validation->set_rules('branch_id', "Branch", 'required');
        $this->form_validation->set_rules('course_id', "Course", 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        }
        else
        {
            $result = $this->Branch_model->changeCourseStatus($branch_id , $course_id);
            if (is_array($result) && $result['error'] === 'dberror')
            {
                $message="Database Error";
                $status=false;
                $results=$result['msg'];
            }
            else if ($result === 'active')
            {
                $message="Branch Course Activated successfully";
                $status=true;
            }
            else if ($result === 'deactivated')
            {
                $message="Branch Course De-Activated successfully";
                $status=true;
            }
            else if ($result === 'nobranch')
            {
                $message= 'Branch is invalid';
                $status=false;
            }
            else if ($result === 'courseInactive')
            {
                $message= 'Course is Inactive, Please activate it';
                $status=false;
            }
            else if ($result === 'nocourse')
            {
                $message= 'Course is invalid';
                $status=false;
            }
            else if ($result === 'nobranchcourse')
            {
                $message= 'Course not found';
                $status=false;
            }
            else
            {
                $message= 'Course can\'t be deleted';
                $status=false;
            }
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    public function extendUser_post(){
        $status=true;$message="";$results='';
        $id = decode($this->get('branch_id'));
//        print_r($this->input->post());die;
        $config = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data(array('id' => $id));

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required|callback_name_check'
                ),
                array(
                    'field' => 'days',
                    'label' => 'days',
                    'rules' => 'required|callback_num_check'
                ),
                array(
                    'field' => 'userid',
                    'label' => 'userid',
                    'rules' => 'required'
                )
            );
            $data = json_decode(file_get_contents("php://input"), true);
            if($data){ $_POST = $data; }

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message="Validation Error";
                $status=false;
                $results=$this->form_validation->error_array();
            } else {
                //
                $added = $this->Branch_model->extendUser($this->input->post(), decode($this->get('branch_id')));
//                print_r($added);die;
                if (is_array($added) && $added['error'] === 'dberror') {
                    $message="Database Error";
                    $status=false;
                    $results=$added['msg'];
                } else if ($added === 'nobranch') {
                    $message="Branch Not found";
                    $status=false;
                    $results=[];
                } else if ($added === 'nouser') {
                    $message="User Not found";
                    $status=false;
                    $results=[];
                } else if($added) {
                    $message="Saved successfully";
                    $status=true;
                    $results=[];
                } else {
                    $message="Something went wrong. Please try after sometime.";
                    $status=false;
                    $results=[];
                }
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }

    public function deactivateUser_post(){
        $status=true;$message="";$results='';
        $id = decode($this->get('branch_id'));
        $config = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data(array('id' => $id));

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $isSuccess = $this->Branch_model->deactivateUser(decode($this->get('branch_id')), $this->input->post('userid'));
            if (is_array($isSuccess) && $isSuccess['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $results=$isSuccess['msg'];
            } else if ($isSuccess === 'nobranch') {
                $message="Branch Not found";
                $status=false;
                $results=[];
            } else if ($isSuccess === 'nouser') {
                $message="User Not found";
                $status=false;
                $results=[];
            } else if($isSuccess) {
                $message="Branch User Deactivated";
                $status=true;
                $results=[];
            } else {
                $message="Something went wrong. Please try after sometime.";
                $status=false;
                $results=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }
    function getBranchDashboardData_get(){
        $branchId=$this->get('branchId');
        $params['branchId']=$branchId;
        $response=$this->Branch_model->getBranchDashboardData($params);
        $data=array("status"=>$response['status'],"message"=>$response['message'],"data"=>$response['data']);
        $this->set_response($data);
    }
    public function extendUserBranchDashboard_post(){
        $status=true;$message="";$results='';
        $id = $this->get('branch_id');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data(array('id' => $id));

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required|callback_name_check'
                ),
                array(
                    'field' => 'days',
                    'label' => 'days',
                    'rules' => 'required|callback_num_check'
                ),
                array(
                    'field' => 'userid',
                    'label' => 'userid',
                    'rules' => 'required'
                )
            );
            $data = json_decode(file_get_contents("php://input"), true);
            if($data){ $_POST = $data; }

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message="Validation Error";
                $status=false;
                $results=$this->form_validation->error_array();
            } else {
                //
                $added = $this->Branch_model->extendUser($this->input->post(), $this->get('branch_id'));
//                print_r($added);die;
                if (is_array($added) && $added['error'] === 'dberror') {
                    $message="Database Error";
                    $status=false;
                    $results=$added['msg'];
                } else if ($added === 'nobranch') {
                    $message="Branch Not found";
                    $status=false;
                    $results=[];
                } else if ($added === 'nouser') {
                    $message="User Not found";
                    $status=false;
                    $results=[];
                } else if($added) {
                    $message="Saved successfully";
                    $status=true;
                    $results=[];
                } else {
                    $message="Something went wrong. Please try after sometime.";
                    $status=false;
                    $results=[];
                }
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }

    public function deactivateUserBranchDashboard_post(){
        $status=true;$message="";$results='';
        $id = $this->get('branch_id');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data(array('id' => $id));

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $isSuccess = $this->Branch_model->deactivateUser($this->get('branch_id'), $this->input->post('userid'));
            if (is_array($isSuccess) && $isSuccess['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $results=$isSuccess['msg'];
            } else if ($isSuccess === 'nobranch') {
                $message="Branch Not found";
                $status=false;
                $results=[];
            } else if ($isSuccess === 'nouser') {
                $message="User Not found";
                $status=false;
                $results=[];
            } else if($isSuccess) {
                $message="Branch User Deactivated";
                $status=true;
                $results=[];
            } else {
                $message="Something went wrong. Please try after sometime.";
                $status=false;
                $results=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }
    
    public function getBranchChartData_get() {
        $id = $this->get('branch_id');
        if (is_numeric($id)) {
            $id = encode($id);
        }
        $id = decode($id);

        $status=true;$message="";$result="";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Branch_model->getBranchChartData($id);
            if (is_array($list) && isset($list['error']) && $list['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $result=$list['msg'];
            } else if ($list === 'nobranch') {
                $message="Branch Not found";
                $status=false;
                $result=[];
            } else if($list) {
                $message="Branch chart details";
                $status=true;
                $result=$list;
            } else {
                $message="Something went wrong. Please try after sometime.";
                $status=false;
                $result=[];
            }
        }
        $resp = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($resp);
    }

    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Function for checking valid string
     *
     * @args
     *  "str"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function name_check($str) {
//        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str)) {
        if (strlen($str) <= 0 || !preg_match("/^[a-zA-Z][a-zA-Z0-9 ]*$/", $str)) {
            $this->form_validation->set_message('name_check', 'The %s field is not valid!, use only Alphabets');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Validates phone is valid.
     *
     * @access  public
     * @param   string
     * @return  Boolean
     */
    function checkPhone($phone)
    {
        /* Valid
         *  (+12) 99988 88123
         *  (+12) 999 8812
         *  (91)1234567890
         *  (91) 1234567890
         *  (91) 12345 67890
         *  (91) 0123-67812
         *  +91 01231 67812
         *  +91 01231-67812
         *  +91 0123167812
         */
//        if (preg_match("/^\(?[+]?([0-9]{2,3})\)?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/", $phone))
        if (preg_match("/^[0-9() +-]{8,15}$/", $phone))
        {
            return true;
        }
        else
        {
//            $this->form_validation->set_message('checkPhone', '(+XX) XXXXX XXXXX OR (+XX) XXX XXXX The Phone Number you entered is not correctly formatted.');
            $this->form_validation->set_message('checkPhone', 'Invalid phone number');
            return FALSE;
        }
    }

    /**
     *
     * @param type $first
     * @param type $second
     * @return boolean
     */
    function checkPrimaryCity($first, $second)
    {
        $statusFirst = $this->checkArray($first);
        $statusSecond = $this->checkArray($second);
        if ($statusFirst)
        {
            $firstArray = explode(',', $first);
        }

        if($statusSecond)
        {
            $secondArray = explode(',', $second);
        }

        if ($statusFirst)
        {
            if($statusSecond)
            {
                return !empty(array_intersect($firstArray, $secondArray));
                /*true -> matched; false -> no-match*/
            }
            else
            {
                foreach ($firstArray as $k1 => $v1) {
                    if($v1 == $second)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        else
        {
            if($statusSecond)
            {
                foreach ($secondArray as $k1 => $v1) {
                    if($v1 == $first)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                if($first == $second)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
    function getManageUsers_get(){
        $branchId=$this->get('branchId');
        $searchVal=$this->get('searchVal');
        $params['branchId']=$branchId;
        $params['searchVal']=$searchVal;
        $res=$this->Branch_model->getManageUsers($params);
        $this->set_response($res);
    }

    function getBranchLeadList_get(){
        $branchId = $this->get('branchId');
        $this->load->model("LeadsInfo_model");
        $data['M3']=$this->LeadsInfo_model->getNewLeadsCount("M3",$branchId);
        if(isset($data['M3']['data'][0]->NewLeads))
            $data['M3'] = $data['M3']['data'][0]->NewLeads;
        $data['M7']=$this->LeadsInfo_model->getNewLeadsCount("M7",$branchId);
        if(isset($data['M7']['data'][0]->NewLeads))
            $data['M7'] = $data['M7']['data'][0]->NewLeads;
        $resp = array("status" => true, "message" => 'Lead stage counts', "data" => $data);
        $this->response($resp);
    }

    function checkArray($val)
    {
        if (preg_match("/,/", $val))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}