<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-17
 * Time: 07:01 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class BranchGrn extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("BranchGrn_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE)
        {
            if (count($this->form_validation->error_array()) > 0)
            {
                return $this->form_validation->error_array();
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    function branchGrnList_get()
    {
        $branchId=$this->get('branchId');
        $this->response($this->BranchGrn_model->branchGrnDataTables($branchId),REST_Controller::HTTP_OK);
    }
    function getBranches_get()
    {
        $searchVal=$this->get('searchVal');
        $branchList = $this->BranchGrn_model->getBranchList($searchVal);
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no branches';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList;
        }
        else
        {
            /*No data found*/
            $message='There are no branches';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function getVendors_get()
    {
        $searchVal=$this->get('searchVal');
        $branchList = $this->BranchGrn_model->getVendorList($searchVal);
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no vendors';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList;
        }
        else
        {
            /*No data found*/
            $message='There are no vendors';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function getProducts_get()
    {
        $searchVal=$this->get('searchVal');
        $branchList = $this->BranchGrn_model->getProductList($searchVal);
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no products';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList['data'];
        }
        else
        {
            /*No data found*/
            $message='There are no products';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function addBranchGrn_post()
    {
        $inputData = $this->post();
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"grnNo","label"=>'grnNo',"rules"=>"required","errors"=>array('required' => 'GTN No should not be blank')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true) {
            $param['invoiceNumber']=$inputData['grnInvoice'];
            $invoiceRes = $this->BranchGrn_model->checkInvoiceNoExist($param);
            if($invoiceRes['status']===true){
                $config['upload_path'] = './uploads/inventory_invoice/';
                $fileName = $inputData['image'];
                $config['file_name']= $fileName;
                $config['allowed_types'] = '*';
                $config['max_size']	= '0';
                $config['remove_spaces']= TRUE;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file'))
                {
                    $final_response['response']=[
                        'status' => FALSE,
                        'message' => $this->upload->display_errors('',''),
                        'data' => array(),
                    ];
                }else {
                    $uploadedFileName = 'uploads/inventory_invoice/' . $this->upload->data('file_name');
                    $data = array(
                        'userID' => $inputData['userID'],
                        'receiverMode' => $inputData['receiverMode'],
                        'receiverObjectId' => $inputData['receiverObjectId'],
                        'requestMode' => $inputData['requestMode'],
                        'requestObjectId' => $inputData['requestObjectId'],
                        'products' => explode(',',$inputData['returnProducts']),
                        'grnNo' => $inputData['grnNo'],
                        'stock_flow_id' => $inputData['stock_flow_id']
                    );
                    $data['invoice_file']=$uploadedFileName;
                    $data['invoice_number']=$inputData['grnInvoice'];
                    $data['remarks']=$inputData['grnRemarks'];
                    $res = $this->BranchGrn_model->addBranchGrn($data);
                    $final_response['response'] = [
                        'status' => $res['status'],
                        'message' => $res['message'],
                        'data' => $res['data'],
                    ];
                }
            }
            else{
                $final_response['response']=[
                    'status' => $invoiceRes['status'],
                    'message' => $invoiceRes['message'],
                    'data' => $invoiceRes['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getWarehouseGtnNumbers_get()
    {
        $searchVal=$this->get('searchVal');
        $branchId=$this->get('branchId');
        $warehouseGtnList = $this->BranchGrn_model->getGtnNumbers($searchVal,$branchId);
        $status=true;$message="";$result="";

        if ($warehouseGtnList == 'badrequest')
        {
            $message='There are no products';
            $status=false;
        }
        else if (isset($warehouseGtnList['error']) && $warehouseGtnList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $warehouseGtnList['msg'];
        }
        else if ($warehouseGtnList)
        {
            $message="list";
            $status=true;
            $result=$warehouseGtnList['data'];
        }
        else
        {
            /*No data found*/
            $message='No details found';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function getGtnProductDetails_get()
    {
        $gtnNo=$this->get('gtnId');
        $this->form_validation->set_data(array("gtnId"=>$gtnNo));

        $validations[]=["field"=>"gtnId","label"=>'gtnId',"rules"=>"required","errors"=>array('required' => 'GRN No should not be blank','regex_match' => 'GRN No should be numeric only')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $data['gtnNo']=$gtnNo;
            $res=$this->BranchGrn_model->checkGtnNoExist($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
            if($res['status']===true)
            {
                $res=$this->BranchGrn_model->getGtnProductDetails($data);
                $final_response['response']=[
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getBranchStock_get(){
        $status=true;
        $message='success';
        $branchId=$this->get('branchId');
        $res = $this->BranchGrn_model->getBranchStock($branchId);
        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }
    function getBranchStockProductWise_get(){
        $status=true;
        $message='success';
        $type=$this->get('type');
        $productId=$this->get('productId');
        $branchId=$this->get('branchId');
        $res = $this->BranchGrn_model->getBranchStockProductWise($type,$productId,$branchId);
        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }
    function getBranchGrnProducts_get()
    {
        $sequenceNumber=$this->get('sequenceNumber');
        $res = $this->BranchGrn_model->getBranchGrnProducts($sequenceNumber);
        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }
}