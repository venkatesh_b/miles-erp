<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: V Parameshwar. Date: 04-02-2016
 * Purpose: Services for CAll order confirmation.
 * 
 */

class CallOrderConfirmation extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('CallOrderConfirmation_model');
        $this->load->model('References_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){

    $this->form_validation->set_rules($data);

    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }

    }
    function getAllLeadStages_get(){

        $res = $this->CallOrderConfirmation_model->getAllLeadStages();

        if(isset($res))
        {
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);
        }

    }
    function saveLeadStages_post(){

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"userID","label"=>'userID',"rules"=>"required","errors"=>array('required' => ' should not be blank')];
              $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=$_POST;
            $leadStages=array();
            //unset($data['userID']);
            $getType=$this->CallOrderConfirmation_model->saveLeadStages($data);

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);



    }
    function getDNDCountConstant_get(){
        $this->load->model('LeadFollowUp_model');
        $res=$this->LeadFollowUp_model->getDNDCountConstant();
        if($res===false){
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => array(),
            ];

        }
        else{
            $final_response['response']=[
                'status' => TRUE,
                'message' => 'success',
                'data' => $res,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function saveDndConfiguration_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $data=array();
        $dndConfigCount=$data['dndConfigCount']=$this->post('dndConfigCount');
        $this->load->model('LeadFollowUp_model');
        $previousDndConfigCount=$this->LeadFollowUp_model->getDNDCountConstant();
        if($previousDndConfigCount===false){
            $previousDndConfigCount=0;
        }
        $data['previousDndConfigCount']=$previousDndConfigCount;
        $res=$this->CallOrderConfirmation_model->saveDndConfiguration($data);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }


}

