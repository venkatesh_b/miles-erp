<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-25
 * Time: 10:52 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH .'/libraries/sendgrid/vendor/autoload.php';
class Campaign extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("Campaign_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
        $this->load->library('SendEmail');
    }

    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE) {
            if (count($this->form_validation->error_array()) > 0) {
                return $this->form_validation->error_array();
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    function getBranchList_get(){
        $branchList = $this->Campaign_model->getBranchList();
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no branches';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList;
        }
        else
        {
            $message='There are no branches';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    public function uploadCampaignTemplate_post()
    {
        $config['upload_path'] = './uploads/campaign_template/';
        $fillName = 'campaign_template_'.date('U');
        $config['file_name']= $fillName;
        $config['allowed_types'] = 'html';
        $config['max_size']	= '0';
        $config['remove_spaces']= TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file'))
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => $this->upload->display_errors('',''),
                'data' => array(),
            ];
        }
        else
        {
            $data=$this->upload->data();
            $file_path = base_url().'uploads/campaign_template/' . $fillName.'.html';
            $final_response['response']=[
                'status' => TRUE,
                'message' => 'success',
                'data' => array('template_path'=>$file_path,'fileName'=>'uploads/campaign_template/'.$data['file_name']),
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }


    function sendTestEmailTemplate_post()
    {
        $testMail = $this->post('emailId');
        $templateContent = $this->post('templatePath');
        $message = file_get_contents($templateContent);
        $sendemail = new sendemail;
        $res = $sendemail->sendMail($testMail, 'Campaign Template', $message, array());
        if($res == 1)
        {
            $final_response['response']=[
                'status' => TRUE,
                'message' => 'success',
                'data' => array(),
            ];
            $this->response($final_response['response'], REST_Controller::HTTP_OK);
        }
    }

    function saveCampaign_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        //$validations[]=["field"=>"campaignName","label"=>'campaignName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]+$/]","errors"=>array('required' => 'Product Name should not be blank','regex_match' => 'Invalid Product Name')];
        $validations[]=["field"=>"campaignName","label"=>'campaignName',"rules"=>"required","errors"=>array('required' => 'Campaign Name should not be blank')];

        $validations[]=["field"=>"campaignBranch","label"=>'campaignBranch',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];

        $validations[]=["field"=>"campaignStart","label"=> 'campaignStart',"rules"=>"required","errors"=>array('required' => 'Start date should not be blank')];

        $validations[]=["field"=>"campaignEnd","label"=>'campaignEnd',"rules"=>"required","errors"=>array('required' => 'End date should not be blank')];

        $validations[]=["field"=>"templatePath","label"=>'templatePath',"rules"=>"required","errors"=>array('required' => 'Template required')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $data = array(
                'campaignId' =>$this->post('campaignId'),
                'name' =>$this->post('campaignName'),
                'fk_branch_id' => $this->post('campaignBranch'),
                'start_date' => date('Y-m-d',strtotime($this->post('campaignStart'))),
                'end_date' => date('Y-m-d',strtotime($this->post('campaignEnd'))),
                'description' => $this->post('campaignDescription'),
                'template_path' => $this->post('templatePath'),
                'campaign_groups' => $this->post('selectedGroups'),
                'fk_created_by' => $this->post('userID'),
                'created_date' => date('Y-m-d H:i:s')
            );
            $res=$this->Campaign_model->addCampaign($data);

            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function campaignList_get()
    {
        $this->response($this->Campaign_model->campaignDataTables());
    }
    function GroupList_get()
    {
        $this->response($this->Campaign_model->GroupDataTable());
    }
    public function getCampaignGroupItems_post()
    {
        $message='Group items data';
        $status=true;
        $groupItemsData=$this->Campaign_model->getGroupItemsList($_POST['groupItems']);
        $data=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>$groupItemsData
        );
        $this->set_response($data);
    }
    public function getFilteredGroupContacts_post()
    {
        //groupName:groupName,course:courseId,source:sourceId,tag:tagId
        /*$checkforGroupName=$this->Campaign_model->checkGroupName($_POST['groupName']);
        if($checkforGroupName == 1)
        {
            $data=array(
                "status"=>false,
                "message"=>'Name already exists',
                "data"=>array()
            );
        }
        else
        {*/
            $getGroupContactsCounts=$this->Campaign_model->getSelectedGroupContacts($_POST['groupData']);
            $data=array(
                "status"=>true,
                "message"=>'Contacts data',
                "data"=>$getGroupContactsCounts
            );
        //}
        $this->set_response($data);
    }
    public function addCampaignGroup_post()
    {
        $groupName          =   $_POST['groupName'];
        $groupDescription   =   $_POST['groupDescription'];
        $groupItemsData     =   $_POST['groupData'];
        $groupContactIds    =   $_POST['selectedContacts'];

        $checkforGroupName=$this->Campaign_model->checkGroupName($groupName);
        if($checkforGroupName == 1)
        {
            $responseData=array(
                "status"=>false,
                "message"=>'Group name already exists',
                "data"=>array()
            );
        }
        else
        {
            $responseData=$this->Campaign_model->addGroup($groupName,$groupDescription,$groupItemsData,$groupContactIds);
        }
        $this->response($responseData, REST_Controller::HTTP_OK);
    }

    public function getGroupDetailsById_get()
    {
        $groupId            =   $_GET['groupId'];
        $groupDetails       =   $this->Campaign_model->getGroupDetailsById($groupId);
        $this->response($groupDetails, REST_Controller::HTTP_OK);
    }
    public function getFilteredGroupContactsEdit_post()
    {
        //groupName:groupName,course:courseId,source:sourceId,tag:tagId

            $getGroupContactsCounts=$this->Campaign_model->getSelectedGroupContactsEdit($_POST['groupId']);
            $data=array(
                "status"=>true,
                "message"=>'Contacts data',
                "data"=>$getGroupContactsCounts
            );

        $this->set_response($data);
    }
    public function editCampaignGroup_post()
    {
        $groupName          =   $_POST['groupName'];
        $groupDescription   =   $_POST['groupDescription'];
        $groupItemsData     =   $_POST['groupData'];
        $groupContactIds    =   $_POST['selectedContacts'];
        $groupId    =   $_POST['groupId'];
        $checkforGroupExist=$this->Campaign_model->checkGroupName('',$groupId);
        if($checkforGroupExist == 0)
        {
            $responseData=array(
                "status"=>false,
                "message"=>'Group doesn\'t exists',
                "data"=>array()
            );
        }
        else {
            $checkforGroupName = $this->Campaign_model->checkEditGroupName($groupName,$groupId);
            if ($checkforGroupName == 1) {
                $responseData = array(
                    "status" => false,
                    "message" => 'Group name already exists',
                    "data" => array()
                );
            } else {

                $responseData = $this->Campaign_model->editGroup($groupName, $groupDescription, $groupItemsData, $groupContactIds,$groupId);
            }
        }
        $this->response($responseData, REST_Controller::HTTP_OK);
    }

    public function getCampaignGroupByBranchId_get()
    {
        $branchId            =   $_GET['branchId'];
        $groupsList       =   $this->Campaign_model->getCampaignGroupsByBranchId($branchId);
        $this->response($groupsList, REST_Controller::HTTP_OK);
    }
    public function getContactCountByGroupIds_get()
    {
        $groupIds            =   $_GET['groupIds'];
        $groupsList          =   $this->Campaign_model->getContactsCountByGroupIds($groupIds);
        $this->response($groupsList, REST_Controller::HTTP_OK);
    }
    public function getCampaignDetails_get()
    {
        $campaignId            =   $_GET['campaignId'];
        $campaignData          =   $this->Campaign_model->getCampaignDetailsById($campaignId);
        $this->response($campaignData, REST_Controller::HTTP_OK);
    }
    function campaignInfoList_get()
    {
        $campaignId            =   $_GET['campaignId'];
        $this->response($this->Campaign_model->campaignInfoDataTables($campaignId));
    }
    public function getCampaignInfoPiechart_get()
    {
        $campaignId            =   $_GET['campaignId'];
        $campaignData          =   $this->Campaign_model->getCampaignInfoPiechartById($campaignId);
        $this->response($campaignData, REST_Controller::HTTP_OK);
    }
    function deleteCampaign_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"campaignId","label"=>'campaignId',"rules"=>"required","errors"=>array('required' => 'Campaign Id should not be blank')];
        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $check=$this->Campaign_model->getCampaignDetailsById($this->post('campaignId'));
            $final_response['response']=[
                'status' => $check['status'],
                'message' => $check['message'],
                'data' => $check['data'],
            ];
            if($check['status']===true) {
                $data = array(
                    'campaignId' => $this->post('campaignId')
                );
                $res = $this->Campaign_model->deleteCampaign($data,$check['data'][0]);

                $final_response['response'] = [
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}