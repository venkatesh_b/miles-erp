<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: V Parameshwar. Date: 04-02-2016
 * Purpose: Services for Companies.
 * 
 */

class Company extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('Company_model');
        $this->load->model('References_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){

        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0){
                return $this->form_validation->error_array();
            }
            else{
                return true;
            }
        }
        else{
            return true;
        }

    }
    function getFeeCompanyList_get(){
        $res=$this->Company_model->getAllCompanies();
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }

    function deleteCompany_get()
    {
        $companyId=$this->get('companyId');
        $this->form_validation->set_data(array("companyId" => $this->get('companyId')));

        $validations[]=["field"=>"companyId","label"=>'companyId',"rules"=>"required|integer","errors"=>array(
            'required' => 'Company ID should not be blank','integer' => 'Company ID should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $resp_checkTag=$this->Company_model->checkCompany($companyId);
            if($resp_checkTag['status']===true){

                $res_delete=$this->Company_model->deleteCompany($companyId);

                $final_response['response']=[
                    'status' => $res_delete['status'],
                    'message' => $res_delete['message'],
                    'data' => $res_delete['data'],
                ];
                if($res_delete['status']===true){
                    //sucess
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    //fail
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
            else{
                $final_response['response']=[
                    'status' => $resp_checkTag['status'],
                    'message' => $resp_checkTag['message'],
                    'data' => $resp_checkTag['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function addCompany_post(){

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $name=trim($this->post('Name'));
        $code=trim($this->post('companyCode'));
        $voucher_code=trim($this->post('voucherCode'));
        $description=$this->post('Description');
        $status=intval($this->post('status'));
        $UserID=$this->post('UserID');

        $validations[]=["field"=>"Name","label"=>'Name',"rules"=>"required|regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => 'Name should not be blank','regex_match' => 'Name should be alphabets  only')];
        $validations[]=["field"=>"companyCode","label"=>'companyCode',"rules"=>"required","errors"=>array('required' => 'Code should not be blank ')];
        $validations[]=["field"=>"voucherCode","label"=>'voucherCode',"rules"=>"required","errors"=>array('required' => 'Voucher code should not be blank ')];
        $validations[]=["field"=>"status","label"=> 'status',"rules"=>"required|integer|regex_match[/^[0-1]$/]",
            "errors"=>array('required' => 'Status should not be blank','integer' => 'Status should be integer value only','regex_match' => 'Status should be either 0 or 1')];
        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true){
            $data=array("name"=>$name,'code'=>$code,'voucher_code'=>$voucher_code,"description"=>$description,
                'fk_created_by'=>$UserID,"created_date"=>date('Y-m-d h:i:s'),
                'status'=>(int)$status

            );

            $getType=$this->Company_model->saveCompany($data);

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getCompanies_get(){

        $this->response($this->Company_model->getCompanies());
    }
    function getSingleCompany_get(){
        $company_id=$this->get('companyID');
        // $company_id=decode($company_id);
        $this->form_validation->set_data(array("companyID" => $company_id));

        $validations[]=["field"=>"companyID","label"=>'companyID',"rules"=>"required|integer","errors"=>array(
            'required' => 'company Id should not be blank','integer' => 'company Id should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){

            $company_data=$this->Company_model->getSingleCompany($company_id);
            $final_response['response']=[
                'status' => $company_data['status'],
                'message' => $company_data['message'],
                'data' => $company_data['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function updateCompany_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $name=trim($this->post('Name'));
        $code=trim($this->post('companyCode'));
        $voucher_code=trim($this->post('voucherCode'));
        $description=$this->post('Description');
        $status=intval($this->post('status'));
        $UserID=$this->post('UserID');
        $companyID=$this->post('CompanyID');
        $validations[]=["field"=>"CompanyID","label"=>"CompanyID","rules"=>"required","errors"=>array(
            'required' => 'company ID should not be blank','integer' => 'company should be integer value only'
        )];

        $validations[]=["field"=>"Name","label"=>'Name',"rules"=>"required|regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => ' Name should not be blank','regex_match' => 'Name should be alphabets  only')];
        $validations[]=["field"=>"companyCode","label"=>'companyCode',"rules"=>"required","errors"=>array('required' => ' Code should not be blank')];
        $validations[]=["field"=>"voucherCode","label"=>'voucherCode',"rules"=>"required","errors"=>array('required' => ' Voucher code should not be blank')];
        $validations[]=["field"=>"status","label"=> 'status',"rules"=>"required|integer|regex_match[/^[0-1]$/]",
            "errors"=>array('required' => 'Status should not be blank','integer' => 'Status should be integer value only','regex_match' => 'Status should be either 0 or 1')];

        //$validations[]=["field"=>"roleDescription","label"=> 'Role Description ',"rules"=>"required","errors"=>array('required' => 'Role Description should not be blank')];


        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array("name"=>$name,'code'=>$code,'voucher_code'=>$voucher_code,"description"=>$description,
                'fk_created_by'=>$UserID,"created_date"=>date('Y-m-d h:i:s'),
                'status'=>$status

            );
            $getType=$this->Company_model->updateCompany($data,$companyID);
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];
        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function statusCompany(){
        $companyId=$this->get('companyId');

        $this->form_validation->set_data(array("companyId" => $this->get('companyId')));

        $validations[]=["field"=>"companyId","label"=>'companyId',"rules"=>"required|integer","errors"=>array(
            'required' => 'Company ID should not be blank','integer' => 'Company ID should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $resp_checkTag=$this->Company_model->checkCompany($companyId);
            if($resp_checkTag['status']===true){

                $res_delete=$this->Company_model->deleteCompany($companyId);
                $final_response['response']=[
                    'status' => $res_delete['status'],
                    'message' => $res_delete['message'],
                    'data' => $res_delete['data'],
                ];
                if($res_delete['status']===true){
                    //sucess
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    //fail
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
            else{
                $final_response['response']=[
                    'status' => $resp_checkTag['status'],
                    'message' => $resp_checkTag['message'],
                    'data' => $resp_checkTag['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getCompanyById_get(){
        $status=true;$message="";$result="";
        $reference_type_value_id = $this->get('company_id');
        $status=true;$message="";$result="";
        $this->form_validation->set_data(array('id' => $reference_type_value_id));
        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        }
        else
        {
            $branchList = $this->References_model->getSingleReferenceValue($reference_type_value_id);
            if ($branchList == 'badrequest')
            {
                $message="No data Found";
                $status=false;
                $result=array();
            }
            else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
            {
                $message='Database error';
                $status=false;
                $result = $branchList['msg'];
            }
            else if ($branchList)
            {
                $message="Reference Detail";
                $status=true;
                $result=$branchList;
            }
            else
            {
                $message="No data found";
                $status=false;
                $result=array();
            }
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function addCompanyReference_post(){

        $companyName=$this->input->post('companyName');
        $companyDescription=$this->input->post('companyDescription');

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"companyName","label"=>'companyName',"rules"=>"required","errors"=>array('required' => 'Company Name should not be blank','regex_match' => 'Invalid Company Name')];

        $validations[]=["field"=>"companyDescription","label"=> 'companyDescription',"rules"=>"","errors"=>array('required' => 'Company Description should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();

            $getType=$this->References_model->getReferenceType('Company');

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

            if($getType['status']===true){

                foreach($getType['data'] as $k=>$v){
                    $reftype=$v->reference_type_id;
                }
                $data = array(
                    'reference_type_id' => $reftype ,
                    'reference_type_val' => $companyName ,
                    'reference_type_val_parent' => NULL,
                    'reference_type_is_active' => 1,
                    'reference_type_val_description' => $companyDescription
                );

                $createRoleRes=$this->References_model->AddReferenceValue($data);
                $final_response['response']=[
                    'status' => $createRoleRes['status'],
                    'message' => $createRoleRes['message'],
                    'data' => $createRoleRes['data'],
                ];
                if($createRoleRes['status']===true){
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }

        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }

    function editCompanyReference_post(){

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $companyId=$this->post('companyId');
        $companyName=$this->post('companyName');
        $companyDescription=$this->post('companyDescription');

        $validations[]=["field"=>"companyId","label"=>"companyId","rules"=>"required|integer","errors"=>array(
            'required' => 'Company ID should not be blank','integer' => 'Tag should be integer value only'
        )];

        $validations[]=["field"=>"companyName","label"=>'companyName',"rules"=>"required","errors"=>array('required' => 'Company Name should not be blank','regex_match' => 'Invalid Company Name')];

        $validations[]=["field"=>"companyDescription","label"=> 'companyDescription',"rules"=>"","errors"=>array('required' => 'Company Description should not be blank')];


        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $resp_checkTag=$this->Company_model->checkCompanyReference($companyId);
            if($resp_checkTag['status']===true){
                $data=array();

                $getType=$this->References_model->getReferenceType('Company');

                $final_response['response']=[
                    'status' => $getType['status'],
                    'message' => $getType['message'],
                    'data' => $getType['data'],
                ];

                if($getType['status']===true){

                    foreach($getType['data'] as $k=>$v){
                        $reftype=$v->reference_type_id;
                    }

                    $data = array(
                        'reference_type_id' => $reftype ,
                        'reference_type_val' => $companyName ,
                        'reference_type_val_parent' => NULL,
                        'reference_type_is_active' => 1,
                        'reference_type_val_description' => $companyDescription,
                        'reference_type_val_id' => $companyId
                    );


                    $createRoleRes=$this->References_model->UpdateReferenceValue($data);
                    $final_response['response']=[
                        'status' => $createRoleRes['status'],
                        'message' => $createRoleRes['message'],
                        'data' => $createRoleRes['data'],
                    ];
                    if($createRoleRes['status']===true){
                        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                    }
                    else{
                        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                    }
                }
            }
            else{
                $final_response['response']=[
                    'status' => $resp_checkTag['status'],
                    'message' => $resp_checkTag['message'],
                    'data' => $resp_checkTag['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }

        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function addPostQualification_post(){

        $postqualificationName=$this->input->post('postqualificationName');
        $postqualificationDescription=$this->input->post('postqualificationDescription');

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"postqualificationName","label"=>'postqualificationName',"rules"=>"required","errors"=>array('required' => 'Post Qualification Name should not be blank','regex_match' => 'Invalid Company Name')];

        $validations[]=["field"=>"postqualificationDescription","label"=> 'postqualificationDescription',"rules"=>"","errors"=>array('required' => 'Post Qualification Description should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();

            $getType=$this->References_model->getReferenceType('Post Qualification');

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

            if($getType['status']===true){

                foreach($getType['data'] as $k=>$v){
                    $reftype=$v->reference_type_id;
                }
                $data = array(
                    'reference_type_id' => $reftype ,
                    'reference_type_val' => $postqualificationName ,
                    'reference_type_val_parent' => NULL,
                    'reference_type_is_active' => 1,
                    'reference_type_val_description' => $postqualificationDescription
                );

                $createRoleRes=$this->References_model->AddReferenceValue($data);
                $final_response['response']=[
                    'status' => $createRoleRes['status'],
                    'message' => $createRoleRes['message'],
                    'data' => $createRoleRes['data'],
                ];
                if($createRoleRes['status']===true){
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }

        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }

    function editPostQualification_post(){

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $postqualificationId=$this->post('postqualificationId');
        $postqualificationName=$this->post('postqualificationName');
        $postqualificationDescription=$this->post('postqualificationDescription');

        $validations[]=["field"=>"postqualificationId","label"=>"postqualificationId","rules"=>"required|integer","errors"=>array(
            'required' => 'Post Qualification ID should not be blank','integer' => 'Post Qualification should be integer value only'
        )];

        $validations[]=["field"=>"postqualificationName","label"=>'postqualificationName',"rules"=>"required","errors"=>array('required' => 'Post Qualification Name should not be blank','regex_match' => 'Invalid Post Qualification')];

        $validations[]=["field"=>"postqualificationDescription","label"=> 'postqualificationDescription',"rules"=>"","errors"=>array('required' => 'Post Qualification Description should not be blank')];


        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $resp_checkTag=$this->Company_model->checkPostQualification($postqualificationId);
            if($resp_checkTag['status']===true){
                $data=array();

                $getType=$this->References_model->getReferenceType('Post Qualification');

                $final_response['response']=[
                    'status' => $getType['status'],
                    'message' => $getType['message'],
                    'data' => $getType['data'],
                ];

                if($getType['status']===true){

                    foreach($getType['data'] as $k=>$v){
                        $reftype=$v->reference_type_id;
                    }

                    $data = array(
                        'reference_type_id' => $reftype ,
                        'reference_type_val' => $postqualificationName ,
                        'reference_type_val_parent' => NULL,
                        'reference_type_is_active' => 1,
                        'reference_type_val_description' => $postqualificationDescription,
                        'reference_type_val_id' => $postqualificationId
                    );


                    $createRoleRes=$this->References_model->UpdateReferenceValue($data);
                    $final_response['response']=[
                        'status' => $createRoleRes['status'],
                        'message' => $createRoleRes['message'],
                        'data' => $createRoleRes['data'],
                    ];
                    if($createRoleRes['status']===true){
                        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                    }
                    else{
                        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                    }
                }
            }
            else{
                $final_response['response']=[
                    'status' => $resp_checkTag['status'],
                    'message' => $resp_checkTag['message'],
                    'data' => $resp_checkTag['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }

        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}