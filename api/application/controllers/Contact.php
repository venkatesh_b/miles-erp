<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: V Parameshwar. Date: 04-02-2016
 * Purpose: Services for Contacts.
 * 
 */

class Contact extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("Contact_model");
        $this->load->model("References_model");
        $this->load->model('Branch_model');
        $this->load->model('Tag_model');
        $this->load->model('Company_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
    }
    function customvalidation($data){
   
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
    }
    function getContactConfigurations_get(){
         
        $res_reftypeid=$this->References_model->getReferenceType('City');
        $final_data=array();
        $final_data['branches']=array();
        $final_data['cities']=array();
        $final_data['tags']=array();
        $final_data['companies']=array();
        $res_reftypeid=$this->References_model->getReferenceType('City');
        if($res_reftypeid['status']==true){
                
                foreach($res_reftypeid['data'] as $k=>$v){
                    $reftype=$v->reference_type_id;
                }
               
                $branches=$this->Branch_model->getAllBranch();
                $final_data['branches']=$branches;
                
                $tags=$this->Tag_model->getAllTags();
                
                if($tags['status']==true)
                $final_data['tags']=$tags['data'];
                
                $companies=$this->Company_model->getAllCompanies();
                if($companies['status']==true)
                $final_data['companies']=$companies['data'];
                
                $cities=$this->References_model->getReferenceValuesList($reftype);
                
                if($cities['status']==true)
                $final_data['cities']=$cities['data'];   
                
        }
        $final_response['response']=[
                'status' => true,
                'message' => 'success',
                'data' => $final_data,
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    
    function getContacts_get(){
        $res=$this->Contact_model->getContacts();
        $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        
       $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addContact_post(){
        
        $data['contactName']=$contactName=$this->post('contactName');
        $data['contactCourse']=$contactCourse=$this->post('contactCourse');
        $data['contactEmail']=$contactEmail=$this->post('contactEmail');
        $data['contactPhone']=$contactPhone=$this->post('contactPhone');
        $data['contactCompany']=$contactCompany=$this->post('contactCompany');
        $data['contactBranch']=$contactBranch=$this->post('contactBranch');
        $data['contactCountry']=$contactCountry=$this->post('contactCountry');
        $data['contactCity']=$contactCity=$this->post('contactCity');
        $data['contactTags']=$contactTags=$this->post('contactTags');
                
        $validations[]=["field"=>"contactName","label"=>'contactName',"rules"=>"required|regex_match[/^[A-Za-z ]+$/]","errors"=>array('required' => 'Contact name should not be blank','regex_match' => 'Contact name should be alphabetic only')];
        
        $validations[]=["field"=>"contactCourse","label"=>'contactCourse',"rules"=>"required|regex_match[/^[A-Za-z ]+$/]","errors"=>array('required' => 'Course name should not be blank','regex_match' => 'Course name should be alphabetic only')];
        
        $validations[]=["field"=>"contactEmail","label"=>'contactEmail',"rules"=>"required|valid_email|is_unique[`contact`.`email`]","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Email is invalid','is_unique'=>'Email already exists.')];
        
        $validations[]=["field"=>"contactPhone","label"=>'contactPhone',"rules"=>"required|regex_match[/^[0-9() +-]{8,15}$/]","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number')];
        
        $validations[]=["field"=>"contactCompany","label"=>'contactCompany',"rules"=>"required|integer","errors"=>array('required' => 'Company id should not be blank','integer' => 'Company id should be numeric')];
        
        $validations[]=["field"=>"contactBranch","label"=>'contactBranch',"rules"=>"required|integer","errors"=>array('required' => 'Branch id should not be blank','integer' => 'Branch id should be numeric')];
        
        $validations[]=["field"=>"contactCountry","label"=>'contactCountry',"rules"=>"required|integer","errors"=>array('required' => 'Country id should not be blank','integer' => 'Country id should be numeric')];
        
        $validations[]=["field"=>"contactCity","label"=>'contactCity',"rules"=>"required|integer","errors"=>array('required' => 'City id should not be blank','integer' => 'City id should be numeric')];
        
        $validations[]=["field"=>"contactTags","label"=>'contactTags',"rules"=>"required|regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Tag id should not be blank','regex_match' => 'Tag ids should be numeric with comma(,) seperated')];
        
        $validationstatus=$this->customvalidation($validations);
        
        if($validationstatus===true){
            
            $expl_contactTags=explode(',',$contactTags);
            $validtags=array();
            $is_validtags=1;//0 invalid,1-valid
            for($i=0;$i<count($expl_contactTags);$i++){
                $checkTagresp=$this->Tag_model->checkTag($expl_contactTags[$i]);
                if($expl_contactTags[$i]!='' && $checkTagresp['status']===true){
                   //$is_validtags=1;
                }
                else{
                    $is_validtags=0;
                    break;
                }
            }
            if($is_validtags==1){
                $addContactres=$this->Contact_model->addContact($data);
                $final_response['response']=[
                'status' => $addContactres['status'],
                'message' => $addContactres['message'],
                'data' => $addContactres['data'],
                ];
            }
            else{
                $final_response['response']=[
                'status' => false,
                'message' => 'Invalid Tags',
                'data' => array(),
                ];
            }
           $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function editContact_post(){
        
    }
    function deleteContact_delete(){
        
        $data['contactId']=$contactId=$this->get('contactId');
        
        $this->form_validation->set_data(array("contactId" => $contactId));

        $validations[]=["field"=>"contactId","label"=>'contactId',"rules"=>"required|integer","errors"=>array('required' => 'Contact ID should not be blank','integer' => 'Contact ID should be integer value only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $chk_contact=$this->Contact_model->checkContact($data);
            $final_response['response']=[
                'status' => $chk_contact['status'],
                'message' => $chk_contact['message'],
                'data' => $chk_contact['data'],
                ];
        
        if($chk_contact['status']===true){
            
            $details=$chk_contact['data'];
            foreach($details as $k=>$v){
                $contact_status=$v->status;
            }
            
            if(isset($contact_status) && ($contact_status==0 || $contact_status==1 || $contact_status==2)){
                $activate_res=$this->Contact_model->deleteContact($data);
                $final_response['response']=[
                'status' => $activate_res['status'],
                'message' => $activate_res['message'],
                'data' => $activate_res['data'],
                ];
            }
            else{
                if(!isset($contact_status)){
                    $message="Invalid Contact ID";
                }
                else if($contact_status==3){
                    $message="Contact already deleted";
                }
                else{
                    $message="Problem";
                }
                
                $final_response['response']=[
                'status' => false,
                'message' => $message,
                'data' => array(),
                ];
                
            }
            
        }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function blockContact_post(){
        
        $validations[]=["field"=>"contactId","label"=>'contactId',"rules"=>"required|integer","errors"=>array('required' => 'Contact ID should not be blank','integer' => 'Contact ID should be integer value only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data['contactId']=$contactId=$this->post('contactId');
        $chk_contact=$this->Contact_model->checkContact($data);
        $final_response['response']=[
                'status' => $chk_contact['status'],
                'message' => $chk_contact['message'],
                'data' => $chk_contact['data'],
                ];
        
        if($chk_contact['status']===true){
            
            $details=$chk_contact['data'];
            foreach($details as $k=>$v){
                $contact_status=$v->status;
            }
            
            if(isset($contact_status) && ($contact_status==0 || $contact_status==1)){
                $activate_res=$this->Contact_model->blockContact($data);
                $final_response['response']=[
                'status' => $activate_res['status'],
                'message' => $activate_res['message'],
                'data' => $activate_res['data'],
                ];
            }
            else{
                if(!isset($contact_status)){
                    $message="Invalid Contact ID";
                }
                else if($contact_status==2){
                    $message="Contact already in blocked status";
                }
                else if($contact_status==3){
                    $message="Deleted contacts cannot be blocked";
                }
                else{
                    $message="Problem";
                }
                $final_response['response']=[
                'status' => false,
                'message' => $message,
                'data' => array(),
                ];
            }
            
        }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    
    function activeContact_post(){
        
        $validations[]=["field"=>"contactId","label"=>'contactId',"rules"=>"required|integer","errors"=>array('required' => 'Contact ID should not be blank','integer' => 'Contact ID should be integer value only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data['contactId']=$contactId=$this->post('contactId');
            $chk_contact=$this->Contact_model->checkContact($data);
            $final_response['response']=[
                'status' => $chk_contact['status'],
                'message' => $chk_contact['message'],
                'data' => $chk_contact['data'],
                ];
        
        if($chk_contact['status']===true){
            
            $details=$chk_contact['data'];
            foreach($details as $k=>$v){
                $contact_status=$v->status;
            }
            
            if(isset($contact_status) && ($contact_status==0 || $contact_status==2)){
                $activate_res=$this->Contact_model->activeContact($data);
                $final_response['response']=[
                'status' => $activate_res['status'],
                'message' => $activate_res['message'],
                'data' => $activate_res['data'],
                ];
            }
            else{
                if(!isset($contact_status)){
                    $message="Invalid Contact ID";
                }
                else if($contact_status==1){
                    $message="Contact already in active";
                }
                else if($contact_status==3){
                    $message="Deleted contacts cannot be activated";
                }
                else{
                    $message="Problem";
                }
                
                $final_response['response']=[
                'status' => false,
                'message' => $message,
                'data' => array(),
                ];
            }
            
        }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
        
    }
    function leadConvert_post(){
        
        $validations[]=["field"=>"contactId","label"=>'contactId',"rules"=>"required|integer","errors"=>array('required' => 'Contact ID should not be blank','integer' => 'Contact ID should be integer value only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data['contactId']=$contactId=$this->post('contactId');
            $chk_contact=$this->Contact_model->checkContact($data);
            $final_response['response']=[
                'status' => $chk_contact['status'],
                'message' => $chk_contact['message'],
                'data' => $chk_contact['data'],
                ];
        
        if($chk_contact['status']===true){
            
            $details=$chk_contact['data'];
            foreach($details as $k=>$v){
                $contact_status=$v->status;
                $lead_status=$v->is_lead;
            }
            
            if(isset($contact_status) && ($contact_status==0 || $contact_status==1)){
                
                if(isset($lead_status) && ($lead_status==0)){
                    $activate_res=$this->Contact_model->leadConvertContact($data);
                    $final_response['response']=[
                    'status' => $activate_res['status'],
                    'message' => $activate_res['message'],
                    'data' => $activate_res['data'],
                    ];
                }
                else{
                    if(!isset($lead_status)){
                        $message="Problem in lead coversion";
                    }
                    else if($lead_status==1){
                        $message="Already converted to lead";
                    }
                    else{
                        $message="Problem in lead coversion";
                    }
                    $final_response['response']=[
                    'status' => false,
                    'message' => $message,
                    'data' => array(),
                    ];
                }
            }
            else{
                if(!isset($contact_status)){
                    $message="Invalid Contact ID";
                }
                else if($contact_status==2){
                    $message="Blocked contacts cannot be coverted to lead";
                }
                else if($contact_status==3){
                    $message="Deleted contacts cannot be coverted to lead";
                }
                else{
                    $message="Problem";
                }
                
                $final_response['response']=[
                'status' => false,
                'message' => $message,
                'data' => array(),
                ];
            }
            
        }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function getContactCountByTags_post()
    {
        $tags=$_POST['Tags'];
        $branchId=$_POST['branchId'];
        $tag_contacts=$this->Contact_model->tagContactsCount($tags,$branchId);
        $this->response($tag_contacts, REST_Controller::HTTP_OK);
    }
    
}

