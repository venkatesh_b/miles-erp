<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class CounsellorVisitors extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("CounsellorVisitors_model");
//        $this->load->model("References_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
//        $this->load->library('Put_method_extenstion');   /* method PUT */
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    
    public function getCounsellorVisitorsList_get() {
        $branch = $this->get('branch_id');
        $userId=$_SERVER['HTTP_USER'];
        $params['branch']=$branch;
        $params['userId']=$userId;
        $params['appliedFiltersString']=$this->get('appliedFiltersString');
        $this->response($this->CounsellorVisitors_model->getCounsellorVisitorsList($params),REST_Controller::HTTP_OK);
    }
    function getCounsellorVisitorInformation_get(){
        $status=true;$message="";$results='';
        $brachXrefLeadId = $this->get('brachXrefLeadId');
        $branchId = $this->get('branchId');
        $hdnBranchVisitorId=$this->get('hdnBranchVisitorId');
        $userId=$_SERVER['HTTP_USER'];
        $params['userId']=$userId;
        $config = array(
            array(
                'field' => 'brachXrefLeadId',
                'label' => 'brachXrefLeadId',
                'rules' => 'required'
            ),
            array(
                'field' => 'branchId',
                'label' => 'branchId',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data(array('brachXrefLeadId' => $brachXrefLeadId, 'branchId' => $branchId));

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {

            $params['brachXrefLeadId']=$brachXrefLeadId;
            $params['branchId']=$branchId;
            $params['hdnBranchVisitorId']=$hdnBranchVisitorId;
            $resposne = $this->CounsellorVisitors_model->getLeadSpanshot($params);
            $message=$resposne['message'];
            $status=$resposne['status'];
            $results=$resposne['data'];

        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }
    function saveCounsellorComments_post(){

        $params['userId']=$userId=$_SERVER['HTTP_USER'];
        $params['branchId']=$branchId=$this->post('branchId');
        $params['branchXrefLeadId']=$branchXrefLeadId=$this->post('branchXrefLeadId');
        $params['comments']=$comments=$this->post('comments');
        $params['branchVisitorId']=$branchVisitorId=$this->post('branchVisitorId');
        /*if(!empty($this->post('enrollDate')) && $this->post('enrollDate')!='') {
            $enrollDate = DateTime::createFromFormat('d M, Y', $this->post('enrollDate'));
            $params['enrollDate'] = $enrollDate->format("Y-m-d");
        }
        else{
            $params['enrollDate']='';
        }*/

        $resposne = $this->CounsellorVisitors_model->saveCounsellorComments($params);

        $data=array("status"=>$resposne['status'],"message"=>$resposne['message'],"data"=>$resposne['data']);
        $this->response($data);

    }




    
}
