<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: Abhilash 
 * Purpose: Services for Courses.
 * 
 * Updated on - 6 Feb 2016
 * 
 * updated by: Parameshwar
 * updated on: 17 Feb 2016:
 * Purpose: Services for creation of courses, edit and delete of a course.
 */

class Courses extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("Course_model");
        $this->load->library('form_validation');
        $this->load->model('User_model');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->helper('encryption');
        $this->load->library('SSP');
    }
    function customvalidation($data){
   
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
}
    function getAllCourseList_get() {
        
        $res = $this->Course_model->getAllCourses();
        
        $status=true;$message="";$results='';
        if(is_array($res) && isset($res['error']) && $res['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $results = $res['msg'];
        }
        else if($res=='nodata')
        {
            $message='No Data found';
            $status=false;
            $results = array("message"=>$message);
        }
        else if($res)
        {
            $message='Courses list';
            $status=true;
            $results = array_values($res);
        }
        else
        {
            $message='No Data found';
            $status=false;
            $results = array("message"=>$message);
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }
    
    function getAllCourses_get() {
        $this->response($this->Course_model->coursesDataTables(),REST_Controller::HTTP_OK);
        exit;
        $res = $this->Course_model->getAllCourses();
        
        $status=true;$message="";$results='';
        if(is_array($res) && isset($res['error']) && $res['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $results = $res['msg'];
        }
        else if($res=='nodata')
        {
            $message='No Data found';
            $status=false;
            $results = array("message"=>$message);
        }
        else if($res)
        {
            $message='Courses list';
            $status=true;
            $results = array_values($res);
        }
        else
        {
            $message='No Data found';
            $status=false;
            $results = array("message"=>$message);
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }
    function getSingleCourse_get(){
        
        $courseId=decode($this->get('courseId'));
        $this->form_validation->set_data(array("courseId"=>$courseId));
        
        $validations[]=["field"=>"courseId","label"=>'courseId',"rules"=>"required","errors"=>array('required' => 'Course ID should not be blank','regex_match' => 'Course ID should be numeric only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data['courseId']=$courseId;
            
            $res=$this->Course_model->checkCourseExist($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
            if($res['status']===true){
            
            $res=$this->Course_model->getCourseDetailsByID($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addCourse_post(){
        
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        
        $validations[]=["field"=>"courseName","label"=>'courseName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]+$/]","errors"=>array('required' => 'Course Name should not be blank','regex_match' => 'Invalid Course Name')];
        
        $validations[]=["field"=>"courseBranches","label"=>'courseBranches',"rules"=>"regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Branch id should not be blank','regex_match' => 'Branch ids should be numeric with comma(,) seperated')];
        
        $validations[]=["field"=>"courseStatus","label"=> 'courseStatus',"rules"=>"required|integer|regex_match[/^[0-1]$/]","errors"=>array('required' => 'Course Status should not be blank','integer' => 'Course Status should be integer value only','regex_match' => 'Course Status should be either 0 or 1')];
        
        $validations[]=["field"=>"courseDescription","label"=>'courseDescription',"rules"=>"required","errors"=>array('required' => 'Course Description should not be blank')];
        
         $validations[]=["field"=>"userID","label"=>'userID',"rules"=>"required|regex_match[/^[0-9]+$/]","errors"=>array('required' => 'User ID should not be blank','regex_match' => 'User ID should be numeric only')];
        
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $checkUserRes=$this->User_model->checkUserExist($this->post('userID'));
            $final_response['response']=[
                'status' => $checkUserRes['status'],
                'message' => $checkUserRes['message'],
                'data' => $checkUserRes['data'],
                ];
            if($checkUserRes['status']===true){
            $data['userID']=$userID=$this->post('userID');
            $data['courseName']=$courseName=$this->post('courseName');
            $data['courseBranches']=$courseBranches=$this->post('courseBranches');
            $data['courseStatus']=$courseStatus=$this->post('courseStatus');
            $data['courseDescription']=$courseDescription=$this->post('courseDescription');
            $res=$this->Course_model->addCourse($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);
        
        
    }
    function updateCourse_post(){
        
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $courseId=decode($this->post('courseId'));
        $validations[]=["field"=>"courseId","label"=>'courseId',"rules"=>"required","errors"=>array('required' => 'Course ID should not be blank','regex_match' => 'Course ID should be numeric only')];
        
        $validations[]=["field"=>"courseName","label"=>'courseName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]+$/]","errors"=>array('required' => 'Course Name should not be blank','regex_match' => 'Invalid Course Name')];
        
        $validations[]=["field"=>"courseBranches","label"=>'courseBranches',"rules"=>"regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Branch id should not be blank','regex_match' => 'Branch ids should be numeric with comma(,) seperated')];
        
        $validations[]=["field"=>"courseStatus","label"=> 'courseStatus',"rules"=>"required|integer|regex_match[/^[0-1]$/]","errors"=>array('required' => 'Course Status should not be blank','integer' => 'Course Status should be integer value only','regex_match' => 'Course Status should be either 0 or 1')];
        
        $validations[]=["field"=>"courseDescription","label"=>'courseDescription',"rules"=>"required","errors"=>array('required' => 'Course Description should not be blank')];
        
        $validations[]=["field"=>"userID","label"=>'userID',"rules"=>"required|regex_match[/^[0-9]+$/]","errors"=>array('required' => 'User ID should not be blank','regex_match' => 'User ID should be numeric only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $checkUserRes=$this->User_model->checkUserExist($this->post('userID'));
            $final_response['response']=[
                'status' => $checkUserRes['status'],
                'message' => $checkUserRes['message'],
                'data' => $checkUserRes['data'],
                ];
            if($checkUserRes['status']===true){
            $data['courseId']=$courseId;
            
            $res=$this->Course_model->checkCourseExist($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
            if($res['status']===true){
            $data['userID']=$courseName=$this->post('userID');
            $data['courseName']=$courseName=$this->post('courseName');
            $data['courseBranches']=$courseBranches=$this->post('courseBranches');
            $data['courseStatus']=$courseStatus=$this->post('courseStatus');
            $data['courseDescription']=$courseDescription=$this->post('courseDescription');
            $res=$this->Course_model->updateCourse($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
            }
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    function deleteCourse_delete(){
        $courseId=decode($this->get('courseId'));
        $status = $this->get('status');
        $this->form_validation->set_data(array("courseId"=>$courseId,"userID"=>$this->get('userID')));
        
        $validations[]=["field"=>"courseId","label"=>'courseId',"rules"=>"required","errors"=>array('required' => 'Course ID should not be blank','regex_match' => 'Course ID should be numeric only')];
        
        $validations[]=["field"=>"userID","label"=>'userID',"rules"=>"required|regex_match[/^[0-9]+$/]","errors"=>array('required' => 'User ID should not be blank','regex_match' => 'User ID should be numeric only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $checkUserRes=$this->User_model->checkUserExist($this->get('userID'));
            $final_response['response']=[
                'status' => $checkUserRes['status'],
                'message' => $checkUserRes['message'],
                'data' => $checkUserRes['data'],
                ];
            if($checkUserRes['status']===true){
            $data['courseId']=$courseId;
            $res=$this->Course_model->checkCourseExist($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
            if($res['status']===true){
                $data['status'] = $status;
            $res=$this->Course_model->deleteCourse($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
            }
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getBranchCoursesList_get(){

        $branchId=$this->get('branchId');
        $params['branchId']=$branchId;
        $resp=$this->Course_model->getBranchCoursesList($params);
        $final_response['response']=[
            'status' => $resp['status'],
            'message' => $resp['message'],
            'data' => $resp['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getMultiBranchCommonCoursesList_get(){

        $branchId=$this->get('branchId');
        $params['branchId']=$branchId;
        $resp=$this->Course_model->getMultiBranchCommonCoursesList($params);
        $final_response['response']=[
            'status' => $resp['status'],
            'message' => $resp['message'],
            'data' => $resp['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    
}

