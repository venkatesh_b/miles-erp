<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {
    function __construct() {
        $_SERVER['HTTP_CONTEXT']='default';
        parent::__construct();
    }
    function downloadReport($file='') {
        $file ='./uploads/'.$file;
        if ($file and file_exists($file))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            if (readfile($file))
            {
                unlink($file);
            }
        }
    }
    function downloadFile($file='') {
        $file ='./uploads/inventory_invoice/'.$file;
        if ($file and file_exists($file))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
        }
        if (readfile($file))
        {

        }
    }
}

?>
