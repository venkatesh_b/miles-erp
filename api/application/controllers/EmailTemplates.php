<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

/**
 * @controller Name Email Template
 * @category        Controller
 * @author          Abhilash
 */
class EmailTemplates extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("EmailTemplates_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
        $this->load->library('SendEmail');
    }

    /**
     * Function get List of all Email Templates
     * 
     * @return - array
     * @created date - 29 April 2016
     * @author : Abhilash
     */
    function getAllEmail_get() {
        $this->response($this->EmailTemplates_model->getAllEmailTemplate(), REST_Controller::HTTP_OK);
    }
    /**
     * Function get template by Id
     * 
     * @return - array
     * @created date - 29 April 2016
     * @author : Abhilash
     */
    function getEmailTemplateById_get() {
        $id = $this->get('eid');
        $status = true;
        $message = "";
        $result = [];

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->EmailTemplates_model->getEmailTemplateById($id);
            if ($list == 'nodata') {
                $message = 'No Template found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $message = "Template Info";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No Template found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($data);
    }
    /**
     * Function get template preview by Id
     * 
     * @return - array
     * @created date - 29 April 2016
     * @author : Abhilash
     */
    function getEmailTemplatePreviewById_get() {
        $id = $this->get('templateID');
        $status = true;
        $message = "";
        $result = [];
        
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->EmailTemplates_model->getEmailTemplatePreviewById($id);
            if ($list == 'nodata') {
                $message = 'No Template found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $message = "Template Info";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No Template found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($data);
    }
    /**
     * Function get template by key
     * 
     * @return - array
     * @created date - 29 April 2016
     * @author : Abhilash
     */
    function getEmailTemplateByTitle_get($title) {
        $id = $title;
        $status = true;
        $message = "";
        $result = [];

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->EmailTemplates_model->getEmailTemplateByTitle($id);
            if ($list == 'nodata') {
                $message = 'No Template found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $message = "Template Info";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No Template found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($data);
    }
    /**
     * Save template
     * 
     * @return - array
     * @created date - 29 April 2016
     * @author : Abhilash
     */
    function saveTemplates_post(){
        $max_fileUpload_size = 2048;    //For max file upload size (in KB)
        $status=true;$message="";$result=[];
        
        $id = $this->get('eid');
        $status = true;
        $message = "";
        $result = [];

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $config = array(
                array(
                    'field' => 'content',
                    'label' => 'Content',
                    'rules' => 'required',
                    'errors' => array(
                        'regex_match' => 'Required',
                    ),
                ),
                array(
                    'field' => 'title',
                    'label' => 'Title',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'subject',
                    'label' => 'Subject',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                )
            );
            
            $data = $_POST;
            /*if((count($_FILES) > 0) && (($_FILES['file']['size']/1024)/1024) > $max_fileUpload_size){
                $message='Upload Error';
                $status=false;
                $result = array('imageUpload' => 'File can\'t be more than 2Mb');
                $data=array("status"=>$status,"message"=>$message,"data"=>$result);
                $this->response($data);
            }*/
            if(count($_FILES) > 0){ 
                foreach($_FILES['file']['name'] as $k=>$v){
                    preg_match_all('/\.[0-9a-z]+$/', $v,$res[]); /*For uploaded Image extension*/
                }
                $fileExtensionAllowed = '';
                foreach($res as $k=>$v){
                    $ext = str_replace('.', '', $v[0][0]);
                    $fileExtensionAllowed[$ext] = $ext;
                }
                if(count($fileExtensionAllowed) > 1){
                    $fileExtensionAllowed = implode('|', $fileExtensionAllowed);
                }else{
                    foreach($fileExtensionAllowed as $k=>$v){
                        $fileExtensionAllowed = $v;
                    }
                }
            }
            $this->form_validation->set_rules($config);
            $this->form_validation->set_data($data);
            
            if ($this->form_validation->run() == FALSE) {
                $message="Validation Error";
                $status=false;
                $result=$this->form_validation->error_array();
            } else {
                if(count($_FILES) > 0){
                    $this->load->helper(array('form', 'url'));
                    $config['upload_path'] = FILE_UPLOAD_LOCATION.'mail_template';
                    $config['allowed_types'] = str_replace('.', '', $fileExtensionAllowed);
                    /*$config['allowed_types'] = DOCUMENT_EXTENSION;
                    $config['max_size']	= DOCUMENT_MAX_SIZE;*/
                    $config['max_width']  = '1024';
                    $config['max_height']  = '768';
                    $this->load->library('upload', $config);
                    $uploaded = [];
                    foreach ($_FILES['file']['name'] as $key => $image) {
                        $_FILES['images[]']['name']= $_FILES['file']['name'][$key];
                        $_FILES['images[]']['type']= $_FILES['file']['type'][$key];
                        $_FILES['images[]']['tmp_name']= $_FILES['file']['tmp_name'][$key];
                        $_FILES['images[]']['error']= $_FILES['file']['error'][$key];
                        $_FILES['images[]']['size']= $_FILES['file']['size'][$key];
                        
                        $config['file_name'] = $data['image'][$key];

                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('images[]')) {
                            $uploaded[] = $data['image'][$key];
                            $this->upload->data();
                        } else {
                            foreach($uploaded as $k=>$v){
                                $uploaded[$k] = FILE_UPLOAD_LOCATION.'mail_template/'.$v;
                            }
                            $this->EmailTemplates_model->deleteOrginalFile($uploaded);
                            $message='Upload Error';
                            $status=false;
                            $result = $this->upload->display_errors();
                            $data=array("status"=>$status,"message"=>$message,"data"=>array('imageUpload' => $result));
                            $this->response($data);
                        }
                    }
                }
                $save = $this->EmailTemplates_model->saveTemplates($data, $id);
                if ($save === 'nodata') {
                    $message = 'No Template found';
                    $status = false;
                } else if (isset($save['error']) && $save['error'] === 'dberror') {
                    $message = 'Database error';
                    $status = false;
                    $result = $save['msg'];
                } else if($save){
                    $message = 'Email Template Saved';
                    $status = true;
                    $result = [];
                } else {
                    $message = 'Something went wrong';
                    $status = false;
                    $result = [];
                }
            }
            $data=array("status"=>$status,"message"=>$message,"data"=>$result);
            $this->response($data);
        }
    }
    /**
     * Function to delete attachemnt using the Id
     * 
     * @return - array
     * @created date - 29 April 2016
     * @author : Abhilash
     */
    function deleteAttachment_post(){
        $status=true;$message="";$result=[];
        
        $id = $this->get('eid');
        $etid = $this->get('etid');
        
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $isDeleted = $this->EmailTemplates_model->deleteAttachment($id, $etid);
            if ($isDeleted === 'nodata') {
                $message = 'No Template found';
                $status = false;
            } else if (isset($isDeleted['error']) && $isDeleted['error'] === 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $save['msg'];
            } else if($isDeleted){
                $message = 'Email Attachment Deleted';
                $status = true;
                $result = [];
            } else {
                $message = 'Something went wrong';
                $status = false;
                $result = [];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->response($data);
    }
    /**
     * Function to send text mail on the basis of the key and the email specified in frontend
     * 
     * @return - array
     * @created date - 29 April 2016
     * @author : Abhilash
     */
    function sendTestMail_get(){
        $id = $this->get('eid');
        $email = $this->get('email');
        $status = true;
        $message = "";
        $result = [];
        
        $config = array(
            array(
                'field' => 'id',
                'label' => 'Id',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'Zip code',
                'rules' => 'required|valid_email',
                'errors' => array(
                    'required' => 'required',
                    'valid_email' => 'Invalid',
                ),
            )
        );

        $this->form_validation->set_data(array('id' => $id, 'email' => $email));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->EmailTemplates_model->getEmailTemplateById($id);
            if ($list == 'nodata') {
                $message = 'No Template found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $key = $list['key'];
                $userData = array(
                    'leadName' => 'Akash',
                    'last_modified' => (new DateTime())->format('d M,Y'),
                    'leadEmail' => 'akash@test.com',
                    'courseName' => 'CPA',
                    'branchName' => 'Hyderabad',
                    'batchCode' => '1010',
                    'enrollNo' => 'XXXXXXXX',
                    'feeheadName' => '',
                    'amountPayable' => '5000',
                    'amountPaid' => '2000',
                    'due_days_amt' => '3000',
                    'due_days_html' => '',
                    'companyName' => 'CTS',
                    'institutionName' => 'JNTU',
                    'currentDate' => (new DateTime())->format('d M,Y'),
                );
                $this->load->model("EmailTemplates_model");
                $mailTemplate = $this->EmailTemplates_model->getTemplate($key,$userData);
                if(is_array($mailTemplate) && isset($mailTemplate['error']) && $mailTemplate['error'] == 'dberror'){
                    //db error
                } else if(!is_array($mailTemplate) && strlen($mailTemplate) == 'nodata'){
                    //no template found
                }else{
                    $sendemail = new sendemail;
                    $res = $sendemail->sendMail($email, $mailTemplate['title'], $mailTemplate['content'], $mailTemplate['attachment']);
                    $message = 'Check the mail';
                    $status = true;
                    $result = [];
                }
            } else {
                /* No data found */
                $message = 'No Template found';
                $status = false;
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->response($data);
    }
    
}
