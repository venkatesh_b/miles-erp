<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 02-05-2016
 * Time: 11:14 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Branch Visit
 * @category        Controller
 * @author          Abhilash
 */
class FeeDefaulters extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("FeeDefaulters_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){

        $this->form_validation->set_rules($data);
        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0){
                return $this->form_validation->error_array();
            }
            else{
                return true;
            }
        }
        else{
            return true;
        }

    }
    public function getFeeDefaultersList_get() {
        $message='Branch Details';
        $status=true;

        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $course = strlen(trim($this->get('course')))>0?"'".trim($this->get('course'))."'":'NULL';
        $batch = strlen(trim($this->get('batch')))>0?"'".trim($this->get('batch'))."'":'NULL';
        $company = strlen(trim($this->get('company')))>0?"'".trim($this->get('company'))."'":'NULL';
        $feehead = strlen(trim($this->get('feehead')))>0?"'".trim($this->get('feehead'))."'":'NULL';

        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0 && strlen(trim($this->get('batch')))<=0 && strlen(trim($this->get('company')))<=0 && strlen(trim($this->get('feehead')))<=0){
            $status=false;
            $message = 'Select any filter';
            $res = [];
        }else{
            $res = $this->FeeDefaulters_model->getFeeDefaultersList($branch, $course, $batch,$company,$feehead);
        }
        /*$data_main=array();
        for($i=0;$i<100;$i++){

            $data_main[]=array('branchName'=>"Hyd",'enrollNo'=>"CPA1001$i",'leadName'=>"kiran Kumar $i",'courseName'=>'CPA','batchCode'=>"HYDCPA1001$i",'companyName'=>'Professional','feeheadName'=>'Training Fee','amountPayable'=>'1,49,000','amountPaid'=>'70,000','dueDays'=>rand(-1,1)*$i);
        }*/
        $data=array("status"=>$status,"message"=>$message,"data"=>$res['data']);
        $this->set_response($data);
    }
    function getFeeDefaulterInfoByLeadNumber_get()
    {
        $this->form_validation->set_data(array("LeadNumber" => $this->get('LeadNumber')));
        $validations[]=["field"=>"LeadNumber","label"=>'LeadNumber',"rules"=>"required","errors"=>array(
            'required' => 'Lead number is required')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $params['LeadNumber'] = $this->get('LeadNumber');
            $res = $this->FeeDefaulters_model->getFeeDefaulterInfoByLeadNumber($params);
            $final_response['response'] = [
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function UpdateCandidateFeeDefaulter_post()
    {
        $feeConcessionData = $this->post('feeConcessionData');
        $candidateFeeId = $this->post('candidateFeeId');
        $validateFeeConcession=$this->FeeDefaulters_model->checkForCandidateFeeDefaulter($candidateFeeId,$feeConcessionData);
        if($validateFeeConcession > 0)
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Right of amount cannot be applied for this lead',
                'data' => NULL,
            ];
        }
        else
        {
            $final_response['response']=$this->FeeDefaulters_model->updateCandidateFeeDefaulter($candidateFeeId,$feeConcessionData);
        }
        $this->response($final_response['response']);
    }
}