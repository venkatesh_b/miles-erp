<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: V Parameshwar. Date: 04-02-2016
 * Purpose: Services for Leads.
 * 
 */

class Lead extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("Lead_model");
        $this->load->model("References_model");
        $this->load->model('Branch_model');
        $this->load->model('Tag_model');
        $this->load->model('Company_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){
   
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
    }
    function getDuplicateContacts_get(){
        $contactId='';
        $contactId=$this->get('contactId');
        $contactId=decode($this->get('contactId'));
        $resp=$this->Lead_model->getDuplicateContacts($contactId);
        $final_response['response']=[
            'status' => $resp['status'],
            'message' => $resp['message'],
            'data' => $resp['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function getContactCities_get(){
        $final_data=array();
        $final_data['cities']=array();
        $countryId=$this->get('countryId');
        $res_reftypeid=$this->Lead_model->getContactCities($countryId);
        $final_response['response']=[
                'status' => true,
                'message' => 'success',
                'data' => $res_reftypeid,
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
         $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getContactConfigurations_get(){
         
        $final_data=array();
        $final_data['branches']=array();
        $final_data['cities']=array();
        $final_data['tags']=array();
        $final_data['companies']=array();
        $res_reftypeid=$this->References_model->getReferenceType('Country');
        
        if($res_reftypeid['status']==true){
                
                foreach($res_reftypeid['data'] as $k=>$v){
                    $reftype=$v->reference_type_id;
                }
               
                //$branches=$this->Branch_model->getAllBranch();
                //$final_data['branches']=$branches;
                $final_data['branches']=array();
                
                $tags=$this->Tag_model->getAllTags();
                
                if($tags['status']==true)
                $final_data['tags']=$tags['data'];
                
                $companies=$this->Company_model->getAllCompanies();
                if($companies['status']==true)
                $final_data['companies']=$companies['data'];
                
                $countries=$this->References_model->getReferenceValuesList($reftype);
                if($countries['status']==true)
                    $final_data['countries']=$countries['data'];
        }
        $final_response['response']=[
                'status' => true,
                'message' => 'success',
                'data' => $final_data,
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    function getContactsList_get()
    {
        $param['appliedFiltersString']=$this->get('appliedFiltersString');
        $param['appliedCityFilters']=$this->get('appliedCityFilters');
        $this->response($this->Lead_model->leadsListDataTables($param),REST_Controller::HTTP_OK);
        exit;
    }
    function getContacts_get(){
        if(!empty($this->get('contactId')))
            $contactId=decode($this->get('contactId'));
        else
            $contactId='';
        
        $res=$this->Lead_model->getContacts($contactId);
        $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        
       $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addContact_post(){
        $dataPost = json_decode(file_get_contents("php://input"), true);
        if($dataPost){ $_POST = $dataPost; }
        //        $data = $this->input->post();
        $this->form_validation->set_data($this->post());
        $data['contactName']=$contactName=$this->post('contactName');
        //$data['contactCourse']=$contactCourse=$this->post('contactCourse');
        $data['contactEmail']=$contactEmail=$this->post('contactEmail');
        $data['contactPhone']=$contactPhone=$this->post('contactPhone');
        //$data['contactCompany']=$contactCompany=$this->post('contactCompany');
        //$data['contactBranch']=$contactBranch=$this->post('contactBranch');
        //$data['contactCountry']=$contactCountry=$this->post('contactCountry');
        $data['contactCity']=$contactCity=$this->post('contactCity');
        $data['contactCountry']=$contactCountry=$this->post('contactCountry');
        $data['contactTags']=$contactTags=$this->post('contactTags');
        $data['contactSource']=$contactSource=$this->post('contactSource');
                
        $validations[]=["field"=>"contactName","label"=>'contactName',"rules"=>"required|regex_match[/^[A-Za-z0-9- ]+$/]","errors"=>array('required' => 'Contact name should not be blank','regex_match' => 'Contact name should be alphabetic only')];
        
        //$validations[]=["field"=>"contactCourse","label"=>'contactCourse',"rules"=>"required|regex_match[/^[A-Za-z ]+$/]","errors"=>array('required' => 'Course name should not be blank','regex_match' => 'Course name should be alphabetic only')];
        
        $validations[]=["field"=>"contactEmail","label"=>'contactEmail',"rules"=>"valid_email","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Email is invalid','is_unique'=>'Email already exists.')];
        
        $validations[]=["field"=>"contactPhone","label"=>'contactPhone',"rules"=>"regex_match[/^[0-9() +-]{8,15}$/]","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number','is_unique'=>'Phone number already exists.')];
        
        //$validations[]=["field"=>"contactCompany","label"=>'contactCompany',"rules"=>"required|integer","errors"=>array('required' => 'Company id should not be blank','integer' => 'Company id should be numeric')];
        
        //$validations[]=["field"=>"contactBranch","label"=>'contactBranch',"rules"=>"required|integer","errors"=>array('required' => 'Branch id should not be blank','integer' => 'Branch id should be numeric')];
        
        //$validations[]=["field"=>"contactCountry","label"=>'contactCountry',"rules"=>"required|integer","errors"=>array('required' => 'Country id should not be blank','integer' => 'Country id should be numeric')];
        $validations[]=["field"=>"contactCountry","label"=>'contactCountry',"rules"=>"required","errors"=>array('required' => 'Country should not be blank','integer' => 'City id should be numeric')];
        $validations[]=["field"=>"contactCity","label"=>'contactCity',"rules"=>"required","errors"=>array('required' => 'City should not be blank','integer' => 'City id should be numeric')];
        //$validations[]=["field"=>"contactTags","label"=>'contactTags',"rules"=>"required","errors"=>array('required' => 'Tags should not be blank','regex_match' => 'Tag ids should be numeric with comma(,) seperated')];
        $validations[]=["field"=>"contactSource","label"=>'contactSource',"rules"=>"required","errors"=>array('required' => 'Source should not be blank','integer' => 'Source id should be numeric')];
        $validationstatus=$this->customvalidation($validations);
        
        if($validationstatus===true){
                $addContactres=$this->Lead_model->addContact($data);
                $final_response['response']=[
                'status' => $addContactres['status'],
                'message' => $addContactres['message'],
                'data' => $addContactres['data'],
                ];
            
            
           $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function editContact_post(){
        
        $dataPost = json_decode(file_get_contents("php://input"), true);
        if($dataPost){ $_POST = $dataPost; }
        //        $data = $this->input->post();
        $this->form_validation->set_data($this->post());
        $data['contactId']=$contactId=decode($this->post('contactId'));
        $data['contactName']=$contactName=$this->post('contactName');
        //$data['contactCourse']=$contactCourse=$this->post('contactCourse');
        $data['contactEmail']=$contactEmail=$this->post('contactEmail');
        $data['contactPhone']=$contactPhone=$this->post('contactPhone');
        //$data['contactCompany']=$contactCompany=$this->post('contactCompany');
        //$data['contactBranch']=$contactBranch=$this->post('contactBranch');
        //$data['contactCountry']=$contactCountry=$this->post('contactCountry');
        $data['contactCity']=$contactCity=$this->post('contactCity');
        $data['contactCountry']=$contactCountry=$this->post('contactCountry');
        $data['contactTags']=$contactTags=$this->post('contactTags');
        $data['contactSource']=$contactTags=$this->post('contactSource');
                
        $validations[]=["field"=>"contactName","label"=>'contactName',"rules"=>"required|regex_match[/^[A-Za-z0-9- ]+$/]","errors"=>array('required' => 'Contact name should not be blank','regex_match' => 'Contact name should be alphabetic only')];
        
        //$validations[]=["field"=>"contactCourse","label"=>'contactCourse',"rules"=>"required|regex_match[/^[A-Za-z ]+$/]","errors"=>array('required' => 'Course name should not be blank','regex_match' => 'Course name should be alphabetic only')];
        
        $validations[]=["field"=>"contactEmail","label"=>'contactEmail',"rules"=>"valid_email","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Email is invalid','is_unique'=>'Email already exists.')];
        
        $validations[]=["field"=>"contactPhone","label"=>'contactPhone',"rules"=>"regex_match[/^[0-9() +-]{8,15}$$/]","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number','is_unique'=>'Phone number already exists.')];
        
        //$validations[]=["field"=>"contactCompany","label"=>'contactCompany',"rules"=>"required|integer","errors"=>array('required' => 'Company id should not be blank','integer' => 'Company id should be numeric')];
        
        //$validations[]=["field"=>"contactBranch","label"=>'contactBranch',"rules"=>"required|integer","errors"=>array('required' => 'Branch id should not be blank','integer' => 'Branch id should be numeric')];
        
        //$validations[]=["field"=>"contactCountry","label"=>'contactCountry',"rules"=>"required|integer","errors"=>array('required' => 'Country id should not be blank','integer' => 'Country id should be numeric')];
        $validations[]=["field"=>"contactCountry","label"=>'contactCountry',"rules"=>"required","errors"=>array('required' => 'Country should not be blank','integer' => 'City id should be numeric')];
        $validations[]=["field"=>"contactCity","label"=>'contactCity',"rules"=>"required","errors"=>array('required' => 'City should not be blank','integer' => 'City id should be numeric')];
        //$validations[]=["field"=>"contactTags","label"=>'contactTags',"rules"=>"required","errors"=>array('required' => 'Tags should not be blank','regex_match' => 'Tag ids should be numeric with comma(,) seperated')];
        $validations[]=["field"=>"contactSource","label"=>'contactSource',"rules"=>"required","errors"=>array('required' => 'Source should not be blank','integer' => 'Source id should be numeric')];
        $validationstatus=$this->customvalidation($validations);
        
        if($validationstatus===true){
                $addContactres=$this->Lead_model->editContact($data);
                $final_response['response']=[
                'status' => $addContactres['status'],
                'message' => $addContactres['message'],
                'data' => $addContactres['data'],
                ];
            
            
           $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    function deleteContact_delete(){
        
        $data['contactId']=$contactId=$this->get('contactId');
        
        $this->form_validation->set_data(array("contactId" => $contactId));

        $validations[]=["field"=>"contactId","label"=>'contactId',"rules"=>"required|integer","errors"=>array('required' => 'Contact ID should not be blank','integer' => 'Contact ID should be integer value only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $chk_contact=$this->Contact_model->checkContact($data);
            $final_response['response']=[
                'status' => $chk_contact['status'],
                'message' => $chk_contact['message'],
                'data' => $chk_contact['data'],
                ];
        
        if($chk_contact['status']===true){
            
            $details=$chk_contact['data'];
            foreach($details as $k=>$v){
                $contact_status=$v->status;
            }
            
            if(isset($contact_status) && ($contact_status==0 || $contact_status==1 || $contact_status==2)){
                $activate_res=$this->Contact_model->deleteContact($data);
                $final_response['response']=[
                'status' => $activate_res['status'],
                'message' => $activate_res['message'],
                'data' => $activate_res['data'],
                ];
            }
            else{
                if(!isset($contact_status)){
                    $message="Invalid Contact ID";
                }
                else if($contact_status==3){
                    $message="Contact already deleted";
                }
                else{
                    $message="Problem";
                }
                
                $final_response['response']=[
                'status' => false,
                'message' => $message,
                'data' => array(),
                ];
                
            }
            
        }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function blockContact_post(){
        
        $validations[]=["field"=>"contactId","label"=>'contactId',"rules"=>"required|integer","errors"=>array('required' => 'Contact ID should not be blank','integer' => 'Contact ID should be integer value only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data['contactId']=$contactId=$this->post('contactId');
        $chk_contact=$this->Contact_model->checkContact($data);
        $final_response['response']=[
                'status' => $chk_contact['status'],
                'message' => $chk_contact['message'],
                'data' => $chk_contact['data'],
                ];
        
        if($chk_contact['status']===true){
            
            $details=$chk_contact['data'];
            foreach($details as $k=>$v){
                $contact_status=$v->status;
            }
            
            if(isset($contact_status) && ($contact_status==0 || $contact_status==1)){
                $activate_res=$this->Contact_model->blockContact($data);
                $final_response['response']=[
                'status' => $activate_res['status'],
                'message' => $activate_res['message'],
                'data' => $activate_res['data'],
                ];
            }
            else{
                if(!isset($contact_status)){
                    $message="Invalid Contact ID";
                }
                else if($contact_status==2){
                    $message="Contact already in blocked status";
                }
                else if($contact_status==3){
                    $message="Deleted contacts cannot be blocked";
                }
                else{
                    $message="Problem";
                }
                $final_response['response']=[
                'status' => false,
                'message' => $message,
                'data' => array(),
                ];
            }
            
        }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    
    function activeContact_post(){
        
        $validations[]=["field"=>"contactId","label"=>'contactId',"rules"=>"required|integer","errors"=>array('required' => 'Contact ID should not be blank','integer' => 'Contact ID should be integer value only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data['contactId']=$contactId=$this->post('contactId');
            $chk_contact=$this->Contact_model->checkContact($data);
            $final_response['response']=[
                'status' => $chk_contact['status'],
                'message' => $chk_contact['message'],
                'data' => $chk_contact['data'],
                ];
        
        if($chk_contact['status']===true){
            
            $details=$chk_contact['data'];
            foreach($details as $k=>$v){
                $contact_status=$v->status;
            }
            
            if(isset($contact_status) && ($contact_status==0 || $contact_status==2)){
                $activate_res=$this->Contact_model->activeContact($data);
                $final_response['response']=[
                'status' => $activate_res['status'],
                'message' => $activate_res['message'],
                'data' => $activate_res['data'],
                ];
            }
            else{
                if(!isset($contact_status)){
                    $message="Invalid Contact ID";
                }
                else if($contact_status==1){
                    $message="Contact already in active";
                }
                else if($contact_status==3){
                    $message="Deleted contacts cannot be activated";
                }
                else{
                    $message="Problem";
                }
                
                $final_response['response']=[
                'status' => false,
                'message' => $message,
                'data' => array(),
                ];
            }
            
        }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
        
    }
    function leadConvert_post(){
        
        $validations[]=["field"=>"contactId","label"=>'contactId',"rules"=>"required|integer","errors"=>array('required' => 'Contact ID should not be blank','integer' => 'Contact ID should be integer value only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data['contactId']=$contactId=$this->post('contactId');
            $chk_contact=$this->Contact_model->checkContact($data);
            $final_response['response']=[
                'status' => $chk_contact['status'],
                'message' => $chk_contact['message'],
                'data' => $chk_contact['data'],
                ];
        
        if($chk_contact['status']===true){
            
            $details=$chk_contact['data'];
            foreach($details as $k=>$v){
                $contact_status=$v->status;
                $lead_status=$v->is_lead;
            }
            
            if(isset($contact_status) && ($contact_status==0 || $contact_status==1)){
                
                if(isset($lead_status) && ($lead_status==0)){
                    $activate_res=$this->Contact_model->leadConvertContact($data);
                    $final_response['response']=[
                    'status' => $activate_res['status'],
                    'message' => $activate_res['message'],
                    'data' => $activate_res['data'],
                    ];
                }
                else{
                    if(!isset($lead_status)){
                        $message="Problem in lead coversion";
                    }
                    else if($lead_status==1){
                        $message="Already converted to lead";
                    }
                    else{
                        $message="Problem in lead coversion";
                    }
                    $final_response['response']=[
                    'status' => false,
                    'message' => $message,
                    'data' => array(),
                    ];
                }
            }
            else{
                if(!isset($contact_status)){
                    $message="Invalid Contact ID";
                }
                else if($contact_status==2){
                    $message="Blocked contacts cannot be coverted to lead";
                }
                else if($contact_status==3){
                    $message="Deleted contacts cannot be coverted to lead";
                }
                else{
                    $message="Problem";
                }
                
                $final_response['response']=[
                'status' => false,
                'message' => $message,
                'data' => array(),
                ];
            }
            
        }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
        
    }
    public function BulkAddContact_post(){
                
                $config['upload_path'] = './uploads/';
                $config['file_name']='file_contacts_'.date('U');
		$config['allowed_types'] = 'csv';
		$config['max_size']	= '0';
                $config['remove_spaces']= TRUE;
                
		$this->load->library('upload', $config);
                
                if (!$this->upload->do_upload('file'))
		{
			
                    $final_response['response']=[
                    'status' => FALSE,
                    'message' => $this->upload->display_errors('',''),
                    'data' => array(),
                    ];
                        
			
		}
		else
		{
                    $data=$this->upload->data();
                    
                    $this->load->library('csvreader');
                    $result = $this->csvreader->parse_file($data['full_path']);
                   if(!isset($result[1]['Name']))
                    {
                        $final_response['response']=[
                            'status' => false,
                            'message' => "No contact name (Name) Column found in excel",
                            'data' => array(),
                            ];
                    }
                    elseif(!isset($result[1]['Phone'])){
                        $final_response['response']=[
                            'status' => false,
                            'message' => "No Phone number (Phone) Column found in excel",
                            'data' => array(),
                            ];
                    }
                    elseif(!isset($result[1]['Email'])){
                        $final_response['response']=[
                            'status' => false,
                            'message' => "No email (Email) Column found in excel",
                            'data' => array(),
                            ];
                    }
                    elseif(!isset($result[1]['Country'])){
                        $final_response['response']=[
                            'status' => false,
                            'message' => "No Country (Country) Column found in excel",
                            'data' => array(),
                            ];
                    }
                    elseif(!isset($result[1]['City'])){
                        $final_response['response']=[
                            'status' => false,
                            'message' => "No City (City) Column found in excel",
                            'data' => array(),
                            ];
                    }
                    else{
                        $excelCount=0;
                        foreach($result as $k_csv=>$v_csv){
                            
                            if(trim($v_csv['Name'])=='' && trim($v_csv['Phone'])=='' && trim($v_csv['Email'])=='' && trim($v_csv['Country'])=='' && trim($v_csv['City'])=='' && trim($v_csv['Tags'])=='' ){
                                
                                unset($result[$k_csv]);
                            }
                            else{
                                $excelCount++;
                            }
                        }
                        if($excelCount>0){
                            $data['Rows'] = array_values($result);
                            $resp=$this->Lead_model->BulkContacts($data);
                            $final_response['response']=[
                            'status' => $resp['status'],
                            'message' => $resp['message'],
                            'data' => $resp['data'],
                            ];
                                unlink($data['full_path']);
                            if($resp['status']===true){
                                $file_upload_id=$resp['data']['uploadId'];
                                $resp=$this->Lead_model->SaveBulkContacts($file_upload_id);
                                $final_response['response']=[
                                'status' => $resp['status'],
                                'message' => $resp['message'],
                                'data' => $resp['data'],
                                ];
                            }
                        }
                        else{
                            $final_response['response']=[
                            'status' => FALSE,
                            'message' => "No Records Found",
                            'data' => array('message' => "No Records Found"),
                            ];
                        }
                          
                    }
                   
                }
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                $this->response($final_response['response'], $final_response['responsehttpcode']);
                
    }
    function getContactCounts_get(){
        $res=$this->Lead_model->getContactCounts();
        $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        
       $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function mergeContact_post(){
        $dataPost = json_decode(file_get_contents("php://input"), true);
        if($dataPost){ $_POST = $dataPost; }
        //        $data = $this->input->post();
        $this->form_validation->set_data($this->post());
        $data['contactName']=$contactName=$this->post('contactName');
        //$data['contactCourse']=$contactCourse=$this->post('contactCourse');
        $data['contactEmail']=$contactEmail=$this->post('contactEmail');
        $data['contactPhone']=$contactPhone=$this->post('contactPhone');
        //$data['contactCompany']=$contactCompany=$this->post('contactCompany');
        //$data['contactBranch']=$contactBranch=$this->post('contactBranch');
        //$data['contactCountry']=$contactCountry=$this->post('contactCountry');
        $data['contactCity']=$contactCity=$this->post('contactCity');
        $data['contactCountry']=$contactCountry=$this->post('contactCountry');
        $data['contactTags']=$contactTags=$this->post('contactTags');
        $data['contactSource']=$contactSource=$this->post('contactSource');
        $validations[]=["field"=>"contactName","label"=>'contactName',"rules"=>"regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => 'Contact name should not be blank','regex_match' => 'Contact name should be alphabetic only')];

        //$validations[]=["field"=>"contactCourse","label"=>'contactCourse',"rules"=>"required|regex_match[/^[A-Za-z ]+$/]","errors"=>array('required' => 'Course name should not be blank','regex_match' => 'Course name should be alphabetic only')];

        $validations[]=["field"=>"contactEmail","label"=>'contactEmail',"rules"=>"valid_email","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Email is invalid','is_unique'=>'Email already exists.')];

        $validations[]=["field"=>"contactPhone","label"=>'contactPhone',"rules"=>"regex_match[/^[0-9() +-]{8,15}$","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number','is_unique'=>'Phone number already exists.')];

        //$validations[]=["field"=>"contactCompany","label"=>'contactCompany',"rules"=>"required|integer","errors"=>array('required' => 'Company id should not be blank','integer' => 'Company id should be numeric')];

        //$validations[]=["field"=>"contactBranch","label"=>'contactBranch',"rules"=>"required|integer","errors"=>array('required' => 'Branch id should not be blank','integer' => 'Branch id should be numeric')];

        //$validations[]=["field"=>"contactCountry","label"=>'contactCountry',"rules"=>"required|integer","errors"=>array('required' => 'Country id should not be blank','integer' => 'Country id should be numeric')];
        $validations[]=["field"=>"contactCountry","label"=>'contactCountry',"rules"=>"","errors"=>array('required' => 'Country should not be blank','integer' => 'City id should be numeric')];
        $validations[]=["field"=>"contactCity","label"=>'contactCity',"rules"=>"","errors"=>array('required' => 'City should not be blank','integer' => 'City id should be numeric')];
        $validations[]=["field"=>"contactTags","label"=>'contactTags',"rules"=>"","errors"=>array('required' => 'Tags should not be blank','regex_match' => 'Tag ids should be numeric with comma(,) seperated')];
        $validations[]=["field"=>"contactSource","label"=>'contactSource',"rules"=>"","errors"=>array('required' => 'Source should not be blank','integer' => 'Source id should be numeric')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true){
            $addContactres=$this->Lead_model->mergeContact($data);
            $final_response['response']=[
                'status' => $addContactres['status'],
                'message' => $addContactres['message'],
                'data' => $addContactres['data'],
            ];


            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addQuickLead_post(){
        $dataPost = json_decode(file_get_contents("php://input"), true);
        if($dataPost){ $_POST = $dataPost; }
        $this->form_validation->set_data($this->post());
        $data['contactName']=$contactName=$this->post('contactName');
        $data['contactEmail']=$contactEmail=$this->post('contactEmail');
        $data['contactPhone']=$contactPhone=$this->post('contactPhone');
        $data['contactCourse']=$contactCourse=$this->post('contactCourse');
        $data['contactCourseText']=$contactCourseText=$this->post('contactCourseText');
        $data['branchId']=$contactBranch=$this->post('branchId');
        $data['userId']=$userId=$_SERVER['HTTP_USER'];
        $data['contactDescription']=$contactDescription=$this->post('contactDescription');
        $data['LeadSource']=$this->post('LeadSource');
        $data['leadTags']=$this->post('leadTags');
        $data['leadType']=$this->post('leadType');


        $validations[]=["field"=>"contactName","label"=>'contactName',"rules"=>"required|regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => 'Contact name should not be blank','regex_match' => 'Contact name should be alphabetic only')];
        $validations[]=["field"=>"contactEmail","label"=>'contactEmail',"rules"=>"valid_email","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Email is invalid','is_unique'=>'Email already exists.')];
        $validations[]=["field"=>"contactPhone","label"=>'contactPhone',"rules"=>"regex_match[/^[0-9() +-]{8,15}$/]","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number','is_unique'=>'Phone number already exists.')];
        $validations[]=["field"=>"contactCourse","label"=>'contactCourse',"rules"=>"required","errors"=>array('required' => 'Course should not be blank')];
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];
        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true){
            $quickLeadResp=$this->Lead_model->addQuickLead($data);
            $final_response['response']=[
                'status' => $quickLeadResp['status'],
                'message' => $quickLeadResp['message'],
                'data' => $quickLeadResp['data'],
            ];
        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function addLead_post(){
        $dataPost = json_decode(file_get_contents("php://input"), true);
        if($dataPost){ $_POST = $dataPost; }

        $this->form_validation->set_data($this->post());
        $data['contactName']=$contactName=$this->post('contactName');
        $data['contactEmail']=$contactEmail=$this->post('contactEmail');
        $data['contactPhone']=$contactPhone=$this->post('contactPhone');
        $data['contactCourse']=$contactCourse=$this->post('contactCourse');
        $data['contactCourseText']=$contactCourseText=$this->post('contactCourseText');
        $data['branchId']=$contactBranch=$this->post('branchId');
        $data['userId']=$userId=$_SERVER['HTTP_USER'];
        $data['contactDescription']=$contactDescription=$this->post('contactDescription');
        $data['Leadcontacttype']=$Leadcontacttype=$this->post('Leadcontacttype');
        $data['LeadContactPostQualification']=$LeadContactPostQualification=$this->post('LeadContactPostQualification');
        $data['LeadContactQualification']=$LeadContactQualification=$this->post('LeadContactQualification');
        $data['LeadContactCompany']=$LeadContactCompany=$this->post('LeadContactCompany');
        $data['LeadContactInstitution']=$LeadContactInstitution=$this->post('LeadContactInstitution');
        $data['LeadSource']=$this->post('LeadSource');
        $data['leadTags']=$this->post('leadTags');


        $validations[]=["field"=>"contactName","label"=>'contactName',"rules"=>"required|regex_match[/^[A-Za-z0-9- ]+$/]","errors"=>array('required' => 'Contact name should not be blank','regex_match' => 'Contact name should be alphabetic only')];
        $validations[]=["field"=>"contactEmail","label"=>'contactEmail',"rules"=>"valid_email","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Email is invalid','is_unique'=>'Email already exists.')];
        $validations[]=["field"=>"contactPhone","label"=>'contactPhone',"rules"=>"regex_match[/^[0-9() +-]{8,15}$/]","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number','is_unique'=>'Phone number already exists.')];
        $validations[]=["field"=>"contactCourse","label"=>'contactCourse',"rules"=>"required","errors"=>array('required' => 'Course should not be blank')];
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];
        $validations[]=["field"=>"Leadcontacttype","label"=>'Leadcontacttype',"rules"=>"required","errors"=>array('required' => 'Contact Type Required')];
        $validations[]=["field"=>"LeadContactPostQualification","label"=>'LeadContactPostQualification',"rules"=>"required","errors"=>array('required' => 'Post Qualification Required')];
        $validations[]=["field"=>"LeadContactQualification","label"=>'LeadContactQualification',"rules"=>"required","errors"=>array('required' => 'Qualification Required')];
        if($Leadcontacttype=='Corporate'){
            $validations[]=["field"=>"LeadContactCompany","label"=>'LeadContactCompany',"rules"=>"required","errors"=>array('required' => 'Company Required')];
        }
        if($Leadcontacttype=='University'){
            $validations[]=["field"=>"LeadContactInstitution","label"=>'LeadContactInstitution',"rules"=>"required","errors"=>array('required' => 'Institution Required')];
        }
        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true){
            $quickLeadResp=$this->Lead_model->addLead($data);
            $final_response['response']=[
                'status' => $quickLeadResp['status'],
                'message' => $quickLeadResp['message'],
                'data' => $quickLeadResp['data'],
            ];
        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    public function BulkAddLead_post(){

        $config['upload_path'] = './uploads/';
        $config['file_name']='file_leads_'.date('U');
        $config['allowed_types'] = 'csv';
        $config['max_size']	= '0';
        $config['remove_spaces']= TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file'))
        {

            $final_response['response']=[
                'status' => FALSE,
                'message' => $this->upload->display_errors('',''),
                'data' => array(),
            ];


        }
        else
        {
            $data=$this->upload->data();

            $this->load->library('csvreader');
            $result = $this->csvreader->parse_file($data['full_path']);
            if(!isset($result[1]['Name']))
            {
                $final_response['response']=[
                    'status' => false,
                    'message' => "No contact name (Name) Column found in excel",
                    'data' => array(),
                ];
            }
            elseif(!isset($result[1]['Phone'])){
                $final_response['response']=[
                    'status' => false,
                    'message' => "No Phone number (Phone) Column found in excel",
                    'data' => array(),
                ];
            }
            elseif(!isset($result[1]['Email'])){
                $final_response['response']=[
                    'status' => false,
                    'message' => "No email (Email) Column found in excel",
                    'data' => array(),
                ];
            }
            elseif(!isset($result[1]['Country'])){
                $final_response['response']=[
                    'status' => false,
                    'message' => "No Country (Country) Column found in excel",
                    'data' => array(),
                ];
            }
            elseif(!isset($result[1]['City'])){
                $final_response['response']=[
                    'status' => false,
                    'message' => "No City (City) Column found in excel",
                    'data' => array(),
                ];
            }
            elseif(!isset($result[1]['Course'])){
                $final_response['response']=[
                    'status' => false,
                    'message' => "No Course Column found in excel",
                    'data' => array(),
                ];
            }
            else{
                $excelCount=0;
                foreach($result as $k_csv=>$v_csv){

                    if(trim($v_csv['Name'])=='' && trim($v_csv['Phone'])=='' && trim($v_csv['Email'])=='' && trim($v_csv['Country'])=='' && trim($v_csv['City'])=='' && trim($v_csv['Tags'])=='' ){

                        unset($result[$k_csv]);
                    }
                    else{
                        $excelCount++;
                    }
                }
                if($excelCount>0){
                    $data['Rows'] = array_values($result);
                    $data['branchId'] = $this->post('branchId');
                    $data['userId'] = $_SERVER['HTTP_USER'];
                    $resp=$this->Lead_model->BulkLeads($data);
                    $final_response['response']=[
                        'status' => $resp['status'],
                        'message' => $resp['message'],
                        'data' => $resp['data'],
                    ];
                    unlink($data['full_path']);
                    if($resp['status']===true){
                        $file_upload_id=$resp['data']['uploadId'];
                        $resp=$this->Lead_model->SaveBulkLeads($file_upload_id);
                        $final_response['response']=[
                            'status' => $resp['status'],
                            'message' => $resp['message'],
                            'data' => $resp['data'],
                        ];
                    }
                }
                else{
                    $final_response['response']=[
                        'status' => FALSE,
                        'message' => "No Records Found",
                        'data' => array('message' => "No Records Found"),
                    ];
                }

            }

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function saveTagOutStationContact_post(){
        $dataPost = json_decode(file_get_contents("php://input"), true);
        if($dataPost){ $_POST = $dataPost; }
        $contactIds=explode(',',$this->post('contactId'));
        $contactId=$data=array();
        for($i=0;$i<count($contactIds);$i++){
            $contactId[]=decode($contactIds[$i]);
        }
        $data['branchId']=$this->post('branchId');
        $data['contactId']=$contactId;
        $data['type']=$this->post('type');
        $res=$this->Lead_model->saveTagOutStationContact($data);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function saveColdCall_post(){


        $param['userId']=$_SERVER['HTTP_USER'];
        $param['leadId']=decode($this->post('leadId'));
        $param['ColdCallContactName']=$this->post('ColdCallContactName');
        $param['ColdCallContactEmail']=$this->post('ColdCallContactEmail');
        $param['ColdCallStatus']=$this->post('ColdCallStatus');
        $param['ColdcallStatusInfo']=$this->post('ColdcallStatusInfo');
        $param['ColdCallContactCourse']=$this->post('ColdCallContactCourse');
        $param['ColdCallContactBranch']=$this->post('ColdCallContactBranch');
        $param['coldCallcontacttype']=$this->post('coldCallcontacttype');
        $param['ColdCallContactQualification']=$this->post('ColdCallContactQualification');
        $param['ColdCallContactCompany']=$this->post('ColdCallContactCompany');
        $param['ColdCallContactInstitution']=$this->post('ColdCallContactInstitution');
        $param['ColdCallDescription']=$this->post('ColdCallDescription');



        $res=$this->Lead_model->saveColdCall($param);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addGuestLead_post(){
        $dataPost = json_decode(file_get_contents("php://input"), true);
        if($dataPost){ $_POST = $dataPost; }
        $this->form_validation->set_data($this->post());
        $data['contactName']=$contactName=$this->post('contactName');
        $data['contactEmail']=$contactEmail=$this->post('contactEmail');
        $data['contactPhone']=$contactPhone=$this->post('contactPhone');
        $data['contactCourse']=$contactCourse=$this->post('contactCourse');
        $data['contactCourseText']=$contactCourseText=$this->post('contactCourseText');
        $data['branchId']=$contactBranch=decode($this->post('branchId'));
        $data['userId']=$userId=$_SERVER['HTTP_USER'];
        $data['contactDescription']=$contactDescription=$this->post('contactDescription');
        $data['leadStage']=$leadStage=$this->post('leadStage');
        $data['CommentType']=$leadStage=$this->post('CommentType');
        $data['dateType']=$leadStage=$this->post('dateType');
        $chosenDate=$this->post('chosenDate');
        $commonDateFormat = DateTime::createFromFormat('d M, Y',$chosenDate);
        $data['chosenDate']= $chosenDate = $commonDateFormat->format("Y-m-d H:i:s");
        $data['counselorId']=$counselorId=$this->post('counselorId');
        $data['batchCode']=$counselorId=$this->post('batchCode');
        $validations[]=["field"=>"contactName","label"=>'contactName',"rules"=>"required|regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => 'Contact name should not be blank','regex_match' => 'Contact name should be alphabetic only')];
        $validations[]=["field"=>"contactEmail","label"=>'contactEmail',"rules"=>"valid_email","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Email is invalid','is_unique'=>'Email already exists.')];
        $validations[]=["field"=>"contactPhone","label"=>'contactPhone',"rules"=>"regex_match[/^[0-9() +-]{8,15}$/]","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number','is_unique'=>'Phone number already exists.')];
        $validations[]=["field"=>"contactCourse","label"=>'contactCourse',"rules"=>"required","errors"=>array('required' => 'Course should not be blank')];
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $quickLeadResp=$this->Lead_model->addGuestLead($data);
            $final_response['response']=[
                'status' => $quickLeadResp['status'],
                'message' => $quickLeadResp['message'],
                'data' => $quickLeadResp['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}

