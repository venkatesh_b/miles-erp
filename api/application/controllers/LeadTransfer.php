<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Lead Transfer
 * @category        Controller
 * @author          Abhilash
 */
class LeadTransfer extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("LeadTransfer_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    /**
     * Function to get Lead datatables
     *
     * @return - array
     * @created date - 30 Mar 2016
     * @author : Abhilash
     */
    public function getLeadTransferList_get() {
        $branch = $this->get('branch_id');
        $this->response($this->LeadTransfer_model->getLeadTransferList($branch),REST_Controller::HTTP_OK);
    }

    /**
     * Function to get Lead snapshot
     *
     * @return - array
     * @created date - 30 Mar 2016
     * @author : Abhilash
     */
    public function getLeadSpanshot_get(){
        $status=true;$message="";$results='';
        $id = $this->get('lead_id');
        $branch_id = $this->get('branch_id');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required'
            ),
            array(
                'field' => 'branch_id',
                'label' => 'Branch Id',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data(array('id' => $id, 'branch_id' => $branch_id));

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $resposne = $this->LeadTransfer_model->getLeadSpanshot($id, $branch_id);
//            print_r($resposne);die;
            if (is_array($resposne) && $resposne['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $results=$resposne['msg'];
            }  else if ($resposne === 'nodata') {
                $message="Lead not found";
                $status=false;
                $results=[];
            }  else if ($resposne === 'nobranch') {
                $message="Branch not found";
                $status=false;
                $results=[];
            } else if($resposne) {
                $message="Lead details";
                $status=true;
                $results=$resposne;
            } else {
                $message="Something went wrong";
                $status=false;
                $results=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }

    /**
     * Function to Save Lead Transfer
     *
     * @return - Boolean
     * @created date - 30 Mar 2016
     * @author : Abhilash
     */
    public function leadTransfer_post() {
        $status=true;$message="";$results='';
//        print_r($this->input->post());die;
        $config = array(
            array(
                'field' => 'branch',
                'label' => 'branch',
                'rules' => 'required'
            ),
            array(
                'field' => 'lead_id',
                'label' => 'lead_id',
                'rules' => 'required'
            ),
            array(
                'field' => 'desc',
                'label' => 'desc',
                'rules' => 'required'
            ),
        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $response = $this->LeadTransfer_model->leadTransfer($this->input->post());
            if (is_array($response) && $response['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $results=$response['msg'];
            }  else if ($response === 'nolead') {
                $message="Lead not found";
                $status=false;
                $results=array('txtleadNumber'=>"Lead not found");
            } else if ($response === 'nobranch') {
                $message="Branch not found";
                $status=false;
                $results=array('txtUserBranch'=>"Branch not found");
            } else if ($response === 'nocourse') {
                $message="Course not found for the branch";
                $status=false;
                $results=array('course'=>"Course not found for the branch");
            } else if($response){
                $message="Date Updated";
                $status=true;
                $results=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }

    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}