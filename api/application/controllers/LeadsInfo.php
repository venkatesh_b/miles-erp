<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 21/3/16
 * Time: 9:44 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class LeadsInfo extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('LeadsInfo_model');
        $this->load->model('CounsellorVisitors_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){

        $this->form_validation->set_rules($data);
        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0){
                return $this->form_validation->error_array();
            }
            else{
                return true;
            }
        }
        else{
            return true;
        }

    }
    function getLeadsList_get(){
        $data['branchId']=$this->get('branchId');
        if($this->get('type'))
        $data['type']=$this->get('type');
        $this->response($result=$this->LeadsInfo_model->getLeadsList($data));
    }
    function getRetailLeadsList_get(){
        $data['branchId']=$this->get('branchId');
        $data['type']=$this->get('type');
        $this->response($result=$this->LeadsInfo_model->getLeadsList($data));
    }
    function getCorporateLeadsList_get(){
        $data['branchId']=$this->get('branchId');
        $data['type']=$this->get('type');
        $this->response($result=$this->LeadsInfo_model->getLeadsList($data));
    }
    function getInstitutionalLeadsList_get(){
        $data['branchId']=$this->get('branchId');
        $data['type']=$this->get('type');
        $this->response($result=$this->LeadsInfo_model->getLeadsList($data));
    }
    function UpdatePersonalInfo_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $infoName=$this->post('pInfoName');
        $infoCourse=$this->post('pInfoCourse');
        $infomobile=$this->post('PinfoMobile');
        $infoAltmobile=$this->post('pInfoAltMobile');
        $infoCompany=$this->post('pInfoCompany');
        $leadGender=$this->post('LeadGender');
        $infoEmail=$this->post('pInfoEmail');
        $infoDesignation=$this->post('pInfoDesignation');
        $infoEducation=$this->post('pInfoEducation');
        $leadAddress=$this->post('leadAddress');
        $UserID=$this->post('UserID');
        $leadID=decode($this->post('LeadID'));

        $validations[]=["field"=>"LeadID","label"=>"LeadID","rules"=>"required","errors"=>array('required' => 'ID should not be blank')];
        $validations[]=["field"=>"pInfoName","label"=>'pInfoName',"rules"=>"required|regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => ' Name should not be blank','regex_match' => 'Name should be alphabets  only')];
        $validations[]=["field"=>"pInfoCourse","label"=>'pInfoCourse',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"pInfoAltMobile","label"=>'pInfoAltMobile',"rules"=>"required|regex_match[/^[0-9]+$/]","errors"=>array('required' => ' mobile should not be blank','regex_match' => 'mobile should be in numbers  only')];
        $validations[]=["field"=>"PinfoMobile","label"=>'PinfoMobile',"rules"=>"regex_match[/^[0-9]+$/]","errors"=>array('required' => ' mobile should not be blank','regex_match' => 'mobile should be in numbers only')];
        //$validations[]=["field"=>"pInfoCompany","label"=>'pInfoCompany',"rules"=>"required","errors"=>array('required' => ' company should not be blank')];
        $validations[]=["field"=>"pInfoEmail","label"=>'pInfoEmail',"rules"=>"required","errors"=>array('required' => 'email should not be blank')];
        $validations[]=["field"=>"pInfoDesignation","label"=>'pInfoDesignation',"rules"=>"required","errors"=>array('required' => ' Designation should not be blank')];
        $validations[]=["field"=>"pInfoEducation","label"=>'pInfoEducation',"rules"=>"required","errors"=>array('required' => ' Education should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $data=array("name"=>$infoName,'gender'=>(int)$leadGender,'fk_course_id'=>$infoCourse,'fk_designation_id'=>$infoDesignation,'fk_qualification_id'=>$infoEducation,
                'alternate_mobile'=>$infoAltmobile,'email'=>$infoEmail,'phone'=>$infomobile,'status'=>'1','address'=> $leadAddress);
            $getType=$this->LeadsInfo_model->updateinfo($data,$leadID,$this->post());
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];
        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }

    function getleadById_get(){

        $lead_id=decode($this->get('lead_id'));
        $branch_xref_lead_id='';
        if(!empty($this->get('branch_xref_lead_id'))){
            $branch_xref_lead_id=decode($this->get('branch_xref_lead_id'));
        }
        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_data=$this->LeadsInfo_model->getSingleLeadById($lead_id,$branch_xref_lead_id);
            if(isset($lead_data['data']) && !empty($lead_data['data']))
            {
                if($lead_data['data'][0]->next_followup_date != "0000-00-00 00:00:00")
                {
                    $lead_data['data'][0]->next_followup_date = date('d M,Y', strtotime($lead_data['data'][0]->next_followup_date));
                }
                else
                {
                    $lead_data['data'][0]->next_followup_date="----";
                }
                if($lead_data['data'][0]->created_on != "0000-00-00 00:00:00")
                {
                    $lead_data['data'][0]->created_on = date('d M,Y', strtotime($lead_data['data'][0]->created_on));
                }
                else
                {
                    $lead_data['data'][0]->created_on="----";
                }
                $lead_data['data'][0]->lead_id = encode($lead_data['data'][0]->lead_id);
            }
            $final_response['response']=[
                'status' => $lead_data['status'],
                'message' => $lead_data['message'],
                'data' => $lead_data['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getLeadHistoryById_get(){

        $lead_id=decode($this->get('lead_id'));
        $branch_xref_lead_id='';
        if(!empty($this->get('branch_xref_lead_id'))){
            $branch_xref_lead_id=$this->get('branch_xref_lead_id');
        }
        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_history=$this->LeadsInfo_model->getLeadHistory($lead_id,$branch_xref_lead_id);
            $final_response['response']=[
                'status' => $lead_history['status'],
                'message' => $lead_history['message'],
                'data' => $lead_history['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getTimelinesById_get(){

        $lead_id=decode($this->get('lead_id'));
        $branch_xref_lead_id='';
        if(!empty($this->get('branch_xref_lead_id'))){
            $branch_xref_lead_id=decode($this->get('branch_xref_lead_id'));
        }
        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_timelines=$this->LeadsInfo_model->getTimelinesById($lead_id,$branch_xref_lead_id);

            if(!empty($lead_timelines['data'])) {
                for($i = 0; $i < count($lead_timelines['data']); $i++) {
                    if ($lead_timelines['data'][$i]->created_on != "0000-00-00 00:00:00") {
                        $lead_timelines['data'][$i]->created_on = date('d M, Y H:i A', strtotime($lead_timelines['data'][$i]->created_on));
                    } else {
                        $lead_timelines['data'][$i]->created_on = "----";
                    }
                }
            }
            $final_response['response']=[
                'status' => $lead_timelines['status'],
                'message' => $lead_timelines['message'],
                'data' => $lead_timelines['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addEducationalInfoDetails_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $Name=$this->post('Name');
        $university=$this->post('University');
        $percentage=$this->post('Percentage');
        //$yearOfCompletion = DateTime::createFromFormat('d M, Y',$this->post('YearOfCompletion'));
        //$yearOfCompletion=$yearOfCompletion->format("Y-m-d");
        $yearOfCompletion=$this->post('YearOfCompletion');
        $comments=$this->post('Comments');
        $lead_id=decode($this->post('LeadID'));
        $UserID=$this->post('UserID');
        $courseDropDown=$this->post('selDropDown');

        $validations[]=["field"=>"Name","label"=>"Name","rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"University","label"=>'University',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"Percentage","label"=>'Percentage',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"YearOfCompletion","label"=>'YearOfCompletion',"errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"LeadID","label"=>'LeadID',"rules"=>"required","errors"=>array('required' => 'should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array("course"=>$Name,'university'=>$university,'percentage'=>$percentage,
                'year_of_completion'=>$yearOfCompletion,
                'comments'=>$comments,
                'lead_id'=>$lead_id,
                'created_on'=>date("Y-m-d H:i:s"),
                'created_by'=>$UserID
            );
            $data['fk_course_id']=$courseDropDown;
            $getType=$this->LeadsInfo_model->AddEducationalInfoDetails($data);
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function UpdateEducationalInfo_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        //echo date('Y-m-d H:i:s',strtotime($this->post('YearOfCompletion')));exit;
        $Name=$this->post('Name');
        $university=$this->post('University');
        $percentage=$this->post('Percentage');
        //$yearOfCompletion = DateTime::createFromFormat('d M, Y',$this->post('YearOfCompletion'));
        //$yearOfCompletion=$yearOfCompletion->format("Y-m-d");
        $yearOfCompletion=$this->post('YearOfCompletion');
        $comments=$this->post('Comments');
        $educationInfoId=$this->post('EducationId');
        $lead_id=decode($this->post('LeadID'));
        $UserID=$this->post('UserID');
        $courseDropDown=$this->post('selDropDown');
        $validations[]=["field"=>"Name","label"=>"Name","rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"University","label"=>'University',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"Percentage","label"=>'Percentage',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"YearOfCompletion","label"=>'YearOfCompletion',"errors"=>array('required' => '%s should not be blank')];
        $validations[]=["field"=>"LeadID","label"=>'LeadID',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"EducationId","label"=>'EducationId',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            //date('Y-m-d H:i:s',strtotime($data['dob']))
            $data=array("course"=>$Name,'university'=>$university,'percentage'=>$percentage,
                'year_of_completion'=>$yearOfCompletion,
                'comments'=>$comments,
                'lead_id'=>$lead_id,
                'updated_on'=>date("Y-m-d h:i:s"),
                'updated_by'=>$UserID
            );
            if($courseDropDown>0){
                $data['fk_course_id']=$courseDropDown;
            }
            $getType=$this->LeadsInfo_model->UpdateEducationalInfoDetails($data,$educationInfoId);

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getEducationInfoById_get(){

        $edu_id=$this->get('EducationInfoId');
        $this->form_validation->set_data(array("EducationInfoId" =>$edu_id));
        $validations[]=["field"=>"EducationInfoId","label"=>'EducationInfoId',"rules"=>"required","errors"=>array(
            'required' => 'Id should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_history=$this->LeadsInfo_model->getEducationDetailsById($edu_id);
            //$lead_history['data'][0]->year_of_completion=date("d M,Y",strtotime($lead_history['data'][0]->year_of_completion));
            $final_response['response']=[
                'status' => $lead_history['status'],
                'message' => $lead_history['message'],
                'data' => $lead_history['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getLeadEducationalInfo_get(){

        $lead_id=decode($this->get('lead_id'));

        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_educational_info=$this->LeadsInfo_model->getLeadEducationalInfo($lead_id);
            for($i=0;$i<count($lead_educational_info['data']);$i++){
                if(isset($lead_educational_info['data'][$i]->year_of_completion))
                {
                    //$lead_educational_info['data'][$i]->year_of_completion = date("d M,Y", strtotime($lead_educational_info['data'][$i]->year_of_completion));
                }
            }
            $final_response['response']=[
                'status' => $lead_educational_info['status'],
                'message' => $lead_educational_info['message'],
                'data' => $lead_educational_info['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function AddDesignation_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $DesignationName=$this->post('DesignationName');
        $UserID=$this->post('UserID');
        $type=$this->post('ReferenceType');
        $validations[]=["field"=>"DesignationName","label"=>"DesignationName","rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array("value"=>$DesignationName,"fk_created_by"=>$UserID,"created_date"=>date("Y-m-d H:i:s"));
            $getType=$this->LeadsInfo_model->AddDesignation($data,$type);
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function deleteEducationalInfoData_get(){
        $infoId=$this->get('EduInfoId');

        $this->form_validation->set_data(array("EduInfoId" => $this->get('EduInfoId')));

        $validations[]=["field"=>"EduInfoId","label"=>'EduInfoId',"rules"=>"required|integer","errors"=>array(
            'required' => 'should not be blank','integer' => ' should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $res_delete=$this->LeadsInfo_model->deleteEducationInfoById($infoId);
            $final_response['response']=[
                'status' => $res_delete['status'],
                'message' => $res_delete['message'],
                'data' => $res_delete['data'],
            ];
            if($res_delete['status']===true){
                //sucess
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            else{
                //fail
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }

        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addOtherInfo_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $cpaCourse=$this->post('CpaCourse');
        $cpaDesignation=$this->post('CpaDesignation');
        $cpaReview=$this->post('CpaReview');
        $presentation=$this->post('Presentation');
        $cmaNumber=$this->post('CmaNumber');
        /*$otherInfoEligibility=$this->post('otherInfoEligibility');*/
        $UserID=$this->post('UserID');
        $leadID=decode($this->post('leadId'));
        $branch_xref_lead_id='';
        if(!empty($this->post('branch_xref_lead_id'))){
            $branch_xref_lead_id=$this->post('branch_xref_lead_id');
        }
        $this->form_validation->set_data(array("leadId" =>$leadID));

        $validations[]=["field"=>"leadId","label"=>'leadId',"rules"=>"required|integer","errors"=>array(
            'required' => 'should not be blank','integer' => ' should be integer value only')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $data = array("Significance_of_CPA"=>(int)$cpaCourse,'Willingness_to_pursue_the_CPA'=>(int)$cpaDesignation,'Response_on_Miles_CPA' =>(int)$cpaReview,
                'Value_addition' => (int)$presentation,
                'CMA_No' => (int)$cmaNumber,
                'updated_on'=>date("Y-m-d H:i:s"),
                'updated_by'=>$UserID,
                /*'eligibility'=>$otherInfoEligibility*/

            );
            $getType = $this->LeadsInfo_model->AddOtherInfoDetails($data,$leadID,$branch_xref_lead_id);
            $final_response['response'] = [
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
      else
      {
          $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);



    }
    function getOtherInfoDetailsByLeadId_get(){
        $lead_id=decode($this->get('lead_id'));
        $branch_xref_lead_id='';
        if(!empty($this->get('branch_xref_lead_id')))
        {
            $branch_xref_lead_id=decode($this->get('branch_xref_lead_id'));
        }
        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_other_info=$this->LeadsInfo_model->getOtherInfoDetailsById($lead_id,$branch_xref_lead_id);
            $final_response['response']=[
                'status' => $lead_other_info['status'],
                'message' => $lead_other_info['message'],
                'data' => $lead_other_info['data']
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getBranchVisitorById_get(){
        $branchXrefId=$this->get('branchXrefId');

        $this->form_validation->set_data(array("branchXrefId" => $branchXrefId));
        $validations[]=["field"=>"branchXrefId","label"=>'branchXrefId',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_other_info=$this->LeadsInfo_model->getBranchVisitorById($branchXrefId);

            if(!empty($lead_other_info['data'][0])){
                //$lead_other_info['data'][0]->created_date=date("d M,Y",strtotime($lead_other_info['data'][0]->created_date));
            }
            $final_response['response']=[
                'status' => $lead_other_info['status'],
                'message' => $lead_other_info['message'],
                'data' => $lead_other_info['data']
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function SaveTimeline_post(){
        $max_fileUpload_size = 1024;    //For Fmax filr upload (in KB)
        $status=true;$message="";$result="";
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        if($this->post('date')!=""){
            $timelineDate = DateTime::createFromFormat('d M, Y',$this->post('date'));
            $timelineDate=$timelineDate->format("Y-m-d H:i:s");
        }else{
            $timelineDate=null;
        }
        $information=$this->post('information');
        $leadId=decode($this->post('LeadId'));
        $typeOfTimeline=$this->post('typeOfTimeline');
        $branchXrefId=$this->post('BranchXrefId');
        $file=$this->post('image');
        $UserID=$this->post('userID');
        /*if(!empty($file)) {
            if ((($_FILES['file']['size'] / 1024) / 1024) > $max_fileUpload_size) {
                $message = 'Upload Error';
                $status = false;
                $result = array('attachemntUpload' => 'File can\'t be more than 2Mb');
                $data = array("status" => $status, "message" => $message, "data" => $result);
                $this->response($data);
            }

            preg_match_all('/\.[0-9a-z]+$/', $_FILES['file']['name'], $res); /*For uploaded Image extension
        }*/
		if(count($_FILES) > 0){
            preg_match_all('/\.[0-9a-z]+$/', $_FILES['file']['name'], $res); 
        }
        $this->form_validation->set_data(array("BranchXrefId" =>$branchXrefId));

        $validations[]=["field"=>"BranchXrefId","label"=>'BranchXrefId',"rules"=>"required|integer","errors"=>array(
            'required' => 'should not be blank','integer' => ' should be integer value only')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
			$data = array(
                            "lead_id"=>$leadId,
                            "type"=>$typeOfTimeline,
                            "interaction_date"=>$timelineDate,
                            'description'=>$information,
                            'created_on'=>date("Y-m-d H:i:s"),
                            'created_by' =>$UserID,
                            "branch_xref_lead_id"=>$branchXrefId
                        );
            if(count($_FILES) > 0)
            {
                $this->load->helper(array('form', 'url'));
                $config['upload_path'] = FILE_UPLOAD_LOCATION;
                $config['file_name'] = $file;
                $config['allowed_types'] = '*';
                /*$config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = $max_fileUpload_size;*/
                //$config['allowed_types'] = str_replace('.', '', $res[0][0]);
                $this->load->library('upload', $config);
                /*$data = array("lead_id"=>$leadId,"interaction_file"=>$file,"type"=>$typeOfTimeline,"interaction_date"=>$timelineDate,'description'=>$information,'created_on'=>date("Y-m-d H:i:s"),'created_by' =>$UserID,"branch_xref_lead_id"=>$branchXrefId
                );*/
                if (!$this->upload->do_upload('file'))
                {
                    $message='Upload Error';
                    $status=false;
                    $result = array('imageUpload' => $this->upload->display_errors());
                    $data=array("status"=>$status,"message"=>$message,"data"=>$result);
                    $this->response($data);
                }
                $data['interaction_file'] = $file;
                $getType = $this->LeadsInfo_model->saveTimelineDetails($data);
            }
            else
            {
                $getType = $this->LeadsInfo_model->saveTimelineDetails($data);
            }
//                $config['max_width']  = '1024';
//                $config['max_height']  = '768';



            $final_response['response'] = [
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);



    }

    function ShowLeadsCount_get(){
        $leads_count=array();
        $branchId=$this->get('branchId');
        $leadType=$this->get('type');
        $newLeadsCount=$this->LeadsInfo_model->getNewLeadsCount("M3",$branchId,$leadType);
        $branchXcefLeads=$this->LeadsInfo_model->getNewLeadsCount('',$branchId,$leadType);
        $transferdLeadsCount=$this->LeadsInfo_model->TransferdLeadsCount($branchId,$leadType);
        $students=$this->LeadsInfo_model->getNewLeadsCount("M7",$branchId,$leadType);
        $leads_count['new_leads']=$newLeadsCount['data'][0]->NewLeads;
        $leads_count['total_leads']=$branchXcefLeads['data'][0]->NewLeads - $transferdLeadsCount['data'][0]->Transfered;
        $leads_count['transferd_leds']=$transferdLeadsCount['data'][0]->Transfered;
        $leads_count['completed_leds']=$students['data'][0]->NewLeads;
        $this->response($result=$leads_count);
    }
    function getleadId_get(){

        $bxref_lead_id=decode($this->get('branch_xref_lead_id'));
        $this->form_validation->set_data(array("branch_xref_lead_id" => $bxref_lead_id));
        $validations[]=["field"=>"branch_xref_lead_id","label"=>'branch_xref_lead_id',"rules"=>"required","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_data=$this->LeadsInfo_model->getleadId($bxref_lead_id);

            $final_response['response']=[
                'status' => $lead_data['status'],
                'message' => $lead_data['message'],
                'data' => $lead_data['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function updateLeadCourseById_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $lead_id=decode($this->post('lead_id'));
        $branch_xref_lead_id=($this->post('branch_xref_lead_id'));
        $course_id=$this->post('courseId');
        $course_name=$this->post('courseName');
        $lead_update_response=$this->LeadsInfo_model->updateLeadCourse($lead_id,$branch_xref_lead_id,$course_id,$course_name);
        $this->CounsellorVisitors_model->releaseLeadIfExist($branch_xref_lead_id);
        $this->response($lead_update_response, REST_Controller::HTTP_OK);
    }

    function saveStudentResume_post()
    {
        $max_fileUpload_size = 1024;    //For Fmax file upload (in KB)

        $final_response=[
            'status' => FALSE,
            'message' => '',
            'data' => [],
        ];

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $comment=$this->post('comment');
        $leadId=decode($this->post('LeadId'));
        $branch_xref_lead_id=decode($this->post('branch_xref_lead_id'));
        $fileName=$this->post('image');
        $UserID=$this->post('userID');

        $validations[]=["field"=>"LeadId","label"=>'Lead ID',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $data = array(
                "lead_id"=>$leadId,
                "placement_file"=>'resume/'.$fileName,
                "placement_comments"=>$comment
            );
            if(count($_FILES) > 0)
            {
                $this->load->helper(array('form', 'url'));
                $config['upload_path'] = FILE_UPLOAD_LOCATION.'resume/';
                $config['file_name'] = $fileName;
                $config['allowed_types'] = '*';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('file'))
                {
                    $message='Upload Error';
                    $status=false;
                    $result = array('imageUpload' => $this->upload->display_errors());
                    $data=array("status"=>$status,"message"=>$message,"data"=>$result);
                    $this->response($data);
                }

                $orgData = $this->LeadsInfo_model->getleadId($branch_xref_lead_id);
                if($orgData){
                    $orgData = $orgData['data'][0]->placement_file;
                    if($orgData!='')
                        $orgData = FILE_UPLOAD_LOCATION.''.$orgData;
                }

                $result = $this->LeadsInfo_model->saveStudentResume($data);
                if($result){
                    //deleted previous resume uploaded
                    if($orgData!='' && file_exists($orgData))
                        unlink($orgData);

                    $final_response = [
                        'status' => true,
                        'message' => 'Resume Uploaded',
                        'data' => [],
                    ];
                }else{
                    $final_response = [
                        'status' => false,
                        'message' => 'Something went wrong',
                        'data' => [],
                    ];
                }
            }
        }
        else
        {
            $final_response=[
                'status' => FALSE,
                'message' => 'Validation failed',
                'data' => $validationstatus,
            ];
        }

        $this->response($final_response);
    }
    function getNetLeadsList_get(){
        $data['branchId']=$this->get('branchId');
        $this->response($result=$this->LeadsInfo_model->getNetLeadsList($data));
    }
    function showNetLeadsCount_get(){
        $leads_count=array();
        $branchId=$this->get('branchId');
        $leads_count['yesterday']=$this->LeadsInfo_model->getNetYesterdaysLeadsCount($type='yesterday',$branchId);
        $leads_count['today']=$this->LeadsInfo_model->getNetYesterdaysLeadsCount($type='today',$branchId);
        $leads_count['total']=$this->LeadsInfo_model->getNetYesterdaysLeadsCount($type='',$branchId);

        $this->response($result=$leads_count);
    }
    function saveEligibility_post()
    {
        $max_fileUpload_size = 1024;    //For Fmax file upload (in KB)

        $final_response=[
            'status' => FALSE,
            'message' => '',
            'data' => [],
        ];

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $eligibility=$this->post('eligibility');
        /*$leadId=decode($this->post('LeadId'));*/
        $branch_xref_lead_id=decode($this->post('branch_xref_lead_id'));
        $UserID=$this->post('userID');

        $validations[]=["field"=>"LeadId","label"=>'Lead ID',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {

            if(count($_FILES) > 0)
            {
                $fileName=$this->post('image');
                //$orginalFile=$this->post('orginalFile');
                $orginalFile = explode('.', $_FILES['file']['name'])[0];
                $data = array(
                    "branch_xref_lead_id"=>$branch_xref_lead_id,
                    "orginal_filename"=>$orginalFile,
                    "filepath"=>'eligibility/'.$fileName,
                    "type"=>'eligibility',
                    "created_by"=> $UserID
                );

                $this->load->helper(array('form', 'url'));
                $config['upload_path'] = FILE_UPLOAD_LOCATION.'eligibility/';
                $config['file_name'] = $fileName;
                $config['allowed_types'] = '*';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('file'))
                {
                    $message='Upload Error';
                    $status=false;
                    $result = array('imageUpload' => $this->upload->display_errors());
                    $data=array("status"=>$status,"message"=>$message,"data"=>$result);
                    $this->response($data);
                }
                $this->db->trans_start();
                $resultId = $this->LeadsInfo_model->saveEligibilityDownload($data);
                $result = $this->LeadsInfo_model->saveEligibility(array("branch_xref_lead_id"=>$branch_xref_lead_id, "eligibility"=>$eligibility));
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $final_response = array('error' => 'loaderror', 'msg' => $error = $this->db->error()['message'], "data"=>[]);
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                    $final_response = array("status"=>true,'message' => 'Saved successfully',"data"=>array("uploadId"=>$resultId));;
                }
            }else{
                $result = $this->LeadsInfo_model->saveEligibility(array("branch_xref_lead_id"=>$branch_xref_lead_id, "eligibility"=>$eligibility));
                if($result){
                    $final_response = [
                        'status' => true,
                        'message' => 'Saved successfully',
                        'data' => [],
                    ];
                }else{
                    $final_response = [
                        'status' => false,
                        'message' => 'Something went wrong',
                        'data' => [],
                    ];
                }
            }
        }
        else
        {
            $final_response=[
                'status' => FALSE,
                'message' => 'Validation failed',
                'data' => $validationstatus,
            ];
        }

        $this->response($final_response);
    }
    function getEligibilityFileList_get(){
        $branch_xref_lead_id=decode($this->get('branch_xref_lead_id'));
        $this->response($this->LeadsInfo_model->getEligibilityFileList($type='eligibility',$branch_xref_lead_id));
    }
    function deleteEligibilityFile_get(){
        /*$branch_xref_lead_uploads_id=decode($this->get('branch_xref_lead_id'));*/
        $branch_xref_lead_uploads_id=$this->get('branch_xref_lead_uploads_id');
        $this->response($this->LeadsInfo_model->deleteEligibilityFile($branch_xref_lead_uploads_id));
    }
    function saveStudentRefrence_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"LeadId","label"=>'Lead ID',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];

        $validations[]=["field"=>"comment","label"=>'Comment',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];

        $validations[]=["field"=>"leads","label"=>'Leads',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $data = $this->post();

            $data['LeadId'] = decode($data['LeadId']);

            $data['leads'] = explode('-', $data['leads']);
            foreach($data['leads'] as $v){
                $studentRef[] = array(
                    "branch_xref_lead_id"=>$data['LeadId'],
                    "lead_id"=>$v,
                    "created_by"=>$_SERVER['HTTP_USER'],
                );
            }
            unset($data['leads']);

            $this->LeadsInfo_model->deleteStudentRefrence(array('branch_xref_lead_id' => $data['LeadId']));

            $final_response = $this->LeadsInfo_model->saveStudentRefrence($studentRef, array('branch_xref_lead_id' => $data['LeadId'], 'refrence_comment' => $data['comment']));
        }else{
            $final_response=[
                'status' => FALSE,
                'message' => 'Validation failed',
                'data' => $validationstatus,
            ];
        }
        $this->response($final_response);
    }

    public function getStudentReference_get(){
        $branch_xref_lead_id=decode($this->get('branch_xref_lead_id'));
        $this->response($this->LeadsInfo_model->getStudentReference($branch_xref_lead_id));
    }
}