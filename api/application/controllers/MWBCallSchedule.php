<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 15-03-2016
 * Time: 02:46 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class MWBCallSchedule extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("MWBCallSchedule_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function customvalidation($data)
    {

        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE) {
            if (count($this->form_validation->error_array()) > 0) {
                return $this->form_validation->error_array();
            } else {
                return true;
            }
        } else {
            return true;
        }

    }
    function getCallCounts_get(){
        $status=true;$message='success';$data='';
        $param='';
        $branchId=$this->get('branchId');
        $fromDate=$this->get('fromDate');
        $toDate=$this->get('toDate');
        $this->form_validation->set_data(array('branchId'=>$branchId));
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true) {
            $status=TRUE;$message='success';$data='';
            $param['branchId']=$branchId;
            $param['userId']=$_SERVER['HTTP_USER'];
            $param['fromDate']=$fromDate;
            $param['toDate']=$toDate;
            $response_users=$this->MWBCallSchedule_model->getColdCallingCount($param);
            // echo $this->CallSchedule_model->getColdCallingCount($param);
            //exit;
            $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        }
        else{
            $status=FALSE;$message='validation failed';$data=$validationstatus;
        }

        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }


    function getLeadColdCallingCount_get(){

        $status=true;$message='success';$data='';
        $param='';
        $branchId=$this->get('branchId');
        //$fromDate=$this->get('fromDate');
        $toDate=$this->get('toDate');
        $lead_type=$this->get('type');
        $this->form_validation->set_data(array('branchId'=>$branchId));
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true) {
            $status=TRUE;$message='success';$data='';
            $param['branchId']=$branchId;
            $param['userId']=$_SERVER['HTTP_USER'];
            //$param['fromDate']=$fromDate;
            $param['toDate']=$toDate;
            $param['type']=$lead_type;
            $response_users=$this->MWBCallSchedule_model->getLeadColdCallingCount($param);
            // echo $this->CallSchedule_model->getColdCallingCount($param);
            //exit;
            $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        }
        else{
            $status=FALSE;$message='validation failed';$data=$validationstatus;
        }

        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function getCallSchedules_get()
    {
        $status=true;$message='success';$data='';
        $param='';
        $branchId=$this->get('branchId');
        //$fromDate=$this->get('fromDate');
        $toDate=$this->get('toDate');
        $lead_type=$this->get('type');
        $this->form_validation->set_data(array('branchId'=>$branchId));
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true)
        {
            $status=TRUE;$message='success';$data='';
            $param['branchId']=$branchId;
            $param['userId']=$_SERVER['HTTP_USER'];
            //$param['fromDate']=$fromDate;
            $param['toDate']=$toDate;
            $param['type']=$lead_type;
            $response_users=$this->MWBCallSchedule_model->getCallSchedules($param);
           // echo $this->CallSchedule_model->getColdCallingCount($param);
            //exit;
            $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        }
        else
        {
            $status=FALSE;$message='validation failed';$data=$validationstatus;
        }
        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }


    function addCallSchedule_post(){
        $status=true;$message='success';$data='';
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }

        $param['branchId']=$this->post('branchId');
        $param['leadCallSchedules']=$this->post('leadCallSchedules');
        //$fromDate=$this->post('fromDate');
        $toDate=$this->post('toDate');
        $param['createdBy']=$_SERVER['HTTP_USER'];
        //$param['fromDate']=$fromDate;
        $param['toDate']=$toDate;
        $param['type']=$this->post('type');
        $response_users=$this->MWBCallSchedule_model->addCallSchedule($param);
        $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function releaseScheduledCalls_post(){

        $status=true;$message='success';$data='';
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }

        $param['branchId']=$this->post('branchId');
        $param['releaseCnt']=$this->post('releaseCnt');
        $param['releaseBy']=$_SERVER['HTTP_USER'];
        $param['assignedUserId']=$this->post('assignedUserId');
        //$fromDate=$this->post('fromDate');
        $toDate=$this->post('toDate');
        //$param['fromDate']=$fromDate;
        $param['toDate']=$toDate;
        $param['type']=$this->post('type');
        $response_users=$this->MWBCallSchedule_model->releaseCallSchedule($param);
        $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }


}