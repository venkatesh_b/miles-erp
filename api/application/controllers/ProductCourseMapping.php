<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-11
 * Time: 02:08 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class ProductCourseMapping extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("ProductCourseMapping_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE) {
            if (count($this->form_validation->error_array()) > 0) {
                return $this->form_validation->error_array();
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    function getProductsInformation_get()
    {
        $searchVal=$this->get('searchVal');
        $this->response($result=$this->ProductCourseMapping_model->getProductsInformation($searchVal));
    }
    function getProductCourseMappings_get()
    {
        $this->response($this->ProductCourseMapping_model->productCourseMappingDataTables(),REST_Controller::HTTP_OK);
    }
    function addProductCourseMapping_post()
    {
        $inputData = $this->post();
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"courseId","label"=>'courseId',"rules"=>"required","errors"=>array('required' => 'Course should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $data = array(
                'userID' => $inputData['userID'],
                'courseId' => $inputData['courseId'],
                'productMappings' => $inputData['productMappings']
            );
            $MapExist = $this->ProductCourseMapping_model->checkCourseMappingExist($inputData['courseId']);
            if($MapExist['status']===false)
            {
                $res = $this->ProductCourseMapping_model->addProductCourseMapping($data);
                $final_response['response'] = [
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
            else{
                $final_response['response'] = [
                    'status' => false,
                    'message' => $MapExist['message'],
                    'data' => $MapExist['data'],
                ];
            }
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    public function getProductMappingDetails_get()
    {
        $productMapId=decode($this->get('productCourseId'));
        $this->form_validation->set_data(array("productCourseId"=>$productMapId));

        $validations[]=["field"=>"productCourseId","label"=>'productCourseId',"rules"=>"required","errors"=>array('required' => 'Product mapping id should not be blank','regex_match' => 'Product mapping id should be numeric only')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $data['productCourseId']=$productMapId;
            $res=$this->ProductCourseMapping_model->checkProductMapExist($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
            if($res['status']===true)
            {
                $res=$this->ProductCourseMapping_model->getProductMappingDetailsById($data);
                $final_response['response']=[
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    public function updateProductCourseMapping_post()
    {
        $inputData = $this->post();
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"ProductMapId","label"=>'ProductMapId',"rules"=>"required","errors"=>array('required' => 'Product mapping id should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $data = array(
                'userID' => $inputData['userID'],
                'courseId' => $inputData['courseId'],
                'productMappings' => $inputData['productMappings']
            );
            if($inputData['courseId'] === decode($inputData['ProductMapId']))
            {
                $res = $this->ProductCourseMapping_model->updateProductCourseMapping($data);
                $final_response['response'] = [
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
            else{
                $final_response['response'] = [
                    'status' => false,
                    'message' => 'Invalid Course',
                    'data' => [],
                ];
            }
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function deleteProductMapping_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $param['courseId']=decode($this->post('courseId'));
        $res=$this->ProductCourseMapping_model->deleteProductMapping($param);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}