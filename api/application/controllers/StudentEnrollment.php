<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Student Enrollment
 * @category        Controller
 * @author          Parameshwar
 */
class StudentEnrollment extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("StudentEnrollment_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    public function getAllConfigurations_get() {
        $message='Student Enrollment Details';
        $status=true;
        $branch = [];
        $course = [];
        $feetype = [];
        $currentbatches=[];
        $previousbatches=[];
        $branchList = $this->StudentEnrollment_model->getBranchListByUserId($_SERVER['HTTP_USER']);
        if(count($branchList) >0 ){
            foreach ($branchList as $key => $value) {
                $branch[$value['id']] = (object)$value;
                $temp = $this->StudentEnrollment_model->getAllBranchCoursesList(array('branchId'=>$value['id']))['data'];
                if(count($temp) > 0){
                    foreach ($temp as $k => $v) {
                        $course[$v->courseId] = $v;

                    }
                }else {
                    $course = [];
                }

                $currentBatchList = $this->StudentEnrollment_model->getCurrentBatchesList(array('branchId'=>$value['id']))['data'];
                if(count($currentBatchList) >0 ) {
                    foreach ($currentBatchList as $keycurrentBatch => $valuecurrentBatch) {
                        $currentbatches[] = (object)$valuecurrentBatch;
                    }
                }

                $previousBatchList = $this->StudentEnrollment_model->getPreviousBatchesList(array('branchId'=>$value['id']))['data'];
                if(count($previousBatchList) >0 ) {
                    foreach ($previousBatchList as $keypreviousBatch => $valuepreviousBatch) {
                        $previousbatches[] = (object)$valuepreviousBatch;
                    }
                }
            }
        } else {
            $branch = [];
        }

        $feetypeList = $this->StudentEnrollment_model->getFeeTypeList()['data'];
        if(count($feetypeList) >0 ) {
            foreach ($feetypeList as $keyFeeType => $valueFeeType) {
                $feetype[] = (object)$valueFeeType;
            }
        }

        $data=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>array(
                'branch' => array_values($branch),
                'course' => array_values($course),
                'feetype' => array_values($feetype),
                'currentbatches' => array_values($currentbatches),
                'previousbatches' => array_values($previousbatches)
            )
        );
        $this->set_response($data);
    }
    public function getStudentEnrollmentsReport_get() {
        $message='Student Enrollment Details';
        $status=true;
        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $course = strlen(trim($this->get('course')))>0?"'".trim($this->get('course'))."'":'NULL';
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";
        $feetype = strlen(trim($this->get('feetype')))>0?"'".trim($this->get('feetype'))."'":'NULL';
        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0 && $from_date!='' && $to_date!='' && strlen(trim($this->get('feetype')))<=0)
        {
            $status=false;
            $message = 'Select any filter';
            $res = [];
        }
        else
        {
            $res = $this->StudentEnrollment_model->getStudentEnrollmentsReport($branch, $course, $from_date,$to_date,$feetype);
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$res['data']);
        $this->set_response($data);
    }
    function getkey($pos){

        return chr(65+$pos);

    }
    public function getStudentEnrollmentsReportExport_get()
    {
        $message = 'Student Enrollment Details';
        $status = true;
        $this->load->library('excel');
        $branch = strlen(trim($this->get('branch'))) > 0 ? "'" . trim($this->get('branch')) . "'" : 'NULL';
        $course = strlen(trim($this->get('course'))) > 0 ? "'" . trim($this->get('course')) . "'" : 'NULL';
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";
        $feetype = strlen(trim($this->get('feetype')))>0?"'".trim($this->get('feetype'))."'":'NULL';
        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0 && $from_date!='' && $to_date!='' && strlen(trim($this->get('feetype')))<=0)
        {
            $status = false;
            $message = 'Select any filter';
            $res = [];
        } else {
            $res = $this->StudentEnrollment_model->getStudentEnrollmentsReport($branch,$course,$from_date,$to_date,$feetype);
        }
        if($res['status']===false){
            $data = array("status" => false, "message" => $res['message'], "data" => array());
        }
        else {
            $header_main = '';
            $datarows = array();
            $headers=array('Enroll No.','Branch','Name','Phone','Email','Qualification','Course','Batch','Fee Type','Company/Group');
            $data_main=array();
            foreach($res['data'] as $kd=>$vd){
                $data_main[]=implode('||',array('enrollNo'=>$vd->enrollNo,'branchId'=>$vd->branchId,'leadName'=>$vd->leadName,'leadPhone'=>$vd->leadPhone,'leadEmail'=>$vd->leadEmail,'leadQualification'=>$vd->leadQualification,'courseName'=>$vd->courseName,'batchCode'=>$vd->batchCode,'feeType'=>$vd->feeType,'feeCompany'=>$vd->feeCompany));
            }
            $header_main = implode('||', $headers);
            $header = $header_main;
            $headervals = explode('||', $header);

            $excelRowstartsfrom=3;
            $excelColumnstartsFrom=2;
            $excelstartsfrom=$excelRowstartsfrom;
            $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'STUDENT ENROLLMENT REPORT');

            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );

            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Branches");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('branchText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel2 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel2);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Courses");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('courseText'));
            $mrgesel2_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Batch Dates");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, ('From '.$this->get('fromDate').' To '.$this->get('toDate')));
            $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Fee Types");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('feetypeText'));
            $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;


            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Report on");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
            $excelstartsfrom=$excelstartsfrom+1;
            $head_starts_from = $excelstartsfrom+1;
            for ($k = 0; $k < count($headervals); $k++) {
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from, $headervals[$k]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );

            }
                $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
            $rowsStratsFrom = $head_starts_from + 1;
            $c = $rowsStratsFrom;
            for ($k = 0; $k < count($data_main); $k++) {
                $datavals = explode('||', $data_main[$k]);

                for ($j = 0; $j <=count($datavals)-1; $j++) {
                       $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, $datavals[$j]);
                }
                $c++;
            }

            $footer_starts_from=$c+2;

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from,'SIGNATURES');

            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '04578C')
                    )
                )
            );

            $footer_starts_from=$footer_starts_from+1;


            $test=(int)((count($headervals)-1)/2);

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$test) . ($footer_starts_from+1);
            $this->excel->getActiveSheet()->mergeCells($mrgesel);

            $mrgesel = $this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . ($footer_starts_from+1);
            $this->excel->getActiveSheet()->mergeCells($mrgesel);

            $footer_starts_from=$footer_starts_from+2;

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$test) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from,'Accountant / Cashier');

            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'a9aaae')
                    )
                )
            );




            $mrgesel = $this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from,'Branch Head');

            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'a9aaae')
                    )
                )
            );
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);

            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('STUDENT ENROLLMENT REPORT');
            $filename = "StudentEnrollmentReport_".date('Ymd_His') . '.xls'; //save our workbook as this file name
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

            //force user to download the Excel file without writing it to server's HD
            $file_path = './uploads/' . $filename;
            $objWriter->save($file_path);
            $file_path = base_url().'uploads/' . $filename;
            $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
        }
        $this->set_response($data);
    }
    public function getStudentAttendanceReport_get() {
        $message='Student Attendance Details';
        $status=true;

        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $course = strlen(trim($this->get('course')))>0?"'".trim($this->get('course'))."'":'NULL';
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";

        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0 && $from_date!='' && $to_date!='')
        {
            $status=false;
            $message = 'Select any filter';
            $res = [];
        }
        else
        {
            $res = $this->StudentEnrollment_model->getStudentAttendanceReport($branch, $course, $from_date,$to_date);
            $status=$res['status'];
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$res['data']);
        $this->set_response($data);
    }
    public function getStudentAttendanceReportExport_get()
    {

        $message = 'Student Attendance Details';
        $status = true;
        $this->load->library('excel');
        $branch = strlen(trim($this->get('branch'))) > 0 ? "'" . trim($this->get('branch')) . "'" : 'NULL';
        $course = strlen(trim($this->get('course'))) > 0 ? "'" . trim($this->get('course')) . "'" : 'NULL';
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";
        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0)
        {
            $status = false;
            $message = 'Select any filter';
            $res = [];
        }
        else
        {
            $res = $this->StudentEnrollment_model->getStudentAttendanceReport($branch, $course,$from_date,$to_date);
        }
        if($res['status']===false){
            $data = array("status" => false, "message" => $res['message'], "data" => array());
        }
        else
        {
            $header_main = '';
            $datarows = array();
            $headers=array('Enroll No.','Branch','Name','Phone','Email','Course','Batch','Scheduled Classes','Attended Classes');
            $data_main=array();
            foreach($res['data'] as $kd=>$vd)
            {
                $data_main[]=implode('||',array('enrollNo'=>$vd->enrollNo,'branchId'=>$vd->branchId,'leadName'=>$vd->leadName,'leadPhone'=>$vd->leadPhone,'leadEmail'=>$vd->leadEmail,'courseName'=>$vd->courseName,'batchCode'=>$vd->batchCode,'scheduledClassCount'=>$vd->scheduledClassCount,'attendedClassCount'=>$vd->attendedClassCount));
            }
            $header_main = implode('||', $headers);
            $header = $header_main;
            $headervals = explode('||', $header);

            $excelRowstartsfrom=3;
            $excelColumnstartsFrom=2;
            $excelstartsfrom=$excelRowstartsfrom;
            $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'STUDENT ATTENDANCE REPORT');
            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );

            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Branches");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('branchText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel2 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel2);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Courses");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('courseText'));
            $mrgesel2_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Batch Dates");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, ('From '.$this->get('fromDate').' To '.$this->get('toDate')));
            $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;


            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Report on");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
            $excelstartsfrom=$excelstartsfrom+1;
            $head_starts_from = $excelstartsfrom+1;
            for ($k = 0; $k < count($headervals); $k++) {
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from, $headervals[$k]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );

            }
            $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
            $rowsStratsFrom = $head_starts_from + 1;
            $c = $rowsStratsFrom;
            for ($k = 0; $k < count($data_main); $k++) {
                $datavals = explode('||', $data_main[$k]);

                for ($j = 0; $j <=count($datavals)-1; $j++) {
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, $datavals[$j]);
                }
                $c++;
            }
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);

            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('STUDENT ATTENDANCE REPORT');
            $filename = "StudentAttendanceReport_".date('Ymd_His') . '.xls'; //save our workbook as this file name
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

            //force user to download the Excel file without writing it to server's HD
            $file_path = './uploads/' . $filename;
            $objWriter->save($file_path);
            $file_path = base_url().'uploads/' . $filename;
            $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
        }
        $this->set_response($data);
    }
    public function getStudentAdhocAttendanceReport_get() {
        $message='Student Adhoc Attendance Details';
        $status=true;

        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $course = strlen(trim($this->get('course')))>0?"'".trim($this->get('course'))."'":'NULL';
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";

        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0  && $from_date!='' && $to_date!='')
        {
            $status=false;
            $message = 'Select any filter';
            $res = [];
        }
        else
        {
            $res = $this->StudentEnrollment_model->getStudentAdhocAttendanceReport($branch, $course, $from_date, $to_date);
            $status=$res['status'];
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$res['data']);
        $this->set_response($data);
    }
    public function getStudentAdhocAttendanceReportExport_get()
    {

        $message = 'Student Adhoc Attendance Details';
        $status = true;
        $this->load->library('excel');
        $branch = strlen(trim($this->get('branch'))) > 0 ? "'" . trim($this->get('branch')) . "'" : 'NULL';
        $course = strlen(trim($this->get('course'))) > 0 ? "'" . trim($this->get('course')) . "'" : 'NULL';
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";

        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0 && $from_date!='' && $to_date!='')
        {
            $status = false;
            $message = 'Select any filter';
            $res = [];
        }
        else
        {
            $res = $this->StudentEnrollment_model->getStudentAdhocAttendanceReport($branch, $course, $from_date, $to_date);
        }
        if($res['status']===false)
        {
            $data = array("status" => false, "message" => $res['message'], "data" => array());
        }
        else {
            $header_main = '';
            $datarows = array();
            $headers=array('Date','Venue','Subject','Faculty','Current Batch Total','Current Batch Attended','Previous Batch','Demo/Walkin','Total');
            $data_main=array();
            foreach($res['data'] as $kd=>$vd){
                $data_main[]=implode('||',array('classDate'=>$vd->classDate,'classVenue'=>$vd->classVenue,'classSubject'=>$vd->classSubject,'classFaculty'=>$vd->classFaculty,'totalBatchCount'=>$vd->totalBatchCount,'classAttendanceCount'=>$vd->classAttendanceCount,'classPreviousAttendanceCount'=>$vd->classPreviousAttendanceCount,'classDemoAttendanceCount'=>"--",'totalClassAttendanceCount'=>$vd->totalClassAttendanceCount));
            }
            $header_main = implode('||', $headers);
            $header = $header_main;
            $headervals = explode('||', $header);

            $excelRowstartsfrom=3;
            $excelColumnstartsFrom=2;
            $excelstartsfrom=$excelRowstartsfrom;
            $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'STUDENT ADHOC ATTENDANCE REPORT');

            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );

            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Branches");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('branchText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel2 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel2);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Courses");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('courseText'));
            $mrgesel2_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Batch Dates");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, ('From '.$this->get('fromDate').' To '.$this->get('toDate')));
            $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;


            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Generated on");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
            $excelstartsfrom=$excelstartsfrom+1;
            $head_starts_from = $excelstartsfrom+1;
            for ($k = 0; $k < count($headervals); $k++) {
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from, $headervals[$k]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );

            }
            $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
            $rowsStratsFrom = $head_starts_from + 1;
            $c = $rowsStratsFrom;
            for ($k = 0; $k < count($data_main); $k++) {
                $datavals = explode('||', $data_main[$k]);

                for ($j = 0; $j <=count($datavals)-1; $j++) {
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, $datavals[$j]);
                }
                $c++;
            }
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);

            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('STUDENT ADHOC ATTENDANCE REPORT');
            $filename = "StudentAdhocAttendanceReport_".date('Ymd_His') . '.xls'; //save our workbook as this file name
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

            //force user to download the Excel file without writing it to server's HD
            $file_path = './uploads/' . $filename;
            $objWriter->save($file_path);
            $file_path = base_url().'uploads/' . $filename;
            $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
        }
        $this->set_response($data);
    }

    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}