<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/*
 * Created By: Abhilash 
 * Purpose: Services for Courses.
 * 
 * Updated on - 6 Feb 2016
 * 
 * updated by: Parameshwar
 * updated on: 17 Feb 2016:
 * Purpose: Services for creation of courses, edit and delete of a course.
 */

class Subject extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("Subject_model");
//        $this->load->model("Branch_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->helper('encryption');
        $this->load->library('SSP');
    }
    
    function getSubjectDatatables_get(){
        $this->response($this->Subject_model->getSubjectDatatables(), REST_Controller::HTTP_OK);
    }
    
    function getSubjectById_get() {
        $id = $this->get('subject_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Subject_model->getSubjectById($id);
            if ($list == 'badrequest') {
                $message = 'No data found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $message = "Subject data";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No data found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function getAllSubjectByFaculty_get() {
        $id = $this->get('faculty_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Subject_model->getAllSubjectByFaculty($id);
            if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list == 'nofaculty') {
                $message = "No faculty found";
                $status = false;
                $result = [];
            } else if ($list) {
                $message = "Subject data";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No data found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function getAllSubjectList_get() {
        $status = true;
        $message = "";
        $result = "";
        $list = $this->Subject_model->getAllSubjectList();
        if (isset($list['error']) && $list['error'] == 'dberror') {
            $message = 'Database error';
            $status = false;
            $result = $list['msg'];
        } else if ($list) {
            $message = "Subject data";
            $status = true;
            $result = $list;
        } else {
            /* No data found */
            $message = 'No data found';
            $status = false;
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    /**
     * Get subject by course
     */
    function getAllSubjectByCourse_get() {
        $id = $this->get('course_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Subject_model->getAllSubjectByCourse($id);
            if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list == 'nocourse') {
                $message = 'Course is invalid';
                $status = false;
                $result = [];
            }else if ($list == 'badrequest') {
                $message = "Subject data";
                $status = true;
                $result = [];
            }else if ($list) {
                $message = "Subject data";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No data found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

    function saveSubject_post() {
        $status = true;
        $message = "";
        $result = "";
        $config = array(
            array(
                'field' => 'subjectName',
                'label' => 'Subject Name',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'subjectCode',
                'label' => 'Subject Code',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'course',
                'label' => 'Course',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            )
        );

        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $data = $this->input->post();
            $isAdded = $this->Subject_model->saveSubject($data);
            if (isset($isAdded['error']) && $isAdded['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $results = $isAdded['msg'];
            } else if ($isAdded === 'codeexist') {
                $result = array('subjectCode' => 'Code already exist');
                $message = "Code already exist";
                $status = false;
            } else if ($isAdded === 'nocourse') {
                $result = array('course' => 'Course is invalid');
                $message = "Course is invalid";
                $status = false;
            } else if ($isAdded) {
                $result = [];
                $message = "Saved successfully";
                $status = true;
            } else {
                $result = [];
                $message = "Something went wrong";
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

    function editSubject_post() {
        $id = $this->get('subject_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            
            $config = array(
                array(
                    'field' => 'subjectName',
                    'label' => 'Subject Name',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'subjectCode',
                    'label' => 'Subject Code',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'course',
                    'label' => 'Course',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                )
            );

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message = "Validation Error";
                $status = false;
                $result = $this->form_validation->error_array();
            } else {
                $data = $this->input->post();
                $isEdited = $this->Subject_model->editSubject($id, $data);
                if (isset($isEdited['error']) && $isEdited['error'] == 'dberror') {
                    $message = 'Database error';
                    $status = false;
                    $results = $isEdited['msg'];
                } else if ($isEdited === 'nocourse') {
                    $result = array('course' => 'Course is invalid');
                    $message = "Course is invalid";
                    $status = false;
                } else if ($isEdited === 'codeexist') {
                    $result = array('subjectCode' => 'Code already exist');
                    $message = "Code already exist";
                    $status = false;
                } else if ($isEdited === 'nodata') {
                    $result = [];
                    $message = "Id is invalid";
                    $status = false;
                } else if ($isEdited) {
                    $result = [];
                    $message = "Saved successfully";
                    $status = true;
                } else {
                    $result = [];
                    $message = "Something went wrong";
                    $status = false;
                }
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function deleteSubject_post() {
        $id = $this->get('subject_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $isDeleted = $this->Subject_model->deleteSubject($id);
            if (isset($isDeleted['error']) && $isDeleted['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $results = $isDeleted['msg'];
            } else if ($isDeleted === 'nodata') {
                $message = "No subject found";
                $status = false;
            } /*else if ($isDeleted === 'alreadyused') {
                $result = [];
                $message = "Venue is already been answered";
                $status = false;
            }*/ else if ($isDeleted === 'active') {
                $message = "Subject Active";
                $status = true;
            } else if ($isDeleted === 'deactivated') {
                $message = "Subject Inactive";
                $status = true;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

}
