<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

/**
 * @controller Name Branch
 * @category        Controller
 * @author          Abhilash
 */
class Survey extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Survey_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function getDefaultSurveySettings_get() {
        $this->load->model("Course_model");
        $course = $this->Course_model->getAllCourses();
        $questionTypes = $this->Survey_model->getQuestionTypes();
        $status = true;
        $message = "";
        $result = array('course' => $course, 'questionTypes' => $questionTypes);
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($data);
    }

    function getAllSurveyDataTables_get() {
        $course_id = $this->get('course_id');
        $this->response($this->Survey_model->getSurveyDataTable($course_id), REST_Controller::HTTP_OK);
    }

    function getSurveyById_get() {
        $id = $this->get('survey_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Survey_model->getSurveyById($id);
            $status = true;
            $message = "";
            $result = "";

            if ($list == 'badrequest') {
                $message = 'No data found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $message = "Survey data";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No data found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($data);
    }
    
    function saveSurveyAnswersForLead_post() {

        $data = $this->input->post();
        if(isset($_SERVER['HTTP_FOLLOWUP']) && $_SERVER['HTTP_FOLLOWUP']!='')
        {
            $followup = $_SERVER['HTTP_FOLLOWUP'];
        }
        else
        {
            $followup = '';
        }

        $isAdded = $this->Survey_model->saveSurveyAnswersForLead($data,$followup);

        if (isset($isAdded['error']) && $isAdded['error'] == 'dberror') {
            $message = 'Database error';
            $status = false;
            $results = $isAdded['msg'];
        } else if ($isAdded === 'nolead') {
            $result = [];
            $message = "No Lead found";
            $status = false;
        } else if ($isAdded === 'noOption') {
            $result = [];
            $message = "Options is invalid";
            $status = false;
        } else if ($isAdded === 'noQuestion') {
            $result = [];
            $message = "No Question found";
            $status = false;
        } else if ($isAdded) {
            $result = [];
            $message = "Saved successfully";
            $status = true;
        } else {
            $result = [];
            $message = "Something went wrong";
            $status = false;
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function getSurveyAnswersForLead_get()
    {
        $id = $this->get('lead_id');
        if(is_numeric($id)){ $id = encode($id); }
        $id = decode($id);
        
        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Survey_model->getSurveyAnswersForLead($id);
            $status = true;
            $message = "";
            $result = "";

            if ($list == 'badrequest') {
                $message = 'No data found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $message = "Survey data";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No data found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($data);
    }

    function getSurveyListByCourse_get() {
        $id = $this->get('course_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Survey_model->getSurveyListByCourse($id);
            $status = true;
            $message = "";
            $result = "";

            if ($list == 'badrequest') {
                $message = 'No data found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $message = "Survey data";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No data found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($data);
    }

    function saveSurvey_post() {
//        print_r($this->input->post());die;
        $status = true;
        $message = "";
        $result = "";
        $config = array(
            array(
                'field' => 'txtTitle',
                'label' => 'question title',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'optionType',
                'label' => 'Option Type',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'course',
                'label' => 'Course',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
        );

        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $data = $this->input->post();
            if ($this->input->post('optionType') == 'radio' || $this->input->post('optionType') == 'checkbox') {
                $config = array(
                    array(
                        'field' => 'options',
                        'label' => 'Options',
                        'rules' => 'required',
                        'errors' => array(
                            'required' => 'Required',
                        ),
                    )
                );
                $this->form_validation->set_rules($config);

                if (is_array($this->input->post('options'))) {
                    foreach ($this->input->post('options') as $key => $value) {
                        $this->form_validation->set_data(array('options' => $value['value']));
                        if ($this->form_validation->run() == FALSE) {
                            $message = "Validation Error";
                            $status = false;
                            $result = $this->form_validation->error_array();
                            $data = array("status" => $status, "message" => $message, "data" => $result);
                            $this->response($data);
                        }
                    }
                }
            }
            $isAdded = $this->Survey_model->saveSurvey($data);
            if (isset($isAdded['error']) && $isAdded['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $results = $isAdded['msg'];
            } else if ($isAdded === 'norefoptions') {
                $result = array('options' => 'Options not available');
                $message = "Options not available";
                $status = false;
            } else if ($isAdded === 'nooptions') {
                $result = array('options' => 'Options is invalid');
                $message = "Options is invalid";
                $status = false;
            } else if ($isAdded === 'nocourse') {
                $result = array('course' => 'Course is invalid');
                $message = "Course is invalid";
                $status = false;
            } else if ($isAdded) {
                $result = [];
                $message = "Saved successfully";
                $status = true;
            } else {
                $result = [];
                $message = "Something went wrong";
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

    function editSurvey_post() {
        $id = $this->get('survey_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $status = true;
            $message = "";
            $result = "";
            $config = array(
                array(
                    'field' => 'txtTitle',
                    'label' => 'question title',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'optionType',
                    'label' => 'Option Type',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
            );

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message = "Validation Error";
                $status = false;
                $result = $this->form_validation->error_array();
            } else {
                $data = $this->input->post();
                if ($this->input->post('optionType') == 'radio' || $this->input->post('optionType') == 'checkbox') {
                    $config = array(
                        array(
                            'field' => 'options',
                            'label' => 'Options',
                            'rules' => 'required',
                            'errors' => array(
                                'required' => 'Required',
                            ),
                        )
                    );
                    $this->form_validation->set_rules($config);

                    if (is_array($this->input->post('options'))) {
                        foreach ($this->input->post('options') as $key => $value) {
                            $this->form_validation->set_data(array('options' => $value['value']));
                            if ($this->form_validation->run() == FALSE) {
                                $message = "Validation Error";
                                $status = false;
                                $result = $this->form_validation->error_array();
                                $data = array("status" => $status, "message" => $message, "data" => $result);
                                $this->response($data);
                            }
                        }
                    }
                }
                $isEdited = $this->Survey_model->editSurvey($id, $data);
                if (isset($isEdited['error']) && $isEdited['error'] == 'dberror') {
                    $message = 'Database error';
                    $status = false;
                    $results = $isEdited['msg'];
                } else if ($isEdited === 'norefoptions') {
                    $result = array('options' => 'Options not available');
                    $message = "Options not available";
                    $status = false;
                } else if ($isEdited === 'nodata') {
                    $result = [];
                    $message = "No survey found";
                    $status = false;
                } else if ($isEdited === 'nocourse') {
                    $result = [];
                    $message = "Course is invalid";
                    $status = false;
                } else if ($isEdited === 'nooptions') {
                    $result = array('options' => 'Options is invalid');
                    $message = "Options is invalid";
                    $status = false;
                } else if ($isEdited) {
                    $result = [];
                    $message = "Saved successfully";
                    $status = true;
                } else {
                    $result = [];
                    $message = "Something went wrong";
                    $status = false;
                }
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

    function deleteSurvey_post() {
        $id = $this->get('survey_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');
//        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $isDeleted = $this->Survey_model->deleteSurvey($id);
            if (isset($isDeleted['error']) && $isDeleted['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $results = $isDeleted['msg'];
            } else if ($isDeleted === 'nodata') {
                $message = "No survey found";
                $status = false;
            } else if ($isDeleted === 'alreadyused') {
                $result = [];
                $message = "Question is already been answered";
                $status = false;
            } else if ($isDeleted === 'active') {
                $message = "Survey Active";
                $status = true;
            } else if ($isDeleted === 'deactivated') {
                $message = "Survey Inactive";
                $status = true;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

}
