<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Modiifed By: Abhilash. Date: 24-02-2016
 * Purpose: Services for Tags.
 * 
 */

class Tag extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('Tag_model');
        $this->load->model('References_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
    }
    function customvalidation($data){
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
    }
    function getTags_get(){
        $res=$this->Tag_model->getAllTags();
        $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    
    function getTagById_get(){
        /*$id = $this->get('tag_id');
        
        if(!preg_match('/^([0-9]+)$/', $id))
        {
            $final_response['response']=[
                'status' => false,
                'message' => "Validation Error",
                'data' => 'Invalid user type id',
            ];
        } else {
            $res=$this->Tag_model->getTagById($id);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
            
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);*/
        
        //$this->References_model
        $status=true;$message="";$result="";
        
        $reference_type_value_id = $this->get('tag_id');
        $status=true;$message="";$result="";
        
        $this->form_validation->set_data(array('id' => $reference_type_value_id));
        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE) 
        {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } 
        else 
        {
            $branchList = $this->References_model->getSingleReferenceValue($reference_type_value_id);
            if ($branchList == 'badrequest')
            {
                $message="No data Found";
                $status=false;
                $result=array();
            }
            else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
            {
                $message='Database error';
                $status=false;
                $result = $branchList['msg'];
            }
            else if ($branchList) 
            {
                $message="Reference Detail";
                $status=true;
                $result=$branchList;
            }
            else 
            {
                $message="No data found";
                $status=false;
                $result=array();
            }
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    
    function addTag_post(){
        
        $tagName=$this->input->post('tagName');
        $tagDescription=$this->input->post('tagDescription');
        
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        
        $validations[]=["field"=>"tagName","label"=>'tagName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]*$/]","errors"=>array('required' => 'Tag Name should not be blank','regex_match' => 'Invalid Tag Name')];
        
        $validations[]=["field"=>"tagDescription","label"=> 'tagDescription',"rules"=>"required","errors"=>array('required' => 'Tag Description should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();
            
            $getType=$this->References_model->getReferenceType('Tag');
            
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
                ];
            
            if($getType['status']===true){
                
                foreach($getType['data'] as $k=>$v){
                    $reftype=$v->reference_type_id;
                }
                $data = array(
                    'reference_type_id' => $reftype ,
                    'reference_type_val' => $tagName ,
                    'reference_type_val_parent' => NULL,
                    'reference_type_is_active' => 1,
                    'reference_type_val_description' => $tagDescription  
                );
                
                $createRoleRes=$this->References_model->AddReferenceValue($data);
                $final_response['response']=[
                'status' => $createRoleRes['status'],
                'message' => $createRoleRes['message'],
                'data' => $createRoleRes['data'],
                ];
                if($createRoleRes['status']===true){
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
        
    }
    
    function editTag_post(){
        
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        
        $tagId=$this->post('tagId');
        $tagName=$this->post('tagName');
        $tagDescription=$this->post('tagDescription');
        
        $validations[]=["field"=>"tagId","label"=>"tagId","rules"=>"required|integer","errors"=>array(
        'required' => 'Tag ID should not be blank','integer' => 'Tag should be integer value only'
        )];
        
        $validations[]=["field"=>"tagName","label"=>'tagName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]*$/]","errors"=>array('required' => 'Tag Name should not be blank','regex_match' => 'Invalid Tag Name')];
        
        $validations[]=["field"=>"tagDescription","label"=> 'tagDescription',"rules"=>"required","errors"=>array('required' => 'Tag Description should not be blank')];
        
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $resp_checkTag=$this->Tag_model->checkTag($tagId);
            if($resp_checkTag['status']===true){
                $data=array();
                
                $getType=$this->References_model->getReferenceType('Tag');

                $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
                ];

                if($getType['status']===true){

                    foreach($getType['data'] as $k=>$v){
                        $reftype=$v->reference_type_id;
                    }

                    $data = array(
                    'reference_type_id' => $reftype ,
                    'reference_type_val' => $tagName ,
                    'reference_type_val_parent' => NULL,
                    'reference_type_is_active' => 1,
                    'reference_type_val_description' => $tagDescription,
                    'reference_type_val_id' => $tagId
                    );


                    $createRoleRes=$this->References_model->UpdateReferenceValue($data);
                    $final_response['response']=[
                    'status' => $createRoleRes['status'],
                    'message' => $createRoleRes['message'],
                    'data' => $createRoleRes['data'],
                    ];
                    if($createRoleRes['status']===true){
                        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                    }
                    else{
                        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                    }
                }
            }
            else{
                $final_response['response']=[
                    'status' => $resp_checkTag['status'],
                    'message' => $resp_checkTag['message'],
                    'data' => $resp_checkTag['data'],
                    ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    function deleteTag_delete(){
        
        $tagId=$this->get('tagId');
        $this->form_validation->set_data(array("tagId" => $this->get('tagId')));

        $validations[]=["field"=>"tagId","label"=>'tagId',"rules"=>"required|integer","errors"=>array(
        'required' => 'Tag ID should not be blank','integer' => 'Tag ID should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $resp_checkTag=$this->Tag_model->checkTag($tagId);
            if($resp_checkTag['status']===true){
                
                $data['reference_type_val_id']=$tagId;
                $res_delete=$this->References_model->RemoveReferenceValue($data);
                $final_response['response']=[
                'status' => $res_delete['status'],
                'message' => $res_delete['message'],
                'data' => $res_delete['data'],
                ];
                if($res_delete['status']===true){
                    //sucess
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    //fail
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
            else{
                $final_response['response']=[
                    'status' => $resp_checkTag['status'],
                    'message' => $resp_checkTag['message'],
                    'data' => $resp_checkTag['data'],
                    ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    
    /**
     * Function for checking number(REGEX)
     * 
     * @args 
     *  "string"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function addCompany_post(){

        $companyName=$this->input->post('companyName');
        $companyDescription=$this->input->post('companyDescription');

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"companyName","label"=>'companyName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]*$/]","errors"=>array('required' => 'Company Name should not be blank','regex_match' => 'Invalid Company Name')];

        $validations[]=["field"=>"companyDescription","label"=> 'companyDescription',"rules"=>"required","errors"=>array('required' => 'Company Description should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();

            $getType=$this->References_model->getReferenceType('Company');

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

            if($getType['status']===true){

                foreach($getType['data'] as $k=>$v){
                    $reftype=$v->reference_type_id;
                }
                $data = array(
                    'reference_type_id' => $reftype ,
                    'reference_type_val' => $companyName ,
                    'reference_type_val_parent' => NULL,
                    'reference_type_is_active' => 1,
                    'reference_type_val_description' => $companyDescription
                );

                $createRoleRes=$this->References_model->AddReferenceValue($data);
                $final_response['response']=[
                    'status' => $createRoleRes['status'],
                    'message' => $createRoleRes['message'],
                    'data' => array('companyName'=>$createRoleRes['message']),
                ];
                if($createRoleRes['status']===true){
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }

        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function addInstitution_post(){

        $institutionName=$this->input->post('institutionName');
        $institutionDescription=$this->input->post('institutionDescription');

        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"institutionName","label"=>'institutionName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]*$/]","errors"=>array('required' => 'Institution Name should not be blank','regex_match' => 'Invalid Institution Name')];

        $validations[]=["field"=>"institutionDescription","label"=> 'institutionDescription',"rules"=>"required","errors"=>array('required' => 'Institution Description should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();

            $getType=$this->References_model->getReferenceType('Institution');

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

            if($getType['status']===true){

                foreach($getType['data'] as $k=>$v){
                    $reftype=$v->reference_type_id;
                }
                $data = array(
                    'reference_type_id' => $reftype ,
                    'reference_type_val' => $institutionName ,
                    'reference_type_val_parent' => NULL,
                    'reference_type_is_active' => 1,
                    'reference_type_val_description' => $institutionDescription
                );

                $createRoleRes=$this->References_model->AddReferenceValue($data);
                $final_response['response']=[
                    'status' => $createRoleRes['status'],
                    'message' => $createRoleRes['message'],
                    'data' => array('institutionName'=>$createRoleRes['message']),
                ];
                if($createRoleRes['status']===true){
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }

        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
}

