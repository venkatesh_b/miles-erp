<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Branch Visit
 * @category        Controller
 * @author          Abhilash
 */
class TelecallerReport extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("BranchReport_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    
    public function getAllConfigurationsByUserId_get() {
        $message='Branch Details';
        $status=true;
        $this->load->model("Branch_model");
        $this->load->model("Course_model");
        $this->load->model("Lead_model");
        $branch = [];
        $course = [];
        $branchList = $this->Branch_model->getBranchListByUserId($_SERVER['HTTP_USER']);
        if(count($branchList) >0 )
        {
            foreach ($branchList as $key => $value)
            {
                $branch[$value['id']] = (object)$value;
                $temp = $this->Course_model->getAllBranchCoursesList(array('branchId'=>$value['id']))['data'];
                if(count($temp) > 0)
                {
                    foreach ($temp as $k => $v)
                    {
                        $course[$v->courseId] = $v;
                    }
                }
                else
                {
                    $course = [];
                }
            }
        }
        else
        {
            $branch = [];
        }
        
        $data=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>array(
                'branch' => array_values($branch),
                'course' => array_values($course)
            )
        );
        $this->set_response($data);
    }
    
    public function getTelecallerReport_get() {
        $message='Telecaller Details';
        $status=true;
        
        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";

        if(strlen(trim($this->get('branch'))) <=0 || strlen(trim($this->get('branch'))) >1){
            $status=false;
            $message = 'Select any one branch';
            $res = [];
        }
        else
        {
            $res = $this->BranchReport_model->getTelecallerReport($branch,$from_date,$to_date);
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$res);
        $this->set_response($data);
    }
    function getkey($pos){

        return chr(65+$pos);

    }
    public function getTelecallerReportExport_get()
    {
        $message = 'Telecaller Details';
        $status = true;
        $this->load->library('excel');
        $branch = strlen(trim($this->get('branch'))) > 0 ? "'" . trim($this->get('branch')) . "'" : 'NULL';
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";

        if (strlen(trim($this->get('branch'))) <= 0 && strlen(trim($this->get('course'))) <= 0 && strlen(trim($this->get('stage'))) <= 0) {
            $status = false;
            $message = 'Select any filter';
            $res = [];
        } else {
            $res = $this->BranchReport_model->getTelecallerReport($branch,$from_date,$to_date);
        }
        if($res===false){
            $data = array("status" => false, "message" => 'No Records Found', "data" => array());
        }
        else {
            $header_main = '';
            foreach ($res as $k => $v) {
                $headers = array();
                $datarows = array();
                foreach ($v as $k1 => $v1)
                {
                        $headers[] = ucfirst(str_replace('_',' ',$k1));
                        $datarows[] = $v1;
                }
                $header_main = implode('||', $headers);
                $data_main[] = implode('||', $datarows);
            }


            $header = $header_main;
            $headervals = explode('||', $header);

            $excelRowstartsfrom=3;
            $excelColumnstartsFrom=2;
            $excelstartsfrom=$excelRowstartsfrom;
            $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'TELECALLER REPORT');

            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );

            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Branch");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('branchText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Report on");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
            $excelstartsfrom=$excelstartsfrom+1;
            $head_starts_from = $excelstartsfrom+1;
            for ($k = 0; $k < count($headervals); $k++) {
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from, $headervals[$k]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );

            }
            $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
            $rowsStratsFrom = $head_starts_from + 1;
            $c = $rowsStratsFrom;
            for ($k = 0; $k < count($data_main); $k++)
            {
                $datavals = explode('||', $data_main[$k]);
                for ($j = 0; $j < count($datavals); $j++)
                {
                    if($j!=14)
                    {
                        if($datavals[$j] == null || $datavals[$j] == '')
                            $datavals[$j] = 0;
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, $datavals[$j]);
                    }
                    else
                    {
                        $this->excel->setActiveSheetIndex(0)
                          ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c,'=SUM('.$this->getkey($excelColumnstartsFrom+2).$c.':'.$this->getkey($excelColumnstartsFrom+13).$c.')');
                    }
                }
                $c++;
            }

            $footer_starts_from=$c;

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+1) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from,'Total');

            for($foot=2;$foot<6;$foot++){
                $this->excel->getActiveSheet()
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$foot) . $footer_starts_from,'=SUM('.$this->getkey($excelColumnstartsFrom+$foot).$rowsStratsFrom.':'.$this->getkey($excelColumnstartsFrom+$foot).($footer_starts_from-1).')');
            }

            $footersel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $footer_starts_from;
            $this->excel->getActiveSheet()->getStyle($footersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($footersel)->getFill()->getStartColor()->setRGB('CCCCCC');



            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('TELECALLER REPORT');
//            $this->excel->getActiveSheet()->getStyle($this->getkey($k) . $head_starts_from)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $filename = "Telecallerreport_".date('Ymd_His') . '.xls'; //save our workbook as this file name
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $file_path = './uploads/' . $filename;
            $objWriter->save($file_path);
            $file_path = base_url().'uploads/' . $filename;
            $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
        }
        $this->set_response($data);
    }

    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}