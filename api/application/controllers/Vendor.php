<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-16
 * Time: 10:50 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Vendor extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("Vendor_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE) {
            if (count($this->form_validation->error_array()) > 0) {
                return $this->form_validation->error_array();
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    function vendors_get()
    {
        $this->response($this->Vendor_model->vendorDataTables(),REST_Controller::HTTP_OK);
    }
    function addVendor_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $validations[]=["field"=>"vendorName","label"=>'vendorName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]+$/]","errors"=>array('required' => 'Vendor Name should not be blank','regex_match' => 'Invalid Name')];

        $validations[]=["field"=>"phoneNumber","label"=>'phoneNumber',"rules"=>"required|regex_match[/^[0-9() +-]{8,15}$/]","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number')];

        $validations[]=["field"=>"vendorEmail","label"=> 'vendorEmail',"rules"=>"required|valid_email","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Invalid Email')];

        /*$validations[]=["field"=>"vendorAddress","label"=>'vendorAddress',"rules"=>"required","errors"=>array('required' => 'Address should not be blank')];*/

        $validations[]=["field"=>"contactByName","label"=>'contactByName',"rules"=>"required","errors"=>array('required' => 'Contact by Name should not be blank')];

        $validations[]=["field"=>"contactByPhone","label"=>'contactByPhone',"rules"=>"required|regex_match[/^(\+\d{1,3}[- ]?)?\d{10}$/]","errors"=>array('required' => 'Contact by Phone No. should not be blank','regex_match' => 'Invalid phone number')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $data = array(
                'name' =>$this->post('vendorName'),
                'phone' => $this->post('phoneNumber'),
                'email' => $this->post('vendorEmail'),
                'address' => $this->post('vendorAddress'),
                'contactby_name' => $this->post('contactByName'),
                'contactby_phone' => $this->post('contactByPhone'),
                'created_by' => $this->post('userID')
            );
            $res=$this->Vendor_model->addVendor($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getVendorDetailsById_get()
    {
        $vendorId=decode($this->get('vendorId'));
        $this->form_validation->set_data(array("vendorId"=>$vendorId));

        $validations[]=["field"=>"vendorId","label"=>'vendorId',"rules"=>"required","errors"=>array('required' => 'Vendor id should not be blank','regex_match' => 'Vendor id should be numeric only')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $res=$this->Vendor_model->checkVendorExist($vendorId);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
            if($res['status']===true)
            {
                $res=$this->Vendor_model->getVendorDetailsById($vendorId);
                $final_response['response']=[
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function updateVendor_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $vendorId=decode($this->post('vendorId'));
        $this->form_validation->set_data($this->post());
        $validations[]=["field"=>"vendorId","label"=>'vendorId',"rules"=>"required","errors"=>array('required' => 'Vendor ID should not be blank','regex_match' => 'Vendor ID should be numeric only')];

        $validations[]=["field"=>"vendorName","label"=>'vendorName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]+$/]","errors"=>array('required' => 'Vendor Name should not be blank','regex_match' => 'Invalid Name')];

        $validations[]=["field"=>"phoneNumber","label"=>'phoneNumber',"rules"=>"required|regex_match[/^[0-9() +-]{8,15}$/]","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number')];

        $validations[]=["field"=>"vendorEmail","label"=> 'vendorEmail',"rules"=>"required|valid_email","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Invalid Email')];

        /*$validations[]=["field"=>"vendorAddress","label"=>'vendorAddress',"rules"=>"required","errors"=>array('required' => 'Address should not be blank')];*/

        $validations[]=["field"=>"contactByName","label"=>'contactByName',"rules"=>"required","errors"=>array('required' => 'Contact by Name should not be blank')];

        $validations[]=["field"=>"contactByPhone","label"=>'contactByPhone',"rules"=>"required|regex_match[/^(\+\d{1,3}[- ]?)?\d{10}$/]","errors"=>array('required' => 'Contact by Phone No. should not be blank','regex_match' => 'Invalid phone number')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $data = array(
                'name' =>$this->post('vendorName'),
                'phone' => $this->post('phoneNumber'),
                'email' => $this->post('vendorEmail'),
                'address' => $this->post('vendorAddress'),
                'contactby_name' => $this->post('contactByName'),
                'contactby_phone' => $this->post('contactByPhone'),
                'updated_by' => $this->post('userID'),
                'updated_on' => date('Y-m-d H:i:s'),
                'vendorId' => $vendorId
            );
            $res=$this->Vendor_model->updateVendor($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function deleteVendor_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $vendorId=decode($this->post('vendorId'));
        $this->form_validation->set_data($this->post());
        $validations[]=["field"=>"vendorId","label"=>'vendorId',"rules"=>"required","errors"=>array('required' => 'Vendor ID should not be blank','regex_match' => 'Vendor ID should be numeric only')];
        $res=$this->Vendor_model->deleteVendor($vendorId);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}