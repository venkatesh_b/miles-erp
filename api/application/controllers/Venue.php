<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/*
 * Created By: Abhilash 
 * Purpose: Services for Courses.
 * 
 * Updated on - 6 Feb 2016
 * 
 * updated by: Parameshwar
 * updated on: 17 Feb 2016:
 * Purpose: Services for creation of courses, edit and delete of a course.
 */

class Venue extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("Venue_model");
        $this->load->model("Branch_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->helper('encryption');
        $this->load->library('SSP');
    }
    
    function getVenueDatatables_get(){
        $this->response($this->Venue_model->getVenueDatatables(), REST_Controller::HTTP_OK);
    }
    
    function getAllVenue_get() {
        $status = true;
        $message = "";
        $result = "";

        $list = $this->Venue_model->getAllVenue();
        $this->load->model("References_model");
        $cityList = $this->References_model->getReferenceValuesList(3);
//        print_r($cityList);die;
        $country = [];
        foreach ($list as $k => $v) {
//            print_r($v->city_id);die;
            foreach ($cityList['data'] as $k1 => $v1) {
//                print_r($v1);die;
                if($v1->Id1 == $v->city_id)
                {
//                    print_r($v1);die;
                    $v->state_id =  $v1->Id2;
                    $v->state =  $v1->State;
                    $v->country_id =  $v1->Id3;
                    $v->country =  $v1->Country;
                }
            }
        }
        if ($list == 'nodata') {
            $message = 'No data found';
            $status = false;
        } else if (isset($list['error']) && $list['error'] == 'dberror') {
            $message = 'Database error';
            $status = false;
            $result = $list['msg'];
        } else if ($list) {
            $message = "Venue data";
            $status = true;
            $result = $list;
        } else {
            /* No data found */
            $message = 'No data found';
            $status = false;
        }

        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function getVenueById_get() {
        $id = $this->get('venue_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Venue_model->getVenueById($id);
            if ($list == 'badrequest') {
                $message = 'No data found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $cityDetails = $this->Branch_model->getStateDetails($list[0]->fk_city_id);
                $message = "Venue data";
                $status = true;
                $result = array('details' => $list[0], 'city' => $cityDetails);
            } else {
                /* No data found */
                $message = 'No data found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

    function saveVenue_post() {
//        print_r($this->input->post());die;
        $status = true;
        $message = "";
        $result = "";
        $config = array(
            array(
                'field' => 'venueName',
                'label' => 'Venue Name',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'city',
                'label' => 'city',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            )
        );

        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE)
        {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        }
        else
        {
            $data = $this->input->post();
            $isAdded = $this->Venue_model->saveVenue($data);
            if (isset($isAdded['error']) && $isAdded['error'] == 'dberror')
            {
                $message = 'Database error';
                $status = false;
                $results = $isAdded['msg'];
            }
            else if ($isAdded == 1)
            {
                $result = [];
                $message = "Saved successfully";
                $status = true;
            }
            else if ($isAdded == 'nocity')
            {
                $result = array('city' => 'City is invalid');
                $message = "City is invalid";
                $status = false;
            }
            else if ($isAdded == 'duplicate')
            {
                $result = array('venueName' => 'Venue name already exists');
                $message = "Venue name already exists";
                $status = false;
            }
            else
            {
                $result = [];
                $message = "Something went wrong";
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

    function editVenue_post() {
        $id = $this->get('venue_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            
            $config = array(
                array(
                    'field' => 'venueName',
                    'label' => 'Venue Name',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'city',
                    'label' => 'city',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                )
            );

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message = "Validation Error";
                $status = false;
                $result = $this->form_validation->error_array();
            } else {
                $data = $this->input->post();
                $isEdited = $this->Venue_model->editVenue($id, $data);
                if (isset($isEdited['error']) && $isEdited['error'] == 'dberror')
                {
                    $message = 'Database error';
                    $status = false;
                    $results = $isEdited['msg'];
                }
                else if ($isEdited == 1)
                {
                    $result = [];
                    $message = "Saved successfully";
                    $status = true;
                }
                else if ($isEdited === 'nocity')
                {
                    $result = array('city' => 'City is invalid');
                    $message = "City is invalid";
                    $status = false;
                }
                else if ($isEdited === 'nodata')
                {
                    $result = [];
                    $message = "Id is invalid";
                    $status = false;
                }
                else if ($isEdited == 'duplicate')
                {
                    $result = array('venueName' => 'Venue name already exists');
                    $message = "Venue name already exists";
                    $status = false;
                }
                else
                {
                    $result = [];
                    $message = "Something went wrong";
                    $status = false;
                }
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function deleteVenue_post() {
        $id = $this->get('venue_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $isDeleted = $this->Venue_model->deleteVenue($id);
            if (isset($isDeleted['error']) && $isDeleted['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $results = $isDeleted['msg'];
            } else if ($isDeleted === 'nodata') {
                $message = "No venue found";
                $status = false;
            } /*else if ($isDeleted === 'alreadyused') {
                $result = [];
                $message = "Venue is already been answered";
                $status = false;
            }*/ else if ($isDeleted === 'active') {
                $message = "Venue Active";
                $status = true;
            } else if ($isDeleted === 'deactivated') {
                $message = "Venue Inactive";
                $status = true;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

}
