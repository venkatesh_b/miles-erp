<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 1/8/16
 * Time: 12:06 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class VoucherReconcelation extends REST_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
        $this->load->model("VoucherReconcelation_model");
    }
    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);
        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0)
            {
                return $this->form_validation->error_array();
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }

    function getConfigList_get()
    {
        $configList = $this->VoucherReconcelation_model->getVoucherReconcelationConfig($_SERVER['HTTP_USER']);
        $this->set_response($configList);
    }
    function getReconcelationList_get()
    {
        $branch_id=$this->input->get('branchId');
        $company_id=$this->input->get('companyId');
        $payment_mode=$this->input->get('PaymentMode');
        $chosen_date = DateTime::createFromFormat('d M, Y', $this->get('selectedDate'));
        $chosen_date = $chosen_date->format("Y-m-d");
        $revenuePostingData=$this->VoucherReconcelation_model->getVoucherReconcelationByDate($branch_id,$company_id,$payment_mode,$chosen_date);
        //$revenuePostingData=$this->VoucherReconcelation_model->getVoucherReconcelationList($branch_id,$company_id,$payment_mode,$chosen_date);
        $this->response($revenuePostingData);
    }
    function ProcessVoucherReconcelation_post()
    {
        $branch_id=$this->input->post('branchId');
        $company_id=$this->input->post('feeCompany');
        $payment_mode=$this->input->post('PaymentMode');
        $chosen_date = DateTime::createFromFormat('d M, Y', $this->post('selectedDate'));
        $chosen_date = $chosen_date->format("Y-m-d");
        $reconcelation_data=$this->input->post('reconcelationData');
        $voucherReconcelation=$this->VoucherReconcelation_model->ProcessVoucherReconcelation($branch_id,$company_id,$payment_mode,$chosen_date,$reconcelation_data);
        $this->response($voucherReconcelation);
    }
}