<?php
/**
 * Created By :  V Parameshwar Dt:23-02-2016
 * Purpose: Helper class for encrypt and decrypt the given string values
 * Encryption: 1- encode, 2-replace backslashes with pipe symbol, 3- url enncoding.
 * Decryption:1-url decoding, 2- replace pipe with backslashes, 3- decode.
 */
if ( ! function_exists('encode'))
{
    function encode($string='')
    {
    	$CI =& get_instance();
        $CI->load->library('encrypt');
        return urlencode(str_replace('/', '|', $CI->encrypt->encode($string)));
    }
}
if ( ! function_exists('decode'))
{
    function decode($string='')
    {
    	$CI =& get_instance();
        $CI->load->library('encrypt');
        return $CI->encrypt->decode(str_replace('|', '/',urldecode($string)));
    }
}

if ( ! function_exists('explodeX')) {
    // Helper function to explode a string based on given array of delimiters.
    // Ex: explodeX(Array(",",";","|"),"1@gmai.com,2@gmail.com|3@gmail.com;4@gmail.com")
    function explodeX($delimiters, $string)
    {
        $string=trim($string);
        $return_array = Array($string); // The array to return
        $d_count = 0;
        while (isset($delimiters[$d_count])) // Loop to loop through all delimiters
        {
            if($delimiters[$d_count]!='') {
                $new_return_array = Array();
                foreach ($return_array as $el_to_split) // Explode all returned elements by the next delimiter
                {
                    $put_in_new_return_array = explode($delimiters[$d_count], $el_to_split);
                    foreach ($put_in_new_return_array as $substr) // Put all the exploded elements in array to return
                    {
                        $new_return_array[] = trim($substr);
                    }
                }
                $return_array = $new_return_array; // Replace the previous return array by the next version
            }
            $d_count++;
        }
        return $return_array; // Return the exploded elements
    }
}

