<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccessLayer {
    var $CI;
    function __construct() {
        // Construct the parent class
        $this->CI =& get_instance();
        $this->CI->load->database();
        $this->CI->load->library('form_validation');
        $this->CI->load->model("AccessLayer_model");

    }
    function checkAccess() {

        $params['context']=$context=trim((isset($_SERVER['HTTP_CONTEXT'])? $_SERVER['HTTP_CONTEXT'] : ''));
        $params['action']=$action=trim((isset($_SERVER['HTTP_ACTION'])? $_SERVER['HTTP_ACTION'] : ''));
        $params['serviceUrl']=$serviceUrl=trim((isset($_SERVER['HTTP_SERVICEURL'])? $_SERVER['HTTP_SERVICEURL'] : ''));
        $params['pageUrl']=$pageUrl=trim((isset($_SERVER['HTTP_PAGEURL'])? $_SERVER['HTTP_PAGEURL'] : ''));
        $params['userId']=$userId=trim((isset($_SERVER['HTTP_USER'])? $_SERVER['HTTP_USER'] : ''));
        if($context!='default'){
            $this->CI->form_validation->set_data(array('context' => $context,'action' => $action,'serviceUrl' => $serviceUrl,'pageUrl' => $pageUrl,'userId' => $userId));



            $validations[]=["field"=>"context","label"=>'context',"rules"=>"required","errors"=>array(
                'required' => 'Request Header - Context should not be blank')];
            $validations[]=["field"=>"action","label"=>'action',"rules"=>"required","errors"=>array(
                'required' => 'Request Header - Action should not be blank')];
            $validations[]=["field"=>"serviceUrl","label"=>'serviceUrl',"rules"=>"required","errors"=>array(
                'required' => 'Request Header - Service Url should not be blank')];
            $validations[]=["field"=>"pageUrl","label"=>'pageUrl',"rules"=>"required","errors"=>array(
                'required' => 'Request Header - Page Url should not be blank')];
            $validations[]=["field"=>"userId","label"=>'userId',"rules"=>"required","errors"=>array(
                'required' => 'Request Header - User ID should not be blank')];
            $this->CI->form_validation->set_rules($validations);


            $status=false;
            $message="Access Denied";
            $results=array("message"=>"Access Denied");
            if ($this->CI->form_validation->run() == FALSE)
            {
                $message="Validation Error";
                $status=false;
                $results=$this->CI->form_validation->error_array();
            }
            else
            {

                $resp=$this->CI->AccessLayer_model->checkAction($params);
                if($resp['status']===true){

                    foreach($resp['data'] as $k=>$v){
                        if($v->_result==0){
                            $status=true;
                        }
                        else{
                            $status=false;
                            $message="Access Denied";
                            $results=array("message"=>$message);
                        }

                    }
                }
                else{
                    $status=false;
                    $message="Access Denied";
                    $results=array("message"=>$message);
                }

            }

            if($status===false){
                $response=json_encode(array("status"=>$status,"message"=>$message,"data"=>$results));
                $this->CI->output->set_header('HTTP/1.0 200 OK');
                $this->CI->output->set_header('HTTP/1.1 200 OK');
                $this->CI->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
                $this->CI->output->set_header('Cache-Control: post-check=0, pre-check=0');
                $this->CI->output->set_header('Pragma: no-cache');
                $this->CI->output->set_content_type('application/json');
                $this->CI->output->set_output($response);
                $this->CI->output->_display();
                exit;
            }
            //return true;
        }
    }
}
