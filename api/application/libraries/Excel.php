<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 14-04-2016
 * Time: 12:48 PM
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";
class Excel extends PHPExcel {
    public function __construct() {
        parent::__construct();
    }
}
