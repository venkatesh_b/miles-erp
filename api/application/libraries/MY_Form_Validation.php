<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Custom Additional validations.
 *
 */
class MY_Form_Validation extends CI_Form_validation {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Function for checking number(REGEX)
     * 
     * @args 
     *  "string"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Function for checking valid string
     * 
     * @args 
     *  "str"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function name_check($str) {
//        die('name');
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str)) {
            $this->set_message('name_check', 'The %s field is not valid!, use only Alphabets');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Validate URL format
     *
     * @access  public
     * @param   string
     * @return  Boolean
     */
    function valid_url_format($str) {
        $pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
        if (!preg_match($pattern, $str)) {
            $this->set_message('valid_url_format', 'The URL you entered is not correctly formatted.');
            return FALSE;
        }

        return TRUE;
    }
    
    /**
     * Validates that a URL is accessible. Also takes ports into consideration. 
     * Note: If you see "php_network_getaddresses: getaddrinfo failed: nodename nor servname provided, or not known" 
     *          then you are having DNS resolution issues and need to fix Apache
     *
     * @access  public
     * @param   string
     * @return  Boolean
     */
    function url_exists($url) {
        $url_data = parse_url($url); // scheme, host, port, path, query
        if (!fsockopen($url_data['host'], isset($url_data['port']) ? $url_data['port'] : 80)) {
            $this->set_message('url_exists', 'The URL you entered is not accessible.');
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Validates that a Date is accessible. Also takes ports into consideration. 
     *
     * @access  public
     * @param   string
     * @return  Boolean
     */
    function checkDateFormat($date) {
//        die('ads');
        if (preg_match("/[0-31]{2}\/[0-12]{2}\/[0-9]{4}/", $date)) {
            if (checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))
                return true;
            else
                $this->set_message('checkDateFormat', '(DD/MM/YYYY) The date you entered is not correctly formatted.');
            return FALSE;
        } else {
            $this->set_message('checkDateFormat', '(DD/MM/YYYY) The date you entered is not correctly formatted.');
            return FALSE;
        }
    }

    /**
     * Validates that a Date is accessible. Also takes ports into consideration. 
     *
     * @access  public
     * @param   string
     * @return  Boolean
     */
    function checkPhone($phone) {
        /* Valid
         * (000) 99988 88123
         *  (+12) 99988 88123
         *  (+12) 999 8812
         */
        if (preg_match("/^\(?[+0-9]{2,3}\)?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/", $phone)) {
            return true;
        } else {
            $this->set_message('checkPhone', '(+XX) XXXXX XXXXX OR (+XX) XXX XXXX The Phone Number you entered is not correctly formatted.');
            return FALSE;
        }
    }

}

// END Form Validation Class