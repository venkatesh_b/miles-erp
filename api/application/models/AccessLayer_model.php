<?php

/* 
 * Created By: V Parameshwar. Date: 03-02-2016
 * Purpose: Services for Tags.
 * 
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AccessLayer_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
    }
    
    public function checkAction($params){
        
        $context=$params['context'];
        $action=$params['action'];
        $serviceUrl=$params['serviceUrl'];
        $pageUrl=$params['pageUrl'];
        $userId=$params['userId'];
        $this->db->select('u.user_id as userID, u.login_id ,r.fk_role_id,rf.`value` as `userType`,rf1.`value` as `userRole`');
        $this->db->from('user u');
        $this->db->join('user_xref_role r', 'r.fk_user_id = u.user_id');
        $this->db->join('reference_type_value rf1', 'rf1.reference_type_value_id = r.fk_role_id');
        $this->db->join('reference_type_value rf', 'rf.reference_type_value_id = u.fk_user_type_id');
        $this->db->where('u.user_id',$userId);
        $this->db->where('u.is_active',1);
        $query_uservalid = $this->db->get();


        if($query_uservalid) {
            if ($query_uservalid->num_rows() > 0) {
                $query = $this->db->query("call Sp_Validate_Request('$context','$action','$serviceUrl','$pageUrl','$userId')");
                if($query){
                    if ($query->num_rows() > 0){
                        $res=$query->result();
                        $this->db->close();
                        $this->load->database();
                        $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
                    }
                    else{
                        $db_response=array("status"=>false,"message"=>'No Data Found',"data"=>array());
                    }
                }
                else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
            }
            else{
                $db_response=array("status"=>false,"message"=>"Invalid credentials","data"=>array("message"=>"Invalid credentials"));
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }

        return $db_response;
     }
}
