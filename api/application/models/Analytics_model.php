<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 7/9/16
 * Time: 6:13 PM
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Analytics_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    public function widgetsRelatedData($branchIds,$fromDate,$toDate)
    {
        $finalData=array();
        if($branchIds !=0)
        {
            $branchIds = explode(',',$branchIds);
        }
        $this->db->select('COUNT(branch_id) as branches');
        $this->db->from('branch');
        $this->db->where('is_active',1);
        if($branchIds!=0)
            $this->db->where_in('branch_id',$branchIds);
        $branches_query = $this->db->get();

        $this->db->select('COUNT(DISTINCT cs.course_id) as courses');
        $this->db->from('course cs');
        $this->db->join("branch_xref_course bxc","bxc.fk_course_id=cs.course_id");
        $this->db->where('cs.is_active',1);
        $this->db->where('bxc.is_active',1);
        if($branchIds != 0)
            $this->db->where_in('bxc.fk_branch_id',$branchIds);
        $courses_query = $this->db->get();

        $this->db->select('IFNULL(SUM(fc.amount_paid)-SUM(fc.reconcel_amount),0) as amount_collected');
        $this->db->from('fee_collection fc');
        if($branchIds != 0)
        {
            $this->db->join("branch_xref_lead bxl","fc.fk_branch_xref_lead_id=bxl.branch_xref_lead_id");
            $this->db->where_in('bxl.branch_id',$branchIds);
            $this->db->where('DATE(receipt_date) >=',date('Y-m-d', strtotime($fromDate)));
            $this->db->where('DATE(receipt_date) <=',date('Y-m-d', strtotime($toDate)));
        }
        $this->db->where('payment_status','Success');
        $feecollected_query = $this->db->get();

        $this->db->select('IFNULL(sum(cfi.amount_payable-cfi.concession-cfi.right_of_amount-cfi.amount_paid),0) as due_amount');
        $this->db->from('candidate_fee cf');
        $this->db->join('candidate_fee_item cfi','cf.candidate_fee_id=cfi.fk_candidate_fee_id','left');
        if($branchIds != 0)
        {
            $this->db->join("branch_xref_lead bxl","fc.fk_branch_xref_lead_id=bxl.branch_xref_lead_id");
            $this->db->where_in('bxl.branch_id',$branchIds);
            $this->db->where('DATE(IFNULL(cfi.updated_on,cfi.created_on)) >=',date('Y-m-d', strtotime($fromDate)));
            $this->db->where('DATE(IFNULL(cfi.updated_on,cfi.created_on)) <=',date('Y-m-d', strtotime($toDate)));
        }
        $feedue_query = $this->db->get();


        if(!$branches_query || !$courses_query || !$feecollected_query || !$feedue_query)
        {
            $error = $this->db->error();
            if($error['message'] =='')
            {
                $error['message']='Something went wrong. Please try again.';
            }
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else
        {
            $branches_data = $branches_query->row();
            $finalData['branches'] = sprintf('%02d', $branches_data->branches);
            $courses_data = $courses_query->row();
            $finalData['courses'] = sprintf('%02d', $courses_data->courses);
            $feecollected_data = $feecollected_query->row();
            $finalData['collection'] = $feecollected_data->amount_collected;
            $feedue_data = $feedue_query->row();
            $finalData['dues'] = $feedue_data->due_amount;
            $db_response=array("status"=>true,"message"=>'success',"data"=>$finalData);
        }
        return $db_response;
    }

    public function LeadStudentYearlyReport()
    {
        $this->db->select('YEAR(bt.acedemic_startdate) as report_year,
                           IFNULL(COUNT(bxl.branch_xref_lead_id),0) as students,
                           IFNULL(SUM(bt.target),0) as student_target,
                           IFNULL(SUM(bt.visitor_target),0) as visitor_target');
        $this->db->from('batch bt');
        $this->db->join('branch_xref_lead bxl','bt.batch_id=bxl.fk_batch_id','left');
        $this->db->join('lead_stage ls','bxl.status=ls.lead_stage_id AND ls.lead_stage="M7"','left');
        $this->db->group_by('YEAR(bt.acedemic_startdate)');

        $leadsstudents_query = $this->db->get();
        if($leadsstudents_query)
        {
            $leadsstudents_data = $leadsstudents_query->result();

            $this->db->select('YEAR(IFNULL(bxl.updated_on,bxl.created_on)) as report_year,
                           COUNT(bxl.branch_xref_lead_id) as visitors');
            $this->db->from('branch_xref_lead bxl');
            $this->db->join('lead_stage ls','bxl.status=ls.lead_stage_id','left');
            $this->db->where_in('ls.lead_stage',['M4','M5','M6']);
            $this->db->group_by('YEAR(IFNULL(bxl.updated_on,bxl.created_on))');
            $visitors_query = $this->db->get();
            if($visitors_query)
            {
                $visitors_data = $visitors_query->result();
                for($i=0;$i<count($leadsstudents_data);$i++)
                {
                    if($leadsstudents_data[$i]->report_year == $visitors_data[$i]->report_year)
                    {
                        if(isset($visitors_data[$i]->visitors))
                            $leadsstudents_data[$i]->visitors=$visitors_data[$i]->visitors;
                        else
                            $leadsstudents_data[$i]->visitors=0;
                    }
                }
            }
            $db_response=array("status"=>true,"message"=>'yearly report',"data"=>$leadsstudents_data);
        }
        else
        {
            $error = $this->db->error();
            if($error['message'] =='')
            {
                $error['message']='Something went wrong. Please try again.';
            }
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function LeadStudentCourseBranchWiseReport()
    {
        /*LEFT JOIN branch_xref_course bxc ON br.branch_id=bxc.fk_branch_id
LEFT JOIN batch bt ON bxc.fk_branch_id=bt.fk_branch_id AND bxc.fk_course_id=bt.fk_course_id*/
        $this->db->select('br.branch_id,br.`name` as branch_name,c.`name` as course_name,IFNULL(SUM(bt.target),0) as target_enrollments,IFNULL(SUM(bt.visitor_target),0) as target_visitors,
                            (SELECT COUNT(bxl_1.branch_xref_lead_id) FROM branch_xref_lead bxl_1 LEFT JOIN lead_stage ls ON bxl_1.`status`=ls.lead_stage_id WHERE br.branch_id=bxl_1.branch_id AND bxl_1.fk_course_id=c.course_id AND ls.lead_stage IN ("M7")) as enrollments,
                            (SELECT COUNT(bxl_2.branch_xref_lead_id) FROM branch_xref_lead bxl_2 LEFT JOIN lead_stage ls ON bxl_2.`status`=ls.lead_stage_id WHERE br.branch_id=bxl_2.branch_id AND bxl_2.fk_course_id=c.course_id AND ls.lead_stage IN ("M4","M5","M6")) as visitors');
        $this->db->from('branch br');
        $this->db->join('branch_xref_course bxc','br.branch_id=bxc.fk_branch_id','left');
        $this->db->join('batch bt','bxc.fk_branch_id=bt.fk_branch_id AND bxc.fk_course_id=bt.fk_course_id','left');
        $this->db->join('branch_xref_lead bxl','bxc.fk_branch_id=bxl.branch_id','left');
        $this->db->join('course c','bxc.fk_course_id=c.course_id','left');
        $this->db->where('br.is_active',1);
        $this->db->group_by('br.branch_id');
        $this->db->group_by('c.course_id');
        $leadstudentcourse_query = $this->db->get();
        if($leadstudentcourse_query)
        {
            $leadstudentcourse_data['leadstudents'] = $leadstudentcourse_query->result();
            /*SELECT br.branch_id,br.`name` as branch_name,IFNULL(SUM(bt.target),0) as target_enrollments,IFNULL(SUM(bt.visitor_target),0) as target_visitors,
(SELECT COUNT(bxl_1.branch_xref_lead_id) FROM branch_xref_lead bxl_1 LEFT JOIN lead_stage ls ON bxl_1.`status`=ls.lead_stage_id
WHERE br.branch_id=bxl_1.branch_id AND ls.lead_stage IN ("M7")) as enrollments,
(SELECT COUNT(bxl_2.branch_xref_lead_id) FROM branch_xref_lead bxl_2 LEFT JOIN lead_stage ls ON bxl_2.`status`=ls.lead_stage_id
WHERE br.branch_id=bxl_2.branch_id AND ls.lead_stage IN ("M4","M5","M6")) as visitors
FROM branch br
LEFT JOIN batch bt ON bt.fk_branch_id=br.branch_id
LEFT JOIN branch_xref_course bxc ON br.branch_id=bxc.fk_branch_id
LEFT JOIN branch_xref_lead bxl ON bxc.fk_branch_id=bxl.branch_id
GROUP BY br.branch_id;*/
            $this->db->select('br.branch_id,br.`name` as branch_name,IFNULL(SUM(bt.target),0) as target_enrollments,IFNULL(SUM(bt.visitor_target),0) as target_visitors,
                            (SELECT COUNT(bxl_1.branch_xref_lead_id) FROM branch_xref_lead bxl_1 LEFT JOIN lead_stage ls ON bxl_1.`status`=ls.lead_stage_id WHERE br.branch_id=bxl_1.branch_id AND ls.lead_stage IN ("M7")) as enrollments,
                            (SELECT COUNT(bxl_2.branch_xref_lead_id) FROM branch_xref_lead bxl_2 LEFT JOIN lead_stage ls ON bxl_2.`status`=ls.lead_stage_id WHERE br.branch_id=bxl_2.branch_id AND ls.lead_stage IN ("M4","M5","M6")) as visitors');
            $this->db->from('branch br');
            $this->db->join('batch bt','bt.fk_branch_id=br.branch_id','left');
            $this->db->join('branch_xref_course bxc','br.branch_id=bxc.fk_branch_id','left');
            $this->db->join('branch_xref_lead bxl','bxc.fk_branch_id=bxl.branch_id','left');
            $this->db->where('br.is_active',1);
            $this->db->group_by('br.branch_id');
            $achievedleadstudentcourse_query = $this->db->get();
            if($achievedleadstudentcourse_query)
            {
                $leadstudentcourse_data['achievedleadstudents'] = $achievedleadstudentcourse_query->result();
                $this->db->select('course_id,`name`');
                $this->db->from('course');
                $courses_query = $this->db->get();
                if($courses_query)
                {
                    $courses_data = $courses_query->result();
                    $leadstudentcourse_data['courses']=$courses_data;
                    $db_response=array("status"=>true,"message"=>'Branchwise report',"data"=>$leadstudentcourse_data);
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
            }
            else
            {
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            if($error['message'] =='')
            {
                $error['message']='Something went wrong. Please try again.';
            }
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }

    public function LeadStudentCourseYearlyReport()
    {
        $this->db->select('YEAR(bxl.created_on) as report_year,cs.`name` as course,
                            (SELECT COUNT(bxl_2.branch_xref_lead_id) FROM branch_xref_lead bxl_2 WHERE YEAR(bxl_2.created_on)=YEAR(bxl.created_on) AND bxl_2.fk_course_id=bxl.fk_course_id) as visitors,
                            (SELECT COUNT(bxl_1.branch_xref_lead_id) FROM branch_xref_lead bxl_1 LEFT JOIN lead_stage ls ON bxl_1.`status`=ls.lead_stage_id WHERE ls.lead_stage IN("M7") AND YEAR(bxl_1.created_on)=YEAR(bxl.created_on) AND bxl_1.fk_course_id=bxl.fk_course_id) as students');
        $this->db->from('branch_xref_lead bxl');
        $this->db->join('course cs','bxl.fk_course_id=cs.course_id');
        $this->db->where('bxl.created_on!=','');
        $this->db->where('cs.is_active',1);
        $this->db->group_by('YEAR(bxl.created_on)');
        $this->db->group_by('bxl.fk_course_id');
        $leadstudentcourse_query = $this->db->get();
        if($leadstudentcourse_query)
        {
            $leadstudentcourse_data['leadstudents'] = $leadstudentcourse_query->result();
            $this->db->select('SUM(target) as student_target,SUM(visitor_target) as visitor_target,YEAR(acedemic_startdate) as report_year,cs.`name` as course');
            $this->db->from('batch bt');
            $this->db->join('course cs','bt.fk_course_id=cs.course_id');
            $this->db->where('cs.is_active',1);
            $this->db->group_by('YEAR(bt.acedemic_startdate)');
            $this->db->group_by('bt.fk_course_id');
            $leadstarget_query = $this->db->get();
            if($leadstarget_query)
            {
                $leadstudenttarget_data = $leadstarget_query->result();
                for($ls=0;$ls<count($leadstudentcourse_data['leadstudents']);$ls++)
                {
                    if(($leadstudentcourse_data['leadstudents'][$ls]->report_year == $leadstudenttarget_data[$ls]->report_year) && ($leadstudentcourse_data['leadstudents'][$ls]->course == $leadstudenttarget_data[$ls]->course))
                    {
                        $leadstudentcourse_data['leadstudents'][$ls]->student_target=$leadstudenttarget_data[$ls]->student_target;
                        $leadstudentcourse_data['leadstudents'][$ls]->visitor_target=$leadstudenttarget_data[$ls]->visitor_target;
                    }
                    else
                    {
                        $leadstudentcourse_data['leadstudents'][$ls]->student_target=0;
                        $leadstudentcourse_data['leadstudents'][$ls]->visitor_target=0;
                    }
                }
                $this->db->select('course_id,`name`');
                $this->db->from('course');
                $courses_query = $this->db->get();
                if($courses_query)
                {
                    $courses_data = $courses_query->result();
                    $leadstudentcourse_data['courses']=$courses_data;
                    $db_response=array("status"=>true,"message"=>'yearly report',"data"=>$leadstudentcourse_data);
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
            }
            else
            {
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            if($error['message'] =='')
            {
                $error['message']='Something went wrong. Please try again.';
            }
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function AchievedLeadsCourseWiseReport()
    {
        $this->db->select('c.course_id,c.`name` as course_name,COUNT(bxl.branch_xref_lead_id) as leads');
        $this->db->from('course c');
        $this->db->join('branch_xref_lead bxl','bxl.fk_course_id=c.course_id');
        $this->db->where('c.is_active',1);
        $this->db->group_by('c.course_id');
        $achievedleadscourse_query = $this->db->get();
        if($achievedleadscourse_query)
        {
            $achievedleadscourse_data = $achievedleadscourse_query->result();
            $db_response=array("status"=>true,"message"=>'coursewise report',"data"=>$achievedleadscourse_data);
        }
        else
        {
            $error = $this->db->error();
            if($error['message'] =='')
            {
                $error['message']='Something went wrong. Please try again.';
            }
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function LeadStudentMonthlyReport()
    {
        $this->db->select('rm.month_index,rm.`name` as month,IFNULL(COUNT(bxl.branch_xref_lead_id),0) as achieved,IFNULL(SUM(bt.target),0) as target,IFNULL(SUM(bt.visitor_target),0) as visitor_target,IFNULL(YEAR(IFNULL(bxl.updated_on,bxl.created_on)),2016) as report_year');
        $this->db->from('branch br');
        $this->db->join('batch bt','br.branch_id=bt.fk_branch_id','left');
        $this->db->join('branch_xref_lead bxl','br.branch_id=bxl.branch_id','left');
        $this->db->join('report_month rm','IFNULL(MONTH(IFNULL(bxl.updated_on,bxl.created_on)),1)=rm.month_index','right');
        $this->db->where('IFNULL(YEAR(IFNULL(bxl.updated_on,bxl.created_on)),2016)',2016);
        $this->db->group_by('rm.report_month_id');
        $this->db->group_by('YEAR(IFNULL(bxl.updated_on,bxl.created_on))');
        $achievedleadscourse_query = $this->db->get();
        if($achievedleadscourse_query)
        {
            $achievedleadscourse_data = $achievedleadscourse_query->result();
            $db_response=array("status"=>true,"message"=>'month wise report',"data"=>$achievedleadscourse_data);
        }
        else
        {
            $error = $this->db->error();
            if($error['message'] =='')
            {
                $error['message']='Something went wrong. Please try again.';
            }
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getAnalyticsFilters()
    {
        /*$this->db->select("c.course_id, c.name as course_name");
        $this->db->from("course c");
        $this->db->where("c.is_active", 1);
        $courses_query = $this->db->get();
        if($courses_query)
        {
            $analyticfilters_data['courses'] = $courses_query->result();*/
            $this->db->select("br.branch_id,br.`name` as branch_name");
            $this->db->from("branch br");
            $this->db->where("br.is_active", 1);
            $branches_query = $this->db->get();
            if($branches_query)
            {
                $analyticfilters_data['branches'] = $branches_query->result();
                $db_response=array("status"=>true,"message"=>'analytics filters',"data"=>$analyticfilters_data);
            }
            else
            {
                $error = $this->db->error();
                if($error['message'] =='')
                {
                    $error['message']='Something went wrong. Please try again.';
                }
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        /*}
        else
        {
            $error = $this->db->error();
            if($error['message'] =='')
            {
                $error['message']='Something went wrong. Please try again.';
            }
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }*/
        return $db_response;
    }
}