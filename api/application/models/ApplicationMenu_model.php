<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ApplicationMenu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = FALSE;
    }
   
    
    function getMenu($data){
            
            $id=$data['UserID'];
            $query = $this->db->query("call Sp_Get_Menu_User($id)");
            if(!$query){
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
            else{
                if ($query->num_rows() > 0)
                {
                    
                    $res_array=$query->result();
                   $parents_indexes=array();
                    foreach($res_array as $k=>$v){
                        $tot_array[]=(array)$v;
                        if($v->parent_id==''){
                            $parents_indexes[]=$k;
                        }
                    }
                    $new = array();
                    foreach ($tot_array as $a){
                        $new[$a['parent_id']][] = $a;
                    }
                    for($mn=0;$mn<count($parents_indexes);$mn++){
                        
                        $tree[] = $this->createTree($new, array($tot_array[$parents_indexes[$mn]]))[0];
                        
                    }
                    $db_response=array("status"=>true,"message"=>'success',"data"=>$tree);
                }
                else{
                    $db_response=array("status"=>false,"message"=>'No Menu found',"data"=>array());
                }
            }
            return $db_response;
            
    }
    function createTree(&$list, $parent){
                $tree = array();
                foreach ($parent as $k=>$l){
                if(isset($list[$l['app_menu_id']])){
                    $l['childs'] = $this->createTree($list, $list[$l['app_menu_id']]);
                }
                $tree[] = $l;
                } 
                return $tree;
                }
   function getApplicationMenu($data){
            
            $id=$data['UserID'];
            //$query = $this->db->query("call Sp_Get_User_Session_Data($id)");
            $this->load->library('mydb');
            $arr  = $this->mydb->GetMultiResults("call Sp_Get_User_Session_Data($id)");
            $menuResult=[];$branchResult=[];
            if(isset($arr[0]))
            {
                $menuResult=$arr[0];
            }
            if(isset($arr[1]))
            {
                $branchResult=$arr[1];
            }
            $finalData['AppMenu']=[];
            $finalData['Branches']=[];

            if(count($menuResult)==0 && count($branchResult)==0)
            {
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>$finalData);
            }
            else
            {
                if (count($menuResult) > 0)
                {
                    $res_array=$menuResult;
                    $parents_indexes=array();
                    foreach($res_array as $k=>$v){
                        if($v['action']!=null){
                        $v['action']=explode('__',$v['action']);
                        }
                        $tot_array[]=(array)$v;
                        if($v['parent_id']==''){
                            $parents_indexes[]=$k;
                        }
                    }
                    $new = array();
                    foreach ($tot_array as $a){
                        $new[$a['parent_id']][] = $a;
                    }
                    for($mn=0;$mn<count($parents_indexes);$mn++){
                        
                        $tree[] = $this->createTree($new, array($tot_array[$parents_indexes[$mn]]))[0];
                        
                    }
                    $finalData['AppMenu']=$tree;
                }
                if(count($branchResult)>0)
                {
                    $finalData['Branches']=$branchResult;
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$finalData);
            }
            return $db_response;
            
    }
}

?>

