<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-26
 * Time: 02:42 PM
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class BooksIssue_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    public function getStudentCourseBooksList($batchId,$leadId)
    {
        $this->db->select('p.product_id,p.`name`,(`pc`.`quantity`+IFNULL(lids.quantity,0)) as quantity,b.batch_id,b.fk_branch_id');
        $this->db->from('batch b');
        $this->db->join('product_xref_course pc','pc.fk_course_id=b.fk_course_id');
        $this->db->join('product p','p.product_id=pc.fk_product_id');
        $this->db->join('lead_item_issue lids','pc.fk_product_id=lids.fk_product_id AND lids.status= 1 AND lids.fk_lead_id = '.$leadId,'left');
        $this->db->where('b.batch_id',$batchId);
        $this->db->where('pc.is_active',0);
        $query = $this->db->get();
        if($query)
        {
            if($query->num_rows()>0)
            {
                $rows=$query->result();
                for($r=0;$r<count($rows);$r++)
                {
                    $rows[$r]->leadId = $leadId;
                    $branchStock_query=$this->db->query("select sum(avail_quantity-transfer_quantity) as quantity,sum(adhoc_quantity) as adhoc_quantity from (
SELECT sum(quantity-returned_quantity) as avail_quantity,0 transfer_quantity,0 adhoc_quantity  FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='branch' and s.fk_receiver_id=".$rows[$r]->fk_branch_id." and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=".$rows[$r]->product_id."
UNION ALL
SELECT 0 avail_quantity,sum(quantity-returned_quantity) as transfer_quantity,0 adhoc_quantity FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='branch' and s.fk_requested_id=".$rows[$r]->fk_branch_id." and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=".$rows[$r]->product_id."
UNION ALL
	SELECT 0 avail_quantity,0 transfer_quantity,SUM(lis.quantity) as adhoc_quantity FROM lead_item_issue lis WHERE fk_product_id=".$rows[$r]->product_id." AND `status`=1 AND fk_lead_id='.$leadId.') z");
                    $issuedQuantity = $this->db->query('SELECT sum(quantity) as quantity FROM lead_item_issue WHERE fk_lead_id ='.$leadId.' AND fk_product_id='.$rows[$r]->product_id.' AND `status`=0 GROUP BY fk_lead_id');
                    $alreadyIssuedQuantity = 0;
                    if($issuedQuantity)
                    {
                        if($issuedQuantity->num_rows()>0)
                        {
                            $issueResult=$issuedQuantity->result();
                            $rows[$r]->issuedQuantity = $issueResult[0]->quantity;
                            $alreadyIssuedQuantity = $issueResult[0]->quantity;
                        }
                        else
                        {
                            $rows[$r]->issuedQuantity = 0;
                        }
                    }
                    if($branchStock_query)
                    {
                        if($branchStock_query->num_rows()>0)
                        {
                            $availableResult=$branchStock_query->result();
                            $rows[$r]->availableQuantity = $availableResult[0]->quantity - $alreadyIssuedQuantity;
                        }
                        else
                        {
                            $rows[$r]->availableQuantity = 0;
                        }
                    }
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'no data','data'=>array());
        }
        return $db_response;
    }
    public function checkAlreadyIssued($batchId,$leadId,$productId)
    {
        $this->db->select('lead_item_issue_id');
        $this->db->from('lead_item_issue');
        $this->db->where('fk_batch_id',$batchId);
        $this->db->where('fk_lead_id',$leadId);
        $this->db->where('fk_product_id',$productId);
        $this->db->where('status',0);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $res = $query->result();
            $issuedId = $res[0]->lead_item_issue_id;
        }
        else
        {
            $issuedId =0;
        }
        return $issuedId;
    }
    public function saveBooksIssued($userId,$issuedBooks)
    {
        $createdBy = $userId;
        for($p=0;$p<count($issuedBooks);$p++)
        {
            $book = explode('|',$issuedBooks[$p]);//0-batchid 1-leadid 2-productid 4-quantity
            $alreadyIssued =$this->checkAlreadyIssued($book[0],$book[1],$book[2]);
            if($alreadyIssued == 0)
            {
                if($book[3]!=0)
                {
                    $data = array(
                        'fk_batch_id' => $book[0],
                        'fk_lead_id' => $book[1],
                        'fk_product_id' => $book[2],
                        'quantity' => $book[3],
                        'status' => 0,
                        'fk_created_by' => $createdBy,
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('lead_item_issue',$data);
                }
            }
            else
            {
                if($book[3]!=0)
                {
                    $this->db->where('fk_batch_id',$book[0]);
                    $this->db->where('fk_lead_id',$book[1]);
                    $this->db->where('fk_product_id',$book[2]);
                    $this->db->where('status',0);
                    $this->db->update('lead_item_issue',array('quantity'=>$book[3],'fk_updated_by'=>$createdBy,'updated_date'=>date('Y-m-d H:i:s')));
                }
            }
        }
        $db_response=array("status"=>true,"message"=>'Books issued successfully',"data"=>[]);
        return $db_response;
    }

    public function getStudentAdhocCourseBooks($courseId,$studentId)
    {
        $this->db->select("p.product_id,p.`name` as product_name,IFNULL(sum(pxc.quantity),0) as available_qty,IFNULL(sum(li.quantity),0) as issued,(SELECT IFNULL(sum(li_ah.quantity),0) FROM lead_item_issue li_ah WHERE pxc.fk_product_id=li_ah.fk_product_id AND li_ah.issue_type='Adhoc' AND li_ah.fk_lead_id=".$studentId.") as adhoc_issue");
        $this->db->from('product_xref_course pxc');
        $this->db->join('product p','pxc.fk_product_id=p.product_id','left');
        $this->db->join('lead_item_issue li','pxc.fk_product_id=li.fk_product_id AND li.issue_type="Default" AND li.fk_lead_id='.$studentId,'left');
        $this->db->where('pxc.fk_course_id',$courseId);
        $this->db->where('pxc.is_active',0);
        $this->db->where('p.is_active',0);
        $this->db->group_by('pxc.fk_product_id,li.fk_lead_id');
        $adhoc_book_query = $this->db->get();
        //echo $this->db->last_query();exit;
        if($adhoc_book_query)
        {
            if($adhoc_book_query->num_rows()>0)
            {
                $adhoc_book_res = $adhoc_book_query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$adhoc_book_res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'No products mapped',"data"=>[]);
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function saveStudentAdhocCourseBooks($createdBy,$studentId,$courseId,$batchId,$adhocProductQty)
    {
        for($ad=0;$ad<count($adhocProductQty);$ad++)
        {
            $adhoc_data = explode('@@@@@@',$adhocProductQty[$ad]);
            $this->db->select('lead_item_issue_id');
            $this->db->from('lead_item_issue');
            $this->db->where('fk_batch_id',$batchId);
            $this->db->where('fk_lead_id',$studentId);
            $this->db->where('fk_product_id',$adhoc_data[1]);
            $this->db->where('status',1);
            $query = $this->db->get();
            if($query)
            {
                if($query->num_rows()>0)
                {
                    $res = $query->result();
                    $lead_item_issue_id = $res[0]->lead_item_issue_id;
                    $this->db->where('lead_item_issue_id',$lead_item_issue_id);
                    $this->db->where('status',1);
                    $this->db->update('lead_item_issue',array('quantity'=>$adhoc_data[0],'issue_type' => 'Adhoc','status' => 1,'fk_updated_by'=>$createdBy,'updated_date'=>date('Y-m-d H:i:s')));
                }
                else
                {
                    $data = array(
                        'fk_batch_id' => $batchId,
                        'fk_lead_id' => $studentId,
                        'fk_product_id' => $adhoc_data[1],
                        'quantity' => $adhoc_data[0],
                        'issue_type' => 'Adhoc',
                        'status' => 1,
                        'fk_created_by' => $createdBy,
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('lead_item_issue',$data);
                }
            }
            else
            {
                $error = $this->db->error();
                return array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        }
        return array("status"=>true,"message"=>'Saved successfully',"data"=>[]);
    }
}