<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-17
 * Time: 07:03 PM
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class BranchGrn_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    public function getBranchList($val) {
        $qry = "select b.branch_id id, b.name as text from branch b where b.is_active=1 and name like '%".$val."%'";

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function getVendorList($val)
    {
        $qry = "select inventory_vendor_id id, name as text from inventory_vendor where is_active=0 and name like '%".$val."%'";

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function getProductList($val)
    {
        $query = $this->db->query("SELECT product_id as id,CONCAT_WS(' | ',code,name) as text FROM product WHERE code like '%".$val."%' or name like '%".$val."%'");
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    public function getPreviousRecords()
    {
        $this->db->select('sequence_number');
        $this->db->from('stockflow');
        $this->db->where('type','GRN');
        $this->db->order_by('stockflow_id','desc');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $recordId = $query->row();
            $previousId = $recordId->sequence_number;
        }
        else{
            $previousId = 0;
        }
        return $previousId;
    }
    public function checkGtnExist($gtnNo)
    {
        $this->db->select('sequence_number');
        $this->db->from('stockflow');
        $this->db->where('type','GTN');
        $this->db->where('sequence_number',$gtnNo);
        //$this->db->where('requested_mode','warehouse');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $existStatus = true;
        }
        else
        {
            $existStatus = false;
        }
        return $existStatus;
    }
    public function addBranchGrn($data)
    {
        $receiverMode = $data['receiverMode'];
        $receiverObjectId = $data['receiverObjectId'];
        $requestMode = $data['requestMode'];
        $requestObjectId = $data['requestObjectId'];
        $createdBy = $data['userID'];
        $invoiceNumber = $data['invoice_number'];
        $invoiceAttachment = $data['invoice_file'];
        $gtnRemarks = $data['remarks'];
        $createdDate = date('Y-m-d H:i:s');
        $products = $data['products'];
        $gtnNo =$data['grnNo'];
        $stock_flow_id =$data['stock_flow_id'];
        if(trim($stock_flow_id)==''){
            $stock_flow_id=NULL;
        }
        $previousRecord = $this->getPreviousRecords();
        if($previousRecord == 0)
        {
            $sequenceNumber = 20001;
        }
        else{
            $sequenceNumber = $previousRecord+1;
        }
        $gtnExist = $this->checkGtnExist($gtnNo);
        if($gtnExist === true)
        {
            $this->db->where('sequence_number',$gtnNo);
            $updateGtn = $this->db->update('stockflow',array('type'=>'GTN','status'=>1));
            $grnId = $this->db->insert('stockflow',array('type'=>'GRN','sequence_number'=>$sequenceNumber,'receiver_mode'=>$receiverMode,'fk_receiver_id'=>$receiverObjectId,'requested_mode'=>$requestMode,'fk_requested_id'=>$requestObjectId,'created_by'=>$createdBy,'created_date'=>$createdDate,'invoice_number'=>$invoiceNumber,'invoice_attachment'=>$invoiceAttachment,'remarks'=>$gtnRemarks,'fk_gtn_stockflow_id'=>$stock_flow_id));
            if($grnId)
            {
                $lastInsertedId = $this->db->insert_id();
                for($p=0;$p<count($products);$p++)
                {
                    $product = explode('|',$products[$p]);
                    $data = array(
                        'fk_stockflow_id'=>$lastInsertedId,
                        'fk_product_id'=>$product[0],
                        'quantity' => $product[1]
                    );
                    $this->db->insert('stockflow_item',$data);
                }
                $status = true;
                $message = 'Received successfully';
            }
            else{
                $error = $this->db->error();
                $status=FALSE;
                $message=$error['message'];
            }
        }
        else{
            $status =FALSE;
            $message = 'Invalid GTN No';
        }

        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function getGtnNumbers($val,$branch)
    {
        $query = $this->db->query("SELECT sequence_number as id,sequence_number as text FROM stockflow WHERE sequence_number like '%".$val."%' and type='GTN' and receiver_mode='branch' and `status`=0 and fk_receiver_id=".$branch);
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    public function checkGtnNoExist($param)
    {
        $this->db->select("stockflow_id");
        $this->db->from("stockflow");
        $this->db->where("sequence_number", $param['gtnNo']);
        $this->db->where('type','GTN');
        //$this->db->where('requested_mode','warehouse');
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid Number',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function checkInvoiceNoExist($param)
    {
        $this->db->select("stockflow_id");
        $this->db->from("stockflow");
        $this->db->where("invoice_number", $param['invoiceNumber']);
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>false,"message"=>'Invoice number already exist',"data"=>array("invoiceNumber"=>'Invoice number already exist'));
            }
            else
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getGtnProductDetails($gtnNo)
    {
        $this->db->select('s.fk_requested_id,s.requested_mode,p.product_id,p.name,p.code,si.quantity,s.stockflow_id');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','si.fk_stockflow_id= s.stockflow_id','left');
        $this->db->join('product p','si.fk_product_id=p.product_id','left');
        $this->db->where('s.sequence_number',$gtnNo['gtnNo']);
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            if(count($list) > 0)
            {
                foreach($list as $val)
                {
                    $list_final ['name'] =  $val->name;
                    $list_final ['request_mode'] = $val->requested_mode;
                    $list_final ['request_id'] = $val->fk_requested_id;
                    $list_final ['stockflow_id'] = $val->stockflow_id;
                }
                $list_final ['products'] = $list;
                $db_response=array("status"=>true,"message"=>"success","data"=>$list_final);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function branchGrnDataTables($branchId)
    {
        $table = 'stockflow s';
        $primaryKey = 's`.`stockflow_id';
        $columns = array(
            array( 'db' => 'CONCAT_WS("",if(s.receiver_mode="branch",(select code from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse","'.INVENTORY_WAREHOUSE_PREFIX.'","NA"))))),`s`.`sequence_number`) as new_sequence_number', 'dt' => 'new_sequence_number',          'field' => 'new_sequence_number' ),
            array( 'db' => '`s`.`sequence_number`',                     'dt' => 'sequence_number',          'field' => 'sequence_number' ),
            array( 'db' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA"))))) as receiveName',              'dt' => 'receiveName',           'field' => 'receiveName'),
            array( 'db' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA"))))) as requestedName',              'dt' => 'requestedName',           'field' => 'requestedName'),
            array( 'db' => 'sum(si.quantity) as total',                 'dt' => 'total',                    'field' => 'total' ),
            array( 'db' => 's.invoice_number',                 'dt' => 'invoice_number',                    'field' => 'invoice_number' ),
            array( 'db' => 's.invoice_attachment',                 'dt' => 'invoice_attachment',                    'field' => 'invoice_attachment' ),
            array( 'db' => 'sum(ifnull(si.returned_quantity,0)) as returnTotal',  'dt' => 'returnTotal',              'field' => 'returnTotal'),
            array( 'db' => '`s`.`created_date` as sentOn',                     'dt' => 'sentOn',          'field' => 'sentOn' ),
            array( 'db' => '`s`.`created_date` as receivedOn',                     'dt' => 'receivedOn',          'field' => 'receivedOn'),
            array( 'db' => '`s`.`stockflow_id`',                     'dt' => 'stockflow_id',          'field' => 'stockflow_id' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'CONCAT_WS("",if(s.receiver_mode="branch",(select code from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse","'.INVENTORY_WAREHOUSE_PREFIX.'","NA"))))),`s`.`sequence_number`)', 'dt' => 'new_sequence_number',          'field' => 'new_sequence_number' ),
            array( 'db' => '`s`.`sequence_number`',            'dt' => 'sequence_number',         'field' => 'sequence_number' ),
            array( 'db' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))',              'dt' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))',           'field' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))'),
            array( 'db' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))',              'dt' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))',           'field' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))'),
            array( 'db' => 's.invoice_number',                 'dt' => 'invoice_number',                    'field' => 'invoice_number' ),
            array( 'db' => '`s`.`created_date`',                     'dt' => 'created_date',          'field' => 'created_date' ),
        );
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        $joinQuery = "from stockflow s JOIN stockflow_item si ON s.stockflow_id=si.fk_stockflow_id";
        $extraWhere = "s.type='GRN' and  s.receiver_mode='branch' and s.fk_receiver_id=".$branchId;
        $groupBy = "s.stockflow_id";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery,$extraWhere,$groupBy));
        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['receivedOn']=date('j M, Y',strtotime($responseData['data'][$i]['receivedOn']));
            $responseData['data'][$i]['sentOn']=date('j M, Y',strtotime($responseData['data'][$i]['sentOn']));
            $responseData['data'][$i]['invoice_attachment']=urlencode(basename('./'.$responseData['data'][$i]['invoice_attachment']));
        }
        return $responseData;
    }
    function getBranchStock($branchId){
        $branchstock_query="SELECT productId,productName,producType,sum(received) as received,sum(dispatched) as dispatched,sum(received-dispatched-issued) as inHand,sum(intransit) as inTransit,sum(issued) as issued,remarks from (
SELECT si.fk_product_id as productId,p.name as productName,rtv.value as producType,0 as received,sum(quantity) as dispatched,0 as intransit,0 as issued,s.remarks FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='branch' and s.fk_requested_id=$branchId and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id group by si.fk_product_id
UNION ALL
SELECT si.fk_product_id as productId,p.name as productName,rtv.value as producType,0 as received,0 as dispatched,sum(quantity) as intransit,0 as issued,s.remarks FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='branch' and s.fk_requested_id=$branchId and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and s.status=0 group by si.fk_product_id
UNION ALL
SELECT si.fk_product_id as productId,p.name,rtv.value as producType,sum(quantity-returned_quantity) as received,0 as dispatched,0 as intransit,0 as issued,s.remarks FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='branch' and s.fk_receiver_id=$branchId and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id group by si.fk_product_id
UNION ALL
SELECT lis.fk_product_id as productId,p.name,rtv.value as producType,0 as received,0 as dispatched,0 as intransit,sum(lis.quantity) as issued,'' remarks FROM `lead_item_issue` lis ,product p,reference_type_value rtv,batch b WHERE lis.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and lis.status=0 and lis.fk_batch_id=b.batch_id and b.fk_branch_id=$branchId group by  lis.fk_product_id
) p group by 1";
        $res=$this->db->query($branchstock_query);

        if($res){
            if($res->num_rows()>0){
                $rows=$res->result();

                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;
    }
    function getBranchStockProductWise($type,$productId,$branchId){
        if($type=='outward'){
            $wareHouse_query="SELECT s.created_date,si.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(quantity) as quantity,IFNULL(s.remarks,'') as remarks,if(s.receiver_mode='branch',(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_receiver_id),'NA'))))) as receiveName,
                          if(s.requested_mode='branch',(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_requested_id),'NA'))))) as requestedName,if(s.status=0,'In Transit','Delivered') as deliverStatus FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='branch' and s.fk_requested_id=$branchId and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=$productId group by s.stockflow_id";
        }
        if($type=='inward'){
            $wareHouse_query="SELECT s.created_date,si.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(quantity-returned_quantity) as quantity,IFNULL(s.remarks,'') as remarks,if(s.receiver_mode='branch',(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_receiver_id),'NA'))))) as receiveName,
if(s.requested_mode='branch',(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_requested_id),'NA'))))) as requestedName FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='branch' and s.fk_receiver_id=$branchId and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=$productId group by s.stockflow_id";
        }
        if($type=='issued'){
            $wareHouse_query="select lis.created_date,lis.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(lis.quantity) as quantity,'' as remarks,CONCAT_WS(\"\",b.code,bat.code,bl.lead_number) as  receiveName,e.employee_code as requestedName from lead_item_issue lis,product p,reference_type_value rtv,batch bat,employee e,branch_xref_lead bl,branch b where lis.fk_product_id=p.product_id and lis.fk_batch_id=bat.batch_id  and rtv.reference_type_value_id=p.fk_product_type_id and lis.status=0 and e.user_id=lis.fk_created_by and bl.branch_xref_lead_id=lis.fk_lead_id and b.branch_id=bat.fk_branch_id and bat.fk_branch_id=$branchId and lis.fk_product_id=$productId group by lis.lead_item_issue_id order by lis.lead_item_issue_id desc";
        }

        $res=$this->db->query($wareHouse_query);

        if($res){
            if($res->num_rows()>0){
                $rows=$res->result();
                foreach($rows as $k=>$v){
                    $v->created_date=date('d M, Y',strtotime($v->created_date));
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;

    }
    public function getBranchGrnProducts($seqNum)
    {
        $this->db->select('si.quantity,p.`name` as productName');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','s.stockflow_id=si.fk_stockflow_id','left');
        $this->db->join('product p','p.product_id=si.fk_product_id');
        $this->db->where('s.sequence_number',$seqNum);
        $query = $this->db->get();
        if($query){
            if($query->num_rows()>0){
                $rows=$query->result();
                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;
    }
}