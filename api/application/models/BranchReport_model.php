<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @controller Name Branch Visit
 * @category        Model
 * @author          Abhilash
 */
class BranchReport_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    /**
     * Function to get Reports
     *
     * @return - array
     * @created date - 13 April 2016
     * @author : Abhilash
     */
    public function getBranchReport($branch, $course, $stage){
        $query = $this->db->query("call Sp_Get_Branchwise_Leads_Report({$branch},{$course},{$stage})");
                if($query){
                    if ($query->num_rows() > 0){
                        $res=$query->result();
                        $this->db->close();
                        $this->load->database();
                        return $res;
                    }
                    else{
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
        return $responseData;
    }

    public function getTelecallerReport($branch,$from_date,$to_date)
    {
        $query = $this->db->query("call Sp_Get_Telecaller_Report({$branch},{$from_date},{$to_date})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $this->db->close();
                $this->load->database();
                return $res;
            }
            else{
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        return $responseData;
    }
}
