<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @controller Name Branch Visit
 * @category        Model
 * @author          Abhilash
 */
class BranchVisit_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    /**
     * Function to get Branch Visit list Datatables
     *
     * @return - array
     * @created date - 31 Mar 2016
     * @author : Abhilash
     */
    public function getBranchVisitList($branch_id){
        $table = 'branch_xref_lead';
        // Table's primary key
        $primaryKey = 'bxl`.`branch_xref_lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array('db' => '`bxl`.`lead_number`', 'dt' => 'lead_number', 'field' => 'lead_number'),
            array('db' => '`l`.`name`', 'dt' => 'name', 'field' => 'name'),
            array('db' => '`l`.`phone`', 'dt' => 'phone', 'field' => 'phone'),
            array('db' => '`l`.`email`', 'dt' => 'email', 'field' => 'email'),
            array('db' => '`c`.`name` as `course_name`', 'dt' => 'course_name', 'field' => 'course_name'),
            array('db' => '`e`.`name` as `counselloer_name`', 'dt' => 'counselloer_name', 'field' => 'counselloer_name'),
            array('db' => '`b`.`branch_visited_date` as `branch_visited_date`', 'dt' => 'branch_visited_date', 'field' => 'branch_visited_date'),
            array('db' => '`b`.`created_date`', 'dt' => 'created_date', 'field' => 'created_date', 'formatter' => function( $d, $row ) {
                return date( 'd M,Y', strtotime($d));
            }),
            array('db' => '`bxl`.`lead_id`', 'dt' => 'lead_id', 'field' => 'lead_id'),
            array('db' => '`br`.`name` as `branch_name`', 'dt' => 'branch_name', 'field' => 'branch_name'),
            array('db' => '`b`.`second_level_counsellor_updated_date`', 'dt' => 'second_level_counsellor_updated_date', 'field' => 'second_level_counsellor_updated_date'),
            array('db' => '`b`.`second_level_counsellor_comments`', 'dt' => 'second_level_counsellor_comments', 'field' => 'second_level_counsellor_comments'),
            array('db' => '`c`.`course_id`', 'dt' => 'course_id', 'field' => 'course_id'),
            array('db' => '`e`.`user_id`', 'dt' => 'user_id', 'field' => 'user_id'),
            array('db' => '`e`.`user_id`', 'dt' => 'user_id', 'field' => 'user_id'),
            array('db' => '`bxl`.`branch_xref_lead_id`', 'dt' => 'branch_xref_lead_id', 'field' => 'branch_xref_lead_id'),
            array('db' => '`bxl`.`lead_number`', 'dt' => 'lead_number', 'field' => 'lead_number')
        );

        $globalFilterColumns = array(
            array('db' => '`bxl`.`lead_number`', 'dt' => 'lead_number', 'field' => 'lead_number'),
            array('db' => '`l`.`name`', 'dt' => 'name', 'field' => 'name'),
            array('db' => '`l`.`phone`', 'dt' => 'phone', 'field' => 'phone'),
            array('db' => '`l`.`email`', 'dt' => 'email', 'field' => 'email'),
            array('db' => '`c`.`name`', 'dt' => 'course_name', 'field' => 'course_name'),
            array('db' => '`e`.`name`', 'dt' => 'counselloer_name', 'field' => 'counselloer_name'),
            array('db' => '`b`.`branch_visited_date`', 'dt' => 'branch_visited_date', 'field' => 'branch_visited_date')
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db' => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /*         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from branch_visitor b JOIN branch_xref_lead bxl ON b.fk_branch_xref_lead_id=bxl.branch_xref_lead_id JOIN lead as l ON l.lead_id=bxl.lead_id JOIN course as c ON c.course_id=bxl.fk_course_id JOIN branch as br ON br.branch_id=bxl.branch_id JOIN employee e ON b.second_level_counsellor_id=e.user_id";
        $extraWhere = "`bxl`.`branch_id`=".$branch_id." AND `bxl`.`is_active`=1";
        $groupBy = "";
        $responseData = (SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere, $groupBy));
        for ($i = 0; $i < count($responseData['data']); $i++) {
            $responseData['data'][$i]['lead_id'] = encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id_encode'] = encode($responseData['data'][$i]['branch_xref_lead_id']);
            $responseData['data'][$i]['course_id'] = encode($responseData['data'][$i]['course_id']);
            if(isset($responseData['data'][$i]['branch_visited_date']) && strlen($responseData['data'][$i]['branch_visited_date'])>0 )
                $responseData['data'][$i]['branch_visited_date']=(new DateTime($responseData['data'][$i]['branch_visited_date']))->format('d M,Y');
            else
                $responseData['data'][$i]['branch_visited_date'] = ' --- ';
            
        }
        return $responseData;
    }

    /**
     * Function to get Branch Visit for leads
     *
     * @return - array
     * @created date - 31 Mar 2016
     * @author : Abhilash
     */
    public function getLeadSpanshot($lead_id, $branch_id,$hdnBranchVisitId=0){
        $error = false;
        $this->db->select('branch_xref_lead_id,lead_id');
        $this->db->from('branch_xref_lead');
        $this->db->where('lead_number', $lead_id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $branch_id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if(!$error){
            $this->db->select('l.name, l.phone, l.email, bxl.branch_xref_lead_id, bxl.lead_id, bxl.branch_id, bxl.fk_course_id, bxl.lead_number, bxl.expected_visit_date, bxl.expected_enroll_date, bxl.transferred_to, bxl.transferred_from, b.name branch_name, c.name course_name,bxl.next_followup_date,bxl.expected_admission_date,ls.lead_stage');
            $this->db->from('branch_xref_lead bxl');
            $this->db->join('lead l', 'l.lead_id=bxl.lead_id');
            $this->db->join('course c', 'c.course_id=bxl.fk_course_id');
            $this->db->join('branch b', 'b.branch_id=bxl.branch_id');
            $this->db->join('lead_stage ls', 'ls.lead_stage_id=bxl.status','left');
            $this->db->where('bxl.lead_number', $lead_id);
            $this->db->where('bxl.branch_id', $branch_id);
            $this->db->where('bxl.is_active', 1);
            $this->db->order_by('bxl.branch_xref_lead_id');

            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    $data = $res_all[0];
                    $data->expected_visit_date = (new DateTime($res_all[0]->expected_visit_date))->format('d M,Y');
                    $data->expected_enroll_date = (new DateTime($res_all[0]->expected_enroll_date))->format('d M,Y');
                    $data->expected_admission_date = (new DateTime($res_all[0]->expected_admission_date))->format('d M,Y');
                    $data->next_followup_date = (new DateTime($res_all[0]->next_followup_date))->format('d M,Y');

                    $this->db->select('e.name ,bv.created_date, bv.second_level_counsellor_updated_date, bv.second_level_counsellor_comments, bv.branch_visited_date,bv.branch_visit_comments,bv.comment_type,bv.second_level_counsellor_id,bv.is_counsellor_visited');
                    $this->db->from('branch_visitor bv');
                    $this->db->join('employee e', 'bv.second_level_counsellor_id=e.user_id');
                    $this->db->where('bv.second_level_counsellor_id IS NOT NULL');
                    $this->db->where('bv.fk_branch_xref_lead_id', $data->branch_xref_lead_id);
                    $this->db->where('bv.status', 1);
                    if($hdnBranchVisitId>0){
                        $this->db->where('bv.branch_visitor_id', $hdnBranchVisitId);
                    }
                    $this->db->order_by('bv.branch_visitor_id', 'desc');
                    $this->db->limit(1);
                    $res_all = $this->db->get();
                    if ($res_all) {
                        $res_all = $res_all->result();
                        if (count($res_all) > 0) {
                            $data->counsellor = $res_all[0]->name;
                            $data->visited_date = (new DateTime($res_all[0]->branch_visited_date))->format('d M,Y');
                            $data->counsellor_created_date = $res_all[0]->created_date;
                            if($res_all[0]->second_level_counsellor_updated_date!=null){
                                $data->second_level_counsellor_updated_date = (new DateTime($res_all[0]->second_level_counsellor_updated_date))->format('d M,Y');
                            }
                            else{
                                $data->second_level_counsellor_updated_date = '----';
                            }

                            $data->second_level_counsellor_comments = $res_all[0]->second_level_counsellor_comments;
                            $data->branch_visit_comments = $res_all[0]->branch_visit_comments;
                            $data->comment_type = $res_all[0]->comment_type;
                            $data->second_level_counsellor_id = $res_all[0]->second_level_counsellor_id;
                            $data->is_counsellor_visited = $res_all[0]->is_counsellor_visited;
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    return $data;
                } else{
                    return 'nodata';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }

    public function getContactVisitorSpanshot($lead_id, $branch_id,$hdnBranchVisitId=0)
    {
        $error = false;
        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $branch_id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if(!$error)
        {
            $this->db->select('`l`.`name`, `l`.`phone`, `l`.`email`, l.lead_id, l.fk_branch_id, \'\' as fk_course_id, `b`.`name` `branch_name`');
            $this->db->from('`lead` `l`');
            $this->db->join('branch b', 'b.branch_id=l.fk_branch_id');
            $this->db->where('l.lead_id', $lead_id);
            $this->db->where('l.fk_branch_id', $branch_id);
            $this->db->where('l.status', 1);
            $this->db->order_by('l.lead_id');

            $res_all = $this->db->get();
            //die($this->db->last_query());
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    $data = $res_all[0];
                    return $data;
                } else{
                    return 'nodata';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }

    /**
     * Function to Save Branch Visit
     *
     * @return - boolean
     * @created date - 31 Mar 2016
     * @author : Abhilash
     */
    public function saveBranchVisit($data)
    {
        $error = false;
        $this->db->select('bxl.branch_xref_lead_id , bxl.lead_id ,l.name');
        $this->db->from('branch_xref_lead bxl');
        $this->db->join('lead l', 'bxl.lead_id=l.lead_id');
        $this->db->where('bxl.lead_number', $data['lead_id']);
        $this->db->where('bxl.branch_id', $data['branch']);
        $leadData = $this->db->get();
        if ($leadData) {
            $leadData = $leadData->result();
            if (count($leadData) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nolead';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('u.user_id, e.name');
        $this->db->from('user u');
        $this->db->join('employee e', 'u.user_id=e.user_id');
        $this->db->where('u.user_id', $data['user']);
        $this->db->where('u.is_active', 1);
        $this->db->where('u.is_seconday_counsellor', 1);
        $user = $this->db->get();
        if ($user) {
            $user = $user->result();
            if (count($user) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nouser';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        $branch_lead_id = $leadData[0]->branch_xref_lead_id;
        $lead_id = $leadData[0]->lead_id;
        if ($data['hdnBranchVisitId'] > 0) {
            $this->db->select('second_level_counsellor_id');
            $this->db->from('branch_visitor');
            $this->db->where('status', 1);
            $this->db->where('branch_visitor_id', $data['hdnBranchVisitId']);
            $result = $this->db->get();
            if ($result) {
                $result = $result->result();
                if (count($result) > 0) {
                    $exist_counseler_id = $result[0]->second_level_counsellor_id;
                } else {
                    $exist_counseler_id = 0;
                }
            } else {
                $exist_counseler_id = 0;
            }
        }
        else{
            $exist_counseler_id = 0;
        }
        if($data['user']!=$exist_counseler_id)
        {
        $this->db->select('branch_visitor_id');
        $this->db->from('branch_visitor');
        $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
        $this->db->where('second_level_counsellor_id', $data['user']);
        $this->db->where('status', 1);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                $error = true;
                $this->db->select('branch_visitor_id');
                $this->db->from('branch_visitor');
                $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
                $this->db->where('second_level_counsellor_id', $data['user']);
                $this->db->where('status', 1);
                $this->db->where('date(branch_visited_date)="' . date('Y-m-d', strtotime($data['visitedDate'])) . '"');
                $result = $this->db->get();
                if ($result) {
                    $result = $result->result();
                    if (count($result) > 0) {
                        $error = true;
                        return 'alreadyDate';
                    } else {
                        $error = false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                $error = false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
        if ($data['hdnBranchVisitId'] == 0)
        {
            $this->db->select('branch_visitor_id');
            $this->db->from('branch_visitor');
            $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
            $this->db->where('status', 1);
            $this->db->where('date(branch_visited_date)="' . date('Y-m-d', strtotime($data['visitedDate'])) . '"');
            $result = $this->db->get();
            if ($result) {
                $result = $result->result();
                if (count($result) > 0) {
                    $error = true;
                    return 'alreadyDate';
                } else {
                    $error = false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        if ($data['hdnBranchVisitId'] == 0) {
            $this->db->select('fk_branch_xref_lead_id');
            $this->db->from('branch_visitor');
            $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
            $this->db->where('date(branch_visited_date)="' . date('Y-m-d', strtotime($data['visitedDate'])) . '"');
            $this->db->where('status', 1);

            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    $error = true;
                    return 'duplicate';
                } else {
                    $error = false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }

        if(!$error){

            $this->db->trans_start();
            if ($data['hdnBranchVisitId'] > 0) {
                $data_update =array(
                    'fk_branch_xref_lead_id' =>$branch_lead_id,
                    'second_level_counsellor_id' =>$data['user'],
                    'created_by' =>$_SERVER['HTTP_USER'],
                    'created_date' =>date('Y-m-d H:i:s'),
                    'branch_visited_date' =>date('Y-m-d H:i:s',strtotime($data['visitedDate'])),
                    'branch_visit_comments' => $data['branchVisitComments']
                );
                $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
                $this->db->where('branch_visitor_id', $data['hdnBranchVisitId']);
                $this->db->update('branch_visitor', $data_update);
            } else {
                $data_insert =array(
                    'fk_branch_xref_lead_id' =>$branch_lead_id,
                    'second_level_counsellor_id' =>$data['user'],
                    'created_by' =>$_SERVER['HTTP_USER'],
                    'created_date' =>date('Y-m-d H:i:s'),
                    'branch_visited_date' =>date('Y-m-d H:i:s',strtotime($data['visitedDate'])),
                    'branch_visit_comments' => $data['branchVisitComments'],
                    'comment_type'=>$data['CommentType']
                );
                $this->db->insert('branch_visitor', $data_insert);
            }
            $data_insert =array(
                'lead_id' =>$lead_id,
                'branch_xref_lead_id' =>$branch_lead_id,
                'type' =>'Allotted Counsellor',
                'description' =>$data['branchVisitComments'].', Allotted '.$leadData[0]->name.' to '.$user[0]->name,
                'created_by' =>$_SERVER['HTTP_USER'],
                'created_on' =>date('Y-m-d H:i:s'),
            );
            $this->db->insert('timeline', $data_insert);
            $data['branchXrefLeadId']=$branch_lead_id;
            if($data['type'] == 'lead')
                $changeStatus=$this->saveBranchComments($data);
            else
                $changeStatus=$this->saveBranchContactComments($data);

            if($changeStatus['status']===false){
                return array('error' => 'dberror', 'msg' => $changeStatus['status']);
            }
            //timeline

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }

    function saveContactBranchVisit($data)
    {
        $leadId=$data['lead_id'];
        $userId=$data['user'];
        $branch_id=$data['branch'];
        $fk_course_id=$data['course'];

        $this->db->select('lead_stage_id,ifnull(call_after_days,0) as call_after_days');
        $this->db->from('lead_stage');
        $this->db->where('lead_stage','M5');
        $query_leadStage=$this->db->get();
        $leadStageDetails=$query_leadStage->result();
        foreach($leadStageDetails as $k_leadStage=>$v_leadStage)
        {
            $lead_stage=$v_leadStage->lead_stage_id;
            $next_followup_date=date('Y-m-d H:i:s',strtotime(' +'.$v_leadStage->call_after_days.' day'));
        }
        //$next_followup_date=date('Y-m-d H:i:s',strtotime(' +1 day'));

        $getSequenceNumber=1;
        $this->db->select('IFNULL((MAX(lead_number)+1),0) as lead_number');
        $this->db->from('branch_xref_lead');
        $this->db->where('branch_id',$branch_id);
        $get_max_leadNumber=$this->db->get();
        if($get_max_leadNumber)
        {
            if ($get_max_leadNumber->num_rows() > 0)
            {
                $select_max_leadNumber=$get_max_leadNumber->result();
                foreach($select_max_leadNumber as $k_lead_number=>$v_lead_number)
                {
                    $final_lead_number=$v_lead_number->lead_number;
                    if($final_lead_number!=0)
                    {
                        $getSequenceNumber=0;
                    }
                }
            }
            else
            {
            }
        }
        else
        {
        }
        if($getSequenceNumber==1)
        {
            $final_lead_number=0;
            $this->db->select('sequence_number');
            $this->db->from('branch');
            $this->db->where('branch_id',$branch_id);
            $get_max_leadNumber=$this->db->get();
            if($get_max_leadNumber)
            {
                if ($get_max_leadNumber->num_rows() > 0)
                {
                    $select_max_leadNumber=$get_max_leadNumber->result();
                    foreach($select_max_leadNumber as $k_lead_number=>$v_lead_number)
                    {
                        $final_lead_number=$v_lead_number->sequence_number+1;
                    }
                }
                else
                {
                }
            }
            else
            {
            }
        }
        $fk_company_id=NULL;
        $fk_institution_id=NULL;
        $contactType=NULL;
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value rtv');
        $this->db->JOIN('reference_type rt','rt.reference_type_id=rtv.reference_type_id');
        $this->db->where('rt.name','Contact Type');
        $this->db->where('rtv.value','Retail');
        $getContactType=$this->db->get();
        //$tem=$this->db->last_query();
        if($getContactType)
        {
            if($getContactType->num_rows()>0)
            {
                $getContactTypeRes=$getContactType->result();
                foreach($getContactTypeRes as $kr=>$vr)
                {
                    $contactType=$vr->reference_type_value_id;
                }
            }
        }

        $data_branch_xref_lead_insert = array("lead_id" => $leadId, "branch_id" => $branch_id,"fk_course_id"=>$fk_course_id, "status" => $lead_stage, "created_by" => $_SERVER['HTTP_USER'], "created_on" => date('Y-m-d H:i:s'), "is_active" => 1, "lead_number" => $final_lead_number,"next_followup_date"=>$next_followup_date,"expected_enroll_date"=>$next_followup_date);
        $res_branch_xref_lead_insert=$this->db->insert('branch_xref_lead',$data_branch_xref_lead_insert);

        $branch_xref_lead_id=$this->db->insert_id();
        $data_insert_timeline=array('lead_id'=>$leadId,'branch_xref_lead_id'=>$branch_xref_lead_id,"type"=>"Before Lead Conversion","description"=>"Converted to Branch visitor","created_by"=>$_SERVER['HTTP_USER'],"created_on"=>date('Y-m-d H:i:s'));
        $res_insert_timeline=$this->db->insert('timeline',$data_insert_timeline);


        //branch visitor
        $branch_lead_id = $branch_xref_lead_id;
        $lead_id = $leadId;

        $this->db->select('bxl.branch_xref_lead_id , bxl.lead_id ,l.name');
        $this->db->from('branch_xref_lead bxl');
        $this->db->join('lead l', 'bxl.lead_id=l.lead_id');
        $this->db->where('bxl.lead_id', $leadId);
        $this->db->where('bxl.branch_id', $branch_id);
        $leadData = $this->db->get();
        if ($leadData)
        {
            $leadData = $leadData->result();
            if (count($leadData) > 0)
            {
                $error = false;
            }
            else
            {
                $error = true;
                return 'nolead';
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('u.user_id, e.name');
        $this->db->from('user u');
        $this->db->join('employee e', 'u.user_id=e.user_id');
        $this->db->where('u.user_id', $userId);
        $this->db->where('u.is_active', 1);
        $this->db->where('u.is_seconday_counsellor', 1);
        $user = $this->db->get();
        if ($user)
        {
            $user = $user->result();
            if (count($user) > 0)
            {
                $error = false;
            }
            else
            {
                $error = true;
                return 'nouser';
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }



        $this->db->select('branch_visitor_id');
        $this->db->from('branch_visitor');
        $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
        $this->db->where('status', 1);
        $this->db->where('date(branch_visited_date)="' . date('Y-m-d', strtotime($data['visitedDate'])) . '"');
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                $error = true;
                return 'alreadyDate';
            } else {
                $error = false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if(!$error)
        {
            $this->db->trans_start();
            $data_insert =array(
                'fk_branch_xref_lead_id' =>$branch_lead_id,
                'second_level_counsellor_id' =>$data['user'],
                'created_by' =>$_SERVER['HTTP_USER'],
                'created_date' =>date('Y-m-d H:i:s'),
                'branch_visited_date' =>date('Y-m-d H:i:s',strtotime($data['visitedDate'])),
                'branch_visit_comments' => $data['branchVisitComments'],
                'comment_type'=>'Visited & Positive (M5)'
            );
            $this->db->insert('branch_visitor', $data_insert);


            $data_insert =array(
                'lead_id' =>$lead_id,
                'branch_xref_lead_id' =>$branch_lead_id,
                'type' =>'Allotted Counsellor',
                'description' =>$data['branchVisitComments'].', Allotted '.$leadData[0]->name.' to '.$user[0]->name,
                'created_by' =>$_SERVER['HTTP_USER'],
                'created_on' =>date('Y-m-d H:i:s'),
            );
            $this->db->insert('timeline', $data_insert);
            $data['branchXrefLeadId']=$branch_lead_id;

            $data['hdnBranchVisitId']=0;
            $data['lead_stage']=$lead_stage;
            $data['dateType'] = 'Expected Enrollment Date';

            $changeStatus=$this->saveBranchContactComments($data);
            if($changeStatus['status']===false)
            {
                return array('error' => 'dberror', 'msg' => $changeStatus['status']);
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            } else {
                $this->db->trans_commit();
                return true;
            }
        }

    }
    function getDNDCountConstant(){
        $this->db->select('value');
        $this->db->from('app_configuration_setting');
        $this->db->where('key','DND CONFIG');
        $res=$this->db->get();
        if($res){
            if($res->num_rows()>0){
                $row=$res->result();
                return $row[0]->value;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }
    function saveBranchComments($params){
        $releaseDue=$this->releaseLeadIfExist($params['branchXrefLeadId']);
        $data_xref_lead_update=array();
        if($params['hdnBranchVisitId']==0){
            $comments='';
            $enrollDate='';
            $leadId=NULL;
            $this->db->select('lead_id,logic_item_name,logic_item_count');
            $this->db->from('branch_xref_lead');
            $this->db->where('branch_xref_lead_id', $params['branchXrefLeadId']);

            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();

                if (count($res_all) > 0) {
                    if (isset($res_all[0]->lead_id)) {
                        $maxCount=$this->getDNDCountConstant();
                        $leadDetails = $res_all[0];
                        $logic_item_name = $leadDetails->logic_item_name;
                        $logic_item_count = $leadDetails->logic_item_count;
                        $leadStage = $params['lead_stage'];
                        $leadStagePrefix = substr($leadStage, 0, 2);
                        if ($leadStagePrefix != '' && $leadStagePrefix == 'L_') {
                            $statusExplode = explode('_', $leadStage);
                            if ($leadStage == $logic_item_name) {
                                $logic_item_name = $logic_item_name;
                                $logic_item_count = $logic_item_count + 1;
                                $nextLeadStage = $params['currentLeadStage'];
                            } else {
                                $logic_item_name = $leadStage;
                                $logic_item_count = 1;
                                $nextLeadStage = $params['currentLeadStage'];
                            }

                            if ($maxCount == $logic_item_count) {
                                $logic_item_name = NULL;
                                $logic_item_count = 0;
                                $nextLeadStage = $statusExplode[2];
                            }

                        } else {
                            $nextLeadStage = $leadStage;
                            $logic_item_name = NULL;
                            $logic_item_count = 0;
                        }
                        if ($params['common_date'] != NULL) {
                            if ($params['dateType'] != '' && $params['dateType'] == 'Expected Visited Date'){
                                $data_xref_lead_update['expected_visit_date'] = $params['common_date'];
                            } else if($params['dateType'] != '' && $params['dateType'] == 'Expected Enrollment Date'){
                                $data_xref_lead_update['expected_enroll_date'] = $params['common_date'];
                            } else if($params['dateType'] != '' && $params['dateType'] == 'Expected Admission Date'){
                                $data_xref_lead_update['expected_admission_date'] = $params['common_date'];
                            } else if ($params['dateType'] != '') {
                                $data_xref_lead_update['next_followup_date'] = $params['common_date'];
                            } else {

                            }
                        }
                        $data_xref_lead_update['logic_item_name'] = $logic_item_name;
                        $data_xref_lead_update['logic_item_count'] = $logic_item_count;
                        $data_xref_lead_update['logic_item_name'] = $logic_item_name;
                        if($nextLeadStage!=''){

                            $this->db->select('lead_stage_id,ifnull(call_after_days,0) as call_after_days');
                            $this->db->from('lead_stage');
                            $this->db->where('lead_stage',$nextLeadStage);
                            $query_leadStage=$this->db->get();
                            $leadStageDetails=$query_leadStage->result();
                            $lead_stage = '';
                            foreach($leadStageDetails as $k_leadStage=>$v_leadStage){
                                $lead_stage=$v_leadStage->lead_stage_id;

                            }
                            $getLeadStageId=$lead_stage;
                            $data_xref_lead_update['status']=$getLeadStageId;
                            $data_xref_lead_update['updated_by']=$_SERVER['HTTP_USER'];
                            $data_xref_lead_update['updated_on']=date('Y-m-d H:i:s');
                            $this->db->where('branch_xref_lead_id',$params['branchXrefLeadId']);
                            $res1=$this->db->update('branch_xref_lead',$data_xref_lead_update);

                            if($res1){
                                $db_response=array('status'=>true,'message'=>'Success','data'=>array());
                            }
                            else{
                                $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>array());
                            }
                        }
                        else{
                            $db_response=array('status'=>false,'message'=>'Invalid status','data'=>array());
                        }
                    }
                    else{
                        $db_response=array('status'=>false,'message'=>'Invalid lead details','data'=>array());
                    }
                }
                else{
                    $db_response=array('status'=>false,'message'=>'Invalid lead details','data'=>array());
                }

            }
            else{
                $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>true,'message'=>'Success','data'=>array());
        }


        return $db_response;

    }
    function saveBranchContactComments($params){
        $releaseDue=$this->releaseLeadIfExist($params['branchXrefLeadId']);
        $data_xref_lead_update=array();
        if($params['hdnBranchVisitId']==0){
            $comments='';
            $enrollDate='';
            $leadId=NULL;
            $this->db->select('lead_id,logic_item_name,logic_item_count');
            $this->db->from('branch_xref_lead');
            $this->db->where('branch_xref_lead_id', $params['branchXrefLeadId']);

            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();

                if (count($res_all) > 0) {
                    if (isset($res_all[0]->lead_id)) {
                        $maxCount=$this->getDNDCountConstant();
                        $leadDetails = $res_all[0];
                        $logic_item_name = $leadDetails->logic_item_name;
                        $logic_item_count = $leadDetails->logic_item_count;
                        /*$leadStage = $params['lead_stage'];
                        $leadStagePrefix = substr($leadStage, 0, 2);
                        if ($leadStagePrefix != '' && $leadStagePrefix == 'L_') {
                            $statusExplode = explode('_', $leadStage);
                            if ($leadStage == $logic_item_name) {
                                $logic_item_name = $logic_item_name;
                                $logic_item_count = $logic_item_count + 1;
                                $nextLeadStage = $params['currentLeadStage'];
                            } else {
                                $logic_item_name = $leadStage;
                                $logic_item_count = 1;
                                $nextLeadStage = $params['currentLeadStage'];
                            }

                            if ($maxCount == $logic_item_count) {
                                $logic_item_name = NULL;
                                $logic_item_count = 0;
                                $nextLeadStage = $statusExplode[2];
                            }

                        } else {
                            $nextLeadStage = $leadStage;
                            $logic_item_name = NULL;
                            $logic_item_count = 0;
                        }*/
                        if ($params['common_date'] != NULL) {
                            if ($params['dateType'] != '' && $params['dateType'] == 'Expected Visited Date'){
                                $data_xref_lead_update['expected_visit_date'] = $params['common_date'];
                            } else if($params['dateType'] != '' && $params['dateType'] == 'Expected Enrollment Date'){
                                $data_xref_lead_update['expected_enroll_date'] =  date( 'Y-m-d', strtotime($params['common_date']));
                            } else if($params['dateType'] != '' && $params['dateType'] == 'Expected Admission Date'){
                                $data_xref_lead_update['expected_admission_date'] = $params['common_date'];
                            } else if ($params['dateType'] != '') {
                                $data_xref_lead_update['next_followup_date'] = $params['common_date'];
                            } else {

                            }
                        }
                        /*$data_xref_lead_update['logic_item_name'] = $logic_item_name;
                        $data_xref_lead_update['logic_item_count'] = $logic_item_count;
                        $data_xref_lead_update['logic_item_name'] = $logic_item_name;*/
                        /*if($nextLeadStage!=''){*/
                        $data_xref_lead_update['updated_by']=$_SERVER['HTTP_USER'];
                        $data_xref_lead_update['updated_on']=date('Y-m-d H:i:s');
                        $this->db->where('branch_xref_lead_id',$params['branchXrefLeadId']);
                        $res1=$this->db->update('branch_xref_lead',$data_xref_lead_update);
                        //die($this->db->last_query());

                        if($res1){
                            $db_response=array('status'=>true,'message'=>'Success','data'=>array());
                        }
                        else{
                            $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>array());
                        }
                        /*}
                        else{
                            $db_response=array('status'=>false,'message'=>'Invalid status','data'=>array());
                        }*/
                    }
                    else{
                        $db_response=array('status'=>false,'message'=>'Invalid lead details','data'=>array());
                    }
                }
                else{
                    $db_response=array('status'=>false,'message'=>'Invalid lead details','data'=>array());
                }

            }
            else{
                $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>true,'message'=>'Success','data'=>array());
        }

        return $db_response;

    }
    function releaseLeadIfExist($branchXrefLeadId){

        $this->db->select('fk_lead_call_schedule_head_id,fk_lead_id');
        $this->db->from('lead_user_xref_lead_due');
        $this->db->where('fk_branch_xref_lead_id', $branchXrefLeadId);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if (count($res_all) > 0) {
                //remove from lead_user_xref_lead_history, lead_user_xref_lead_release
                $data_insert =array(
                    'release_count' =>1,
                    'released_by' =>$_SERVER['HTTP_USER'],
                    'released_on' =>date('Y-m-d H:i:s'),
                    'fk_lead_call_schedule_head_id' =>$res_all[0]->fk_lead_call_schedule_head_id
                );
                $this->db->insert('lead_user_xref_lead_release_head', $data_insert);
                $releaseHead=$this->db->insert_id();
                $data_insert1 =array(
                    'fk_user_id' =>$_SERVER['HTTP_USER'],
                    'fk_branch_xref_lead_id' => $branchXrefLeadId,
                    'fk_lead_id' => $res_all[0]->fk_lead_id,
                    'fk_lead_user_xref_lead_release_head_id' => $releaseHead,
                    'fk_lead_call_schedule_head_id' =>$res_all[0]->fk_lead_call_schedule_head_id
                );
                $this->db->insert('lead_user_xref_lead_release', $data_insert1);

                $this->db->where('fk_branch_xref_lead_id',$branchXrefLeadId);
                $this->db->delete('lead_user_xref_lead_due');

            }

        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

    }
    public function getLeadsList($data){

        $table = 'branch_xref_lead';
        // Table's primary key
        $primaryKey = 'branch_xref_lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'bl.lead_number',                  'dt' => 'lead_number',           'field' => 'lead_number' ),
            array( 'db' => 'c.name as course_name',           'dt' => 'course_name',          'field' => 'course_name' ),
            array( 'db' => 'ls.lead_stage as level',              'dt' => 'level',             'field' => 'level' ),
            array( 'db' => 'l.name as lead_name',             'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => 'e1.name AS branch_vist_name',        'dt' => 'branch_vist_name',     'field' => 'branch_vist_name' ),
            array( 'db' => 'bv.created_date',        'dt' => 'created_date',     'field' => 'created_date' ),
            array( 'db' => 'bv.branch_visit_comments',        'dt' => 'branch_visit_comments',     'field' => 'branch_visit_comments' ),
            array( 'db' => 'e.name AS counsellor_name',        'dt' => 'counsellor_name',     'field' => 'counsellor_name' ),
            array( 'db' => 'bv.second_level_counsellor_updated_date',        'dt' => 'second_level_counsellor_updated_date',     'field' => 'second_level_counsellor_updated_date' ),
            array( 'db' => 'bv.second_level_counsellor_comments',        'dt' => 'second_level_counsellor_comments',     'field' => 'second_level_counsellor_comments' ),
            array( 'db' => '`bl`.`created_on`',               'dt' => 'created_on',           'field' => 'created_on', 'formatter' => function( $d, $row ) {
                return date( 'd M,Y', strtotime($d));}),
            array( 'db' => 'l.email',                         'dt' => 'email',                'field' => 'email' ),
            array( 'db' => 'l.phone',                         'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'rtvcity.value as city',           'dt' => 'city',                 'field' => 'city' ),
            array( 'db' => 'rtvcmp.value as company',         'dt' => 'company',              'field' => 'company' ),
            array( 'db' => 'rtvqual.value as qualification',  'dt' => 'qualification',        'field' => 'qualification' ),
            array( 'db' => 'rtvinit.value as institution',    'dt' => 'institution',          'field' => 'institution' ),
            array( 'db' => 'bl.lead_id',                      'dt' => 'lead_id',              'field' => 'lead_id' ),
            array( 'db' => 'bv.is_counsellor_visited',        'dt' => 'is_counsellor_visited',     'field' => 'is_counsellor_visited' ),
            array( 'db' => 'bl.branch_xref_lead_id',          'dt' => 'branch_xref_lead_id',  'field' => 'branch_xref_lead_id' ),
            array( 'db' => 'bv.branch_visitor_id',          'dt' => 'branch_visitor_id',  'field' => 'branch_visitor_id' ),
            array( 'db' => 'if(l.gender=1,1,0) as gender',          'dt' => 'gender',  'field' => 'gender' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name ',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'c.name',    'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'ls.lead_stage', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
            array( 'db' => 'l.email',                         'dt' => 'email',                'field' => 'email' ),
            array( 'db' => 'l.phone',                         'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'rtvqual.value as qualification',  'dt' => 'qualification',        'field' => 'qualification' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from branch_xref_lead bl left join lead l on l.lead_id=bl.lead_id
                       join branch_visitor bv on bv.fk_branch_xref_lead_id = bl.branch_xref_lead_id
                       left join lead_stage ls on  ls.lead_stage_id=bl.status
                       left join employee e on bv.second_level_counsellor_id=e.user_id
                       left join employee e1 on bv.created_by=e1.user_id
                       left join course c on c.course_id=bl.fk_course_id
                       left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
                       left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
                       left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
                       left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id";


        $extraWhere = " bl.branch_id=" . $data['branchId'] . " AND bv.created_by=".$_SERVER['HTTP_USER']." and bv.status=1 ";



        $groupBy = "";


        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id_encode'] = encode($responseData['data'][$i]['branch_xref_lead_id']);
            $responseData['data'][$i]['second_level_counsellor_updated_date']=(new DateTime($responseData['data'][$i]['second_level_counsellor_updated_date']))->format('d M,Y');
            $responseData['data'][$i]['created_date']=(new DateTime($responseData['data'][$i]['created_date']))->format('d M,Y');
        }
        return $responseData;


    }
    function deleteBranchVisit($params){
        $details=$this->getBranchVisitDetails($params);
        if($details['status']===true) {
            $is_counsellor_visited=$details['data'][0]->is_counsellor_visited;
            if($is_counsellor_visited==0){
                $data_update['status'] = 0;//1-active,0-deleted
                $this->db->where('branch_visitor_id', $params['branchVisitId']);
                $res_update = $this->db->update('branch_visitor', $data_update);
                if ($res_update) {
                    $db_response = array('status' => TRUE, 'message' => 'Deleted Successfully', 'data' => array('message', 'Deleted Successfully'));
                } else {
                    $db_response = array('status' => FALSE, 'message' => 'Something Went Wrong! ', 'data' => array('message', 'Something Went Wrong! '));
                }
            }
            else{
                $db_response = array('status' => FALSE, 'message' => "Can't be deleted! Counselling already completed.", 'data' => array('message', 'Something Went Wrong! '));
            }

        }
        else{
            $db_response = array('status' => $details['status'], 'message' => $details['message'], 'data' => array('message', $details['message']));
        }
        return $db_response;
    }
    function getBranchVisitDetails($params){
        $this->db->select('bxl.branch_xref_lead_id,bxl.lead_number,bv.comment_type,bv.is_counsellor_visited');
        $this->db->from('branch_visitor bv');
        $this->db->join('branch_xref_lead bxl','bxl.branch_xref_lead_id=bv.fk_branch_xref_lead_id');
        $this->db->where('branch_visitor_id',$params['branchVisitId']);
        $q=$this->db->get();
        if($q){
            if($q->num_rows()>0){
                $db_response=array('status'=>TRUE,'message'=>'Success! ','data'=>$q->result());
            }
            else{
                $db_response=array('status'=>FALSE,'message'=>'No Details Found! ','data'=>array('message','No Details Found! '));
            }
        }
        else{
            $db_response=array('status'=>FALSE,'message'=>'Something Went Wrong! ','data'=>array('message','Something Went Wrong! '));
        }
        return $db_response;

    }

    function getBranchVisitContactsInformation($val='',$branch_id)
    {
        $query = $this->db->query("SELECT l.lead_id as id,CONCAT_WS(' | ',l.name,l.email,l.phone) as text FROM lead l where (l.name like '%".$val."%' or l.email like '%".$val."%' or l.phone like '%".$val."%') AND l.lead_id NOT IN(SELECT lead_id FROM branch_xref_lead WHERE branch_id = ".$branch_id.")");
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    function getContactLeadSpanshot($lead_id, $branch_id,$hdnBranchVisitId=0)
    {
        $error = false;

        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $branch_id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if(!$error){
            $this->db->select('`l`.`name`, `l`.`phone`, `l`.`email`, l.lead_id, l.fk_branch_id, \'\' as fk_course_id, `b`.`name` `branch_name`');
            $this->db->from('`lead` `l`');
            $this->db->join('branch b', 'b.branch_id=l.fk_branch_id');
            $this->db->where('l.lead_id', $lead_id);
            $this->db->where('l.fk_branch_id', $branch_id);
            $this->db->where('l.status', 1);
            $this->db->order_by('l.lead_id');

            $res_all = $this->db->get();
            //die($this->db->last_query());
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    $data = $res_all[0];
                    return $data;
                } else{
                    return 'nodata';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }
    function getBranchVisitorContactInformation($val='',$branchId = ''){
        $condition = '';
        if(isset($branchId) && $branchId!='')
        {
            $condition=' and bxl.branch_id='.$branchId;
        }
        $query = $this->db->query("select l.lead_id as id,CONCAT_WS(' | ',l.lead_id,l.name,l.email,l.phone) as text, '' as fk_course_id
             from lead l
             where (l.lead_id like '%".$val."%' or l.name like '%".$val."%' or l.email like '%".$val."%' or l.phone like '%".$val."%')");
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }

    /**
     * Function to get Branch Visit for leads/Contacts
     *
     * @return - array
     * @created date - 27 Jan 2017
     * @author : Abhilash
     */
    public function getContactSnapshot($lead_id, $branch_id,$hdnBranchVisitId=0){
        $error = false;

        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $branch_id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if(!$error){
            $this->db->select('`l`.`name`, `l`.`phone`, `l`.`email`, l.lead_id, l.fk_branch_id, \'\' as fk_course_id');
            $this->db->from('`lead` `l`');
            /*$this->db->join('branch b', 'b.branch_id=l.fk_branch_id');*/
            $this->db->where('l.lead_id', $lead_id);
            /*$this->db->where('l.fk_branch_id', $branch_id);*/
            $this->db->where('l.status', 1);
            $this->db->order_by('l.lead_id');

            $res_all = $this->db->get();
            //die($this->db->last_query());
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    $data = $res_all[0];
                    return $data;
                } else{
                    return 'nodata';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }
}