<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @Model Name      user
 * @category        Model
 * @author          Abhilash
 * @Description     For Branches/Branch CRUD Services
 */
class Branch_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    public function getDashboard() {
        $final = [];

        $this->db->select('count(*) as tot');
        $this->db->from('branch');
        $this->db->where('is_active', '1');

        $res_all = $this->db->get();


        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $final['branch'] = $res_all[0]->tot;
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        /* For course */
        $qry = "select 
                    bxc.fk_course_id courseId, count(*) as totalBranchCourse, c.name branchcourseName
                from branch_xref_course bxc
                    left join course c on c.course_id=bxc.fk_course_id
                where bxc.is_active=1
                group by fk_course_id";


        $res_all = $this->db->query($qry);

        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $tmp = [];
                foreach ($res_all as $key => $val) {
                    $tmp[] = array(
                        "courseId" => $val->courseId,
                        "branchcourseName" => $val->branchcourseName,
                        "totalBranchCourse" => $val->totalBranchCourse,
                        "totalStudents" => 0,
                    );
                }
                $final['student'] = $tmp;
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* For Fee */
        /* For students */
        return $final;
    }

    function getBranchHead() {
        $this->db->select("u.user_id, u.login_id, u.is_super_admin, rtv.value roleName, e.name, e.employee_code");
        $this->db->from("user_xref_role uxr");
        $this->db->join("user u", "u.user_id = uxr.fk_user_id");
        $this->db->join("employee e", "u.user_id = e.user_id");
        $this->db->join("reference_type_value rtv", "uxr.fk_role_id=rtv.reference_type_value_id");
        $this->db->join("reference_type rt", "rt.reference_type_id=rtv.reference_type_id");
        $this->db->where("rt.reference_type_id", '9');
        $this->db->where("(rtv.value='superadmin' OR rtv.value='Branch Head' OR rtv.value='Admin' OR rtv.value='Operations and Sales Manager' OR rtv.value='Centre Manager')");
        $branch = $this->db->get();
        if ($branch) {
            $branch = $branch->result();
            if (count($branch) > 0) {
                foreach ($branch as $k => $v) {
                    $userid = $v->user_id;
                    $v->user_id = $v->user_id;
//                    $v->user_id=encode($v->user_id);
                }
                return $branch;
            } else {
                $branch = [];
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    function getBranchUsers($id, $search){
        $user = '';
        $this->db->select(' bxu.user_id ');
        $this->db->from('branch_xref_user bxu');
        $this->db->where('bxu.branch_id',$id);
        $this->db->group_by('bxu.user_id');
        
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                foreach ($res_all as $key => $value) {
                    $user .= $value->user_id.',';
                }
            } else {
                $user = '';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        $user = implode(',',array_filter(explode(',', $user)));

        $this->db->select('e.user_id ,e.name as value, e.email');
        $this->db->from('user u');
        $this->db->join('employee e','u.user_id=e.user_id');
        $this->db->where('u.is_active', 1);
        if($user != ''){
            $this->db->where('u.user_id NOT IN ('.$user.')');
        }
        $this->db->like('e.name', $search);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                return $res_all;
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /* List branch */
    public function getBranchList() {
        $qry = "select b.branch_id id, b.name name from branch b where b.is_active=1 ";

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function getBranchListByUserId($userId) {
        $qry = "select 
            b.branch_id id, b.name name 
        from branch b
        left join branch_xref_user bxu ON b.branch_id=bxu.branch_id
         where b.is_active=1 AND bxu.user_id=".$userId;

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /* -- not used -- */

    public function getAllBranch() {
        $head = '';
        $qry = "select br.branch_id, br.fk_head_id from branch as br where br.fk_head_id<>0";

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $crId = '';
                foreach ($query as $key => $val) {
                    $res = $this->getDetails($val['fk_head_id']);
                    if ($res && $res != 'badrequest' && $res != 'dberror') {
                        $head[$val['fk_head_id']] = $res;
                        if ($crId != $val['fk_head_id']) {
                            $child = array();
                        }
                        $res = $this->getDetails($val['branch_id']);
                        if ($res || $res != 'badrequest' || $res != 'dberror') {
                            $child[] = $res;
                        }
                        $head[$val['fk_head_id']]->child = (object) $child;
                    }
                    $crId = $val['fk_head_id'];
                }
            }

            $qry = "select br.branch_id, br.fk_head_id from branch as br where br.fk_head_id=0";
            $query = $this->db->query($qry);
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    foreach ($query as $key => $val) {
                        if (!isset($head[$val['branch_id']])) {
                            $res = $this->getDetails($val['branch_id']);
                            if ($res && $res != 'badrequest' && $res != 'dberror') {
                                $head[$val['branch_id']] = $this->getDetails($val['branch_id']);
                            }
                        }
                    }
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            return $head;
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    public function getBranch($id) {

        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $id);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $this->setDefaultAccessToSU();  //For SU

                /* For checking the user been disabled for that certain branch */
                $this->getUserSpecificBranch($_SERVER['HTTP_USER']);

                //found id
                $qry = "select 
                            b.branch_id, b.name, b.created_date, b.image, b.phone, b.address, b.zipcode, b.branch_email, b.code bcode, b.description, b.fk_city_id, b.sequence_number, b.batch_sequence, b.is_active AS branch_active,
                            u.user_id, u.login_id, 
                            e.name uname,
                            rtv.reference_type_value_id, rtv.reference_type_id ,rtv.value, rtv.parent_id, rtv.is_active, rtv.code, rtv.reference_type_id, rtv.reference_type_value_id
                          from branch_xref_city as bxc
                            left join branch as b on bxc.fk_branch_id=b.branch_id
                            left join reference_type_value as rtv on bxc.fk_city_id=rtv.reference_type_value_id
                            left join branch_xref_user as bxu on bxu.branch_id = b.branch_id
                            left join user as u on b.fk_head_id = u.user_id
                            left join employee as e on e.user_id=u.user_id
                          where bxc.fk_branch_id=" . $id ." AND bxu.user_id=" . $_SERVER['HTTP_USER'];
                $query = $this->db->query($qry);
                if ($query) {
                    $query = $query->result();
                    if (count($query) > 0) {
                        $final = [];
                        foreach ($query as $key => $val) {

                            /* Code for country */
                            $details = $this->getStateDetails($val->reference_type_value_id);

                            if ($details == 'dberror') {
                                return 'dberror';
                            } else if ($details) {
                                $courses = [];
                                $user = [];
                                /* For Courses */
                                $qry = "select c.name, c.course_id,
                                        (select GROUP_CONCAT(CONCAT(b1.code,'-',b1.batch_id)) from batch b1
                                            LEFT JOIN course c1 ON b1.fk_course_id=c1.course_id
                                            WHERE
                                            b1.fk_branch_id=bxc.fk_branch_id AND b1.fk_course_id=bxc.fk_course_id) AS branch_code,
                                        bxc.fk_branch_id
                                        from branch_xref_course bxc
                                           LEFT JOIN course c on c.course_id=bxc.fk_course_id
                                           LEFT JOIN batch b ON b.fk_course_id=bxc.fk_course_id
                                        where bxc.fk_branch_id=" . $id ." 
                                        GROUP BY c.course_id";
                                $res = $this->db->query($qry);
                                if ($res) {
                                    $res = $res->result();
                                    if (count($res) > 0) {
                                        foreach ($res as $k => $v) {
                                            $branchid=$v->fk_branch_id;
                                            $v->fk_branch_id = encode($v->fk_branch_id);
                                            $v->course_id = $v->course_id;
                                            $amountPayable = 0;
                                            $amountPaid = 0;
                                            $query = $this->db->query("call Sp_Get_Due_List_Report('{$branchid}','{$v->course_id}',null,null,null,null)");
                                            if($query){
                                                if ($query->num_rows() > 0){
                                                    $duelist=$query->result();
                                                    foreach ($duelist as $key => $value) {
                                                        $amountPayable += $value->amountPayable;
                                                        $amountPaid += $value->amountPaid;
                                                    }
                                                } 
                                            } 
                                            $this->db->close();
                                            $this->load->database();
                                            $v->totalAmount = $amountPayable;
                                            $v->amountDue = $amountPayable-$amountPaid;
                                            $v->amountDuePercentage = $amountPaid!=0 || $amountPayable!=0?number_format((float)(($v->amountDue/$amountPayable)*100), 2, '.', '').' %':'0 %';
                                            $qry = "SELECT 
                                                (select count(*) from branch_xref_lead bxl
                                                    JOIN lead l ON bxl.lead_id=l.lead_id
                                                    WHERE
                                                    bxl.is_active=1
                                                    AND bxl.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                                                    AND bxl.branch_id=".$branchid." 
                                                    AND bxl.fk_course_id=".$v->course_id.") AS enrolled ,
                                                (select count(*) from branch_xref_lead bxl
                                                    JOIN lead l ON bxl.lead_id=l.lead_id
                                                    JOIN batch b ON bxl.fk_batch_id=b.batch_id
                                                    WHERE
                                                    bxl.is_active=1 
                                                    AND bxl.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                                                    AND bxl.branch_id=".$branchid." 
                                                    AND bxl.fk_course_id=".$v->course_id.") AS dropout";
                                            $studentDetails = $this->db->query($qry);
                                            if ($studentDetails) {
                                                $studentDetails = $studentDetails->result();
                                                if (count($studentDetails) > 0) {
                                                    $v->enrolled = $studentDetails[0]->enrolled;
                                                    $v->dropout = $studentDetails[0]->dropout;
                                                } else {
                                                    $v->enrolled = '0';
                                                    $v->dropout = '0';
                                                }
                                            } else {
                                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                            }
                                            $v->branch_code = explode(',',$v->branch_code);
//                                            $v->course_id=encode($v->course_id);
                                        }
                                        $courses = $res;
                                    }
                                } else {
                                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                }
                                /* For Active Users */
                                $user = [];
                                $userTemp = [];
                                $qry = "select 
                                            bxu.days, bxu.updated_date alotted_date,
                                            u.login_id email, u.user_id, u.fk_default_branch_id, u.is_active status, 
                                            e.employee_code code, e.name, e.phone, e.image, 
                                            rtv.value as role_name, uxr.*
                                        from branch_xref_user bxu
                                        join user u on u.user_id =bxu.user_id
                                        join employee e on u.user_id =e.user_id
                                        join user_xref_role uxr on u.user_id=uxr.fk_user_id
                                        join reference_type_value rtv on rtv.reference_type_value_id=uxr.fk_role_id
                                        where bxu.branch_id =" . $id . ";";
                                $res = $this->db->query($qry);
                                if ($res) {
                                    $res = $res->result();
                                    if (count($res) > 0) {
                                        foreach ($res as $k => $v) {
                                            $userTemp[$v->user_id] = $v->user_id;
                                            $userid = $v->user_id;
//                                            $v->user_id = encode($v->user_id);
                                            $v->user_id = $v->user_id;
                                            $v->alotted_date = date('d M, Y', strtotime($v->alotted_date));
                                            if ($v->days != 0) {
                                                $addDate = "+" . $v->days . " days";
                                                $future = strtotime($v->alotted_date . $addDate);
                                                $endDate = Date('Y-m-d H:i:s', $future);
                                                $datetime1 = new DateTime(date('Y-m-d H:i:s'));
                                                $datetime2 = new DateTime($endDate);
                                                $interval = $datetime1->diff($datetime2);
                                                $v->daysLeft = str_replace('-', '', str_replace('+', '', $interval->format('%R%a days')));
                                            }
                                        }
                                        $user['active'] = $res;
                                    }
                                } else {
                                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                }
                                if(is_array($userTemp)){
                                    $addtionalQryString = 'AND bxu.user_id NOT IN ('.implode(',', $userTemp).') ';
                                } else {
                                    $addtionalQryString = '';
                                }
                                /* For In-Active Users */
                                $qry = "select 
                                          bxu.branch_id, bxu.user_id, bxu.days, bxu.created_date created_date, bxu.end_date end_date,
                                          u.login_id email, u.user_id, u.fk_default_branch_id, u.is_active status, 
                                          e.employee_code code, e.name, e.phone, e.image, 
                                          rtv.value, uxr.*
                                      from branch_xref_user_history bxu
                                      join user u on u.user_id =bxu.user_id
                                      INNER JOIN (SELECT created_date, MAX(end_date) as TopDate FROM branch_xref_user_history GROUP BY user_id) AS EachItem ON EachItem.TopDate = bxu.end_date 
                                      join employee e on u.user_id =e.user_id
                                      join user_xref_role uxr on u.user_id=uxr.fk_user_id
                                      join reference_type_value rtv on rtv.reference_type_value_id=uxr.fk_role_id
                                      where bxu.branch_id =" . $id . " ". $addtionalQryString ." group by u.user_id order by bxu.end_date desc;";
                                $res = $this->db->query($qry);
                                if ($res) {
                                    $res = $res->result();
                                    if (count($res) > 0) {
                                        foreach ($res as $k => $v) {
//                                            $userid=$v->user_id;
//                                            $v->user_id = encode($v->user_id);
                                            $v->user_id = $v->user_id;
                                            $v->created_date = date('d M, Y', strtotime($v->created_date));
                                            $v->end_date = date('d M, Y', strtotime($v->end_date));
                                            if($v->end_date){
                                                $datetime1 = new DateTime($v->created_date);
                                                $datetime2 = new DateTime($v->end_date);
                                                $interval = $datetime1->diff($datetime2);
                                                $v->inactiveDays = str_replace('-', '', str_replace('+', '', $interval->format('%R%a days')));
                                            }else{
                                                $v->inactiveDays = 0;
                                            }
                                        }
                                        $user['inactive'] = $res;
                                    }
                                } else {
                                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                }
                                /* For Main Branch city */
                                $main = $this->getStateDetails($val->fk_city_id);
                                /* For Primary Branch city */
                                $primary = [];
                                $qry = "select
                                            bxc.* , rtv.*
                                        from branch_xref_city as bxc
                                        left join reference_type_value rtv on rtv.reference_type_value_id=bxc.fk_city_id
                                            where bxc.is_primary=1 AND bxc.is_secondary=0 AND bxc.fk_branch_id=" . $id;
                                $res = $this->db->query($qry);
                                if ($res) {
                                    $res = $res->result();
                                    if (count($res) > 0) {
                                        foreach ($res as $key => $value) {
                                            $primary[] = [
                                                'cityId' => $value->fk_city_id,
                                                'cityName' => $value->value
                                            ];
                                        }
                                    }
                                } else {
                                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                }

                                /* For Secondary Branch city */
                                $secondary = [];
                                $qry = "select
                                            bxc.* , rtv.*
                                        from branch_xref_city as bxc
                                        left join reference_type_value rtv on rtv.reference_type_value_id=bxc.fk_city_id
                                            where bxc.is_primary=0 AND bxc.is_secondary=1 AND bxc.fk_branch_id=" . $id;
                                $res = $this->db->query($qry);
                                if ($res) {
                                    $res = $res->result();
                                    if (count($res) > 0) {
//                                        $secondary = $res[0];
                                        foreach ($res as $key => $value) {
                                            $secondary[] = [
                                                'cityId' => $value->fk_city_id,
                                                'cityName' => $value->value
                                            ];
                                        }
                                    }
                                } else {
                                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                }
//                                var_dump($val);die;
                                $tmp = array(
                                    'branchId' => $val->branch_id,
                                    'branchIdEnc' => encode($val->branch_id),
                                    'branchName' => $val->name,
                                    'branchImage' => $val->image,
                                    'branchCreatedDate' => $val->created_date,
                                    'branchAddress' => $val->address,
                                    'branchZipcode' => $val->zipcode,
                                    'branchPhone' => $val->phone,
                                    'branchEmail' => $val->branch_email !== null ? $val->branch_email : ' -- ',
                                    'branchCode' => $val->bcode,
                                    'userId' => $val->user_id,
                                    'userName' => $val->uname !== null ? $val->uname : ' -- ',
                                    'userEmailId' => $val->login_id,
                                    'userEmailId' => $val->login_id !== null ? $val->login_id : ' -- ',
                                    'description' => $val->description,
                                    'sequence_number' => $val->sequence_number,
                                    'batch_sequence' => $val->batch_sequence,
                                    'branch_active' => $val->branch_active,
                                    'branchHeadId' => $main['id'],
                                    'branchHeadCity' => $main['city'],
                                    'branchHeadState' => $main['state'],
                                    'branchHeadStateId' => $main['stateId'],
                                    'branchHeadCountryId' => $main['countryId'],
                                    'branchHeadCountry' => $main['country'],
                                    'primary' => $primary,
                                    'secondary' => $secondary,
                                    'courses' => $courses,
                                    'user' => $user,
                                );
                                $final = $tmp;
                            } else {
                                return false;
                            }
                        }
                        return $final;
                    } else {
                        return 'badrequest';
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /* -- Not Used -- */

    private function getDetails($id) {
        $qry = "select 
                        b.branch_id, b.name, b.created_date, b.address, b.zipcode, b.branch_email, b.code as code, b.fk_head_id, b.description,
                        u.user_id, u.login_id,
                        rtv.value, rtv.parent_id, rtv.is_active, rtv.code as ref_code, rtv.reference_type_id, rtv.reference_type_value_id
                from branch_xref_city as bxc
                        left join branch as b on bxc.fk_branch_id=b.branch_id
                        left join reference_type_value as rtv on bxc.fk_city_id=rtv.reference_type_value_id
                        left join user as u on b.fk_created_by=u.user_id
                where bxc.fk_branch_id=" . $id . " group by branch_id";
        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $final = [];
                foreach ($query as $key => $val) {
                    /* For Main Branch city */
                    $main = [];
                    $qry = "select
                                bxc.*
                            from branch_xref_city as bxc
                                where bxc.is_primary=0 AND bxc.is_secondary=0 AND bxc.fk_branch_id=" . $id;
                    $res = $this->db->query($qry);
                    if ($res) {
                        $res = $res->result();
                        if (count($res) > 0) {
                            $main = $res[0];
                            $main = $this->getStateDetails($res[0]->fk_city_id);
                            if ($main == 'dberror') {
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }

                    /* For Primary Branch city */
                    $primary = [];
                    $qry = "select
                                bxc.* , rtv.*
                            from branch_xref_city as bxc
                            left join reference_type_value rtv on rtv.reference_type_value_id=bxc.fk_city_id
                                where bxc.is_primary<>0 AND bxc.is_secondary=0 AND bxc.fk_branch_id=" . $id;
                    $res = $this->db->query($qry);
                    if ($res) {
                        $res = $res->result();
                        if (count($res) > 0) {
                            foreach ($res as $key => $value) {
                                $primary[] = [
                                    'cityId' => $value->fk_city_id,
                                    'cityName' => $value->value
                                ];
                            }
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }

                    /* For Secondary Branch city */
                    $secondary = [];
                    $qry = "select
                                bxc.* , rtv.*
                            from branch_xref_city as bxc
                            left join reference_type_value rtv on rtv.reference_type_value_id=bxc.fk_city_id
                                where bxc.is_primary=0 AND bxc.is_secondary<>0 AND bxc.fk_branch_id=" . $id;
                    $res = $this->db->query($qry);
                    if ($res) {
                        $res = $res->result();
                        if (count($res) > 0) {
                            foreach ($res as $key => $value) {
                                $secondary[] = [
                                    'cityId' => $value->fk_city_id,
                                    'cityName' => $value->value
                                ];
                            }
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }

                    $tmp = array(
                        'branchId' => $val['branch_id'],
                        'branchName' => $val['name'],
                        'branchCreatedDate' => $val['created_date'],
                        'branchSddress' => $val['address'],
                        'branchZipcode' => $val['zipcode'],
                        'branchEmail' => $val['branch_email'],
                        'branchCode' => $val['code'],
                        'userId' => $val['user_id'],
                        'userEmailId' => $val['login_id'],
                        'description' => $val['description'],
                        'branchHeadId' => $main['id'],
                        'branchHeadCity' => $main['city'],
                        'branchHeadState' => $main['state'],
                        'branchHeadCountry' => $main['country'],
                        'primary' => $primary,
                        'Secondary' => $secondary
                    );
                    array_push($final, (object) $tmp);
                }
                return $final[0];
            } else {
                return 'badrequest';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    public function addBranch($data) {
        /* For City */
        $valid = false;
        $result = $this->getStateDetails($data['city']);
        if ($result == 'nostate') {
            $valid = false;
            return 'nocity';
        } else if (is_array($result) && isset($result['error'])) {
            $valid = false;
            return $res;
        } else {
            $valid = true;
        }
        /* Main city not in primary city, add it to primary city */
        if (!preg_match("/" . $data['city'] . "/", $data['primaryCity'])) {
            $data['primaryCity'] .= ',' . $data['city'];
        }
        /* For used city as main city */
        $qry = 'select branch_id from branch b where b.fk_city_id=' . $data['city'];
        $response = $this->db->query($qry);
        if ($response) {
            $response = $response->result_array();
            if (count($response) > 0) {
                $valid = false;
                return 'usedCity';
            } else {
                $valid = true;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* if city is already in primary city of other branch */
        $qry = 'select fk_branch_id from branch_xref_city bxc where bxc.is_primary=1 AND bxc.fk_city_id=' . $data['city'];
        $response = $this->db->query($qry);
        if ($response) {
            $response = $response->result_array();
            if (count($response) > 0) {
                $valid = false;
                return 'usedCity';
            } else {
                $valid = true;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* For primaryCity */
        $primaryCity = explode(",", $data['primaryCity']);
        if (is_array($primaryCity) && count($primaryCity) > 0) {
            foreach ($primaryCity as $key => $val) {
                $res = $this->getStateDetails($val);
                if ($res == 'nostate') {
                    $valid = false;
                    return 'noprimaryCity';
                } else if (is_array($res) && isset($res['error'])) {
                    $valid = false;
                    return 'noprimaryCity';
//                    return $res;
                } else {
                    $valid = true;
                }
                /* Primary City check */
                $qry = 'select fk_branch_id from branch_xref_city bxc where bxc.is_primary=1 AND bxc.fk_city_id=' . $val;
                $response = $this->db->query($qry);
                if ($response) {
                    $response = $response->result_array();
                    if (count($response) > 0) {
                        $valid = false;
                        return 'usedPrimary';
                    } else {
                        $valid = true;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }
        }
        /* For secondaryCity */
        $secondaryCity = '';
        if (strlen($data['secondaryCity']) > 0) {
            $secondaryCity = explode(",", $data['secondaryCity']);
            if (is_array($secondaryCity) && count($secondaryCity) > 0) {
                foreach ($secondaryCity as $key => $val) {
                    $res = $this->getStateDetails($val);
                    if ($res == 'nostate') {
                        $valid = false;
                        return 'nosecondaryCity';
                    } else if (is_array($res) && isset($res['error'])) {
                        $valid = true;
                        return 'nosecondaryCity';
                        //                    return $res; /*Modified for Branch Secondary city as its not manatory*/
                    } else {
                        $valid = true;
                    }
                    /* Primary City check in secondary city section */
                    $qry = 'select fk_branch_id from branch_xref_city bxc where bxc.is_primary=1 AND bxc.fk_city_id=' . $val;
                    $response = $this->db->query($qry);
                    if ($response) {
                        $response = $response->result_array();
                        if (count($response) > 0) {
                            $valid = false;
                            return 'usedPrimarySecondary';
                        } else {
                            $valid = true;
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }
            }
        }
        /* For State */
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value');
        $this->db->where('reference_type_value_id', $data['state']);
        $this->db->where('reference_type_id', '2');
        $this->db->where('is_active', '1');
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nostate';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* For Country */
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value');
        $this->db->where('reference_type_value_id', $data['country']);
        $this->db->where('reference_type_id', '1');
        $this->db->where('is_active', '1');
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nocountry';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* For course */
        if (preg_match("/,/", $data['course'])) {
            $course = explode(",", $data['course']);
            if (is_array($course) && count($course) > 0) {
                foreach ($course as $key => $val) {
                    $this->db->select('course_id');
                    $this->db->from('course');
                    $this->db->where('course_id', $val);
                    $query = $this->db->get();
                    if ($query) {
                        $query = $query->result_array();
                        if (count($query) > 0) {
                            $valid = true;
                        } else {
                            $valid = false;
                            return 'nocourse';
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }
            }
        } else {
            $course = $data['course'];
            $this->db->select('course_id');
            $this->db->from('course');
            $this->db->where('course_id', $data['course']);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                } else {
                    $valid = false;
                    return 'nocourse';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        /* For valid user */
        $this->db->select('rtv.value');
        $this->db->from('user u');
        $this->db->where('user_id', $data['branchHead']);
        $this->db->join('user_xref_role uxr', 'uxr.fk_user_id=u.user_id');
        $this->db->join('reference_type_value rtv', 'rtv.reference_type_value_id=uxr.fk_role_id');
        $this->db->join('reference_type rt', 'rtv.reference_type_id=rt.reference_type_id');
        $this->db->where('u.is_active', '1');
        /*        $this->db->where('rtv.reference_type_id', '9'); */
        $user = $this->db->get();
        if ($user) {
            $user = $user->result_array();
            if (count($user) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nouser';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        if ($valid) {
            $this->db->trans_start();
            /* Remove main city from secondary city if exist */
            $qry = 'select fk_branch_id, fk_city_id from branch_xref_city bxc where bxc.is_secondary=1 AND bxc.fk_city_id=' . $data['city'];
            $response = $this->db->query($qry);
            if ($response) {
                $response = $response->result_array();
                if (count($response) > 0) {
                    /* get Details of old branch */
                    $old_branch[$response[0]['fk_branch_id']] = array(
                        'branchId' => $response[0]['fk_branch_id'],
                        'city' => $response[0]['fk_city_id']
                    );
                    /* Delete the main id from secondary list */
                    $this->db->delete('branch_xref_city', array("fk_city_id" => $data['city'], 'is_secondary' => 1));
                    
                    /*For checking and lead present in lead due*/
                    $this->removeDependecyForCityChange($value);
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            /* Remove main city from Secondary City if exist */
            if (strlen($data['primaryCity']) > 0) {
                foreach ($primaryCity as $key => $value) {
                    $qry = 'select fk_branch_id from branch_xref_city bxc where bxc.is_secondary=1 AND bxc.fk_city_id=' . $value;
                    $response = $this->db->query($qry);
                    if ($response) {
                        $response = $response->result_array();
                        if (count($response) > 0) {
                            $old_branch[$response[0]['fk_branch_id']] = array(
                                'branchId' => $response[0]['fk_branch_id'],
                                'city' => $response[0]['fk_city_id']
                            );
                            /* Delete the main id from secondary list */
                            $this->db->delete('branch_xref_city', array("fk_city_id" => $response[0]['fk_city_id'], 'is_secondary' => 1));
                            
                            /*For checking and lead present in lead due*/
                            $this->removeDependecyForCityChange($value);
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }
            }
            if (isset($data['image'])) {
                $data_insert = array(
                    'name' => $data['name'],
                    'address' => $data['address'],
                    'zipcode' => $data['zip'],
                    'branch_email' => $data['email'],
                    'phone' => $data['phone'],
                    'code' => $data['code'],
                    'fk_city_id' => $data['city'],
                    'fk_head_id' => $data['branchHead'],
                    'fk_created_by' => $data['userId'],
                    'created_date' => date('Y-m-d H:i:s'),
                    'description' => $data['desc'],
                    'image' => $data['image'],
                    'sequence_number' => $data['lsequence'],
                    'batch_sequence' => $data['bsequence']
                );
            } else {
                $data_insert = array(
                    'name' => $data['name'],
                    'address' => $data['address'],
                    'zipcode' => $data['zip'],
                    'branch_email' => $data['email'],
                    'phone' => $data['phone'],
                    'code' => $data['code'],
                    'fk_city_id' => $data['city'],
                    'fk_head_id' => $data['branchHead'],
                    'fk_created_by' => $data['userId'],
                    'created_date' => date('Y-m-d H:i:s'),
                    'description' => $data['desc'],
                    'sequence_number' => $data['lsequence'],
                    'batch_sequence' => $data['bsequence']
                );
            }

            $res_insert = $this->db->insert('branch', $data_insert);

            if ($this->db->error()['code'] == '0') {
                if ($res_insert) {
                    $last_updated_id = $this->db->insert_id();
                    $data_insert1 = array(
                        'fk_branch_id' => $last_updated_id,
                        'fk_city_id' => $data['city']
                    );
                    $this->db->delete('branch_xref_city', $data_insert1);

                    foreach ($primaryCity as $key => $value) {
                        $data_insert2 = array(
                            'fk_branch_id' => $last_updated_id,
                            'fk_city_id' => $value,
                            'is_primary' => 1,
                            'is_secondary' => 0,
                        );
                        $this->db->insert('branch_xref_city', $data_insert2);
                    }
                    if (strlen($data['secondaryCity']) > 0) {
                        foreach ($secondaryCity as $key => $value) {
                            $data_insert3 = array(
                                'fk_branch_id' => $last_updated_id,
                                'fk_city_id' => $value,
                                'is_primary' => 0,
                                'is_secondary' => 1,
                            );
                            $this->db->insert('branch_xref_city', $data_insert3);
                        }
                    }
                    if (preg_match("/,/", $data['course'])) {
                        foreach ($course as $key => $value) {
                            $data_insert4 = array(
                                'fk_branch_id' => $last_updated_id,
                                'fk_course_id' => $value,
                                'is_active' => 1,
                            );
                            $this->db->insert('branch_xref_course', $data_insert4);
                        }
                    } else {
                        $data_insert4 = array(
                            'fk_branch_id' => $last_updated_id,
                            'fk_course_id' => $data['course'],
                            'is_active' => 1,
                        );
                        $this->db->insert('branch_xref_course', $data_insert4);
                    }
                    /* For User */
                    if (strtolower($user[0]['value']) != 'superadmin') {
//                        $this->db->where('branch_id', $last_updated_id);
                        $days = 0;  /* Remove with the new UI for branch table, may be loop the function as above mentioned */
                        $data_insert4 = array(
                            'branch_id' => $last_updated_id,
                            'user_id' => $data['branchHead'],
                            'days' => $days,
                            'updated_date' => date('Y-m-d H:i:s'),
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('branch_xref_user', $data_insert4);
                    }

                    $this->setDefaultAccessToSU();  /* For SU */

                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    } else {
                        $this->db->trans_commit();
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }

    public function editBranch($id, $data) {

        $valid = false;
        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (!$res_all) {
                $valid = false;
                return 'badrequest';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* Duplcicate Branch Code */
        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id!=', $id);
        $this->db->where('code', $data['code']);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $valid = false;
                return 'branchcode';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* Main city not in primary city, add it to primary city */
        if (!preg_match("/" . $data['city'] . "/", $data['primaryCity'])) {
            $data['primaryCity'] .= ', ' . $data['city'];
        }
        /* For used city as main city */
        $qry = 'select branch_id from branch b where b.fk_city_id=' . $data['city'] . ' AND b.branch_id!=' . $id;
        $response = $this->db->query($qry);
        if ($response) {
            $response = $response->result_array();
            if (count($response) > 0) {
                $valid = false;
                return 'usedCity';
            } else {
                $valid = true;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* if city is already in primary city of other branch */
        $qry = 'select fk_branch_id from branch_xref_city bxc where bxc.is_primary=1 AND bxc.fk_city_id=' . $data['city'] . ' AND bxc.fk_branch_id!=' . $id;
        $response = $this->db->query($qry);
        if ($response) {
            $response = $response->result_array();
            if (count($response) > 0) {
                $valid = false;
                return 'usedCity';
            } else {
                $valid = true;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* For primaryCity */
        $primaryCity = explode(",", $data['primaryCity']);
        if (is_array($primaryCity) && count($primaryCity) > 0) {
            foreach ($primaryCity as $key => $val) {
                $res = $this->getStateDetails($val);
                if ($res == 'nostate') {
                    $valid = false;
                    return 'noprimaryCity';
                } else if (is_array($res) && isset($res['error'])) {
                    $valid = false;
                    return 'noprimaryCity';
//                    return $res;
                } else {
                    $valid = true;
                }
                /* Primary City check */
                $qry = 'select fk_branch_id from branch_xref_city bxc where bxc.is_primary=1 AND bxc.fk_city_id=' . $val . ' AND bxc.fk_branch_id!=' . $id;
                $response = $this->db->query($qry);
                if ($response) {
                    $response = $response->result_array();
                    if (count($response) > 0) {
                        $valid = false;
                        return 'usedPrimary';
                    } else {
                        $valid = true;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }
        }
        /* For secondaryCity */
        $secondaryCity = '';
        if (strlen($data['secondaryCity']) > 0) {
            $secondaryCity = explode(",", $data['secondaryCity']);
            if (is_array($secondaryCity) && count($secondaryCity) > 0) {
                foreach ($secondaryCity as $key => $val) {
                    $res = $this->getStateDetails($val);
                    if ($res == 'nostate') {
                        $valid = false;
                        return 'nosecondaryCity';
                    } else if (is_array($res) && isset($res['error'])) {
                        $valid = true;
                        return 'nosecondaryCity';
                        //                    return $res; /*Modified for Branch Secondary city as its not manatory*/
                    } else {
                        $valid = true;
                    }
                    /* Primary City check in secondary city section */
                    $qry = 'select fk_branch_id from branch_xref_city bxc where bxc.is_primary=1 AND bxc.fk_city_id=' . $val;
                    $response = $this->db->query($qry);
                    if ($response) {
                        $response = $response->result_array();
                        if (count($response) > 0) {
                            $valid = false;
                            return 'usedPrimarySecondary';
                        } else {
                            $valid = true;
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }
            }
        }
        /* For State */
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value');
        $this->db->where('reference_type_value_id', $data['state']);
        $this->db->where('reference_type_id', '2');
        $this->db->where('is_active', '1');
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nostate';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* For Country */
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value');
        $this->db->where('reference_type_value_id', $data['country']);
        $this->db->where('reference_type_id', '1');
        $this->db->where('is_active', '1');
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nocountry';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* For course */
        $course = explode(",", $data['course']);
        if (is_array($course) && count($course) > 0) {
            foreach ($course as $key => $val) {
                $this->db->select('*');
                $this->db->from('course');
                $this->db->where('course_id', $val);
                $query = $this->db->get();
                if ($query) {
                    $query = $query->result_array();
                    if (count($query) > 0) {
                        $valid = true;
                    } else {
                        $valid = false;
                        return 'nocourse';
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }
        }

        /* For valid user */
        $this->db->select('rtv.value');
        $this->db->from('user u');
        $this->db->where('user_id', $data['branchHead']);
        $this->db->join('user_xref_role uxr', 'uxr.fk_user_id=u.user_id');
        $this->db->join('reference_type_value rtv', 'rtv.reference_type_value_id=uxr.fk_role_id');
        $this->db->join('reference_type rt', 'rtv.reference_type_id=rt.reference_type_id');
        $this->db->where('u.is_active', '1');
        /*        $this->db->where('rtv.reference_type_id', '9'); */
        $user = $this->db->get();
        if ($user) {
            $user = $user->result_array();
            if (count($user) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nouser';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('sequence_number, batch_sequence');
        $this->db->from('branch');
        $this->db->where('branch_id', $id);
        $branchDetails = $this->db->get();
        if ($branchDetails) {
            $branchDetails = $branchDetails->result_array();
            if (count($branchDetails) > 0) {
                $branchDetails = $branchDetails[0];
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
//        print_r($branchDetails);die;
        /*For Lead Number*/
        if($branchDetails['sequence_number'] != $data['lsequence']){
            $this->db->select('branch_xref_lead_id');
            $this->db->from('branch_xref_lead');
            //        $this->db->where('lead_number', $data['lsequence']+1);
            $this->db->where('branch_id', $id);
            //        $this->db->where('is_active', 1);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {

                    $valid = false;
                    return 'usedsequence';
                } else {
                    $valid = true;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }

        /*For Batch Sequence Number*/
        if($branchDetails['batch_sequence'] != $data['bsequence']){
            $this->db->select('batch_id');
            $this->db->from('batch');
            //        $this->db->where('code', $data['bsequence']+1);
            $this->db->where('fk_branch_id', $id);
            $this->db->where('is_active', 1);
            $query = $this->db->get();
            //        die($this->db->last_query());
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {

                    $valid = false;
                    return 'usedbatchsequence';
                } else {
                    $valid = true;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }

        if ($valid) {
            /**/
            $cityArray = [];
            /*Get Id of city which is new*/
            $this->db->select('fk_city_id');
            $this->db->from('branch_xref_city');
            $this->db->where('fk_branch_id',$id);
            $query = $this->db->get();
//            die($this->db->last_query());
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    foreach ($query as $key => $value) {
                        array_push($cityArray,$value['fk_city_id']);
                    }
                } else {
                    array_push($cityArray,$query[0]['fk_city_id']);
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            //city
            $this->db->where('fk_branch_id', $id);
            $res_update = $this->db->delete('branch_xref_city');
            //course
            $this->db->where('fk_branch_id', $id);
            $res_update = $this->db->update('branch_xref_course', array("is_active" => 0));

            if ($this->db->error()['code'] == '0') {
                if ($res_update) {
                    $tempCity = [];
                    if (isset($data['image'])) {
                        $data_update = array(
                            'name' => $data['name'],
                            'address' => $data['address'], 
                            'zipcode' => $data['zip'],
                            'branch_email' => $data['email'],
                            'phone' => $data['phone'],
                            'code' => $data['code'],
                            'fk_city_id' => $data['city'],
                            'fk_head_id' => $data['branchHead'],
                            'fk_created_by' => $data['userId'],
                            'created_date' => date('Y-m-d H:i:s'),
                            'description' => $data['desc'],
                            'image' => $data['image'],
                            'sequence_number' => $data['lsequence'],
                            'batch_sequence' => $data['bsequence'],
                        );
                    } else {
                        $data_update = array(
                            'name' => $data['name'],
                            'address' => $data['address'],
                            'zipcode' => $data['zip'],
                            'branch_email' => $data['email'],
                            'phone' => $data['phone'],
                            'code' => $data['code'],
                            'fk_city_id' => $data['city'],
                            'fk_head_id' => $data['branchHead'],
                            'fk_created_by' => $data['userId'],
                            'created_date' => date('Y-m-d H:i:s'),
                            'description' => $data['desc'],
                            'sequence_number' => $data['lsequence'],
                            'batch_sequence' => $data['bsequence'],
                        );
                    }

                    $this->db->trans_begin();
                    $this->db->where('branch_id', $id);
                    $res_update = $this->db->update('branch', $data_update);
                    /**/

                    /* $data_update1 = array(
                      'fk_branch_id' => $id,
                      'fk_city_id' => $data['city'],
                      'is_primary' => 0,
                      'is_secondary' => 0
                      );
                      $res_update = $this->db->insert('branch_xref_city', $data_update1); */

                    /* $data_update1 = array(
                      'is_primary' => 0,
                      'is_secondary' => 0,
                      ); */
                    $this->db->delete('branch_xref_city', array("fk_branch_id" => $id));
                    foreach ($primaryCity as $key => $value) {
                        $data_insert2 = array(
                            'fk_branch_id' => $id,
                            'fk_city_id' => $value,
                            'is_primary' => 1,
                            'is_secondary' => 0,
                        );
//                        array_push($tempCity, array('fk_city_id'=>$value));
                        array_push($tempCity, $value);
                        $this->db->insert('branch_xref_city', $data_insert2);
                    }
                    if (strlen($data['secondaryCity']) > 0) {
                        foreach ($secondaryCity as $key => $value) {
                            $data_insert3 = array(
                                'fk_branch_id' => $id,
                                'fk_city_id' => $value,
                                'is_primary' => 0,
                                'is_secondary' => 1,
                            );
//                            array_push($tempCity, array('fk_city_id'=>$value));
                            array_push($tempCity, $value);
                            $this->db->insert('branch_xref_city', $data_insert3);
                        }
                    }
                    /*For Contact release functionality*/
                    if(is_array($tempCity) && is_array($cityArray)){
                        $diffArray = array_diff($tempCity, $cityArray);
                        foreach ($diffArray as $key => $value) {
                            /*For checking and lead present in lead due*/
                            $this->removeDependecyForCityChange($value);
                        }
                    } else if(is_array($tempCity)){
                        if(($key = array_search($cityArray, $tempCity)) !== false) {
                            unset($tempCity[$key]);
                            $diffArray = $tempCity;
                            foreach ($diffArray as $key => $value) {
                                /*For checking and lead present in lead due*/
                                $this->removeDependecyForCityChange($value);
                            }
                        }
                    } else if(is_array($cityArray)){
                        if(($key = array_search($tempCity, $cityArray)) !== false) {
                            unset($cityArray[$key]);
                            $diffArray = $cityArray;
                            foreach ($diffArray as $key => $value) {
                                /*For checking and lead present in lead due*/
                                $this->removeDependecyForCityChange($value);
                            }
                        }
                    }
                    
                    $this->db->delete('branch_xref_course', array("fk_branch_id" => $id));
                    if (preg_match("/,/", $data['course'])) {
                        foreach ($course as $key => $value) {
                            $data_insert4 = array(
                                "fk_course_id" => $value,
                                "fk_branch_id" => $id,
                                'is_active' => 1,
                            );
                            $this->db->insert('branch_xref_course', $data_insert4);
                        }
                    } else {
                        $data_insert4 = array(
                            "fk_course_id" => $data['course'],
                            "fk_branch_id" => $id,
                            'is_active' => 1,
                        );
                        $this->db->insert('branch_xref_course', $data_insert4);
                    }
                    $this->setDefaultAccessToSU();  //For SU	
                    /* For User */
                    if (strtolower($user[0]['value']) != 'superadmin') {
                        /*Deleted User */
                        $this->db->where('branch_id', $id);
                        $this->db->where('user_id', $data['branchHead']);
                        $this->db->delete('branch_xref_user');
                        
                        $days = 0;  /* Remove with the new UI for branch table, may be loop the function as above mentioned */
                        $data_insert4 = array(
                            'branch_id' => $id,
                            'user_id' => $data['branchHead'],
                            'days' => $days,
                            'updated_date' => date('Y-m-d H:i:s'),
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('branch_xref_user', $data_insert4);
                    }
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    } else {
                        $this->db->trans_commit();
                        return true;
                    }
                    /**/
                } else {
                    return false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            /**/
        }
    }

    public function changeStatus($id) {
        $this->db->select('*');
        $this->db->from('branch');
        $this->db->where('branch_id', $id);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if ($res_all) {

                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );

                $this->db->where('branch_id', $id);
                $res_update = $this->db->update('branch', $data_update);

                if ($this->db->error()['code'] == '0') {
                    if ($res_update) {
                        if ($is_active) {
                            $data_update = array(
                                'is_active' => 1
                            );
                            $this->db->where('fk_branch_id', $id);
                            $this->db->update('branch_xref_course', $data_update);
                            return 'active';
                        } else {
                            $data_update = array(
                                'is_active' => 0
                            );
                            $this->db->where('fk_branch_id', $id);
                            $this->db->update('branch_xref_course', $data_update);
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    public function changeCourseStatus($branch_id, $course_id) {
        $error = true;

        $this->db->select('*');
        $this->db->from('branch');
        $this->db->where('branch_id', $branch_id);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('course_id', $course_id);
//        $this->db->where('is_active', 1);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                if($res_all[0]->is_active == 0){
                    $error = true;
                    return 'courseInactive';
                }else{
                    $error = false;
                }
            } else {
                $error = true;
                return 'nocourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('*');
        $this->db->from('branch_xref_course');
        $this->db->where('fk_branch_id', $branch_id);
        $this->db->where('fk_course_id', $course_id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranchcourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if (!$error) {
            $is_active = '';
            $is_active = !$res_all[0]->is_active;
            $data_update = array(
                'is_active' => $is_active
            );
            $this->db->where('fk_branch_id', $branch_id);
            $this->db->where('fk_course_id', $course_id);
            $res_update = $this->db->update('branch_xref_course', $data_update);
            
            $this->db->select('fk_branch_id');
            $this->db->from('branch_xref_course');
            $this->db->where('fk_branch_id', $branch_id);
//            $this->db->where('fk_course_id', $course_id);
            $this->db->where('is_active', 1);
            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) >= 1) {
                    if ($is_active) {
                        $data_update = array(
                            'is_active' => 1
                        );
                        $this->db->where('branch_id', $branch_id);
                        $this->db->update('branch', $data_update);
                    } 
                } else {
                    if (!$is_active) {
                        $data_update = array(
                            'is_active' => 0
                        );
                        $this->db->where('branch_id', $branch_id);
                        $this->db->update('branch', $data_update);
                    } 
                }
                if ($res_update) {
                    return 'active';
                } else {
                    return 'deactivated';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            
        }
    }

    public function extendUser($data, $branch_id){
        $userid = $data['userid'];
        $branch_id = $branch_id;

        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $branch_id);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('user_id');
        $this->db->from('user');
        $this->db->where('user_id', $userid);
//        $this->db->where('is_active', 1);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nouser';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if(!$error){
            $this->db->select('*');
            $this->db->from('branch_xref_user');
            $this->db->where('branch_id', $branch_id);
            $this->db->where('user_id', $userid);

            $res_all = $this->db->get();

            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    //update
                    $days = $res_all[0]->days;
                    $data_update = array(
                        'days' => $data['days'],
                        'updated_date' => Date('Y-m-d H:i:s'),
                    );
                    $this->db->where('branch_id', $branch_id);
                    $this->db->where('user_id', $userid);
                    $res_update = $this->db->update('branch_xref_user', $data_update);
                    if($res_update){
                        return true;
                    }
                } else {
                    //insert
                    $data_insert = array(
                        'branch_id' => $branch_id,
                        'user_id' => $userid,
                        'days' => $data['days'],
                        'updated_date' => date('Y-m-d H:i:s'),
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    $is_inserted = $this->db->insert('branch_xref_user', $data_insert);
                    if($is_inserted){
                        return true;
                    }
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }

    public function deactivateUser($branch_id, $userid){
        $branch_id = $branch_id;
        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $branch_id);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('user_id');
        $this->db->from('user');
        $this->db->where('user_id', $userid);
//        $this->db->where('is_active', 1);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nouser';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if(!$error){

            $this->db->select('branch_id, created_date, updated_date, days, user_id');
            $this->db->from('branch_xref_user');
            $this->db->where('branch_id', $branch_id);
            $this->db->where('user_id', $userid);

            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    $this->db->delete('branch_xref_user', array('branch_id' => $branch_id, 'user_id' => $userid));
                    if(count($res_all) > 0){
                        $this->db->insert('branch_xref_user_history', array('branch_id' => $branch_id,
                            'user_id' => $res_all[0]->user_id,
                            'created_date' => $res_all[0]->updated_date,
                            'days' => $res_all[0]->days,
                            'end_date' => date('Y-m-d H:i:s')
                        ));
                    } else {
                        $this->db->insert('branch_xref_user_history', array('branch_id' => $branch_id,
                            'user_id' => $userid,
                            'days' => 0,
                            'end_date' => date('Y-m-d H:i:s')
                        ));
                    }
                    return true;
                } else {
                    $this->db->insert('branch_xref_user_history', array('branch_id' => $branch_id,
                        'user_id' => $userid,
                        'days' => 0,
                        'end_date' => date('Y-m-d H:i:s')
                    ));
                    return true;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }

    /**
     * Function for adding acces to branch for all SU
     * @return boolean
     */
    public function setDefaultAccessToSU() {
        /* Getting user role and SU User */
        $qry = "SELECT 
		u.user_id
	FROM user u
		JOIN user_xref_role uxr on uxr.fk_user_id=u.user_id
		JOIN reference_type_value rtv on rtv.reference_type_value_id=uxr.fk_role_id
		JOIN reference_type rt on rt.reference_type_id=rtv.reference_type_id
	WHERE
		rt.name='User Role' 
		AND rtv.value='Superadmin';";
        $response = $this->db->query($qry);
        if ($response) {
            $response = $response->result_array();
            if (count($response) > 0) {
                $this->db->trans_start();
                foreach ($response as $k => $v) {
                    $qry = "SELECT  b.branch_id FROM branch b WHERE b.is_active=1";
                    $branch = $this->db->query($qry);
                    if ($branch) {
                        $branch = $branch->result_array();
                        if (count($branch) > 0) {
                            foreach ($branch as $key => $value) {
                                $qry = "SELECT b.user_id FROM branch_xref_user b WHERE b.user_id=".$v['user_id']." AND b.branch_id=".$value['branch_id'];
                                $user = $this->db->query($qry);
                                if ($user) {
                                    $user = $user->result_array();
                                    if (count($user) <= 0) {
                                        $data_insert = array(
                                            'branch_id' => $value['branch_id'],
                                            'user_id' => $v['user_id'],
                                            'days' => 0, /* For unlimited access */
                                            'updated_date' => date('Y-m-d H:i:s'),
                                            'created_date' => date('Y-m-d H:i:s')
                                        );
                                        $is_del = $this->db->delete('branch_xref_user', ['branch_id' => $value['branch_id'], 'user_id' => $v['user_id']]);
                                        $is_inserted = $this->db->insert('branch_xref_user', $data_insert);
                                    }
                                }
                            }
                        }
                    }
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            }
        } else {

        }
    }

    public function getStateDetails($data) {
        $this->db->select('value as city, parent_id, reference_type_value_id as id');
        $this->db->from('reference_type_value');
        $this->db->where('reference_type_value_id', $data);
        $query = $this->db->get();

        if ($query) {
            $tmp = $query->result_array();
            if ($tmp) {
                $this->db->select('value as state, parent_id, reference_type_value_id as stateId');
                $this->db->from('reference_type_value');
                $this->db->where('reference_type_value_id', $tmp[0]['parent_id']);
                $temp = $query = $this->db->get();

                if ($query) {
                    $temp = $temp->result_array();
                    if ($temp) {
                        $this->db->select('value as country, reference_type_value_id as countryId');
                        $this->db->from('reference_type_value');
                        $this->db->where('reference_type_value_id', $temp[0]['parent_id']);
                        $query = $this->db->get();

                        if ($query) {
                            $temp1 = $query->result_array();
                            if ($temp1) {
                                $res_all = array(
                                    'parent_id' => $tmp[0]['parent_id'],
                                    'id' => $tmp[0]['id'],
                                    'city' => $tmp[0]['city'],
                                    'stateId' => $temp[0]['stateId'],
                                    'state' => $temp[0]['state'],
                                    'countryId' => $temp1[0]['countryId'],
                                    'country' => $temp1[0]['country'],
                                );
                                return $res_all;
                            } else {
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                        } else {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nostate';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    public function getUserSpecificBranch($id) {
        $qry = "select 
                    b.branch_id, b.name , ubx.*
                from branch_xref_user as ubx left join branch as b on ubx.branch_id=b.branch_id";
        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            $branch = [];
            if (count($query) > 0) {
                foreach ($query as $key => $val) {
                    if ($val['days'] != 0) {
                        $addDate = "+" . $val['days'] . " days";
                        $timestamp = strtotime($val['updated_date'] . $addDate);
                        $currenttime = strtotime(date('Y-m-d H:i:s'));
                        if ($timestamp < $currenttime && $val['days'] > 0) {
                            //exceeds; then remove user
                            $this->db->delete('branch_xref_user', array('branch_id' => $val['branch_id'], 'user_id' => $val['user_id']));
                            $this->db->insert('branch_xref_user_history', array('branch_id' => $val['branch_id'],
                                'user_id' => $val['user_id'],
                                'created_date' => $val['updated_date'],
                                'days' => $val['days'],
                                'end_date' => date('Y-m-d H:i:s')
                            ));
                        }
                    }
                }
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        //return data
        $qry = "select 
                    b.branch_id, b.name , ubx.*
                from branch_xref_user as ubx left join branch as b on ubx.branch_id=b.branch_id
                where 
                    ubx.user_id = '" . $id . "'";
        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            $branch = [];
            if (count($query) > 0) {
                /*foreach ($query as $key => $val) {

                    $addDate = "+" . $val['days'] . " days";
                    $timestamp = strtotime($val['created_date'] . $addDate);
                    $currenttime = strtotime(date('Y-m-d H:i:s'));
                    if ($timestamp > $currenttime && $val['days'] == 0) {
                        //we have time
                        $branch[$val['branch_id']] = $query[$key];
                    } else {
                        //exceeds
                        if ($val['days'] != 0) {
                            $this->db->delete('branch_xref_user', array('branch_id' => $val['branch_id'], 'user_id' => $id));
                            $this->db->insert('branch_xref_user_history', array('branch_id' => $val['branch_id'],
                                'user_id' => $val['user_id'],
                                'created_date' => $val['created_date'],
                                'days' => $val['days'],
                                'end_date' => date('Y-m-d H:i:s')
                            ));
                        }
                    }
                }*/
                return $query;
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    public function getBranchDataTableDetails($user_id) {
        $this->setDefaultAccessToSU();
        $table = 'branch';
        // Table's primary key
        $primaryKey = 'b`.`branch_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array('db' => '`b`.`code` as branch_code', 'dt' => 'branch_code', 'field' => 'branch_code'),
            array('db' => '`c`.`name` as course_name', 'dt' => 'course_name', 'field' => 'course_name'),
            array('db' => '(select ba.acedemic_startdate
                            from batch ba 
                            where ba.is_active=1 
                            AND (ba.marketing_startdate<=DATE(NOW()) AND ba.marketing_enddate>=DATE(NOW()))
                            AND ba.fk_branch_id=b.branch_id 
                            AND ba.fk_course_id=c.course_id limit 0,1) as current_acadamics_start', 'dt' => 'current_acadamics_start', 'field' => 'current_acadamics_start'),
            array('db' => '(select ba.marketing_enddate
                            from batch ba 
                            where ba.is_active=1 
                            AND (ba.marketing_startdate<=DATE(NOW()) AND ba.marketing_enddate>=DATE(NOW()))
                            AND ba.fk_branch_id=b.branch_id 
                            AND ba.fk_course_id=c.course_id limit 0,1) as current_marketing_end', 'dt' => 'current_marketing_end', 'field' => 'current_marketing_end'),
            array('db' => '(select count(bxl1.branch_xref_lead_id) from branch_xref_lead bxl1 where 
                            bxl1.is_active=1 
                            AND bxl1.`status`=(select ls1.lead_stage_id from lead_stage ls1 where ls1.lead_stage=\'M7\')
                            AND bxl1.branch_id=b.branch_id 
                            AND bxl1.fk_course_id=c.course_id limit 0,1) as current_enrolled_students', 'dt' => 'current_enrolled_students', 'field' => 'current_enrolled_students'),
            array('db' => '(select ba.acedemic_startdate
                            from batch ba 
                            where ba.is_active=1 
                            AND (ba.marketing_startdate>DATE(NOW()) AND ba.marketing_enddate>DATE(NOW()))
                            AND ba.fk_branch_id=b.branch_id 
                            AND ba.fk_course_id=c.course_id limit 0,1) as next_acadamics_start', 'dt' => 'next_acadamics_start', 'field' => 'next_acadamics_start'),
            array('db' => '(select ba.marketing_enddate
                            from batch ba 
                            where ba.is_active=1 
                            AND (ba.marketing_startdate>DATE(NOW()) AND ba.marketing_enddate>DATE(NOW()))
                            AND ba.fk_branch_id=b.branch_id 
                            AND ba.fk_course_id=c.course_id limit 0,1) as next_marketing_start', 'dt' => 'next_marketing_start', 'field' => 'next_marketing_start'),
            array('db' => '(select count(bxl1.branch_xref_lead_id) 
                            from branch_xref_lead bxl1,lead_stage ls
                            where bxl1.is_active=1
                                AND ls.lead_stage_id= bxl1.status AND ls.lead_stage IN ("M7")
                                AND bxl1.branch_id=b.branch_id 
                                AND bxl1.fk_course_id=c.course_id limit 0,1) as all_students', 'dt' => 'all_students', 'field' => 'all_students'),
            array('db' => 'if(`b`.`is_active`=1,1,0) as branch_status', 'dt' => 'branch_status', 'field' => 'branch_status'),
            array('db' => 'if(`bxc`.`is_active`=1,1,0) as branch_course_status', 'dt' => 'branch_course_status', 'field' => 'branch_course_status'),
            array('db' => 'if(`c`.`is_active`=1,1,0) as course_status', 'dt' => 'course_status', 'field' => 'course_status'),
            array('db' => '`b`.`name` as branch_name', 'dt' => 'branch_name', 'field' => 'branch_name'),
            array('db' => '`b`.`branch_id`', 'dt' => 'branch_id', 'field' => 'branch_id'),
            array('db' => '`c`.`course_id`', 'dt' => 'course_id', 'field' => 'course_id')
        );

        $globalFilterColumns = array(
            array('db' => '`b`.`code`', 'dt' => 'code', 'field' => 'code'),
            array('db' => '`b`.`name`', 'dt' => 'name', 'field' => 'name'),
            array('db' => '`c`.`name`', 'dt' => 'course_name', 'field' => 'course_name')
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db' => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /*         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM branch b JOIN branch_xref_course bxc ON bxc.fk_branch_id=b.branch_id JOIN course c ON c.course_id=bxc.fk_course_id JOIN branch_xref_user bxu ON bxu.branch_id=b.branch_id ";
        $extraWhere = "`bxu`.`user_id` = " . $user_id;
        $groupBy = "";
        $responseData = (SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere, $groupBy,"course_name"));
        for ($i = 0; $i < count($responseData['data']); $i++) {
            $responseData['data'][$i]['branch_id'] = encode($responseData['data'][$i]['branch_id']);
            $responseData['data'][$i]['course_id'] = encode($responseData['data'][$i]['course_id']);
            if($responseData['data'][$i]['next_acadamics_start'] >0){
                $responseData['data'][$i]['next_acadamics_start'] = (new DateTime($responseData['data'][$i]['next_acadamics_start']))->format('d M,Y');
            }else {
                $responseData['data'][$i]['next_acadamics_start'] = '----';
            }
            if($responseData['data'][$i]['next_marketing_start'] >0){
                $responseData['data'][$i]['next_marketing_start'] = (new DateTime($responseData['data'][$i]['next_marketing_start']))->format('d M,Y');
            }else {
                $responseData['data'][$i]['next_marketing_start'] = '----';
            }
            if($responseData['data'][$i]['current_acadamics_start'] >0){
                $responseData['data'][$i]['current_acadamics_start'] = (new DateTime($responseData['data'][$i]['current_acadamics_start']))->format('d M,Y');
            }else {
                $responseData['data'][$i]['current_acadamics_start'] = '----';
            }
            if($responseData['data'][$i]['current_marketing_end'] >0){
                $responseData['data'][$i]['current_marketing_end'] = (new DateTime($responseData['data'][$i]['current_marketing_end']))->format('d M,Y');
            }else {
                $responseData['data'][$i]['current_marketing_end'] = '----';
            }
        }
        return $responseData;
    }

    function getDashboardWidgetsData($user_id) {
        $this->setDefaultAccessToSU();
        $final = [];
        //Branches related count
        $this->db->select('COUNT(CASE WHEN b.is_active = 1 THEN 1 END) as active,COUNT(CASE WHEN b.is_active = 0 THEN 1 END) as inActive');
        $this->db->from('branch b');
        $this->db->join("branch_xref_user bxu", "b.branch_id = bxu.branch_id", "left");
        $this->db->where('bxu.user_id', $user_id);
        $userData = $this->db->get()->row_array();
        $final['branches'] = $userData;

        //Branches related count
        $this->db->select('c.`name`,count(bxl.branch_xref_lead_id) as student');
        $this->db->from('course c');
        $this->db->join("branch_xref_lead bxl", " bxl.fk_course_id=c.course_id AND bxl.is_active=1 AND bxl.`status`=(SELECT l.lead_stage_id FROM lead_stage l WHERE l.lead_stage='M7')", "left");
        $this->db->group_by('c.course_id');
        $StudentCoursesData = $this->db->get()->result();
        $final['enrolled_students'] = $StudentCoursesData;
        
        return $final;
    }
    function feeDueWidget($user_id) {
        $final = [];
        $qry = "SELECT (SELECT IFNULL(sum((cfi.amount_payable-cfi.concession-cfi.right_of_amount) - cfi.amount_paid),0) as dueAmount from
		candidate_fee_item cfi,
		candidate_fee cf,
		fee_structure fs,
		fee_structure_item fsi,
		branch_xref_lead bxl,
		lead l,
		reference_type rt, 
		reference_type_value rtv
		where
		cf.candidate_fee_id=cfi.fk_candidate_fee_id and
		fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id and
		bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id and
		l.lead_id=bxl.lead_id 
    AND rt.reference_type_id=rtv.reference_type_id
    AND rtv.value='Retail'
    AND fs.fk_type_id=rtv.reference_type_value_id) AS retail,
(SELECT IFNULL(sum((cfi.amount_payable-cfi.concession-cfi.right_of_amount) - cfi.amount_paid),0) as dueAmount from
		candidate_fee_item cfi,
		candidate_fee cf,
		fee_structure fs,
		fee_structure_item fsi,
		branch_xref_lead bxl,
		lead l,
		reference_type rt, 
		reference_type_value rtv
		where
		cf.candidate_fee_id=cfi.fk_candidate_fee_id and
		fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id and
		bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id and
		l.lead_id=bxl.lead_id 
    AND rt.reference_type_id=rtv.reference_type_id
    AND rtv.value='Corporate'
    AND fs.fk_type_id=rtv.reference_type_value_id) AS corporate,
(SELECT IFNULL(sum((cfi.amount_payable-cfi.concession-cfi.right_of_amount) - cfi.amount_paid),0) as dueAmount from
		candidate_fee_item cfi,
		candidate_fee cf,
		fee_structure fs,
		fee_structure_item fsi,
		branch_xref_lead bxl,
		lead l,
		reference_type rt, 
		reference_type_value rtv
		where
		cf.candidate_fee_id=cfi.fk_candidate_fee_id and
		fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id and
		bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id and
		l.lead_id=bxl.lead_id 
    AND rt.reference_type_id=rtv.reference_type_id
    AND rtv.value='Group'
    AND fs.fk_type_id=rtv.reference_type_value_id) AS institute";
        $res_all = $this->db->query($qry);
        
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $final = $res_all;
            }
        } else {
            //no data
            array_push($final,[
                'retail' => '0',
                'corporate' => '0',
                'institute' => '0'
            ]);
        }
        return $final;
    }
    function studentTypeWidget($user_id) {
        $final = [];
        
        $qry = 'select (SELECT count(*) FROM lead l WHERE l.fk_company_id IS NULL AND l.fk_institution_id IS NULL) AS retail,
 (SELECT count(*) FROM lead l WHERE l.fk_company_id IS NOT NULL AND l.fk_institution_id IS NULL) AS corporate,
 (SELECT count(*) FROM lead l WHERE l.fk_company_id IS NULL AND l.fk_institution_id IS NOT NULL) AS institute';
        $res_all = $this->db->query($qry);

        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $final = $res_all;
            }
        } else {
            //no data
            array_push($final,[
                'retail' => '0',
                'corporate' => '0',
                'institute' => '0'
            ]);
        }
        return $final;
    }
    
    function removeDependecyForCityChange($CityId){
        $this->db->select('lead_id');
        $this->db->from('lead');
        $this->db->where('city_id', $CityId);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                //release it
                foreach ($query as $key => $value) {
                    $this->load->model('Lead_model');
                    $res = $this->Lead_model->releaseDueIfExists($value['lead_id']);
                    if($res['status']){
                        return true;
                    } else{
                        return array('error' => 'dberror', 'msg' => $error = $res['message']);
                    }
                }
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    function getBranchDashboardData($params){

        $branchId=$params['branchId'];
        $finalData['branchDetails']=array();
        $finalData['joined']=array();
        $finalData['inventory']=array();
        $finalData['studentRelation']=array();
        $finalData['feeDues']=array();
        $finalData['coursesInfo']=array();
        $finalData['currentCoursesInfo']=array();
        $finalData['studentActivities']=array();
        $finalData['manageUsers']=array();

        //branch Details//
            $this->db->select('name,phone,code');
            $this->db->from('branch');
            $this->db->where('branch_id',$branchId);
            $branchDetailsRes=$this->db->get();
            if($branchDetailsRes){
                if($branchDetailsRes->num_rows()>0){
                    $branchDetailsResult=$branchDetailsRes->result();
                    $finalData['branchDetails']=$branchDetailsResult[0];
                }
            }
        //-------------//

        //joined information//
            $this->db->select('branch_xref_lead_id');
            $this->db->from('branch_xref_lead bxl');
            $this->db->join('lead_stage l','bxl.status=l.lead_stage_id');
            $this->db->where('l.lead_stage','M7');
            $this->db->where('bxl.branch_id',$branchId);
            $this->db->where('bxl.is_active',1);
            $joinedRes=$this->db->get();
            if($joinedRes){
                $finalData['joined']['count']=$joinedRes->num_rows();
            }
        //-----------------//

        //inventory information //
        $finalData['inventory']['issued']=0;
        $bracnchIssued=$this->db->query("SELECT ifnull(sum(quantity),0) as branchIssued FROM lead_item_issue l,batch b where l.fk_batch_id=b.batch_id and b.fk_branch_id=$branchId");
        if($bracnchIssued){
            if($bracnchIssued->num_rows()>0){
                $bracnchIssuedRow=$bracnchIssued->result();
                $finalData['inventory']['issued']=$bracnchIssuedRow[0]->branchIssued;
            }
        }


        $finalData['inventory']['total']=$finalData['joined']['count'];
        $finalData['inventory']['stock']=0;
        $Stockqry = "select (select if(sum(si.quantity-si.returned_quantity)>0,sum(si.quantity-si.returned_quantity),0) from product p
                                LEFT JOIN product_xref_course pxc ON p.product_id=pxc.fk_product_id
                                LEFT JOIN stockflow_item si ON p.product_id=si.fk_product_id
                                LEFT JOIN stockflow s ON si.fk_stockflow_id=s.stockflow_id
                                LEFT JOIN batch b ON b.fk_course_id=pxc.fk_course_id
                                WHERE s.type='GRN'
                                and s.receiver_mode='branch'
                                AND b.batch_id in (select batch_id from batch where fk_branch_id=$branchId)
                                AND s.fk_receiver_id=b.fk_branch_id) -
                            (select if(sum(si.quantity-si.returned_quantity)>0,sum(si.quantity-si.returned_quantity),0) from product p
                                LEFT JOIN product_xref_course pxc ON p.product_id=pxc.fk_product_id
                                LEFT JOIN stockflow_item si ON p.product_id=si.fk_product_id
                                LEFT JOIN stockflow s ON si.fk_stockflow_id=s.stockflow_id
                                LEFT JOIN batch b ON b.fk_course_id=pxc.fk_course_id
                                WHERE s.type='GTN'
                                and s.requested_mode='branch'
                                AND b.batch_id in (select batch_id from batch where fk_branch_id=$branchId )
                                AND s.fk_requested_id=b.fk_branch_id) AS total";
        $resStock = $this->db->query($Stockqry);
        if($resStock){

            if($resStock->num_rows()>0){
                $rowStock=$resStock->result();
                $finalData['inventory']['stock']=$rowStock[0]->total;
            }
        }



        //------------------//

        //student relation//
        $finalData['studentRelation']['total']='--';
        $finalData['studentRelation']['selectedtotal']=$finalData['joined']['count'];
        //----------------//

        // Fee dues //
        $this->db->select('sum(cfi.amount_payable-cfi.concession-cfi.right_of_amount-cfi.amount_paid) as dueAmount');
        $this->db->from('candidate_fee_item cfi');
        $this->db->join('candidate_fee cf','cf.candidate_fee_id=cfi.fk_candidate_fee_id');
        $this->db->join('branch_xref_lead bxl','bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id');
        $this->db->where('bxl.branch_id',$branchId);
        $this->db->where('bxl.fk_batch_id is not NULL');
        $feeDuesRes=$this->db->get();
        if($feeDuesRes){
            if($feeDuesRes->num_rows()>0){
                $feeDuesResult=$feeDuesRes->result();
                $finalData['feeDues']=$feeDuesResult[0];
            }
        }
        //----------------//

        //----- course details ----//
        $this->db->select("datediff(b.marketing_enddate,date(now())) as leftDays, b.code , ba.code branch_code, b.batch_id,b.fk_branch_id,b.fk_course_id,b.marketing_startdate,b.marketing_enddate,b.acedemic_startdate,b.acedemic_enddate,b.target,c.name");
        $this->db->from('batch b');
        $this->db->join('course c','b.fk_course_id=c.course_id');
        $this->db->join('branch ba','b.fk_branch_id=ba.branch_id');
        $this->db->where("date(now()) between date(b.marketing_startdate) and date(b.marketing_enddate)");
        $this->db->where("b.fk_branch_id",$branchId);
        $courseRes = $this->db->get();
        if($courseRes){
            if ($courseRes->num_rows() > 0){
                $courseResult=$courseRes->result();
                foreach($courseResult as $key=>$val){
                    $val->code = $val->branch_code.'-'.$val->name.'-M7-'.str_pad($val->code, 4, '0', STR_PAD_LEFT);     /*Adding prefix to batch code*/
                    $val->marketing_startdate=date('d M, Y',strtotime($val->marketing_startdate));
                    $val->marketing_enddate=date('d M, Y',strtotime($val->marketing_enddate));
                    $val->acedemic_startdate=date('d M, Y',strtotime($val->acedemic_startdate));
                    $val->acedemic_enddate=date('d M, Y',strtotime($val->acedemic_enddate));

                    $this->db->select('branch_visitor_id');
                    $this->db->from('branch_visitor bv');
                    $this->db->join('branch_xref_lead bxl','bxl.branch_xref_lead_id=bv.fk_branch_xref_lead_id');
                    $this->db->where('bxl.branch_id',$val->fk_branch_id);
                    $this->db->where('bxl.fk_course_id',$val->fk_course_id);
                    $branchVisitRes=$this->db->get();

                    if($branchVisitRes){
                        $val->branchVisitCount=$branchVisitRes->num_rows();
                    }
                    else{
                        $val->branchVisitCount=0;
                    }

                    $this->db->select('branch_xref_lead_id');
                    $this->db->from('branch_xref_lead bxl');
                    $this->db->join('lead_stage l','bxl.status=l.lead_stage_id');
                    $this->db->where('l.lead_stage','M7');
                    $this->db->where('bxl.fk_batch_id',$val->batch_id);
                    $this->db->where('bxl.is_active',1);
                    $filledSeatsRes=$this->db->get();
                    if($filledSeatsRes){
                        $val->filledSeatsCount=$filledSeatsRes->num_rows();
                    }
                    else{
                        $val->filledSeatsCount=0;
                    }


                    // calls information //
                    $this->db->select('lead_user_xref_lead_due_id');
                    $this->db->from('lead_user_xref_lead_due l');
                    $this->db->join('branch_xref_lead bxl','bxl.branch_xref_lead_id=l.fk_branch_xref_lead_id');
                    $this->db->where('bxl.branch_id',$val->fk_branch_id);
                    $this->db->where('bxl.fk_course_id',$val->fk_course_id);
                    $callsRes=$this->db->get();
                    if($callsRes){
                        $val->callsCount=$callsRes->num_rows();
                    }
                    else{
                        $val->callsCount=0;
                    }
                    //-------------------//
                        $val->encodedBranchId=encode($val->fk_branch_id);
                }
                $finalData['coursesInfo']=$courseResult;
            }

        }
        //------------------------//



        //----- current course details ----//
        $this->db->select("datediff(b.marketing_enddate,date(now())) as leftDays, b.code,ba.code branch_code,b.batch_id,b.fk_branch_id,b.fk_course_id,b.marketing_startdate,b.marketing_enddate,b.acedemic_startdate,b.acedemic_enddate,b.target,c.name");
        $this->db->from('batch b');
        $this->db->join('course c','b.fk_course_id=c.course_id');
        $this->db->join('branch ba','b.fk_branch_id=ba.branch_id');
        $this->db->where("date(now()) between date(b.acedemic_startdate) and date(b.acedemic_enddate)");
        $this->db->where("b.fk_branch_id",$branchId);
        $courseCurrentRes = $this->db->get();
        if($courseCurrentRes){
            if ($courseCurrentRes->num_rows() > 0){
                $courseCurrentResult=$courseCurrentRes->result();
                foreach($courseCurrentResult as $key=>$val){
                    $val->code = $val->branch_code.'-'.$val->name.'-M7-'.str_pad($val->code, 4, '0', STR_PAD_LEFT);     /*Adding prefix to batch code*/
                    $val->marketing_startdate=date('d M, Y',strtotime($val->marketing_startdate));
                    $val->marketing_enddate=date('d M, Y',strtotime($val->marketing_enddate));
                    $val->acedemic_startdate=date('d M, Y',strtotime($val->acedemic_startdate));
                    $val->acedemic_enddate=date('d M, Y',strtotime($val->acedemic_enddate));

                    $this->db->select('branch_visitor_id');
                    $this->db->from('branch_visitor bv');
                    $this->db->join('branch_xref_lead bxl','bxl.branch_xref_lead_id=bv.fk_branch_xref_lead_id');
                    $this->db->where('bxl.branch_id',$val->fk_branch_id);
                    $this->db->where('bxl.fk_course_id',$val->fk_course_id);
                    $branchVisitRes=$this->db->get();

                    if($branchVisitRes){
                        $val->branchVisitCount=$branchVisitRes->num_rows();
                    }
                    else{
                        $val->branchVisitCount=0;
                    }

                    $this->db->select('branch_xref_lead_id');
                    $this->db->from('branch_xref_lead bxl');
                    $this->db->join('lead_stage l','bxl.status=l.lead_stage_id');
                    $this->db->where('l.lead_stage','M7');
                    $this->db->where('bxl.fk_batch_id',$val->batch_id);
                    $this->db->where('bxl.is_active',1);
                    $filledSeatsRes=$this->db->get();
                    if($filledSeatsRes){
                        $val->filledSeatsCount=$filledSeatsRes->num_rows();
                    }
                    else{
                        $val->filledSeatsCount=0;
                    }

                    //batch wise fee due information//
                    $this->db->select('ifnull(sum(cfi.amount_payable-cfi.concession-cfi.right_of_amount-cfi.amount_paid),0) as dueAmount');
                    $this->db->from('candidate_fee_item cfi');
                    $this->db->join('candidate_fee cf','cf.candidate_fee_id=cfi.fk_candidate_fee_id');
                    $this->db->join('branch_xref_lead bxl','bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id');
                    $this->db->where('bxl.fk_batch_id',$val->batch_id);
                    $feeDuesRes=$this->db->get();
                    $val->feeDueAmount=0;
                    if($feeDuesRes){
                        if($feeDuesRes->num_rows()>0){
                            $feeDuesResult=$feeDuesRes->result();
                            $val->feeDueAmount=$feeDuesResult[0]->dueAmount;
                        }
                    }
                    //-------------------//

                    //subject info //
                    $this->db->select('bxc.batch_xref_class_id,bxc.class_date,v.name location,f.name as faculty');
                    $this->db->from('batch_xref_class bxc');
                    $this->db->join('venue v','v.venue_id=bxc.fk_venue_id');
                    $this->db->join('faculty f','bxc.fk_faculty_id=f.faculty_id');
                    $this->db->where('bxc.fk_batch_id',$val->batch_id);
                    $this->db->where('date(bxc.class_date)>=date(now())');
                    $this->db->where('bxc.is_active',1);
                    $this->db->order_by('bxc.batch_xref_class_id','asc');
                    $this->db->limit(1);
                    $currentClassRes=$this->db->get();
                    $val->currentClassDate='--';
                    $val->currentClassFaculty='--';
                    $val->currentClassLocation='--';
                    if($currentClassRes){
                        if($currentClassRes->num_rows()>0){
                            $currentClassResult=$currentClassRes->result();
                            $currentClassId=$currentClassResult[0]->batch_xref_class_id;
                            $val->currentClassDate=date('d M, Y',strtotime($currentClassResult[0]->class_date));
                            $val->currentClassFaculty=$currentClassResult[0]->faculty;
                            $val->currentClassLocation=$currentClassResult[0]->location;
                        }
                        else{
                            $currentClassId=0;
                        }
                    }
                    else{
                        $currentClassId=0;
                    }

                    //------------/
                    //check for class position //
                    $this->db->select('bxc.batch_xref_class_id,bxc.class_date,v.name location,f.name as faculty');
                    $this->db->from('batch_xref_class bxc');
                    $this->db->join('venue v','v.venue_id=bxc.fk_venue_id');
                    $this->db->join('faculty f','bxc.fk_faculty_id=f.faculty_id');
                    $this->db->where('bxc.fk_batch_id',$val->batch_id);
                    $this->db->where('bxc.is_active',1);
                    $this->db->order_by('bxc.batch_xref_class_id','asc');
                    $currentClassRes=$this->db->get();
                    $val->totalClassCount=0;
                    $val->currentClassCount=0;
                    if($currentClassRes){
                        $totalClassCount=$currentClassRes->num_rows();
                        $val->totalClassCount=$totalClassCount;
                        /*if($currentClassId!=0){
                            if($totalClassCount>0){
                                $position=0;
                                foreach($currentClassRes as $k_class=>$v_class){
                                    $position=$position+1;
                                    if($v_class->batch_xref_class_id==$currentClassId){
                                        $val->currentClassCount=$position;
                                        break;
                                    }
                                }
                            }
                        }*/
                    }
                    //if($currentClassId!=0){
                        $this->db->select('bxc.batch_xref_class_id,bxc.class_date,v.name location,f.name as faculty');
                        $this->db->from('batch_xref_class bxc');
                        $this->db->join('venue v','v.venue_id=bxc.fk_venue_id');
                        $this->db->join('faculty f','bxc.fk_faculty_id=f.faculty_id');
                        $this->db->where('bxc.fk_batch_id',$val->batch_id);
                        $this->db->where('date(bxc.class_date)<date(now())');
                        $this->db->where('bxc.is_active',1);
                        $this->db->order_by('bxc.batch_xref_class_id','asc');
                        $currentClassRes=$this->db->get();
                        if($currentClassRes){
                            $position=$currentClassRes->num_rows();
                        }
                        else{
                            $position=0;
                        }
                    /*}
                    else{
                        $position=0;
                    }*/
                        $val->currentClassCount=$position;
                    //----------//

                    //issued material information //
                    $res_books=$this->db->query("SELECT ifnull(sum(quantity),0) as issued FROM lead_item_issue l where fk_batch_id=".$val->batch_id);
                    if($res_books){
                        if($res_books->num_rows()>0){
                            $row_books=$res_books->result();
                            $val->issuedBooksCount =$row_books[0]->issued;
                        }
                        else{
                            $val->issuedBooksCount = 0;
                        }
                    }
                    else {
                        $val->issuedBooksCount = '--';
                    }
                    //--------------//
                    $val->encodedBranchId=encode($val->fk_branch_id);
                }
                $finalData['currentCoursesInfo']=$courseCurrentResult;
            }
        }
        //------------------------//

        $db_response=array('status'=>true,'message'=>'success','data'=>$finalData);
        return $db_response;


    }
    public function getManageUsers($params){
        $status=true;$message='success';$data=[];
        $branchId=$params['branchId'];
        $val=$params['searchVal'];
        $students="select e.user_id as id ,e.name as `text` from user u,employee e where e.user_id=u.user_id and u.is_active=1 and u.user_id not in (select bxu.user_id from branch_xref_user bxu where bxu.branch_id=$branchId) and e.name like '%".$val."%'";

        $res=$this->db->query($students);
        if($res){
            if($res->num_rows()>0){
                $status=true;$message='success';$data=$res->result();
            }
            else{
                $status=false;$message='No records found';$data=[];
            }
        }
        else{
            $status=false;$message='Something went wrong.';$data=[];
        }
        return array('status'=>$status,'message'=>$message,'data'=>$data);
    }
    
    public function getBranchChartData($branchId) {
        $this->db->select('*');
        $this->db->from('branch b');
        $this->db->where('b.branch_id',$branchId);
        $this->db->where('b.is_active',1);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result)<=0) {
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $totalTarget = 0;
        
        $this->db->select('c.name, b.target, sum(if(bxl.branch_xref_lead_id IS NOT NULL ,1,0)) as filled, (b.target-sum(if(bxl.branch_xref_lead_id IS NOT NULL ,1,0))) AS available');
        $this->db->from('batch b');
        $this->db->join('branch_xref_lead bxl','b.batch_id=bxl.fk_batch_id','left');
        $this->db->join('course c','b.fk_course_id=c.course_id','left');
        $this->db->where('b.fk_branch_id',$branchId);
        $this->db->where('now() between b.marketing_startdate AND `b`.marketing_enddate');
        $this->db->group_by('c.course_id');
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result)>0) {
                foreach ($result as $k => $v) {
                    $course[] = $v->name;
                    $totalTarget += $v->target;
                }
                $final = [];
                $finalCourse = [];
                foreach ($result as $k => $v) {
                    $coursePercentage = 0;
                    $individualPercentage = 0;
                    $balancePercentage = 0;
                    $coursePercentage = round((($v->target/$totalTarget)*100),2);
                    $individualPercentage = round((($v->filled/$v->target)*100),2);
                    $balancePercentage = round($coursePercentage - $individualPercentage ,2);
                    $finalCourse[] = $individualPercentage.'+'.$balancePercentage;
                    $final[] = array(
                        'courseName' => $v->name,
                        'target' => $v->target,
                        'individual' => $v->filled,
                        'balance' => ($v->target - $v->filled),
                        'coursePercentage'=>$coursePercentage,
                        'individualPercentage' => $individualPercentage,
                        'balancePercentage' => $balancePercentage
                    );
                }
                return array('course'=>$course, 'final'=>$final, 'finalCourse' => $finalCourse);
            }else{
                return array('course'=>0, 'final'=>0, 'finalCourse'=>0);
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
}

