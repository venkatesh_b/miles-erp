<?php

/* 
 * Created By: V Parameshwar. Date: 03-02-2016
 * Purpose: Services for call order confirmation.
 * 
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CallOrderConfirmation_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }


    public function getAllLeadStages(){
//    $sql ="SELECT b.lead_stage_id as left_id,a.lead_stage_id as right_id,b.lead_stage as left_side,a.lead_stage as right_side,b.description as left_desc,a.description as right_desc,b.call_after_days as left_days,a.call_after_days as right_days,b.conversion_rate as left_conversion_rate,a.conversion_rate as right_conversion_rate
//    FROM lead_stage a, lead_stage b
//    WHERE a.lead_stage_id = b.linked_to and a.is_hidden=0 and b.is_hidden=0  ORDER BY a.order ASC";
//        $sql="select b.lead_stage_id as left_id,b.lead_stage as left_side,b.description as left_desc,a.description as right_desc,
//        a.lead_stage_id right_id,a.lead_stage as right_side,a.call_after_days as right_days,b.call_after_days as left_days,a.conversion_rate as right_conversion_rate,b.conversion_rate as left_conversion_rate
//        from lead_stage a,lead_stage b where a.lead_stage_id = b.linked_to
//        UNION
//        select '' as left_id,'' as left_side,\"\",description,lead_stage_id right_id,lead_stage as right_side,\"\",\"\",call_after_days right_days,conversion_rate as right_conversion_rate
//        from lead_stage where  linked_to='' and lead_stage_id not in (select linked_to from lead_stage)";
        $sql="select b.lead_stage_id as left_id,b.lead_stage as left_side,b.description as left_desc,a.description as right_desc,
        a.lead_stage_id right_id,a.lead_stage as right_side,a.call_after_days as right_days,b.call_after_days as left_days,a.conversion_rate as right_conversion_rate,b.conversion_rate as left_conversion_rate
        from lead_stage a,lead_stage b where a.lead_stage_id = b.linked_to
        UNION
        select '' as left_id,'' as left_side,\"\",description,lead_stage_id right_id,lead_stage as right_side,call_after_days right_days,\"\",conversion_rate as right_conversion_rate,\"\"
        from lead_stage where  linked_to='' and lead_stage_id not in (select linked_to from lead_stage)";

         $query = $this->db->query($sql);
         $query->result_array();


        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid ID',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    function saveLeadStages($data){
        $db_response="";

        if(isset($data) && !empty($data)) {

            $values=array();
            $i=0;
            foreach($data as $k=>$v) {
                if ($k != 'leadsConversionRate'){
                    $explode = explode("###", $v);
                if (isset($explode[0])) {
                    $values['description'] = $explode[0];
                } else {
                    $values['description'] = "";
                }
                if (isset($explode[1])) {
                    $values['call_after_days'] = $explode[1];
                } else {
                    $values['call_after_days'] = "";

                }

                if (isset($explode[2])) {
                    $values['updated_by'] = $data['userID'];
                    $values['updated_on'] = date("Y-m-d H:i:s");
                    $this->db->where("lead_stage_id", $explode[2]);
                    $res_update = $this->db->update('lead_stage', $values);
                }
            }

            }
            if ($res_update) {
                $db_response = array("status" => true, "message" => "Updated successfully", "data" => array());
            } else {
                $error = $this->db->error();
                $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
            }
        }
        if(isset($data['leadsConversionRate']) && trim($data['leadsConversionRate'])!=''){
            $leadsConversionRates=explode('|',$data['leadsConversionRate']);
            for($k=0;$k<count($leadsConversionRates);$k++){
                $leadsConversionRatesInd=explode('#',$leadsConversionRates[$k]);
                $dt['conversion_rate']=$leadsConversionRatesInd[1];
                $this->db->where("lead_stage_id",$leadsConversionRatesInd[0]);
                $res_update = $this->db->update('lead_stage',$dt);
            }
        }

        return $db_response;
    }
    function saveDndConfiguration($params){
            if($params['dndConfigCount']>0){
                if($params['dndConfigCount']!=$params['previousDndConfigCount']){
                    $data_update['value']=$params['dndConfigCount'];
                    $this->db->where('key','DND CONFIG');
                    $res_update=$this->db->update('app_configuration_setting',$data_update);
                    if($res_update){
                        $status=true;$message='Success';$data_res=[];
                        if($params['dndConfigCount']<$params['previousDndConfigCount']) {
                            $this->db->select('lead_id,branch_xref_lead_id,logic_item_name,logic_item_count');
                            $this->db->from('branch_xref_lead');
                            $this->db->where('logic_item_count>=', $params['dndConfigCount']);
                            $this->db->where('logic_item_count>0');
                            $sel_leads = $this->db->get();
                            if ($sel_leads) {
                                if ($sel_leads->num_rows() > 0) {
                                    $sel_leads_row = $sel_leads->result();
                                    foreach ($sel_leads_row as $k_lead => $v_lead) {
                                        $strleadsatge=array();
                                        $data_update_bxlead = array();
                                        $nextLeadStage='';
                                        $nextLeadRes='';
                                        $data_lead_user_xref_lead_history=array();
                                        $call_schedule_head_id='';
                                        $strleadsatge=explode('_',$v_lead->logic_item_name);
                                        $nextLeadStageText=$strleadsatge[2];
                                        $nextLeadRes=$this->getLeadStageId($nextLeadStageText);
                                        if($nextLeadRes===false){

                                        }
                                        else {
                                            $nextLeadStage=$nextLeadRes['stage_id'];
                                            $call_schedule_head_id = $this->getHeadId($v_lead->branch_xref_lead_id);
                                            if ($call_schedule_head_id === false) {

                                            } else {
                                                $delete_due = $this->db->delete("lead_user_xref_lead_due", array("fk_branch_xref_lead_id" => $v_lead->branch_xref_lead_id));
                                                $data_lead_user_xref_lead_history=[];
                                                $data_lead_user_xref_lead_history['fk_user_id'] = $_SERVER['HTTP_USER'];
                                                $data_lead_user_xref_lead_history['fk_branch_xref_lead_id'] = $v_lead->branch_xref_lead_id;
                                                $data_lead_user_xref_lead_history['fk_lead_id'] = $v_lead->lead_id;
                                                $data_lead_user_xref_lead_history['fk_lead_call_schedule_head_id'] = $call_schedule_head_id;
                                                $data_lead_user_xref_lead_history['lead_status'] = $nextLeadStage;
                                                $data_lead_user_xref_lead_history['description'] = '';
                                                $data_lead_user_xref_lead_history['created_on'] = date('Y-m-d H:i:s');
                                                $this->db->insert('lead_user_xref_lead_history', $data_lead_user_xref_lead_history);

                                            }


                                            $data_update_bxlead['logic_item_name'] = NULL;
                                            $data_update_bxlead['logic_item_count'] = 0;
                                            $data_update_bxlead['updated_by'] = $_SERVER['HTTP_USER'];
                                            $data_update_bxlead['updated_on'] = date('Y-m-d H:i:s');
                                            $data_update_bxlead['status'] = $nextLeadStage;
                                            $this->db->where('branch_xref_lead_id', $v_lead->branch_xref_lead_id);
                                            $res_update_bxlead=$this->db->update('branch_xref_lead', $data_update_bxlead);

                                            $data_timeline = '';
                                            $data_timeline['lead_id'] = $v_lead->lead_id;
                                            $data_timeline['branch_xref_lead_id'] = $v_lead->branch_xref_lead_id;
                                            $data_timeline['type'] = 'Lead Status';
                                            $data_timeline['description'] = '';
                                            $data_timeline['created_by'] = $_SERVER['HTTP_USER'];
                                            $data_timeline['created_on'] = date('Y-m-d H:i:s');
                                            $this->db->insert('timeline', $data_timeline);
                                        }

                                    }
                                } else {

                                }
                            } else {
                                $status = true;
                                $message = 'Success';
                                $data_res = [];
                            }
                            $this->db->select('lead_id');
                            $this->db->from('lead');
                            $this->db->where('logic_item_count>=',$params['dndConfigCount']);
                            $sel_coldcall_leads = $this->db->get();
                            if($sel_coldcall_leads){
                                $sel_coldcall_leads_row = $sel_coldcall_leads->result();
                                foreach ($sel_coldcall_leads_row as $k_coldcall_lead => $v_coldcall_lead) {
                                    $this->releaseDueIfExists($v_coldcall_lead->lead_id);
                                    $date_lead_update['logic_item_name']=NULL;
                                    $date_lead_update['logic_item_count']=0;
                                    $date_lead_update['is_dnd']=1;
                                    $this->db->where('lead_id',$v_coldcall_lead->lead_id);
                                    $this->db->update('lead',$date_lead_update);


                                }
                            }
                            else{

                            }

                        }
                    }
                    else{
                        $status=false;$message='Failed! Please try again.';$data_res=[];
                    }

                }
                else{
                    $status=true;$message='success';$data_res=[];
                }
            }
            else{
                $status=false;$message='Value should be greater than zero';$data_res=[];
            }
            return $db_response=array('status'=>$status,"message"=>$message,"data"=>$data_res);

    }
    function getLeadStageId($stage=''){
        $data='';
        $this->db->select('lead_stage_id');
        $this->db->from('lead_stage');
        $this->db->where('lead_stage',$stage);
        $res=$this->db->get();
        if($res){
            if($res->num_rows()>0){
                $row=$res->result()[0];
                $data['stage_id']=$row->lead_stage_id;
                return $data;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }
    function getHeadId($id){
        $this->db->select("fk_lead_call_schedule_head_id");
        $this->db->from("lead_user_xref_lead_due");
        $this->db->where("fk_branch_xref_lead_id",$id);
        $query = $this->db->get();
        if($query) {
            foreach ($query->result() as $k) {
                return $k->fk_lead_call_schedule_head_id;
            }
            return false;
        }
        else{
            return false;
        }


    }
    function releaseDueIfExists($leadId=''){
        //{"status":true,"message":"success","data":{"branchId":"1","releaseCnt":"10","releaseBy":"1","assignedUserId":"9"}}
        $status=TRUE;$message='success';$data=array();
        $cnt=0;
        $tobededucted=array();

        //$availableTotal="select  count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." ";
        $availableTotal="select  count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where  l.status=1 and l.lead_id=uxl.lead_id and uxl.lead_id=$leadId";
        $resTotal=$this->db->query($availableTotal);
        if($resTotal) {
            $release=0;
            $resultTotal = $resTotal->result();
            foreach ($resultTotal as $kTotal => $vTotal) {
                $release = $vTotal->availCnt;
            }
            if($release>0) {
                //$available = "select  uxl.call_schedule_head_id,count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='" . $param['branchId'] . "') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " group by uxl.call_schedule_head_id";
                $available = "select  uxl.call_schedule_head_id,count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where  l.status=1 and l.lead_id=uxl.lead_id and uxl.lead_id=" . $leadId . " group by uxl.call_schedule_head_id";
                $res = $this->db->query($available);
                if ($res) {
                    $result = $res->result();
                    foreach ($result as $k => $v) {
                        $headId = $v->call_schedule_head_id;
                        $availCnt = $v->availCnt;

                        if ($availCnt < $release) {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $availCnt);
                            $release = $release - $availCnt;
                        } elseif ($availCnt > $release) {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                            $release = $release - $release;
                        } elseif ($availCnt == $release) {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                            $release = $release - $release;
                        } else {

                        }
                        if ($release == 0) {
                            break;
                        }
                    }
                    if (count($tobededucted) > 0) {
                        for ($i = 0; $i < count($tobededucted); $i++) {

                            $callHeadId = $tobededucted[$i]['headId'];
                            $releaseCount = $tobededucted[$i]['releaseCount'];

                            $data_release_head['release_count'] = $releaseCount;
                            $data_release_head['released_by'] = $_SERVER['HTTP_USER'];
                            $data_release_head['released_on'] = date('Y-m-d H:i:s');
                            $data_release_head['call_schedule_head_id'] = $callHeadId;
                            $insert_release_head = $this->db->insert('user_xref_lead_release_head', $data_release_head);
                            if ($insert_release_head) {
                                //release head inserted successfully.
                                $release_head_id = $this->db->insert_id();
                                //$available="select  l.lead_id from lead l,branch b,user_xref_lead_due uxl where l.city_id=b.fk_city_id and b.branch_id=".$param['branchId']." and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." and uxl.call_schedule_head_id=".$callHeadId." order by l.lead_id asc limit ".$releaseCount;
                                //$available = "select  l.lead_id from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='" . $param['branchId'] . "') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " and uxl.call_schedule_head_id=" . $callHeadId . " order by l.lead_id asc limit " . $releaseCount;
                                $available = "select  l.lead_id,uxl.user_id from lead l,user_xref_lead_due uxl where l.status=1 and l.lead_id=uxl.lead_id and uxl.lead_id=" . $leadId . " and uxl.call_schedule_head_id=" . $callHeadId . " order by l.lead_id asc limit " . $releaseCount;
                                $select_lead_ids = $this->db->query($available);

                                if ($select_lead_ids) {
                                    //leads fetched successfully
                                    $result_sel_leadIds = $select_lead_ids->result();
                                    $data_insert_releaseCalls = '';
                                    $data_release_leads = '';
                                    foreach ($result_sel_leadIds as $result_sel_leadIds_k => $result_sel_leadIds_v) {
                                        $data_insert_releaseCalls[] = array("user_id" => $result_sel_leadIds_v->user_id, "lead_id" => $result_sel_leadIds_v->lead_id, "head_id" => $callHeadId, "release_head_id" => $release_head_id);
                                        $data_release_leads[] = $result_sel_leadIds_v->lead_id;
                                    }
                                    if ($data_insert_releaseCalls != '') {
                                        //release calls found
                                        $insert_release_calls = $this->db->insert_batch('user_xref_lead_release', $data_insert_releaseCalls);
                                        if ($insert_release_calls) {
                                            //release calls successfully inserted
                                            $data_release_leads = array_map('intval', $data_release_leads);
                                            $data_release_leads = implode(",", $data_release_leads);

                                            /*$this->db->where_in('lead_id', $data_release_leads);
                                            $this->db->where('call_schedule_head_id', $callHeadId);
                                            $this->db->where('user_id', $param['assignedUserId']);
                                            $delete_due_calls=$this->db->delete('user_xref_lead_due');*/
                                            $delete_due_calls_query = "DELETE FROM user_xref_lead_due WHERE call_schedule_head_id=$callHeadId  AND lead_id in ($data_release_leads)";
                                            $delete_due_calls = $this->db->query($delete_due_calls_query);
                                            if ($delete_due_calls) {
                                                //calls deleted successfully
                                                $this->db->where('call_schedule_head_id', $callHeadId);
                                                $this->db->set('total_scheduled_calls', 'total_scheduled_calls-' . $releaseCount, FALSE);
                                                $update_call_head = $this->db->update('call_schedule_head');
                                                if ($update_call_head) {
                                                    //successfully updated call head counts
                                                    $status = TRUE;
                                                    $message = 'Released successfully';
                                                    $data = array();

                                                } else {
                                                    //problem in updating count of call head
                                                    $error = $this->db->error();
                                                    $status = FALSE;
                                                    $message = 'problem in updating count of call head';
                                                    $data = array("message" => $error['message']);
                                                }
                                            } else {
                                                //problem in deleting calls from dues
                                                $error = $this->db->error();
                                                $status = FALSE;
                                                $message = 'problem in deleting calls from dues';
                                                $data = array("message" => $error['message']);
                                            }

                                        } else {
                                            //problem in batch insert of release calls
                                            $error = $this->db->error();
                                            $status = FALSE;
                                            $message = 'problem in batch insert of release calls';
                                            $data = array("message" => $error['message']);
                                        }
                                    } else {
                                        //no release calls found
                                        $error = $this->db->error();
                                        $status = FALSE;
                                        $message = 'no release calls found';
                                        $data = array("message" => $error['message']);
                                    }
                                } else {
                                    //problem in fetching leads for given head
                                    $error = $this->db->error();
                                    $status = FALSE;
                                    $message = 'problem in fetching leads for given head';
                                    $data = array("message" => $error['message']);
                                }

                            } else {
                                //problem in insertion of relese head id
                                $error = $this->db->error();
                                $status = FALSE;
                                $message = 'problem in insertion of release head id';
                                $data = array("message" => $error['message']);
                            }
                        }
                        $multi_db_response[] = array("status" => $status, "message" => $message, "data" => $data);
                    } else {
                        //no availabled heads found
                        $error = $this->db->error();
                        $status = FALSE;
                        $message = 'no available heads found';
                        $data = array("message" => $error['message']);
                    }
                } else {
                    //problem in fetching the available lead heads
                    $error = $this->db->error();
                    $status = FALSE;
                    $message = 'problem in fetching the available lead heads';
                    $data = array("message" => $error['message']);
                }
            }
            else{
                $status = FALSE;
                $message = 'No releases found';
                $data = array("message" => 'No releases found');
            }
        }
        else{
            $error = $this->db->error();
            $status = FALSE;
            $message = 'problem in fetching the total available leads';
            $data = array("message" => $error['message']);
        }
        $message="Released successfully";
        $multi_db_response=array("message"=>$message);
        $db_response=array("status"=>TRUE,"message"=>$message,"data"=>$multi_db_response);
        return $db_response;
    }


    /* Added by s.raju */
}
