<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 15-03-2016
 * Time: 02:48 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CallSchedule_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');

    }
    function getCallSchedules($param)
    {
        $this->db->select('u.user_id,e.name,e.image,rtv.`value` as role');
        $this->db->from('user u');
        $this->db->join('branch_xref_user b','b.user_id=u.user_id');
        $this->db->join('employee e','e.user_id=u.user_id');
        $this->db->join('user_xref_role uxr','uxr.fk_user_id=u.user_id');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=uxr.fk_role_id');
        $this->db->where('u.is_active',1);
        $this->db->where('rtv.`value`!=','Superadmin');
        $this->db->where('b.branch_id',$param['branchId']);
        $query_users=$this->db->get();
        if($query_users){
            if ($query_users->num_rows() > 0){
                $res=$query_users->result();
                foreach($res as $k_users=>$v_users){

                    $previousCallsCnt=0;
                    $todaysCallsCnt=0;
                    $inHandCallsCnt=0;
                    $params='';
                    $params['user_id']=$v_users->user_id;
                    $params['branchId']=$param['branchId'];
                    $params['tagId'] = $param['tagId'];
                    $params['sourceId'] = $param['sourceId'];
                    $previousCallsCnt=$this->getPreviousCallsCount($params);
                    $todaysCallsCnt=$this->getTodaysCallsCount($params);
                    $inHandCallsCnt=$this->getInHandCallsCount($params);
                    $v_users->previousCallsCount=$previousCallsCnt;
                    $v_users->todaysCallsCnt=$todaysCallsCnt;
                    $v_users->inHandCallsCount=$inHandCallsCnt;

                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'No Data Found',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    function getColdCallingCount($param)
    {
        $followup_count=$available_count=$totalCount=0;
        //$available="select l.lead_id from lead l,branch b where l.city_id=b.fk_city_id and b.branch_id='".$param['branchId']."' and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";
        //$available="select l.lead_id from lead l where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";
        //$available="select l.lead_id from lead l where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";

        //$city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."')) OR l.fk_branch_id='".$param['branchId']."')";

        //$city_condition=" ((ISNULL(l.fk_branch_id) AND l.city_id IN (SELECT bxc.fk_city_id FROM branch_xref_city bxc WHERE (bxc.is_primary = 1 OR bxc.is_secondary = 1) AND bxc.fk_branch_id = ".$param['branchId'].")) OR l.fk_branch_id = ".$param['branchId'].") ";

        //$available_contacts="select l.lead_id from lead l where ".$city_condition." and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0 and ISNULL(lead_next_followup_date)";exit;
        //$followup_contacts="select l.lead_id from lead l where ".$city_condition." and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0 and DATE(lead_next_followup_date)=DATE(NOW())";

        /*$available_contacts="select l.lead_id from lead l";
        if($param['tagId'] != '')
        {
            $available_contacts.="JOIN lead_xref_tag lxt ON l.lead_id = lxt.lead_id AND lxt.tag_id = ".$param['tagId'];
        }
        $available_contacts.=" where ".$city_condition;
        if($param['sourceId'] != '')
        {
            $available_contacts.=" and l.fk_source_id= ".$param['sourceId'];
        }
        $available_contacts.=" and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0 and ISNULL(lead_next_followup_date)";


        $followup_contacts="select l.lead_id from lead l";
        if($param['tagId'] != '')
        {
            $followup_contacts.="JOIN lead_xref_tag lxt ON l.lead_id = lxt.lead_id AND lxt.tag_id = ".$param['tagId'];
        }
        $followup_contacts.=" where ".$city_condition;
        if($param['sourceId'] != '')
        {
            $followup_contacts.=" and l.fk_source_id= ".$param['sourceId'];
        }
        echo $followup_contacts.=" and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0 and DATE(lead_next_followup_date)=DATE(NOW())";
        exit;

        $available_contacts_res=$this->db->query($available_contacts);
        if($available_contacts_res)
        {
            $available_count=$available_contacts_res->num_rows();
        }
        $followup_contacts_res=$this->db->query($followup_contacts);
        if($followup_contacts_res)
        {
            $followup_count=$followup_contacts_res->num_rows();
        }
        $totalCount=$available_count+$followup_count;*/
        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."')) OR l.fk_branch_id='".$param['branchId']."')";
        //$available="select l.lead_id from lead l where ".$city_condition." and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";

        $available="select l.lead_id from lead l";
        if($param['tagId'] != '')
        {
            $available.="JOIN lead_xref_tag lxt ON l.lead_id = lxt.lead_id AND lxt.tag_id = ".$param['tagId'];
        }
        $available.=" where ".$city_condition;
        if($param['sourceId'] != '')
        {
            $available.=" and l.fk_source_id= ".$param['sourceId'];
        }
        $available.=" and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";

        $res=$this->db->query($available);
        if($res)
        {
            $totalCount=$res->num_rows();
        }
        else
        {

        }
        $db_response=array("status"=>true,"message"=>'success',"data"=>array('count'=>$totalCount));
        return $db_response;
    }
    function getLeadColdCallingCount($param){

        $this->db->select("lead_stage_id,lead_stage");
        $this->db->from('lead_stage');
        //$this->db->where('is_hidden',0);
        $this->db->where('is_current',1);
        $this->db->order_by('order','asc');
        $res_leadcoldcalls=$this->db->get();
        $result_leadcoldcalls=$res_leadcoldcalls->result();
        $leadStages='';
        $cnt=array();
        $fromDate=$param['fromDate'];
        $toDate=$param['toDate'];
        $toDate=date('Y-m-d',strtotime($toDate.' +1 day'));
        $date_condition=" AND ((l.next_followup_date between '$fromDate' AND '$toDate') OR (l.expected_visit_date between '$fromDate' AND '$toDate') OR (l.expected_enroll_date between '$fromDate' AND '$toDate')) ";
        foreach($result_leadcoldcalls as $k_leadcoldcalls=>$v_leadcoldcalls){
            $leadStages[]=array("stage_id"=>$v_leadcoldcalls->lead_stage_id,"stage"=>$v_leadcoldcalls->lead_stage);

            $this->db->select('count(branch_xref_lead_id) cnt');
            $this->db->from('branch_xref_lead');
            $this->db->where('branch_id',$param['branchId']);
            $this->db->where('status',$v_leadcoldcalls->lead_stage_id);
            $this->db->where('is_active',1);
            $avail_sel_query="select count(l.branch_xref_lead_id) cnt from branch_xref_lead l where l.branch_id='".$param['branchId']."' and l.status=".$v_leadcoldcalls->lead_stage_id." and l.is_active=1 and l.branch_xref_lead_id not in (select fk_branch_xref_lead_id from lead_user_xref_lead_due where fk_lead_stage_id=".$v_leadcoldcalls->lead_stage_id.") $date_condition";
            $cnt_leadcall_query=$this->db->query($avail_sel_query);
            if($cnt_leadcall_query){
                $cnt_leadcall_query_res=$cnt_leadcall_query->result();
                foreach($cnt_leadcall_query_res as $k_result_lead_count=>$v_result_lead_count){
                    $cnt[$v_leadcoldcalls->lead_stage_id]=$v_result_lead_count->cnt;
                }
            }
            else{
                $cnt[$v_leadcoldcalls->lead_stage_id]=0;
            }

        }
        $data['stages']=$leadStages;
        $data['stageCounts']=$cnt;
        $db_response=array("status"=>true,"message"=>'success',"data"=>array('count'=>$data));
        return $db_response;
    }
    function getPreviousCallsCount($params){
        $cnt=0;
        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."')) OR l.fk_branch_id='".$params['branchId']."')";
        //$available="select  l.lead_id from lead l,user_xref_lead_history uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];
        //$available="select  l.lead_id from lead l,user_xref_lead_history uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];

        //echo $available="select  l.lead_id from lead l,user_xref_lead_history uxl where $city_condition  and l.status=1 and l.lead_id=uxl.lead_id AND DATE(uxl.created_on) = DATE_SUB(CURDATE(), INTERVAL 1 DAY) and uxl.user_id=".$params['user_id'];exit;
        //echo $available="select  l.lead_id from lead l,user_xref_lead_history uxl where $city_condition  and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];
        $available = "select l.lead_id
                        from
                        lead l
                        JOIN user_xref_lead_history uxl ON l.lead_id = uxl.lead_id ";
        if($params['tagId'] != '')
        {
            $available .= "JOIN lead_xref_tag lxt ON lxt.lead_id = l.lead_id AND lxt.tag_id = ".$params['tagId'];
        }

        $available .= " where ".$city_condition." l.status=1 and";

        if($params['sourceId'] != '')
        {
            $available .= " l.fk_source_id = ".$params['sourceId']." AND ";
        }

        $available .= " DATE(uxl.created_on) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                        and uxl.user_id=".$params['user_id'];

        $res=$this->db->query($available);
        if($res)
        {
            $cnt=$res->num_rows();
        }
        return ($cnt);
    }
    function getTodaysCallsCount($params)
    {
        $cnt=0;
        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."')) OR l.fk_branch_id='".$params['branchId']."')";
        //$available="select  l.lead_id from lead l,user_xref_lead_history uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];
        //$available="select  l.lead_id from lead l,user_xref_lead_history uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];

        //echo $available="select  l.lead_id from lead l,user_xref_lead_history uxl where $city_condition  and l.status=1 and l.lead_id=uxl.lead_id AND DATE(uxl.created_on) = CURDATE() and uxl.user_id=".$params['user_id'];exit;

        $available = "select l.lead_id
                        from
                        lead l
                        JOIN user_xref_lead_history uxl ON l.lead_id = uxl.lead_id ";
        if($params['tagId'] != '')
        {
            $available .= "JOIN lead_xref_tag lxt ON lxt.lead_id = l.lead_id AND lxt.tag_id = ".$params['tagId'];
        }

        $available .= " where ".$city_condition." l.status=1 and";

        if($params['sourceId'] != '')
        {
            $available .= " l.fk_source_id = ".$params['sourceId']." AND ";
        }

        $available .= " DATE(uxl.created_on) = CURDATE()
                        and uxl.user_id = ".$params['user_id'];

        $res=$this->db->query($available);

        if($res)
        {
            $cnt=$res->num_rows();
        }
        return ($cnt);
    }
    function getInHandCallsCount($params)
    {
        $cnt=0;
        $cnt_lead=0;
        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."')) OR l.fk_branch_id='".$params['branchId']."')";
        //$date_condition='';
        //$available="select  l.lead_id from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];
        //$available="select  l.lead_id from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];
        //echo $available="select  l.lead_id from lead l,user_xref_lead_due uxl where $city_condition and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];

        //$available="select  l.lead_id from lead l,user_xref_lead_due uxl where $city_condition and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];

        $available = "select l.lead_id
                        from
                        lead l
                        JOIN user_xref_lead_due uxl ON l.lead_id = uxl.lead_id ";
        if($params['tagId'] != '')
        {
            $available .= "JOIN lead_xref_tag lxt ON lxt.lead_id = l.lead_id AND lxt.tag_id = ".$params['tagId'];
        }

        $available .= " where ".$city_condition." AND l.status=1 AND";

        if($params['sourceId'] != '')
        {
            $available .= " l.fk_source_id = ".$params['sourceId']." AND ";
        }
        $available .= " uxl.user_id = ".$params['user_id'];
        //echo $available;exit;
        $res=$this->db->query($available);
        if($res){
            $cnt=$res->num_rows();
        }
        else
        {

        }
        return ($cnt);
    }
    function addCallSchedule($params){

        $assignedBy=$params['createdBy'];
        $callSchedules=$params['callSchedules'];
        $chosenTagId = $params['tagId'];
        $chosenSourceId = $params['sourceId'];
        if($callSchedules!='') {
            $callSchedules_explode = explode('|', $callSchedules);
            for ($i = 0; $i < count($callSchedules_explode); $i++) {
                $each_callSchedule = $callSchedules_explode[$i];
                $eah_callSchedule_explode = explode('-', $each_callSchedule);
                $assignedTo = $eah_callSchedule_explode[0];
                $totalAssignedCnt = $eah_callSchedule_explode[1];
                if ($totalAssignedCnt > 0) {
                    $cnt = 0;
                    $data_insert_head = array("total_scheduled_calls" => $totalAssignedCnt, "assigned_to" => $assignedTo, "assigned_on" => date('Y-m-d H:i:s'), "assigned_by" => $assignedBy);
                    $res_insert_head = $this->db->insert('call_schedule_head', $data_insert_head);
                    if ($res_insert_head)
                    {
                        $head_id = $this->db->insert_id();
                        //$available = "select l.lead_id from lead l,branch b where l.city_id=b.fk_city_id and b.branch_id='" . $params['branchId'] . "' and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;
                        //$available = "select l.lead_id from lead l where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='" . $params['branchId'] . "') and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;
                        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."')) OR l.fk_branch_id='".$params['branchId']."')";

                        //$available = "select l.lead_id from lead l where $city_condition and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;

                        $available = "select l.lead_id FROM lead l ";
                        if($chosenTagId != '')
                        {
                            $available .= " JOIN lead_xref_tag lxt ON lxt.lead_id = l.lead_id AND lxt.tag_id = ".$chosenTagId;
                        }
                        $available .= " where ".$city_condition;
                        if($chosenSourceId != '')
                        {
                            $available .= " AND l.fk_source_id = ".$chosenSourceId;
                        }
                        $available .= " AND l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;
                        $res = $this->db->query($available);
                        if ($res)
                        {
                            $cnt = $res->num_rows();
                            $result = $res->result();
                            $data_insert_calls = '';
                            foreach ($result as $k_r => $v_r)
                            {
                                $data_insert_calls[] = array("user_id" => $assignedTo, "lead_id" => $v_r->lead_id, "call_schedule_head_id" => $head_id);
                            }
                            if (count($data_insert_calls) > 0)
                            {
                                $res_insert_calls = $this->db->insert_batch('user_xref_lead_due', $data_insert_calls);
                            }
                        }
                        else
                        {

                        }
                    }
                }

            }
        }
        $db_response=array("status"=>true,"message"=>'success',"data"=>array("message"=>'success'));
        return $db_response;
    }
    function releaseCallSchedule($param){

        //{"status":true,"message":"success","data":{"branchId":"1","releaseCnt":"10","releaseBy":"1","assignedUserId":"9"}}
        $status=TRUE;$message='success';$data=array();
        $cnt=0;
        $tobededucted=array();

        //$availableTotal="select  count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." ";
        //$availableTotal="select  count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." ";
        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."')) OR l.fk_branch_id='".$param['branchId']."')";
        $availableTotal="select  count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where $city_condition and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." ";
        $resTotal=$this->db->query($availableTotal);
        if($resTotal)
        {
                $release=0;
                $resultTotal = $resTotal->result();
                foreach ($resultTotal as $kTotal => $vTotal)
                {
                    $release = $vTotal->availCnt;
                }
                if($param['releaseCnt'] > $release)
                {
                    $status = FALSE;
                    $message = 'Cannot release more than assigned calls';
                    $data = array("message" => 'Cannot release more than assigned calls');
                }
                else
                {
                    $release = $param['releaseCnt'];
                    if($release>0)
                    {
                        //$available = "select  uxl.call_schedule_head_id,count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='" . $param['branchId'] . "') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " group by uxl.call_schedule_head_id";
                        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."')) OR l.fk_branch_id='".$param['branchId']."')";
                        //$available = "select  uxl.call_schedule_head_id,count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='" . $param['branchId'] . "') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " group by uxl.call_schedule_head_id";
                        $available = "select  uxl.call_schedule_head_id,count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where $city_condition and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " group by uxl.call_schedule_head_id";
                        $res = $this->db->query($available);
                        if ($res) {
                            $result = $res->result();
                            foreach ($result as $k => $v) {
                                $headId = $v->call_schedule_head_id;
                                $availCnt = $v->availCnt;

                                if ($availCnt < $release) {
                                    $tobededucted[] = array("headId" => $headId, "releaseCount" => $availCnt);
                                    $release = $release - $availCnt;
                                } elseif ($availCnt > $release) {
                                    $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                                    $release = $release - $release;
                                } elseif ($availCnt == $release) {
                                    $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                                    $release = $release - $release;
                                } else {

                                }
                                if ($release == 0) {
                                    break;
                                }
                            }
                            if (count($tobededucted) > 0) {
                                for ($i = 0; $i < count($tobededucted); $i++) {

                                    $callHeadId = $tobededucted[$i]['headId'];
                                    $releaseCount = $tobededucted[$i]['releaseCount'];

                                    $data_release_head['release_count'] = $releaseCount;
                                    $data_release_head['released_by'] = $param['releaseBy'];
                                    $data_release_head['released_on'] = date('Y-m-d H:i:s');
                                    $data_release_head['call_schedule_head_id'] = $callHeadId;
                                    $insert_release_head = $this->db->insert('user_xref_lead_release_head', $data_release_head);
                                    if ($insert_release_head) {
                                        //release head inserted successfully.
                                        $release_head_id = $this->db->insert_id();
                                        //$available="select  l.lead_id from lead l,branch b,user_xref_lead_due uxl where l.city_id=b.fk_city_id and b.branch_id=".$param['branchId']." and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." and uxl.call_schedule_head_id=".$callHeadId." order by l.lead_id asc limit ".$releaseCount;
                                        //$available = "select  l.lead_id from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='" . $param['branchId'] . "') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " and uxl.call_schedule_head_id=" . $callHeadId . " order by l.lead_id asc limit " . $releaseCount;
                                        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."')) OR l.fk_branch_id='".$param['branchId']."')";
                                        //$available = "select  l.lead_id from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='" . $param['branchId'] . "') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " and uxl.call_schedule_head_id=" . $callHeadId . " order by l.lead_id asc limit " . $releaseCount;
                                        $available = "select  l.lead_id from lead l,user_xref_lead_due uxl where $city_condition and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " and uxl.call_schedule_head_id=" . $callHeadId . " order by l.lead_id asc limit " . $releaseCount;
                                        $select_lead_ids = $this->db->query($available);

                                        if ($select_lead_ids) {
                                            //leads fetched successfully
                                            $result_sel_leadIds = $select_lead_ids->result();
                                            $data_insert_releaseCalls = '';
                                            $data_release_leads = '';
                                            foreach ($result_sel_leadIds as $result_sel_leadIds_k => $result_sel_leadIds_v) {
                                                $data_insert_releaseCalls[] = array("user_id" => $param['assignedUserId'], "lead_id" => $result_sel_leadIds_v->lead_id, "head_id" => $callHeadId, "release_head_id" => $release_head_id);
                                                $data_release_leads[] = $result_sel_leadIds_v->lead_id;
                                            }
                                            if ($data_insert_releaseCalls != '') {
                                                //release calls found
                                                $insert_release_calls = $this->db->insert_batch('user_xref_lead_release', $data_insert_releaseCalls);
                                                if ($insert_release_calls) {
                                                    //release calls successfully inserted
                                                    $data_release_leads = array_map('intval', $data_release_leads);
                                                    $data_release_leads = implode(",", $data_release_leads);

                                                    /*$this->db->where_in('lead_id', $data_release_leads);
                                                    $this->db->where('call_schedule_head_id', $callHeadId);
                                                    $this->db->where('user_id', $param['assignedUserId']);
                                                    $delete_due_calls=$this->db->delete('user_xref_lead_due');*/
                                                    $delete_due_calls_query = "DELETE FROM user_xref_lead_due WHERE call_schedule_head_id=$callHeadId AND user_id=" . $param['assignedUserId'] . " AND lead_id in ($data_release_leads)";
                                                    $delete_due_calls = $this->db->query($delete_due_calls_query);
                                                    if ($delete_due_calls) {
                                                        //calls deleted successfully
                                                        $this->db->where('call_schedule_head_id', $callHeadId);
                                                        $this->db->where('assigned_to', $param['assignedUserId']);
                                                        $this->db->set('total_scheduled_calls', 'total_scheduled_calls-' . $releaseCount, FALSE);
                                                        $update_call_head = $this->db->update('call_schedule_head');
                                                        if ($update_call_head) {
                                                            //successfully updated call head counts
                                                            $status = TRUE;
                                                            $message = 'Released successfully';
                                                            $data = array();

                                                        } else {
                                                            //problem in updating count of call head
                                                            $error = $this->db->error();
                                                            $status = FALSE;
                                                            $message = 'problem in updating count of call head';
                                                            $data = array("message" => $error['message']);
                                                        }
                                                    } else {
                                                        //problem in deleting calls from dues
                                                        $error = $this->db->error();
                                                        $status = FALSE;
                                                        $message = 'problem in deleting calls from dues';
                                                        $data = array("message" => $error['message']);
                                                    }

                                                } else {
                                                    //problem in batch insert of release calls
                                                    $error = $this->db->error();
                                                    $status = FALSE;
                                                    $message = 'problem in batch insert of release calls';
                                                    $data = array("message" => $error['message']);
                                                }
                                            } else {
                                                //no release calls found
                                                $error = $this->db->error();
                                                $status = FALSE;
                                                $message = 'no release calls found';
                                                $data = array("message" => $error['message']);
                                            }
                                        } else {
                                            //problem in fetching leads for given head
                                            $error = $this->db->error();
                                            $status = FALSE;
                                            $message = 'problem in fetching leads for given head';
                                            $data = array("message" => $error['message']);
                                        }

                                    } else {
                                        //problem in insertion of relese head id
                                        $error = $this->db->error();
                                        $status = FALSE;
                                        $message = 'problem in insertion of release head id';
                                        $data = array("message" => $error['message']);
                                    }
                                }
                                $multi_db_response[] = array("status" => $status, "message" => $message, "data" => $data);
                            } else {
                                //no availabled heads found
                                $error = $this->db->error();
                                $status = FALSE;
                                $message = 'no available heads found';
                                $data = array("message" => $error['message']);
                            }
                        } else {
                            //problem in fetching the available lead heads
                            $error = $this->db->error();
                            $status = FALSE;
                            $message = 'problem in fetching the available lead heads';
                            $data = array("message" => $error['message']);
                        }
                    }
                    else{
                        $status = FALSE;
                        $message = 'No releases found';
                        $data = array("message" => 'No releases found');
                    }
                }
        }
        else
        {
            $error = $this->db->error();
            $status = FALSE;
            $message = 'problem in fetching the total available leads';
            $data = array("message" => $error['message']);
        }
        if($status == false)
        {
            $db_response=array("status"=>$status,"message"=>$message,"data"=>$data);
        }
        else
        {
            $message="Released successfully";
            $multi_db_response=array("message"=>$message);
            $db_response=array("status"=>TRUE,"message"=>$message,"data"=>$multi_db_response);
        }
        return $db_response;

    }
    function releaseCallScheduleByUser($param)
    {

        //{"status":true,"message":"success","data":{"branchId":"1","releaseCnt":"10","releaseBy":"1","assignedUserId":"9"}}
        $status = TRUE;
        $message = 'success';
        $data = array();
        $cnt = 0;
        $tobededucted = array();

        $availableTotal = "select  count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " ";
        $resTotal = $this->db->query($availableTotal);
        if ($resTotal) {
            $release = 0;
            $resultTotal = $resTotal->result();
            foreach ($resultTotal as $kTotal => $vTotal) {
                $release = $vTotal->availCnt;
            }
            if ($release > 0) {
                $available = "select  uxl.call_schedule_head_id,count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " group by uxl.call_schedule_head_id";
                $res = $this->db->query($available);
                if ($res) {
                    $result = $res->result();
                    foreach ($result as $k => $v) {
                        $headId = $v->call_schedule_head_id;
                        $availCnt = $v->availCnt;

                        if ($availCnt < $release) {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $availCnt);
                            $release = $release - $availCnt;
                        } elseif ($availCnt > $release) {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                            $release = $release - $release;
                        } elseif ($availCnt == $release) {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                            $release = $release - $release;
                        } else {

                        }
                        if ($release == 0) {
                            break;
                        }
                    }
                    if (count($tobededucted) > 0) {
                        for ($i = 0; $i < count($tobededucted); $i++) {

                            $callHeadId = $tobededucted[$i]['headId'];
                            $releaseCount = $tobededucted[$i]['releaseCount'];

                            $data_release_head['release_count'] = $releaseCount;
                            $data_release_head['released_by'] = $param['releaseBy'];
                            $data_release_head['released_on'] = date('Y-m-d H:i:s');
                            $data_release_head['call_schedule_head_id'] = $callHeadId;
                            $insert_release_head = $this->db->insert('user_xref_lead_release_head', $data_release_head);
                            if ($insert_release_head) {
                                //release head inserted successfully.
                                $release_head_id = $this->db->insert_id();
                                $available = "select  l.lead_id from lead l,user_xref_lead_due uxl where l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " and uxl.call_schedule_head_id=" . $callHeadId . " order by l.lead_id asc limit " . $releaseCount;
                                $select_lead_ids = $this->db->query($available);

                                if ($select_lead_ids) {
                                    //leads fetched successfully
                                    $result_sel_leadIds = $select_lead_ids->result();
                                    $data_insert_releaseCalls = '';
                                    $data_release_leads = '';
                                    foreach ($result_sel_leadIds as $result_sel_leadIds_k => $result_sel_leadIds_v) {
                                        $data_insert_releaseCalls[] = array("user_id" => $param['assignedUserId'], "lead_id" => $result_sel_leadIds_v->lead_id, "head_id" => $callHeadId, "release_head_id" => $release_head_id);
                                        $data_release_leads[] = $result_sel_leadIds_v->lead_id;
                                    }
                                    if ($data_insert_releaseCalls != '') {
                                        //release calls found
                                        $insert_release_calls = $this->db->insert_batch('user_xref_lead_release', $data_insert_releaseCalls);
                                        if ($insert_release_calls) {
                                            //release calls successfully inserted
                                            $data_release_leads = array_map('intval', $data_release_leads);
                                            $data_release_leads = implode(",", $data_release_leads);

                                            /*$this->db->where_in('lead_id', $data_release_leads);
                                            $this->db->where('call_schedule_head_id', $callHeadId);
                                            $this->db->where('user_id', $param['assignedUserId']);
                                            $delete_due_calls=$this->db->delete('user_xref_lead_due');*/
                                            $delete_due_calls_query = "DELETE FROM user_xref_lead_due WHERE call_schedule_head_id=$callHeadId AND user_id=" . $param['assignedUserId'] . " AND lead_id in ($data_release_leads)";
                                            $delete_due_calls = $this->db->query($delete_due_calls_query);
                                            if ($delete_due_calls) {
                                                //calls deleted successfully
                                                $this->db->where('call_schedule_head_id', $callHeadId);
                                                $this->db->where('assigned_to', $param['assignedUserId']);
                                                $this->db->set('total_scheduled_calls', 'total_scheduled_calls-' . $releaseCount, FALSE);
                                                $update_call_head = $this->db->update('call_schedule_head');
                                                if ($update_call_head) {
                                                    //successfully updated call head counts
                                                    $status = TRUE;
                                                    $message = 'Released successfully';
                                                    $data = array();

                                                } else {
                                                    //problem in updating count of call head
                                                    $error = $this->db->error();
                                                    $status = FALSE;
                                                    $message = 'problem in updating count of call head';
                                                    $data = array("message" => $error['message']);
                                                }
                                            } else {
                                                //problem in deleting calls from dues
                                                $error = $this->db->error();
                                                $status = FALSE;
                                                $message = 'problem in deleting calls from dues';
                                                $data = array("message" => $error['message']);
                                            }

                                        } else {
                                            //problem in batch insert of release calls
                                            $error = $this->db->error();
                                            $status = FALSE;
                                            $message = 'problem in batch insert of release calls';
                                            $data = array("message" => $error['message']);
                                        }
                                    } else {
                                        //no release calls found
                                        $error = $this->db->error();
                                        $status = FALSE;
                                        $message = 'no release calls found';
                                        $data = array("message" => $error['message']);
                                    }
                                } else {
                                    //problem in fetching leads for given head
                                    $error = $this->db->error();
                                    $status = FALSE;
                                    $message = 'problem in fetching leads for given head';
                                    $data = array("message" => $error['message']);
                                }

                            } else {
                                //problem in insertion of relese head id
                                $error = $this->db->error();
                                $status = FALSE;
                                $message = 'problem in insertion of release head id';
                                $data = array("message" => $error['message']);
                            }
                        }
                        $multi_db_response[] = array("status" => $status, "message" => $message, "data" => $data);
                    } else {
                        //no availabled heads found
                        $error = $this->db->error();
                        $status = FALSE;
                        $message = 'no available heads found';
                        $data = array("message" => $error['message']);
                    }
                } else {
                    //problem in fetching the available lead heads
                    $error = $this->db->error();
                    $status = FALSE;
                    $message = 'problem in fetching the available lead heads';
                    $data = array("message" => $error['message']);
                }
            } else {
                $status = FALSE;
                $message = 'No releases found';
                $data = array("message" => 'No releases found');
            }
        } else {
            $error = $this->db->error();
            $status = FALSE;
            $message = 'problem in fetching the total available leads';
            $data = array("message" => $error['message']);
        }

        $message = "Released successfully";
        $multi_db_response = array("message" => $message);
        $db_response = array("status" => TRUE, "message" => $message, "data" => $multi_db_response);
        return $db_response;

    }
}