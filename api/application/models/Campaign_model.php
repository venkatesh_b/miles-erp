<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-25
 * Time: 10:53 AM
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Campaign_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    public function getBranchList() {
        $qry = "select b.branch_id id, b.name name from branch b where b.is_active=1 ";

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function checkCampaignNameExist($cname,$cid)
    {
        $this->db->select('campaign_id');
        $this->db->from('campaign');
        $this->db->where('name',$cname);
        if($cid !=0)
            $this->db->where('campaign_id !=',$cid);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $existStatus = false;
        }
        else
        {
            $existStatus = true;
        }
        return $existStatus;
    }
    public function addCampaign($data)
    {
        $query = $this->checkCampaignNameExist($data['name'],$data['campaignId']);
        if($query === false)
        {
            $db_response=array("status"=>false,"message"=>'Campaign name already exist',"data"=>array('campaignName'=>'Campaign name already exist'));
        }
        else
        {
            $campaign_groups = $data['campaign_groups'];
            $campaign_id = $data['campaignId'];
            if($campaign_id == 0)
            {
                $campaign_data = array(
                    'name' =>$data['name'],
                    'fk_branch_id' => $data['fk_branch_id'],
                    'start_date' => $data['start_date'],
                    'end_date' => $data['end_date'],
                    'description' => $data['description'],
                    'template_path' => $data['template_path'],
                    'fk_created_by' => $data['fk_created_by'],
                    'created_date' => $data['created_date']
                );
                $res_insert=$this->db->insert('campaign', $campaign_data);
                $status = ($this->db->affected_rows() != 1) ? false : true;
                if($status == true)
                {
                    $campaign_id=$this->db->insert_id();
                    for($t=0;$t<count($campaign_groups);$t++)
                    {
                        $campaign_group_data = array(
                            'fk_campaign_id' => $campaign_id,
                            'fk_group_id' => $campaign_groups[$t]
                        );
                        $this->db->insert('campaign_xref_group', $campaign_group_data);
                    }
                    $this->db->select('fk_lead_id,email,"'.$campaign_id.'" as fk_campaign_id,"'.$data['fk_created_by'].'" as fk_created_by,"'.$data['created_date'].'" as created_date');
                    $this->db->from('group_contact gc');
                    $this->db->where_in('gc.fk_group_id', $campaign_groups);
                    $this->db->where('gc.email !=', '');
                    $branch_data = $this->db->get();
                    $contactsdata = $branch_data->result_array();

                    $this->db->insert_batch('campaign_contact', $contactsdata);

                    //Sendgrid integration starts

                    $sg = new \SendGrid('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');
                    //Fetching the contacts and sending it to sendgrid and fetching the sendgrid ids of contacts
                    $this->db->select('cc.email,l.`name` as first_name,"" as last_name');
                    $this->db->from('campaign_contact cc');
                    $this->db->join('lead l','l.lead_id=cc.fk_lead_id');
                    $this->db->where_in('cc.fk_campaign_id', $campaign_id);
                    $campaign_contact_data = $this->db->get();
                    $Campaign_contactsdata = $campaign_contact_data->result_array();
                    $contact_list_response = $sg->client->contactdb()->recipients()->post($Campaign_contactsdata);
                    $contact_list = json_decode($contact_list_response->body());
                    $receipientIDs=$contact_list->persisted_recipients;

                    //creating a list name for the campaign
                    $request_body = json_decode('{
                        "name": "'.$data['name'].'"
                        }');
                    $campaign_list_response = $sg->client->contactdb()->lists()->post($request_body);
                    if($campaign_list_response->statusCode() == 200 || $campaign_list_response->statusCode() == 201)
                    {
                        $campaign_list = json_decode($campaign_list_response->body());
                        $sendgrid_campaign_list_id = $campaign_list->id;

                        $request_body = $receipientIDs;
                        $campaign_contact_list_response = $sg->client->contactdb()->lists()->_($sendgrid_campaign_list_id)->recipients()->post($request_body);

                        if($campaign_contact_list_response->statusCode() == 200 || $campaign_contact_list_response->statusCode() == 201)
                        {
                            //sendgrid campaign creation
                            $campaign_template_content = trim(file_get_contents(BASE_URL.$data['template_path']));
                            $campaign_template_plain_content = trim(strip_tags($campaign_template_content));

                            $campaign_template_content = $campaign_template_content. ' <a href="[unsubscribe]">click here to unsubscribe</a>';
                            $campaign_content_data = str_replace("\"","'",$campaign_template_content);
                            $output = str_replace(array("\r\n", "\r"), "\n", $campaign_content_data);
                            $lines = explode("\n", $output);
                            $new_lines = array();
                            foreach ($lines as $i => $line) {
                                if(!empty($line))
                                    $new_lines[] = trim($line);
                            }
                            $campaign_content_data = implode($new_lines);
                            $request_body = json_decode('{
                            "categories":["campaigns"],
                            "custom_unsubscribe_url": "'.BASE_URL.'unsubscribe",
                            "html_content": "'.$campaign_content_data.'",
                            "ip_pool": "",
                            "list_ids": ['.$sendgrid_campaign_list_id.'],
                            "plain_content": "'.$campaign_template_plain_content.' [unsubscribe]",
                            "segment_ids": [],
                            "sender_id": 44910,
                            "subject": "Miles - '.$data['name'].'",
                            "title": "'.$data['name'].'"
                            }');
                            $campaign_response = $sg->client->campaigns()->post($request_body);
                            $campaign_response = json_decode($campaign_response->body());

                            $sendgrid_campaign_id=$campaign_response->id;


                            //$campaign_start=strtotime($data['start_date']);
                            if (date('Y-m-d') == date('Y-m-d', strtotime($data['start_date']))) {
                                $campaign_scheduled_time=strtotime(date('Y-m-d H:i:s'));
                            }
                            else
                            {
                                $campaign_scheduled_time=strtotime($data['start_date']);
                            }

                            $campaign_schedule_request = json_decode('{
                            "send_at": "'.$campaign_scheduled_time.'"
                            }');
                            //$campaign_id = $sendgrid_campaign_id;
                            $schedule_campaign_response = $sg->client->campaigns()->_($sendgrid_campaign_id)->schedules()->post($campaign_schedule_request);

                            $campaign_data = array(
                                'sendgrid_campaign_id' => $sendgrid_campaign_id,
                                'sendgrid_campaign_list_id' => $sendgrid_campaign_list_id
                            );
                            $this->db->where('campaign_id',$campaign_id);
                            $this->db->update('campaign',$campaign_data);
                        }

                    }
                    //End of sendgrid integration



                    $db_response=array("status"=>true,"message"=>'Campaign saved successfully',"data"=>array());
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
            else
            {
                $campaign_data = array(
                    'name' =>$data['name'],
                    'fk_branch_id' => $data['fk_branch_id'],
                    'start_date' => $data['start_date'],
                    'end_date' => $data['end_date'],
                    'description' => $data['description'],
                    'template_path' => $data['template_path'],
                    'fk_updated_by' => $data['fk_created_by'],
                    'updated_date' => $data['created_date']
                );
                $this->db->where('campaign_id',$campaign_id);
                $this->db->update('campaign',$campaign_data);
                $status = ($this->db->affected_rows() != 1) ? false : true;
                if($status == true)
                {
                    for($t=0;$t<count($campaign_groups);$t++)
                    {
                        $this->db->where('fk_campaign_id',$campaign_id);
                        $this->db->where('fk_group_id',$campaign_groups[$t]);
                        $campaign_group = $this->db->get('campaign_xref_group');
                        if ( $campaign_group->num_rows() == 0 )
                        {
                            $campaign_group_data = array(
                                'fk_campaign_id' => $campaign_id,
                                'fk_group_id' => $campaign_groups[$t]
                            );
                            $this->db->insert('campaign_xref_group', $campaign_group_data);
                            $status = ($this->db->affected_rows() != 1) ? false : true;
                            if($status == true)
                            {
                                $this->db->select('fk_lead_id,email,"'.$campaign_id.'" as fk_campaign_id,"'.$data['fk_created_by'].'" as fk_created_by,"'.$data['created_date'].'" as created_date');
                                $this->db->from('group_contact gc');
                                $this->db->where('gc.fk_group_id', $campaign_groups[$t]);
                                $this->db->where('gc.email !=', '');
                                $branch_data = $this->db->get();
                                $contactsdata = $branch_data->result_array();
                                $this->db->insert_batch('campaign_contact', $contactsdata);
                                if(($this->db->affected_rows() < 1))
                                {
                                    $error = $this->db->error();
                                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                                }
                            }
                            else
                            {
                                $error = $this->db->error();
                                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                            }
                        }
                    }

                    $db_response=array("status"=>true,"message"=>'Campaign updated successfully',"data"=>array());
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
        }
        return $db_response;
    }

    public function campaignDataTables()
    {
        $table = 'campaign';
        // Table's primary key
        $primaryKey = 'cp`.`campaign_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`cp`.`campaign_id`',                    'dt' => 'campaign_id',      'field' => 'campaign_id' ),
            array( 'db' => '`cp`.`name` as campaign',                           'dt' => 'campaign',         'field' => 'campaign' ),
            array( 'db' => '`br`.`name` as branchName',                           'dt' => 'branchName',           'field' => 'branchName' ),
            array( 'db' => 'count(gc.fk_lead_id) as `totalContacts`',      'dt' => 'totalContacts',    'field' => 'totalContacts' ),
            array( 'db' => '`cp`.`start_date`',                     'dt' => 'start_date',       'field' => 'start_date', 'formatter' => function( $d, $row ) {
                    return date('d M,Y', strtotime($d));
                }),
            array( 'db' => '`cp`.`end_date`',                       'dt' => 'end_date',         'field' => 'end_date', 'formatter' => function( $d, $row ) {
                    return date('d M,Y', strtotime($d));
                }),
            array( 'db' => '`cp`.`status`',                         'dt' => 'status',           'field' => 'status' ),
            array( 'db' => '`cp`.`campaign_id`',                    'dt' => 'campaign_id',      'field' => 'campaign_id' ),
            array( 'db' => 'date(`cp`.`start_date`) as start_date_org',                    'dt' => 'start_date_org',      'field' => 'start_date_org' ),
            );

        $globalFilterColumns = array(
            array( 'db' => '`cp`.`name`',                           'dt' => '`name`',         'field' => '`name`' ),
            array( 'db' => '`cp`.`start_date`',                     'dt' => 'start_date',       'field' => 'start_date' ),
            array( 'db' => '`cp`.`end_date`',                       'dt' => 'end_date',         'field' => 'end_date'),
            array( 'db' => '`cp`.`status`',                         'dt' => 'status',           'field' => 'status' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM campaign cp LEFT JOIN branch br ON cp.fk_branch_id=br.branch_id LEFT JOIN campaign_xref_group cg ON cg.fk_campaign_id=cp.campaign_id LEFT JOIN group_contact gc ON gc.fk_group_id=cg.fk_group_id ";
        $extraWhere = "";
        $groupBy = "`cp`.`campaign_id`";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));
        $delete_status=['created','approved','rejected'];
        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['is_editable']=0;
            $responseData['data'][$i]['is_deletable']=0;
            if(in_array($responseData['data'][$i]['status'],$delete_status) && strtotime($responseData['data'][$i]['start_date_org'])>strtotime(date('Y-m-d'))){
                $responseData['data'][$i]['is_editable']=1;
                $responseData['data'][$i]['is_deletable']=1;
            }

        }
        return $responseData;
    }

    public function GroupDataTable()
    {
        $table = 'group';
        // Table's primary key
        $primaryKey = 'g`.`group_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`g`.`name` as groupName',                              'dt' => 'groupName',     'field' => 'groupName' ),
            array( 'db' => '`g`.`description`',                                    'dt' => 'description',   'field' => 'description' ),
            array( 'db' => 'count(gc.group_contact_id) as contacts',               'dt' => 'contacts',      'field' => 'contacts' ),
            array( 'db' => '`g`.`group_id`',                                       'dt' => 'group_id',      'field' => 'group_id' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`g`.`name`',            'dt' => 'name',             'field' => 'name' ),
            array( 'db' => '`g`.`description`',     'dt' => 'description',      'field' => 'description' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM `group` g LEFT JOIN group_contact gc ON g.group_id=gc.fk_group_id ";
        $extraWhere = "";
        $groupBy = "`g`.`group_id`";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));
        return $responseData;
    }

    function getGroupItemsList($groupItems)
    {
        $groupItemsData=array();
        for($i=0;$i<count($groupItems);$i++)
        {
            if($groupItems[$i]=='Branch')
            {
                //SELECT course_id,`name` FROM course WHERE is_active=1;
                $this->db->select('branch_id as Id,`name` as Name');
                $this->db->from('branch');
                $this->db->where('is_active',1);
            }
            elseif($groupItems[$i]=='Course')
            {
                //SELECT course_id,`name` FROM course WHERE is_active=1;
                $this->db->select('course_id as Id,`name` as Name');
                $this->db->from('course');
                $this->db->where('is_active',1);
            }
            else
            {
                $this->db->select('rtv.reference_type_value_id as Id,rtv.`value` as Name');
                $this->db->from('reference_type rt');
                $this->db->join('reference_type_value rtv', 'rtv.reference_type_id=rt.reference_type_id');
                $this->db->where('rt.`name`',$groupItems[$i]);
                $this->db->where('rtv.is_active',1);
            }
            $groupitem_data = $this->db->get();
            if($this->db->count_all_results()>0)
            {
                $groupItemsData[$groupItems[$i]] = $groupitem_data->result_array();
            }
        }
        return $groupItemsData;
    }
    function checkGroupName($groupName,$groupId=0)
    {
        $this->db->select('group_id');
        $this->db->from('group');
        if($groupName!='')
        $this->db->where('`name`',$groupName);
        if($groupId >0 )
            $this->db->where('`group_id`',$groupId);
        $groupitem_data = $this->db->get();
        return $groupitem_data->num_rows();
    }
    function checkEditGroupName($groupName,$groupId=0)
    {
        $this->db->select('group_id');
        $this->db->from('group');
        $this->db->where('`name`',$groupName);
        if($groupId >0 )
            $this->db->where('`group_id`!=',$groupId);
        $groupitem_data = $this->db->get();
        return $groupitem_data->num_rows();
    }
    function getSelectedGroupContacts($groupItems)
    {
        $branchData=$groupItems['branch'];
        $this->db->select('branch_id,fk_city_id');
        $this->db->from('branch b');
        $this->db->where_in('b.branch_id', $branchData);
        $branch_data = $this->db->get();
        $branchdata = $branch_data->result_array();
        $cityIds = array_column($branchdata, 'fk_city_id');

        $this->db->select('ld.lead_id,ld.`name`,ld.email,GROUP_CONCAT(rtv.`value` separator ", ") as tags');
        $this->db->from('lead ld');
        $this->db->join('lead_xref_tag lxt','lxt.lead_id=ld.lead_id');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=lxt.tag_id');
        if(isset($groupItems['source']) && count($groupItems['source'])>0)
        {
            $this->db->join('reference_type_value src','src.reference_type_value_id=ld.fk_source_id');
        }
        if(isset($groupItems['tag']) && count($groupItems['tag'])>0)
        {

        }
        if(isset($groupItems['course']) && count($groupItems['course'])>0)
        {
            $this->db->join('branch_xref_lead bxl','bxl.lead_id=ld.lead_id');
        }
        $this->db->where('ld.email !=','');
        $where="(ld.city_id IN (".implode(',',$cityIds).") OR ld.fk_branch_id IN (".implode(',',$branchData).") )";
        $this->db->where($where);
        //$this->db->or_where_in('ld.fk_branch_id',$branchData);
        if(isset($groupItems['source']) && count($groupItems['source'])>0)
        {
            $this->db->where_in('src.reference_type_value_id',$groupItems['source']);
        }
        if(isset($groupItems['tag']) && count($groupItems['tag'])>0)
        {
            $this->db->where_in('lxt.tag_id',$groupItems['tag']);
        }
        if(isset($groupItems['course']) && count($groupItems['course'])>0)
        {
            $this->db->where_in('bxl.fk_course_id',$groupItems['course']);
            $this->db->where_in('bxl.is_active', 1);
        }

        $this->db->group_by('ld.lead_id');
        $this->db->order_by('ld.name', 'asc');

        $branch_data = $this->db->get();
        return $branch_data->result_array();
        //return $this->db->last_query();
    }

    function addGroup($groupName,$groupDescription,$groupItems,$groupContactIds)
    {
        $status=true;
        $groupContactIds=array_values($groupContactIds);
        $message = 'Group added successfully';
        $test = '111';
        $groupData = array(
                        'name'          =>  $groupName,
                        'description'   =>  $groupDescription,
                        'fk_created_by' =>  $_SERVER['HTTP_USER'],
                        'created_date'  =>  date('Y-m-d H:i:s')
        );
        //$afftectedRows = $this->db->affected_rows();
        $this->db->insert('group', $groupData);
        $status = ($this->db->affected_rows() != 1) ? false : true;
        if($status == true)
        {
            $group_id=$this->db->insert_id();
            //Adding selected group items table (selcted branch,course,source,tag etc.,)
            if(isset($groupItems['branch']) && count($groupItems['branch'])>0)
            {
                $sourceData = array();
                for($i=0;$i<count($groupItems['branch']);$i++)
                {
                    $sourceData[$i]['item_type']='branch';
                    $sourceData[$i]['fk_group_id']=$group_id;
                    $sourceData[$i]['item_id']=$groupItems['branch'][$i];
                }
                $this->db->insert_batch('group_item', $sourceData);
                $status = ($this->db->affected_rows() < 1) ? false : true;
                if($status == true)
                {
                    $status=true;
                }
                else
                {
                    $status=false;
                    $message="Error at branch insertion";
                }
            }
            if(isset($groupItems['course']) && count($groupItems['course'])>0)
            {
                $sourceData = array();
                for($i=0;$i<count($groupItems['course']);$i++)
                {
                    $sourceData[$i]['item_type']='course';
                    $sourceData[$i]['fk_group_id']=$group_id;
                    $sourceData[$i]['item_id']=$groupItems['course'][$i];
                }
                if($status == true)
                {
                    $this->db->insert_batch('group_item', $sourceData);
                    $status = ($this->db->affected_rows() < 1) ? false : true;
                    if($status == true)
                    {
                        $status=true;
                    }
                    else
                    {
                        $status=false;
                        $message="Error at course insertion";
                    }
                }
            }

            if(isset($groupItems['source']) && count($groupItems['source'])>0)
            {
                $sourceData = array();
                for($i=0;$i<count($groupItems['source']);$i++)
                {
                    $sourceData[$i]['item_type']='source';
                    $sourceData[$i]['fk_group_id']=$group_id;
                    $sourceData[$i]['item_id']=$groupItems['source'][$i];
                }
                if($status == true)
                {
                    //if($this->db->insert_batch('group_item', $sourceData))
                    $this->db->insert_batch('group_item', $sourceData);
                    $status = ($this->db->affected_rows() < 1) ? false : true;
                    if($status == true)
                    {
                        $status=true;
                    }
                    else
                    {
                        $status=false;
                        $message="Error at source insertion";
                    }
                }
            }

            if(isset($groupItems['tag']) && count($groupItems['tag'])>0)
            {
                $sourceData = array();
                for($i=0;$i<count($groupItems['tag']);$i++)
                {
                    $sourceData[$i]['item_type']='tag';
                    $sourceData[$i]['fk_group_id']=$group_id;
                    $sourceData[$i]['item_id']=$groupItems['tag'][$i];
                }
                if($status == true)
                {
                    //if($this->db->insert_batch('group_item', $sourceData))
                    $this->db->insert_batch('group_item', $sourceData);
                    $status = ($this->db->affected_rows() < 1) ? false : true;
                    if($status == true)
                    {
                        $status=true;
                    }
                    else
                    {
                        $status=false;
                        $message="Error at tag insertion";
                    }
                }
            }

            //Inserting selected contacts from group contacts tables

            if(count($groupContactIds)>0)
            {
                $contactData = array();
                for($c=0;$c<count($groupContactIds);$c++)
                {
                    $contactData[$c]['fk_group_id']=$group_id;
                    $contactData[$c]['fk_lead_id']=$groupContactIds[$c]['id'];
                    $contactData[$c]['email']=$groupContactIds[$c]['email'];
                }
                if($status == true)
                {
                    //if($this->db->insert_batch('group_contact', $contactData))
                    $this->db->insert_batch('group_contact', $contactData);
                    $status = ($this->db->affected_rows() < 1) ? false : true;
                    if($status == true)
                    {
                        $status=true;
                    }
                    else
                    {
                        $status=false;
                        $message="Error at contact insertion";
                    }
                }
            }
            if($status == false)
            {
                $this->db->where('group_id', $group_id);
                $this->db->delete('group');

                $this->db->where('fk_group_id', $group_id);
                $this->db->delete('group_item');

                $this->db->where('fk_group_id', $group_id);
                $this->db->delete('group_contact');

                //$message="Insertion failed. Please try again";
            }
        }
        else
        {
            $status=false;
            $message="Insertion failed. Please try again";
        }

        $addGroupResponse=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>[]
        );
        return $addGroupResponse;
    }

    public function getGroupDetailsById($groupId)
    {
        $group_details =   [];
        $this->db->select('group_id,name,description');
        $this->db->from('group');
        $this->db->where('`group_id`',$groupId);
        $group_data = $this->db->get();
        if($group_data->num_rows() == 0)
        {
            $status=false;
            $message="Group doesn't exists";
        }
        else
        {
            $status=true;
            $message="Group details";
            $groupData = $group_data->result_array();
            $group_details['basic'] = $groupData[0];
            $this->db->select('group_concat(item_id) as items,item_type');
            $this->db->from('group_item');
            $this->db->where('`fk_group_id`',$groupId);
            $this->db->group_by('`item_type`');
            $group_item_data = $this->db->get();
            $group_details['items'] = $group_item_data->result();
        }
        $response=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>$group_details
        );
        return $response;
    }
    function getSelectedGroupContactsEdit($groupId)
    {

        $this->db->select('ld.lead_id,ld.`name`,gc.email,GROUP_CONCAT(rtv.`value` separator ", ") as tags');
        $this->db->from('group_contact gc');
        $this->db->join('lead ld','`ld`.`lead_id`=`gc`.`fk_lead_id`');
        $this->db->join('lead_xref_tag lxt','lxt.lead_id=ld.lead_id');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=lxt.tag_id');
        $this->db->where('gc.fk_group_id=',$groupId);
        $this->db->group_by('gc.group_contact_id');
        $this->db->order_by('ld.name', 'asc');

        $branch_data = $this->db->get();
        return $branch_data->result_array();
        //return $this->db->last_query();
    }
    function editGroup($groupName,$groupDescription,$groupItems,$groupContactIds,$groupId)
    {
        $status=true;
        $message = 'Group updated successfully';
        $test = '111';
        $groupData = array(
            'name'          =>  $groupName,
            'description'   =>  $groupDescription,
            'fk_updated_by' =>  $_SERVER['HTTP_USER'],
            'updated_date'  =>  date('Y-m-d H:i:s')
        );
        //$afftectedRows = $this->db->affected_rows();
        $this->db->where('group_id',$groupId);
        $this->db->update('group', $groupData);
        $status = ($this->db->affected_rows() != 1) ? false : true;

        if($status == true)
        {

            $group_id=$groupId;

            $this->db->where('fk_group_id',$group_id);
            $this->db->delete('group_item');


            $this->db->where('fk_group_id',$group_id);
            $this->db->delete('group_contact');

            //Adding selected group items table (selcted branch,course,source,tag etc.,)

            if(isset($groupItems['branch']) && count($groupItems['branch'])>0)
            {
                $sourceData = array();
                for($i=0;$i<count($groupItems['branch']);$i++)
                {
                    $sourceData[$i]['item_type']='branch';
                    $sourceData[$i]['fk_group_id']=$group_id;
                    $sourceData[$i]['item_id']=$groupItems['branch'][$i];
                }
                $this->db->insert_batch('group_item', $sourceData);
                $status = ($this->db->affected_rows() < 1) ? false : true;
                if($status == true)
                {
                    $status=true;
                }
                else
                {
                    $status=false;
                    $message="Error at branch insertion";
                }
            }
            if(isset($groupItems['course']) && count($groupItems['course'])>0)
            {
                $sourceData = array();
                for($i=0;$i<count($groupItems['course']);$i++)
                {
                    $sourceData[$i]['item_type']='course';
                    $sourceData[$i]['fk_group_id']=$group_id;
                    $sourceData[$i]['item_id']=$groupItems['course'][$i];
                }
                if($status == true)
                {
                    $this->db->insert_batch('group_item', $sourceData);
                    $status = ($this->db->affected_rows() < 1) ? false : true;
                    if($status == true)
                    {
                        $status=true;
                    }
                    else
                    {
                        $status=false;
                        $message="Error at course insertion";
                    }
                }
            }

            if(isset($groupItems['source']) && count($groupItems['source'])>0)
            {
                $sourceData = array();
                for($i=0;$i<count($groupItems['source']);$i++)
                {
                    $sourceData[$i]['item_type']='source';
                    $sourceData[$i]['fk_group_id']=$group_id;
                    $sourceData[$i]['item_id']=$groupItems['source'][$i];
                }
                if($status == true)
                {
                    //if($this->db->insert_batch('group_item', $sourceData))
                    $this->db->insert_batch('group_item', $sourceData);
                    $status = ($this->db->affected_rows() < 1) ? false : true;
                    if($status == true)
                    {
                        $status=true;
                    }
                    else
                    {
                        $status=false;
                        $message="Error at source insertion";
                    }
                }
            }

            if(isset($groupItems['tag']) && count($groupItems['tag'])>0)
            {
                $sourceData = array();
                for($i=0;$i<count($groupItems['tag']);$i++)
                {
                    $sourceData[$i]['item_type']='tag';
                    $sourceData[$i]['fk_group_id']=$group_id;
                    $sourceData[$i]['item_id']=$groupItems['tag'][$i];
                }
                if($status == true)
                {
                    //if($this->db->insert_batch('group_item', $sourceData))
                    $this->db->insert_batch('group_item', $sourceData);
                    $status = ($this->db->affected_rows() < 1) ? false : true;
                    if($status == true)
                    {
                        $status=true;
                    }
                    else
                    {
                        $status=false;
                        $message="Error at tag insertion";
                    }
                }
            }

            //Inserting selected contacts from group contacts tables
            if(count($groupContactIds)>0)
            {
                $contactData = array();
                for($c=0;$c<count($groupContactIds);$c++)
                {
                    $contactData[$c]['fk_group_id']=$group_id;
                    $contactData[$c]['fk_lead_id']=$groupContactIds[$c]['id'];
                    $contactData[$c]['email']=$groupContactIds[$c]['email'];
                }
                if($status == true)
                {
                    //if($this->db->insert_batch('group_contact', $contactData))
                    $this->db->insert_batch('group_contact', $contactData);
                    $status = ($this->db->affected_rows() < 1) ? false : true;
                    if($status == true)
                    {
                        $status=true;
                    }
                    else
                    {
                        $status=false;
                        $message="Error at contact insertion";
                    }
                }
            }
            if($status == false)
            {
                $this->db->where('group_id', $group_id);
                $this->db->delete('group');

                $this->db->where('fk_group_id', $group_id);
                $this->db->delete('group_item');

                $this->db->where('fk_group_id', $group_id);
                $this->db->delete('group_contact');

                //$message="Insertion failed. Please try again";
            }
        }
        else
        {
            $status=false;
            $message="Insertion failed. Please try again";
        }

        $addGroupResponse=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>[]
        );
        return $addGroupResponse;
    }

    public function getCampaignGroupsByBranchId($branchId)
    {
        $groupData =   [];
        $this->db->select('gp.group_id,gp.name');
        $this->db->from('group gp');
        $this->db->join('group_item gi','gp.group_id=gi.fk_group_id');
        $this->db->where('gi.item_type','branch');
        $this->db->where('gi.item_id',$branchId);
        $group_data = $this->db->get();
        if($group_data->num_rows() == 0)
        {
            $status=false;
            $message="Create campaign group before campaign creation";
        }
        else
        {
            $status=true;
            $message="Group details";
            $groupData = $group_data->result_array();
        }
        $response=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>$groupData
        );
        return $response;
    }
    public function getContactsCountByGroupIds($groupIds)
    {
        $groupsData =   [];
        $this->db->select('gp.group_id,gp.name as groupName,COUNT(gc.group_contact_id) as contacts');
        $this->db->from('group gp');
        $this->db->join('group_contact gc','gp.group_id=gc.fk_group_id','left');
        $this->db->where_in('gp.group_id',$groupIds);
        $this->db->group_by('gp.group_id');
        $groups_data = $this->db->get();
        if($groups_data->num_rows() == 0)
        {
            $status=false;
            $message="Group details not there";
        }
        else
        {
            $status=true;
            $message="Group list";
            $groupsData = $groups_data->result_array();
        }
        $response=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>$groupsData
        );
        return $response;

    }

    function getCampaignDetailsById($campaignId)
    {
        $campaignData = [];
        $this->db->select('cp.campaign_id,cp.`name`,cp.fk_branch_id,start_date,end_date,description,template_path,GROUP_CONCAT(cg.fk_group_id separator ",") as fk_group_id,cp.status');
        $this->db->from('campaign cp');
        $this->db->join('campaign_xref_group cg','cg.fk_campaign_id=cp.campaign_id','left');
        $this->db->where('cp.campaign_id',$campaignId);
        $this->db->group_by('cp.campaign_id');
        $campaign_data = $this->db->get();
        if($campaign_data->num_rows() == 0)
        {
            $status=false;
            $message="Incorrect campaign details";
        }
        else
        {
            $status=true;
            $message="Campaign details";
            $campaignData = $campaign_data->result();
            $delete_status=['created','approved','rejected'];
            foreach($campaignData as $k=>$v){
                $v->start_date_formatted=date('d M, Y',strtotime($v->start_date));
                $v->is_editable=0;
                $v->is_deletable=0;
                if(in_array($v->status,$delete_status) && strtotime($v->start_date)>strtotime(date('Y-m-d'))){
                    $v->is_editable=1;
                    $v->is_deletable=1;
                }
            }
        }
        $response=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>$campaignData
        );
        return $response;
    }
    public function campaignInfoDataTables($campaignId='')
    {
        $table = 'campaign_contact';
        // Table's primary key
        $primaryKey = 'cp`.`campaign_contact_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`l`.`name`',                           'dt' => 'name',         'field' => 'name' ),
            array( 'db' => '`cp`.`email`',                           'dt' => 'email',           'field' => 'email' ),
            array( 'db' => '`l`.`phone`',                           'dt' => 'phone',         'field' => 'phone' ),
            array( 'db' => '`cp`.`status`',                         'dt' => 'status',           'field' => 'status' ),
            array( 'db' => '`cp`.`campaign_contact_id`',                    'dt' => 'campaign_contact_id',      'field' => 'campaign_contact_id' ),
        );

        $globalFilterColumns = array(
            array( 'db' => '`l`.`name`',                           'dt' => 'name',         'field' => 'name' ),
            array( 'db' => '`cp`.`email`',                           'dt' => 'email',           'field' => 'email' ),
            array( 'db' => '`l`.`phone`',                           'dt' => 'phone',         'field' => 'phone' ),
            array( 'db' => '`cp`.`status`',                         'dt' => 'status',           'field' => 'status' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM campaign_contact cp LEFT JOIN lead l ON cp.fk_lead_id=l.lead_id";
        $extraWhere = " cp.fk_campaign_id='$campaignId'";
        $groupBy = "";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));
        return $responseData;
    }
    function getCampaignInfoPiechartById($campaignId)
    {
        $campaignData = [];
        $status=true;
        $message="Campaign details";

        $query="SELECT c.status, COUNT(campaign_contact_id) AS total,ROUND(COUNT(campaign_contact_id) / (SELECT COUNT(1) AS cnt FROM campaign_contact where fk_campaign_id=$campaignId)  * 100) AS `percentage`
  FROM campaign_contact c where c.fk_campaign_id=$campaignId GROUP BY c.status ORDER BY c.status ASC";
        $res=$this->db->query($query);
        if($res){
            if($res->num_rows()>0){
                $status=true;
                $campaignData=$res->result();
                foreach($campaignData as $k=>$v){
                    $v->status=ucfirst($v->status);
                }
            }
            else{
                $status=false;
                $campaignData=[];
            }
        }
        else{
            $status=false;
            $campaignData=[];
        }
        /*$campaignData['Opened']=30;
        $campaignData['Unopened']=40;
        $campaignData['Bounced']=30;*/

        $response=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>$campaignData
        );
        return $response;
    }
    function deleteCampaign($params,$details){
        $delete_status=['created','approved','rejected'];
        $campaignData = [];
        $status=true;
        $message="Delete campaign";
        if(in_array($details->status,$delete_status)){

            $data_update['status']='deleted';
            $data_update['fk_updated_by']=$_SERVER['HTTP_USER'];
            $data_update['updated_date']=date('Y-m-d H:i:s');
            $this->db->where('campaign_id',$params['campaignId']);
            $res=$this->db->update('campaign',$data_update);
            if($res){
                $status=true;$message='Campaign deleted successfully';$campaignData=[];
            }
            else{
                $status=false;$message='Something went wrong';$campaignData=[];
            }
        }
        else {
            $status=false;$message='Campaign cannot be deleted';$campaignData=[];
        }
        $response=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>$campaignData
        );
        return $response;
    }
}