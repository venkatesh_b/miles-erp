<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @controller Name City wise report
 * @category        Model
 * @author          Abhilash
 */
class CityReport_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    /**
     * Function to get Reports by City
     *
     * @return - array
     * @created date - 20 April 2016
     * @author : Abhilash
     */
    public function getCityReport($branch, $course, $branchText, $courseText){
        $query = $this->db->query("call Sp_Get_Citywise_Leads_Report({$branch},{$course})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $this->db->close();
                $this->load->database();
                $branchData = [];
                $branchName = [];
                $course = [];
                $filterCourse = explode(',',$courseText);
                foreach($res as $key=>$val){
                        $branchData[$val->branchName][] = array(
                            'branchName' => $val->branchName,
                            'courseName' => $val->courseName,
                            'lead_stage' => $val->lead_stage,
                            'leadCount' => $val->leadCount,
                            'conversion_rate' => $val->conversion_rate,
                            'conversion_rate' => $val->conversion_rate,
                            'prob' => $val->prob,
                        );
                    $branchName[$val->branchName] = $val->branchName;
                    $course[$val->courseName] = $val->courseName;
                }
                return array('data' => $branchData,'branch' => array_values($branchName) , 'course' => array_values($course));
            }
            else{
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        return $responseData;
    }
    
    public function getData() {
        $subheader = array('data' => array(
            'City' => 'Hyd',
            'Lead' => 'M1',
            '' => ' -- ',
            '' => '',
            '' => '',
            '' => '',
            '' => '',
            '' => '',
            '' => '',
            '' => '',
            '' => '',
            '' => '',
            '' => ''
        ),
            
        );
    }

}
