<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @Model Name      Lead Trasfer
 * @category        Model
 * @author          Abhilash
 * @Description     For Batch CRUD Services
 */
class CounsellorVisitors_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    
    public function getCounsellorVisitorsList($params){
        $filter_condition='';
        if($params['appliedFiltersString']!=''){
            $Filters=explode(',',$params['appliedFiltersString']);
        }
        else{
            $Filters=array();
        }
        if(count($Filters)==0){
            $filter_condition=" `bv`.`is_counsellor_visited`=0 OR  ";
        }else{
            if (in_array('history', $Filters)) {
                $filter_condition.=" `bv`.`is_counsellor_visited`=1 OR ";
            }
            if (in_array('pending', $Filters)) {
                $filter_condition .= " `bv`.`is_counsellor_visited`=0 OR ";
            }
        }
        $filter_condition=rtrim($filter_condition,' OR ');
        $filter_condition='('.$filter_condition.')';
        $filter_condition.=' AND ';
        $branch_id=$params['branch'];
        $user_id=$params['userId'];
        $table = 'branch_xref_lead';
        // Table's primary key
        $primaryKey = 'bxl`.`branch_xref_lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes

        $columns = array(
            array('db' => '`bxl`.`lead_number`', 'dt' => 'lead_number', 'field' => 'lead_number'),
            array('db' => '`c`.`name` as `course_name`', 'dt' => 'course_name', 'field' => 'course_name'),
            array('db' => '`ls`.`lead_stage`', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
            array('db' => '`l`.`name`', 'dt' => 'name', 'field' => 'name'),
            array('db' => '`l`.`phone`', 'dt' => 'phone', 'field' => 'phone'),
            array('db' => '`l`.`email`', 'dt' => 'email', 'field' => 'email'),

            array('db' => '`b`.`name` as `branch_name`', 'dt' => 'branch_name', 'field' => 'branch_name'),
            array('db' => '`bv`.`branch_visited_date`', 'dt' => 'branch_visited_date', 'field' => 'branch_visited_date'),
            array('db' => '`bxl`.`lead_id`', 'dt' => 'lead_id', 'field' => 'lead_id'),
            array('db' => '`bxl`.`fk_course_id` as `course_id`', 'dt' => 'course_id', 'field' => 'course_id'),
            array('db' => '`bv`.`branch_visit_comments`', 'dt' => 'branch_visit_comments', 'field' => 'branch_visit_comments'),
            array('db' => '`bv`.`second_level_counsellor_comments`', 'dt' => 'second_level_counsellor_comments', 'field' => 'second_level_counsellor_comments'),
            array('db' => '`bv`.`is_counsellor_visited`', 'dt' => 'is_counsellor_visited', 'field' => 'is_counsellor_visited'),
            array('db' => '`bv`.`second_level_counsellor_updated_date`', 'dt' => 'second_level_counsellor_updated_date', 'field' => 'second_level_counsellor_updated_date'),
            array('db' => '`rtv1`.`value` as `cName`', 'dt' => 'cName', 'field' => 'cName'),
            array('db' => '`rtv3`.`value` AS `qualificationName`', 'dt' => 'qualificationName', 'field' => 'qualificationName'),
            array('db' => '`l`.`fk_company_id` AS `company_id`', 'dt' => 'company_id', 'field' => 'company_id'),
            array('db' => '`l`.`fk_qualification_id` AS `qualification_id`', 'dt' => 'qualification_id', 'field' => 'qualification_id'),
            array('db' => '`l`.`fk_institution_id` AS `institution_id`', 'dt' => 'institution_id', 'field' => 'institution_id'),
            array('db' => '`bxl`.`status` AS `lead_status_id`', 'dt' => 'lead_status_id', 'field' => 'lead_status_id'),
            array('db' => '`e1`.`name` as `branch_visit_name`', 'dt' => 'branch_visit_name', 'field' => 'branch_visit_name'),
            array('db' => '`e2`.`name` as `counsellor_name`', 'dt' => 'counsellor_name', 'field' => 'counsellor_name'),
            array('db' => '`bv`.`second_level_counsellor_comments` as `counsellor_comments`', 'dt' => 'counsellor_comments', 'field' => 'counsellor_comments'),
            array('db' => '`bv`.`second_level_counsellor_updated_date` as `counsellor_comments_date`', 'dt' => 'counsellor_comments_date', 'field' => 'counsellor_comments_date'),
            array('db' => '`bxl`.`branch_xref_lead_id` as `branch_xref_lead_id`', 'dt' => 'branch_xref_lead_id', 'field' => 'branch_xref_lead_id'),
            array('db' => '`bv`.`branch_visitor_id` as `branch_visitor_id`', 'dt' => 'branch_visitor_id', 'field' => 'branch_visitor_id'),
            array( 'db' => 'if(l.gender=1,1,0) as gender',          'dt' => 'gender',  'field' => 'gender' )

        );

        $globalFilterColumns = array(
            array('db' => '`bxl`.`lead_number`', 'dt' => 'lead_number', 'field' => 'lead_number'),
            array('db' => '`l`.`name`', 'dt' => 'name', 'field' => 'name'),
            array('db' => '`l`.`phone`', 'dt' => 'phone', 'field' => 'phone'),
            array('db' => '`l`.`email`', 'dt' => 'email', 'field' => 'email'),
            array('db' => '`c`.`name`', 'dt' => 'course_name', 'field' => 'course_name'),
            array('db' => '`b`.`name`', 'dt' => 'branch_name', 'field' => 'branch_name'),
            array('db' => '`bv`.`branch_visited_date`', 'dt' => 'branch_visited_date', 'field' => 'branch_visited_date'),
            array('db' => '`rtv1`.`value`', 'dt' => 'cName', 'field' => 'cName'),
            array('db' => '`rtv3`.`value`', 'dt' => '`rtv3`.`value`', 'field' => 'qualificationName'),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db' => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /*         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM branch_visitor bv "
                . "left join branch_xref_lead bxl ON bxl.branch_xref_lead_id=bv.fk_branch_xref_lead_id "
                . "JOIN lead as l ON l.lead_id=bxl.lead_id "
                . "JOIN course as c ON c.course_id=bxl.fk_course_id "
                . "JOIN branch as b ON b.branch_id=bxl.branch_id "
                . "LEFT JOIN employee e1 ON e1.user_id=bv.created_by "
                . "LEFT JOIN employee e2 ON e2.user_id=bv.second_level_counsellor_id "
                . "LEFT JOIN user u1 ON u1.user_id=e1.user_id "
                . "LEFT JOIN user u2 ON u2.user_id=e2.user_id "
                . "LEFT JOIN reference_type_value rtv1 ON (rtv1.reference_type_value_id=l.fk_company_id OR rtv1.reference_type_value_id=l.fk_institution_id) "
                . "LEFT JOIN reference_type_value rtv3 ON rtv3.reference_type_value_id=l.fk_qualification_id  "
                . "LEFT JOIN lead_stage ls ON bxl.`status`=ls.lead_stage_id";
        $extraWhere = " $filter_condition  bxl.branch_id=$branch_id AND bv.second_level_counsellor_id=$user_id AND u1.is_active=1 AND u2.is_active=1";
        $groupBy = "";
        $responseData = (SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere, $groupBy));
        for ($i = 0; $i < count($responseData['data']); $i++) {
            if(isset($responseData['data'][$i]['branch_visited_date']) && strlen($responseData['data'][$i]['branch_visited_date'])>0 )
                $responseData['data'][$i]['branch_visited_date']=(new DateTime($responseData['data'][$i]['branch_visited_date']))->format('d M,Y');
                        else
                $responseData['data'][$i]['branch_visited_date'] = ' --- ';
            
            $responseData['data'][$i]['counsellor_visited_date']=(new DateTime($responseData['data'][$i]['second_level_counsellor_updated_date']))->format('d M,Y');
            $responseData['data'][$i]['counsellor_comments_date']=(new DateTime($responseData['data'][$i]['counsellor_comments_date']))->format('d M,Y');
            $responseData['data'][$i]['second_level_counsellor_updated_date']=(new DateTime($responseData['data'][$i]['second_level_counsellor_updated_date']))->format('d M,Y');
            $responseData['data'][$i]['lead_id'] = encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id_encode'] = encode($responseData['data'][$i]['branch_xref_lead_id']);
            $responseData['data'][$i]['comments']=array();
            $getComments="select bv.branch_visited_date,bv.second_level_counsellor_updated_date as counsellor_visited_date,bv.branch_visit_comments,bv.second_level_counsellor_comments,e1.name as counseollor_name,e.name as second_counsellor_name from branch_visitor bv,employee e1,employee e where bv.fk_branch_xref_lead_id =".$responseData['data'][$i]['branch_xref_lead_id']." AND is_counsellor_visited=1 AND status=1 AND bv.created_by=e1.user_id AND bv.second_level_counsellor_id=e.user_id order by bv.branch_visitor_id DESC";
            $res_comment=$this->db->query($getComments);
            if($res_comment){
                if($res_comment->num_rows()>0){
                    $row_comment=$res_comment->result();
                    foreach($row_comment as $k_comment=>$v_comment){
                        $responseData['data'][$i]['comments'][]=array('counsellor_visited_date'=>(new DateTime($v_comment->counsellor_visited_date))->format('d M,Y'),'branch_visited_date'=>(new DateTime($v_comment->branch_visited_date))->format('d M,Y'),'second_level_comments'=>$v_comment->second_level_counsellor_comments,'branch_visit_comments'=>$v_comment->branch_visit_comments,'counseollor_name'=>$v_comment->counseollor_name,'second_counseollor_name'=>$v_comment->second_counsellor_name);
                    }
                }
            }
        }
        return $responseData;
    }
    

    
    public function getLeadSpanshot($params){
        $error = false;
        $status = false;
        $msg='No Data Found';
        $data=array();

        $brachXrefLeadId=$params['brachXrefLeadId'];
        $branchId=$params['branchId'];
        $hdnBranchVisitorId=$params['hdnBranchVisitorId'];
        $this->db->select('branch_xref_lead_id,lead_id');
        $this->db->from('branch_xref_lead');
        $this->db->where('branch_xref_lead_id', $brachXrefLeadId);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {

                $this->db->select('l.name, l.phone, l.email, bxl.branch_xref_lead_id, bxl.lead_id, bxl.branch_id, bxl.fk_course_id, bxl.lead_number, bxl.expected_visit_date, bxl.expected_enroll_date, bxl.transferred_to, bxl.transferred_from, b.name branch_name, c.name course_name,bv.second_level_counsellor_comments,bv.second_level_counsellor_updated_date,ls.lead_stage');
                $this->db->from('branch_xref_lead bxl');
                $this->db->join('lead l', 'l.lead_id=bxl.lead_id');
                $this->db->join('course c', 'c.course_id=bxl.fk_course_id');
                $this->db->join('branch b', 'b.branch_id=bxl.branch_id');
                $this->db->join('branch_visitor bv', 'bv.fk_branch_xref_lead_id=bxl.branch_xref_lead_id');
                $this->db->join('lead_stage ls', 'bxl.status=ls.lead_stage_id');
                $this->db->where('bv.second_level_counsellor_id', $params['userId']);
                $this->db->where('bxl.branch_xref_lead_id', $brachXrefLeadId);
                $this->db->where('bxl.branch_id', $branchId);
                $this->db->where('bv.branch_visitor_id', $hdnBranchVisitorId);

                //$this->db->where('bxl.is_active', 1);
                $res_details = $this->db->get();
                if($res_details){
                    if($res_details->num_rows()>0){
                        $row_details=$res_details->result();
                        foreach ($row_details as $key => $value) {
//                            print_r($value);die;
                            $value->second_level_counsellor_updated_date = (new DateTime($value->second_level_counsellor_updated_date))->format('d M,Y');
                            $value->expected_visit_date = (new DateTime($value->expected_visit_date))->format('d M,Y');
                        }
                        $status = true;
                        $msg='success';
                        $data=$row_details;
                    }
                    else{
                        $status = false;
                        $msg='No Data Found';
                        $data=array();
                    }
                }
                else{
                    $status = false;
                    $msg=$this->db->error()['message'];
                    $data=array();
                }



            } else {
                $status = false;
                $msg='No Data Found';
                $data=array();
            }
        } else {

            $status = false;
            $msg=$this->db->error()['message'];
            $data=array();
        }
        return $db_response=array("status"=>$status,"message"=>$msg,"data"=>$data);

        



    }
    

    function saveCounsellorComments($params){
        $releaseDue=$this->releaseLeadIfExist($params['branchXrefLeadId']);
        $comments='';
        $enrollDate='';
        $leadId=NULL;
        $this->db->select('lead_id');
        $this->db->from('branch_xref_lead');
        $this->db->where('branch_xref_lead_id', $params['branchXrefLeadId']);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();

            if (count($res_all) > 0) {
                if (isset($res_all[0]->lead_id)) {
                    $leadId = $res_all[0]->lead_id;

                        $data_insert_timeline=array('lead_id'=>$leadId,'branch_xref_lead_id'=>$params['branchXrefLeadId'],"type"=>"Counsellor Approval","description"=>$params['comments'],"created_by"=>$_SERVER['HTTP_USER'],"created_on"=>date('Y-m-d H:i:s'));
                        $res_insert_timeline=$this->db->insert('timeline',$data_insert_timeline);

                        $data_update_visitor['second_level_counsellor_comments']=$params['comments'];
                        $data_update_visitor['second_level_counsellor_updated_date']=date('Y-m-d H:i:s');
                        $data_update_visitor['is_counsellor_visited']=1;
                        $this->db->where('branch_visitor_id',$params['branchVisitorId']);
                        $res2=$this->db->update('branch_visitor',$data_update_visitor);
                        if($res2){
                            $db_response=array('status'=>true,'message'=>'Success','data'=>array());
                        }
                        else{
                            $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>array());
                        }


                }
                else{
                    $db_response=array('status'=>false,'message'=>'Invalid lead details','data'=>array());
                }
            }
            else{
                $db_response=array('status'=>false,'message'=>'Invalid lead details','data'=>array());
            }

        }
        else{
            $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>array());
        }

        return $db_response;

    }
    function releaseLeadIfExist($branchXrefLeadId){

        $this->db->select('fk_lead_call_schedule_head_id,fk_lead_id');
        $this->db->from('lead_user_xref_lead_due');
        $this->db->where('fk_branch_xref_lead_id', $branchXrefLeadId);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if (count($res_all) > 0) {
                //remove from lead_user_xref_lead_history, lead_user_xref_lead_release
                $data_insert =array(
                    'release_count' =>1,
                    'released_by' =>$_SERVER['HTTP_USER'],
                    'released_on' =>date('Y-m-d H:i:s'),
                    'fk_lead_call_schedule_head_id' =>$res_all[0]->fk_lead_call_schedule_head_id
                );
                $this->db->insert('lead_user_xref_lead_release_head', $data_insert);
                $releaseHead=$this->db->insert_id();
                $data_insert1 =array(
                    'fk_user_id' =>$_SERVER['HTTP_USER'],
                    'fk_branch_xref_lead_id' => $branchXrefLeadId,
                    'fk_lead_id' => $res_all[0]->fk_lead_id,
                    'fk_lead_user_xref_lead_release_head_id' => $releaseHead,
                    'fk_lead_call_schedule_head_id' =>$res_all[0]->fk_lead_call_schedule_head_id
                );
                $this->db->insert('lead_user_xref_lead_release', $data_insert1);

                $this->db->where('fk_branch_xref_lead_id',$branchXrefLeadId);
                $this->db->delete('lead_user_xref_lead_due');

            }

        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

    }
    
}
