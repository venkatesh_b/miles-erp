<?php

/* 
 * Created By: Abhilash 
 * Purpose: Services for Courses.
 * 
 * Updated on - 6 Feb 2016
 * 
 * updated by: Parameshwar
 * updated on: 17 Feb 2016:
 * Purpose: Services for creation of courses, edit and delete of a course.
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Course_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    
    public function checkCourseExist($params){
        $this->db->select("course_id, name, description,is_active");
        $this->db->from("course");
        $this->db->where("course_id", $params['courseId']);
        $this->db->where_in("is_active", array(0,1));
        
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid Course',"data"=>array());
            }
        }
        else
        {
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getAllCourses() {
        $this->db->select("c.course_id courseId, c.name, c.description,c.is_active,c.fk_created_by,e.name as createdBy");
        $this->db->from("course c");
        $this->db->join("user u","u.user_id=c.fk_created_by");
        $this->db->join("employee e","e.user_id=u.user_id");
        $this->db->where("c.is_active", 1);
        
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            foreach($list as $k=>$v){
//                $courseId='';
                $courseId=$v->courseId;
//                $v->courseId=encode($v->courseId);    /*Removed because of branch multi-select*/
//                $v->courseId=$v->courseId;
                $this->db->select("bxc.fk_branch_id,b.name");
                $this->db->from("branch_xref_course bxc");
                $this->db->join("branch b","b.branch_id=bxc.fk_branch_id");
                $this->db->where("bxc.fk_course_id", $courseId);
                $this->db->where("bxc.is_active", 1);
                $branches = $this->db->get();
                $v->branchesCount=$branches->num_rows();
                $v->branches=$branches->result();
            }
            if(count($list) > 0)
            {
                return $list;
            }
            else
            {
                return 'nodata';
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function getCourseDetailsByID($params) {
        $this->db->select("c.course_id courseId, c.name, c.description,c.is_active,c.fk_created_by,e.name as createdBy");
        $this->db->from("course c");
        $this->db->join("user u","u.user_id=c.fk_created_by");
        $this->db->join("employee e","e.user_id=u.user_id");
        $this->db->where("course_id", $params['courseId']);
        
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            foreach($list as $k=>$v){
                $this->db->select("bxc.fk_branch_id,b.name");
                $this->db->from("branch_xref_course bxc");
                $this->db->join("branch b","b.branch_id=bxc.fk_branch_id");
                $this->db->where("bxc.fk_course_id", $v->courseId);
                $this->db->where("bxc.is_active", 1);
                $branches = $this->db->get();
                $v->branchesCount=$branches->num_rows();
                $v->branches=$branches->result();
            }
            if(count($list) > 0)
            {
                $db_response=array("status"=>true,"message"=>"success","data"=>$list);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
                return $db_response;
    }
    function addCourse($params){
            
        
            $this->db->select('course_id');
            $this->db->from('course');
            $this->db->where('name',$params['courseName']);
            $query = $this->db->get();
            if(!$query){
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
            else{
            if ($query->num_rows() > 0)
            {
                $db_response=array("status"=>false,"message"=>'Course already exist',"data"=>array('courseName'=>'Course already exist'));
            }
            else{
                    $data['courseName']=$courseName=$params['courseName'];
                    $data['courseBranches']=$courseBranches=$params['courseBranches'];
                    $data['courseStatus']=$courseStatus=$params['courseStatus'];
                    $data['courseDescription']=$courseDescription=$params['courseDescription'];
                    $data['userID']=$userID=$params['userID'];
                    $data_insert=array();
                    $data_insert=array('name' => $courseName,'fk_created_by' => $userID,'is_active' => (int)$courseStatus, 'created_date'=>date('Y-m-d H:i:s'),'description'=>$courseDescription);
                    $res_insert=$this->db->insert('course', $data_insert);
                    if($res_insert){
                        $course_id=$this->db->insert_id();
                        $courseBranches=explode(',',$courseBranches);
                        $savecourse=array();
                        $savecourse['courseId']=$course_id;
                        $savecourse['courseBranches']=$params['courseBranches'];
                        $savecourse['courseStatus']=$params['courseStatus'];
                        $db_response=$this->saveCourseBranches($savecourse);
                        if($params['courseStatus']==0){
                            //$this->deleteCourse($params);
                        }
                        if($db_response['status']==true)
                            $db_response['message']='Course added successfully';
                    }
                    else{
                        $error = $this->db->error(); 
                        $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                    }
            }
            }
            
                return $db_response;
        
    }
    function updateCourse($params){
        
            $this->db->select('course_id');
            $this->db->from('course');
            $this->db->where('name',$params['courseName']);
            $this->db->where('course_id!=',$params['courseId']);
            $query = $this->db->get();
            if(!$query){
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
            }
            else{
            if ($query->num_rows() > 0)
            {
                $db_response=array("status"=>false,"message"=>'Course already exist',"data"=>array("message"=>'Course already exist'));
            }
            else{
                $data['courseId']=$courseId=$params['courseId'];
                $data['courseName']=$courseName=$params['courseName'];
                $data['courseBranches']=$courseBranches=$params['courseBranches'];
                $data['courseStatus']=$courseStatus=$params['courseStatus'];
                $data['courseDescription']=$courseDescription=$params['courseDescription'];
                $data['userID']=$userID=$params['userID'];
                $data_update=array();
                $data_update=array('name' => $courseName,'fk_created_by' => $userID,'is_active' => (int)$courseStatus, 'created_date'=>date('Y-m-d H:i:s'),'description'=>$courseDescription);
                $this->db->where('course_id', $courseId);
                $res_update=$this->db->update('course', $data_update);
                if($res_update){
                    $savecourse=array();
                    $savecourse['courseId']=$courseId;
                    $savecourse['courseBranches']=$params['courseBranches'];
                    $savecourse['courseStatus']=$params['courseStatus'];
                    $db_response=$this->saveCourseBranches($savecourse);
                    if($params['courseStatus']==0){
                        $this->deleteCourse($params);
                    }
                    if($db_response['status']==true)
                        $db_response['message']='Course updated successfully';
                }
                else{
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
            }
            
                return $db_response;
    }
    function saveCourseBranches($params){
        $alreadyadded=array();
        $tobeadded=array();
        $branch_ids=explode(',',$params['courseBranches']);
        $tobeadded=$branch_ids;

        $courseId=$params['courseId'];
        $courseStaus=(int)$params['courseStatus'];
        $this->db->select("bc.fk_branch_id");
        $this->db->from('branch_xref_course bc');
        $this->db->where("bc.fk_course_id",$courseId);
        $this->db->where("bc.is_active",1);
        $query = $this->db->get();
        
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $k=>$v){
                    $alreadyadded[]=$v->fk_branch_id;
                }
            }
            else{
                $alreadyadded=array();
            }
        }
        else{
            $alreadyadded=array();
        }
            $tobeadded=array_values(array_unique(array_filter($tobeadded)));
            $alreadyadded=array_values(array_unique(array_filter($alreadyadded)));
            $diffinalready=array_values(array_diff($alreadyadded,$tobeadded));
            $diffintobeadded=array_values(array_diff($tobeadded,$alreadyadded));
            $data_insert_courseBranches=array();
            if(count($diffintobeadded)>0){
                for($ij=0;$ij<count($diffintobeadded);$ij++){

                    if(trim($diffintobeadded[$ij])!=''){
                        $checkExist=0;

                        $this->db->select("bc.fk_branch_id");
                        $this->db->from('branch_xref_course bc');
                        $this->db->where("bc.fk_course_id",$courseId);
                        $this->db->where("bc.fk_branch_id",$diffintobeadded[$ij]);
                        $queryCheck = $this->db->get();
                        if ($queryCheck->num_rows()>0){
                            $checkExist=1;
                        }
                        if($checkExist==0){
                            //insert;
                            $data_insert_courseBranches=array(
                                'fk_branch_id' => $diffintobeadded[$ij],
                                'fk_course_id' => $courseId,
                                'is_active' => $courseStaus);
                            $res_insert_accessids=$this->db->insert('branch_xref_course', $data_insert_courseBranches);

                        }
                        else{
                            //update
                            $data_update = array(
                                'is_active' => $courseStaus
                            );
                            $this->db->where('fk_course_id', $courseId);
                            $this->db->where_in('fk_branch_id', $diffintobeadded[$ij]);
                            $res_update=$this->db->update('branch_xref_course', $data_update);
                        }


                    }

                }
                $db_response=array("status"=>true,"message"=>"success","data"=>array());
            }
            else{
                 $db_response=array("status"=>true,"message"=>"success","data"=>array());
            }
            if(count($diffinalready)>0){
                $data_update = array(
                 'is_active' => 0
                );
                $this->db->where('fk_course_id', $courseId);
                $this->db->where_in('fk_branch_id', $diffinalready);
                $res_update=$this->db->update('branch_xref_course', $data_update);
                if($res_update){
                    $db_response=array("status"=>true,"message"=>'success',"data"=>array());
                }
                else{
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
            else{
                 $db_response=array("status"=>true,"message"=>"success","data"=>array());
            }
            return $db_response;
    }
    function deleteCourse($params){

            $data['courseId']=$courseId=$params['courseId'];
            $data_update=array();
            $status = !$params['status'];
            $data_update=array('is_active' => $status);
            $this->db->where('course_id', $courseId);
            $res_update=$this->db->update('course', $data_update);
            if($res_update){
                    $data_update=array();
                    $data_update=array('is_active' => $status);
                    $this->db->where('fk_course_id', $courseId);
                    $res_update=$this->db->update('branch_xref_course', $data_update);
                    if($res_update){
                       $db_response=array("status"=>true,"message"=>"Deleted successfully","data"=>array('message'=>'Deleted successfully'));
                    }
                    else{
                        $error = $this->db->error(); 
                        $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                    }
                }
                else{
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
                return $db_response;
    
    }
    public function coursesDataTables()
    {
        $table = 'course';
        // Table's primary key
        $primaryKey = 'c`.`course_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`c`.`name` as course_name',                         'dt' => 'course_name',                    'field' => 'course_name' ),
            array( 'db' => 'count(b.branch_id) as `branchesCount`', 'dt' => 'branchesCount',               'field' => 'branchesCount' ),
            array( 'db' => '`c`.`description`',                   'dt' => 'description',              'field' => 'description' ),
            array( 'db' => '`c`.`course_id`',       'dt' => 'course_id',  'field' => 'course_id' ),
            array( 'db' => '`e`.`name` as created_by',                 'dt' => 'created_by',            'field' => 'created_by' ),
            array( 'db' => 'if(`c`.`is_active`=1,1,0) as `is_active`',                  'dt' => 'is_active',             'field' => 'is_active'),
            array( 'db' => '`c`.`fk_created_by`',                 'dt' => 'fk_created_by',            'field' => 'fk_created_by' ),
            array( 'db' => 'GROUP_CONCAT(" ",b.name ORDER BY b.name ASC) as `Branches`',   'dt' => 'Branches',                    'field' => 'Branches' ),
        );

        $globalFilterColumns = array(
            array( 'db' => '`c`.`name`'         ,'dt' => 'name'         ,'field' => 'name' ),
            array( 'db' => '`c`.`description`'  ,'dt' => 'description'  ,'field' => 'description' ),
            array( 'db' => '`e`.`name`'         ,'dt' => 'name'         ,'field' => 'name' ),
            array( 'db' => '`c`.`is_active`'         ,'dt' => 'is_active'         ,'field' => 'is_active' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM `course` AS `c` JOIN `user` AS `u` ON (`u`.`user_id`=`c`.`fk_created_by`) JOIN `employee` AS `e` ON (`e`.`user_id` = `u`.`user_id`) LEFT JOIN `branch_xref_course` AS `bxc` ON (`bxc`.`fk_course_id` = `c`.`course_id` AND `bxc`.`is_active`=1) LEFT JOIN `branch` b ON (b.branch_id=bxc.fk_branch_id) ";
        $extraWhere = "";
        $groupBy = "c.course_id";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['course_id']=encode($responseData['data'][$i]['course_id']);
        }
        return $responseData;
    }
    function getBranchCoursesList($params){
        $branchId=$params['branchId'];
        $this->db->select("c.course_id courseId, c.name, c.description,c.is_active,c.fk_created_by");
        $this->db->from("course c");
        $this->db->join("branch_xref_course bxc" ,"bxc.fk_course_id=c.course_id");
        $this->db->where("bxc.is_active", 1);
        $this->db->where("bxc.fk_branch_id", $branchId);
        $this->db->where("c.is_active", 1);

        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            $db_response=array("status"=>true,"message"=>"success","data"=>$list);
        }
        else
        {
            $db_response=array("status"=>false,"message"=>"fail","data"=>array());
        }
            return $db_response;
    }
    
    function getAllBranchCoursesList($params){
        $branchId=$params['branchId'];
        $this->db->select("c.course_id courseId, c.name, c.description,c.is_active,c.fk_created_by");
        $this->db->from("course c");
        $this->db->join("branch_xref_course bxc" ,"bxc.fk_course_id=c.course_id");
        $this->db->where("bxc.fk_branch_id", $branchId);

        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            $db_response=array("status"=>true,"message"=>"success","data"=>$list);
        }
        else
        {
            $db_response=array("status"=>false,"message"=>"fail","data"=>array());
        }
            return $db_response;
    }
    function getMultiBranchCommonCoursesList($params){
        $branchId=$params['branchId'];
        if($branchId==''){
            $branchId="''";
        }
        $query="select c.course_id courseId, c.name, c.description,c.is_active,c.fk_created_by from branch_xref_course bxc , branch b,course c where b.branch_id=bxc.fk_branch_id and c.course_id=bxc.fk_course_id and bxc.is_active=1 and bxc.fk_branch_id in (".$branchId.") group by bxc.fk_course_id having count(*)=".count(explode(',',$branchId));
        $list = $this->db->query($query);
        if($list)
        {
            $list = $list->result();
            $db_response=array("status"=>true,"message"=>"success","data"=>$list);
        }
        else
        {
            $db_response=array("status"=>false,"message"=>"fail","data"=>array());
        }
        return $db_response;
    }
}