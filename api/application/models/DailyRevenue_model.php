<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 25/4/16
 * Time: 5:27 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class DailyRevenue_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    public function getLatestDRSPostingData($branch_id,$company_id,$payment_mode,$chosen_date)
    {
        $flag=0;
        if($payment_mode=='cash')
            $is_cash=0;
        else
            $is_cash=1;
        $this->db->select("`d`.`drs_id`, d.opening_balance,d.closing_balance,d.amount_deposited,d.amount_collected,d.signoff_amount,DATE_FORMAT(d.posting_date, '%Y-%m-%d') as posting_date");
        $this->db->from("drs d");
        $this->db->where("`d`.`fk_branch_id`",$branch_id);
        $this->db->where("`d`.`fk_company_id`",$company_id);
        $this->db->where_in("`d`.`is_cash`", $is_cash);
        $this->db->order_by("d.created_date", "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                if($chosen_date < $query->row()->posting_date)
                {
                    $flag=1;
                }
                else
                {
                    $flag=0;
                }
            }
            else
            {
                $flag=0;
            }
        }
        return $flag;
    }
    public function getDailyRevenuePosting($branch_id,$company_id,$payment_mode,$chosen_date)
    {
        $validate_posting=$this->getLatestDRSPostingData($branch_id,$company_id,$payment_mode,$chosen_date);
        if($validate_posting == 1)
        {
            return array("status"=>false,"message"=>'Selected date cannot be less than previous posting date',"data"=>[]);
        }
        else
        {
            if($payment_mode=='cash')
                $is_cash=0;
            else
                $is_cash=1;
            $this->db->select("`d`.`drs_id`, d.opening_balance,d.closing_balance,d.amount_deposited,d.amount_collected,d.signoff_amount,DATE_FORMAT(d.posting_date, '%Y-%m-%d') as posting_date");
            $this->db->from("drs d");
            $this->db->where("`d`.`fk_branch_id`",$branch_id);
            $this->db->where("`d`.`fk_company_id`",$company_id);
            $this->db->where("DATE(`d`.`posting_date`)",$chosen_date);
            $this->db->where_in("`d`.`is_cash`", $is_cash);
            $drs_posting_query = $this->db->get();
            if($drs_posting_query)
            {
                if ($drs_posting_query->num_rows() > 0)
                {
                    return array("status"=>false,"message"=>'Posting done for selected date',"data"=>[]);
                }
                else
                {
                    if($payment_mode == 'cash')
                    {
                        return $this->getDailyRevenueCashPosting($branch_id,$company_id,$payment_mode,$chosen_date);
                    }
                    else
                    {
                        return $this->getDailyRevenueOtherPosting($branch_id,$company_id,$payment_mode,$chosen_date);
                    }
                }
            }

        }
    }

    public function getDailyRevenueOtherPosting($branch_id,$company_id,$payment_mode,$chosen_date)
    {
        if($payment_mode=='cash')
            $is_cash=0;
        else
            $is_cash=1;
        $prev_drs_data=$this->getPreviousDRSData($branch_id,$company_id,$is_cash);
        $this->db->select("fc.fee_collection_id,fc.receipt_number,DATE_FORMAT(fc.receipt_date, '%d %b,%Y') as receipt_date,fc.payment_mode,fc.credit_card_number,fc.cheque_number,fc.fk_fee_company_id,fc.amount_paid");
        $this->db->from("fee_collection fc");
        $this->db->join("branch_xref_lead bxl","fc.fk_branch_xref_lead_id=bxl.branch_xref_lead_id","left");
        $this->db->where("fc.fee_collection_id > ",$prev_drs_data['last_fee_collection_id']);
        $this->db->where("bxl.branch_id ",$branch_id);
        $this->db->where("fc.fk_fee_company_id ",$company_id);
        $this->db->where("fc.payment_status ","Success");
        $this->db->where_in('fc.payment_mode', array('Cheque','DD','Credit Card'));
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $res['revenuePosting']=$query->result();
                $total_amount_paid=$this->getDRSTotalAmountPaid($prev_drs_data['last_fee_collection_id'],$company_id,$branch_id,$chosen_date,$is_cash);
                $amount_collected=$this->getDRSTotalAmountCollected($prev_drs_data['last_fee_collection_id'],$company_id,$branch_id,$chosen_date,$is_cash);
                $res['opening_balance']=$prev_drs_data['openingBalance']+$total_amount_paid;
                $res['amount_collected']=$amount_collected;
                $this->db->select("company_bank_detail_id,fk_company_id,bank_name,bank_city,bank_branch,acc_no");
                $this->db->from("company_bank_detail cbd");
                $this->db->where("cbd.fk_company_id",$company_id);
                $query = $this->db->get();
                if ($query->num_rows() > 0)
                {
                    $res['fee_company_list']=$query->result();
                }
                else
                {
                    $res['fee_company_list']=null;
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'No records found',"data"=>[]);
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getDailyRevenueCashPosting($branch_id,$company_id,$payment_mode,$chosen_date)
    {
        $this->db->select("company_bank_detail_id,fk_company_id,bank_name,bank_city,bank_branch,acc_no");
        $this->db->from("company_bank_detail cbd");
        $this->db->where("cbd.fk_company_id",$company_id);
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $res['revenuePosting']=$query->result();
                $prev_drs_data=$this->getPreviousDRSData($branch_id,$company_id,$payment_mode);
                $total_amount_paid=$this->getDRSTotalAmountPaid($prev_drs_data['last_fee_collection_id'],$company_id,$branch_id,$chosen_date,$payment_mode);
                $amount_collected=$this->getDRSTotalAmountCollected($prev_drs_data['last_fee_collection_id'],$company_id,$branch_id,$chosen_date,$payment_mode);
                $res['opening_balance']=$prev_drs_data['openingBalance']+$total_amount_paid;
                $res['amount_collected']=$amount_collected;
                //return $prev_drs_data['openingBalance'];
                if($prev_drs_data['openingBalance']==0 && $total_amount_paid==0 && $amount_collected ==0)
                {
                    $db_response=array("status"=>false,"message"=>'No records found',"data"=>[]);
                }
                else
                {
                    $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
                }
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'No records found',"data"=>[]);
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getPreviousDRSData($branch_id,$company_id,$payment_mode)
    {
        $this->db->select("closing_balance,last_fee_collection_id");
        $this->db->from("drs");
        $this->db->where("fk_branch_id",$branch_id);
        $this->db->where("fk_company_id",$company_id);
        $this->db->where("is_cash",$payment_mode);
        $this->db->order_by("created_date", "desc");
        $this->db->limit(1);
        $openbal_query = $this->db->get();
        if ($openbal_query->num_rows() > 0)
        {
            $drs_data=$openbal_query->result();
            $response['openingBalance']         = $drs_data[0]->closing_balance;
            $response['last_fee_collection_id'] = $drs_data[0]->last_fee_collection_id;
        }
        else
        {
            $response['openingBalance']         = 0;
            $response['last_fee_collection_id'] = 0;
        }
        return $response;
    }
    public function getDRSTotalAmountPaid($last_fee_collection_id,$company_id,$branch_id,$chosen_date,$payment_mode)
    {
        $this->db->select_sum('fc.amount_paid');
        $this->db->from('fee_collection fc');
        $this->db->join("branch_xref_lead bxl","fc.fk_branch_xref_lead_id=bxl.branch_xref_lead_id","left");
        if($payment_mode == 0)
            $this->db->where('fc.payment_mode', 'Cash');
        else
            $this->db->where_in('fc.payment_mode', array('Cheque','DD','Credit Card'));
        $this->db->where('fc.fee_collection_id >', $last_fee_collection_id);
        $this->db->where('fc.fk_fee_company_id', $company_id);
        $this->db->where('bxl.branch_id', $branch_id);
        $this->db->where('DATE(fc.receipt_date) <=', $chosen_date);
        $query = $this->db->get();
        if ($query->row()->amount_paid > 0)
            $total_amount_paid = $query->row()->amount_paid;
        else
            $total_amount_paid = 0;
        return $total_amount_paid;
    }
    public function getDRSTotalAmountCollected($last_fee_collection_id,$company_id,$branch_id,$chosen_date,$payment_mode)
    {
        $this->db->select_sum('fc.amount_paid');
        $this->db->from('fee_collection fc');
        $this->db->join("branch_xref_lead bxl","fc.fk_branch_xref_lead_id=bxl.branch_xref_lead_id","left");
        if($payment_mode == 0)
            $this->db->where('fc.payment_mode', 'Cash');
        else
            $this->db->where_in('fc.payment_mode', array('Cheque','DD','Credit Card'));
        $this->db->where('fc.fee_collection_id >', $last_fee_collection_id);
        $this->db->where('fc.fk_fee_company_id', $company_id);
        $this->db->where('bxl.branch_id', $branch_id);
        $this->db->where('DATE(fc.receipt_date) ', $chosen_date);
        $query = $this->db->get();
        if ($query->row()->amount_paid > 0)
            $amt_collected = $query->row()->amount_paid;
        else
            $amt_collected = 0;
        return $amt_collected;
    }
    public function getLastFeeCollectionId($branch_id,$company_id,$isCashPayment)
    {
        $this->db->select("drs_id,fk_company_id,closing_balance,last_fee_collection_id");
        $this->db->from("drs");
        $this->db->where("fk_branch_id",$branch_id);
        $this->db->where("fk_company_id",$company_id);
        $this->db->where("is_cash",$isCashPayment);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->row()->last_fee_collection_id;
        }
        else
        {
            $this->db->select('fc.fee_collection_id');
            $this->db->from('fee_collection fc');
            $this->db->join("branch_xref_lead bxl","fc.fk_branch_xref_lead_id=bxl.branch_xref_lead_id","left");
            if($isCashPayment == 0)
                $this->db->where('fc.payment_mode', 'Cash');
            else
                $this->db->where_in('fc.payment_mode', array('Cheque','DD','Credit Card'));
            $this->db->where("fc.payment_status ","Success");
            $this->db->where('fc.fk_fee_company_id', $company_id);
            $this->db->where('bxl.branch_id', $branch_id);
            $this->db->where('fc.fee_collection_id >', 0);
            $this->db->order_by("fc.receipt_date", "desc");
            $this->db->limit(1);
            $query = $this->db->get();
            if ($query->num_rows() > 0)
            {
                return $query->row()->fee_collection_id;
            }
            else
            {
                return 0;
            }
        }
    }
    public function updateDailyRevenueCashPosting($rawRevenueCashPosting,$drs_data)
    {
        $lastFeeCollectedId=$this->getLastFeeCollectionId($drs_data['branch_id'],$drs_data['company_id'],$drs_data['payment_type']);
        $currentDate= date('Y-m-d H:i:s');
        /*'signoff_amount'            =>  $drs_data['signoff_amount'],
            'signoff_comments'          =>  $drs_data['signoff_comments'],
            'signoff_given_by'          =>  $_SERVER['HTTP_USER'],*/
        if(($drs_data['signoff_amount']<=0 || $drs_data['signoff_amount'] == '') && $drs_data['signoff_comments']!='')
        {
            $drsData = array(
                'fk_company_id'             =>  $drs_data['company_id'],
                'posting_date'              =>  $drs_data['selected_date'],
                'fk_branch_id'              =>  $drs_data['branch_id'],
                'is_cash'                   =>  $drs_data['payment_type'],
                'opening_balance'           =>  $drs_data['opening_balance'],
                'amount_deposited'          =>  $drs_data['amount_deposited'],
                'amount_collected'          =>  $drs_data['amount_collected'],
                'closing_balance'           =>  $drs_data['closing_balance'],
                'last_fee_collection_id'    =>  $lastFeeCollectedId,
                'created_by'                =>  $_SERVER['HTTP_USER'],
                'created_date'              =>  $currentDate
            );
        }
        else
        {
            $drsData = array(
                'fk_company_id'             =>  $drs_data['company_id'],
                'posting_date'              =>  $drs_data['selected_date'],
                'fk_branch_id'              =>  $drs_data['branch_id'],
                'is_cash'                   =>  $drs_data['payment_type'],
                'opening_balance'           =>  $drs_data['opening_balance'],
                'amount_deposited'          =>  $drs_data['amount_deposited'],
                'amount_collected'          =>  $drs_data['amount_collected'],
                'closing_balance'           =>  $drs_data['closing_balance'],
                'signoff_amount'            =>  $drs_data['signoff_amount'],
                'signoff_comments'          =>  $drs_data['signoff_comments'],
                'signoff_given_by'          =>  $_SERVER['HTTP_USER'],
                'last_fee_collection_id'    =>  $lastFeeCollectedId,
                'created_by'                =>  $_SERVER['HTTP_USER'],
                'created_date'              =>  $currentDate
            );
        }
        $this->db->insert('drs', $drsData);
        $drs_id=$this->db->insert_id();

        for($i=0;$i<count($rawRevenueCashPosting);$i++)
        {
            $revenueCashPostingData=explode('@@@@@@',$rawRevenueCashPosting[$i]);
            $amount = $revenueCashPostingData[0];
            $voucher = $revenueCashPostingData[1];
            $bank_id = $revenueCashPostingData[2];
            $bank_receipt = $revenueCashPostingData[3];
            $postingData = array(
                'fk_drs_id'             =>  $drs_id,
                'fk_company_bank_id'    =>  $bank_id,
                'amount'                =>  $amount,
                'fk_fee_collection_id'  =>  null,
                'bank_voucher_number'   =>  $voucher,
                'bank_receipt'          =>  $bank_receipt,
                'created_by'            =>  $_SERVER['HTTP_USER'],
                'created_date'          =>  $currentDate
            );
            $this->db->insert('drs_bank_deposit', $postingData);
        }
        return array("status"=>true,"message"=>'Revenue Posted successfully',"data"=>[]);
    }
    function updateDailyRevenueOtherPosting($rawRevenueOtherPosting,$drs_data)
    {
        $lastFeeCollectedId=$this->getLastFeeCollectionId($drs_data['branch_id'],$drs_data['company_id'],$drs_data['payment_type']);
        $currentDate= date('Y-m-d H:i:s');
        $drsData = array(
            'fk_company_id'             =>  $drs_data['company_id'],
            'posting_date'              =>  $drs_data['selected_date'],
            'fk_branch_id'              =>  $drs_data['branch_id'],
            'is_cash'                   =>  $drs_data['payment_type'],
            'opening_balance'           =>  $drs_data['opening_balance'],
            'amount_deposited'          =>  $drs_data['amount_deposited'],
            'amount_collected'          =>  $drs_data['amount_collected'],
            'closing_balance'           =>  $drs_data['closing_balance'],
            'last_fee_collection_id'    =>  $lastFeeCollectedId,
            'created_by'                =>  $_SERVER['HTTP_USER'],
            'created_date'              =>  $currentDate
        );
        $this->db->insert('drs', $drsData);
        $drs_id=$this->db->insert_id();
        for($i=0;$i<count($rawRevenueOtherPosting);$i++)
        {
            $revenueOtherPostingData=explode('@@@@@@',$rawRevenueOtherPosting[$i]);
            $bank_id     = $revenueOtherPostingData[0];
            $voucher    = $revenueOtherPostingData[1];
            $amount    = $revenueOtherPostingData[2];
            $feeCollectionId    = $revenueOtherPostingData[3];
            $bank_receipt = $revenueOtherPostingData[4];
            $postingData = array(
                'fk_drs_id'             =>  $drs_id,
                'fk_company_bank_id'    =>  $bank_id,
                'amount'                =>  $amount,
                'fk_fee_collection_id'  =>  $feeCollectionId,
                'bank_voucher_number'   =>  $voucher,
                'bank_receipt'          =>  $bank_receipt,
                'created_by'                =>  $_SERVER['HTTP_USER'],
                'created_date'              =>  $currentDate,
            );
            $this->db->insert('drs_bank_deposit', $postingData);
        }
        return array("status"=>true,"message"=>'Revenue Posted successfully',"data"=>[]);
    }

    public function getDailyRevenueAuthorization($branch_id,$company_id,$payment_mode,$chosen_date)
    {
        if($payment_mode == 'cash')
        {
            $is_cash=0;
            return $this->getDailyRevenueCashAuthorization($branch_id,$company_id,$is_cash,$chosen_date);
        }
        else
        {
            $is_cash=1;
            return $this->getDailyRevenueOtherAuthorization($branch_id,$company_id,$is_cash,$chosen_date);
        }
    }

    public function getDailyRevenueCashAuthorization($branch_id,$company_id,$is_cash,$chosen_date)
    {
        $this->db->select("d.drs_id,dbd.drs_bank_deposit_id,dbd.fk_company_bank_id,cbd.bank_name,cbd.bank_city,cbd.bank_branch,cbd.acc_no,dbd.amount,dbd.bank_voucher_number");
        $this->db->from("drs d");
        $this->db->join("drs_bank_deposit dbd","d.drs_id=dbd.fk_drs_id","left");
        $this->db->join("company_bank_detail cbd","dbd.fk_company_bank_id=cbd.company_bank_detail_id","left");
        $this->db->where("d.fk_branch_id",$branch_id);
        $this->db->where("d.fk_company_id",$company_id);
        $this->db->where("d.is_cash",$is_cash);
        $this->db->where("DATE(d.created_date)",$chosen_date);
        $this->db->where("(dbd.authorized_by IS NULL OR dbd.authorized_by=0)");
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $response['revenueAuthorization']=$query->result();
                $this->db->select("`d`.`drs_id`, d.opening_balance,d.closing_balance,d.amount_deposited,d.amount_collected,d.signoff_amount");
                $this->db->from("drs d");
                $this->db->where("d.fk_branch_id",$branch_id);
                $this->db->where("d.fk_company_id",$company_id);
                $this->db->where("d.is_cash",$is_cash);
                $this->db->where("DATE(d.created_date)",$chosen_date);
                $query = $this->db->get();
                if ($query->num_rows() > 0)
                {
                    $response['openingBalance']     = (int) $query->row()->opening_balance;
                    $response['closingBalance']     = (int) $query->row()->closing_balance;
                    $response['amountCollection']   = (int) $query->row()->amount_collected;
                    $response['amountDeposited']    = (int) $query->row()->amount_deposited;
                    $response['signoffAmount']      = (int) $query->row()->signoff_amount;
                }
                else
                {
                    $response['openingBalance']     = 0;
                    $response['closingBalance']     = 0;
                    $response['amountDeposited']    = 0;
                    $response['signoffAmount']      = 0;
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$response);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'No records present to authorize',"data"=>[]);
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }

    public function getDailyRevenueOtherAuthorization($branch_id,$company_id,$is_cash,$chosen_date)
    {
        $this->db->select("d.drs_id,dbd.drs_bank_deposit_id,fc.receipt_number,DATE_FORMAT(fc.receipt_date, '%d %b,%Y') as receipt_date,fc.credit_card_number,fc.cheque_number,dbd.fk_company_bank_id,cbd.bank_name,dbd.bank_voucher_number,fc.payment_mode");
        $this->db->from("drs d");
        $this->db->join("drs_bank_deposit dbd","d.drs_id=dbd.fk_drs_id");
        $this->db->join("company_bank_detail cbd","dbd.fk_company_bank_id=cbd.company_bank_detail_id","left");
        $this->db->join("fee_collection fc","dbd.fk_fee_collection_id=fc.fee_collection_id","left");
        $this->db->where("d.fk_branch_id",$branch_id);
        $this->db->where("d.fk_company_id",$company_id);
        $this->db->where("d.is_cash",$is_cash);
        $this->db->where("DATE(d.created_date)",$chosen_date);
        $this->db->where("(dbd.authorized_by IS NULL OR dbd.authorized_by=0)");
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $res['revenueAuthorization']=$query->result();
                $this->db->select("`d`.`drs_id`, d.opening_balance,d.closing_balance,d.amount_deposited,d.amount_collected,d.signoff_amount");
                $this->db->from("drs d");
                $this->db->where("d.fk_branch_id",$branch_id);
                $this->db->where("d.fk_company_id",$company_id);
                $this->db->where("d.is_cash",$is_cash);
                $this->db->where("DATE(d.created_date)",$chosen_date);
                $query = $this->db->get();
                if ($query->num_rows() > 0)
                {
                    $response['openingBalance']     = $query->row()->opening_balance;
                    $response['closingBalance']     = $query->row()->closing_balance;
                    $response['amountDeposited']    = $query->row()->amount_deposited;
                    $response['signoffAmount']      = $query->row()->signoff_amount;
                }
                else
                {
                    $response['openingBalance']     = 0;
                    $response['closingBalance']     = 0;
                    $response['amountDeposited']    = 0;
                    $response['signoffAmount']      = 0;
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'No records present to authorize',"data"=>[]);
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }



    public function updateDailyRevenueAuthorization($rawRevenueAuthorizationItems)
    {
        $currentDate= date('Y-m-d H:i:s');
        for($i=0;$i<count($rawRevenueAuthorizationItems);$i++)
        {
            $RawauthorizeItem=explode('@@@@@@',$rawRevenueAuthorizationItems[$i]);
            $drs_item_id=$RawauthorizeItem[0];
            $authorizeComments=$RawauthorizeItem[1];
            $data = array(
                'authorized_by' => $_SERVER['HTTP_USER'],
                'authorized_comments' => $authorizeComments,
                'authorized_date' => $currentDate
            );
            $this->db->where('drs_bank_deposit_id', $drs_item_id);
            $this->db->update('drs_bank_deposit', $data);
        }
        return array("status"=>true,"message"=>'Revenue Posted successfully',"data"=>[]);
    }

    public function getDailyRevenueCollection($branch_id,$companyIds,$payment_mode,$chosen_date)
    {
        for($c=0;$c<count($companyIds);$c++)
        {
            $this->db->select("company_id,`name`,`code`");
            $this->db->from("company c");
            $this->db->where("c.company_id",$companyIds[$c]);
            $company_query = $this->db->get();

            $this->db->select("fh.fee_head_id,fh.`name`,SUM(fc.amount_paid) as amount_paid");
            $this->db->from("fee_collection fc");
            $this->db->join("fee_collection_item fci","fc.fee_collection_id=fci.fk_fee_collection_id","LEFT");
            $this->db->join("fee_head fh","fci.fk_fee_head_id=fh.fee_head_id","LEFT");
            $this->db->where("fc.fk_fee_company_id",$companyIds[$c]);
            $this->db->where("DATE(fc.receipt_date)",$chosen_date);
            $this->db->group_by("fci.fk_fee_head_id");
            $fh_query = $this->db->get();

            $this->db->select("fh.fee_head_id,fh.`name`,SUM(fc.amount_paid) as amount_paid");
            $this->db->from("fee_collection fc");
            $this->db->join("fee_collection_item fci","fc.fee_collection_id=fci.fk_fee_collection_id","LEFT");
            $this->db->join("fee_head fh","fci.fk_fee_head_id=fh.fee_head_id","LEFT");
            $this->db->where("fc.fk_fee_company_id",$companyIds[$c]);
            $this->db->where("DATE(fc.receipt_date)",$chosen_date);
            $fh_total_query = $this->db->get();

            $this->db->select("cbd.bank_name,cbd.acc_no,SUM(dbd.amount) as deposited_amount");
            $this->db->from("drs d");
            $this->db->join("drs_bank_deposit dbd","d.drs_id=dbd.fk_drs_id","LEFT");
            $this->db->join("company_bank_detail cbd","dbd.fk_company_bank_id=cbd.company_bank_detail_id");
            $this->db->where("d.fk_company_id",$companyIds[$c]);
            $this->db->where("DATE(d.posting_date)",$chosen_date);
            $this->db->group_by("cbd.company_bank_detail_id");
            $bank_deposits_query = $this->db->get();

            $this->db->select("opening_balance,closing_balance");
            $this->db->from("drs d");
            $this->db->where("d.fk_company_id",$companyIds[$c]);
            $this->db->where("d.is_cash",0);
            $this->db->where("DATE(d.posting_date)",$chosen_date);
            $this->db->order_by("d.posting_date", "desc");
            $this->db->limit(1);
            $in_hand_query = $this->db->get();

            if ($fh_query->num_rows() > 0)
            {
                $response[$company_query->row()->name]['collection']=$fh_query->result_array();
                $response[$company_query->row()->name]['total_collection']=$fh_total_query->row()->amount_paid;
            }
            else
            {
                $response[$company_query->row()->name]['collection']=0;
                $response[$company_query->row()->name]['total_collection']=0;
            }

            if ($bank_deposits_query->num_rows() > 0)
            {
                $response[$company_query->row()->name]['bank_deposits']=$bank_deposits_query->result_array();
            }
            else
            {
                $response[$company_query->row()->name]['bank_deposits']=0;
            }

            if ($in_hand_query->num_rows() > 0)
            {
                $response[$company_query->row()->name]['in_hand']=$in_hand_query->result_array();
            }
            else
            {
                $response[$company_query->row()->name]['in_hand']=0;
            }

        }
        return array("status"=>true,"message"=>'Revenue Collection details',"data"=>$response);
    }
}