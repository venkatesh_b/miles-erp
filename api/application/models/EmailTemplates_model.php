<?php

/**
 * @Model Name EmailTemplates_model
 * @category        Model
 * @author          Abhilash
 * @Description     Application related configuration
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EmailTemplates_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
    }

    function getAllEmailTemplate() {
        $table = 'email_template';
        // Table's primary key
        $primaryKey = 'e`.`eid';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array('db' => '`e`.`title`', 'dt' => 'title', 'field' => 'title'),
            array('db' => '`e`.`subject`', 'dt' => 'subject', 'field' => 'subject'), 
            array('db' => 'count(`et`.`eid`) AS `total`', 'dt' => 'total', 'field' => 'total'),
            array('db' => '`e`.`eid`', 'dt' => 'eid', 'field' => 'eid'),
            array('db' => '`et`.`etid`', 'dt' => 'etid', 'field' => 'etid'),
            array('db' => 'GROUP_CONCAT(`et`.`attachment`) AS attachment', 'dt' => 'attachment', 'field' => 'attachment'),
            array('db' => 'if(`et`.`status`=1,1,0) as `active_attachment`', 'dt' => 'active_attachment', 'field' => 'active_attachment')
        );

        $globalFilterColumns = array(
            array('db' => '`e`.`title`', 'dt' => 'title', 'field' => 'title'),
            array('db' => '`e`.`subject`', 'dt' => 'subject', 'field' => 'subject'),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db' => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM email_template e "
                . " LEFT JOIN email_template_attachment et ON (e.eid=et.eid AND `et`.`status`=1)";
        $extraWhere = "";
        $groupBy = "e.eid";

        $responseData = (SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere, $groupBy));

        return $responseData;
    }
    
    function getTemplate($key,$userData){
        $template = $this->getEmailTemplateByTitle($key);
		if(is_array($template) && isset($template['error']) && $template['error']== 'dberror'){
            return $template;
        } else if($template == 'nodata'){
            return 'nodata';
        }
        $rawHtml = $template['content'];
        $title = $template['title'];
        $subject = $template['subject'];
        $rawHtml = preg_replace('/{{title}}/',$title,$rawHtml);
        foreach($userData as $k => $v){
            $pattern = '/{{'.$k.'}}/';
            $rawHtml = preg_replace($pattern,$v,$rawHtml);
        }
        $attachment = [];
        if(isset($template['attachment']) && is_array($template['attachment'])){
            foreach($template['attachment'] as $k => $v){
                /*For making copy*/
                $file = FILE_UPLOAD_LOCATION.'mail_template/'.$v->attachment;
                $newfile = FILE_UPLOAD_LOCATION.'mail_template/'.$v->original_name;

                if (!copy($file, $newfile)) {
                    //return "failed to copy";
                }
                $attachment[] = FILE_UPLOAD_LOCATION.'mail_template/'.$v->original_name;
                /*delete functionality */
//                unlink(FILE_UPLOAD_LOCATION.'/mail_template/'.$v->original_name); 
            }
        }
        return array('content' => $rawHtml, 'title' => $subject, 'attachment' => $attachment);
    }
    
    function deleteOrginalFile($data){
//        print_r($data);die;
        if(is_array($data)){
            foreach ($data as $key => $value) {
                if(file_exists($value)){
                    unlink($value); 
                }
            }
        }else{
            if(file_exists($data)){
                unlink($data); 
            }
        }
    }
    
    function getEmailTemplateById($id){
        $this->db->select('*');
        $this->db->from('email_template e');
        $this->db->where('e.eid', $id);
        
        $result = $this->db->get();
//        die($this->db->last_query());
        if($result){
            $result = $result->result();
            if(count($result) > 0){
                $this->db->select('*');
                $this->db->from('email_template_attachment et');
                $this->db->where('et.eid', $id);
                $this->db->where('et.status', '1');
                $res = $this->db->get();
//                die($this->db->last_query());
                if($res){
                    $res = $res->result();
                    if(count($res) > 0){
                        return array(
                            'eid' => $result[0]->eid,
                            'key' => $result[0]->key,
                            'title' => $result[0]->title,
                            'subject' => $result[0]->subject,
                            'content' => $result[0]->content,
                            'attachment' => $res
                        );
                    }else{
                        return array(
                            'eid' => $result[0]->eid,
                            'key' => $result[0]->key,
                            'title' => $result[0]->title,
                            'subject' => $result[0]->subject,
                            'content' => $result[0]->content,
                            'attachment' => 0
                        );
                    }
                }else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }else{
                return 'nodata';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
    }
    
    function getEmailTemplatePreviewById($id){
        $this->db->select('*');
        $this->db->from('email_template e');
        $this->db->where('e.eid', $id);
        
        $result = $this->db->get();
//        die($this->db->last_query());
        if($result){
            $result = $result->result();
            if(count($result) > 0){
                $this->db->select('*');
                $this->db->from('email_template_attachment et');
                $this->db->where('et.eid', $id);
                $this->db->where('et.status', '1');
                $res = $this->db->get();
//                die($this->db->last_query());
                if($res){
                    $res = $res->result();
                        return array(
                            'eid' => $result[0]->eid,
                            'key' => $result[0]->key,
                            'title' => $result[0]->title,
                            'subject' => $result[0]->subject,
                            'content' => $result[0]->content,
                        );
                }else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }else{
                return 'nodata';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
    }
    
    function getEmailTemplateByTitle($key){
        $this->db->select('*');
        $this->db->from('email_template e');
        $this->db->where('e.key', $key);
        
        $result = $this->db->get();
        if($result){
            $result = $result->result();
            if(count($result) > 0){
                $this->db->select('*');
                $this->db->from('email_template_attachment et');
                $this->db->where('et.eid', $result[0]->eid);
                $this->db->where('et.status', '1');
                $res = $this->db->get();
                if($res){
                    $res = $res->result();
                    if(count($res) > 0){
                        return array(
                            'eid' => $result[0]->eid,
                            'key' => $result[0]->key,
                            'title' => $result[0]->title,
                            'content' => $result[0]->content,
                            'subject' => $result[0]->subject,
                            'attachment' => $res
                        );
                    }else{
                        return array(
                            'eid' => $result[0]->eid,
                            'title' => $result[0]->title,
                            'content' => $result[0]->content,
                            'subject' => $result[0]->subject,
                            'attachment' => 0
                        );
                    }
                }else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }else{
                return 'nodata';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
    }
    
    function saveTemplates($data, $id){
        $this->db->select('*');
        $this->db->from('email_template e');
        $this->db->where('e.eid', $id);
        $result = $this->db->get();
        if($result){
            $result = $result->result();
            if(count($result) > 0){
                $this->db->trans_begin();
                $this->db->where('eid' ,$id);
                $this->db->update('email_template',
                    array('content' => $data['content'],
                        'title' => $data['title'],
                        'subject' => $data['subject']
                    )
                );
                if(isset($data['image'])){
                    if(is_array($data['image'])){
                        foreach($data['image'] as $k=>$v){
                            $this->db->insert('email_template_attachment',array(
                                                                            'attachment' => $v,
                                                                            'original_name' => $data['orginalName'][$k],
                                                                            'status' => '1',
                                                                            'eid' => $id,
                                                                        )
                            );
                        }
                    }else{
                        $this->db->insert('email_template_attachment',array(
                                                                            'attachment' => $data['image'],
                                                                            'original_name' => $data['orginalName'],
                                                                            'status' => '1',
                                                                            'eid' => $id,
                                                                        )
                        );
                    }
                }
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            } else {
                return 'nodata';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    function deleteAttachment($id, $etid){
        $this->db->select('*');
        $this->db->from('email_template e');
        $this->db->where('e.eid', $id);
        
        $result = $this->db->get();
        if($result){
            $result = $result->result();
            if(count($result) > 0){
                $this->db->trans_begin();
                $this->db->where('etid', $etid);
                $this->db->update('email_template_attachment', array('status' => 0));
//                die($this->db->last_query());
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            }else {
                return 'nodata';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

}