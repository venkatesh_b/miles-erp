<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 21/3/16
 * Time: 9:49 PM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FeeAssign_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    function getFeeDetailstoLeadNumber($lead_number,$branch='',$courseId='')
    {
        $this->db->select("l.fk_company_id,rtvcontact_type.value as contact_type,rtvcontact_type.reference_type_value_id as contact_type_id,l.fk_institution_id as inisti_id,bl.branch_xref_lead_id,l.name,l.phone,l.email,bl.branch_id, bl.fk_course_id,c.name as course, fs.fk_corporate_company_id
           ,fs.fk_institution_id,rtvcmy.value as company,rtvini.value as institute, rtvqual.value as qualification, b.name as branch,c.name as course
            ,cf.candidate_fee_id,fs.fk_type_id,fs.is_allowed_for_class_room_training,fs.is_allowed_for_online_training,cf.fk_fee_structure_id,fs.fk_training_type,fs.is_regular_fee");
        $this->db->from("branch_xref_lead bl");
        $this->db->join("branch b","b.branch_id=bl.branch_id");
        $this->db->join('course c','c.course_id=bl.fk_course_id','left');
        $this->db->join("lead l","l.lead_id = bl.lead_id","left");
        $this->db->join("reference_type_value rtvqual","rtvqual.reference_type_value_id=l.fk_qualification_id","left");
        $this->db->join("reference_type_value rtvcontact_type","rtvcontact_type.reference_type_value_id=l.fk_contact_type_id","left");
        $this->db->join("reference_type_value rtvcmy","rtvcmy.reference_type_value_id=l.fk_company_id","left");
        $this->db->join("reference_type_value rtvini","rtvini.reference_type_value_id=l.fk_institution_id","left");
        $this->db->join("candidate_fee cf","cf.fk_branch_xref_lead_id=bl.branch_xref_lead_id AND cf.is_regular_fee=1 AND cf.is_active=0","left");
        $this->db->join("fee_structure fs","fs.fee_structure_id=cf.fk_fee_structure_id","left");
        //$this->db->join("candidate_fee_item cfi","cfi.fk_candidate_fee_id=cf.candidate_fee_id","left");
        $this->db->where("bl.lead_number",$lead_number);
        $this->db->where("bl.fk_course_id",$courseId);
        $this->db->where("bl.branch_id",$branch);
        $this->db->where("bl.is_active",1);
        $query = $this->db->get();
       //echo  $this->db->last_query();exit;
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Lead number not exist',"data"=>array('name'=>'Lead number not exist'));
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
     }

    function saveCandidate($data){
        $db_response="";
        if(isset($data) && !empty($data))
        {
            if($data['pageUrl']!=null && $data['courseId']!=null)
            {
                $this->db->select("branch_xref_lead_id");
                $this->db->from("branch_xref_lead");
                $this->db->where("branch_xref_lead_id",$data['fk_branch_xref_lead_id']);
                $this->db->where("fk_course_id",$data['courseId']);
                $lead_data=$this->db->get();
                if ($lead_data->num_rows() == 0)
                {
                    $insert_branch_xref_query="INSERT INTO branch_xref_lead(lead_id,branch_id,fk_course_id,fk_batch_id,fk_user_xref_lead_history_id,`status`,next_followup_date,updated_by,updated_on,lead_number,Significance_of_CPA,Willingness_to_pursue_the_CPA,Response_on_Miles_CPA,Value_addition,CMA_No,expected_visit_date,expected_enroll_date,expected_admission_date,is_mail_sent,transferred_to,transferred_from,dnd_count,logic_item_count,subscribe_status,eligibility,created_by,created_on) SELECT lead_id,branch_id,".$data['courseId'].",fk_batch_id,fk_user_xref_lead_history_id,`status`,next_followup_date,updated_by,updated_on,lead_number,Significance_of_CPA,Willingness_to_pursue_the_CPA,Response_on_Miles_CPA,Value_addition,CMA_No,expected_visit_date,expected_enroll_date,expected_admission_date,is_mail_sent,transferred_to,transferred_from,dnd_count,logic_item_count,subscribe_status,eligibility,".$data['assigned_by'].",'".$data['assigned_on']."' FROM branch_xref_lead WHERE branch_xref_lead_id=".$data['fk_branch_xref_lead_id'];
                    $query_insert=$this->db->query($insert_branch_xref_query);
                    $data['fk_branch_xref_lead_id']=$this->db->insert_id();
                }
                else
                {
                    $this->db->select("candidate_fee_id");
                    $this->db->from("candidate_fee");
                    $this->db->where("fk_branch_xref_lead_id",$data['fk_branch_xref_lead_id']);
                    $cf_data_query=$this->db->get();
                    if($cf_data_query->num_rows()>0)
                    {
                        $cf_data=$cf_data_query->result();
                        $update_data['is_active']=1;
                        $this->db->where('candidate_fee_id',$cf_data[0]->candidate_fee_id);
                        $this->db->update('candidate_fee',$update_data);

                        $this->db->set('amount_payable', 'amount_paid', FALSE);
                        $this->db->where('fk_candidate_fee_id', $cf_data[0]->candidate_fee_id);
                        $this->db->update('candidate_fee_item');
                        //echo $this->db->last_query();exit;
                    }
                }
            }
            $candidate_fee_data['fk_fee_structure_id']=$data['fk_fee_structure_id'];
            $candidate_fee_data['fk_branch_xref_lead_id']=$data['fk_branch_xref_lead_id'];
            $candidate_fee_data['assigned_on']=$data['assigned_on'];
            $candidate_fee_data['assigned_by']=$data['assigned_by'];
            $insert_can_fee=$this->db->insert('candidate_fee',$candidate_fee_data);
        }
        $last_id=$this->db->insert_id();

        if(isset($last_id))
        {
            $data_result=$this->getFeeItems($data['fk_fee_structure_id']);
            if(isset($data_result['data']) && !empty($data_result['data'])){
                foreach($data_result['data'] as $v)
                {
                    $candidateFeeItem['fk_candidate_fee_id']=$last_id;
                    $candidateFeeItem['fk_fee_structure_item_id']=$v->fee_structure_item_id;
                    $candidateFeeItem['amount_payable']=$v->amount;
                    $candidateFeeItem['created_on']=date('Y-m-d H:i:s');
                    $candidateFeeItem['created_by']=$_SERVER['HTTP_USER'];
                    $res_insert = $this->db->insert('candidate_fee_item',$candidateFeeItem);
                    if ($res_insert)
                    {
                        $db_response = array("status" => true, "message" => "Fee assigned successfully", "data" => array());
                    } else
                    {
                        $error = $this->db->error();
                        $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
                    }
                }
            }
        }
        return $db_response;
    }
    public function getFeeItems($id){
        $this->db->select("fsi.fee_structure_item_id,fsi.due_days,fsi.amount,fh.name as fee_head_name");
        $this->db->from('fee_structure_item fsi');
        $this->db->join('fee_head fh','fh.fee_head_id=fsi.fk_fee_head_id','left');
        $this->db->where("fsi.fk_fee_structure_id",$id);
        $query = $this->db->get();

        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'No  Data Found',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    public function getFeeDetailsFromCandidateItem($id){

//        $this->db->select("candidate_fee_item_id,fk_fee_structure_item_id,amount_payable");
//        $this->db->from('candidate_fee_item');
//        $this->db->where("fk_candidate_fee_id",$id);
//
        $this->db->select("cfi.candidate_fee_item_id,cfi.amount_payable,cfi.fk_fee_structure_item_id,fh.name as feeHeadName,fsi.due_days");
        $this->db->from('candidate_fee_item cfi');
        $this->db->join(" fee_structure_item fsi","fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id","left");
        $this->db->join(" fee_head fh","fh.fee_head_id=fsi.fk_fee_head_id","left");
        $this->db->where("cfi.fk_candidate_fee_id",$id);


        $query = $this->db->get();

        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'No  Data Found',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    public function getFeeStructureInfo($data)
    {
        $current_date=date('Y-m-d');
        $this->db->select("*");
        $this->db->from('fee_structure');
        //$this->db->where("fk_branch_id",$data['fk_branch_id']);
        if(isset($data['fk_branch_id']) && !empty($data['fk_branch_id']))
        {
            $this->db->where("fk_branch_id",$data['fk_branch_id']);
        }
        else
        {
            $this->db->where("fk_branch_id",null);
        }
        if(isset($data['fk_type_id']))
        {
            $this->db->where("fk_type_id", $data['fk_type_id']);
        }
        if(isset($data['fk_institution_id']))
        {
            $this->db->where("fk_institution_id", $data['fk_institution_id']);
        }
        else
        {
            $this->db->where("fk_institution_id",null);
        }
        if(isset($data['fk_corporate_company_id']))
        {
            $this->db->where("fk_corporate_company_id", $data['fk_corporate_company_id']);
        }
        else
        {
            $this->db->where("fk_corporate_company_id",null);
        }
        $this->db->where("fk_course_id",$data['fk_course_id']);
        //$this->db->where("fk_course_id",1);
        $this->db->where("fk_training_type",$data['fk_training_type']);
        $this->db->where("is_regular_fee",1);
        $this->db->where("end_date is null");
        $this->db->where("DATE(effective_from) <= '".$current_date."'");
        $this->db->where("is_active",1);
        $this->db->order_by("effective_from",'desc');
        $this->db->limit(1);
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $this->db->select("*");
                $this->db->from('fee_structure');
                $this->db->where("fk_branch_id",null);
                if(isset($data['fk_type_id']))
                {
                    $this->db->where("fk_type_id", $data['fk_type_id']);
                }
                if(isset($data['fk_institution_id']))
                {
                    $this->db->where("fk_institution_id", $data['fk_institution_id']);
                }
                else
                {
                    $this->db->where("fk_institution_id",null);
                }
                if(isset($data['fk_corporate_company_id']))
                {
                    $this->db->where("fk_corporate_company_id", $data['fk_corporate_company_id']);
                }
                else
                {
                    $this->db->where("fk_corporate_company_id",null);
                }
                $this->db->where("fk_course_id",$data['fk_course_id']);
                //$this->db->where("fk_course_id",1);
                $this->db->where("fk_training_type",$data['fk_training_type']);
                $this->db->where("is_regular_fee",1);
                $this->db->where("end_date is null");
                //$this->db->where("DATE(effective_from) <= DATE(".$current_datetime.")");
                $this->db->where("DATE(effective_from) <= '".$current_date."'");
                $this->db->where("is_active",1);
                $this->db->order_by("effective_from",'desc');
                $this->db->limit(1);
                $query = $this->db->get();
                if($query)
                {
                    if ($query->num_rows() > 0)
                    {
                        $res = $query->result();
                        $db_response = array("status" => true, "message" => 'success', "data" => $res);
                    }
                    else
                    {
                        $db_response=array("status"=>false,"message"=>'No  Data Found',"data"=>array());
                    }
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }

    public function getFeeAssignList($branchId)
    {

        $table = 'candidate_fee';
        // Table's primary key
        $primaryKey = 'candidate_fee_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'l.name as lead_name',          'dt' => 'lead_name',         'field' => 'lead_name' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'c.name as course_name',          'dt' => 'course_name',         'field' => 'course_name' ),
            array( 'db' => 'sum(cfi.amount_payable) as amount_payable',           'dt' => 'amount_payable',    'field' => 'amount_payable' ),
            array( 'db' => 'sum(cfi.amount_paid) as amount_paid',              'dt' => 'amount_paid',       'field' => 'amount_paid' ),
            array( 'db' => 'fh.name as feeHeadName',       'dt' => 'feeHeadName',       'field' => 'feeHeadName' ),
            array( 'db' => 'cf.candidate_fee_id',          'dt' => 'candidate_fee_id',  'field' => 'candidate_fee_id' ),
            array( 'db' => 'cfi.candidate_fee_item_id',          'dt' => 'candidate_fee_item_id',  'field' => 'candidate_fee_item_id' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'cfi.amount_payable',   'dt' => 'amount_payable',   'field' => 'amount_payable' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'fh.name',   'dt' => 'fh.name',   'field' => 'fh.name' ),
            array( 'db' => 'c.name',          'dt' => 'c.name',         'field' => 'c.name' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from candidate_fee cf
                        left join branch_xref_lead bl on bl.branch_xref_lead_id = cf.fk_branch_xref_lead_id
                        left join course c on c.course_id = bl.fk_course_id
                        left join lead l on l.lead_id=bl.lead_id
                        left join candidate_fee_item cfi on cfi.fk_candidate_fee_id=cf.candidate_fee_id
                        left join fee_structure_item fsi on fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id
                        left join fee_head fh on fh.fee_head_id=fsi.fk_fee_head_id
                        ";
        //$extraWhere = "";
        $extraWhere = " bl.branch_id=".$branchId;
        $groupBy = "cf.candidate_fee_id";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));
        return $responseData;
    }

    /* Added by s.raju */

    function getFeeAssignInfo($params){

        $this->db->select('l.name,l.phone,l.email,b.name as branchName,c.name as courseName,fh.name feeHeadName,cfi.amount_payable,cfi.candidate_fee_item_id,rtvCompany.value as companyName,rtvQualification.value as qualification,rtvInstitution.value as institution,rtvContactType.value as contactType,fsi.due_days,cf.candidate_fee_id,cfi.modified_amount');
        $this->db->from('lead l');
        $this->db->JOIN('branch_xref_lead bxl','bxl.lead_id=l.lead_id');
        $this->db->JOIN('branch b','b.branch_id=bxl.branch_id');
        $this->db->JOIN('course c','c.course_id=bxl.fk_course_id');
        $this->db->JOIN('candidate_fee cf','cf.fk_branch_xref_lead_id=bxl.branch_xref_lead_id');
        $this->db->JOIN('candidate_fee_item cfi','cfi.fk_candidate_fee_id=cf.candidate_fee_id');
        $this->db->JOIN('fee_structure_item fsi','fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id');
        $this->db->JOIN('fee_head fh','fh.fee_head_id=fk_fee_head_id');
        $this->db->JOIN('reference_type_value rtvCompany','rtvCompany.reference_type_value_id=l.fk_company_id','LEFT');
        $this->db->JOIN('reference_type_value rtvQualification','rtvQualification.reference_type_value_id=l.fk_qualification_id','LEFT');
        $this->db->JOIN('reference_type_value rtvInstitution','rtvInstitution.reference_type_value_id=l.fk_institution_id','LEFT');
        $this->db->JOIN('reference_type_value rtvContactType','rtvContactType.reference_type_value_id=l.fk_contact_type_id','LEFT');
        $this->db->where('cf.candidate_fee_id',$params['candidate_fee_item_id']);
        $this->db->order_by('fsi.due_days','asc');
        $res_query=$this->db->get();
        if($res_query){
            if($res_query->num_rows()>0){
                $result=$res_query->result();
                $result[0]->is_concession_pending=0;
                foreach($result as $k_r=>$v_r){
                    $candidate_fee_id=$v_r->candidate_fee_id;
                    $v_r->is_modification_allowed=0;
                    $this->db->select('candidate_fee_item_id');
                    $this->db->from('candidate_fee_item');
                    $this->db->where('amount_paid>',0);
                    $this->db->where('fk_candidate_fee_id',$candidate_fee_id);
                    $res_query_sub=$this->db->get();
                    if($res_query_sub->num_rows()>0){
                        $v_r->is_modification_allowed=0;
                    }
                    else{
                        $v_r->is_modification_allowed=1;
                    }

                    $this->db->select('cfi.candidate_fee_item_id,cfi.concession,cf.approved_status,cf.approval_type,cfi.right_of_amount');
                    $this->db->from('candidate_fee_item cfi');
                    $this->db->join('candidate_fee cf','cf.candidate_fee_id=cfi.fk_candidate_fee_id');
                    $this->db->where('cfi.candidate_fee_item_id',$v_r->candidate_fee_item_id);
                    $res_query_sub=$this->db->get();
                    if($res_query_sub->num_rows()>0){
                        $concessions=$res_query_sub->result();
                        foreach($concessions as $k_concession=>$v_concession){
                            $v_r->concession_amount=$v_concession->concession;
                            if($v_concession->approval_type=='Concession' && $v_concession->approved_status=='Pending' ){
                                //$v_r->concession_amount=0;
                                $result[0]->is_concession_pending=1;

                            }
                            else{
                                //$v_r->concession_amount=0;
                                if($v_concession->approval_type=='Concession' && $v_concession->approved_status=='Success' ){
                                    //$v_r->concession_amount=$v_concession->concession;
                                }
                                else{
                                    //$v_r->concession_amount=0;
                                }


                            }
                            $v_r->right_of_amount=$v_concession->right_of_amount;

                        }

                    }
                    else{

                    }




                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$result);
            }
            else{
                $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array('status'=>false,'message'=>$error['message'],'data'=>array());
        }
        return $db_response;
    }
    function saveModifyFee($params){
        $userRole=$_SERVER['HTTP_USERROLE'];
        $feeModifications_exp=explode('|',$params['feeModifications']);
        $current_datetime=date('Y-m-d H:i:s');
        for($i=0;$i<count($feeModifications_exp);$i++){
            $feeModifications_exp2=explode('-',$feeModifications_exp[$i]);
            $data_update['modified_amount']=$feeModifications_exp2[1];
            $data_update['updated_by']=$_SERVER['HTTP_USER'];
            $data_update['updated_on']=$current_datetime;
            $this->db->where('candidate_fee_item_id',$feeModifications_exp2[0]);
            $res_update=$this->db->update('candidate_fee_item',$data_update);
            if($res_update){
                if(strtolower($userRole)=='superadmin' || $params['type']=='approval'){

                    $data_update_main='';
                    $data_update_main['amount_payable']=$feeModifications_exp2[1];
                    $data_update_main['modified_amount']=$feeModifications_exp2[1];
                    $data_update_main['updated_by']=$_SERVER['HTTP_USER'];
                    $data_update_main['updated_on']=$current_datetime;
                    $this->db->where('candidate_fee_item_id',$feeModifications_exp2[0]);
                    $res_update_main=$this->db->update('candidate_fee_item',$data_update_main);

                    $data_update_head = array(
                        'modified_by' => $_SERVER['HTTP_USER'],
                        'modified_on' => $current_datetime,
                        'approval_type' =>'Fee modification','approved_status'=>'Success');
                    $this->db->where('candidate_fee_id', $params['candidate_fee_id']);
                    $this->db->update('candidate_fee',$data_update_head);

                    $comments='Fee modification Approved';

                }
                else{
                    $data_update_head = array(
                        'modified_by' => $_SERVER['HTTP_USER'],
                        'modified_on' => $current_datetime,
                        'approval_type' =>'Fee modification','approved_status'=>'Pending');
                    $this->db->where('candidate_fee_id', $params['candidate_fee_id']);
                    $this->db->update('candidate_fee',$data_update_head);
                    $comments='Fee modification Pending';
                }

                $data_audit['fk_candidate_fee_id']=$params['candidate_fee_id'];
                $data_audit['fk_candidate_fee_item_id']=$feeModifications_exp2[0];
                $data_audit['amount']=$feeModifications_exp2[1];
                $data_audit['fk_created_by']=$_SERVER['HTTP_USER'];
                $data_audit['created_on']=$current_datetime;
                $data_audit['type']='Fee modification';
                $data_audit['comments']=$comments;
                $this->db->insert('fee_audit',$data_audit);


                $db_response=array('status'=>true,'message'=>'Fee Modified Successfully','data'=>array());
            }
            else{
                $error = $this->db->error();
                $db_response=array('status'=>false,'message'=>$error['message'],'data'=>array());
            }

        }

        return $db_response;
    }

    function getFeeAssignInfoByLeadNumber($params)
    {
        $this->db->select('l.name,l.phone,l.email,b.name as branchName,c.name as courseName,fh.name feeHeadName,cfi.amount_payable,cfi.amount_paid,cfi.modified_concession,cfi.candidate_fee_item_id,rtvCompany.value as companyName,rtvQualification.value as qualification,rtvInstitution.value as institution,rtvContactType.value as contactType,fsi.due_days,cf.candidate_fee_id,cf.approved_status,cf.approval_type,cfi.modified_amount');
        $this->db->from('lead l');
        $this->db->JOIN('branch_xref_lead bxl','bxl.lead_id=l.lead_id');
        $this->db->JOIN('branch b','b.branch_id=bxl.branch_id');
        $this->db->JOIN('course c','c.course_id=bxl.fk_course_id');
        $this->db->JOIN('candidate_fee cf','cf.fk_branch_xref_lead_id=bxl.branch_xref_lead_id');
        $this->db->JOIN('candidate_fee_item cfi','cfi.fk_candidate_fee_id=cf.candidate_fee_id');
        $this->db->JOIN('fee_structure_item fsi','fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id');
        $this->db->JOIN('fee_head fh','fh.fee_head_id=fk_fee_head_id');
        $this->db->JOIN('reference_type_value rtvCompany','rtvCompany.reference_type_value_id=l.fk_company_id','LEFT');
        $this->db->JOIN('reference_type_value rtvQualification','rtvQualification.reference_type_value_id=l.fk_qualification_id','LEFT');
        $this->db->JOIN('reference_type_value rtvInstitution','rtvInstitution.reference_type_value_id=l.fk_institution_id','LEFT');
        $this->db->JOIN('reference_type_value rtvContactType','rtvContactType.reference_type_value_id=l.fk_contact_type_id','LEFT');
        $this->db->where('bxl.lead_number',$params['LeadNumber']);
        $this->db->where('cf.is_regular_fee',1);
        $this->db->where('cfi.amount_payable > 0');
        $res_query=$this->db->get();
        //echo $this->db->last_query();exit;
        if($res_query){
            if($res_query->num_rows()>0){
                $result=$res_query->result();
                $is_concession_allowed=1;
                $is_modification_request=0;
                foreach($result as $k_r=>$v_r)
                {
                    $candidate_fee_id=$v_r->candidate_fee_id;
                    $approved_status=$v_r->approved_status;
                    if($approved_status == '' || $approved_status == null || $approved_status == 'Success')
                        $v_r->approve_status=0;
                    else
                        $v_r->approve_status=1;
                    if($approved_status == 'Pending' && $v_r->approval_type == 'Fee Modification')
                        $is_modification_request=1;
                    //$where="amount_paid > 0  AND fk_candidate_fee_id = ".$candidate_fee_id;
                    /*$where="fk_candidate_fee_id = ".$candidate_fee_id;
                    $this->db->select('candidate_fee_item_id');
                    $this->db->from('candidate_fee_item');
                    $this->db->where($where);
                    $res_query_sub=$this->db->get();
                    if($res_query_sub->num_rows()>0 && $v_r->approve_status==1)
                    {
                        $v_r->is_concession_allowed=0;
                    }
                    else
                    {
                        $v_r->is_concession_allowed=1;
                        $is_concession_allowed=1;
                    }*/

                    $result[0]->is_concession_allowed=$is_concession_allowed;
                    $result[0]->is_modification_request=$is_modification_request;
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$result);
            }
            else
            {
                $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array('status'=>false,'message'=>$error['message'],'data'=>array());
        }
        return $db_response;
    }

    function checkForCandidateFeeConcession($candidateFeeId)
    {
        $where="cfi.amount_paid > 0 AND (ISNULL(cf.approved_status) OR cf.approved_status='Success') AND cf.candidate_fee_id = ".$candidateFeeId;
        $this->db->select('cf.candidate_fee_id');
        $this->db->from('candidate_fee cf');
        $this->db->JOIN('candidate_fee_item cfi','cf.candidate_fee_id=cfi.fk_candidate_fee_id');
        $this->db->where($where);
        $res_query_sub=$this->db->get();
        return $res_query_sub->num_rows();
    }
    function checkForCandidateFeeConcessionStatus($candidateFeeId)
    {
        $where="cf.approval_type='Concession' and cf.candidate_fee_id = ".$candidateFeeId;
        $this->db->select('cf.candidate_fee_id,cf.approved_status');
        $this->db->from('candidate_fee cf');
        $this->db->where($where);
        $res_query=$this->db->get();
        return $res_query->result();
    }
    function checkForCandidateFeeModificationStatus($candidateFeeId)
    {
        $where="cf.approval_type='Fee modification' and cf.candidate_fee_id = ".$candidateFeeId;
        $this->db->select('cf.candidate_fee_id,cf.approved_status');
        $this->db->from('candidate_fee cf');
        $this->db->where($where);
        $res_query=$this->db->get();
        return $res_query->result();
    }
    function updateCandidateFeeConcession($candidateFeeId,$feeConcessionData)
    {
        $current_datetime=date('Y-m-d H:i:s');
        $userRole=$_SERVER['HTTP_USERROLE'];

        for($f=0;$f<count($feeConcessionData);$f++)
        {
            $feeConcessionRaw=explode("@@@@@@",$feeConcessionData[$f]);
            $candidateFeeId=$feeConcessionRaw[0];
            $candidateFeeItemId=$feeConcessionRaw[1];
            $amountPayable=$feeConcessionRaw[2];
            $concessionAmount=$feeConcessionRaw[3];
            if($userRole == 'Superadmin')
            {
                $updateConcessionQuery="UPDATE candidate_fee_item cfi JOIN candidate_fee cf ON cfi.fk_candidate_fee_id=cf.candidate_fee_id SET cfi.concession=".$concessionAmount.", cfi.modified_concession=".$concessionAmount.", cf.approval_type='Concession',cf.approved_status='Success',cf.requested_by=".$_SERVER['HTTP_USER'].",cf.requested_on='".$current_datetime."',cfi.updated_by=".$_SERVER['HTTP_USER'].",cfi.updated_on='".$current_datetime."' WHERE cfi.candidate_fee_item_id=".$candidateFeeItemId;
                $audit_comments='Concession Approved';
            }
            else
            {
                $updateConcessionQuery="UPDATE candidate_fee_item cfi JOIN candidate_fee cf ON cfi.fk_candidate_fee_id=cf.candidate_fee_id SET cfi.concession=0,cfi.modified_concession=".$concessionAmount.", cf.approval_type='Concession',cf.approved_status='Pending',cf.requested_by=".$_SERVER['HTTP_USER'].",cf.requested_on='".$current_datetime."',cfi.updated_by=".$_SERVER['HTTP_USER'].",cfi.updated_on='".$current_datetime."' WHERE cfi.candidate_fee_item_id=".$candidateFeeItemId;
                $audit_comments='Concession Pending';
            }
            //echo $updateConcessionQuery;exit;
            $query_update=$this->db->query($updateConcessionQuery);
            if($query_update)
            {
                $data['fk_candidate_fee_id']=$candidateFeeId;
                $data['fk_candidate_fee_item_id']=$candidateFeeItemId;
                $data['amount']=$concessionAmount;
                $data['fk_created_by']=$_SERVER['HTTP_USER'];
                $data['created_on']=$current_datetime;
                $data['type']='Concession';
                $data['comments']=$audit_comments;
                $this->db->insert('fee_audit',$data);
            }
            else
            {
                $error = $this->db->error();
                return array('status'=>false,'message'=>$error['message'],'data'=>array());
            }
        }
        $db_response=array('status'=>true,'message'=>'Concession request is success','data'=>array());
        return $db_response;
    }


    function approveCandidateFeeConcession($candidateFeeId,$feeConcessionData)
    {
        $current_datetime=date('Y-m-d H:i:s');
        for($f=0;$f<count($feeConcessionData);$f++)
        {
            $feeConcessionRaw=explode("@@@@@@",$feeConcessionData[$f]);
            $candidateFeeItemId=$feeConcessionRaw[0];
            $amountPayable=$feeConcessionRaw[1];
            $concessionAmount=$feeConcessionRaw[2];
            $updateConcessionQuery="UPDATE candidate_fee_item cfi JOIN candidate_fee cf ON cfi.fk_candidate_fee_id=cf.candidate_fee_id SET cfi.concession=".$concessionAmount.",cfi.modified_concession=".$concessionAmount.", cf.approval_type='Concession',cf.approved_status='Success',cf.requested_by=".$_SERVER['HTTP_USER'].",cf.requested_on='".$current_datetime."',cfi.updated_by=".$_SERVER['HTTP_USER'].",cfi.updated_on='".$current_datetime."' WHERE cfi.candidate_fee_item_id=".$candidateFeeItemId;
            $query_update=$this->db->query($updateConcessionQuery);
            $data['fk_candidate_fee_id']=$candidateFeeId;
            $data['fk_candidate_fee_item_id']=$candidateFeeItemId;
            $data['amount']=$concessionAmount;
            $data['fk_created_by']=$_SERVER['HTTP_USER'];
            $data['created_on']=$current_datetime;
            $data['type']='Concession';
            $audit_comments='Concession Approved';
            $data['comments']=$audit_comments;
            $this->db->insert('fee_audit',$data);
        }
        $db_response=array('status'=>true,'message'=>'Concession approved successfully','data'=>array());
        return $db_response;
    }

    public function rejectCandidateFeeConcessionRequest($candidateFeeId,$requestType)
    {
        $current_datetime=date('Y-m-d H:i:s');
        if($requestType == 'reject')
        {
            $data = array(
                'approval_type' => 'Concession',
                'approved_status' => 'Rejected',
                'approved_by' => $_SERVER['HTTP_USER'],
                'approved_on' => $current_datetime
            );
            $this->db->trans_start();
            $this->db->where('candidate_fee_id', $candidateFeeId);
            $this->db->update('candidate_fee', $data);

            $data = array(
                'concession' => 0,
                'modified_concession' => 0,
                'updated_by' => $_SERVER['HTTP_USER'],
                'updated_on' => $current_datetime
            );
            $this->db->where('fk_candidate_fee_id', $candidateFeeId);
            $this->db->update('candidate_fee_item', $data);

            $this->db->select('candidate_fee_item_id,modified_concession');
            $this->db->from('candidate_fee_item');
            $this->db->where('fk_candidate_fee_id',$candidateFeeId);
            $res=$this->db->get();
            if($res){
                if($res->num_rows()>0){
                    foreach($res->result() as $k=>$v){
                        $data_audit=[];
                        $data_audit['fk_candidate_fee_id']=$candidateFeeId;
                        $data_audit['fk_candidate_fee_item_id']=$v->candidate_fee_item_id;
                        $data_audit['amount']=$v->modified_concession;
                        $data_audit['fk_created_by']=$_SERVER['HTTP_USER'];
                        $data_audit['created_on']=$current_datetime;
                        $data_audit['type']='Concession';
                        $audit_comments='Concession Rejected';
                        $data_audit['comments']=$audit_comments;
                        $this->db->insert('fee_audit',$data_audit);
                    }
                }
            }


            $this->db->trans_complete();
            if($this->db->trans_status() === FALSE)
            {
                $db_response=array('status'=>false,'message'=>'Request cannot be processed','data'=>array());
            }
            else
            {
                $db_response=array('status'=>true,'message'=>'Concession rejected successfully','data'=>array());
            }
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'Invalid concession request','data'=>array());
        }
        return $db_response;
    }
    public function rejectCandidateFeeModificationRequest($candidateFeeId,$requestType)
    {
        $current_datetime=date('Y-m-d H:i:s');
        if($requestType == 'reject')
        {
            $data = array(
                'approval_type' => 'Fee modification',
                'approved_status' => 'Rejected',
                'approved_by' => $_SERVER['HTTP_USER'],
                'approved_on' => $current_datetime
            );
            $this->db->trans_start();
            $this->db->where('candidate_fee_id', $candidateFeeId);
            $this->db->update('candidate_fee', $data);

            $this->db->select('candidate_fee_item_id,modified_amount');
            $this->db->from('candidate_fee_item');
            $this->db->where('fk_candidate_fee_id',$candidateFeeId);
            $res=$this->db->get();
            if($res){
                if($res->num_rows()>0){
                    foreach($res->result() as $k=>$v){
                        $data_audit=[];
                        $data_audit['fk_candidate_fee_id']=$candidateFeeId;
                        $data_audit['fk_candidate_fee_item_id']=$v->candidate_fee_item_id;
                        $data_audit['amount']=$v->modified_amount;
                        $data_audit['fk_created_by']=$_SERVER['HTTP_USER'];
                        $data_audit['created_on']=$current_datetime;
                        $data_audit['type']='Fee modification';
                        $audit_comments='Fee modification Rejected';
                        $data_audit['comments']=$audit_comments;
                        $this->db->insert('fee_audit',$data_audit);
                    }
                }
            }


            $this->db->trans_complete();
            if($this->db->trans_status() === FALSE)
            {
                $db_response=array('status'=>false,'message'=>'Request cannot be processed','data'=>array());
            }
            else
            {
                $db_response=array('status'=>true,'message'=>'Fee modification rejected successfully','data'=>array());
            }
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'Invalid fee modification request','data'=>array());
        }
        return $db_response;
    }

    public function getFeeAssignInfoByCandidateFeeId($candidateFeeId)
    {
        $this->db->select('l.name,l.phone,l.email,b.name as branchName,c.name as courseName,fh.name feeHeadName,cfi.amount_payable,cfi.amount_paid,cfi.modified_concession,cfi.candidate_fee_item_id,rtvCompany.value as companyName,rtvQualification.value as qualification,rtvInstitution.value as institution,rtvContactType.value as contactType,fsi.due_days,cf.candidate_fee_id,cf.approved_status,cf.approval_type');
        $this->db->from('lead l');
        $this->db->JOIN('branch_xref_lead bxl','bxl.lead_id=l.lead_id');
        $this->db->JOIN('branch b','b.branch_id=bxl.branch_id');
        $this->db->JOIN('course c','c.course_id=bxl.fk_course_id');
        $this->db->JOIN('candidate_fee cf','cf.fk_branch_xref_lead_id=bxl.branch_xref_lead_id');
        $this->db->JOIN('candidate_fee_item cfi','cfi.fk_candidate_fee_id=cf.candidate_fee_id');
        $this->db->JOIN('fee_structure_item fsi','fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id');
        $this->db->JOIN('fee_head fh','fh.fee_head_id=fk_fee_head_id');
        $this->db->JOIN('reference_type_value rtvCompany','rtvCompany.reference_type_value_id=l.fk_company_id','LEFT');
        $this->db->JOIN('reference_type_value rtvQualification','rtvQualification.reference_type_value_id=l.fk_qualification_id','LEFT');
        $this->db->JOIN('reference_type_value rtvInstitution','rtvInstitution.reference_type_value_id=l.fk_institution_id','LEFT');
        $this->db->JOIN('reference_type_value rtvContactType','rtvContactType.reference_type_value_id=l.fk_contact_type_id','LEFT');
        $this->db->where('cf.candidate_fee_id',$candidateFeeId);
        $res_query=$this->db->get();
        //return $this->db->last_query();
        //exit;
        if($res_query){
            if($res_query->num_rows()>0){
                $result=$res_query->result();
                $is_amount_paid=0;
                $is_concession_allowed=0;
                $is_concession_pending=0;
                foreach($result as $k_r=>$v_r)
                {
                    $candidate_fee_id=$v_r->candidate_fee_id;
                    $approved_status=$v_r->approved_status;
                    if($approved_status == 'Pending')
                        $v_r->approve_status=1;
                    else
                        $v_r->approve_status=0;

                    if($approved_status=='Pending' && $v_r->approval_type=='Concession')
                    {
                        $is_concession_pending=1;
                    }
                    $result[0]->is_concession_pending=$is_concession_pending;
                    //$v_r->is_concession_allowed=0;
                    $where="amount_paid > 0  AND fk_candidate_fee_id = ".$candidate_fee_id;
                    $this->db->select('candidate_fee_item_id');
                    $this->db->from('candidate_fee_item');
                    $this->db->where($where);
                    $res_query_sub=$this->db->get();
                    $num_rows=$res_query_sub->num_rows();
                    if($num_rows>0){
                        $is_amount_paid=1;
                    }
                    $result[0]->is_amount_paid=$is_amount_paid;
                    if($num_rows>0 || $v_r->approve_status==1)
                    {
                        //$v_r->is_concession_allowed=0;
                    }
                    else
                    {
                        //$v_r->is_concession_allowed=1;
                        $is_concession_allowed=1;
                    }
                    $result[0]->is_concession_allowed=$is_concession_allowed;
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$result);
            }
            else
            {
                $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array('status'=>false,'message'=>$error['message'],'data'=>array());
        }
        return $db_response;
    }

    public function getFeeConcessionList($branchId)
    {
        $table = 'candidate_fee';
        // Table's primary key
        $primaryKey = 'candidate_fee_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'l.name as lead_name',                       'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => 'bxf.lead_number',                            'dt' => 'lead_number',          'field' => 'lead_number' ),
            array( 'db' => 'b.name as branchName',                      'dt' => 'branchName',           'field' => 'branchName' ),
            array( 'db' => '(SUM(cfi.amount_payable) - SUM(cfi.concession)) as amount_payable', 'dt' => 'amount_payable',       'field' => 'amount_payable' ),
            array( 'db' => 'SUM(cfi.amount_paid) as amount_paid',       'dt' => 'amount_paid',          'field' => 'amount_paid' ),
            array( 'db' => 'cf.approved_status',                        'dt' => 'approved_status',      'field' => 'approved_status' ),
            array( 'db' => 'cf.candidate_fee_id',                       'dt' => 'candidate_fee_id',     'field' => 'candidate_fee_id' ),
            array( 'db' => 'cfi.candidate_fee_item_id',                 'dt' => 'candidate_fee_item_id','field' => 'candidate_fee_item_id' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name',                                    'dt' => 'name',             'field' => 'name' ),
            array( 'db' => 'b.name',                                    'dt' => 'b.name',           'field' => 'b.name' ),
            array( 'db' => 'bxf.lead_number',                            'dt' => 'lead_number',          'field' => 'lead_number' ),
            array( 'db' => 'cf.approved_status',                        'dt' => 'approved_status',  'field' => 'approved_status' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from candidate_fee cf
                        left join branch_xref_lead bxf on bxf.branch_xref_lead_id = cf.fk_branch_xref_lead_id
                        left join lead l on l.lead_id=bxf.lead_id
                        left join branch b on b.branch_id=bxf.branch_id
                        left join candidate_fee_item cfi on cfi.fk_candidate_fee_id=cf.candidate_fee_id
                        left join fee_structure_item fsi on fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id
                        left join fee_head fh on fh.fee_head_id=fsi.fk_fee_head_id";
        //$extraWhere = "cf.approval_type='Concession' and cf.approved_status='Pending'";//bl.branch_id=".$branchId;
        $extraWhere = "cf.approval_type='Concession' AND bxf.branch_id=".$branchId;
        $groupBy = "cf.candidate_fee_id";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        // for($i=0;$i<count($responseData['data']);$i++)
        // {
        //   $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
        //}
        return $responseData;

    }

    public function getFeeConcessionRequestList($branchId)
    {
        $table = 'candidate_fee';
        // Table's primary key
        $primaryKey = 'candidate_fee_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'l.name as lead_name',                       'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => 'bxf.lead_number',                            'dt' => 'lead_number',          'field' => 'lead_number' ),
            array( 'db' => 'b.name as branchName',                      'dt' => 'branchName',           'field' => 'branchName' ),
            array( 'db' => '(SUM(cfi.amount_payable) - SUM(cfi.concession)) as amount_payable', 'dt' => 'amount_payable',       'field' => 'amount_payable' ),
            array( 'db' => 'SUM(cfi.amount_paid) as amount_paid',       'dt' => 'amount_paid',          'field' => 'amount_paid' ),
            array( 'db' => 'cf.approved_status',                        'dt' => 'approved_status',      'field' => 'approved_status' ),
            array( 'db' => 'cf.candidate_fee_id',                       'dt' => 'candidate_fee_id',     'field' => 'candidate_fee_id' ),
            array( 'db' => 'cfi.candidate_fee_item_id',                 'dt' => 'candidate_fee_item_id','field' => 'candidate_fee_item_id' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name',                                    'dt' => 'name',             'field' => 'name' ),
            array( 'db' => 'b.name',                                    'dt' => 'b.name',           'field' => 'b.name' ),
            //array( 'db' => '(SUM(cfi.amount_payable) - SUM(cfi.concession))', 'dt' => '(SUM(cfi.amount_payable) - SUM(cfi.concession))',   'field' => '(SUM(cfi.amount_payable) - SUM(cfi.concession))' ),
            //array( 'db' => 'SUM(cfi.amount_paid)',       'dt' => 'SUM(cfi.amount_paid)',      'field' => 'SUM(cfi.amount_paid)' ),
            array( 'db' => 'cf.approved_status',                        'dt' => 'approved_status',  'field' => 'approved_status' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from candidate_fee cf
                        left join branch_xref_lead bxf on bxf.branch_xref_lead_id = cf.fk_branch_xref_lead_id
                        left join lead l on l.lead_id=bxf.lead_id
                        left join branch b on b.branch_id=bxf.branch_id
                        left join candidate_fee_item cfi on cfi.fk_candidate_fee_id=cf.candidate_fee_id
                        left join fee_structure_item fsi on fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id
                        left join fee_head fh on fh.fee_head_id=fsi.fk_fee_head_id";
        $extraWhere = "cf.approval_type='Concession' and cf.approved_status='Pending' AND bxf.branch_id=".$branchId;
        $groupBy = "cf.candidate_fee_id";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        // for($i=0;$i<count($responseData['data']);$i++)
        // {
        //   $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
        //}
        return $responseData;

    }


    function getLeadFeeRequestList()
    {
        $table = 'candidate_fee';
        // Table's primary key
        $primaryKey = 'candidate_fee_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'l.name as lead_name',                       'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => 'b.name as branchName',                      'dt' => 'branchName',           'field' => 'branchName' ),
            array( 'db' => '(SUM(cfi.amount_payable) - SUM(cfi.concession)) as amount_payable', 'dt' => 'amount_payable',       'field' => 'amount_payable' ),
            array( 'db' => 'SUM(cfi.amount_paid) as amount_paid',       'dt' => 'amount_paid',          'field' => 'amount_paid' ),
            array( 'db' => 'cf.approved_status',                        'dt' => 'approved_status',      'field' => 'approved_status' ),
            array( 'db' => 'cf.candidate_fee_id',                       'dt' => 'candidate_fee_id',     'field' => 'candidate_fee_id' ),
            array( 'db' => 'cfi.candidate_fee_item_id',                 'dt' => 'candidate_fee_item_id','field' => 'candidate_fee_item_id' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name',                                    'dt' => 'name',             'field' => 'name' ),
            array( 'db' => 'b.name',                                    'dt' => 'b.name',           'field' => 'b.name' ),
            array( 'db' => '(SUM(cfi.amount_payable) - SUM(cfi.concession)) as amount_payable', 'dt' => 'amount_payable',   'field' => 'amount_payable' ),
            array( 'db' => 'SUM(cfi.amount_paid) as amount_paid',       'dt' => 'amount_paid',      'field' => 'amount_paid' ),
            array( 'db' => 'cf.approved_status',                        'dt' => 'approved_status',  'field' => 'approved_status' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from candidate_fee cf
                        left join branch_xref_lead bxf on bxf.branch_xref_lead_id = cf.fk_branch_xref_lead_id
                        left join lead l on l.lead_id=bxf.lead_id
                        left join branch b on b.branch_id=bxf.branch_id
                        left join candidate_fee_item cfi on cfi.fk_candidate_fee_id=cf.candidate_fee_id
                        left join fee_structure_item fsi on fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id
                        left join fee_head fh on fh.fee_head_id=fsi.fk_fee_head_id";
        $extraWhere = "cf.approval_type='Concession'";
        $groupBy = "cf.candidate_fee_id";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        // for($i=0;$i<count($responseData['data']);$i++)
        // {
        //   $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
        //}
        return $responseData;
    }

    function getLeadsInformation($val='',$branchId = '')
    {
        $condition = '';
        if(isset($branchId) && $branchId!='')
        {
            $condition=' and bxl.branch_id='.$branchId;
        }
        //$query = $this->db->query("select bxl.lead_number  as id,CONCAT_WS(' | ',bxl.lead_number,l.name,l.email,l.phone) as text,bxl.fk_course_id from branch_xref_lead bxl,lead l where l.lead_id=bxl.lead_id and bxl.is_active=1 and (bxl.lead_number like '%".$val."%' or l.name like '%".$val."%' or l.email like '%".$val."%' or l.phone like '%".$val."%')".$condition." GROUP BY bxl.lead_id");
        $query = $this->db->query("select bxl.lead_number  as id,CONCAT_WS(' | ',bxl.lead_number,l.name,l.email,l.phone,c.`name`) as text,bxl.fk_course_id from branch_xref_lead bxl JOIN lead l ON bxl.lead_id=l.lead_id JOIN course c ON bxl.fk_course_id=c.course_id where l.lead_id=bxl.lead_id and bxl.is_active=1 and (bxl.lead_number like '%".$val."%' or l.name like '%".$val."%' or l.email like '%".$val."%' or l.phone like '%".$val."%')".$condition);
        //echo  $this->db->last_query();exit;
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    public function getFeeAssignModificationsList($branchId)
    {
        $table = 'candidate_fee';
        // Table's primary key
        $primaryKey = 'candidate_fee_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'l.name as lead_name',          'dt' => 'lead_name',         'field' => 'lead_name' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'b.name as branch_name',          'dt' => 'branch_name',         'field' => 'branch_name' ),
            array( 'db' => 'sum(cfi.amount_payable) as amount_payable',           'dt' => 'amount_payable',    'field' => 'amount_payable' ),
            array( 'db' => 'sum(cfi.amount_paid) as amount_paid',              'dt' => 'amount_paid',       'field' => 'amount_paid' ),
            array( 'db' => 'cf.approved_status',       'dt' => 'approved_status',       'field' => 'approved_status' ),
            array( 'db' => 'fh.name as feeHeadName',       'dt' => 'feeHeadName',       'field' => 'feeHeadName' ),
            array( 'db' => 'cf.candidate_fee_id',          'dt' => 'candidate_fee_id',  'field' => 'candidate_fee_id' ),
            array( 'db' => 'cfi.candidate_fee_item_id',          'dt' => 'candidate_fee_item_id',  'field' => 'candidate_fee_item_id' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'cfi.amount_payable',   'dt' => 'amount_payable',   'field' => 'amount_payable' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'fh.name',   'dt' => 'fh.name',   'field' => 'fh.name' )

        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from candidate_fee cf
                        left join branch_xref_lead bl on bl.branch_xref_lead_id = cf.fk_branch_xref_lead_id
                        left join lead l on l.lead_id=bl.lead_id
                        left join candidate_fee_item cfi on cfi.fk_candidate_fee_id=cf.candidate_fee_id
                        left join fee_structure_item fsi on fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id
                        left join fee_head fh on fh.fee_head_id=fsi.fk_fee_head_id
                        left join branch b on b.branch_id=bl.branch_id
                        ";
        //$extraWhere = " bl.branch_id=".$branchId;
        $extraWhere = " cf.approval_type='Fee Modification' and cf.approved_status='Pending' and bl.branch_id=".$branchId;
        $groupBy = "cf.candidate_fee_id";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        // for($i=0;$i<count($responseData['data']);$i++)
        // {
        //   $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
        //}
        return $responseData;

    }

    function getBranchVisitLeadsInformation($val='',$branchId = '',$type = '')
    {
        $condition = '';
        if(isset($branchId) && $branchId!='')
        {
            $condition=' and bxl.branch_id='.$branchId;
        }
        $query = $this->db->query("select bxl.lead_number  as id,CONCAT_WS(' | ',bxl.lead_number,l.name,l.email,l.phone,c.`name`) as text,bxl.fk_course_id from branch_xref_lead bxl JOIN lead l ON bxl.lead_id=l.lead_id JOIN course c ON bxl.fk_course_id=c.course_id where l.lead_id=bxl.lead_id and bxl.is_active=1 and (bxl.lead_number like '%".$val."%' or l.name like '%".$val."%' or l.email like '%".$val."%' or l.phone like '%".$val."%')".$condition);
        if($query->num_rows()>0)
        {
            $result = $data=$query->result_array();
            if($type == 'contact'){
                $result = [];
                foreach($data as $k=>$v){
                    $result[$v['id']] = $v;
                }
                if(isset($branchId) && $branchId!='')
                {
                    $condition=' and l.fk_branch_id='.$branchId;
                }
                $query = $this->db->query("select l.lead_id as id,CONCAT_WS(' | ',l.lead_id,l.name,l.email,l.phone) as text, '' as fk_course_id
             from lead l
             where (l.lead_id like '%".$val."%' or l.name like '%".$val."%' or l.email like '%".$val."%' or l.phone like '%".$val."%')".$condition);
                if($query->num_rows()>0)
                {
                    $data=$query->result_array();
                    foreach($data as $k=>$v){
                        if(!isset($result[$v['id']]))
                            $result[$v['id']] = $v;
                    }
                }
                $result = array_values($result);
            }
            $db_response=array('status'=>true,'message'=>'success','data'=>$result);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
}