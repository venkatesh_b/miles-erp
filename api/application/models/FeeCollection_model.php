<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 28/3/16
 * Time: 2:17 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FeeCollection_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    public function checkBranchLeadInfo($branchId,$leadNumber)
    {
        $this->db->select("count(*) as records,bxl.branch_xref_lead_id");
        $this->db->from('branch_xref_lead bxl');
        $this->db->where("bxl.lead_number",$leadNumber);
        $this->db->where("bxl.branch_id",$branchId);
        $query = $this->db->get();
        $LeadInfo=$query->result();
        return $LeadInfo[0]->records;
    }
    public function checkLeadFeeCollection($branchId,$leadNumber)
    {
        $this->db->select("count(*) as records,bxl.branch_xref_lead_id");
        $this->db->from('branch_xref_lead bxl');
        $this->db->join('candidate_fee cf','bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id');
        $this->db->where("bxl.lead_number",$leadNumber);
        $this->db->where("bxl.branch_id",$branchId);
        $this->db->where("cf.is_regular_fee",1);
        $query = $this->db->get();
        $LeadInfo=$query->result();
        return $LeadInfo[0]->records;
    }

    public function getLeadFeeInfoByBranchIdLeadNumber($branchId,$leadNumber,$leadCourseId)
    {
        $current_date=date('Y-m-d');
        $this->db->select("bxl.branch_xref_lead_id,bxl.lead_number,l.`name`,l.email,l.phone,c.course_id,c.`name` as courseName,bxl.fk_batch_id,b.batch_id,CONCAT_WS('-',br.code,c.name,'M7',b.code) as code,b.alias_name,cf.candidate_fee_id,SUM(cfi.amount_payable) as amount_payable,SUM(cfi.amount_paid) as amount_paid,SUM(cfi.concession) as concession_amount,SUM(cfi.refunded_other_amount) as refunded_amount");
        $this->db->from('branch_xref_lead bxl');
        $this->db->join('lead l','bxl.lead_id=l.lead_id');
        $this->db->join('course c','bxl.fk_course_id=c.course_id','left');
        $this->db->join('branch br','br.branch_id=bxl.branch_id');
        $this->db->join('batch b','bxl.fk_batch_id=b.batch_id','left');
        $this->db->join('candidate_fee cf','bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id','left');
        $this->db->join('candidate_fee_item cfi','cf.candidate_fee_id=cfi.fk_candidate_fee_id','left');
        $this->db->where("bxl.branch_id",$branchId);
        $this->db->where("bxl.lead_number",$leadNumber);
        $this->db->where("bxl.fk_course_id",$leadCourseId);
        $this->db->where("cfi.amount_payable>0");
        $this->db->where("bxl.is_active",1);
        $this->db->where("l.`status`",1);
        $this->db->where("cf.`is_regular_fee`",1);
        $this->db->group_by('bxl.fk_course_id');
        $this->db->order_by('bxl.branch_xref_lead_id','desc');
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows()>0)
            {
                $Lead_feeInfo_data=$query->result();
                $LeadFeeInfo['LeadInfo']=$Lead_feeInfo_data;
                $lead_flag=0;
                for($cf=0;$cf<count($Lead_feeInfo_data);$cf++)
                {
                    $candidate_fee_id=$Lead_feeInfo_data[$cf]->candidate_fee_id;
                    $candidate_course_id=$Lead_feeInfo_data[$cf]->course_id;
                    $candidate_course_name=$Lead_feeInfo_data[$cf]->courseName;
                    $branch_xref_lead_id=$Lead_feeInfo_data[$cf]->branch_xref_lead_id;
                    $this->db->select("batch_id,CONCAT_WS('-',ba.code,c.name,'M7',b.code) as code,alias_name");
                    $this->db->from('batch b');
                    $this->db->join('branch ba','ba.branch_id=b.fk_branch_id');
                    $this->db->join('course c','c.course_id=b.fk_course_id');
                    $this->db->where("fk_branch_id",$branchId);
                    $this->db->where("fk_course_id",$candidate_course_id);
                    $this->db->where("DATE(marketing_enddate) >=",$current_date);
                    $Batchesquery = $this->db->get();
                    if ($Batchesquery->num_rows() > 0 || $Lead_feeInfo_data[$cf]->fk_batch_id!='')
                    {
                        $Batches=$Batchesquery->result();
                        $this->db->select("cfi.candidate_fee_item_id,DATE_FORMAT(cfi.created_on,'%d %b,%Y') as FeeItem_Date,fh.`fee_head_id` as feeHeadId,fh.`name` as feeHead,fc.company_id As CompanyId,fc.`name` AS feeCompany,cfi.amount_payable as Payable,(cfi.amount_paid-cfi.refunded_other_amount) as Paid,cfi.concession as Concession,(`cfi`.`amount_payable` - cfi.concession -`cfi`.`amount_paid`+cfi.refunded_other_amount) as balance,if(fs.is_partial_payment_allowed=0,0,1) as is_partial_payment_allowed,'".$candidate_course_name."' as courseName,'".$candidate_course_id."' as CourseId,'".$branch_xref_lead_id."' as branchXrefLeadId");
                        $this->db->from('candidate_fee_item cfi');
                        $this->db->join('fee_structure_item fsi','cfi.fk_fee_structure_item_id=fsi.fee_structure_item_id','left');
                        $this->db->join('fee_structure fs','fs.fee_structure_id=fsi.fk_fee_structure_id','left');
                        $this->db->join('fee_head fh','fsi.fk_fee_head_id=fh.fee_head_id','left');
                        $this->db->join('company fc','fh.fk_company_id=fc.company_id','left');
                        $this->db->where("cfi.fk_candidate_fee_id",$candidate_fee_id);
                        $FeeItemsquery = $this->db->get();
                        $FeeItems=$FeeItemsquery->result();
                        $LeadFeeInfo['FeeItems'][$cf]=$FeeItems;
                        $LeadFeeInfo['Batches'][$cf]=$Batches;
                    }
                    else
                    {
                        $lead_flag=1;
                        break;
                    }
                }
                if($lead_flag ==0)
                {
                    $db_response=array("status"=>true,"message"=>'success',"data"=>$LeadFeeInfo);
                }
                else
                {
                    $db_response=array("status"=>false,"message"=>'No running batches found to collect fee',"data"=>array());
                }
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Lead number not found',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }

    public function addLeadFeeCollectionItem($LeadFeeCollectionData)
    {
        $current_datetime=date('Y-m-d H:i:s');
        /*$branchID=$LeadFeeCollectionData['branchID'];
        $courseID=$LeadFeeCollectionData['courseId'];
        $lead_batchID=null;
        $lead_batch_query = $this->db->query('SELECT branch_xref_lead_id FROM branch_xref_lead WHERE fk_batch_id='.$LeadFeeCollectionData['batchID'].' AND branch_xref_lead_id='.$LeadFeeCollectionData['branch_xref_lead_id']);
        if($lead_batch_query->num_rows()==0)
        {
            $batch_query = $this->db->query('SELECT batch_id FROM batch WHERE fk_course_id='.$courseID.' AND batch_id='.$LeadFeeCollectionData['batchID'].' AND fk_branch_id='.$LeadFeeCollectionData['branchID']);
            if($batch_query->num_rows()>0)
            {
                $batch_data = $batch_query->row_array();
                $lead_batchID=$batch_data['batch_id'];
            }
        }
        else
        {
            $lead_batchID=$LeadFeeCollectionData['batchID'];
        }
        echo 'BATch--'.$LeadFeeCollectionData['batchID'];
        echo $lead_batchID;
        exit;
        if($lead_batchID==null)
        {
            return arra y("status"=>false,"message"=>'No running batches found for this course',"data"=>[]);
        }
        else*/
        {
            //$LeadFeeCollectionData['batchID']=$lead_batchID;
            $manualReceipt = $LeadFeeCollectionData['manualReceipt'];
            $paymentRemarks = $LeadFeeCollectionData['PaymentRemarks'];
            if( isset($LeadFeeCollectionData['feePaidDate'] ) && ($LeadFeeCollectionData['feePaidDate']!='') )
            {
                $feePaidDate = DateTime::createFromFormat('d M, Y',$LeadFeeCollectionData['feePaidDate']);
                $feePaidDate = $feePaidDate->format("Y-m-d");
            }
            else
            {
                $feePaidDate='';
            }
            $ChequeDDDated = '';
            if(isset($LeadFeeCollectionData['ChequeDDDated'])  && ($LeadFeeCollectionData['ChequeDDDated']!='') )
            {
                $ChequeDDDated = DateTime::createFromFormat('d M, Y',$LeadFeeCollectionData['ChequeDDDated']);
                $ChequeDDDated=$ChequeDDDated->format("Y-m-d");
            }
            //Generate Random Receipt Number
            $feeCollectionId_query = $this->db->query('SELECT max(fee_collection_id) as collectionId FROM fee_collection');
            $feeCollectionId_data = $feeCollectionId_query->row_array();
            $feeCollectionId=$feeCollectionId_data['collectionId']+1;

            $branch_query = $this->db->query('SELECT branch_id,name,code FROM branch where branch_id='.$LeadFeeCollectionData['branchID']);
            $branch_data = $branch_query->row_array();
            $branchCode=$branch_data['code'];

            $company_query = $this->db->query('SELECT `name`,`code` FROM company WHERE company_id='.$LeadFeeCollectionData['feecompanyId']);
            $company_data = $company_query->row_array();
            $companyCode=$company_data['code'];

            $lead_stage_query = $this->db->query('SELECT lead_stage_id,lead_stage,lead_stage_description FROM lead_stage WHERE lead_stage="M7"');
            $lead_stage_data = $lead_stage_query->row_array();
            $lead_stage_id=$lead_stage_data['lead_stage_id'];

            $lead_data_query = $this->db->query('SELECT branch_xref_lead_id,lead_id,`status`,fk_batch_id FROM branch_xref_lead WHERE branch_xref_lead_id='.$LeadFeeCollectionData['branch_xref_lead_id']);
            $lead_details = $lead_data_query->row_array();
            $current_lead_stage_id=$lead_details['status'];
            $current_lead_batch_id=$lead_details['fk_batch_id'];
            $lead_id=$lead_details['lead_id'];
            $receiptNumber=date('Y').$branchCode.$companyCode.str_pad($feeCollectionId, 6, '0', STR_PAD_LEFT);
            //End of Receipt Generation
            $feeCollectionData = array(
                'receipt_number' => $receiptNumber,
                'receipt_date' => (($feePaidDate!=null) ? $feePaidDate : $current_datetime),
                'manual_receipt' => $manualReceipt,
                'created_date' => $current_datetime,
                'payment_type' => $LeadFeeCollectionData['paymentType'],
                'payment_mode' => $LeadFeeCollectionData['paymentMode'],
                'credit_card_number' => (isset($LeadFeeCollectionData['cardNumber']) ? $LeadFeeCollectionData['cardNumber'] : null),
                'fk_cheque_issued_bank_id' => (isset($LeadFeeCollectionData['IssueBankId']) ? $LeadFeeCollectionData['IssueBankId'] : null),
                'cheque_number' => (isset($LeadFeeCollectionData['IssueChequeDDNumber']) ? $LeadFeeCollectionData['IssueChequeDDNumber'] : null),
                'cheque_date' => $ChequeDDDated,
                'payment_remarks' => $paymentRemarks,
                'payment_status' => 'Success',
                'fk_branch_xref_lead_id' => $LeadFeeCollectionData['branch_xref_lead_id'],
                'amount_paid' => $LeadFeeCollectionData['payingAmount'],
                'fk_fee_company_id' => $LeadFeeCollectionData['feecompanyId'],
                'created_by' => $LeadFeeCollectionData['user'],
                'created_on' => $current_datetime
            );

            $this->db->insert('fee_collection', $feeCollectionData);
            $feeCollectionId = $this->db->insert_id();

            $feeCollectionItemData = array(
                'fk_fee_collection_id' => $feeCollectionId,
                'fk_fee_head_id' => $LeadFeeCollectionData['feeHeadId'],
                'fk_candidate_fee_item_id' => $LeadFeeCollectionData['candidate_fee_item_id'],
                'amount' => $LeadFeeCollectionData['feeItem_Amount'],
                'created_by' => $LeadFeeCollectionData['user'],
                'created_on' => $current_datetime
            );
            $this->db->insert('fee_collection_item', $feeCollectionItemData);

            $updateQuery="update candidate_fee_item set amount_paid= amount_paid+".$LeadFeeCollectionData['payingAmount']." where candidate_fee_item_id=". $LeadFeeCollectionData['candidate_fee_item_id'];
            $this->db->query($updateQuery);

            if($current_lead_batch_id != $LeadFeeCollectionData['batchID'])
            {
                if($current_lead_batch_id == '')
                {
                    $this->db->select('batch_id');
                    $this->db->from('batch');
                    $this->db->where('batch_id',$LeadFeeCollectionData['batchID']);
                    $this->db->where('fk_course_id',$LeadFeeCollectionData['courseId']);
                    $batch_cr_qry=$this->db->get();
                    if($batch_cr_qry->num_rows()==0)
                    {
                        $this->db->select('batch_id');
                        $this->db->from('batch');
                        $this->db->where('DATE(marketing_enddate) >=',$current_datetime);
                        $this->db->where('fk_course_id',$LeadFeeCollectionData['courseId']);
                        $this->db->order_by("batch_id","desc");
                        $batch_id_qry=$this->db->get();
                        $batch_data=$batch_id_qry->result();
                    }
                    else
                    {
                        $batch_data=$batch_cr_qry->result();
                    }
                    $lead_batch_id=$batch_data[0]->batch_id;
                    $updateQuery="UPDATE branch_xref_lead SET `fk_batch_id`=".$lead_batch_id.",updated_by=".$LeadFeeCollectionData['user'].",updated_on='".$current_datetime."' WHERE branch_xref_lead_id=".$LeadFeeCollectionData['branch_xref_lead_id'];
                    $this->db->query($updateQuery);
                }
                else
                {
                    $updateQuery="UPDATE branch_xref_lead SET `fk_batch_id`=".$LeadFeeCollectionData['batchID'].",updated_by=".$LeadFeeCollectionData['user'].",updated_on='".$current_datetime."' WHERE branch_xref_lead_id=".$LeadFeeCollectionData['branch_xref_lead_id'];
                    $this->db->query($updateQuery);
                }
            }
            if($current_lead_stage_id != $lead_stage_id)
            {
                $updateQuery="UPDATE branch_xref_lead SET `status`=".$lead_stage_id.",updated_by=".$LeadFeeCollectionData['user'].",updated_on='".$current_datetime."',student_last_followup_date = '".$current_datetime."' WHERE branch_xref_lead_id=".$LeadFeeCollectionData['branch_xref_lead_id'];
                $this->db->query($updateQuery);
                $query = $this->db->query("CALL SP_Add_TimeLine({$lead_id},{$LeadFeeCollectionData['branch_xref_lead_id']},'Lead Status','Lead Successfully converted to student ',{$LeadFeeCollectionData['user']})");
            }
            return $this->feeCollectionDetails($feeCollectionId);
        }
    }

    public function feeCollectionDetails($feeCollectionId)
    {
        $this->db->select("fc.fee_collection_id,bxl.lead_number,IF(isnull(bt.code),'----',CONCAT_WS('-',b.code,c.name,'M7',bt.code)) as batchInfo,bt.alias_name,b.`name` as branchName,c.`name` as courseName,fc.receipt_number,l.`name` as studentName,fc.payment_type,fc.payment_mode,fh.`name` as feeHead,fc.amount_paid,fcp.`name` as companyName,rtv.`value` as cityName,fc.payment_status");
        $this->db->select("DATE_FORMAT(fc.receipt_date, '%d %b,%Y') AS receipt_date", FALSE);
        $this->db->select("DATE_FORMAT(fc.receipt_date, '%d %b,%Y %H:%i:%S') AS receipt_date_time", FALSE);
        $this->db->from('fee_collection fc');
        $this->db->join('branch_xref_lead bxl','fc.fk_branch_xref_lead_id=bxl.branch_xref_lead_id','left');
        $this->db->join('branch b','bxl.branch_id=b.branch_id','left');
        $this->db->join('batch bt','bxl.fk_batch_id=bt.batch_id','left');
        $this->db->join('course c','bxl.fk_course_id=c.course_id','left');
        $this->db->join('lead l','bxl.lead_id=l.lead_id','left');
        $this->db->join('fee_collection_item fci','fc.fee_collection_id=fci.fk_fee_collection_id','left');
        $this->db->join('fee_head fh','fh.fee_head_id=fci.fk_fee_head_id','left');
        $this->db->join('company fcp','fcp.company_id=fh.fk_company_id','left');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=b.fk_city_id','left');
        $this->db->where("fc.fee_collection_id",$feeCollectionId);
        $FeeCollectionquery = $this->db->get();

        if($FeeCollectionquery)
        {
            $FeeCollection=$FeeCollectionquery->result();
            $db_response=array("status"=>true,"message"=>'success',"data"=>$FeeCollection);
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }

    public function cancelFeeCollectionReceipt($feeCollectionId,$user_id)
    {
        $current_datetime=date('Y-m-d H:i:s');
        //$updateFeeStatusQuery="UPDATE fee_collection SET `payment_status`='Cancelled', cancelled_date='".$current_datetime."', updated_by=".$user_id.", updated_on='".$current_datetime."' WHERE fee_collection_id=".$feeCollectionId;
        $updateFeeStatusQuery="UPDATE fee_collection SET `payment_status`='Cancelled', cancelled_by='".$user_id."',cancelled_date='".$current_datetime."', updated_by=".$user_id.", updated_on='".$current_datetime."' WHERE fee_collection_id=".$feeCollectionId;
        $this->db->query($updateFeeStatusQuery);
        $updateFeeAmountQuery="UPDATE candidate_fee_item cfi JOIN fee_collection_item fci ON cfi.candidate_fee_item_id=fci.fk_candidate_fee_item_id JOIN fee_collection fc ON fci.fk_fee_collection_id=fc.fee_collection_id SET cfi.amount_paid=cfi.amount_paid-fc.amount_paid WHERE fc.fee_collection_id=".$feeCollectionId;
        $this->db->query($updateFeeAmountQuery);
        $feeCollectionAuditData = array(
            'fk_fee_collection_id' => $feeCollectionId,
            'fk_created_by' => $user_id,
            'created_date' => $current_datetime,
        );
        $this->db->insert('fee_collection_audit', $feeCollectionAuditData);
        $status=($this->db->affected_rows() != 1) ? false : true;
        $message=($this->db->affected_rows() != 1) ? null : 'success';
        return $db_response=array("status"=>$status,"message"=>$message,"data"=>null);
    }

    public function feeCollectionList($branchId)
    {
        $table = 'fee_collection';
        // Table's primary key
        $primaryKey = 'fc`.`fee_collection_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes

        $columns = array(
            array( 'db' => '`fc`.`fee_collection_id`',              'dt' => 'fee_collection_id',        'field' => 'fee_collection_id' ),
            array( 'db' => '`cmp`.`name` as Company',              'dt' => 'Company',                  'field' => 'Company' ),
            array( 'db' => '`bxl`.`lead_number`',                   'dt' => 'lead_number',              'field' => 'lead_number' ),
            array( 'db' => '`l`.`name` as studentName',             'dt' => 'studentName',              'field' => 'studentName' ),
            array( 'db' => '`fc`.`receipt_date`',                   'dt' => 'receipt_date',             'field' => 'receipt_date',        'formatter' => function( $d, $row ){    return date( 'd M,Y', strtotime($d)); }  ),
            array( 'db' => '`fc`.`payment_status`',                 'dt' => 'payment_status',           'field' => 'payment_status' ),
            array( 'db' => '`e`.`name` as createdBy',               'dt' => 'createdBy',                'field' => 'createdBy' ),
            array( 'db' => '`fc`.`created_on`',                     'dt' => 'created_on',               'field' => 'created_on',        'formatter' => function( $d, $row ){    return date( 'd M,Y', strtotime($d)); } ),
            array( 'db' => 'ABS(`fc`.`amount_paid`) as amount_paid',                    'dt' => 'amount_paid',              'field' => 'amount_paid' ),
            array( 'db' => '`fc`.`receipt_number`',                 'dt' => 'receipt_number',           'field' => 'receipt_number' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`fc`.`receipt_number`',                 'dt' => 'receipt_number',           'field' => 'receipt_number' ),
            array( 'db' => '`cmp`.`name`',                         'dt' => 'Company',                  'field' => 'Company' ),
            array( 'db' => '`bxl`.`lead_number`',                   'dt' => 'lead_number',              'field' => 'lead_number' ),
            array( 'db' => '`l`.`name`',                            'dt' => 'studentName',              'field' => 'studentName' ),
            array( 'db' => '`fc`.`receipt_date`',                   'dt' => 'receipt_date',             'field' => 'receipt_date' ),
            array( 'db' => '`fc`.`payment_status`',                 'dt' => 'payment_status',           'field' => 'payment_status' ),
            array( 'db' => '`e`.`name`',                            'dt' => 'createdBy',                'field' => 'createdBy' ),
            array( 'db' => '`fc`.`created_on`',                     'dt' => 'created_on',               'field' => 'created_on' ),
            array( 'db' => '`fc`.`amount_paid`',                    'dt' => 'amount_paid',              'field' => 'amount_paid' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM fee_collection fc JOIN company cmp ON fc.fk_fee_company_id=cmp.company_id JOIN branch_xref_lead bxl ON fc.fk_branch_xref_lead_id=bxl.branch_xref_lead_id JOIN lead l ON bxl.lead_id=l.lead_id JOIN employee e ON fc.created_by=e.user_id ";
        $extraWhere = " bxl.branch_id=".$branchId;
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        /*for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['reference_type_value_id']=encode($responseData['data'][$i]['reference_type_value_id']);
        }*/
        return $responseData;
    }
    public function getLeadFeeInfoDetailsUnassignedFee($branchId,$leadNumber,$leadCourseId)
    {
        $current_date=date('Y-m-d');
        $this->db->select("bxl.branch_xref_lead_id,bxl.lead_number,l.`name`,l.email,l.phone,c.course_id,c.`name` as courseName,bxl.fk_batch_id,b.batch_id,CONCAT_WS('-',br.code,c.name,'M7',b.code) as code,l.fk_contact_type_id,l.fk_institution_id,l.fk_company_id,rtv.value as contactType,br.branch_id");
        $this->db->from('branch_xref_lead bxl');
        $this->db->join('lead l','bxl.lead_id=l.lead_id');
        $this->db->join('course c','bxl.fk_course_id=c.course_id','left');
        $this->db->join('branch br','br.branch_id=bxl.branch_id');
        $this->db->join('batch b','bxl.fk_batch_id=b.batch_id','left');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=l.fk_contact_type_id','left');
        $this->db->where("bxl.branch_id",$branchId);
        $this->db->where("bxl.lead_number",$leadNumber);
        $this->db->where("bxl.fk_course_id",$leadCourseId);
        $this->db->where("bxl.is_active",1);
        $this->db->where("l.`status`",1);
        $this->db->order_by("bxl.`branch_xref_lead_id`","desc");
        $this->db->limit(1);
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows()>0)
            {
                $LeadInfo=$query->result();
                $contactType=$LeadInfo[0]->contactType;
                $companyId='';
                $instiutionId='';
                $fk_branch_id=$LeadInfo[0]->branch_id;
                $fk_course_id=$LeadInfo[0]->course_id;
                if($contactType=='Corporate')
                {
                    $companyId=$LeadInfo[0]->fk_company_id;
                }
                if($contactType=='University')
                {
                    $instiutionId=$LeadInfo[0]->fk_institution_id;
                }
                $query="select fee_structure_id,name from fee_structure where is_active=1 and is_regular_fee=0 and (fk_branch_id is null or fk_branch_id='$fk_branch_id') and (fk_corporate_company_id is null or fk_corporate_company_id='$companyId') and (fk_institution_id is null or fk_institution_id='$instiutionId') and fk_course_id='$fk_course_id'";
                $feestructuresRes = $this->db->query($query);

                $FeeItems=$feestructuresRes->result();
                $LeadFeeInfo['LeadInfo']=$LeadInfo;
                $LeadFeeInfo['FeeStructures']=$FeeItems;
                $LeadFeeInfo['Batches']=array();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$LeadFeeInfo);

            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Lead number not found',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getLeadFeeInfoByBranchIdLeadNumberUnassignedFee($branchId,$leadNumber,$feeStructureType='')
    {
        $current_date=date('Y-m-d');
        $this->db->select("count(*) as records,bxl.branch_xref_lead_id,bxl.lead_number,l.`name`,l.email,l.phone,c.course_id,c.`name` as courseName,bxl.fk_batch_id,b.batch_id,CONCAT_WS('-',br.code,c.name,'M7',b.code) as code");
        $this->db->from('branch_xref_lead bxl');
        $this->db->join('lead l','bxl.lead_id=l.lead_id');
        $this->db->join('course c','bxl.fk_course_id=c.course_id','left');
        $this->db->join('branch br','br.branch_id=bxl.branch_id');
        $this->db->join('batch b','bxl.fk_batch_id=b.batch_id','left');
        $this->db->where("bxl.branch_id",$branchId);
        $this->db->where("bxl.lead_number",$leadNumber);
        $this->db->where("bxl.is_active",1);
        $this->db->where("l.`status`",1);
        $query = $this->db->get();
        $is_candidate_fee_item=0;
        if($query)
        {
            $LeadInfo=$query->result();
            if ($LeadInfo[0]->records > 0)
            {
                $candidate_course_id=$LeadInfo[0]->course_id;
                $this->db->select('candidate_fee_id');
                $this->db->from('candidate_fee');
                $this->db->where('fk_branch_xref_lead_id',$LeadInfo[0]->branch_xref_lead_id);
                $this->db->where('fk_fee_structure_id',$feeStructureType);
                $res=$this->db->get();
                if($res && $res->num_rows()>0)
                {
                    $is_candidate_fee_item=1;
                    $rows=$res->result();
                    $candidate_fee_id=$rows[0]->candidate_fee_id;
                    $branch_xref_lead_id=$rows[0]->fk_branch_xref_lead_id;
                    $LeadInfo[0]->candidate_fee_id=$candidate_fee_id;
                    $this->db->select("cfi.candidate_fee_item_id,DATE_FORMAT(cfi.created_on,'%d %b,%Y') as FeeItem_Date,fh.`fee_head_id` as feeHeadId,fh.`name` as feeHead,fc.company_id As CompanyId,fc.`name` AS feeCompany,cfi.amount_payable as Payable,cfi.amount_paid as Paid,cfi.concession as Concession,(`cfi`.`amount_payable` - cfi.concession -`cfi`.`amount_paid`) as balance,if(fs.is_partial_payment_allowed=0,0,1) as is_partial_payment_allowed,'".$branch_xref_lead_id."' as branchXrefLeadId");
                    $this->db->from('candidate_fee_item cfi');
                    $this->db->join('fee_structure_item fsi','cfi.fk_fee_structure_item_id=fsi.fee_structure_item_id','left');
                    $this->db->join('fee_structure fs','fs.fee_structure_id=fsi.fk_fee_structure_id');
                    $this->db->join('fee_head fh','fsi.fk_fee_head_id=fh.fee_head_id','left');
                    $this->db->join('company fc','fh.fk_company_id=fc.company_id','left');
                    $this->db->where("cfi.fk_candidate_fee_id",$candidate_fee_id);
                }
                else
                {
                    $LeadInfo[0]->candidate_fee_id=$feeStructureType;
                    $this->db->select("fsi.fee_structure_item_id as candidate_fee_item_id,DATE_FORMAT(fs.created_date,'%d %b,%Y') as FeeItem_Date,fh.`fee_head_id` as feeHeadId,fh.`name` as feeHead,fc.company_id As CompanyId,fc.`name` AS feeCompany,fsi.amount as Payable,0 as Paid,0 as Concession,fsi.amount as balance,if(fs.is_partial_payment_allowed=0,0,1) as is_partial_payment_allowed");
                    $this->db->from('fee_structure fs');
                    $this->db->join('fee_structure_item fsi','fs.fee_structure_id=fsi.fk_fee_structure_id','left');
                    $this->db->join('fee_head fh','fsi.fk_fee_head_id=fh.fee_head_id','left');
                    $this->db->join('company fc','fh.fk_company_id=fc.company_id','left');
                    $this->db->where("fs.fee_structure_id",$feeStructureType);
                }
                $FeeItemsquery = $this->db->get();
                $FeeItems=$FeeItemsquery->result();
                foreach($FeeItems as $key =>$val){
                    $val->is_candidate_fee_item=$is_candidate_fee_item;
                    $val->feeStructureType=$feeStructureType;
                }
                $LeadFeeInfo['LeadInfo']=$LeadInfo;
                $LeadFeeInfo['FeeItems']=$FeeItems;

                $db_response=array("status"=>true,"message"=>'success',"data"=>$LeadFeeInfo);


            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Lead number not found',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function addLeadFeeCollectionUnassignedFeeItem($LeadFeeCollectionData)
    {
        $current_datetime=date('Y-m-d H:i:s');
        if($LeadFeeCollectionData['is_candidate_fee_item_id']==1)
        {

        }
        else
        {
            $this->db->select('cf.candidate_fee_id,cfi.candidate_fee_item_id');
            $this->db->from('candidate_fee cf');
            $this->db->join('candidate_fee_item cfi','cfi.fk_candidate_fee_id=cf.candidate_fee_id');
            $this->db->where('cf.fk_branch_xref_lead_id',$LeadFeeCollectionData['branch_xref_lead_id']);
            $this->db->where('cf.fk_fee_structure_id',$LeadFeeCollectionData['feeStructureId']);
            $this->db->where('cfi.fk_fee_structure_item_id',$LeadFeeCollectionData['candidate_fee_item_id']);
            $r=$this->db->get();
            if($r && $r->num_rows()>0){
                $w=$r->result();
                $LeadFeeCollectionData['candidate_fee_id']=$w[0]->candidate_fee_id;
                $LeadFeeCollectionData['candidate_fee_item_id']=$w[0]->candidate_fee_item_id;
            }
            else
            {
                $data_insert_candidate_fee=array();
                $data_insert_candidate_fee['fk_branch_xref_lead_id']=$LeadFeeCollectionData['branch_xref_lead_id'];
                $data_insert_candidate_fee['fk_fee_structure_id']=$LeadFeeCollectionData['feeStructureId'];
                $data_insert_candidate_fee['assigned_by']=$_SERVER['HTTP_USER'];
                $data_insert_candidate_fee['assigned_on']=$current_datetime;
                $data_insert_candidate_fee['is_regular_fee']=0;
                $insert_candidate_fee_res=$this->db->insert('candidate_fee',$data_insert_candidate_fee);
                if($insert_candidate_fee_res){
                    $LeadFeeCollectionData['candidate_fee_id']=$this->db->insert_id();
                    $this->db->select('fsi.fee_structure_item_id,fsi.amount');
                    $this->db->from('fee_structure_item fsi');
                    $this->db->where('fsi.fk_fee_structure_id',$LeadFeeCollectionData['feeStructureId']);
                    $r_fees=$this->db->get();
                    if($r_fees && $r_fees->num_rows()>0)
                    {
                        $row_fees=$r_fees->result();
                        foreach($row_fees as $k_f=>$v_f)
                        {
                            $data_insert_candidate_fee_item = array();
                            $data_insert_candidate_fee_item['fk_candidate_fee_id'] = $LeadFeeCollectionData['candidate_fee_id'];
                            $data_insert_candidate_fee_item['fk_fee_structure_item_id'] = $v_f->fee_structure_item_id;
                            $data_insert_candidate_fee_item['amount_payable'] = $v_f->amount;
                            $data_insert_candidate_fee_item['amount_paid'] = 0;
                            $data_insert_candidate_fee_item['created_by'] = $_SERVER['HTTP_USER'];
                            $data_insert_candidate_fee_item['created_on'] = $current_datetime;
                            $insert_candidate_fee_item_res=$this->db->insert('candidate_fee_item',$data_insert_candidate_fee_item);
                            if($insert_candidate_fee_item_res && $v_f->fee_structure_item_id==$LeadFeeCollectionData['candidate_fee_item_id'])
                            {
                                $LeadFeeCollectionData['candidate_fee_item_id']=$this->db->insert_id();
                            }
                        }
                    }

                }

            }
        }
        if( isset($LeadFeeCollectionData['feePaidDate'] ) && ($LeadFeeCollectionData['feePaidDate']!='') )
        {
            $feePaidDate = DateTime::createFromFormat('d M, Y',$LeadFeeCollectionData['feePaidDate']);
            $feePaidDate = $feePaidDate->format("Y-m-d");
        }
        else
        {
            $feePaidDate='';
        }
        $ChequeDDDated = '';
        if(isset($LeadFeeCollectionData['ChequeDDDated'])  && ($LeadFeeCollectionData['ChequeDDDated']!='') )
        {
            $ChequeDDDated = DateTime::createFromFormat('d M, Y',$LeadFeeCollectionData['ChequeDDDated']);
            $ChequeDDDated=$ChequeDDDated->format("Y-m-d");
        }
        //Generate Random Receipt Number
        $feeCollectionId_query = $this->db->query('SELECT max(fee_collection_id) as collectionId FROM fee_collection');
        $feeCollectionId_data = $feeCollectionId_query->row_array();
        $feeCollectionId=$feeCollectionId_data['collectionId']+1;

        $branch_query = $this->db->query('SELECT branch_id,name,code FROM branch where branch_id='.$LeadFeeCollectionData['branchID']);
        $branch_data = $branch_query->row_array();
        $branchCode=$branch_data['code'];

        $company_query = $this->db->query('SELECT `name`,`code` FROM company WHERE company_id='.$LeadFeeCollectionData['feecompanyId']);
        $company_data = $company_query->row_array();
        $companyCode=$company_data['code'];

        $lead_stage_query = $this->db->query('SELECT lead_stage_id,lead_stage,lead_stage_description FROM lead_stage WHERE lead_stage="M7"');
        $lead_stage_data = $lead_stage_query->row_array();
        $lead_stage_id=$lead_stage_data['lead_stage_id'];

        $lead_data_query = $this->db->query('SELECT branch_xref_lead_id,lead_id,`status`,fk_batch_id FROM branch_xref_lead WHERE branch_xref_lead_id='.$LeadFeeCollectionData['branch_xref_lead_id']);
        $lead_details = $lead_data_query->row_array();
        $current_lead_stage_id=$lead_details['status'];
        $current_lead_batch_id=$lead_details['fk_batch_id'];
        $lead_id=$lead_details['lead_id'];

        //$receiptNumber=$branchCode.'/'.str_pad($feeCollectionId, 6, '0', STR_PAD_LEFT).date('/dmY');
        $receiptNumber=date('Y').$branchCode.$companyCode.str_pad($feeCollectionId, 6, '0', STR_PAD_LEFT);
        $manualReceipt = $LeadFeeCollectionData['manualReceipt'];
        $paymentRemarks = $LeadFeeCollectionData['PaymentRemarks'];
        //End of Receipt Generation
        $feeCollectionData = array(
            'receipt_number' => $receiptNumber,
            'receipt_date' => (($feePaidDate!=null) ? $feePaidDate : $current_datetime),
            'manual_receipt' => $manualReceipt,
            'created_date' => $current_datetime,
            'payment_type' => $LeadFeeCollectionData['paymentType'],
            'payment_mode' => $LeadFeeCollectionData['paymentMode'],
            'credit_card_number' => (isset($LeadFeeCollectionData['cardNumber']) ? $LeadFeeCollectionData['cardNumber'] : null),
            'fk_cheque_issued_bank_id' => (isset($LeadFeeCollectionData['IssueBankId']) ? $LeadFeeCollectionData['IssueBankId'] : null),
            'cheque_number' => (isset($LeadFeeCollectionData['IssueChequeDDNumber']) ? $LeadFeeCollectionData['IssueChequeDDNumber'] : null),
            'cheque_date' => $ChequeDDDated,
            'payment_status' => 'Success',
            'payment_remarks' => $paymentRemarks,
            'fk_branch_xref_lead_id' => $LeadFeeCollectionData['branch_xref_lead_id'],
            'amount_paid' => $LeadFeeCollectionData['payingAmount'],
            'fk_fee_company_id' => $LeadFeeCollectionData['feecompanyId'],
            'created_by' => $LeadFeeCollectionData['user'],
            'created_on' => $current_datetime
        );

        $this->db->insert('fee_collection', $feeCollectionData);
        $feeCollectionId = $this->db->insert_id();

        $feeCollectionItemData = array(
            'fk_fee_collection_id' => $feeCollectionId,
            'fk_fee_head_id' => $LeadFeeCollectionData['feeHeadId'],
            'fk_candidate_fee_item_id' => $LeadFeeCollectionData['candidate_fee_item_id'],
            'amount' => $LeadFeeCollectionData['feeItem_Amount'],
            'created_by' => $LeadFeeCollectionData['user'],
            'created_on' => $current_datetime
        );
        $this->db->insert('fee_collection_item', $feeCollectionItemData);

        $updateQuery="update candidate_fee_item set amount_paid= amount_paid+".$LeadFeeCollectionData['payingAmount']." where candidate_fee_item_id=". $LeadFeeCollectionData['candidate_fee_item_id'];
        $this->db->query($updateQuery);

        return $this->feeCollectionDetails($feeCollectionId);
    }
}