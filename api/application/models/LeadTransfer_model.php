<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @controller Name Leead Transfer
 * @category        Models
 * @author          Abhilash
 */
class LeadTransfer_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    /**
     * Function to get Lead List datatables
     *
     * @return - array
     * @created date - 30 Mar 2016
     * @author : Abhilash
     */
    public function getLeadTransferList($branch_id){
        $table = 'branch_xref_lead';
        // Table's primary key
        $primaryKey = 'bxl`.`branch_xref_lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array('db' => '`bxl`.`lead_number`', 'dt' => 'lead_number', 'field' => 'lead_number'),
            array('db' => '`c`.`name` as `course_name`', 'dt' => 'course_name', 'field' => 'course_name'),
            array('db' => '`ls`.`lead_stage`', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
            array('db' => '`b1`.`name` as `transfered_to`', 'dt' => 'transfered_to', 'field' => 'transfered_to'),
            array('db' => '`l`.`name`', 'dt' => 'name', 'field' => 'name'),
            array('db' => '`l`.`phone`', 'dt' => 'phone', 'field' => 'phone'),
            array('db' => '`l`.`email`', 'dt' => 'email', 'field' => 'email'),
//            array('db' => '`b2`.`name` as `transfered_from`', 'dt' => 'transfered_from', 'field' => 'transfered_from'),
            array('db' => '`b1`.`branch_id` as `transfered_to_id`', 'dt' => 'transfered_to_id', 'field' => 'transfered_to_id'),
//            array('db' => '`b2`.`branch_id` as `transfered_from_id`', 'dt' => 'transfered_from_id', 'field' => 'transfered_from_id'),
            array('db' => '`rtv1`.`value` as `cName`', 'dt' => 'cName', 'field' => 'cName'),
            array('db' => '`rtv3`.`value` AS `qualificationName`', 'dt' => 'qualificationName', 'field' => 'qualificationName'),
            array('db' => '`bxl`.`lead_id`', 'dt' => 'lead_id', 'field' => 'lead_id'),
            array('db' => '`l`.`fk_company_id` AS `company_id`', 'dt' => 'company_id', 'field' => 'company_id'),
            array('db' => '`l`.`fk_qualification_id` AS `qualification_id`', 'dt' => 'qualification_id', 'field' => 'qualification_id'),
            array('db' => '`l`.`fk_institution_id` AS `institution_id`', 'dt' => 'institution_id', 'field' => 'institution_id'),
            array('db' => '`bxl`.`status` AS `lead_status_id`', 'dt' => 'lead_status_id', 'field' => 'lead_status_id'),
            array('db' => '`bxl`.`fk_course_id` as `course_id`', 'dt' => 'course_id', 'field' => 'course_id'),
            array('db' => '`bv`.`second_level_counsellor_updated_date`', 'dt' => 'second_level_counsellor_updated_date', 'field' => 'second_level_counsellor_updated_date'),
            array('db' => '`bv`.`second_level_counsellor_comments`', 'dt' => 'second_level_counsellor_comments', 'field' => 'second_level_counsellor_comments'),
            array('db' => '`e`.`name` as `counselloer_name`', 'dt' => 'counselloer_name', 'field' => 'counselloer_name'),
            array('db' => '`bv`.`branch_visited_date` as `branch_visited_date`', 'dt' => 'branch_visited_date', 'field' => 'branch_visited_date'),
            array('db' => 'if(`bv`.`is_counsellor_visited`=1,1,0) AS `is_counsellor_visited`', 'dt' => 'is_counsellor_visited', 'field' => 'is_counsellor_visited'),
            array('db' => '`bv`.`branch_visited_date` as `branch_visited_date`', 'dt' => 'branch_visited_date', 'field' => 'branch_visited_date'),
            array('db' => 'e1.name as branch_visit_name', 'dt' => 'branch_visit_name', 'field' => 'branch_visit_name'),
            array('db' => 'bv.branch_visit_comments as branch_visit_comments', 'dt' => 'branch_visit_comments', 'field' => 'branch_visit_comments'),
            array('db' => '`bxl`.`branch_xref_lead_id`', 'dt' => 'branch_xref_lead_id', 'field' => 'branch_xref_lead_id'),
            array( 'db' => 'if(l.gender=1,1,0) as gender',          'dt' => 'gender',  'field' => 'gender' ),
            array( 'db' => '(select bxl1.branch_xref_lead_id from branch_xref_lead bxl1 where bxl1.lead_id=bxl.lead_id AND bxl1.is_active=1 order by bxl1.branch_xref_lead_id desc LIMIT 1) AS new_branch_xref_lead_id',          'dt' => 'new_branch_xref_lead_id',  'field' => 'new_branch_xref_lead_id' )
        );

        $globalFilterColumns = array(
            array('db' => '`l`.`name`', 'dt' => 'name', 'field' => 'name'),
            array('db' => '`l`.`phone`', 'dt' => 'phone', 'field' => 'phone'),
            array('db' => '`l`.`email`', 'dt' => 'email', 'field' => 'email'),
            array('db' => '`c`.`name`', 'dt' => 'course_name', 'field' => 'course_name'),
            array('db' => '`b1`.`name`', 'dt' => 'transfered_to', 'field' => 'transfered_to'),
            array( 'db' => '`bxl`.`lead_number`',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array('db' => '`rtv1`.`value`', 'dt' => 'cName', 'field' => 'cName'),
            array('db' => '`rtv3`.`value`', 'dt' => '`rtv3`.`value`', 'field' => 'qualificationName'),
            array('db' => '`ls`.`lead_stage`', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
//            array('db' => '`b2`.`name`', 'dt' => 'transfered_from', 'field' => 'transfered_from')
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db' => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /*         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM branch_xref_lead bxl "
                . "JOIN lead as l ON l.lead_id=bxl.lead_id "
                . "JOIN course as c ON c.course_id=bxl.fk_course_id "
                /*. "JOIN branch as b ON b.branch_id=bxl.branch_id "*/
                . "LEFT JOIN branch b1 ON b1.branch_id = bxl.transferred_to "
                . "LEFT JOIN reference_type_value rtv1 ON (rtv1.reference_type_value_id=l.fk_company_id OR rtv1.reference_type_value_id=l.fk_institution_id) "
                . "LEFT JOIN reference_type_value rtv3 ON rtv3.reference_type_value_id=l.fk_qualification_id  "
                . "LEFT JOIN lead_stage ls ON bxl.`status`=ls.lead_stage_id "
                . "LEFT JOIN branch_visitor bv ON bxl.branch_xref_lead_id=bv.fk_branch_xref_lead_id "
                . "LEFT JOIN employee e1 ON e1.user_id=bv.created_by 
                    LEFT JOIN employee e2 ON e2.user_id=bv.created_by
                    LEFT JOIN user u1 ON u1.user_id=e1.user_id 
                    LEFT JOIN user u2 ON u2.user_id=e2.user_id 
                    LEFT JOIN employee e ON bv.second_level_counsellor_id=e.user_id";
        $extraWhere = "`bxl`.`branch_id`=".$branch_id." AND `bxl`.`is_active`=0";
        $groupBy = "";
        $responseData = (SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere, $groupBy));

        for ($i = 0; $i < count($responseData['data']); $i++)
        {
            $responseData['data'][$i]['transfered_to_id'] = encode($responseData['data'][$i]['transfered_to_id']);
//            $responseData['data'][$i]['transfered_from_id'] = encode($responseData['data'][$i]['transfered_from_id']);
            $responseData['data'][$i]['second_level_counsellor_updated_date'] = (new DateTime($responseData['data'][$i]['second_level_counsellor_updated_date']))->format('d M,Y');
            $responseData['data'][$i]['branch_visited_date'] = (new DateTime($responseData['data'][$i]['branch_visited_date']))->format('d M,Y');
            $responseData['data'][$i]['lead_id'] = encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id_encode'] = encode($responseData['data'][$i]['branch_xref_lead_id']);
            $responseData['data'][$i]['course_id'] = encode($responseData['data'][$i]['course_id']);
            $responseData['data'][$i]['comments']=array();
            $getComments="select bv.branch_visited_date,bv.second_level_counsellor_updated_date as counsellor_visited_date,bv.branch_visit_comments,bv.second_level_counsellor_comments,e1.name as counseollor_name,e.name as second_counsellor_name from branch_visitor bv,employee e1,employee e where bv.fk_branch_xref_lead_id =".$responseData['data'][$i]['new_branch_xref_lead_id']." AND is_counsellor_visited=1 AND status=1 AND bv.created_by=e1.user_id AND bv.second_level_counsellor_id=e.user_id order by bv.branch_visitor_id DESC";
            $res_comment=$this->db->query($getComments);
            if($res_comment){
                if($res_comment->num_rows()>0){
                    $row_comment=$res_comment->result();
                    foreach($row_comment as $k_comment=>$v_comment){
                        $responseData['data'][$i]['comments'][]=array('counsellor_visited_date'=>(new DateTime($v_comment->counsellor_visited_date))->format('d M,Y'),'branch_visited_date'=>(new DateTime($v_comment->branch_visited_date))->format('d M,Y'),'second_level_comments'=>$v_comment->second_level_counsellor_comments,'branch_visit_comments'=>$v_comment->branch_visit_comments,'counseollor_name'=>$v_comment->counseollor_name,'second_counseollor_name'=>$v_comment->second_counsellor_name);
                    }
                }
            }

        }
        return $responseData;
    }

    /**
     * Function to get Lead snapshot
     *
     * @return - array
     * @created date - 30 Mar 2016
     * @author : Abhilash
     */
    public function getLeadSpanshot($lead_id, $branch_id){
        $error = false;
        $this->db->select('branch_xref_lead_id,lead_id');
        $this->db->from('branch_xref_lead');
        $this->db->where('lead_number', $lead_id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $branch_id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if(!$error){
            $this->db->select('l.name, l.phone, l.email, bxl.branch_xref_lead_id, bxl.lead_id, bxl.branch_id, bxl.fk_course_id, bxl.lead_number, bxl.expected_visit_date, bxl.expected_enroll_date, bxl.transferred_to, bxl.transferred_from, b.name branch_name, c.name course_name');
            $this->db->from('branch_xref_lead bxl');
            $this->db->join('lead l', 'l.lead_id=bxl.lead_id');
            $this->db->join('course c', 'c.course_id=bxl.fk_course_id');
            $this->db->join('branch b', 'b.branch_id=bxl.branch_id');
            $this->db->where('bxl.lead_number', $lead_id);
            $this->db->where('bxl.branch_id', $branch_id);
            $this->db->where('bxl.is_active', 1);
            $this->db->order_by('bxl.branch_xref_lead_id');

            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    $data = $res_all[0];
                    foreach ($data as $key => $value) {
                        if($key == 'expected_visit_date'){
                            $data->$key = (new DateTime($value))->format('d M,Y');
                        }
                        if($key == 'expected_enroll_date'){
                            $data->$key = (new DateTime($value))->format('d M,Y');
                        }
                    }
                    if($data->transferred_to === null){
                        return $data;
                    } else {
                        $this->db->select('name');
                        $this->db->from('branch');
                        $this->db->where('branch_id', $data->transferred_to);

                        $res_all = $this->db->get();
                        if ($res_all) {
                            $res_all = $res_all->result();
                            if (count($res_all) > 0) {
                                $to = $res_all[0]->name;
                            }
                        } else {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }

                        $this->db->select('name');
                        $this->db->from('branch');
                        $this->db->where('branch_id', $data->transferred_from);

                        $res_all = $this->db->get();
                        if ($res_all) {
                            $res_all = $res_all->result();
                            if (count($res_all) > 0) {
                                $from = $res_all[0]->name;
                            }
                        } else {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                        foreach ($data as $key => $value) {
                            if($key === 'transferred_to'){
                                $data->$key = $to;
                            }
                            if($key === 'transferred_from'){
                                $data->$key = $from;
                            }
                        }
                        return $data;
                    }
                } else{
                    return 'nodata';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }

    /**
     * Function to save Lead transfer
     *
     * @return - array
     * @created date - 30 Mar 2016
     * @author : Abhilash
     */
    public function leadTransfer($data)
    {
        $error = false;
        $this->db->select('*');
        $this->db->from('branch_xref_lead');
        $this->db->where('lead_number', $data['lead_id']);
        $this->db->where('branch_id', $data['oldBranch']);
        $leadData = $this->db->get();
        if ($leadData) {
            $leadData = $leadData->result();
            if (count($leadData) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nolead';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', $data['branch']);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        //nocourse
        $this->db->select('fk_branch_id');
        $this->db->from('branch_xref_course');
        $this->db->where('fk_branch_id', $data['branch']);
        $this->db->where('fk_course_id', $leadData[0]->fk_course_id);
        $this->db->where('is_active', 1);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nocourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if(!$error){
            $leadData = $leadData[0];
            $leadsequence = 0;

            $this->db->select_max('lead_number');
            $this->db->from('branch_xref_lead');
            $this->db->where('branch_id', $data['branch']);
            $this->db->where('is_active', 1);

            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0 && strlen($query[0]['lead_number']) > 0) {
                    $leadsequence = $query[0]['lead_number'];
                } else {
                    $this->db->select('sequence_number');
                    $this->db->from('branch');
                    $this->db->where('branch_id', $data['branch']);
                    $query = $this->db->get();
                    if ($query) {
                        $query = $query->result_array();
                        if (count($query) > 0){
                            $leadsequence = $query[0]['sequence_number'];
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }
            }else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }

            $to = $leadData->branch_id;
            $leadId = $leadData->branch_xref_lead_id;
            $new = $leadData;
            foreach ($new as $key=>$value) {
                if($key == 'branch_xref_lead_id'){
                    unset($new->$key);
                }
                if($key == 'branch_id'){
                    $new->$key = $data['branch'];
                }
                if($key == 'updated_by'){
                    $new->$key = $_SERVER['HTTP_USER'];
                }
                if($key == 'updated_on'){
                    $new->$key = date('Y-m-d H:i:s');
                }
                if($key == 'lead_number'){
                    $new->$key = $leadsequence+1;
                }
                if($key == 'transferred_to'){
                    $new->$key = $data['branch'];
                }
                if($key == 'transferred_from'){
                    $new->$key = $to;
                }
            }

            $this->db->select('*');
            $this->db->from('branch_xref_lead');
            $this->db->where('lead_number', $data['lead_id']);
            $this->db->where('branch_id', $data['oldBranch']);
            $old = $this->db->get();
            if ($old) {
                $old = $old->result()[0];
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            foreach ($old as $key=>$value) {
                if($key == 'updated_by'){
                    $old->$key = $_SERVER['HTTP_USER'];
                }
                if($key == 'updated_on'){
                    $old->$key = date('Y-m-d H:i:s');
                }
                if($key == 'is_active'){
                    $old->$key = 0;
                }
                if($key == 'transferred_to'){
                    $old->$key = $leadData->branch_id;
                }
            }

            $this->db->select('fk_lead_call_schedule_head_id');
            $this->db->from('lead_user_xref_lead_due');
            $this->db->where('fk_lead_id', $leadData->lead_id);
            $this->db->where('fk_branch_xref_lead_id', $leadId);

            $res_all = $this->db->get();
            $this->db->trans_start();
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    //remove from lead_user_xref_lead_history, lead_user_xref_lead_release
                    $data_insert =array(
                        'release_count' =>1,
                        'released_by' =>$_SERVER['HTTP_USER'],
                        'released_on' =>date('Y-m-d H:i:s'),
                        'fk_lead_call_schedule_head_id' =>$res_all[0]->fk_lead_call_schedule_head_id
                    );
                    $this->db->insert('lead_user_xref_lead_release_head', $data_insert);

                    $data_insert1 =array(
                        'fk_user_id' =>$_SERVER['HTTP_USER'],
                        'fk_branch_xref_lead_id' => $leadId,
                        'fk_lead_id' => $data['lead_id'],
                        'fk_lead_user_xref_lead_release_head_id' => $this->db->insert_id(),
                        'fk_lead_call_schedule_head_id' =>$res_all[0]->fk_lead_call_schedule_head_id
                    );
                    $this->db->insert('lead_user_xref_lead_release', $data_insert1);

                    $this->db->delete('lead_user_xref_lead_due', ['fk_lead_id'=> $leadData->lead_id, 'fk_branch_xref_lead_id'=> $leadId]);
                }

                //remove and add only from branch_xref_lead
                $this->db->insert('branch_xref_lead', $new);
                $last_inserted_id = $this->db->insert_id();

                $this->db->where('lead_id', $leadData->lead_id);
                $this->db->where('branch_xref_lead_id', $leadId);
                $this->db->update('branch_xref_lead', $old);
                
                $data_insert =array(
                    'lead_id' =>$leadData->lead_id,
                    /*'branch_xref_lead_id' =>$leadId,*/
                    'branch_xref_lead_id' =>$last_inserted_id,
                    'type' =>'lead transfer',
                    'description' =>$data['desc'],
                    'created_by' =>$_SERVER['HTTP_USER'],
                    'created_on' =>date('Y-m-d H:i:s'),
                );
                $this->db->insert('timeline', $data_insert);

                //For Branch Visit if allotted
                $this->db->select('*');
                $this->db->from('branch_visitor');
                $this->db->where('fk_branch_xref_lead_id', $leadId);
                $result = $this->db->get();
                if ($result) {
                    $result = $result->result();
                    if (count($result) > 0) {
                        //delete old and add new with current details
                        foreach ($result as $key => $value) {
                            $value->fk_branch_xref_lead_id = $last_inserted_id;
                        }
                        $this->db->delete('branch_visitor', array('fk_branch_xref_lead_id' => $leadId));
                        $this->db->insert('branch_visitor', $result[0]);
                        //timeline for Branch Visit
                        $data_insert =array(
                            'lead_id' =>$leadData->lead_id,
                            'branch_xref_lead_id' =>$last_inserted_id,
                            'type' =>'Allotted Counsellor',
                            'description' =>'After Lead Transfer',
                            'created_by' =>$_SERVER['HTTP_USER'],
                            'created_on' =>date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('timeline', $data_insert);
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
                
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }

        }
    }

}
