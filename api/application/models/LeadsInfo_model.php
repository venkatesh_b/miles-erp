<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 21/3/16
 * Time: 9:49 PM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LeadsInfo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    function updateinfo($data,$lead_id,$params=array()){

        $branch_lead['fk_course_id']=$data['fk_course_id'];

        $this->db->select('l.alternate_mobile,l.phone,bxl.lead_number,b.`code`');
        $this->db->from('lead l');
        $this->db->JOIN('branch_xref_lead bxl','bxl.lead_id=l.lead_id');
        $this->db->JOIN('branch b','b.branch_id=bxl.branch_id');
        $this->db->where('l.lead_id!=',$lead_id);
        $this->db->where("(l.phone=".$data['alternate_mobile']." OR "."l.phone=".$data['phone'].")");
        $query = $this->db->get();
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else
        {
            if ($query->num_rows() > 0)
            {
                $res_duplicate=$query->result();
                foreach($res_duplicate as $k_dup=>$v_dup){
                    if($v_dup->phone==$data['phone'] || $v_dup->phone==$data['alternate_mobile'])
                    {
                        if($v_dup->phone==$data['phone'])
                        {
                            $data_error['mobile']='Contact number already exist';
                            break;
                        }
                        elseif($v_dup->phone==$data['alternate_mobile'])
                        {
                            $data_error['alternate']='Contact number already exist';
                            break;
                        }
                    }
                    else
                    {

                    }
                }
                $db_response=array("status"=>false,"message"=>'Contact number already exist for lead number '.$v_dup->lead_number.' in '.$v_dup->code.' branch',"data"=>$data_error);
            }
            else
            {
                $contactType=NULL;
                $this->db->select('reference_type_value_id');
                $this->db->from('reference_type_value rtv');
                $this->db->JOIN('reference_type rt','rt.reference_type_id=rtv.reference_type_id');
                $this->db->where('rt.name','Contact Type');
                $this->db->where('rtv.value',$params['coldCallcontacttype']);
                $getContactType=$this->db->get();
                $tem=$this->db->last_query();
                $fk_company_id=NULL;
                $fk_institution_id=NULL;
                if($getContactType)
                {
                    if($getContactType->num_rows()>0)
                    {
                        $getContactTypeRes=$getContactType->result();
                        foreach($getContactTypeRes as $kr=>$vr)
                        {
                            $contactType=$vr->reference_type_value_id;
                        }
                    }
                }
                if($params['coldCallcontacttype']=='Corporate')
                {
                    $fk_company_id=$params['ColdCallContactCompany'];
                    $fk_institution_id=NULL;
                }
                else if($params['coldCallcontacttype']=='University'){
                    $fk_institution_id=$params['ColdCallContactInstitution'];
                    $fk_company_id=NULL;
                }
                else if($params['coldCallcontacttype']=='Retail'){
                    $fk_company_id=NULL;
                    $fk_institution_id=NULL;
                }
                else{
                    $fk_company_id=NULL;
                    $fk_institution_id=NULL;
                }
                unset($data['fk_course_id']);
                $data['fk_company_id']=$fk_company_id;
                $data['fk_institution_id']=$fk_institution_id;
                $data['fk_contact_type_id']=$contactType;
                $this->db->where('lead_id', $lead_id);
                $update= $this->db->update('lead',$data);
                $res_update=array();
                if($update){

                    $this->db->where('lead_id', $lead_id);
                    $res_update = $this->db->update('branch_xref_lead',$branch_lead);
                }else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }


                if($res_update){
                    $db_response=array("status"=>true,"message"=>"updated successfully","data"=>array());
                }
                else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }

        }

        return $db_response;

    }
    function getSingleLeadById($lead_id,$branch_xref_lead_id=''){

        $this->db->select("bl.branch_xref_lead_id,if(l.gender=1,1,0) as gender,l.lead_id,l.name,e.name as created_by,e1.name as last_modified,l.alternate_mobile,l.phone,l.status,bl.next_followup_date,bl.lead_number,bl.created_on,
         ls.lead_stage as leadStage,l.email,c.course_id,rtvqual.reference_type_value_id as qual_id,rtvqual.value as qualification,c.name as course,rtvcity.value as city,rtvDesignation.reference_type_value_id as desi_id,rtvDesignation.value as designation,rtvcmp.reference_type_value_id as companyID,rtvcmp.value as company,rtvinit.value as institution,rtvcntType.value as contactType,l.fk_institution_id,l.secondary_phone,l.secondary_email,rtvqual.parent_id as parent_qual_id,rtvSource.value as contactSource, l.address, bl.eligibility");
        $this->db->from("lead l");
        $this->db->join('branch_xref_lead bl', 'bl.lead_id=l.lead_id', 'left');
        $this->db->join('user u','u.user_id=bl.updated_by','left');
        $this->db->join("lead_stage ls","ls.lead_stage_id=bl.status");
        $this->db->join('course c', 'c.course_id=bl.fk_course_id', 'left');
        $this->db->join('employee e', "e.user_id=bl.created_by ","left");
        $this->db->join('employee e1', "e1.user_id=bl.updated_by","left");
        $this->db->join('reference_type_value rtvcity', 'rtvcity.reference_type_value_id=l.city_id', 'left');
        $this->db->join('reference_type_value rtvcmp', 'rtvcmp.reference_type_value_id=l.fk_company_id', 'left');
        $this->db->join('reference_type_value rtvqual', 'rtvqual.reference_type_value_id=l.fk_qualification_id', 'left');
        $this->db->join('reference_type_value rtvDesignation', 'rtvDesignation.reference_type_value_id=l.fk_designation_id', 'left');
        $this->db->join('reference_type_value rtvinit', 'rtvinit.reference_type_value_id=l.fk_institution_id', 'left');
        $this->db->join('reference_type_value rtvcntType', 'rtvcntType.reference_type_value_id=l.fk_contact_type_id', 'left');
        $this->db->join('reference_type_value rtvSource', 'rtvSource.reference_type_value_id=l.fk_source_id', 'left');
        $this->db->where("l.lead_id",$lead_id);
        if($branch_xref_lead_id!=''){
            $this->db->where("bl.branch_xref_lead_id",$branch_xref_lead_id);
        }
        //$this->db->where("bl.is_active=1");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                if($branch_xref_lead_id!='')
                    $is_fee_exist_query="select if(count(cf.candidate_fee_id)>0,1,0) as is_feeexist  from candidate_fee cf,branch_xref_lead bxl where bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id and bxl.lead_id=".$lead_id." and bxl.lead_number=".$res[0]->lead_number." and cf.fk_branch_xref_lead_id=".$branch_xref_lead_id;
                else
                    $is_fee_exist_query="select if(count(cf.candidate_fee_id)>0,1,0) as is_feeexist  from candidate_fee cf,branch_xref_lead bxl where bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id and bxl.lead_id=".$lead_id." and bxl.lead_number=".$res[0]->lead_number;
                $is_fee_res=$this->db->query($is_fee_exist_query);
                $is_feeexist=0;
                if($is_fee_res){
                    if($is_fee_res->num_rows()>0){
                        $is_fee_row=$is_fee_res->result();
                        $is_feeexist=$is_fee_row[0]->is_feeexist;
                    }

                }
                else{

                }
                $res[0]->is_feeexist=$is_feeexist;
                //exit;
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;


    }
    function getLeadEducationalInfo($lead_id){
        $this->db->select("lead_educational_info_id,course,university,year_of_completion,percentage,comments");
        $this->db->from("lead_educational_info");
        $this->db->where("lead_id",$lead_id);
        $this->db->order_by("year_of_completion","desc");
        $query = $this->db->get();
        $db_response=array();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        /*for($i=0;$i<count($db_response['data']);$i++)
        {
            $db_response['data'][$i]->lead_educational_info_id=encode($db_response['data'][$i]->lead_educational_info_id);
        }*/

        return $db_response;

    }
    function getLeadHistory($lead_id,$branch_xref_lead_id=''){

        $sql="SELECT DISTINCT(z.user_id),z.email, z.name, z.phone, z.image,z.description, z.Designation FROM ((SELECT e.email, e.name, e.phone, e.image,ulh.description, rtv.value as Designation,ulh.created_on,ulh.user_id
                FROM user_xref_lead_history ulh
                LEFT JOIN user u ON u.user_id = ulh.user_id
                LEFT JOIN employee e ON e.user_id=u.user_id
                LEFT JOIN user_xref_role ur ON ur.fk_user_id=u.user_id
                LEFT JOIN reference_type_value rtv ON rtv.reference_type_value_id = ur.fk_role_id
                WHERE ulh.lead_id = '".$lead_id."' )
                union all
                (SELECT e.email, e.name, e.phone, e.image,lulh.description, rtv.value as Designation,lulh.created_on,lulh.fk_user_id as user_id
                FROM lead_user_xref_lead_history lulh
                LEFT JOIN user u ON u.user_id = lulh.fk_user_id
                LEFT JOIN employee e ON e.user_id=u.user_id
                LEFT JOIN user_xref_role ur ON ur.fk_user_id=u.user_id
                LEFT JOIN reference_type_value rtv ON rtv.reference_type_value_id = ur.fk_role_id
                WHERE lulh.fk_lead_id = '".$lead_id."')) z group by z.user_id order by z.created_on DESC";

        $query = $this->db->query($sql);
        $query->result_array();

        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function AddDesignation($data,$type){
        $this->db->select('reference_type_id');
        $this->db->from('reference_type');

        $this->db->where('name',trim($type));

        $query = $this->db->get();
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{
            if ($query->num_rows() > 0)
            {
                $results=$query->result();

                $this->db->select('reference_type_value_id');
                $this->db->from('reference_type_value');

                $this->db->where('value',trim($data['value']));
                $this->db->where('reference_type_id',$results[0]->reference_type_id);

                $query = $this->db->get();
                if ($query->num_rows() > 0)
                {
                    $db_response=array("status"=>false,"message"=>"Already Exist","data"=>array("message"=>"Already Exist"));
                }
                else{
                    $data['reference_type_id']= $results[0]->reference_type_id;
                    $res_insert=$this->db->insert('reference_type_value', $data);
                    if($res_insert){
                        $db_response=array("status"=>true,"message"=>"Added successfully","data"=>array());
                    }
                    else{
                        $error = $this->db->error();
                        $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                    }
                }



            }

        }

        return $db_response;

    }

    function checkforEducationInfo($data,$edu_info_id=0)
    {
        //lead_id,course
        $this->db->select('lead_educational_info_id');
        $this->db->from('lead_educational_info');
        $this->db->where('course', $data['course']);
        $this->db->where('lead_id', $data['lead_id']);
        if($edu_info_id!=0)
        {
            $this->db->where('lead_educational_info_id !=', $edu_info_id);
        }
        $venue_res = $this->db->get();
        $num = $venue_res->num_rows();
        return $num;
    }

    function AddEducationalInfoDetails($data){
        $db_response="";
        $is_valid=$this->checkforEducationInfo($data);
        if(isset($data) && !empty($data))
        {
            if($is_valid == 0)
            {
                $res_insert = $this->db->insert('lead_educational_info', $data);
                if ($res_insert) {
                    $db_response = array("status" => true, "message" => "Added successfully", "data" => array());
                } else {
                    $error = $this->db->error();
                    $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
                }
            }
            else
            {
                $db_response = array("status" => false, "message" => "Qualification already added", "data" => array());
            }
        }


        return $db_response;

    }
    function UpdateEducationalInfoDetails($data,$id){
        $db_response="";
        $is_valid=$this->checkforEducationInfo($data,$id);

        if(isset($data) && !empty($data))
        {
            if($is_valid == 0)
            {
                $this->db->where('lead_educational_info_id', $id);
                $res_insert = $this->db->update('lead_educational_info', $data);
                if ($res_insert)
                {
                    $db_response = array("status" => true, "message" => "Updated successfully", "data" => array());
                }
                else
                {
                    $error = $this->db->error();
                    $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
                }
            }
            else
            {
                $db_response = array("status" => false, "message" => "Qualification already added", "data" => array());
            }
        }
        return $db_response;

    }
    function getEducationDetailsById($edu_id){
        $this->db->select("course,university,year_of_completion,percentage,comments,fk_course_id");
        $this->db->from("lead_educational_info");
        $this->db->where("lead_educational_info_id",$edu_id);
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $res[0]->fk_course_parent_id='';
                if($res[0]->fk_course_id!=NULL && $res[0]->fk_course_id!=''){
                    $this->db->select('parent_id');
                    $this->db->from('reference_type_value');
                    $this->db->where('reference_type_value_id',$res[0]->fk_course_id);
                    $query1 = $this->db->get();
                    if($query1->num_rows() > 0){
                        $res1=$query1->result();
                        $res[0]->fk_course_parent_id=$res1[0]->parent_id;
                    }
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    function deleteEducationInfoById($infoId){

        $this->db->where('lead_educational_info_id', $infoId);
        $status=$this->db->delete('lead_educational_info');
        if($status){
            $db_response=array("status"=>true,"message"=>"Deleted successfully","data"=>array());
        }else{
            $db_response=array("status"=>false,"message"=>"Problem in deleting","data"=>array());
        }
        return $db_response;
    }
    function AddOtherInfoDetails($data,$lead_id,$branch_xref_lead_id=''){
        $db_response="";
        if(isset($data) && !empty($data)) {
            if(isset($lead_id)) {
                $this->db->where("lead_id", $lead_id);
                if($branch_xref_lead_id!='')
                    $this->db->where("branch_xref_lead_id", $branch_xref_lead_id);
                $res_insert = $this->db->update('branch_xref_lead', $data);
            }

            if ($res_insert) {
                $db_response = array("status" => true, "message" => "Updated successfully", "data" => array());
            } else {
                $error = $this->db->error();
                $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
            }
        }


        return $db_response;

    }
    function getOtherInfoDetailsById($lead_id,$branch_xref_lead_id=''){
        $this->db->select("Significance_of_CPA,Willingness_to_pursue_the_CPA,Response_on_Miles_CPA,Value_addition,CMA_No,eligibility");
        $this->db->from("branch_xref_lead");
        $this->db->where("lead_id",$lead_id);
        if($branch_xref_lead_id!=''){
            $this->db->where("branch_xref_lead_id",$branch_xref_lead_id);
        }
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    public function getTimelinesById($lead_id,$branch_xref_lead_id=''){
        $this->db->select("e.name,t.description,t.interaction_file,t.interaction_date,t.type,t.created_on,rtv.value as Designation");
        $this->db->from("timeline t");
        $this->db->join('lead l', 'l.lead_id = t.lead_id', 'left');
        if($branch_xref_lead_id!='')
            $this->db->join('branch_xref_lead bl', 'bl.branch_xref_lead_id=t.branch_xref_lead_id AND bl.branch_xref_lead_id='.$branch_xref_lead_id, 'left');
        else
            $this->db->join('branch_xref_lead bl', 'bl.branch_xref_lead_id=t.branch_xref_lead_id', 'left');
        $this->db->join('user_xref_lead_history ulh','ulh.user_xref_lead_history_id=bl.fk_user_xref_lead_history_id','left');
        $this->db->join('employee e','e.user_id=t.created_by','left');
        $this->db->join('user_xref_role ur','ur.fk_user_id=t.created_by','left');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id = ur.fk_role_id','left');
        $this->db->where("t.lead_id",$lead_id);
        $this->db->order_by("t.timeline_id","DESC");

        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;


    }
    function getBranchVisitorById($branchXrefId){
        $this->db->select("bv.second_level_counsellor_comments,e.name,bv.second_level_counsellor_updated_date as created_date");
        $this->db->from("branch_visitor bv");
        $this->db->join("employee e","e.user_id=bv.created_by","left");
        $this->db->where("bv.status",1);
        if(isset($branchXrefId) && !empty($branchXrefId)) {
            $this->db->where("bv.fk_branch_xref_lead_id",$branchXrefId);
        }
        $this->db->where("is_counsellor_visited",1);
        $this->db->order_by('bv.branch_visitor_id','desc');
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $k=>$v){
                    $v->created_date=date("d M,Y",strtotime($v->created_date));
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    function saveTimelineDetails($data){

        $db_response="";
        if(isset($data) && !empty($data)) {
        unset($data['attachment']);
            $res_insert = $this->db->insert('timeline', $data);
            if ($res_insert) {
                $db_response = array("status" => true, "message" => "Added successfully", "data" => array());
            } else {
                $error = $this->db->error();
                $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
            }
        }
        return $db_response;

    }
    public function getNewLeadsCount($status="",$branchId='',$leadType=''){
        $this->db->select("count(branch_xref_lead_id) as NewLeads");
        $this->db->from("branch_xref_lead bl");
        $this->db->join("lead l","l.lead_id=bl.lead_id","left");
        $this->db->join("lead_stage ls","ls.lead_stage_id=bl.status","left");
        $this->db->join("reference_type_value rtvtype","rtvtype.reference_type_value_id=l.fk_contact_type_id","left");

        $this->db->where("bl.branch_id",$branchId);
        $this->db->where("bl.is_active",1);
        if(isset($leadType) && !empty($leadType) && $leadType!='') {
            $this->db->where("rtvtype.value",$leadType);
        }
        if(isset($status) && !empty($status) && $status!='') {
            $this->db->where("ls.lead_stage",$status);
        }
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'no data',"data"=>0);
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }

    function TransferdLeadsCount($branchId='',$leadType='')
    {
        $this->db->select("count(branch_xref_lead_id) as Transfered");
        $this->db->from("branch_xref_lead bl");
        $this->db->join("lead l","l.lead_id=bl.lead_id","left");
        $this->db->join("reference_type_value rtvtype","rtvtype.reference_type_value_id=l.fk_contact_type_id","left");
        $this->db->where("rtvtype.value",$leadType);
        $this->db->where("transferred_to",$branchId);
        $this->db->where("transferred_to is not null and transferred_from is not null");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'no data',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;


    }
    public function getLeadsList($data){

        $table = 'branch_xref_lead';
        // Table's primary key
        $primaryKey = 'branch_xref_lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'bl.lead_number',                  'dt' => 'lead_number',           'field' => 'lead_number' ),
            array( 'db' => 'c.name as course_name',           'dt' => 'course_name',          'field' => 'course_name' ),
            array( 'db' => 'ls.lead_stage as level',              'dt' => 'level',             'field' => 'level' ),
            array( 'db' => 'l.name as lead_name',             'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => '`bl`.`created_on`',               'dt' => 'created_on',           'field' => 'created_on', 'formatter' => function( $d, $row ) {
                return date( 'd M,Y', strtotime($d));}),
            array( 'db' => 'l.email',                         'dt' => 'email',                'field' => 'email' ),
            array( 'db' => 'l.phone',                         'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'rtvcity.value as city',           'dt' => 'city',                 'field' => 'city' ),
            array( 'db' => 'rtvcmp.value as company',         'dt' => 'company',              'field' => 'company' ),
            array( 'db' => 'rtvqual.value as qualification',  'dt' => 'qualification',        'field' => 'qualification' ),
            array( 'db' => 'rtvinit.value as institution',    'dt' => 'institution',          'field' => 'institution' ),
            array( 'db' => 'bl.lead_id',                      'dt' => 'lead_id',              'field' => 'lead_id' ),
            array( 'db' => 'bl.branch_xref_lead_id',          'dt' => 'branch_xref_lead_id',  'field' => 'branch_xref_lead_id' ),
            array( 'db' => 'c.course_id',                     'dt' => 'course_id',            'field' => 'course_id' ),
            array( 'db' => 'if(l.gender=1,1,0) as gender',          'dt' => 'gender',  'field' => 'gender' ),
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name ',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'c.name',    'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'ls.lead_stage', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
            array( 'db' => 'l.email', 'dt' => 'email', 'field' => 'email'),
            array( 'db' => 'l.phone', 'dt' => 'phone', 'field' => 'phone'),
            array( 'db' => 'rtvqual.value',  'dt' => 'rtvqual.value',        'field' => 'qualification' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from branch_xref_lead bl left join lead l on l.lead_id=bl.lead_id
                       left join lead_stage ls on  ls.lead_stage_id=bl.status
                       left join course c on c.course_id=bl.fk_course_id
                       left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
                       left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
                       left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
                       left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id
                       left join reference_type_value rtvtype on rtvtype.reference_type_value_id=l.fk_contact_type_id
                       ";

        if(isset($data['branchId']) && !empty($data['branchId']))
        {
            if(isset($data['type']) && !empty($data['type']))
            {
                $extraWhere = "bl.is_active=1 AND ls.lead_stage!='M7' and bl.branch_id=" . $data['branchId'] . " and rtvtype.value='" . $data['type'] . "' AND bl.is_active=1";
            }
            else
            {
                $extraWhere = "bl.is_active=1 AND ls.lead_stage!='M7' and bl.branch_id=" . $data['branchId'] . " AND bl.is_active=1";
            }
        }
        else
        {
            $data['branchId']=0;
            $extraWhere = "bl.is_active=1 AND ls.lead_stage!='M7' and bl.branch_id=".$data['branchId']."";
        }
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id_encode']=encode($responseData['data'][$i]['branch_xref_lead_id']);
            //$responseData['data'][$i]['second_level_counsellor_updated_date']=(new DateTime($responseData['data'][$i]['second_level_counsellor_updated_date']))->format('d M,Y');
            //$responseData['data'][$i]['branch_visited_date']=(new DateTime($responseData['data'][$i]['branch_visited_date']))->format('d M,Y');
            $responseData['data'][$i]['comments']=array();
            $getComments="select bv.branch_visited_date,bv.second_level_counsellor_updated_date as counsellor_visited_date,bv.branch_visit_comments,bv.second_level_counsellor_comments,e1.name as counseollor_name,e.name as second_counsellor_name from branch_visitor bv,employee e1,employee e where bv.fk_branch_xref_lead_id =".$responseData['data'][$i]['branch_xref_lead_id']." AND is_counsellor_visited=1 AND status=1 AND bv.created_by=e1.user_id AND bv.second_level_counsellor_id=e.user_id order by bv.branch_visitor_id DESC";
            $res_comment=$this->db->query($getComments);
            if($res_comment){
                if($res_comment->num_rows()>0){
                    $row_comment=$res_comment->result();
                    foreach($row_comment as $k_comment=>$v_comment){
                        $responseData['data'][$i]['comments'][]=array('counsellor_visited_date'=>(new DateTime($v_comment->counsellor_visited_date))->format('d M,Y'),'branch_visited_date'=>(new DateTime($v_comment->branch_visited_date))->format('d M,Y'),'second_level_comments'=>$v_comment->second_level_counsellor_comments,'branch_visit_comments'=>$v_comment->branch_visit_comments,'counseollor_name'=>$v_comment->counseollor_name,'second_counseollor_name'=>$v_comment->second_counsellor_name);
                    }
                }
            }
        }
        return $responseData;


    }
    function getleadId($lead_id){

        $this->db->select("bl.branch_xref_lead_id,if(l.gender=1,1,0) as gender,l.lead_id,l.name,e.name as created_by,e1.name as last_modified,l.alternate_mobile,l.phone,l.status,bl.next_followup_date,bl.lead_number,bl.created_on,
         ls.lead_stage as leadStage,l.email,c.course_id,rtvqual.reference_type_value_id as qual_id,rtvqual.value as qualification,c.name as course,rtvcity.value as city,rtvDesignation.reference_type_value_id as desi_id,rtvDesignation.value as designation,rtvcmp.reference_type_value_id as companyID,rtvcmp.value as company,rtvinit.value as institution,rtvcntType.value as contactType,l.fk_institution_id,l.secondary_phone,l.secondary_email,rtvqual.parent_id as parent_qual_id,rtvSource.value as contactSource,`bl`.`fk_batch_id` as leadBatchId, bl.placement_file, bl.placement_comments");
        $this->db->from("lead l");
        $this->db->join('branch_xref_lead bl', 'bl.lead_id=l.lead_id', 'left');
        $this->db->join('user u','u.user_id=bl.updated_by','left');
        $this->db->join("lead_stage ls","ls.lead_stage_id=bl.status");
        $this->db->join('course c', 'c.course_id=bl.fk_course_id', 'left');
        $this->db->join('employee e', "e.user_id=bl.created_by ","left");
        $this->db->join('employee e1', "e1.user_id=bl.updated_by","left");
        $this->db->join('reference_type_value rtvcity', 'rtvcity.reference_type_value_id=l.city_id', 'left');
        $this->db->join('reference_type_value rtvcmp', 'rtvcmp.reference_type_value_id=l.fk_company_id', 'left');
        $this->db->join('reference_type_value rtvqual', 'rtvqual.reference_type_value_id=l.fk_qualification_id', 'left');
        $this->db->join('reference_type_value rtvDesignation', 'rtvDesignation.reference_type_value_id=l.fk_designation_id', 'left');
        $this->db->join('reference_type_value rtvinit', 'rtvinit.reference_type_value_id=l.fk_institution_id', 'left');
        $this->db->join('reference_type_value rtvcntType', 'rtvcntType.reference_type_value_id=l.fk_contact_type_id', 'left');
        $this->db->join('reference_type_value rtvSource', 'rtvSource.reference_type_value_id=l.fk_source_id', 'left');
        $this->db->where("bl.branch_xref_lead_id",$lead_id);
        //$this->db->where("bl.is_active=1");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $k=>$v){
                    $v->lead_id=encode($v->lead_id);
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    /* Added by s.raju */



    function updateLeadCourse($lead_id,$branch_xref_lead_id,$course_id,$course_name)
    {
        $this->db->select("candidate_fee_id");
        $this->db->from("candidate_fee");
        $this->db->where('fk_branch_xref_lead_id',$branch_xref_lead_id);
        $cf_query = $this->db->get();
        if($cf_query->num_rows() >0)
        {
            $db_response=array("status"=>false,"message"=>'Fee already assigned to this lead',"data"=>array($lead_id,$branch_xref_lead_id));
        }
        else
        {
            $this->db->select("b.branch_xref_lead_id,l.lead_id,c.name as course");
            $this->db->from("lead l");
            $this->db->join('branch_xref_lead b', 'b.lead_id=l.lead_id');
            $this->db->join('course c', 'c.course_id=b.fk_course_id');
            $this->db->where('b.lead_id',$lead_id);
            $query = $this->db->get();
            if($query)
            {
                if ($query->num_rows() > 0)
                {
                    $lead_data = $query->result();
                    $data['fk_course_id']=$course_id;
                    $this->db->where('lead_id', $lead_id);
                    $this->db->where('branch_xref_lead_id', $branch_xref_lead_id);
                    $update= $this->db->update('branch_xref_lead',$data);
                    $tdata['lead_id']=$lead_id;
                    $tdata['branch_xref_lead_id']=$branch_xref_lead_id;
                    $tdata['type']='Lead Course Change';
                    $tdata['description']='Lead Course Changed from '.$lead_data[0]->course.' to '.$course_name;
                    $tdata['created_by']=$_SERVER['HTTP_USER'];
                    $tdata['created_on']=date('Y-m-d H:i:s');
                    $tline_insert = $this->db->insert('timeline', $tdata);
                    $db_response=array("status"=>true,"message"=>'Lead course updated successfully',"data"=>array());
                }
                else
                {
                    $db_response=array("status"=>false,"message"=>'Incorrect details',"data"=>array($lead_id,$branch_xref_lead_id));
                }
            }
            else
            {
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        }
        return $db_response;
    }
    function saveStudentResume($data)
    {
        $this->db->where('lead_id', $data['lead_id']);
        $update = $this->db->update('branch_xref_lead', $data);
        if($update){
            return true;
        }else{
            return false;
        }
    }
    public function getNetLeadsList($data)
    {
        $table = 'branch_xref_lead';
        // Table's primary key
        $primaryKey = 'branch_xref_lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'bl.lead_number',                  'dt' => 'lead_number',           'field' => 'lead_number' ),
            array( 'db' => 'c.name as course_name',           'dt' => 'course_name',          'field' => 'course_name' ),
            array( 'db' => 'ls.lead_stage as level',              'dt' => 'level',             'field' => 'level' ),
            array( 'db' => 'l.name as lead_name',             'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => '`bl`.`created_on`',               'dt' => 'created_on',           'field' => 'created_on', 'formatter' => function( $d, $row ) {
                return date( 'd M,Y', strtotime($d));}),
            array( 'db' => 'l.email',                         'dt' => 'email',                'field' => 'email' ),
            array( 'db' => 'l.phone',                         'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'rtvcity.value as city',           'dt' => 'city',                 'field' => 'city' ),
            array( 'db' => 'rtvcmp.value as company',         'dt' => 'company',              'field' => 'company' ),
            array( 'db' => 'rtvqual.value as qualification',  'dt' => 'qualification',        'field' => 'qualification' ),
            array( 'db' => 'rtvinit.value as institution',    'dt' => 'institution',          'field' => 'institution' ),
            array( 'db' => 'bl.lead_id',                      'dt' => 'lead_id',              'field' => 'lead_id' ),
            array( 'db' => 'bl.branch_xref_lead_id',          'dt' => 'branch_xref_lead_id',  'field' => 'branch_xref_lead_id' ),
            array( 'db' => 'c.course_id',                     'dt' => 'course_id',            'field' => 'course_id' ),
            array( 'db' => 'if(l.gender=1,1,0) as gender',          'dt' => 'gender',  'field' => 'gender' ),
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name ',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'c.name',    'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'ls.lead_stage', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
            array( 'db' => 'l.email', 'dt' => 'email', 'field' => 'email'),
            array( 'db' => 'l.phone', 'dt' => 'phone', 'field' => 'phone'),
            array( 'db' => 'rtvqual.value',  'dt' => 'rtvqual.value',        'field' => 'qualification' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from branch_xref_lead bl left join lead l on l.lead_id=bl.lead_id
                       left join lead_stage ls on  ls.lead_stage_id=bl.status
                       left join course c on c.course_id=bl.fk_course_id
                       left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
                       left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
                       left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
                       left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id
                       left join reference_type_value rtv on rtv.reference_type_value_id=l.fk_source_id";

        if(isset($data['branchId']) && !empty($data['branchId'])) {
            $extraWhere = "bl.is_active=1 AND ls.lead_stage!='M7' AND rtv.value='net enquiry' and bl.branch_id=" . $data['branchId'] . " AND bl.is_active=1";
        }else{
            $data['branchId']=0;
            $extraWhere = "bl.is_active=1 AND ls.lead_stage!='M7' AND rtv.value='net enquiry' and bl.branch_id=".$data['branchId']."";
        }
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id_encode']=encode($responseData['data'][$i]['branch_xref_lead_id']);
            $responseData['data'][$i]['comments']=array();
            $getComments="select bv.branch_visited_date,bv.second_level_counsellor_updated_date as counsellor_visited_date,bv.branch_visit_comments,bv.second_level_counsellor_comments,e1.name as counseollor_name,e.name as second_counsellor_name from branch_visitor bv,employee e1,employee e where bv.fk_branch_xref_lead_id =".$responseData['data'][$i]['branch_xref_lead_id']." AND is_counsellor_visited=1 AND status=1 AND bv.created_by=e1.user_id AND bv.second_level_counsellor_id=e.user_id order by bv.branch_visitor_id DESC";
            $res_comment=$this->db->query($getComments);
            if($res_comment){
                if($res_comment->num_rows()>0){
                    $row_comment=$res_comment->result();
                    foreach($row_comment as $k_comment=>$v_comment){
                        $responseData['data'][$i]['comments'][]=array('counsellor_visited_date'=>(new DateTime($v_comment->counsellor_visited_date))->format('d M,Y'),'branch_visited_date'=>(new DateTime($v_comment->branch_visited_date))->format('d M,Y'),'second_level_comments'=>$v_comment->second_level_counsellor_comments,'branch_visit_comments'=>$v_comment->branch_visit_comments,'counseollor_name'=>$v_comment->counseollor_name,'second_counseollor_name'=>$v_comment->second_counsellor_name);
                    }
                }
            }
        }
        return $responseData;
    }
    public function getNetYesterdaysLeadsCount($type="",$branchId=''){
        $count = 0;
        $this->db->select("count(branch_xref_lead_id) as counts");
        $this->db->from("branch_xref_lead bl");
        $this->db->join("lead l","l.lead_id=bl.lead_id","left");
        $this->db->join("reference_type_value rtv","rtv.reference_type_value_id=l.fk_source_id","left");
        $this->db->where("bl.branch_id",$branchId);
        $this->db->where("rtv.value","net enquiry");
        $this->db->where("bl.is_active",1);

        if($type == 'yesterday'){
            $this->db->where("DATE_FORMAT(subdate(now(), 1),'%Y-%m-%d 00:00:00')=DATE_FORMAT(bl.created_on,'%Y-%m-%d 00:00:00')");
        }
        else if($type == 'today'){
            $this->db->where("DATE_FORMAT(now(),'%Y-%m-%d 00:00:00')=DATE_FORMAT(bl.created_on,'%Y-%m-%d 00:00:00')");
        }

        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $count = $res[0]->counts;
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            $count = 0;
        }
        return $count;
    }

    public function saveEligibilityDownload($data){
        $res_insert=$this->db->insert('branch_xref_lead_uploads', $data);
        if($res_insert){
            $this->db->where('branch_xref_lead_id', $data['branch_xref_lead_id']);
            return true;
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
        }
        return $db_response;
    }

    public function saveEligibility($data){
        $this->db->where('branch_xref_lead_id', $data['branch_xref_lead_id']);
        $update = $this->db->update('branch_xref_lead', array('eligibility'=>$data['eligibility']));
        if($update){
            return true;
        }else{
            return false;
        }
    }

    public function getEligibilityFileList($type, $branch_xref_lead_id){
        $eligibility = '';
        $this->db->select('bxl.eligibility');
        $this->db->from('branch_xref_lead bxl');
        $this->db->where('bxl.branch_xref_lead_id', $branch_xref_lead_id);
        $query = $this->db->get();
        if($query){
            $eligibility = $query->result();
            $eligibility = $eligibility[0]->eligibility;
        }

        $this->db->select('*');
        $this->db->from('branch_xref_lead_uploads');
        $this->db->where('type', $type);
        $this->db->where('branch_xref_lead_id', $branch_xref_lead_id);
        $query = $this->db->get();
        if($query){
            $result = $query->result();
            /*foreach($result as$key=>$val){
                $val->branch_xref_lead_uploads_id = encode($val->branch_xref_lead_uploads_id);
            }*/
            $db_response=array("status"=>true,"message"=>'list',"data"=>array('data'=>$result, 'eligibility' => $eligibility));
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }

    public function deleteEligibilityFile($branch_xref_lead_id){
        $this->db->where('branch_xref_lead_uploads_id', $branch_xref_lead_id);
        $status=$this->db->delete('branch_xref_lead_uploads');
        if($status){
            $db_response=array("status"=>true,"message"=>"Deleted successfully","data"=>array());
        }else{
            $db_response=array("status"=>false,"message"=>"Problem in deleting","data"=>array());
        }
        return $db_response;
    }

    public function saveStudentRefrence($leadRef, $data){

        foreach($leadRef as $val)
            $res_insert=$this->db->insert('student_reference', $val);
//        die($this->db->last_query());

        $this->db->where('branch_xref_lead_id', $data['branch_xref_lead_id']);
        $this->db->update('branch_xref_lead', $data);

        return array("status"=>true,"message"=>"Updated successfully","data"=>array());
    }

    public function deleteStudentRefrence($data){
        $this->db->where('branch_xref_lead_id', $data['branch_xref_lead_id']);
        $status=$this->db->delete('student_reference');
        return 1;
    }

    public function getStudentReference($branch_xref_lead_id){

        $this->db->select('bxl.refrence_comment');
        $this->db->from('branch_xref_lead bxl');
        $this->db->where('bxl.branch_xref_lead_id', $branch_xref_lead_id);
        $query = $this->db->get();
        if($query){
            $data = $query->result();
            $data = $data[0]->refrence_comment;
        }

        $this->db->select('sr.*, l.name');
        $this->db->from('student_reference sr');
        $this->db->join('branch_xref_lead bxl', 'sr.branch_xref_lead_id=bxl.branch_xref_lead_id');
        $this->db->join('lead l', 'bxl.lead_id=l.lead_id');
        $this->db->where('sr.branch_xref_lead_id', $branch_xref_lead_id);
        $query = $this->db->get();
        if($query){
            $result = $query->result();
            /*foreach($result as$key=>$val){
                $val->branch_xref_lead_uploads_id = encode($val->branch_xref_lead_uploads_id);
            }*/
            $db_response=array("status"=>true,"message"=>'list',"data"=>array('data'=>$result, 'ref_comment' => $data));
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
}