<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 15-03-2016
 * Time: 02:48 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MWBCallSchedule_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');

    }
    function getCallSchedules($param){
        $this->db->select('u.user_id,e.name,e.image,rtv.`value` as role');
        $this->db->from('user u');
        $this->db->join('branch_xref_user b','b.user_id=u.user_id');
        $this->db->join('employee e','e.user_id=u.user_id');
        $this->db->join('user_xref_role uxr','uxr.fk_user_id=u.user_id');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=uxr.fk_role_id');
        $this->db->where('u.is_active',1);
        $this->db->where('rtv.`value`!=','Superadmin');
        $this->db->where('b.branch_id',$param['branchId']);
        $query_users=$this->db->get();
        if($query_users){
            if ($query_users->num_rows() > 0)
            {
                $res=$query_users->result();
                foreach($res as $k_users=>$v_users)
                {
                    $previousCallsCnt=0;
                    $inHandCallsCnt=0;
                    $params='';
                    $params['type'] = $param['type'];
                    $params['user_id']=$v_users->user_id;
                    $params['branchId']=$param['branchId'];
                    //$params['fromDate']=$param['fromDate'];
                    $params['toDate']=$param['toDate'];
                    $previousCallsCnt=$this->getPreviousCallsCount($params);
                    $inHandCallsCnt=$this->getInHandCallsCount($params);
                    $v_users->previousCallsCount=$previousCallsCnt;
                    $v_users->inHandCallsCount=$inHandCallsCnt;
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'No Data Found',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    function getColdCallingCount($param){
        $cnt=0;
        //$available="select l.lead_id from lead l,branch b where l.city_id=b.fk_city_id and b.branch_id='".$param['branchId']."' and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";
        //$available="select l.lead_id from lead l where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";
        //$available="select l.lead_id from lead l where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";
        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."')) OR l.fk_branch_id='".$param['branchId']."')";
        $available="select l.lead_id from lead l where $city_condition and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";
        $res=$this->db->query($available);
        if($res){
            $cnt=$res->num_rows();
        }
        else{

        }
        $db_response=array("status"=>true,"message"=>'success',"data"=>array('count'=>$cnt));
        return $db_response;

    }
    function getLeadColdCallingCount($param)
    {
        $this->db->select("lead_stage_id,lead_stage");
        $this->db->from('lead_stage');
        //$this->db->where('is_hidden',0);
        $this->db->where('is_current',1);
        $this->db->where('lead_stage!=','M7');
        $this->db->order_by('order','asc');
        $res_leadcoldcalls=$this->db->get();
        $result_leadcoldcalls=$res_leadcoldcalls->result();
        $leadStages='';
        $cnt=array();
        //$fromDate=$param['fromDate'];
        $toDate=$param['toDate'];
        $toDate=date('Y-m-d',strtotime($toDate.' +1 day'));
        //$date_condition=" AND ((l.next_followup_date between '$fromDate' AND '$toDate') OR (l.expected_visit_date between '$fromDate' AND '$toDate') OR (l.expected_enroll_date between '$fromDate' AND '$toDate')) ";
        $date_condition=" AND ((bxl.next_followup_date < '".$toDate."') OR (bxl.expected_visit_date < '".$toDate."') OR (bxl.expected_enroll_date < '".$toDate."')) ";
        foreach($result_leadcoldcalls as $k_leadcoldcalls=>$v_leadcoldcalls)
        {
            $leadStages[]=array("stage_id"=>$v_leadcoldcalls->lead_stage_id,"stage"=>$v_leadcoldcalls->lead_stage);
            //$avail_sel_query="select count(l.branch_xref_lead_id) cnt from branch_xref_lead  where l.branch_id='".$param['branchId']."' and l.status=".$v_leadcoldcalls->lead_stage_id." and l.is_active=1 and l.branch_xref_lead_id not in (select fk_branch_xref_lead_id from lead_user_xref_lead_due where fk_lead_stage_id=".$v_leadcoldcalls->lead_stage_id.") $date_condition";
            $avail_sel_query="select count(bxl.branch_xref_lead_id) cnt from branch_xref_lead bxl
                                JOIN lead l ON bxl.lead_id = l.lead_id
                                JOIN reference_type_value rtv ON l.fk_contact_type_id = rtv.reference_type_value_id
                                where bxl.branch_id='".$param['branchId']."' and bxl.status=".$v_leadcoldcalls->lead_stage_id." AND rtv.`value`='".$param['type']."' and bxl.is_active=1 and
                                bxl.branch_xref_lead_id not in
                                (select fk_branch_xref_lead_id from lead_user_xref_lead_due where fk_lead_stage_id=".$v_leadcoldcalls->lead_stage_id.") $date_condition";
            $cnt_leadcall_query=$this->db->query($avail_sel_query);
            if($cnt_leadcall_query)
            {
                $cnt_leadcall_query_res=$cnt_leadcall_query->result();
                foreach($cnt_leadcall_query_res as $k_result_lead_count=>$v_result_lead_count)
                {
                    $cnt[$v_leadcoldcalls->lead_stage_id]=$v_result_lead_count->cnt;
                }
            }
            else
            {
                $cnt[$v_leadcoldcalls->lead_stage_id]=0;
            }
        }

        $data['stages']=$leadStages;
        $data['stageCounts']=$cnt;

        // old batches //
        $res_leadcoldcalls='';
        $this->db->select("lead_stage_id,lead_stage");
        $this->db->from('lead_stage');
        $this->db->where('is_current=',0);
        $this->db->order_by('order','asc');
        //$this->db->where('is_hidden=',0);
        $res_leadcoldcalls=$this->db->get();
        $result_leadcoldcalls=$res_leadcoldcalls->result();
        $leadStages='';
        $cnt=array();

        foreach($result_leadcoldcalls as $k_leadcoldcalls=>$v_leadcoldcalls)
        {
            $leadStages[]=array("stage_id"=>$v_leadcoldcalls->lead_stage_id,"stage"=>$v_leadcoldcalls->lead_stage);
            /*$this->db->select('count(bxl.branch_xref_lead_id) cnt');
            $this->db->from('branch_xref_lead bxl');
            $this->db->where('bxl.branch_id',$param['branchId']);
            $this->db->where('bxl.status',$v_leadcoldcalls->lead_stage_id);
            $this->db->where('bxl.is_active',1);*/

            //$avail_sel_query="select count(l.branch_xref_lead_id) cnt from branch_xref_lead l where l.branch_id='".$param['branchId']."' and l.status=".$v_leadcoldcalls->lead_stage_id." and l.is_active=1 and l.branch_xref_lead_id not in (select fk_branch_xref_lead_id from lead_user_xref_lead_due where fk_lead_stage_id=".$v_leadcoldcalls->lead_stage_id.") $date_condition";
            $avail_sel_query="select count(bxl.branch_xref_lead_id) cnt from branch_xref_lead bxl
                                JOIN lead l ON bxl.lead_id = l.lead_id
                                JOIN reference_type_value rtv ON l.fk_contact_type_id = rtv.reference_type_value_id
                                where bxl.branch_id='".$param['branchId']."' and bxl.status=".$v_leadcoldcalls->lead_stage_id." AND rtv.`value`='".$param['type']."' and bxl.is_active=1 and
                                bxl.branch_xref_lead_id not in
                                (select fk_branch_xref_lead_id from lead_user_xref_lead_due where fk_lead_stage_id=".$v_leadcoldcalls->lead_stage_id.") $date_condition";
            $cnt_leadcall_query=$this->db->query($avail_sel_query);
            if($cnt_leadcall_query)
            {
                $cnt_leadcall_query_res=$cnt_leadcall_query->result();
                foreach($cnt_leadcall_query_res as $k_result_lead_count=>$v_result_lead_count)
                {
                    $cnt[$v_leadcoldcalls->lead_stage_id]=$v_result_lead_count->cnt;
                }
            }
            else
            {
                $cnt[$v_leadcoldcalls->lead_stage_id]=0;
            }
        }
        $data['Oldstages']=$leadStages;
        $data['OldstageCounts']=$cnt;

        $db_response=array("status"=>true,"message"=>'success',"data"=>array('count'=>$data));
        return $db_response;
    }
    function getPreviousCallsCount($params)
    {
        $cnt=0;
        $cnt_lead=0;
        //$fromDate=$params['fromDate'];
        $toDate=$params['toDate'];
        $toDate=date('Y-m-d',strtotime($toDate.' +1 day'));
        //$date_condition=" AND ((l.next_followup_date between '$fromDate' AND '$toDate') OR (l.expected_visit_date between '$fromDate' AND '$toDate') OR (l.expected_enroll_date between '$fromDate' AND '$toDate')) ";
        $date_condition=" AND ((bxl.next_followup_date < '".$toDate."') OR (bxl.expected_visit_date < '".$toDate."') OR (bxl.expected_enroll_date < '".$toDate."')) ";

        //$city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."')) OR l.fk_branch_id='".$params['branchId']."')";
        //$available="select  l.lead_id from lead l,user_xref_lead_history uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];
        //$available="select  l.lead_id from lead l,user_xref_lead_history uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];

        //echo $available_lead="select  l.branch_xref_lead_id from branch_xref_lead l,lead_user_xref_lead_history uxl where l.branch_id='".$params['branchId']."' and l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=".$params['user_id']." $date_condition";

        $available_lead="SELECT bxl.branch_xref_lead_id
                                FROM branch_xref_lead bxl
                                JOIN lead_user_xref_lead_history uxh ON bxl.branch_xref_lead_id = uxh.fk_branch_xref_lead_id
                                JOIN lead l ON bxl.lead_id = l.lead_id
                                LEFT JOIN reference_type_value rtv ON rtv.reference_type_value_id = l.fk_contact_type_id
                                WHERE rtv.`value` = '".$params['type']."' AND bxl.branch_id='".$params['branchId']."' AND uxh.fk_user_id = ".$params['user_id']."  ". $date_condition. " GROUP BY bxl.branch_xref_lead_id";
        $res_lead=$this->db->query($available_lead);
        if($res_lead)
        {
            $cnt_lead=$res_lead->num_rows();
        }
        else
        {

        }
        return ($cnt_lead);
    }
    function getInHandCallsCount($params)
    {
        $cnt=0;
        $cnt_lead=0;
        //$fromDate=$params['fromDate'];
        $toDate=$params['toDate'];
        $toDate=date('Y-m-d',strtotime($toDate.' +1 day'));
        //$date_condition=" AND ((l.next_followup_date between '$fromDate' AND '$toDate') OR (l.expected_visit_date between '$fromDate' AND '$toDate') OR (l.expected_enroll_date between '$fromDate' AND '$toDate')) ";
        $date_condition=" AND ((bxl.next_followup_date < '".$toDate."') OR (bxl.expected_visit_date < '".$toDate."') OR (bxl.expected_enroll_date < '".$toDate."')) ";
        //$city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."')) OR l.fk_branch_id='".$params['branchId']."')";
        //$date_condition='';
        //$available="select  l.lead_id from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];
        //$available="select  l.lead_id from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$params['user_id'];

        //$available_lead="select l.branch_xref_lead_id from branch_xref_lead l,lead_user_xref_lead_due uxl where l.branch_id='".$params['branchId']."' and l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=".$params['user_id']." $date_condition";
        $available_lead="SELECT bxl.branch_xref_lead_id
                                FROM branch_xref_lead bxl
                                JOIN lead_user_xref_lead_due uxh ON bxl.branch_xref_lead_id = uxh.fk_branch_xref_lead_id
                                JOIN lead l ON bxl.lead_id = l.lead_id
                                LEFT JOIN reference_type_value rtv ON rtv.reference_type_value_id = l.fk_contact_type_id
                                WHERE rtv.`value` = '".$params['type']."' AND bxl.branch_id='".$params['branchId']."' AND uxh.fk_user_id = ".$params['user_id']."  ". $date_condition. " GROUP BY bxl.branch_xref_lead_id";
        $res_lead=$this->db->query($available_lead);
        if($res_lead){
            $cnt_lead=$res_lead->num_rows();
        }
        else{

        }

        return ($cnt_lead);

    }
    function addCallSchedule($params){
        $assignedBy=$params['createdBy'];
        $leadCallSchedules=$params['leadCallSchedules'];
        //$fromDate=$params['fromDate'];
        $toDate=$params['toDate'];
        $toDate=date('Y-m-d',strtotime($toDate.' +1 day'));
        //$date_condition=" AND ((l.next_followup_date between '$fromDate' AND '$toDate') OR (l.expected_visit_date between '$fromDate' AND '$toDate') OR (l.expected_enroll_date between '$fromDate' AND '$toDate')) ";
        $date_condition=" AND ((l.next_followup_date < '$toDate') OR (l.expected_visit_date < '$toDate') OR (l.expected_enroll_date < '$toDate')) ";


        // [leadCallSchedules] => 14|9-1^14|11-2
        if($leadCallSchedules!='') {
            $leadCallSchedules_explode = explode('^', $leadCallSchedules);
            for ($i = 0; $i < count($leadCallSchedules_explode); $i++) {
                $each_callSchedule = $leadCallSchedules_explode[$i];
                $eah_callSchedule_explode = explode('-', $each_callSchedule);

                $eah_callSchedule_explode2 = explode('|', $eah_callSchedule_explode[0]);
                $leadStageId = $eah_callSchedule_explode2[0];
                $assignedTo = $eah_callSchedule_explode2[1];
                $totalAssignedCnt = $eah_callSchedule_explode[1];
                if ($totalAssignedCnt > 0) {
                    $cnt = 0;
                    $data_insert_head = array("fk_lead_stage_id" => $leadStageId, "total_scheduled_calls" => $totalAssignedCnt, "assigned_to" => $assignedTo, "assigned_on" => date('Y-m-d H:i:s'), "assigned_by" => $assignedBy);
                    $res_insert_head = $this->db->insert('lead_call_schedule_head', $data_insert_head);
                    if ($res_insert_head) {
                        $head_id = $this->db->insert_id();
                        //$available = "select l.lead_id from lead l,branch b where l.city_id=b.fk_city_id and b.branch_id='" . $params['branchId'] . "' and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;
                        //$available = "select l.branch_xref_lead_id,l.lead_id from branch_xref_lead l where l.branch_id='" . $params['branchId'] . "' AND l.status=" . $leadStageId . " AND l.is_active=1 AND l.branch_xref_lead_id not in (select fk_branch_xref_lead_id from lead_user_xref_lead_due where fk_lead_stage_id=" . $leadStageId . ") $date_condition ORDER BY l.branch_xref_lead_id asc LIMIT " . $totalAssignedCnt;exit;
                        $available = "select
                                        bxl.branch_xref_lead_id,bxl.lead_id
                                        from branch_xref_lead bxl
                                        JOIN lead l ON bxl.lead_id = l.lead_id
                                        JOIN reference_type_value rtvtype ON rtvtype.reference_type_value_id = l.fk_contact_type_id
                                        where
                                        bxl.branch_id='" . $params['branchId'] . "' AND bxl.status=" . $leadStageId . " AND bxl.is_active=1 AND rtvtype.`value` = '" . $params['type'] . "'
                                        AND bxl.branch_xref_lead_id
                                        not in (select fk_branch_xref_lead_id from lead_user_xref_lead_due where fk_lead_stage_id=" . $leadStageId . ")". $date_condition ."ORDER BY bxl.branch_xref_lead_id asc LIMIT " . $totalAssignedCnt;
                        $res = $this->db->query($available);
                        if ($res)
                        {
                            $cnt = $res->num_rows();
                            $result = $res->result();
                            $data_insert_calls = '';
                            foreach ($result as $k_r => $v_r) {
                                $data_insert_calls[] = array("fk_branch_xref_lead_id" => $v_r->branch_xref_lead_id, "fk_user_id" => $assignedTo, "fk_lead_id" => $v_r->lead_id, "fk_lead_stage_id" => $leadStageId, "fk_lead_call_schedule_head_id" => $head_id);
                            }
                            if (count($data_insert_calls) > 0)
                            {
                                $res_insert_calls = $this->db->insert_batch('lead_user_xref_lead_due', $data_insert_calls);
                            }
                        }
                        else
                        {

                        }
                    }
                }

            }
        }
        $db_response=array("status"=>true,"message"=>'success',"data"=>array("message"=>'success'));
        return $db_response;
    }
    function releaseCallSchedule($param)
    {
        //-----------------//
        $status=TRUE;$message='success';$data=array();
        $data_release_head=array();
        $tobededucted=array();
        //$fromDate=$param['fromDate'];
        $toDate=$param['toDate'];
        $toDate=date('Y-m-d',strtotime($toDate.' +1 day'));
        //$date_condition=" AND ((l.next_followup_date between '$fromDate' AND '$toDate') OR (l.expected_visit_date between '$fromDate' AND '$toDate') OR (l.expected_enroll_date between '$fromDate' AND '$toDate')) ";
        $date_condition=" AND ((bxl.next_followup_date < '".$toDate."') OR (bxl.expected_visit_date < '".$toDate."') OR (bxl.expected_enroll_date < '".$toDate."')) ";
        //$availableTotal="select  count(l.branch_xref_lead_id) as availCnt from branch_xref_lead l,lead_user_xref_lead_due uxl where l.branch_id='".$param['branchId']."' and l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=".$param['assignedUserId']." $date_condition ";
        $availableTotal="select
                        count(bxl.branch_xref_lead_id) as availCnt
                        from branch_xref_lead bxl
                        JOIN lead_user_xref_lead_due uxl ON bxl.branch_xref_lead_id = uxl.fk_branch_xref_lead_id
                        JOIN lead l ON bxl.lead_id = l.lead_id
                        JOIN reference_type_value rtvtype ON l.fk_contact_type_id = rtvtype.reference_type_value_id
                        where bxl.branch_id='".$param['branchId']."' and uxl.fk_user_id=".$param['assignedUserId']." AND rtvtype.`value`='".$param['type']."'". $date_condition;
        $resTotal=$this->db->query($availableTotal);
        if($resTotal)
        {
            $release = 0;
            $resultTotal = $resTotal->result();
            foreach ($resultTotal as $kTotal => $vTotal)
            {
                $release = $vTotal->availCnt;
            }
            if($param['releaseCnt'] > $release)
            {
                $status = FALSE;
                $message = 'Cannot release more than assigned calls';
                $data = array("message" => 'Cannot release more than assigned calls');
            }
            else
            {
                $release = $param['releaseCnt'];
                if($release>0)
                {
                    //echo $available = "select uxl.fk_lead_call_schedule_head_id,count(l.branch_xref_lead_id) as availCnt from branch_xref_lead l,lead_user_xref_lead_due uxl where l.branch_id='" . $param['branchId'] . "' and l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=" . $param['assignedUserId'] . " $date_condition group by uxl.fk_lead_call_schedule_head_id";exit;
                    $available = "select
                                uxl.fk_lead_call_schedule_head_id,count(bxl.branch_xref_lead_id) as availCnt
                                from branch_xref_lead bxl
                                JOIN lead_user_xref_lead_due uxl ON bxl.branch_xref_lead_id = uxl.fk_branch_xref_lead_id
                                JOIN lead l ON bxl.lead_id = l.lead_id
                                JOIN reference_type_value rtvtype ON l.fk_contact_type_id = rtvtype.reference_type_value_id
                                where bxl.branch_id='" . $param['branchId'] . "' AND rtvtype.`value`='" . $param['type'] . "' and uxl.fk_user_id=" . $param['assignedUserId'] . $date_condition." group by uxl.fk_lead_call_schedule_head_id";
                    $res = $this->db->query($available);
                    if ($res)
                    {
                        $result = $res->result();
                        foreach ($result as $k => $v)
                        {
                            $headId = $v->fk_lead_call_schedule_head_id;
                            $availCnt = $v->availCnt;
                            if ($availCnt < $release)
                            {
                                $tobededucted[] = array("headId" => $headId, "releaseCount" => $availCnt);
                                $release = $release - $availCnt;
                            }
                            elseif ($availCnt > $release)
                            {
                                $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                                $release = $release - $release;
                            }
                            elseif ($availCnt == $release)
                            {
                                $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                                $release = $release - $release;
                            }
                            else
                            {

                            }
                            if ($release == 0)
                            {
                                break;
                            }
                        }
                        if (count($tobededucted) > 0)
                        {
                            for ($i = 0; $i < count($tobededucted); $i++)
                            {
                                $callHeadId = $tobededucted[$i]['headId'];
                                $releaseCount = $tobededucted[$i]['releaseCount'];

                                $data_release_head['release_count'] = $releaseCount;
                                $data_release_head['released_by'] = $param['releaseBy'];
                                $data_release_head['released_on'] = date('Y-m-d H:i:s');
                                $data_release_head['fk_lead_call_schedule_head_id'] = $callHeadId;
                                $insert_release_head = $this->db->insert('lead_user_xref_lead_release_head', $data_release_head);
                                if ($insert_release_head)
                                {
                                    //release head inserted successfully.
                                    $release_head_id = $this->db->insert_id();
                                    //$available="select  l.lead_id from lead l,branch b,user_xref_lead_due uxl where l.city_id=b.fk_city_id and b.branch_id=".$param['branchId']." and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." and uxl.call_schedule_head_id=".$callHeadId." order by l.lead_id asc limit ".$releaseCount;
                                    //$available = "select  l.lead_id,uxl.fk_branch_xref_lead_id from branch_xref_lead l,lead_user_xref_lead_due uxl where l.branch_id='" . $param['branchId'] . "' and l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=" . $param['assignedUserId'] . " and uxl.fk_lead_call_schedule_head_id=" . $callHeadId . " $date_condition order by l.branch_xref_lead_id asc limit " . $releaseCount;
                                    $available="select
                                                bxl.lead_id,uxl.fk_branch_xref_lead_id
                                                from branch_xref_lead bxl
                                                JOIN lead_user_xref_lead_due uxl ON bxl.branch_xref_lead_id = uxl.fk_branch_xref_lead_id
                                                JOIN lead l ON bxl.lead_id = l.lead_id
                                                JOIN reference_type_value rtvtype ON l.fk_contact_type_id = rtvtype.reference_type_value_id
                                                where bxl.branch_id='" . $param['branchId'] . "' AND rtvtype.`value`='" . $param['type'] . "' and uxl.fk_user_id=" . $param['assignedUserId'] ."and uxl.fk_lead_call_schedule_head_id=" . $callHeadId." ".$date_condition." order by bxl.branch_xref_lead_id asc limit " . $releaseCount;
                                    $select_lead_ids = $this->db->query($available);

                                    if ($select_lead_ids)
                                    {
                                        //leads fetched successfully
                                        $result_sel_leadIds = $select_lead_ids->result();
                                        $data_insert_releaseCalls = '';
                                        $data_release_leads = '';
                                        foreach ($result_sel_leadIds as $result_sel_leadIds_k => $result_sel_leadIds_v)
                                        {
                                            $data_insert_releaseCalls[] = array("fk_user_id" => $param['assignedUserId'], "fk_branch_xref_lead_id" => $result_sel_leadIds_v->fk_branch_xref_lead_id, "fk_lead_id" => $result_sel_leadIds_v->lead_id, "fk_lead_call_schedule_head_id" => $callHeadId, "fk_lead_user_xref_lead_release_head_id" => $release_head_id);
                                            $data_release_leads[] = $result_sel_leadIds_v->fk_branch_xref_lead_id;
                                        }
                                        if ($data_insert_releaseCalls != '')
                                        {
                                            //release calls found
                                            $insert_release_calls = $this->db->insert_batch('lead_user_xref_lead_release', $data_insert_releaseCalls);
                                            if ($insert_release_calls) {
                                                //release calls successfully inserted
                                                $data_release_leads = array_map('intval', $data_release_leads);
                                                $data_release_leads = implode(",", $data_release_leads);

                                                /*$this->db->where_in('lead_id', $data_release_leads);
                                                $this->db->where('call_schedule_head_id', $callHeadId);
                                                $this->db->where('user_id', $param['assignedUserId']);
                                                $delete_due_calls=$this->db->delete('user_xref_lead_due');*/
                                                $delete_due_calls_query = "DELETE FROM lead_user_xref_lead_due WHERE fk_lead_call_schedule_head_id=$callHeadId AND fk_user_id=" . $param['assignedUserId'] . " AND fk_branch_xref_lead_id in ($data_release_leads)";
                                                $delete_due_calls = $this->db->query($delete_due_calls_query);
                                                if ($delete_due_calls) {
                                                    //calls deleted successfully
                                                    $this->db->where('lead_call_schedule_head_id', $callHeadId);
                                                    $this->db->where('assigned_to', $param['assignedUserId']);
                                                    $this->db->set('total_scheduled_calls', 'total_scheduled_calls-' . $releaseCount, FALSE);
                                                    $update_call_head = $this->db->update('lead_call_schedule_head');
                                                    if ($update_call_head) {
                                                        //successfully updated call head counts
                                                        $status = TRUE;
                                                        $message = 'Released successfully';
                                                        $data = array();

                                                    } else {
                                                        //problem in updating count of call head
                                                        $error = $this->db->error();
                                                        $status = FALSE;
                                                        $message = 'problem in updating count of call head';
                                                        $data = array("message" => $error['message']);
                                                    }
                                                } else {
                                                    //problem in deleting calls from dues
                                                    $error = $this->db->error();
                                                    $status = FALSE;
                                                    $message = 'problem in deleting calls from dues';
                                                    $data = array("message" => $error['message']);
                                                }

                                            } else {
                                                //problem in batch insert of release calls
                                                $error = $this->db->error();
                                                $status = FALSE;
                                                $message = 'problem in batch insert of release calls';
                                                $data = array("message" => $error['message']);
                                            }
                                        }
                                        else
                                        {
                                            //no release calls found
                                            $error = $this->db->error();
                                            $status = FALSE;
                                            $message = 'no release calls found';
                                            $data = array("message" => $error['message']);
                                        }
                                    }
                                    else
                                    {
                                        //problem in fetching leads for given head
                                        $error = $this->db->error();
                                        $status = FALSE;
                                        $message = 'problem in fetching leads for given head';
                                        $data = array("message" => $error['message']);
                                    }
                                }
                                else
                                {
                                    //problem in insertion of relese head id
                                    $error = $this->db->error();
                                    $status = FALSE;
                                    $message = 'problem in insertion of release head id';
                                    $data = array("message" => $error['message']);
                                }
                            }
                            $multi_db_response[] = array("status" => $status, "message" => $message, "data" => $data);
                        } else {
                            //no availabled heads found
                            $error = $this->db->error();
                            $status = FALSE;
                            $message = 'no available heads found';
                            $data = array("message" => $error['message']);
                        }
                    } else {
                        //problem in fetching the available lead heads
                        $error = $this->db->error();
                        $status = FALSE;
                        $message = 'problem in fetching the available lead heads';
                        $data = array("message" => $error['message']);
                    }
                }
                else{
                    $status = FALSE;
                    $message = 'No release entries found';
                    $data = array("message" =>'No release entries found');
                }
            }
        }
        else
        {
            $error = $this->db->error();
            $status = FALSE;
            $message = 'problem in fetching the total available leads';
            $data = array("message" => $error['message']);
        }

        if($status == false)
        {
            $db_response=array("status"=>$status,"message"=>$message,"data"=>$data);
        }
        else
        {
            $message="Released successfully";
            $multi_db_response=array("message"=>$message);
            $db_response=array("status"=>TRUE,"message"=>$message,"data"=>$multi_db_response);
        }
        return $db_response;

    }
    function releaseCallScheduleByUser($param){

        //-----------------//
        $status=TRUE;$message='success';$data=array();
        $data_release_head=array();
        $tobededucted=array();
        $availableTotal="select  count(l.branch_xref_lead_id) as availCnt from branch_xref_lead l,lead_user_xref_lead_due uxl where l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=".$param['assignedUserId']." ";
        $resTotal=$this->db->query($availableTotal);
        if($resTotal) {
            $release = 0;
            $resultTotal = $resTotal->result();
            foreach ($resultTotal as $kTotal => $vTotal) {
                $release = $vTotal->availCnt;
            }

            if($release>0) {
                $available = "select  uxl.fk_lead_call_schedule_head_id,count(l.branch_xref_lead_id) as availCnt from branch_xref_lead l,lead_user_xref_lead_due uxl where  l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=" . $param['assignedUserId'] . " group by uxl.fk_lead_call_schedule_head_id";
                $res = $this->db->query($available);
                if ($res) {
                    $result = $res->result();
                    foreach ($result as $k => $v) {
                        $headId = $v->fk_lead_call_schedule_head_id;
                        $availCnt = $v->availCnt;

                        if ($availCnt < $release) {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $availCnt);
                            $release = $release - $availCnt;
                        } elseif ($availCnt > $release) {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                            $release = $release - $release;
                        } elseif ($availCnt == $release) {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                            $release = $release - $release;
                        } else {

                        }
                        if ($release == 0) {
                            break;
                        }
                    }
                    if (count($tobededucted) > 0) {
                        for ($i = 0; $i < count($tobededucted); $i++) {

                            $callHeadId = $tobededucted[$i]['headId'];
                            $releaseCount = $tobededucted[$i]['releaseCount'];

                            $data_release_head['release_count'] = $releaseCount;
                            $data_release_head['released_by'] = $param['releaseBy'];
                            $data_release_head['released_on'] = date('Y-m-d H:i:s');
                            $data_release_head['fk_lead_call_schedule_head_id'] = $callHeadId;
                            $insert_release_head = $this->db->insert('lead_user_xref_lead_release_head', $data_release_head);
                            if ($insert_release_head) {
                                //release head inserted successfully.
                                $release_head_id = $this->db->insert_id();
                                //$available="select  l.lead_id from lead l,branch b,user_xref_lead_due uxl where l.city_id=b.fk_city_id and b.branch_id=".$param['branchId']." and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." and uxl.call_schedule_head_id=".$callHeadId." order by l.lead_id asc limit ".$releaseCount;
                                $available = "select  l.lead_id,uxl.fk_branch_xref_lead_id from branch_xref_lead l,lead_user_xref_lead_due uxl where  l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=" . $param['assignedUserId'] . " and uxl.fk_lead_call_schedule_head_id=" . $callHeadId . " order by l.branch_xref_lead_id asc limit " . $releaseCount;
                                $select_lead_ids = $this->db->query($available);

                                if ($select_lead_ids) {
                                    //leads fetched successfully
                                    $result_sel_leadIds = $select_lead_ids->result();
                                    $data_insert_releaseCalls = '';
                                    $data_release_leads = '';
                                    foreach ($result_sel_leadIds as $result_sel_leadIds_k => $result_sel_leadIds_v) {
                                        $data_insert_releaseCalls[] = array("fk_user_id" => $param['assignedUserId'], "fk_branch_xref_lead_id" => $result_sel_leadIds_v->fk_branch_xref_lead_id, "fk_lead_id" => $result_sel_leadIds_v->lead_id, "fk_lead_call_schedule_head_id" => $callHeadId, "fk_lead_user_xref_lead_release_head_id" => $release_head_id);
                                        $data_release_leads[] = $result_sel_leadIds_v->fk_branch_xref_lead_id;
                                    }
                                    if ($data_insert_releaseCalls != '') {
                                        //release calls found
                                        $insert_release_calls = $this->db->insert_batch('lead_user_xref_lead_release', $data_insert_releaseCalls);
                                        if ($insert_release_calls) {
                                            //release calls successfully inserted
                                            $data_release_leads = array_map('intval', $data_release_leads);
                                            $data_release_leads = implode(",", $data_release_leads);

                                            /*$this->db->where_in('lead_id', $data_release_leads);
                                            $this->db->where('call_schedule_head_id', $callHeadId);
                                            $this->db->where('user_id', $param['assignedUserId']);
                                            $delete_due_calls=$this->db->delete('user_xref_lead_due');*/
                                            $delete_due_calls_query = "DELETE FROM lead_user_xref_lead_due WHERE fk_lead_call_schedule_head_id=$callHeadId AND fk_user_id=" . $param['assignedUserId'] . " AND fk_branch_xref_lead_id in ($data_release_leads)";
                                            $delete_due_calls = $this->db->query($delete_due_calls_query);
                                            if ($delete_due_calls) {
                                                //calls deleted successfully
                                                $this->db->where('lead_call_schedule_head_id', $callHeadId);
                                                $this->db->where('assigned_to', $param['assignedUserId']);
                                                $this->db->set('total_scheduled_calls', 'total_scheduled_calls-' . $releaseCount, FALSE);
                                                $update_call_head = $this->db->update('lead_call_schedule_head');
                                                if ($update_call_head) {
                                                    //successfully updated call head counts
                                                    $status = TRUE;
                                                    $message = 'Released successfully';
                                                    $data = array();

                                                } else {
                                                    //problem in updating count of call head
                                                    $error = $this->db->error();
                                                    $status = FALSE;
                                                    $message = 'problem in updating count of call head';
                                                    $data = array("message" => $error['message']);
                                                }
                                            } else {
                                                //problem in deleting calls from dues
                                                $error = $this->db->error();
                                                $status = FALSE;
                                                $message = 'problem in deleting calls from dues';
                                                $data = array("message" => $error['message']);
                                            }

                                        } else {
                                            //problem in batch insert of release calls
                                            $error = $this->db->error();
                                            $status = FALSE;
                                            $message = 'problem in batch insert of release calls';
                                            $data = array("message" => $error['message']);
                                        }
                                    } else {
                                        //no release calls found
                                        $error = $this->db->error();
                                        $status = FALSE;
                                        $message = 'no release calls found';
                                        $data = array("message" => $error['message']);
                                    }
                                } else {
                                    //problem in fetching leads for given head
                                    $error = $this->db->error();
                                    $status = FALSE;
                                    $message = 'problem in fetching leads for given head';
                                    $data = array("message" => $error['message']);
                                }

                            } else {
                                //problem in insertion of relese head id
                                $error = $this->db->error();
                                $status = FALSE;
                                $message = 'problem in insertion of release head id';
                                $data = array("message" => $error['message']);
                            }
                        }
                        $multi_db_response[] = array("status" => $status, "message" => $message, "data" => $data);
                    } else {
                        //no availabled heads found
                        $error = $this->db->error();
                        $status = FALSE;
                        $message = 'no available heads found';
                        $data = array("message" => $error['message']);
                    }
                } else {
                    //problem in fetching the available lead heads
                    $error = $this->db->error();
                    $status = FALSE;
                    $message = 'problem in fetching the available lead heads';
                    $data = array("message" => $error['message']);
                }
            }
            else{
                $status = FALSE;
                $message = 'No release entries found';
                $data = array("message" =>'No release entries found');
            }
        }
        else{
            $error = $this->db->error();
            $status = FALSE;
            $message = 'problem in fetching the total available leads';
            $data = array("message" => $error['message']);
        }
        $message="Released successfully";
        $multi_db_response=array("message"=>$message);
        $db_response=array("status"=>TRUE,"message"=>$message,"data"=>$multi_db_response);
        return $db_response;

    }
}