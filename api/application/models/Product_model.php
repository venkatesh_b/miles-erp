<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-09
 * Time: 05:18 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');

    }
    public function checkProductCodeExist($code='',$productId='')
    {
        $this->db->select('product_id');
        $this->db->from('product');
        $this->db->where('code',$code);
        if(isset($productId) && $productId!='' && $productId>0)
            $this->db->where('product_id !=',$productId);
        $query = $this->db->get();
        if($query)
        {
            if($query->num_rows()>0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
            return false;
    }
    public function addProduct($data='')
    {
        $query = $this->checkProductCodeExist($data['code'],0);
            if($query===false)
            {
                $db_response=array("status"=>false,"message"=>'Code already exist',"data"=>array('code'=>'Code already used'));
            }
            else
            {
                $res_insert=$this->db->insert('product', $data);
                if($res_insert)
                {
                    $db_response=array("status"=>true,"message"=>'Product saved successfully',"data"=>array());
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
        return $db_response;
    }
    public function checkProductExist($params)
    {
        $this->db->select("product_id");
        $this->db->from("product");
        $this->db->where("product_id", $params['productId']);
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid Product',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getProductDetailsById($data)
    {
        $this->db->select("product_id,name,code,version,description,fk_product_type_id,fk_publication_id");
        $this->db->from("product");
        $this->db->where("product_id", $data['productId']);
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            if(count($list) > 0)
            {
                $db_response=array("status"=>true,"message"=>"success","data"=>$list);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function updateProduct($data='')
    {
        $query = $this->checkProductCodeExist($data['code'],$data['product_id']);
            if($query===false)
            {
                $db_response=array("status"=>false,"message"=>'Code already exist',"data"=>array('code'=>'Code already used'));
            }
            else
            {
                $modify_data = array
                (
                    'name'                  =>      $data['name'],
                    'fk_product_type_id'    =>      $data['fk_product_type_id'],
                    'fk_publication_id'     =>      $data['fk_publication_id'],
                    'code'                  =>      $data['code'],
                    'version'               =>      $data['version'],
                    'description'           =>      $data['description'],
                    'updated_by'            =>      $_SERVER['HTTP_USER'],
                    'updated_on'            =>      date('Y-m-d H:i:s')
                );
                $this->db->where('product_id', $data['product_id']);
                $res_update=$this->db->update('product', $modify_data);
                if($res_update)
                {
                    $db_response=array("status"=>true,"message"=>'Product saved successfully',"data"=>[]);
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>[]);
                }
            }
        return $db_response;
    }
    public function deleteProduct($data)
    {
        $productId=$data['productId'];
        $this->db->select('product_xref_course_id');
        $this->db->from('product_xref_course');
        $this->db->where('fk_product_id',$productId);
        $res=$this->db->get();
        $status='';$message='';
        if($res)
        {
            if($res->num_rows()>0)
            {
                $status=TRUE;
                $message="Product cannot be deleted, already mapped to course";
            }
            else
            {
                $this->db->where('product_id',$productId);
                $del_res = $this->db->delete('product');
                if($del_res)
                {
                    $status=TRUE;
                    $message="Deleted successfully";
                }
                else
                {
                    $error = $this->db->error();
                    $status=FALSE;
                    $message=$error['message'];
                }
            }
        }
        else
        {
            $error = $this->db->error();
            $status=FALSE;
            $message=$error['message'];
        }
        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function productDataTables()
    {
        $table = 'product';
        $primaryKey = 'p`.`product_id';
        $columns = array(
            array( 'db' => '`p`.`name`',                        'dt' => 'name',             'field' => 'name' ),
            array( 'db' => '`rtv`.`value` as `product_type`',   'dt' => 'product_type',     'field' => 'product_type'),
            array( 'db' => '`p`.`code`',                        'dt' => 'code',             'field' => 'code' ),
            array( 'db' => '`rtv1`.`value` as `publication`',   'dt' => 'publication',      'field' => 'publication'),
            array( 'db' => 'p.version',                     'dt' => 'version',      'field' => 'version' ),
           /* array( 'db' => 'p.description',                     'dt' => 'description',      'field' => 'description' ),*/
            array( 'db' => '`p`.`product_id`',                  'dt' => 'product_id',       'field' => 'product_id' )
        );
        $globalFilterColumns = array(
            array( 'db' => '`p`.`name`',            'dt' => 'name',             'field' => 'name' ),
            array( 'db' => '`rtv`.`value`',         'dt' => 'value',            'field' => 'value' ),
            array( 'db' => '`p`.`code`',            'dt' => 'code',             'field' => 'code' ),
            array( 'db' => '`rtv1`.`value`',        'dt' => 'value',            'field' => 'value' ),
            array( 'db' => '`p`.`version`',            'dt' => 'version',             'field' => 'version' )
            /*array( 'db' => 'p.description',         'dt' => 'description',      'field' => 'description' )*/
        );
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        $joinQuery = "FROM `product` AS `p` JOIN `reference_type_value` AS `rtv` ON (`rtv`.`reference_type_value_id` = `p`.`fk_product_type_id`) LEFT JOIN `reference_type_value` AS `rtv1` ON (`rtv1`.`reference_type_value_id` = `p`.`fk_publication_id`)  ";
        $extraWhere = "";
        $groupBy = "";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery,$extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['product_id']=encode($responseData['data'][$i]['product_id']);
        }
        return $responseData;
    }
}