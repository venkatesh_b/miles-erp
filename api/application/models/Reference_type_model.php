<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reference_type_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAllCountry($data) {
        $ref_type_id = $data;
        $this->db->select('tv.value as country, tv.reference_type_value_id as id');
        $this->db->from('reference_type t');
        $this->db->join('reference_type_value tv', 't.reference_type_id=tv.fk_reference_type_id', 'left');
        $this->db->where('t.reference_type_id', $data['reference_type_id']);
        $query1 = $this->db->get();

        if ($query1) {
            $r = $query1->result_array();

            $tmp = array();
            foreach ($r as $key => $val) {
                $this->db->select('value as name, reference_type_value_id as id');
                $this->db->from('reference_type_value');
                $this->db->where('parent_id', $val['id']);
                $query = $this->db->get();

                if ($query) {
//                    $tmp[] = $query->result_array();  /*for future use if any processing has to be done*/
                    $res_all = array('id' => $val['id'],
                        'name' => $val['country'],
                        'child' => $query->result_array());
                } else {
                    return 'dberror';
                }
            }

            if ($res_all) {
                return $res_all;
            } else {
                return false;
            }
        } else {
            return 'dberror';
        }
    }

    /**
     * Function for geting State details and child (City)
     * 
     * @args 
     *  "reference_type_id"(INT) -> id for State
     * 
     * @return - array
     * @date modified - 25 Jan 2016
     * @author : Sam
     */
    function getState($data) {
        $this->db->select('value as state, parent_id, reference_type_value_id as id');
        $this->db->from('reference_type_value');
        $this->db->where('fk_reference_type_id', '2');
        $this->db->where('reference_type_value_id', $data['reference_type_id']);
        $query = $this->db->get();

        if ($query) {
            $tmp = $query->result_array();
//            print_r($tmp);die;
            if ($tmp) {
                $this->db->select('value as name, reference_type_value_id as id');
                $this->db->from('reference_type_value');
                $this->db->where('fk_reference_type_id', '3');
                $this->db->where('parent_id', $tmp[0]['id']);
                $temp = $query = $this->db->get();
//                print_r($temp->result());
//                die;
                if ($query) {
                    if ($temp) {
                        $res_all = array('id' => $tmp[0]['parent_id'],
                            'name' => $tmp[0]['state'],
                            'child' => $query->result_array(),
                            'parent_link' => 'http://' . $_SERVER['HTTP_HOST'] . '/index.php/api/referencetype/country/?reference_type_id=1');
                        return $res_all;
                    } else {
                        return false;
                    }
                }else{
                    return 'dberror';
                }
            } else {
                return 'nostate';
            }
        } else {
            return 'dberror';
        }
    }

    /**
     * Function for geting City details
     * 
     * @args 
     *  "reference_type_id"(INT) -> id for City
     * 
     * @return - array
     * @date modified - 19 Jan 2016
     * @author : Sam
     */
    function getAllCity($data) {
//        print_r($data);die;
//        $this->db->select('value as name, reference_type_value_id as id');
        $this->db->select('value as state, is_active, parent_id, created_date');
        $this->db->from('reference_type_value');
        $this->db->where('fk_reference_type_id', '3');
        $this->db->where('reference_type_value_id', $data['reference_type_id']);
        $query = $this->db->get();
        if($query){
            $tmp = $query->result_array();
            if ($tmp) {
                array_push($tmp, array(
                    'parent_link' => 'http://' . $_SERVER['HTTP_HOST'] . '/index.php/api/referencetype/state/?reference_type_id=' . $tmp[0]['parent_id']
                ));
                return $tmp;
            } else {
                return false;
            }
        }else{
            return 'dberror';
        }
        
    }

}
