<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 15-03-2016
 * Time: 02:48 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class SRCallSchedule_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    function getStudentColdCallingCount($param)
    {
        $this->db->select("lead_stage_id,lead_stage");
        $this->db->from('lead_stage');
        $this->db->where('lead_stage=','M7');
        $this->db->order_by('order','asc');
        $res_leadcoldcalls=$this->db->get();
        $result_leadcoldcalls=$res_leadcoldcalls->result();
        $leadStages='';
        $cnt=array();
        //$toDate=$param['toDate'];
        $current_datetime=date('Y-m-d');

        //$date_condition=" AND ((bxl.next_followup_date < '".$toDate."') OR (bxl.expected_visit_date < '".$toDate."') OR (bxl.expected_enroll_date < '".$toDate."')) ";
        $date_condition=" AND (DATE(bxl.updated_on + INTERVAL 100 DAY) <= '".$current_datetime."') ";
        //$date_condition = "";
        foreach($result_leadcoldcalls as $k_leadcoldcalls=>$v_leadcoldcalls)
        {
            $leadStages[]=array("stage_id"=>$v_leadcoldcalls->lead_stage_id,"stage"=>$v_leadcoldcalls->lead_stage);

            $avail_sel_query="select count(bxl.branch_xref_lead_id) cnt from branch_xref_lead bxl
                                JOIN lead l ON bxl.lead_id = l.lead_id
                                where bxl.branch_id='".$param['branchId']."' and bxl.status=".$v_leadcoldcalls->lead_stage_id." AND bxl.is_active=1 and
                                bxl.branch_xref_lead_id not in
                                (select fk_branch_xref_lead_id from lead_user_xref_lead_due where fk_lead_stage_id=".$v_leadcoldcalls->lead_stage_id.") ".$date_condition;
            //exit;
            $cnt_leadcall_query=$this->db->query($avail_sel_query);
            if($cnt_leadcall_query)
            {
                $cnt_leadcall_query_res=$cnt_leadcall_query->result();
                foreach($cnt_leadcall_query_res as $k_result_lead_count=>$v_result_lead_count)
                {
                    $cnt = $v_result_lead_count->cnt;
                }
            }
            else
            {
                $cnt = 0;
            }
        }
        $db_response=array("status"=>true,"message"=>'success',"data"=>$cnt);
        return $db_response;
    }

    function getStudentCallSchedules($param)
    {
        $this->db->select('u.user_id,e.name,e.image,rtv.`value` as role');
        $this->db->from('user u');
        $this->db->join('branch_xref_user b','b.user_id=u.user_id');
        $this->db->join('employee e','e.user_id=u.user_id');
        $this->db->join('user_xref_role uxr','uxr.fk_user_id=u.user_id');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=uxr.fk_role_id');
        $this->db->where('u.is_active',1);
        $this->db->where('rtv.`value`!=','Superadmin');
        $this->db->where('b.branch_id',$param['branchId']);
        $query_users=$this->db->get();
        if($query_users){
            if ($query_users->num_rows() > 0)
            {
                $res=$query_users->result();
                foreach($res as $k_users=>$v_users)
                {
                    $previousCallsCnt=0;
                    $inHandCallsCnt=0;
                    $params='';
                    $params['user_id']=$v_users->user_id;
                    $params['branchId']=$param['branchId'];
                    $params['toDate']=$param['toDate'];
                    $previousCallsCnt=$this->getPreviousCallsCount($params);
                    $inHandCallsCnt=$this->getInHandCallsCount($params);
                    $v_users->previousCallsCount=$previousCallsCnt;
                    $v_users->inHandCallsCount=$inHandCallsCnt;
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'No Data Found',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }

    function getPreviousCallsCount($params)
    {
        $cnt=0;
        $cnt_lead=0;
        $toDate=$params['toDate'];
        $toDate=date('Y-m-d',strtotime($toDate.' +1 day'));
        $date_condition=" AND ((bxl.next_followup_date < '".$toDate."') OR (bxl.expected_visit_date < '".$toDate."') OR (bxl.expected_enroll_date < '".$toDate."')) ";

        $available_lead="SELECT bxl.branch_xref_lead_id
                                FROM branch_xref_lead bxl
                                JOIN lead_user_xref_lead_history uxh ON bxl.branch_xref_lead_id = uxh.fk_branch_xref_lead_id
                                JOIN lead l ON bxl.lead_id = l.lead_id
                                WHERE uxh.lead_status='M7' AND bxl.branch_id='".$params['branchId']."' AND uxh.fk_user_id = ".$params['user_id']."  ". $date_condition. " GROUP BY bxl.branch_xref_lead_id";
        $res_lead=$this->db->query($available_lead);
        if($res_lead)
        {
            $cnt_lead=$res_lead->num_rows();
        }
        else
        {

        }
        return ($cnt_lead);
    }
    function getInHandCallsCount($params)
    {
        $cnt=0;
        $cnt_lead=0;
        $toDate=$params['toDate'];
        $toDate=date('Y-m-d',strtotime($toDate.' +1 day'));
        $date_condition=" AND ((bxl.next_followup_date < '".$toDate."') OR (bxl.expected_visit_date < '".$toDate."') OR (bxl.expected_enroll_date < '".$toDate."')) ";

        $available_lead="SELECT bxl.branch_xref_lead_id
                                FROM branch_xref_lead bxl
                                JOIN lead_user_xref_lead_due uxh ON bxl.branch_xref_lead_id = uxh.fk_branch_xref_lead_id
                                JOIN lead l ON bxl.lead_id = l.lead_id
                                WHERE uxh.fk_lead_stage_id='20' AND bxl.branch_id='".$params['branchId']."' AND uxh.fk_user_id = ".$params['user_id']."  ". $date_condition. " GROUP BY bxl.branch_xref_lead_id";
        $res_lead=$this->db->query($available_lead);
        if($res_lead)
        {
            $cnt_lead=$res_lead->num_rows();
        }
        else
        {

        }
        return ($cnt_lead);
    }

    function addCallSchedule($params)
    {
        $assignedBy=$params['createdBy'];
        $callSchedules=$params['callSchedules'];
        $leadStageId = 20;
        if($callSchedules!='')
        {
            $callSchedules_explode = explode('|', $callSchedules);
            for ($i = 0; $i < count($callSchedules_explode); $i++)
            {
                $each_callSchedule = $callSchedules_explode[$i];
                $eah_callSchedule_explode = explode('-', $each_callSchedule);
                $assignedTo = $eah_callSchedule_explode[0];
                $totalAssignedCnt = $eah_callSchedule_explode[1];
                if ($totalAssignedCnt > 0) {
                    $cnt = 0;
                    $data_insert_head = array("total_scheduled_calls" => $totalAssignedCnt, "assigned_to" => $assignedTo, "assigned_on" => date('Y-m-d H:i:s'), "assigned_by" => $assignedBy);
                    $res_insert_head = $this->db->insert('call_schedule_head', $data_insert_head);
                    if ($res_insert_head)
                    {
                        $head_id = $this->db->insert_id();
                        //$available = "select l.lead_id from lead l,branch b where l.city_id=b.fk_city_id and b.branch_id='" . $params['branchId'] . "' and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;
                        //$available = "select l.lead_id from lead l where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='" . $params['branchId'] . "') and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;

                        //$available = "select l.lead_id from lead l where $city_condition and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;

                        $date_condition = '';
                        $available = "select
                                        bxl.branch_xref_lead_id,bxl.lead_id
                                        from branch_xref_lead bxl
                                        JOIN lead l ON bxl.lead_id = l.lead_id
                                        where
                                        bxl.branch_id='" . $params['branchId'] . "' AND bxl.status=" . $leadStageId . " AND bxl.is_active=1
                                        AND bxl.branch_xref_lead_id
                                        not in (select fk_branch_xref_lead_id from lead_user_xref_lead_due where fk_lead_stage_id=" . $leadStageId . ")". $date_condition ."ORDER BY bxl.branch_xref_lead_id asc LIMIT " . $totalAssignedCnt;

                        $res = $this->db->query($available);
                        if ($res)
                        {
                            $cnt = $res->num_rows();
                            $result = $res->result();
                            $data_insert_calls = '';
                            foreach ($result as $k_r => $v_r)
                            {
                                //$data_insert_calls[] = array("user_id" => $assignedTo, "lead_id" => $v_r->lead_id, "call_schedule_head_id" => $head_id, "call_schedule_head_id" => $head_id);
                                $data_insert_calls[] = array("fk_branch_xref_lead_id" => $v_r->branch_xref_lead_id, "fk_user_id" => $assignedTo, "fk_lead_id" => $v_r->lead_id, "fk_lead_stage_id" => $leadStageId, "fk_lead_call_schedule_head_id" => $head_id);
                            }
                            if (count($data_insert_calls) > 0)
                            {
                                $res_insert_calls = $this->db->insert_batch('lead_user_xref_lead_due', $data_insert_calls);
                            }
                        }
                        else
                        {

                        }
                    }
                }

            }
        }
        $db_response=array("status"=>true,"message"=>'success',"data"=>array("message"=>'success'));
        return $db_response;
    }

    function releaseCallSchedule($param)
    {
        //-----------------//
        $status=TRUE;$message='success';$data=array();
        $data_release_head=array();
        $tobededucted=array();
        //$fromDate=$param['fromDate'];
        $toDate=$param['toDate'];
        $toDate=date('Y-m-d',strtotime($toDate.' +1 day'));
        //$date_condition=" AND ((l.next_followup_date between '$fromDate' AND '$toDate') OR (l.expected_visit_date between '$fromDate' AND '$toDate') OR (l.expected_enroll_date between '$fromDate' AND '$toDate')) ";

        //$date_condition=" AND ((bxl.next_followup_date < '".$toDate."') OR (bxl.expected_visit_date < '".$toDate."') OR (bxl.expected_enroll_date < '".$toDate."')) ";
        $date_condition="";
        //$availableTotal="select  count(l.branch_xref_lead_id) as availCnt from branch_xref_lead l,lead_user_xref_lead_due uxl where l.branch_id='".$param['branchId']."' and l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=".$param['assignedUserId']." $date_condition ";
        $availableTotal="select
                        count(bxl.branch_xref_lead_id) as availCnt
                        from branch_xref_lead bxl
                        JOIN lead_user_xref_lead_due uxl ON bxl.branch_xref_lead_id = uxl.fk_branch_xref_lead_id
                        JOIN lead l ON bxl.lead_id = l.lead_id
                        where uxl.fk_lead_stage_id='20' AND bxl.branch_id='".$param['branchId']."' AND uxl.fk_user_id=".$param['assignedUserId']." ". $date_condition;

        $resTotal=$this->db->query($availableTotal);
        if($resTotal)
        {
            $release = 0;
            $resultTotal = $resTotal->result();
            foreach ($resultTotal as $kTotal => $vTotal)
            {
                $release = $vTotal->availCnt;
            }
            if($param['releaseCnt'] > $release)
            {
                $status = FALSE;
                $message = 'Cannot release more than assigned calls';
                $data = array("message" => 'Cannot release more than assigned calls');
            }
            else
            {
                $release = $param['releaseCnt'];
                if($release>0)
                {
                    //echo $available = "select uxl.fk_lead_call_schedule_head_id,count(l.branch_xref_lead_id) as availCnt from branch_xref_lead l,lead_user_xref_lead_due uxl where l.branch_id='" . $param['branchId'] . "' and l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=" . $param['assignedUserId'] . " $date_condition group by uxl.fk_lead_call_schedule_head_id";exit;
                    $available = "select
                                uxl.fk_lead_call_schedule_head_id,count(bxl.branch_xref_lead_id) as availCnt
                                from branch_xref_lead bxl
                                JOIN lead_user_xref_lead_due uxl ON bxl.branch_xref_lead_id = uxl.fk_branch_xref_lead_id
                                JOIN lead l ON bxl.lead_id = l.lead_id
                                where uxl.fk_lead_stage_id='20' AND bxl.branch_id='" . $param['branchId'] . "' AND uxl.fk_user_id=" . $param['assignedUserId'] . $date_condition." group by uxl.fk_lead_call_schedule_head_id";
                    $res = $this->db->query($available);
                    if ($res)
                    {
                        $result = $res->result();
                        foreach ($result as $k => $v)
                        {
                            $headId = $v->fk_lead_call_schedule_head_id;
                            $availCnt = $v->availCnt;
                            if ($availCnt < $release)
                            {
                                $tobededucted[] = array("headId" => $headId, "releaseCount" => $availCnt);
                                $release = $release - $availCnt;
                            }
                            elseif ($availCnt > $release)
                            {
                                $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                                $release = $release - $release;
                            }
                            elseif ($availCnt == $release)
                            {
                                $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                                $release = $release - $release;
                            }
                            else
                            {

                            }
                            if ($release == 0)
                            {
                                break;
                            }
                        }
                        if (count($tobededucted) > 0)
                        {
                            for ($i = 0; $i < count($tobededucted); $i++)
                            {
                                $callHeadId = $tobededucted[$i]['headId'];
                                $releaseCount = $tobededucted[$i]['releaseCount'];

                                $data_release_head['release_count'] = $releaseCount;
                                $data_release_head['released_by'] = $param['releaseBy'];
                                $data_release_head['released_on'] = date('Y-m-d H:i:s');
                                $data_release_head['fk_lead_call_schedule_head_id'] = $callHeadId;
                                $insert_release_head = $this->db->insert('lead_user_xref_lead_release_head', $data_release_head);
                                if ($insert_release_head)
                                {
                                    //release head inserted successfully.
                                    $release_head_id = $this->db->insert_id();
                                    //$available="select  l.lead_id from lead l,branch b,user_xref_lead_due uxl where l.city_id=b.fk_city_id and b.branch_id=".$param['branchId']." and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." and uxl.call_schedule_head_id=".$callHeadId." order by l.lead_id asc limit ".$releaseCount;
                                    //$available = "select  l.lead_id,uxl.fk_branch_xref_lead_id from branch_xref_lead l,lead_user_xref_lead_due uxl where l.branch_id='" . $param['branchId'] . "' and l.branch_xref_lead_id=uxl.fk_branch_xref_lead_id and uxl.fk_user_id=" . $param['assignedUserId'] . " and uxl.fk_lead_call_schedule_head_id=" . $callHeadId . " $date_condition order by l.branch_xref_lead_id asc limit " . $releaseCount;
                                    $available="select
                                                bxl.lead_id,uxl.fk_branch_xref_lead_id
                                                from branch_xref_lead bxl
                                                JOIN lead_user_xref_lead_due uxl ON bxl.branch_xref_lead_id = uxl.fk_branch_xref_lead_id
                                                JOIN lead l ON bxl.lead_id = l.lead_id
                                                where uxl.fk_lead_stage_id='20' AND bxl.branch_id='" . $param['branchId'] . "' AND uxl.fk_user_id=" . $param['assignedUserId'] ." AND uxl.fk_lead_call_schedule_head_id=" . $callHeadId." ".$date_condition." order by bxl.branch_xref_lead_id asc limit " . $releaseCount;
                                    $select_lead_ids = $this->db->query($available);

                                    if ($select_lead_ids)
                                    {
                                        //leads fetched successfully
                                        $result_sel_leadIds = $select_lead_ids->result();
                                        $data_insert_releaseCalls = '';
                                        $data_release_leads = '';
                                        foreach ($result_sel_leadIds as $result_sel_leadIds_k => $result_sel_leadIds_v)
                                        {
                                            $data_insert_releaseCalls[] = array("fk_user_id" => $param['assignedUserId'], "fk_branch_xref_lead_id" => $result_sel_leadIds_v->fk_branch_xref_lead_id, "fk_lead_id" => $result_sel_leadIds_v->lead_id, "fk_lead_call_schedule_head_id" => $callHeadId, "fk_lead_user_xref_lead_release_head_id" => $release_head_id);
                                            $data_release_leads[] = $result_sel_leadIds_v->fk_branch_xref_lead_id;
                                        }
                                        if ($data_insert_releaseCalls != '')
                                        {
                                            //release calls found
                                            $insert_release_calls = $this->db->insert_batch('lead_user_xref_lead_release', $data_insert_releaseCalls);
                                            if ($insert_release_calls) {
                                                //release calls successfully inserted
                                                $data_release_leads = array_map('intval', $data_release_leads);
                                                $data_release_leads = implode(",", $data_release_leads);

                                                /*$this->db->where_in('lead_id', $data_release_leads);
                                                $this->db->where('call_schedule_head_id', $callHeadId);
                                                $this->db->where('user_id', $param['assignedUserId']);
                                                $delete_due_calls=$this->db->delete('user_xref_lead_due');*/
                                                $delete_due_calls_query = "DELETE FROM lead_user_xref_lead_due WHERE fk_lead_stage_id='20' AND fk_lead_call_schedule_head_id=$callHeadId AND fk_user_id=" . $param['assignedUserId'] . " AND fk_branch_xref_lead_id in ($data_release_leads)";
                                                $delete_due_calls = $this->db->query($delete_due_calls_query);
                                                if ($delete_due_calls) {
                                                    //calls deleted successfully
                                                    $this->db->where('lead_call_schedule_head_id', $callHeadId);
                                                    $this->db->where('assigned_to', $param['assignedUserId']);
                                                    $this->db->set('total_scheduled_calls', 'total_scheduled_calls-' . $releaseCount, FALSE);
                                                    $update_call_head = $this->db->update('lead_call_schedule_head');
                                                    if ($update_call_head) {
                                                        //successfully updated call head counts
                                                        $status = TRUE;
                                                        $message = 'Released successfully';
                                                        $data = array();

                                                    } else {
                                                        //problem in updating count of call head
                                                        $error = $this->db->error();
                                                        $status = FALSE;
                                                        $message = 'problem in updating count of call head';
                                                        $data = array("message" => $error['message']);
                                                    }
                                                } else {
                                                    //problem in deleting calls from dues
                                                    $error = $this->db->error();
                                                    $status = FALSE;
                                                    $message = 'problem in deleting calls from dues';
                                                    $data = array("message" => $error['message']);
                                                }

                                            } else {
                                                //problem in batch insert of release calls
                                                $error = $this->db->error();
                                                $status = FALSE;
                                                $message = 'problem in batch insert of release calls';
                                                $data = array("message" => $error['message']);
                                            }
                                        }
                                        else
                                        {
                                            //no release calls found
                                            $error = $this->db->error();
                                            $status = FALSE;
                                            $message = 'no release calls found';
                                            $data = array("message" => $error['message']);
                                        }
                                    }
                                    else
                                    {
                                        //problem in fetching leads for given head
                                        $error = $this->db->error();
                                        $status = FALSE;
                                        $message = 'problem in fetching leads for given head';
                                        $data = array("message" => $error['message']);
                                    }
                                }
                                else
                                {
                                    //problem in insertion of relese head id
                                    $error = $this->db->error();
                                    $status = FALSE;
                                    $message = 'problem in insertion of release head id';
                                    $data = array("message" => $error['message']);
                                }
                            }
                            $multi_db_response[] = array("status" => $status, "message" => $message, "data" => $data);
                        } else {
                            //no availabled heads found
                            $error = $this->db->error();
                            $status = FALSE;
                            $message = 'no available heads found';
                            $data = array("message" => $error['message']);
                        }
                    } else {
                        //problem in fetching the available lead heads
                        $error = $this->db->error();
                        $status = FALSE;
                        $message = 'problem in fetching the available lead heads';
                        $data = array("message" => $error['message']);
                    }
                }
                else{
                    $status = FALSE;
                    $message = 'No release entries found';
                    $data = array("message" =>'No release entries found');
                }
            }
        }
        else
        {
            $error = $this->db->error();
            $status = FALSE;
            $message = 'problem in fetching the total available leads';
            $data = array("message" => $error['message']);
        }

        if($status == false)
        {
            $db_response=array("status"=>$status,"message"=>$message,"data"=>$data);
        }
        else
        {
            $message="Released successfully";
            $multi_db_response=array("message"=>$message);
            $db_response=array("status"=>TRUE,"message"=>$message,"data"=>$multi_db_response);
        }
        return $db_response;

    }

    public function getFollowUpStudentsList($data){
        $table = 'branch_xref_lead';
        // Table's primary key
        $primaryKey = 'branch_xref_lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes

        $columns = array(
            array( 'db' => 'bl.lead_number',                    'dt' => 'lead_number',           'field' => 'lead_number' ),
            array( 'db' => 'CONCAT_WS(\'\',b.code,bat.code,bl.lead_number) as lead_enroll_number',             'dt' => 'lead_enroll_number',            'field' => 'lead_enroll_number' ),
            array( 'db' => 'CONCAT_WS("-",b.code,c.name,"M7",bat.code) as batch_code',            'dt' => 'batch_code',          'field' => 'batch_code' ),
            array( 'db' => 'c.name as course_name',             'dt' => 'course_name',          'field' => 'course_name' ),
            array( 'db' => 'e.name AS followup_username',       'dt' => 'followup_username',    'field' => 'followup_username' ),
            array( 'db' => 'bl.updated_on AS followup_date',    'dt' => 'followup_date',        'field' => 'followup_date' ),
            array( 'db' => 'l.name as lead_name',               'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => '`bl`.`created_on`',                 'dt' => 'created_on',           'field' => 'created_on', 'formatter' => function( $d, $row ) {
                    return date( 'd M,Y', strtotime($d));}),
            array( 'db' => 'l.email',                           'dt' => 'email',                'field' => 'email' ),
            array( 'db' => 'l.phone',                           'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'ls.lead_stage as level',              'dt' => 'level',             'field' => 'level' ),
            array( 'db' => 'bl.updated_by AS followup_user_id', 'dt' => 'followup_user_id',     'field' => 'followup_user_id' ),
            array( 'db' => 'rtvcity.value as city',             'dt' => 'city',                 'field' => 'city' ),
            array( 'db' => 'rtvcmp.value as company',           'dt' => 'company',              'field' => 'company' ),
            array( 'db' => 'rtvqual.value as qualification',    'dt' => 'qualification',        'field' => 'qualification' ),
            array( 'db' => 'rtvinit.value as institution',      'dt' => 'institution',          'field' => 'institution' ),
            array( 'db' => 'bl.lead_id',                        'dt' => 'lead_id',              'field' => 'lead_id' ),
            array( 'db' => 'bl.branch_xref_lead_id',            'dt' => 'branch_xref_lead_id',  'field' => 'branch_xref_lead_id' ),
            array( 'db' => 'if(l.gender=1,1,0) as gender',          'dt' => 'gender',  'field' => 'gender' ),
            array( 'db' => 'b.name as branch_name',             'dt' => 'branch_name',          'field' => 'branch_name' ),
            array( 'db' => 'bat.alias_name',             'dt' => 'alias_name',          'field' => 'alias_name' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name ',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'c.name',    'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'l.email ',   'dt' => 'email',   'field' => 'email' ),
            array( 'db' => 'l.phone',    'dt' => 'phone',   'field' => 'phone' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'ls.lead_stage', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
            array( 'db' => 'e.name', 'dt' => 'followup_username', 'field' => 'followup_username'),
            array( 'db' => 'bl.updated_on', 'dt' => 'followup_date', 'field' => 'followup_date'),
            array( 'db' => 'CONCAT_WS(\'\',b.code,bat.code,bl.lead_number)',             'dt' => 'lead_enroll_number',            'field' => 'lead_enroll_number' ),
            array( 'db' => 'CONCAT_WS("-",b.code,c.name,"M7",bat.code)',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'alias_name',    'dt' => 'alias_name',   'field' => 'alias_name' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        /*$joinQuery = "from branch_xref_lead bl left join lead l on l.lead_id=bl.lead_id
                        left join lead_stage ls on  ls.lead_stage_id=bl.status
                        left join course c on c.course_id=bl.fk_course_id
                        left join branch b on b.branch_id=bl.branch_id
                        left join batch bat on bat.batch_id=bl.fk_batch_id
                        left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
                        left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
                        left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
                        left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id
                        LEFT JOIN employee e ON bl.updated_by=e.user_id";*/
        $joinQuery = "from lead_user_xref_lead_due lul
                       left join branch_xref_lead bl on bl.branch_xref_lead_id = lul.fk_branch_xref_lead_id
                       left join lead l on l.lead_id = bl.lead_id
                       left join lead_stage ls on  ls.lead_stage_id=bl.status
                       left join course c on c.course_id=bl.fk_course_id
                       left join branch b on b.branch_id=bl.branch_id
                       left join batch bat on bat.batch_id=bl.fk_batch_id
                       left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
                       left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
                       left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
                       left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id
                       LEFT JOIN employee e ON bl.updated_by=e.user_id";
        $extraWhere = "";
        //$extraWhere = "bl.is_active=1 and bl.branch_id IN (" . $branch . ") AND ls.lead_stage='M7' AND bl.is_active=1 ";

        if(isset($data['branchId']) && !empty($data['branchId']))
        {
                $extraWhere.= "bl.is_active=1 AND ls.lead_stage='M7' and bl.branch_id=" . $data['branchId'] . "";
        }
        else
        {
            $data['branchId']=0;
            $extraWhere.= "bl.is_active=1 AND ls.lead_stage='M7' and bl.branch_id=".$data['branchId']."";
        }
        if(isset($data['userID']) && !empty($data['userID']))
        {

            $extraWhere.=" and lul.fk_user_id=".$data['userID']."";
        }
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id_encode']=encode($responseData['data'][$i]['branch_xref_lead_id']);
            //$responseData['data'][$i]['second_level_counsellor_updated_date']=(new DateTime($responseData['data'][$i]['second_level_counsellor_updated_date']))->format('d M,Y');
            //$responseData['data'][$i]['branch_visited_date']=(new DateTime($responseData['data'][$i]['branch_visited_date']))->format('d M,Y');
            $responseData['data'][$i]['comments']=array();
            $getComments="select bv.branch_visited_date,bv.second_level_counsellor_updated_date as counsellor_visited_date,bv.branch_visit_comments,bv.second_level_counsellor_comments,e1.name as counseollor_name,e.name as second_counsellor_name from branch_visitor bv,employee e1,employee e where bv.fk_branch_xref_lead_id =".$responseData['data'][$i]['branch_xref_lead_id']." AND is_counsellor_visited=1 AND status=1 AND bv.created_by=e1.user_id AND bv.second_level_counsellor_id=e.user_id order by bv.branch_visitor_id DESC";
            $res_comment=$this->db->query($getComments);
            if($res_comment){
                if($res_comment->num_rows()>0){
                    $row_comment=$res_comment->result();
                    foreach($row_comment as $k_comment=>$v_comment){
                        $responseData['data'][$i]['comments'][]=array('counsellor_visited_date'=>(new DateTime($v_comment->counsellor_visited_date))->format('d M,Y'),'branch_visited_date'=>(new DateTime($v_comment->branch_visited_date))->format('d M,Y'),'second_level_comments'=>$v_comment->second_level_counsellor_comments,'branch_visit_comments'=>$v_comment->branch_visit_comments,'counseollor_name'=>$v_comment->counseollor_name,'second_counseollor_name'=>$v_comment->second_counsellor_name);
                    }
                }
            }
        }
        return $responseData;
    }
}