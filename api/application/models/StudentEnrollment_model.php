<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @controller Name Student Enrollment
 * @category        Model
 * @author          Parameshwar
 */
class StudentEnrollment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    public function getStudentEnrollmentsReport($branch, $course, $fromDate,$toDate,$feetype)
    {
        $query = $this->db->query("call Sp_Get_StudentEnrollment_Report({$branch},{$course},{$fromDate},{$toDate},{$feetype})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $this->db->close();
                $this->load->database();
                $db_response=array('status'=>true,'message'=>'success','data'=>$res);
            }
            else{
                $db_response=array('status'=>false,'message'=>'No Records Found','data'=>array('message'=>'No Records Found'));
            }
        } else {
            $error = $this->db->error()['message'];
            $db_response=array('status'=>false,'message'=>$error,'data'=>array('message'=>$error));
        }
        return $db_response;
    }

    public function getBranchListByUserId($userId) {
        $qry = "select
                b.branch_id id, b.name name
            from branch b
            left join branch_xref_user bxu ON b.branch_id=bxu.branch_id
             where b.is_active=1 AND bxu.user_id=".$userId;

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    function getAllBranchCoursesList($params){
        $branchId=$params['branchId'];
        $this->db->select("c.course_id courseId, c.name, c.description,c.is_active,c.fk_created_by");
        $this->db->from("course c");
        $this->db->join("branch_xref_course bxc" ,"bxc.fk_course_id=c.course_id");
        $this->db->where("bxc.fk_branch_id", $branchId);

        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            $db_response=array("status"=>true,"message"=>"success","data"=>$list);
        }
        else
        {
            $db_response=array("status"=>false,"message"=>"fail","data"=>array());
        }
        return $db_response;
    }
    public function getFeeTypeList(){
        $this->db->select("rtv.value as name,rtv.reference_type_value_id as fee_type_id");
        $this->db->from("reference_type_value rtv");
        $this->db->join("reference_type rt" ,"rt.reference_type_id=rtv.reference_type_id");
        $this->db->where("rt.name",'Fee Type');
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid ID',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getCurrentBatchesList($params){
        $branchId=$params['branchId'];
        $this->db->select("bat.batch_id,CONCAT_WS('-',b.code,c.name,'M7',bat.code) as code");
        $this->db->from('batch bat');
        $this->db->join('course c','c.course_id=bat.fk_course_id');
        $this->db->join('branch b','b.branch_id=bat.fk_branch_id');
        $this->db->where_in("bat.fk_branch_id",$branchId);
        if(!empty($params['courseId'])){
            $this->db->where_in("bat.fk_course_id",$params['courseId']);
        }
        $this->db->where("date(now()) between date(bat.marketing_startdate) and date(bat.marketing_enddate)");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'No Current Batches Found',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getPreviousBatchesList($params){
        $branchId=$params['branchId'];
        $this->db->select("bat.batch_id,CONCAT_WS('-',b.code,c.name,'M7',bat.code) as code");
        $this->db->from('batch bat');
        $this->db->join('course c','c.course_id=bat.fk_course_id');
        $this->db->join('branch b','b.branch_id=bat.fk_branch_id');
        $this->db->where_in("bat.fk_branch_id",$branchId);
        if(!empty($params['courseId'])){
            $this->db->where_in("bat.fk_course_id",$params['courseId']);
        }
        $this->db->where("date(bat.marketing_startdate)<date(now()) and date(bat.marketing_enddate)<date(now())");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'No Previous Batches Found',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    function getStudentAttendanceReport($branch,$course,$fromDate,$toDate)
    {
        $query = $this->db->query("call Sp_Get_StudentAttendance_Report({$branch},{$course},{$fromDate},{$toDate})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $this->db->close();
                $this->load->database();
                $db_response=array('status'=>true,'message'=>'success','data'=>$res);
            }
            else{
                $db_response=array('status'=>false,'message'=>'No Records Found','data'=>array('message'=>'No Records Found'));
            }
        } else {
            $error = $this->db->error()['message'];
            $db_response=array('status'=>false,'message'=>$error,'data'=>array('message'=>$error));
        }
        return $db_response;
    }
    function getStudentAdhocAttendanceReport($branch,$course,$fromDate,$toDate){
        $query = $this->db->query("call Sp_Get_StudentAdhocAttendance_Report({$branch},{$course},{$fromDate},{$toDate})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $k=>$v){
                    $v->totalClassAttendanceCount=$v->classAttendanceCount+$v->classPreviousAttendanceCount;
                    $v->classDate=date('d M, Y',strtotime($v->classDate));
                }
                $this->db->close();
                $this->load->database();
                $db_response=array('status'=>true,'message'=>'success','data'=>$res);
            }
            else{
                $db_response=array('status'=>false,'message'=>'No Records Found','data'=>array('message'=>'No Records Found'));
            }
        } else {
            $error = $this->db->error()['message'];
            $db_response=array('status'=>false,'message'=>$error,'data'=>array('message'=>$error));
        }
        return $db_response;
    }


}
