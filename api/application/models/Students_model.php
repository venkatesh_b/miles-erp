<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @controller Name Branch Visit
 * @category        Model
 * @author          Abhilash
 */
class Students_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    /**
     * Function to get Reports
     *
     * @return - array
     * @created date - 13 April 2016
     * @author : Abhilash
     */
    public function getStudentsList($branch){
        $table = 'branch_xref_lead';
        // Table's primary key
        $primaryKey = 'branch_xref_lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes

        $columns = array(
            array( 'db' => 'bl.lead_number',                    'dt' => 'lead_number',           'field' => 'lead_number' ),
            array( 'db' => 'CONCAT_WS(\'\',b.code,bat.code,bl.lead_number) as lead_enroll_number',             'dt' => 'lead_enroll_number',            'field' => 'lead_enroll_number' ),
            array( 'db' => 'CONCAT_WS("-",b.code,c.name,"M7",bat.code) as batch_code',            'dt' => 'batch_code',          'field' => 'batch_code' ),
            array( 'db' => 'c.name as course_name',             'dt' => 'course_name',          'field' => 'course_name' ),
            array( 'db' => 'e.name AS followup_username',       'dt' => 'followup_username',    'field' => 'followup_username' ),
            array( 'db' => 'bl.updated_on AS followup_date',    'dt' => 'followup_date',        'field' => 'followup_date' ),
            array( 'db' => 'l.name as lead_name',               'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => '`bl`.`created_on`',                 'dt' => 'created_on',           'field' => 'created_on', 'formatter' => function( $d, $row ) {
                return date( 'd M,Y', strtotime($d));}),
            array( 'db' => 'l.email',                           'dt' => 'email',                'field' => 'email' ),
            array( 'db' => 'l.phone',                           'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'ls.lead_stage as level',              'dt' => 'level',             'field' => 'level' ),
            array( 'db' => 'bl.updated_by AS followup_user_id', 'dt' => 'followup_user_id',     'field' => 'followup_user_id' ),
            array( 'db' => 'rtvcity.value as city',             'dt' => 'city',                 'field' => 'city' ),
            array( 'db' => 'rtvcmp.value as company',           'dt' => 'company',              'field' => 'company' ),
            array( 'db' => 'rtvqual.value as qualification',    'dt' => 'qualification',        'field' => 'qualification' ),
            array( 'db' => 'rtvinit.value as institution',      'dt' => 'institution',          'field' => 'institution' ),
            array( 'db' => 'bl.lead_id',                        'dt' => 'lead_id',              'field' => 'lead_id' ),
            array( 'db' => 'bl.branch_xref_lead_id',            'dt' => 'branch_xref_lead_id',  'field' => 'branch_xref_lead_id' ),
            array( 'db' => 'if(l.gender=1,1,0) as gender',          'dt' => 'gender',  'field' => 'gender' ),
            array( 'db' => 'b.name as branch_name',             'dt' => 'branch_name',          'field' => 'branch_name' ),
            array( 'db' => 'bat.alias_name',             'dt' => 'alias_name',          'field' => 'alias_name' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name ',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'c.name',    'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'l.email ',   'dt' => 'email',   'field' => 'email' ),
            array( 'db' => 'l.phone',    'dt' => 'phone',   'field' => 'phone' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'ls.lead_stage', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
            array( 'db' => 'e.name', 'dt' => 'followup_username', 'field' => 'followup_username'),
            array( 'db' => 'bl.updated_on', 'dt' => 'followup_date', 'field' => 'followup_date'),
            array( 'db' => 'CONCAT_WS(\'\',b.code,bat.code,bl.lead_number)',             'dt' => 'lead_enroll_number',            'field' => 'lead_enroll_number' ),
            array( 'db' => 'CONCAT_WS("-",b.code,c.name,"M7",bat.code)',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'alias_name',    'dt' => 'alias_name',   'field' => 'alias_name' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from branch_xref_lead bl left join lead l on l.lead_id=bl.lead_id
                        left join lead_stage ls on  ls.lead_stage_id=bl.status
                        left join course c on c.course_id=bl.fk_course_id
                        left join branch b on b.branch_id=bl.branch_id
                        left join batch bat on bat.batch_id=bl.fk_batch_id
                        left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
                        left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
                        left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
                        left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id
                        LEFT JOIN employee e ON bl.updated_by=e.user_id";

        $extraWhere = "bl.is_active=1 and bl.branch_id IN (" . $branch . ") AND ls.lead_stage='M7' AND bl.is_active=1 ";
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id']=encode($responseData['data'][$i]['branch_xref_lead_id']);
            //$responseData['data'][$i]['second_level_counsellor_updated_date']=(new DateTime($responseData['data'][$i]['second_level_counsellor_updated_date']))->format('d M,Y');
            $responseData['data'][$i]['followup_date']=(new DateTime($responseData['data'][$i]['followup_date']))->format('d M,Y');
            $responseData['data'][$i]['comments']=array();
            $getComments="select bv.branch_visited_date,bv.second_level_counsellor_updated_date as counsellor_visited_date,bv.branch_visit_comments,bv.second_level_counsellor_comments,e1.name as counseollor_name,e.name as second_counsellor_name from branch_visitor bv,employee e1,employee e where bv.fk_branch_xref_lead_id =".$responseData['data'][$i]['branch_xref_lead_id']." AND is_counsellor_visited=1 AND status=1 AND bv.created_by=e1.user_id AND bv.second_level_counsellor_id=e.user_id order by bv.branch_visitor_id DESC";
            $res_comment=$this->db->query($getComments);
            if($res_comment){
                if($res_comment->num_rows()>0){
                    $row_comment=$res_comment->result();
                    foreach($row_comment as $k_comment=>$v_comment){
                        $responseData['data'][$i]['comments'][]=array('counsellor_visited_date'=>(new DateTime($v_comment->counsellor_visited_date))->format('d M,Y'),'branch_visited_date'=>(new DateTime($v_comment->branch_visited_date))->format('d M,Y'),'second_level_comments'=>$v_comment->second_level_counsellor_comments,'branch_visit_comments'=>$v_comment->branch_visit_comments,'counseollor_name'=>$v_comment->counseollor_name,'second_counseollor_name'=>$v_comment->second_counsellor_name);
                    }
                }
            }
        }
        return $responseData;
    }

    public function getBranchListByUserId($userId) {
        $qry = "select
                b.branch_id id, b.name name
            from branch b
            left join branch_xref_user bxu ON b.branch_id=bxu.branch_id
             where b.is_active=1 AND bxu.user_id=".$userId;

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    function getAllBranchCoursesList($params){
        $branchId=$params['branchId'];
        $this->db->select("c.course_id courseId, c.name, c.description,c.is_active,c.fk_created_by");
        $this->db->from("course c");
        $this->db->join("branch_xref_course bxc" ,"bxc.fk_course_id=c.course_id");
        $this->db->where("bxc.fk_branch_id", $branchId);

        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            $db_response=array("status"=>true,"message"=>"success","data"=>$list);
        }
        else
        {
            $db_response=array("status"=>false,"message"=>"fail","data"=>array());
        }
        return $db_response;
    }
    public function getCurrentBatchesList($params){
        $branchId=$params['branchId'];
        $this->db->select("bat.batch_id,CONCAT_WS('-',b.code,c.name,'M7',bat.code) as code");
        $this->db->from('batch bat');
        $this->db->join('course c','c.course_id=bat.fk_course_id');
        $this->db->join('branch b','b.branch_id=bat.fk_branch_id');
        $this->db->where_in("bat.fk_branch_id",$branchId);
        if(!empty($params['courseId'])){
            $this->db->where_in("fk_course_id",$params['courseId']);
        }
        $this->db->where("date(now()) between date(marketing_startdate) and date(marketing_enddate)");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'No Current Batches Found',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getPreviousBatchesList($params){
        $branchId=$params['branchId'];
        $this->db->select("bat.batch_id,CONCAT_WS('-',b.code,c.name,'M7',bat.code) as code");
        $this->db->from('batch bat');
        $this->db->join('course c','c.course_id=bat.fk_course_id');
        $this->db->join('branch b','b.branch_id=bat.fk_branch_id');
        $this->db->where_in("bat.fk_branch_id",$branchId);
        if(!empty($params['courseId'])){
            $this->db->where_in("fk_course_id",$params['courseId']);
        }
        $this->db->where("date(marketing_startdate)<date(now()) and date(marketing_enddate)<date(now())");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'No Previous Batches Found',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getStudentsListExcel($branch){

     $query="SELECT l.name as lead_name, c.name as course_name, b.name as branch_name, bat.code as batch_code, `bl`.`created_on`, ls.lead_stage as level, l.email, l.phone, rtvcity.value as city, rtvcmp.value as company, rtvqual.value as qualification, rtvinit.value as institution, bl.lead_id, CONCAT_WS('',b.code,bat.code,bl.lead_number) as lead_number, bl.branch_xref_lead_id,
    CONCAT_WS(',',ifnull(l.email,0),ifnull(l.phone,0),ifnull(rtvqual.value,0),ifnull(rtvcmp.value,0),ifnull(rtvinit.value,0)) as info
    from branch_xref_lead bl left join lead l on l.lead_id=bl.lead_id
    left join lead_stage ls on  ls.lead_stage_id=bl.status
    left join course c on c.course_id=bl.fk_course_id
    left join branch b on b.branch_id=bl.branch_id
    left join batch bat on bat.batch_id=bl.fk_batch_id
    left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
    left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
    left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
    left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id
    WHERE bl.is_active=1 and bl.branch_id IN (" . $branch . ") AND ls.lead_stage='M7' AND bl.is_active=1";
    $res=$this->db->query($query);
        if($res){
            if($res->num_rows()>0){
                return $res->result();
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }

    public function getStudentFeeDetails($data,$branch_xref_lead_id='') {
        /*$qry = "SELECT
(cfi.amount_payable-cfi.concession-cfi.right_of_amount) as amountPayable,(cfi.amount_paid-cfi.refunded_other_amount) as amountPaid,ifnull(DATEDIFF(date_add(ba.acedemic_startdate,INTERVAL fsi.due_days day),cfi.updated_on),0) as dueDays from
		candidate_fee_item cfi,
		candidate_fee cf,
		fee_structure_item fsi,
		fee_head fh,
		company c,
		branch_xref_lead bxl,
		lead l,
		branch b,
		course co,
		batch ba
		where
		cf.candidate_fee_id=cfi.fk_candidate_fee_id and
		fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id and
		fh.fee_head_id=fsi.fk_fee_head_id and
		c.company_id=fh.fk_company_id and
		bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id and
		l.lead_id=bxl.lead_id and
		b.branch_id=bxl.branch_id and
		co.course_id=bxl.fk_course_id and
		ba.batch_id=bxl.fk_batch_id and
		bxl.lead_id=".$data;
        if($branch_xref_lead_id!=''){
            $qry.=' AND bxl.branch_xref_lead_id= '.$branch_xref_lead_id;
        }*/
        $qry="SELECT
        (cfi.amount_payable-cfi.concession-cfi.right_of_amount) as amountPayable,
        (cfi.amount_paid-cfi.refunded_other_amount) as amountPaid,
        ifnull(DATEDIFF(date_add(ba.acedemic_startdate,INTERVAL fsi.due_days day),cfi.updated_on),0) as dueDays
        FROM
        candidate_fee_item cfi
        LEFT JOIN candidate_fee cf ON cf.candidate_fee_id=cfi.fk_candidate_fee_id
        LEFT JOIN fee_structure_item fsi ON fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id
        LEFT JOIN fee_head fh ON fh.fee_head_id=fsi.fk_fee_head_id
        LEFT JOIN company c ON c.company_id=fh.fk_company_id
        LEFT JOIN branch_xref_lead bxl ON bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id
        LEFT JOIN lead l ON l.lead_id=bxl.lead_id
        LEFT JOIN branch b ON b.branch_id=bxl.branch_id
        LEFT JOIN course co ON co.course_id=bxl.fk_course_id
        LEFT JOIN batch ba ON ba.batch_id=bxl.fk_batch_id
        WHERE bxl.lead_id=".$data;
        if($branch_xref_lead_id!='')
        {
            $qry.=' AND bxl.branch_xref_lead_id= '.$branch_xref_lead_id;
        }
        $res_all = $this->db->query($qry);
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                return $res_all;
            } else {
                return 'nodata';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function getStudentFeeReceiptDetails($data,$branch_xref_lead_id='') {
        $qry = "SELECT bxl.lead_id,bxl.lead_number,fc.receipt_number,fc.receipt_date,fc.payment_status,fc.amount_paid,c.name,fc.payment_remarks
        from
		candidate_fee_item cfi,
		candidate_fee cf,
		fee_structure_item fsi,
		fee_head fh,
		company c,
		branch_xref_lead bxl,
		lead l,
    fee_collection fc,
    fee_collection_item fci
		where
		cf.candidate_fee_id=cfi.fk_candidate_fee_id and
		fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id and
		fh.fee_head_id=fsi.fk_fee_head_id and
		c.company_id=fh.fk_company_id and
		bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id and
		l.lead_id=bxl.lead_id and
    fc.fee_collection_id=fci.fk_fee_collection_id and
    fci.fk_candidate_fee_item_id=cfi.candidate_fee_item_id and
    bxl.lead_id='$data'
    ";
        if($branch_xref_lead_id!=''){
            $qry.=' AND bxl.branch_xref_lead_id= '.$branch_xref_lead_id;
        }
        $qry.=" order by fc.fee_collection_id desc";
        $res_all = $this->db->query($qry);

        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                foreach($res_all as $k=>$v){
                    $v->receipt_date=date('d M,Y',strtotime($v->receipt_date));
                }
                return $res_all;
            } else {
                return 'nodata';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /**
     * Function to get Student list(Exam status = Cleared All Alumni)
     *
     * @return - array
     * @created date - 25 Jan 2017
     * @author : Abhilash
     */
    public function getClearedAlumniLeadList($data){
        $table = 'branch_xref_lead';
        // Table's primary key
        $primaryKey = 'branch_xref_lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes

        $columns = array(
            array( 'db' => 'bl.lead_number',                    'dt' => 'lead_number',           'field' => 'lead_number' ),
            array( 'db' => 'CONCAT_WS(\'\',b.code,bat.code,bl.lead_number) as lead_enroll_number',             'dt' => 'lead_enroll_number',            'field' => 'lead_enroll_number' ),
            array( 'db' => 'CONCAT_WS("-",b.code,c.name,"M7",bat.code) as batch_code',            'dt' => 'batch_code',          'field' => 'batch_code' ),
            array( 'db' => 'c.name as course_name',             'dt' => 'course_name',          'field' => 'course_name' ),
            array( 'db' => 'e.name AS followup_username',       'dt' => 'followup_username',    'field' => 'followup_username' ),
            array( 'db' => 'bl.updated_on AS followup_date',    'dt' => 'followup_date',        'field' => 'followup_date' ),
            array( 'db' => 'l.name as lead_name',               'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => '`bl`.`created_on`',                 'dt' => 'created_on',           'field' => 'created_on', 'formatter' => function( $d, $row ) {
                return date( 'd M,Y', strtotime($d));}),
            array( 'db' => 'l.email',                           'dt' => 'email',                'field' => 'email' ),
            array( 'db' => 'l.phone',                           'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'ls.lead_stage as level',              'dt' => 'level',             'field' => 'level' ),
            array( 'db' => 'bl.updated_by AS followup_user_id', 'dt' => 'followup_user_id',     'field' => 'followup_user_id' ),
            array( 'db' => 'rtvcity.value as city',             'dt' => 'city',                 'field' => 'city' ),
            array( 'db' => 'rtvcmp.value as company',           'dt' => 'company',              'field' => 'company' ),
            array( 'db' => 'rtvqual.value as qualification',    'dt' => 'qualification',        'field' => 'qualification' ),
            array( 'db' => 'rtvinit.value as institution',      'dt' => 'institution',          'field' => 'institution' ),
            array( 'db' => 'bl.lead_id',                        'dt' => 'lead_id',              'field' => 'lead_id' ),
            array( 'db' => 'bl.branch_xref_lead_id',            'dt' => 'branch_xref_lead_id',  'field' => 'branch_xref_lead_id' ),
            array( 'db' => 'if(l.gender=1,1,0) as gender',          'dt' => 'gender',  'field' => 'gender' ),
            array( 'db' => 'b.name as branch_name',             'dt' => 'branch_name',          'field' => 'branch_name' ),
            array( 'db' => 'bat.alias_name',             'dt' => 'alias_name',          'field' => 'alias_name' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'l.name ',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'c.name',    'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'l.email ',   'dt' => 'email',   'field' => 'email' ),
            array( 'db' => 'l.phone',    'dt' => 'phone',   'field' => 'phone' ),
            array( 'db' => 'bl.lead_number',    'dt' => 'lead_number',   'field' => 'lead_number' ),
            array( 'db' => 'ls.lead_stage', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
            array( 'db' => 'e.name', 'dt' => 'followup_username', 'field' => 'followup_username'),
            array( 'db' => 'bl.updated_on', 'dt' => 'followup_date', 'field' => 'followup_date'),
            array( 'db' => 'CONCAT_WS(\'\',b.code,bat.code,bl.lead_number)',             'dt' => 'lead_enroll_number',            'field' => 'lead_enroll_number' ),
            array( 'db' => 'CONCAT_WS("-",b.code,c.name,"M7",bat.code)',   'dt' => 'name',   'field' => 'name' ),
            array( 'db' => 'alias_name',    'dt' => 'alias_name',   'field' => 'alias_name' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from branch_xref_lead bl left join lead l on l.lead_id=bl.lead_id
                        left join lead_stage ls on  ls.lead_stage_id=bl.status
                        left join course c on c.course_id=bl.fk_course_id
                        left join branch b on b.branch_id=bl.branch_id
                        left join batch bat on bat.batch_id=bl.fk_batch_id
                        left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
                        left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
                        left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
                        left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id
                        LEFT JOIN employee e ON bl.updated_by=e.user_id
                        JOIN lead_sr_answer lsr ON lsr.fk_branch_xref_lead_id = bl.branch_xref_lead_id
                        JOIN question qt ON lsr.fk_question_id = qt.question_id AND trim(qt.title) = 'Exam Status'
                        JOIN question_option qopt ON lsr.answer = qopt.question_option_id AND (qopt.`value` = 'Cleared all - Alumni' OR qopt.`value` = 'Cleared both - Alumni')";

        $extraWhere = "bl.is_active=1 and bl.branch_id IN (" . $data['branch'] . ") AND ls.lead_stage='M7' AND bl.is_active=1 ";
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id']=encode($responseData['data'][$i]['branch_xref_lead_id']);
            $responseData['data'][$i]['followup_date']=(new DateTime($responseData['data'][$i]['followup_date']))->format('d M,Y');
            $responseData['data'][$i]['comments']=array();
            $getComments="select bv.branch_visited_date,bv.second_level_counsellor_updated_date as counsellor_visited_date,bv.branch_visit_comments,bv.second_level_counsellor_comments,e1.name as counseollor_name,e.name as second_counsellor_name from branch_visitor bv,employee e1,employee e where bv.fk_branch_xref_lead_id =".$responseData['data'][$i]['branch_xref_lead_id']." AND is_counsellor_visited=1 AND status=1 AND bv.created_by=e1.user_id AND bv.second_level_counsellor_id=e.user_id order by bv.branch_visitor_id DESC";
            $res_comment=$this->db->query($getComments);
            if($res_comment){
                if($res_comment->num_rows()>0){
                    $row_comment=$res_comment->result();
                    foreach($row_comment as $k_comment=>$v_comment)
                    {
                        $responseData['data'][$i]['comments'][]=array('counsellor_visited_date'=>(new DateTime($v_comment->counsellor_visited_date))->format('d M,Y'),'branch_visited_date'=>(new DateTime($v_comment->branch_visited_date))->format('d M,Y'),'second_level_comments'=>$v_comment->second_level_counsellor_comments,'branch_visit_comments'=>$v_comment->branch_visit_comments,'counseollor_name'=>$v_comment->counseollor_name,'second_counseollor_name'=>$v_comment->second_counsellor_name);
                    }
                }
            }
        }
        return $responseData;
    }

    public function getClearedAlumniLeadListExcel($branch){

        $query="SELECT l.name as lead_name, c.name as course_name, b.name as branch_name, bat.code as batch_code, `bl`.`created_on`, ls.lead_stage as level, l.email, l.phone, rtvcity.value as city, rtvcmp.value as company, rtvqual.value as qualification, rtvinit.value as institution, bl.lead_id, CONCAT_WS('',b.code,bat.code,bl.lead_number) as lead_number, bl.branch_xref_lead_id,
    CONCAT_WS(',',ifnull(l.email,0),ifnull(l.phone,0),ifnull(rtvqual.value,0),ifnull(rtvcmp.value,0),ifnull(rtvinit.value,0)) as info
    from branch_xref_lead bl left join lead l on l.lead_id=bl.lead_id
    left join lead_stage ls on  ls.lead_stage_id=bl.status
    left join course c on c.course_id=bl.fk_course_id
    left join branch b on b.branch_id=bl.branch_id
    left join batch bat on bat.batch_id=bl.fk_batch_id
    left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
    left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
    left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
    left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id
    JOIN lead_sr_answer lsr ON lsr.fk_branch_xref_lead_id = bl.branch_xref_lead_id
    JOIN question qt ON lsr.fk_question_id = qt.question_id AND trim(qt.title) = 'Exam Status'
    JOIN question_option qopt ON lsr.answer = qopt.question_option_id AND (qopt.`value` = 'Cleared all - Alumni' OR qopt.`value` = 'Cleared both - Alumni')
    WHERE bl.is_active=1 and bl.branch_id IN (" . $branch . ") AND ls.lead_stage='M7' AND bl.is_active=1";
        $res=$this->db->query($query);
        if($res){
            if($res->num_rows()>0){
                return $res->result();
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }

}
