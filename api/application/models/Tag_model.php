<?php

/* 
 * Modified By: Abhilash. Date: 03-02-2016
 * Purpose: Services for Tags.
 * 
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tag_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
    }
    public function checkTag($param){
        $this->db->select("rtv.reference_type_value_id,rtv.`value`,rtv.description");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='Tag'");
        $this->db->where("rtv.reference_type_value_id",$param);
        $this->db->order_by("rtv.`value`");
        $query = $this->db->get();
        if($query){
        if ($query->num_rows() > 0){
            $res=$query->result();
            //exit;
            $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
        }
        else{
            $db_response=array("status"=>false,"message"=>'Invalid Tag ID',"data"=>array());
        }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
        
    }
    public function getAllTags(){
        
        $this->db->select("rtv.reference_type_value_id as id,rtv.`value` as tagName,rtv.description as tagDescription");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='Tag'");
        $this->db->where("rtv.is_active=1");
        //$this->db->order_by("rtv.`reference_type_value_id`", "desc");
        $this->db->order_by("rtv.`value`", "asc");
        $query = $this->db->get();
        if($query){
        if ($query->num_rows() > 0){
            $res=$query->result();
            $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
        }
        else{
            $db_response=array("status"=>false,"message"=>'No Tags Found',"data"=>array());
        }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
     }
}
