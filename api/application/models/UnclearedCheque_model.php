<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @controller Name Uncleared Cheque report
 * @category        Model
 * @author          Parameshwar
 */
class UnclearedCheque_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    public function getUnclearedChequeListReport($branch){
        $query = $this->db->query("call Sp_Get_UnclearedChequeDDs_List_Report({$branch})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $this->db->close();
                $this->load->database();
                foreach($res as $k=>$v){
                    if(isset($v->deletedOn))
                        $v->deletedOn=date('d M, Y',strtotime($v->deletedOn));
                    if(isset($v->receipt_date))
                        $v->receipt_date=date('d M, Y',strtotime($v->receipt_date));
                    if(isset($v->created_date))
                        $v->created_date=date('d M, Y',strtotime($v->created_date));
                    if(isset($v->created_on))
                        $v->created_on=date('d M, Y',strtotime($v->created_on));
                    if(isset($v->posting_date))
                        $v->posting_date=date('d M, Y',strtotime($v->posting_date));
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$res);
            }
            else{
                $db_response=array('status'=>false,'message'=>'No Records Found','data'=>array('message'=>'No Records Found'));
            }
        } else {
            $error = $this->db->error()['message'];
            $db_response=array('status'=>false,'message'=>$error,'data'=>array('message'=>$error));
        }
        return $db_response;
    }

    public function getBranchListByUserId($userId) {
        $qry = "select
                b.branch_id as id, b.name
            from branch b
            left join branch_xref_user bxu ON b.branch_id=bxu.branch_id
             where b.is_active=1 AND bxu.user_id=".$userId;
        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
}
