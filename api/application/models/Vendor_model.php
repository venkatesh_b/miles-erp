<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-16
 * Time: 10:52 AM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vendor_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');

    }
    public function checkVendorEmailExist($emailId='',$vendorId='')
    {
        $this->db->select('inventory_vendor_id');
        $this->db->from('inventory_vendor');
        $this->db->where('email',$emailId);
        $this->db->where('is_active',0);
        if(isset($vendorId) && $vendorId!='' && $vendorId>0)
            $this->db->where('inventory_vendor_id !=',$vendorId);
        $query = $this->db->get();
        if($query)
        {
            if($query->num_rows()>0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
            return false;
    }
    public function checkVendorExist($vendorId='')
    {
        $this->db->select("inventory_vendor_id");
        $this->db->from("inventory_vendor");
        $this->db->where("inventory_vendor_id", $vendorId);
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid Product',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function checkDeletedVendor($vendorEmail='')
    {
        $this->db->select('inventory_vendor_id');
        $this->db->from('inventory_vendor');
        $this->db->where('email',$vendorEmail);
        $this->db->where('is_active',1);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $results = $query->row();
            $deleteStatus = $results->inventory_vendor_id;
        }
        else{
            $deleteStatus = 0;
        }
        return $deleteStatus;
    }
    public function addVendor($data)
    {
        $query = $this->checkVendorEmailExist($data['email'],0);
        if($query===false)
        {
            $db_response=array("status"=>false,"message"=>'Email already exist',"data"=>array('vendor_email'=>'Email already exist'));
        }
        else
        {
            $isDeleted = $this->checkDeletedVendor($data['email']);
            if($isDeleted === 0){ //new vendors
                $res_insert=$this->db->insert('inventory_vendor', $data);
                if($res_insert)
                {
                    $db_response=array("status"=>true,"message"=>'Vendor saved successfully',"data"=>array());
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }else{ //deleted vendors
                $modifyData = array(
                    'name' =>$data['name'],
                    'phone' => $data['phone'],
                    'email' => $data['email'],
                    'address' => $data['address'],
                    'contactby_name' => $data['contactby_name'],
                    'contactby_phone' => $data['contactby_phone'],
                    'created_by' => $data['created_by'],
                    'created_date' => date('Y-m-d H:i:s'),
                    'is_active' => 0
                );
                $this->db->where('inventory_vendor_id',$isDeleted);
                $update_record = $this->db->update('inventory_vendor',$modifyData);
                if($update_record)
                {
                    $db_response=array("status"=>true,"message"=>'Vendor saved successfully',"data"=>array());
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
        }
        return $db_response;
    }
    public function getVendorDetailsById($vendorId='')
    {
        $this->db->select("name,email,address,phone,contactby_name,contactby_phone");
        $this->db->from("inventory_vendor");
        $this->db->where("inventory_vendor_id", $vendorId);
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            if(count($list) > 0)
            {
                $db_response=array("status"=>true,"message"=>"success","data"=>$list);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function updateVendor($data='')
    {
        $query = $this->checkVendorEmailExist($data['email'],$data['vendorId']);
        if($query===false)
        {
            $db_response=array("status"=>false,"message"=>'Email already exist',"data"=>array('vendor_email'=>'Email already exist'));
        }
        else
        {
            $modifiedData = array(
                'name' =>$data['name'],
                'phone' => $data['phone'],
                'email' => $data['email'],
                'address' => $data['address'],
                'contactby_name' => $data['contactby_name'],
                'contactby_phone' => $data['contactby_phone'],
                'updated_by' => $data['updated_by'],
                'updated_date' => date('Y-m-d H:i:s')
            );
            $this->db->where('inventory_vendor_id',$data['vendorId']);
            $res_insert=$this->db->update('inventory_vendor', $modifiedData);
            if($res_insert)
            {
                $db_response=array("status"=>true,"message"=>'Vendor saved successfully',"data"=>array());
            }
            else
            {
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
            }
        }
        return $db_response;
    }
    public function deleteVendor($vendorId='')
    {
        $this->db->where('inventory_vendor_id',$vendorId);
        $del_res = $this->db->update('inventory_vendor',array('is_active'=>1));
        if($del_res)
        {
            $status=TRUE;
            $message="Vendor deleted successfully";
        }
        else
        {
            $error = $this->db->error();
            $status=FALSE;
            $message=$error['message'];
        }
        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function vendorDataTables()
    {
        $table = 'inventory_vendor';
        $primaryKey = 'inventory_vendor_id';
        $columns = array(
            array( 'db' => 'name',                  'dt' => 'name',                 'field' => 'name' ),
            array( 'db' => 'email',                 'dt' => 'email',                'field' => 'email'),
            array( 'db' => 'phone',                 'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'contactby_name',        'dt' => 'contactby_name',       'field' => 'contactby_name'),
            array( 'db' => 'contactby_phone',       'dt' => 'contactby_phone',      'field' => 'contactby_phone' ),
            array( 'db' => 'inventory_vendor_id',   'dt' => 'inventory_vendor_id',  'field' => 'inventory_vendor_id' )
        );
        $globalFilterColumns = array(
            array( 'db' => 'name',                  'dt' => 'name',                 'field' => 'name' ),
            array( 'db' => 'email',                 'dt' => 'email',                'field' => 'email'),
            array( 'db' => 'phone',                 'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'contactby_name',        'dt' => 'contactby_name',       'field' => 'contactby_name'),
            array( 'db' => 'contactby_phone',       'dt' => 'contactby_phone',      'field' => 'contactby_phone' )
        );
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        $joinQuery = "";
        $extraWhere = "is_active=0";
        $groupBy = "";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery,$extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['inventory_vendor_id']=encode($responseData['data'][$i]['inventory_vendor_id']);
        }
        return $responseData;
    }
}