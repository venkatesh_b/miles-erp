<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 4/8/16
 * Time: 7:41 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class VoucherReconcelation_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    public function getVoucherReconcelationConfig($userId)
    {
        $this->db->select('b.branch_id, b.name');
        $this->db->from('branch b');
        $this->db->join('branch_xref_user bxu','b.branch_id=bxu.branch_id');
        $this->db->where('b.is_active',1);
        $this->db->where('bxu.user_id',$userId);
        $branch_qry=$this->db->get();
        if($branch_qry)
        {
            $branch_list=$branch_qry->result();
            $voucherReconcelation['Branches']=$branch_list;
            $this->db->select('company_id,`name`');
            $this->db->from('company');
            $company_qry=$this->db->get();
            if($company_qry)
            {
                $company_list=$company_qry->result();
                $voucherReconcelation['Companies']=$company_list;
                $db_response=array("status"=>true,"message"=>'success',"data"=>$voucherReconcelation);
            }
            else
            {
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    function getVoucherReconcelationByDate($branch_id,$company_id,$payment_mode,$chosen_date)
    {
        $this->db->select('fc.fee_collection_id,(fc.amount_paid-fc.reconcel_amount) as amount_paid,fc.payment_mode,cp.`name` as fee_company');
        $this->db->from('fee_collection fc');
        $this->db->join('company cp','fc.fk_fee_company_id=cp.company_id');
        $this->db->join('branch_xref_lead bxl','fc.fk_branch_xref_lead_id=bxl.branch_xref_lead_id');
        $this->db->where('bxl.branch_id',$branch_id);
        $this->db->where('fc.amount_paid !=fc.reconcel_amount');
        $this->db->where('DATE(fc.receipt_date) ',$chosen_date);
        $this->db->where('fc.fk_fee_company_id',$company_id);
        if($payment_mode == 'cash')
        {
            $this->db->where('fc.payment_mode','Cash');
        }
        else
        {
            $this->db->where('fc.payment_mode !=','Cash');
        }
        $this->db->where('fc.payment_status','Success');
        $reconcelation_qry=$this->db->get();
        if($reconcelation_qry)
        {
            $this->db->select('voucher_id,`name`,amount');
            $this->db->from('voucher');
            $this->db->where('fk_branch_id',$branch_id);
            $this->db->where('fk_company_id',$company_id);
            $this->db->where('DATE(voucher_date) ',$chosen_date);
            $this->db->where('is_active',1);
            $this->db->where('status !=','Reconceled');
            $voucher_qry=$this->db->get();
            if($voucher_qry)
            {
                $reconcelation_list['reconcelations']=$reconcelation_qry->result();
                $reconcelation_list['vouchers']=$voucher_qry->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$reconcelation_list);
            }
            else
            {
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }

    function ProcessVoucherReconcelation($branch_id,$company_id,$payment_mode,$chosen_date,$reconcelation_data)
    {
        for($rc=0;$rc<count($reconcelation_data);$rc++)
        {
            $reconcelation_raw=explode('@@@@@@',$reconcelation_data[$rc]);
            $reconcel_amt=$reconcelation_raw[0];
            $voucher_id=$reconcelation_raw[1];
            $feecollection_id=$reconcelation_raw[2];
            $data_insert=array('fk_voucher_id' => $voucher_id,'fk_fee_collection_id'=>$feecollection_id,'reconcel_amount'=>$reconcel_amt,'fk_created_by' => $_SERVER['HTTP_USER'], 'created_date'=>date('Y-m-d H:i:s'));
            $res_insert=$this->db->insert('voucher_reconcel', $data_insert);
            $query="update fee_collection set reconcel_amount=reconcel_amount+".$reconcel_amt." where fee_collection_id=".$feecollection_id;
            $this->db->query($query);
            $query="update voucher set status='Assigned' where voucher_id=".$voucher_id;
            $this->db->query($query);
        }
        $this->db->select('voucher_id,`name`,amount');
        $this->db->from('voucher');
        $this->db->where('fk_branch_id',$branch_id);
        $this->db->where('DATE(voucher_date) ',$chosen_date);
        $this->db->where('is_active',1);
        $this->db->where('status !=','Reconceled');
        $voucher_qry=$this->db->get();
        $voucher_data=$voucher_qry->result();
        for($v=0;$v<count($voucher_data);$v++)
        {
            $voucher_id=$voucher_data[$v]->voucher_id;;
            $voucher_amt=$voucher_data[$v]->amount;
            $reconcel_qry="select sum(reconcel_amount) as total_amt from voucher_reconcel where fk_voucher_id=".$voucher_id;
            $voucher_qry=$this->db->query($reconcel_qry);
            $voucher_rec_data=$voucher_qry->result();
            if($voucher_amt == $voucher_rec_data[0]->total_amt)
            {
                $query="update voucher set status='Reconceled' where voucher_id=".$voucher_id;
                $this->db->query($query);
            }
        }
        $db_response=array("status"=>true,"message"=>'success',"data"=>[]);
        return $db_response;
    }
}
?>