<?php

/* 
 * Created By: Abhilash 
 * Purpose: Services for Vouchers.
 * 
 * Updated on - 6 Feb 2016
 *
 * updated by: Venktesh
 * updated on: 17 Feb 2016:
 * Purpose: Services for creation of Vouchers, edit and delete of a Voucher.
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Voucher_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    
    public function getVoucherDetailsByID($params)
    {
        $this->db->select("v.voucher_id,v.`name`,fk_company_id,fk_branch_id,v.amount,v.description,v.voucher_date,v.`status`,v.is_active");
        $this->db->from("voucher v");
        $this->db->where("voucher_id", $params['voucherId']);
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            if(count($list) > 0)
            {
                $db_response=array("status"=>true,"message"=>"success","data"=>$list);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
                return $db_response;
    }

    function addVoucher($params){
            $this->db->select('voucher_id');
            $this->db->from('voucher');
            $this->db->where('name',$params['voucherName']);
            $query = $this->db->get();
            if(!$query){
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
            else
            {
                if ($query->num_rows() > 0)
                {
                    $db_response=array("status"=>false,"message"=>'Voucher already exists',"data"=>array('txtVoucherName'=>'Voucher already exists'));
                }
                else
                {
                        $data['voucherName']=$voucherName=$params['voucherName'];
                        $data['CompanyId']=$companyId=$params['companyId'];
                        $data['BranchId']=$branchId=$params['branchId'];
                        $data['voucherAmount']=$voucherAmount=$params['voucherAmount'];
                        $data['voucherDate']=$voucherDate=$params['voucherDate'];
                        $data['voucherDescription']=$voucherDescription=$params['voucherDescription'];
                        $data['voucherStatus']=$voucherStatus=$params['voucherStatus'];
                        $data['userID']=$userID=$params['userID'];
                        $data_insert=array('name' => $voucherName,'description'=>$voucherDescription,'fk_company_id'=>$companyId,'fk_branch_id'=>$branchId,'amount'=>$voucherAmount,'voucher_date' => $voucherDate,'is_active' => (int)$voucherStatus,'fk_created_by' => $userID, 'created_date'=>date('Y-m-d H:i:s'));
                        $res_insert=$this->db->insert('voucher', $data_insert);
                        if($res_insert)
                        {
                            $db_response=array("status"=>true,"message"=>'Voucher added successfully',"data"=>array("message"=>'Voucher added successfully'));
                        }
                        else
                        {
                            $error = $this->db->error();
                            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                        }
                }
            }
                return $db_response;
        
    }
    function updateVoucher($params)
    {
        $this->db->select('voucher_id');
        $this->db->from('voucher');
        $this->db->where('name',$params['voucherName']);
        $this->db->where('voucher_id!=',$params['voucherId']);
            $query = $this->db->get();
            if(!$query)
            {
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
            }
            else
            {
                if ($query->num_rows() > 0)
                {
                    $db_response=array("status"=>false,"message"=>'Voucher already exists',"data"=>array('txtVoucherName'=>'Voucher already exists'));
                }
                else
                {
                    $data['voucherId']=$voucherId=$params['voucherId'];
                    $data['voucherName']=$voucherName=$params['voucherName'];
                    $data['CompanyId']=$companyId=$params['companyId'];
                    $data['BranchId']=$branchId=$params['branchId'];
                    $data['voucherAmount']=$voucherAmount=$params['voucherAmount'];
                    $data['voucherDate']=$voucherDate=$params['voucherDate'];
                    $data['voucherDescription']=$voucherDescription=$params['voucherDescription'];
                    $data['voucherStatus']=$voucherStatus=$params['voucherStatus'];
                    $data['userID']=$userID=$params['userID'];
                    $data_update=array('name' => $voucherName,'description'=>$voucherDescription,'fk_company_id'=>$companyId,'fk_branch_id'=>$branchId,'amount'=>$voucherAmount,'voucher_date'=>$voucherDate,'is_active' => (int)$voucherStatus,'fk_created_by' => $userID, 'created_date'=>date('Y-m-d H:i:s'));
                    $this->db->where('voucher_id', $voucherId);
                    $res_update=$this->db->update('voucher', $data_update);
                    if($res_update)
                    {
                        $db_response=array("status"=>true,"message"=>'Voucher updated successfully',"data"=>array("message"=>'Voucher updated successfully'));
                    }
                    else
                    {
                        $error = $this->db->error();
                        $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                    }
                }
            }
            
                return $db_response;
    }

    function deleteVoucher($params){

            $data['voucherId']=$voucherId=$params['voucherId'];
            $data_update=array();
            $status = !$params['status'];
            $data_update=array('is_active' => $status);
            $this->db->where('voucher_id', $voucherId);
            $res_update=$this->db->update('voucher', $data_update);
            if($res_update)
            {
                       $db_response=array("status"=>true,"message"=>"Status updated successfully","data"=>array('message'=>'Status updated successfully'));
            }
            else
            {
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
            }
                return $db_response;
    
    }
    public function voucherDataTables()
    {
        $table = 'voucher';
        // Table's primary key
        $primaryKey = 'v`.`voucher_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`v`.`name` as voucher_name',            'dt' => 'voucher_name',             'field' => 'voucher_name' ),
            array( 'db' => 'b.`name` as branch_name',                'dt' => 'branch_name',             'field' => 'branch_name' ),
            array( 'db' => 'v.amount',                              'dt' => 'amount',                    'field' => 'amount' ),
            array( 'db' => '`v`.`voucher_date`'                     ,'dt' => 'voucher_date'     ,'field' => 'voucher_date', 'formatter' => function( $d, $row ) {
                    return date( 'd M,Y', strtotime($d));
                }),
            array( 'db' => 'v.status',                              'dt' => 'status',                    'field' => 'status' ),
            array( 'db' => 'if(`v`.`is_active`=1,1,0) as `is_active`','dt' => 'is_active',               'field' => 'is_active'),
            array( 'db' => '`v`.`voucher_id`',                      'dt' => 'voucher_id',               'field' => 'voucher_id' ),
            array( 'db' => '`cp`.`voucher_code`',                      'dt' => 'voucher_code',               'field' => 'voucher_code' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`v`.`name`',            'dt' => '`v`.`name`',             'field' => '`v`.`name`' ),
            array( 'db' => 'b.`name`',                'dt' => 'b.`name`',             'field' => 'b.`name`' ),
            array( 'db' => 'v.amount',                              'dt' => 'v.amount',                    'field' => 'v.amount' ),
            array( 'db' => 'v.status',                              'dt' => 'v.status',                    'field' => 'v.status' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM voucher v LEFT JOIN company cp ON cp.company_id=v.fk_company_id LEFT JOIN branch b ON v.fk_branch_id=b.branch_id LEFT JOIN employee e ON v.fk_created_by=e.user_id ";
        $extraWhere = "";
        $groupBy = "v.voucher_id";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['voucher_id']=encode($responseData['data'][$i]['voucher_id']);
        }
        return $responseData;
    }
}