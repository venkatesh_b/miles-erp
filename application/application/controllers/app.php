<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 17/2/16
 * Time: 11:46 AM
 */
class App extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('encrypt');
        $this->load->library('email');
        // code to remove the cache //
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
        // code to remove the cache //
    }
    public function index()
    {
        if($this->session->userdata('userData'))
        {
            redirect(APP_REDIRECT.$this->session->userdata('indexURL'));
        }
        else
        {
            $data['main']='login';
            $data['accessToken']=null;
            $this->load->view("header",$data);
            $this->load->view("login");
            $this->load->view("footer");
        }
    }
    public function details($view=null,$id=0,$sub_id=0)
    {
        if ($this->session->userdata('userData') == FALSE)
        {
            redirect(BASE_URL);
        }
        elseif($view == 'logout')
        {
            $session_items = array('userData' => '', 'menuData' => '', 'branchesData' => '');
            $this->session->unset_userdata($session_items);
            $this->session->sess_destroy();
            redirect(BASE_URL);
        }
        elseif($view == 'fee_receipt')
        {
            $userData=$this->session->userdata('userData');
            $data['userData'] = $userData;
            $data['Id']=$id;
            $data['accessToken']='Bearer '.$userData['accessToken'];
            $data['menuItem']=ucfirst($view);
            $this->load->view($view,$data);
        }
        elseif($view == 'template_preview')
        {
            $userData=$this->session->userdata('userData');
            $data['userData'] = $userData;
            $data['Id']=$id;
            $data['accessToken']='Bearer '.$userData['accessToken'];
            $data['menuItem']=ucfirst($view);
            $this->load->view($view,$data);
        }
        else
        {
            $userData=$this->session->userdata('userData');
            if (!file_exists(APPPATH."views/{$view}.php"))
            {
                $view='dummy';
            }
            $data['userData'] = $userData;
            $data['header'] = "header";
            $data['left'] = "left_panel";
            $data['footer'] = "footer";
            $data['main']=$view;
            $data['Id']=$id;
            $data['Sub_id']=$sub_id;
            $data['accessToken']='Bearer '.$userData['accessToken'];
            $data['menuItem']=ucfirst($view);
            $this->load->view("landing",$data);
        }
    }
}