    <div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
        <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
        <div class="content-header-wrap">
            <h3 class="content-header">Ad hoc Attendance Report</h3>
            <div class="content-header-btnwrap">
                <ul>
                    <li access-element="report"><a class="" href="javascript:" onclick="javascript:filterStudentAdhocAttendanceReportExport(this)"><i class="fa fa-file-excel-o"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- InstanceEndEditable -->
        <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
            <div class="fixed-wrap clearfix">
            <div class="fixed-wrap clearfix">
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Branch</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="branchFilter"></div>
                    </div>

                    <div class="col-sm-6 reports-filter-wrapper">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Course</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="courseFilter"></div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                    <div>
                        <label class="heading-uppercase">Batch Dates</label>
                    </div>
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="input-field col-sm-4 mt0 pl0">
                            <div class="input-field mt0">
                                <label class="datepicker-label datepicker">From Date </label>
                                <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtFromDate">
                            </div>
                        </div>
                        <div class="input-field col-sm-4 mt0 pl0">
                            <div class="input-field mt0">
                                <label class="datepicker-label datepicker">To Date </label>
                                <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtToDate">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 pl0">
                        <a class="btn blue-btn btn-sm" href="javascript:" onclick="javascript:filterStudentAdhocAttendanceReport()">Proceed</a>
                    </div>
                </div>
                <div id="studentadhocattendance-report-list-wrapper" access-element="report">
                <div class="col-sm-12 clearfix mt15 p0">
                    <table class="table table-inside table-custom branchwise-report-table" id="studentadhocattendance-report-list">
                        <thead>
                        <tr class="">
                            <th rowspan="2" class="valign-middle">Date</th>
                            <th rowspan="2" class="valign-middle">Venue</th>
                            <th rowspan="2" class="valign-middle">Subject</th>
                            <th rowspan="2" class="valign-middle">Faculty</th>
                            <th colspan="2" class="text-center">Current Batches</th>
                            <th rowspan="2" class="valign-middle text-right">Previous Batch</th>
                            <th rowspan="2" class="valign-middle text-right">Demo/Walkin</th>
                            <th rowspan="2" class="valign-middle text-right">Total</th>
                        </tr>
                        <tr class="">

                            <th class=" text-right">Total</th>
                            <th class=" text-right">Attended</th>


                        </tr>
                      </thead>

                    </table>
                </div>
                </div>
            </div>

            <!-- InstanceEndEditable --></div>
    </div>

    <script>
        docReady(function () {
            $("#studentadhocattendance-report-list-wrapper").hide();
            studentAdhocAttendaneReportPageLoad();
        });
        var StudentAdhocAttendanceConfigResponse='';
        $(document).ready(function () {

            $('input[name="batch_curr_prev"]').click(function () {
                var selectedCheck=[];
                $('input[name="batch_curr_prev"]').each(function () {
                    if ($(this).prop('checked')) {
                        selectedCheck.push($(this).val());
                    }

                });
                var html = '';
                if ($.inArray('current', selectedCheck) != -1)
                {
                    if(StudentAdhocAttendanceConfigResponse.currentbatches.length > 0){
                        for(var a in StudentAdhocAttendanceConfigResponse.currentbatches){
                            html += '<a class="active" data-id="'+StudentAdhocAttendanceConfigResponse.currentbatches[a].batch_id+'" href="javascript:;">'+StudentAdhocAttendanceConfigResponse.currentbatches[a].code+'</a>'
                        }

                    }
                }
                if ($.inArray('previous', selectedCheck) != -1)
                {
                    if(StudentAdhocAttendanceConfigResponse.previousbatches.length > 0){
                        for(var a in StudentAdhocAttendanceConfigResponse.previousbatches){
                            html += '<a class="active" data-id="'+StudentAdhocAttendanceConfigResponse.previousbatches[a].batch_id+'" href="javascript:;">'+StudentAdhocAttendanceConfigResponse.previousbatches[a].code+'</a>'
                        }

                    }
                }
                $('#batchFilter').html(html);
            });
        });

        /* Student adhoc attendance report starts here */
        function studentAdhocAttendaneReportPageLoad()
        {
            var userId = $('#userId').val();
            var action = 'report';
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

            var ajaxurl=API_URL+'index.php/StudentEnrollment/getAllConfigurations';
            var params = {};
            commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
                var dashboard = '';
                //return false;
                if (response.data.length == 0 || response == -1 || response['status'] == false) {
                } else {
                    StudentAdhocAttendanceConfigResponse=response.data;
                    var RawData = response.data;
                    var html = '';
                    if(RawData.branch.length > 0){
                        for(var a in RawData.branch){
                            html += '<a class="active" data-id="'+RawData.branch[a].id+'" href="javascript:;">'+RawData.branch[a].name+'</a>'
                        }
                        $('#branchFilter').html(html);
                    }
                    html = '';
                    if(RawData.course.length > 0){
                        for(var a in RawData.course){
                            html += '<a class="active" data-id="'+RawData.course[a].courseId+'" href="javascript:;">'+RawData.course[a].name+'</a>'
                        }
                        $('#courseFilter').html(html);
                    }
                    html = '';
                    if(RawData.currentbatches.length > 0){
                        for(var a in RawData.currentbatches){
                            html += '<a class="active" data-id="'+RawData.currentbatches[a].batch_id+'" href="javascript:;">'+RawData.currentbatches[a].code+'</a>'
                        }

                    }
                    if(RawData.previousbatches.length > 0){
                        for(var a in RawData.previousbatches){
                            html += '<a class="active" data-id="'+RawData.previousbatches[a].batch_id+'" href="javascript:;">'+RawData.previousbatches[a].code+'</a>'
                        }

                    }
                    $('#batchFilter').html(html);
                    buildpopover();
                }
            }});
        }
        function filterStudentAdhocAttendanceReport(This)
        {
            $("#txtFromDate").removeClass("required");
            $("#txtToDate").removeClass("required");
            $('#txtFromDate').parent().find('label').removeClass("active");
            $('#txtFromDate').parent().find('span.required-msg').remove();
            $('#txtToDate').parent().find('label').removeClass("active");
            $('#txtToDate').parent().find('span.required-msg').remove();
            var branchId = [];
            var courseId = [];
            var fromDate=$('#txtFromDate').val();
            var toDate=$('#txtToDate').val();
            if(fromDate.trim()!='') {
                var txtfromDate = dateConvertion(fromDate);
            }
            if(toDate.trim()!='') {
                var txttoDate = dateConvertion(toDate);
            }
            var error = true;
            $("#branchFilter > .active").each(function()
            {
                error = false;
                branchId.push($(this).attr('data-id'));
            });
            $("#courseFilter > .active").each(function()
            {
                error = false;
                courseId.push($(this).attr('data-id'));
            });
            var branchId=branchId.join(', ');
            var courseId=courseId.join(', ');
            if(branchId.trim()=='')
            {
                error=true;
                alertify.dismissAll();
                notify('Select any one branch','error');
                return false;
            }
            else if(courseId.trim()=='')
            {
                error=true;
                alertify.dismissAll();
                notify('Select any one course','error');
                return false;
            }
            else if($('#txtFromDate').val().trim()=='')
            {
                error = true;
                alertify.dismissAll();
                notify('Select from date','error');
                return false;
            }
            else if($('#txtToDate').val().trim()=='')
            {
                error = true;
                alertify.dismissAll();
                notify('Select to date','error');
                return false;

            }
            else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime())
            {
                $("#txtFromDate").addClass("required");
                $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
                $("#txtToDate").addClass("required");
                $("#txtToDate").after('<span class="required-msg">Invalid</span>');
                alertify.dismissAll();
                notify('Invalid dates','error');
                error=true;
                return false;
            }
            else
            {
                error=false;
            }
            if(!error)
            {
                var total=0;
                var pageTotal=0;
                var action = 'report';
                var ajaxurl=API_URL+'index.php/StudentEnrollment/getStudentAdhocAttendanceReport';
                var params = {branch: branchId,course: courseId, fromDate:fromDate,toDate:toDate};
                var userID=$('#userId').val();
                var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
                $("#studentadhocattendance-report-list-wrapper").show();
                $("#studentadhocattendance-report-list").dataTable().fnDestroy();
                $tableobj = $('#studentadhocattendance-report-list').DataTable( {
                    "fnDrawCallback": function() {
                        var $api = this.api();
                        var pages = $api.page.info().pages;
                        if(pages > 1)
                        {
                            $('.dataTables_paginate').css("display", "block");
                            $('.dataTables_length').css("display", "block");
                            //$('.dataTables_filter').css("display", "block");
                        } else {
                            $('.dataTables_paginate').css("display", "none");
                            $('.dataTables_length').css("display", "none");
                            //$('.dataTables_filter').css("display", "none");
                        }
                        verifyAccess();
                        buildpopover();
                    },
                    dom: "Bfrtip",
                    bInfo: false,
                    "serverSide": false,
                    "oLanguage":
                    {
                        "sSearch": "<span class='icon-search f16'></span>",
                        "sEmptyTable": "No Records Found",
                        "sZeroRecords": "No Records Found",
                        "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
                    },
                    "bProcessing": true,
                    ajax: {
                        url:ajaxurl,
                        type:'GET',
                        data:params,
                        headers:headerParams,
                        error:function(response) {
                            DTResponseerror(response);
                        }
                    },
                    columns: [
                        {
                            width: "10%",
                            data: null, render: function ( data, type, row )
                        {
                            return data.classDate;
                        }
                        },
                        {
                            width: "15%",
                            data: null, render: function ( data, type, row )
                        {
                            return data.classVenue;
                        }
                        },
                        {
                            width: "15%",
                            data: null, render: function ( data, type, row )
                        {

                            return data.classSubject;
                        }
                        },
                        {
                            width: "15%",
                            data: null, render: function ( data, type, row )
                        {

                            return data.classFaculty;
                        }
                        },
                        {
                            width: "7%",
                            data: null, render: function ( data, type, row )
                        {

                            return "<div class='text-right'>"+data.totalBatchCount+"</div>";
                        }
                        },
                        {
                            width: "7%",
                            data: null, render: function ( data, type, row )
                        {
                            return "<div class='text-right'>"+data.classAttendanceCount+"</div>";

                        }
                        },
                        {
                            width: "7%",
                            data: null, render: function ( data, type, row )
                        {
                            return "<div class='text-right'>"+data.classPreviousAttendanceCount+"</div>";

                        }
                        },
                        {
                            width: "7%",
                            data: null, render: function ( data, type, row )
                        {
                            return "<div class='text-right'>--</div>";
                        }
                        },
                        {
                            width: "7%",
                            data: null, render: function ( data, type, row )
                        {
                            return "<div class='text-right'>"+data.totalClassAttendanceCount+"</div>";

                        }
                        }


                    ]
                } );
                DTSearchOnKeyPressEnter();

            } else {
                alertify.dismissAll();
                notify('Select any filter', 'error', 10);
            }
        }
        function filterStudentAdhocAttendanceReportExport(This)
        {
            $("#txtFromDate").removeClass("required");
            $("#txtToDate").removeClass("required");
            $('#txtFromDate').parent().find('label').removeClass("active");
            $('#txtFromDate').parent().find('span.required-msg').remove();
            $('#txtToDate').parent().find('label').removeClass("active");
            $('#txtToDate').parent().find('span.required-msg').remove();
            var branchId = [];
            var branchIdText = [];
            var courseId = [];
            var courseIdText = [];
            var error = true;
            var fromDate=$('#txtFromDate').val();
            var toDate=$('#txtToDate').val();
            if(fromDate.trim()!='') {
                var txtfromDate = dateConvertion(fromDate);
            }
            if(toDate.trim()!='') {
                var txttoDate = dateConvertion(toDate);
            }

            $("#branchFilter > .active").each(function() {
                error = false;
                branchId.push($(this).attr('data-id'));
                branchIdText.push($(this).text());
            });
            $("#courseFilter > .active").each(function() {
                error = false;
                courseId.push($(this).attr('data-id'));
                courseIdText.push($(this).text());
            });
            $("#batchFilter > .active").each(function() {
                error = false;
                batchId.push($(this).attr('data-id'));
                batchIdText.push($(this).text());
            });

            var branchId=branchId.join(', ');
            var courseId=courseId.join(', ');

            if(branchId.trim()=='')
            {
                error=true;
                alertify.dismissAll();
                notify('Select any one branch','error');
                return false;
            }
            else if(courseId.trim()=='')
            {
                error=true;
                alertify.dismissAll();
                notify('Select any one course','error');
                return false;
            }
            else if($('#txtFromDate').val().trim()=='')
            {
                error = true;
                alertify.dismissAll();
                notify('Select from date','error');
                return false;
            }
            else if($('#txtToDate').val().trim()=='')
            {
                error = true;
                alertify.dismissAll();
                notify('Select to date','error');
                return false;

            }
            else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime())
            {
                $("#txtFromDate").addClass("required");
                $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
                $("#txtToDate").addClass("required");
                $("#txtToDate").after('<span class="required-msg">Invalid</span>');
                alertify.dismissAll();
                notify('Invalid dates','error');
                error=true;
                return false;
            }
            else
            {
                error=false;
            }

            if(!error){
                var total=0;
                var pageTotal=0;
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                var action = 'report';
                var ajaxurl=API_URL+'index.php/StudentEnrollment/getStudentAdhocAttendanceReportExport';
                var params = {'branch': branchId, 'course': courseId, fromDate:fromDate,toDate:toDate,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', ')};
                var userID=$('#userId').val();
                var type='GET';
                //return false;
                var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
                var response =
                    commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterStudentAdhocAttendanceReportExportResponse});


            } else {
                alertify.dismissAll();
                notify('Select any filter', 'error', 10);
            }
        }

        function filterStudentAdhocAttendanceReportExportResponse(response){
            alertify.dismissAll();
            if(response.status===true){
                var download=API_URL+'Download/downloadReport/'+response.file_name;
                window.location.href=(download);
            }
            else{
                notify(response.message,'error');
            }
        }
        /* Student adhoc attendance report ends here */
    </script>

