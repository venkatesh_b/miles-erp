<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 19/5/16
 * Time: 5:48 PM
 */
?>
<div class="contentpanel">
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="userBranchId" value="0" />
    <div class="content-header-wrap">
        <h3 class="content-header" id="branchDashboard_headerinfo_wrapper">Hyderabad (HYD)</h3>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody">
        <div class="fixed-wrap clearfix animated fadeIn">
            <div class="col-sm-12 branch-dashboard-top-widget">
                <div class="col-4 border-right">
                    <div class="icon"><span class="icon-users1 anchor-blue"></span></div>
                    <div class="f16 text-uppercase">Joined</div>
                    <div class="f21 dark-blue mt10" id="branchDashboard_joinedCount_wrapper">120</div>
                    <!--<div class=""><span class="mr8 red" id="branchDashboard_joinedPercLastWeek_wrapper">40%<i class="caret f18"></i></span><span class="ash">from last week</span></div>-->
                </div>
                <div class="col-4 border-right" style="display:none">
                    <div class="icon"><span class="icon-list2 anchor-blue"></span></div>
                    <div class="f16 text-uppercase">Inventory</div>
                    <div class="f21 dark-blue"><span class="ash f14 display-block" style="line-height:9px;">Issued/Total/Stock</span><span id="branchDashboard_inventory_wrapper">0/0/0</span></div>
                    <!--<div class=""><span class="mr8 red">40%<i class="caret f18"></i></span><span class="ash">from last week</span></div>-->
                </div>
                <div class="col-4 border-right">
                    <div class="icon"><span class="icon-users2 anchor-blue"></span></div>
                    <div class="f16 text-uppercase">SR Coverage</div>
                    <div class="f21 dark-blue mt10" id="branchDashboard_studentrelation_wrapper">0/0</div>
                    <!--<div class=""><span class="mr8 green">40%<i class="caret f18" style="transform:rotate(180deg);"></i></span><span class="ash">from last week</span></div>-->
                </div>
                <div class="col-4">
                    <div class="icon"><span class="fa fa-rupee anchor-blue"></span></div>
                    <div class="f16 text-uppercase">Dues</div>
                    <div class="f21 dark-blue mt10" id="branchDashboard_feeDue_wrapper">3,64,373</div>
                    <!--<div class=""><span class="mr8 green" id="branchDashboard_feeDuePerc_wrapper">40%<i class="caret f18" style="transform:rotate(180deg);"></i></span><span class="ash">from last week</span></div>-->
                </div>
            </div>
            <div class="col-sm-12 p0">
                <div class="col-sm-9 pl0">
                    <table class="branh-dboard-infotable table-responsive mt15" width="100%" id="coursesInfoWrapper">
                        <thead>
                        <th>Courses</th>
                        <th>Info</th>
                        <th>Conversions</th>
                        <th>Visitors</th>
                        <th>Calls</th>
                        <th>status</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-3 pt20">
                    <table class="table-responsive mt15"  width="100%" id="leadStageTable">
                        <thead>
                            <th>Lead Stage</th>
                            <th>Counts</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!--<div id="donut_chart" style="background-color: transparent; height:270px; width:270px"></div>
                    <ul class="chart-legends">
                    </ul>-->
                </div>
            </div>
            <div class="col-sm-12 p0">
                <div class="col-sm-9 pl0">
                    <table class="branh-dboard-infotable table-responsive branch-due-table mt15"  width="100%" id="currentCoursesInfoWrapper">
                        <thead>
                        <th>Batch</th>
                        <th>Info</th>
                        <th>Student</th>
                        <th>Dues</th>
                        <!--<th>Books</th>-->
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-3 mt15 p0">
                    <div class="widget-right border border-blue">
                        <div class="widget-header light-dark-blue relative">
                            <h4 class="heading-uppercase f14 m0 p10">Manage Users <a access-element="manage users" class="pull-right slide-down" href="javascript:"><i class="icon-plus-circle"></i></a></h4>
                        </div>
                        <div class="widget-contnet bg-white clearfix">
                            <div style="display:none;" class="hide-data clearfix pb10">
                                <div class="col-sm-12 mt10">
                                    <div class="input-field">
                                        <input id="autocomplete" class="validate autocomplete" type="text">
                                        <label for="autocomplete">Name</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <input type="text" class="validate " id="days" onkeypress="return isNumberKey(event)" maxlength="3">
                                        <label for="days">No. of Days</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <button class="btn blue-btn btn-xs" type="button" onclick="saveBranchUsers_branchDashboard(this)"> <i class="icon-right mr8"></i> Save </button>
                                    <button class="btn blue-light-btn btn-xs" onClick="closeBranchManageUser_branchDashboard()" type="button"><i class="icon-times mr8"></i>Cancel</button>
                                </div>
                            </div>
                            <span id="branchActiveUser"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var glbCount = 0;
    docReady(function(){
        buildBranchDashboardData(this);
        /*buildChart(this);*/
        buildLeadsTable();
    });
    $('#userBranchList').change(function(){
        $("#userBranchId").val(0);
        buildBranchDashboardData(this);
        /*buildChart(this);*/
        buildLeadsTable();
    });
    function buildLeadsTable(){
        var ajaxurl=API_URL+'index.php/Branch/getBranchLeadList';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        var params={branchId:branchId};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: this, method: 'GET', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
            if (response.data.length == 0 || response == -1 || response['status'] == false) {
                console.log(response.message);
            } else {
                $('#leadStageTable tbody').html('');
                var _html = '';
                var listData = response.data;
                _html += '<tr class="">';
                _html += '<td><a href="'+BASE_URL+'app/database" >M3</a></td>';
                _html += '<td>';
                _html += listData.M3;
                _html += '</td>';
                _html += '</tr>';
                _html += '<tr class="">';
                _html += '<td><a href="'+BASE_URL+'app/students" >M7</a></td>';
                _html += '<td>';
                _html += listData.M7;
                _html += '</td>';
                _html += '</tr>';
                $('#leadStageTable tbody').html(_html);
            }
        }});
    }
    /*function buildChart(This){
        var ajaxurl=API_URL+'index.php/Branch/getBranchChartData';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        var params={branch_id:branchId};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: This, method: 'GET', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
            if (response.data.length == 0 || response == -1 || response['status'] == false) {
//                console.log(response.message);
                $('#donut_chart').parent().hide();
            } else {
                var finalData = response.data,
                    colorArray = {'CPA' :'#6177ff', 'CMA': '#7cc343','PGDA': '#fba737'},
                    tempArray = [],
                    detailsArray = [],
                    totPercent = [],
                    dataArray = [];
                
                $('.chart-legends').html('');
                var rawHtml = '';
                if(Object.size(finalData.final)<=0){
                    $('#donut_chart').parent().hide();
                }else{
                    $('#donut_chart').parent().show();
                    for(var a in finalData.finalCourse){
                        var temp = finalData.finalCourse[a].split('+');
                        if(temp.length > 0){
                            var count = 0;
                            var tmp = 0;
                            for(var b in temp){
                                if(count == 0){
                                    tmp = JSON.parse(temp[b]);
                                    count++;
                                } else if(count == 1){
                                    tempArray.push([tmp , JSON.parse(temp[b])]);
                                    count = 0;
                                }
                            }
                        }
                    }
                    for(var a in finalData.finalCourse){
                        var categories = ['Enrolled ('+Math.floor((tempArray[a][0]/finalData.final[a].coursePercentage)*100)+'%)',
                            'Vacant ('+Math.floor((tempArray[a][1]/finalData.final[a].coursePercentage)*100)+'%)'];
                        var individual = finalData.final[a].individual;
                        var balance = finalData.final[a].balance;
                        var temp = {
                            y: finalData.final[a].coursePercentage,
                            color: colorArray[finalData.final[a].courseName],
                            drilldown: {
                                name: finalData.final[a].courseName,
                                categories: categories,
                                data: tempArray[a],
                                target: finalData.final[a].target,
                                individual: individual,
                                balance: balance,
                            }
                        };
                        rawHtml += '<li><i class="fa fa-square" style="color:'+colorArray[finalData.final[a].courseName]+'"></i>'+finalData.final[a].courseName+'</li>';
                        dataArray.push(temp);
                    }
                    $('.chart-legends').html(rawHtml);

                    var colors = colorArray,
                        categories = finalData.course,
                        data = dataArray,
                        browserData = [],
                        versionsData = [],
                        i,
                        j,
                        dataLen = data.length,
                        drillDataLen,
                        brightness;

                    // Build the data arrays
                    for (i = 0; i < dataLen; i += 1) {
                        // add inner circle data
                        browserData.push({
                            name: categories[i],
                            y: data[i].y,
                            target: data[i].drilldown.target,
                            color: data[i].color
                        });

                        // add version data
                        drillDataLen = data[i].drilldown.data.length;
                        for (j = 0; j < drillDataLen; j += 1) {
                           brightness = 0.15 - (j / drillDataLen) / 2;
                            versionsData.push({
                                name: data[i].drilldown.categories[j],
                                y: data[i].drilldown.data[j],
                                target: data[i].drilldown.target,
                                individual: data[i].drilldown.individual,
                                balance: data[i].drilldown.balance,
                                color: Highcharts.Color(data[i].color).brighten(brightness).get()
                            });
                        }
                    }

                    // Create the chart
                    $('#donut_chart').highcharts({
                        chart: {
                            type: 'pie',
                            //spacing:[-10,-10,-10,-10],
                            backgroundColor:'rgba(255, 255, 255, 0)',
                            height:270,
                            width:270
                        },
                        title: {
                            text: '',
                        style: {
                           color: '#F00',
                        }
                        },
                        yAxis: {
                            title: {
                                text: ''
                            }
                        },
                        plotOptions: {
                            pie: {
                                shadow: false,
                                center: ['50%', '50%'],
                                borderWidth: 2,
                                showInLegend: false
                            }
                        },
                        legend:{
                            align:"center",
                            verticalAlign:"bottom",
                            floating:true,
                            layout:'horizontal',
                            y:30
                        },                  
                        tooltip: {
                            valueSuffix: '',
                            shared: true,
                            useHTML: true,
                            backgroundColor: '#000',
                            borderColor: "#000",
                            style: {
                                "color": "#fff",
                            },
                            /!*formatter: function() {
                                return customFormatPointName(this.point.name);
                            }*!/
                        },
                        series: [{
                            name: 'Target',
                            data: browserData,
                            size: '60%',
                            dataLabels: {
                                formatter: function () {
                                    return this.y > 5 ? this.point.name + '(' + this.point.target + ')': null;
                                },
                                color: '#000',
                                distance: -50,
                                style: {
                                    textShadow: false ,
                                    fontFamily:"arial"
                                }
                            }
                        }, {
                            name: 'Count',
                            data: versionsData,
                            size: '80%',
                            innerSize: '60%',
                            dataLabels: {
                                formatter: function () {
                                    // display only if larger than 1
                                    var presentData = 0;
                                    if(glbCount==0){
                                        presentData = this.point.individual+' ';
                                        glbCount++;
                                    }else{
                                        presentData = this.point.balance+' ';
                                        glbCount=0;
                                    }
                                    return this.y > 1 ? '' +presentData : null;
                                },
                                color: '#000',
                                distance: -25,
                                style: {
                                    textShadow: false,
                                    fontFamily:"arial" 
                                }
                            }
                        }]
                    });
                }
            }
        }});
    }*/
    function resetBranchDashboardData(){
        $('#branchDashboard_headerinfo_wrapper').html('----');
        $('#branchDashboard_joinedCount_wrapper').html(0);
        $('#branchDashboard_inventory_wrapper').html('0/0/0');
        $('#branchDashboard_studentrelation_wrapper').html('0/0');
        $('#branchDashboard_feeDue_wrapper').html(0);
        $('#coursesInfoWrapper tbody').html('');
        $('#currentCoursesInfoWrapper tbody').html('');
        closeBranchManageUser_branchDashboard();
    }
    function buildBranchDashboardData(This){
        resetBranchDashboardData();
        var ajaxurl=API_URL+'index.php/Branch/getBranchDashboardData';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        var params={branchId:branchId};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: This, method: 'GET', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: buildBranchDashboardDataResponse});
        loadBranchUsersAutocompleteNew();
        //loadBranchUsersAutocomplete();
        manageUsersData(This);
    }
    function buildBranchDashboardDataResponse(response){
        var dashboardData=response.data;
        if(dashboardData.branchDetails.name){
            $('#branchDashboard_headerinfo_wrapper').html(dashboardData.branchDetails.name+' ('+dashboardData.branchDetails.code+')');
        }
        if(dashboardData.joined.count){
            $('#branchDashboard_joinedCount_wrapper').html(dashboardData.joined.count);
        }
        if(dashboardData.feeDues.dueAmount) {
            $('#branchDashboard_feeDue_wrapper').html(moneyFormat(dashboardData.feeDues.dueAmount));
        }

        if(dashboardData.inventory) {
            $('#branchDashboard_inventory_wrapper').html(dashboardData.inventory.issued+'/'+dashboardData.inventory.total+'/'+dashboardData.inventory.stock);
        }

        if(dashboardData.studentRelation) {
            $('#branchDashboard_studentrelation_wrapper').html(dashboardData.studentRelation.selectedtotal+'/'+dashboardData.studentRelation.total);
        }



        var coursesInfoHtml='';
        if(dashboardData.coursesInfo && dashboardData.coursesInfo.length>0){
            var counter=1;
            $.each(dashboardData.coursesInfo, function (key, value) {

                var statusType='';
                var statusTypeLabel='';
                var vacancyClass='';
                var percentage=(parseInt(value.filledSeatsCount)/parseInt(value.target))*100;
                var vacancyPercentage=100-percentage;
                vacancyPercentage=Math.floor(vacancyPercentage);
                if(vacancyPercentage>65){
                    statusType='red-bg';
                    statusTypeLabel='Not Satisfactory';
                    vacancyClass='red';
                }
                else if(vacancyPercentage<=65 && vacancyPercentage>40){
                    statusType='label-warning';
                    statusTypeLabel='Better';
                    vacancyClass='orange';
                }
                else{
                    statusType='label-success';
                    statusTypeLabel='Good';
                    vacancyClass='green';
                }
                var trclass="course-n";
                if(value.name.toUpperCase()=='CPA')
                {
                    trclass="course-n cpa-course";
                }
                else if(value.name.toUpperCase()=='CMA')
                {
                    trclass="course-n cma-course";
                }
                else
                {
                    trclass="course-n pgda-course";
                }

                coursesInfoHtml+="<tr class=\""+trclass+"\">";
                coursesInfoHtml+="<td>"+value.name+"</td>";
                coursesInfoHtml+="<td>";
                coursesInfoHtml+="<div class=\"clearfix pt5 \">";
                coursesInfoHtml+="<span class=\"pull-left anchor-blue ml5\"><a class=\"anchor-blue\" href='"+BASE_URL+'app/batch_details/'+value.encodedBranchId+'/'+value.batch_id+"'>"+value.code+"</a></span>";
                coursesInfoHtml+="<span class=\"pull-right red mr8\">"+value.leftDays+" Days left</span></div>";
                coursesInfoHtml+="<div class=\"clearfix\">";
                coursesInfoHtml+="<div class=\"display-inline col-3 border-right\"><span class=\"display-block\">Acadamic</span><span class=\"display-block\">"+value.acedemic_startdate+"</span></div>";
                coursesInfoHtml+="<div class=\"display-inline col-3 border-right\"><span class=\"display-block\">Mkg. Start</span><span class=\"display-block\">"+value.marketing_startdate+"</span></div>";
                coursesInfoHtml+="<div class=\"display-inline col-3\"><span class=\"display-block\">Mkg. End</span><span class=\"display-block\">"+value.marketing_enddate+"</span></div>";
                coursesInfoHtml+="</div>";
                coursesInfoHtml+="</td>";
                coursesInfoHtml+="<td>";
                coursesInfoHtml+="<div>"+value.filledSeatsCount+"</div>";
                coursesInfoHtml+="<div>"+value.target+"</div>";
                coursesInfoHtml+="</td>";
                coursesInfoHtml+="<td>";
                coursesInfoHtml+="<div><span class=\"icon-users1\"></span></div>";
                coursesInfoHtml+="<div>"+value.branchVisitCount+"</div>";
                coursesInfoHtml+="</td>";
                coursesInfoHtml+="<td>";
                coursesInfoHtml+="<div><span class=\"icon-phone\"></span></div>";
                coursesInfoHtml+="<div>"+value.callsCount+"</div>";
                coursesInfoHtml+="</td>";
                coursesInfoHtml+="<td>";
                coursesInfoHtml+="<div class='display-block float-none mb10'><span class=\""+vacancyClass+" f18\">"+vacancyPercentage+"%<span class=\"f14\"> Vacancy</span></span></div>";
                coursesInfoHtml+="<div class='float-none'><span class=\"label "+statusType+" text-uppercase\">"+statusTypeLabel+"</span></div>";
                coursesInfoHtml+="</td>";
                coursesInfoHtml+="</tr>";

            });

        }
        else{
            coursesInfoHtml="<tr class=\"course-n\"><td colspan='6' class=\"no-records\">No Courses Found! </td></tr>";
        }
        $('#coursesInfoWrapper tbody').html(coursesInfoHtml);
        var currentCoursesInfoHtml='';
        if(dashboardData.currentCoursesInfo && dashboardData.currentCoursesInfo.length>0) {
            $.each(dashboardData.currentCoursesInfo, function (key, value) {
                currentCoursesInfoHtml += "<tr class=\"course-n\">";
                currentCoursesInfoHtml += "<td>";
                currentCoursesInfoHtml += "<div class=\"text-center\">";
                currentCoursesInfoHtml += "<span class=\"ml5\"><a class=\"anchor-blue\" href='"+BASE_URL+'app/batch_details/'+value.encodedBranchId+'/'+value.batch_id+"'>"+value.code+"</a></span>";

                currentCoursesInfoHtml += "</div>";
                currentCoursesInfoHtml += "<div class=\"clearfix\">";
                currentCoursesInfoHtml += "<div class=\"display-inline col-2 border-right\"><span class=\"display-block\">Start Date</span><span class=\"display-block\">"+value.acedemic_startdate+"</span></div>";
                currentCoursesInfoHtml += "<div class=\"display-inline col-2\"><span class=\"display-block\">End Date</span><span class=\"display-block\">"+value.acedemic_enddate+"</span></div>";

                currentCoursesInfoHtml += "</div>";
                currentCoursesInfoHtml += "</td>";
                currentCoursesInfoHtml += "<td>";
                currentCoursesInfoHtml += "<div class=\"clearfix\">";
                currentCoursesInfoHtml += "<span class=\"pull-left ml5\">"+value.currentClassDate+"</span>";
                currentCoursesInfoHtml += "</div>";
                currentCoursesInfoHtml += "<div class=\"clearfix custom-row\">";
                currentCoursesInfoHtml += "<div class=\"display-inline col-sm-3 plr5 border-right\"><span class=\"display-block\">Classes</span><span class=\"display-block\">"+value.currentClassCount+"/"+value.totalClassCount+"</span></div>";
                currentCoursesInfoHtml += "<div class=\"display-inline col-sm-4 plr5 border-right\"><span class=\"display-block\">Faculty</span><span class=\"display-block\">"+value.currentClassFaculty+"</span></div>";
                currentCoursesInfoHtml += "<div class=\"display-inline col-sm-5 plr5\"><span class=\"display-block\">Location</span><span class=\"display-block\">"+value.currentClassLocation+"</span></div>";
                currentCoursesInfoHtml += "</div>";
                currentCoursesInfoHtml += "</td>";
                currentCoursesInfoHtml += "<td>";
                currentCoursesInfoHtml += "<div>";
                currentCoursesInfoHtml += "<span class=\"icon-users1\"></span>";
                currentCoursesInfoHtml += "</div>";
                currentCoursesInfoHtml += "<div>"+value.filledSeatsCount+"/"+value.target+"</div>";
                currentCoursesInfoHtml += "</td>";
                currentCoursesInfoHtml += "<td>";
                currentCoursesInfoHtml += "<div>";
                currentCoursesInfoHtml += "<span class=\"icon-btc-currency1\"></span>";
                currentCoursesInfoHtml += "</div>";
                currentCoursesInfoHtml += "<div>"+moneyFormat(parseInt(value.feeDueAmount))+"</div>";
                currentCoursesInfoHtml += "</td>";
                /*currentCoursesInfoHtml += "<td>";
                currentCoursesInfoHtml += "<div>";
                currentCoursesInfoHtml += "<span class=\"icon-actions\"></span>";
                currentCoursesInfoHtml += "</div>";
                currentCoursesInfoHtml += "<div>"+value.issuedBooksCount+"</div>";
                currentCoursesInfoHtml += "</td>";*/
                currentCoursesInfoHtml += "</tr>";
            });
        }
        else{
            currentCoursesInfoHtml="<tr class=\"course-n\"><td colspan='4' class=\"no-records\">No Batches Found! </td></tr>";
        }
        $('#currentCoursesInfoWrapper tbody').html(currentCoursesInfoHtml);


    }
    function manageUsersData(This){
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var branchId=$('#userBranchList').val();
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/Branch/getBranchById';
        var params = {'branch_id': branchId};
        commonAjaxCall({This: This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: manageUsersDataResponse});
    }
    function manageUsersDataResponse(response){
        var activeUser = '';
        activeUser += '<h5 class="heading-uppercase pl10">Active Members</h5>';
        if(typeof response.data.user!='undefined' && typeof response.data.user.active !='undefined'){
            if(response.data.user.active.length > 0) {
                for (var index in response.data.user.active) {
                    activeUser += '<div class="col-sm-12 mtb10 userworking-info-wrap c-userworking-info-wrap">';
//                    activeUser += '<div class="dropdown feehead-list pull-right custom-dropdown-style"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="javascript:" data-target="#" id="dLabel"> <i class="fa fa-ellipsis-v f18 plr10"></i> </a>';
//                    activeUser += '<ul aria-labelledby="dLabel" class="dropdown-menu pull-right">';
//                    activeUser += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
////                if(response.data.user.active[index]['role_name'] != 'Superadmin'){
//                    activeUser += '<li access-element="list"><a onclick="extendBranchUsers_branchDashboard(\'' + response.data.user.active[index]['user_id'] + '\', \'' + response.data.user.active[index]['name'] + '\',this)" href="javascript:;">Extend</a></li>';
//                    activeUser += '<li access-element="list"><a onclick="deactiveBranchUsers_branchDashboard(\'' + response.data.user.active[index]['user_id'] + '\', this)" href="javascript:;">Deactivate</a></li>';
////                }
//                    activeUser += '</ul>';
//                    activeUser += '</div>';
                    if(response.data.user.active[index]['role_name'] == 'Superadmin'){
                        activeUser += '';   /*No option for any superadmin*/
                    }else{
                        if(('<?php echo $userData['Role']; ?>'=='Superadmin' || '<?php echo $userData['Role']; ?>'=='Branch Head') && response.data.user.active[index]['user_id']!=<?php echo $userData['userId']; ?>){
                            activeUser += '<div class="dropdown feehead-list pull-right custom-dropdown-style"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#" data-target="#" id="dLabel"> <i class="fa fa-ellipsis-v f18 plr10"></i> </a>';
                            activeUser += '<ul aria-labelledby="dLabel" class="dropdown-menu pull-right">';
                            activeUser += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                            activeUser += '<li access-element="manage users"><a onclick="extendBranchUsers_branchDashboard(\'' + response.data.user.active[index]['user_id'] + '\', \'' + response.data.user.active[index]['name'] + '\',this)" href="javascript:;">Extend</a></li>';
                            activeUser += '<li access-element="manage users"><a onclick="deactiveBranchUsers_branchDashboard(\'' + response.data.user.active[index]['user_id'] + '\', this)" href="javascript:;">Deactivate</a></li>';
                            activeUser += '</ul>';
                            activeUser += '</div>';
                        }
                    }
                    var userImage='';
                    if(response.data.user.active[index]['image'] && CheckFileExists(response.data.user.active[index]['image']))
                    {
                        userImage += '<img src="'+response.data.user.active[index]['image']+'" class="mCS_img_loaded">';

                    }
                    else
                    {
                        userImage += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png" class="mCS_img_loaded">';
                    }
                    activeUser += '<div class="userworking-img"> '+userImage+' </div>';
                    activeUser += '<div class="pull-left pl5 inline-lable">';
                    activeUser += '<label class="label-data ellipsis">' + response.data.user.active[index]['name'] + '</label>';
                    activeUser += '<p class="label-text ellipsis">' + response.data.user.active[index]['role_name'] + '</p>';
                    activeUser += '<div class="label-hr col-sm-12 p0">';
                    activeUser += '<label class="label-text ellipsis">Added on : </label>';
                    activeUser += '<span>' + response.data.user.active[index]['alotted_date'] + '</span> </div>';
                    activeUser += '</div>';

                    if (response.data.user.active[index]['days'] != 0) {
                        activeUser += '<span title="Number of days left" class="usersdays-left">' + response.data.user.active[index]['daysLeft'] + '</span>';
                    } else if (response.data.user.active[index]['days'] === undefined) {
                        activeUser += '<span title="Number of days left" class="usersdays-left"> -- </span>';
                    }
                    //                                activeUser += '<div> </div>';
                    activeUser += '</div>';
                }
            }

        }
        if(typeof response.data.user!='undefined' && typeof response.data.user.inactive !== 'undefined' && response.data.user.inactive.length > 0){
            activeUser += '<h5 class="heading-uppercase pl10">Inactive Members</h5>';
            for (var index in response.data.user.inactive) {
                var userImage='';
                if(response.data.user.inactive[index]['image'] && CheckFileExists(response.data.user.inactive[index]['image']))
                {
                    userImage += '<img src="'+response.data.user.inactive[index]['image']+'" class="mCS_img_loaded">';

                }
                else
                {
                    userImage += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png" class="mCS_img_loaded">';
                }
                activeUser += '<div data-trigger="hover" data-placement="top" data-toggle="popover" class="col-sm-12 mtb10 userworking-info-wrap inactive-users popper c-userworking-info-wrap" data-original-title="' + response.data.user.inactive[index]['name'] + '" title="' + response.data.user.inactive[index]['name'] + '">'
                    + '<div class="userworking-img"> '+userImage+' </div>'
                    + '<div class="pull-left pl5">'
                    + '<label class="label-data ellipsis">' + response.data.user.inactive[index]['name'] + '</label>'
                    + '<p class="label-text ellipsis">' + response.data.user.inactive[index]['value'] + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="popper-content hide">'
                    + '<div class="plr15"><p>'
                    + '<label class="popover-label ash f14">From :</label>'
                    + response.data.user.inactive[index]['created_date'] + '</p>'
                    + '<p>'
                    + '<label class="popover-label ash f14">To :</label>'
                    + response.data.user.inactive[index]['end_date'] + '</p>'
                    + '<p>'
                    + '<label class="popover-label ash f14">Days :</label>'
                    + response.data.user.inactive[index]['days'] + '</p>'
                    + '</div></div>';
            }
        }
        $('#branchActiveUser').html();
        $('#branchActiveUser').html(activeUser);
        buildpopover();
    }
    function loadBranchUsersAutocomplete(){
        $('.autocomplete').on('keyup', function (ev) {
            // stuff happens
            $val = $('#autocomplete').val().trim();
            $('.autocomplete-content').remove();
            var branchId=$('#userBranchList').val();
            $('#autocomplete').parent().find('.required-msg').remove();
            $('#autocomplete').removeClass("required");

            if(ev.keyCode == 27){
                $select = $('.autocomplete-content');
                $select.children('li').addClass('hide');
                $('#autocomplete').val('');
            }
            if ((/^[a-zA-Z ]+$/.test($('#autocomplete').val()))) {
                var userId = $('#hdnUserId').val();
                var action = 'list';
                var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
                var ajaxurl = API_URL + 'index.php/Branch/getBranchUsersDashboard';
                var params = {'branch_id': branchId, 'search': $val};
                commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                    if(!response.status || response.data.length<=0){
                        //alertify.dismissAll();
                        //notify('No Data Found', 'error', 10);
                    }else{
                        var input_selector = 'input[type=text], input[type=search]';
                        var dataObj = $.map(response.data, function (item) {
                            return {'value': item.value, 'id': item.user_id}
                        });
                        $('#autocomplete').data('array', dataObj);

                        $(input_selector).each(function () {

                            var $input = $(this);

                            if ($input.hasClass('autocomplete')) {

                                var $array = $input.data('array'),
                                    $inputDiv = $input.closest('.input-field'); // Div to append on
                                // Check if "data-array" isn't empty
                                if ($array !== '') {
                                    // Create html element
                                    var $html = '<ul class="autocomplete-content hide">';

                                    for (var i = 0; i < $array.length; i++) {
                                        // If path and class aren't empty add image to auto complete else create normal element
                                        if ($array[i]['path'] !== '' && $array[i]['path'] !== undefined && $array[i]['path'] !== null && $array[i]['class'] !== undefined && $array[i]['class'] !== '') {
                                            $html += '<li class="autocomplete-option" data-id="'+$array[i]['id']+'"><span>' + $array[i]['value'] + '</span></li>';
                                        } else {
                                            $html += '<li class="autocomplete-option" data-id="'+$array[i]['id']+'"><span>' + $array[i]['value'] + '</span></li>';
                                        }
                                    }

                                    $html += '</ul>';
                                    $inputDiv.append($html); // Set ul in body
                                    // End create html element

                                    $val = $('#autocomplete').val().trim();
                                    $select = $('.autocomplete-content');
                                    // Check if the input isn't empty
                                    $select.css('width', $input.width());

                                    if ($val != '') {
                                        $select.children('li').addClass('hide');
                                        $select.children('li').filter(function() {
                                            $select.removeClass('hide'); // Show results

                                            // If text needs to highlighted
                                            if ($input.hasClass('highlight-matching')) {
                                                highlight($val);
                                            }
                                            var check = true;
                                            /*for (var i in $val) {
                                             if ($val[i].toLowerCase() !== $(this).text().toLowerCase()[i])
                                             check = false;
                                             };*/
                                            return check ? $(this).text().toLowerCase().indexOf($val.toLowerCase()) !== -1 : false;
                                        }).removeClass('hide');
                                    } else {
                                        $select.children('li').addClass('hide');
                                    }

                                    // Set input value
                                    $('.autocomplete-option').click(function() {
                                        $('#userBranchId').val($(this).attr('data-id'));
                                        $input.val($(this).text().trim());
                                        $('.autocomplete-option').addClass('hide');
                                    });
                                } else {
                                    //                                return false;
                                    var $html = '<ul class="autocomplete-content hide">';
                                    $html += '<li class="autocomplete-option"><span>' + $array[i]['value'] + '</span></li>';
                                    $html += '</ul>';
                                    $inputDiv.append($html);
                                }
                            }
                        });
                    }
                }});
            } else if (!(/^[a-zA-Z]+$/.test($val)) && !($val.trim().length <= 0)) {
                $('#autocomplete').addClass("required");
                $("#autocomplete").after('<span class="required-msg">Must be alphabets</span>');
            }
        });
    }
    function extendBranchUsers_branchDashboard(id, name, This){
        $('#autocomplete').parent().find('label').addClass("active");
        $('.hide-data input').removeClass("required");
        $('#autocomplete').val(name);
        $('.hide-data').slideDown();
        $('#userBranchId').val(id);
    }
    function deactiveBranchUsers_branchDashboard(uid){
        notify('Processing..', 'warning', 10);
        var id = $('#userBranchList').val();
        var userId = $('#hdnUserId').val();

        var data = {
            userid: uid
        };
        var action = 'list';
//            var userId = $('#userId').val();

        var ajaxurl = API_URL + "index.php/Branch/deactivateUserBranchDashboard/branch_id/"+id;
        var params = data;
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
            if (response == -1 || response['status'] == false){
//
            } else {
                $('.hide-data input').removeClass("required");
                $('.hide-data').parent().find('label').addClass("active");
                buildBranchDashboardData();
                closeBranchManageUser_branchDashboard();
                alertify.dismissAll();
                notify(response.message, 'success', 10);
            }
        }});
    }
    function saveBranchUsers_branchDashboard(This){
        $('.hide-data input').removeClass("required");
        $('.hide-data').parent().find('label').addClass("active");

        var id = $('#userBranchList').val();
        var uid = $('#userBranchId').val();

        var flag = 0;

        var name = $('#autocomplete').val();
        var days = $('#days').val();

        if (name == '') {
            $("#autocomplete").addClass("required");
            $("#autocomplete").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (!(/^[a-zA-Z ]+$/.test(name))) {
            $("#autocomplete").addClass("required");
            $("#autocomplete").after('<span class="required-msg">Must contain only Alphabets</span>');
            flag = 1;
        }

        if (days == '') {
            $("#days").addClass("required");
            $("#days").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (!(/^[a-zA-Z0-9]+$/.test(days))) {
            $("#days").addClass("required");
            $("#days").after('<span class="required-msg">Must contain only Numbers</span>');
            flag = 1;
        }
        if(flag != 1){
            notify('Processing..', 'warning', 10);
            var data = {
                name: name,
                userid: uid,
                days: days
            };
            var action = 'list';
            var userId = $('#hdnUserId').val();
            var ajaxurl = API_URL + "index.php/Branch/extendUserBranchDashboard/branch_id/"+id;
            var params = data;
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
                $('.hide-data input').removeClass("required");
                $('.hide-data').parent().find('label').addClass("active");
                if (response == -1 || response['status'] == false){
                    //
                    alertify.dismissAll();
                    notify(response.message, 'error', 10);
                } else {
                    $('.hide-data input').removeClass("required");
                    $('.hide-data').parent().find('label').addClass("active");

//                    buildBranchDashboardData();
                    manageUsersData();
                    closeBranchManageUser();

                    alertify.dismissAll();
                    notify(response.message, 'success', 10);
                }
            }})
        } else {
            alertify.dismissAll();
            notify('Validation Error', 'error', 10);
        }
    }
    function closeBranchManageUser_branchDashboard(){
        $('.hide-data span.required-msg').remove();
        $('.hide-data input').removeClass("required");
        $(".hide-data").slideUp();
        $('#userBranchId').val('0');
        $('.autocomplete-content').remove();
        $('#autocomplete').val('');
        $('#days').val('');
        $('.hide-data label').removeClass("active");
    }
</script>