<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
<input type="hidden" id="branchId" name="branchId" value="0"/>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
<div class="content-header-wrap">
    <h3 class="content-header">Branches </h3>
    <div class="content-header-btnwrap">
        <ul>
            <li access-element="add"><a class="tooltipped" data-position="left" data-tooltip="Add Branch" class="viewsidePanel" onclick="getBranchDetails(this, 0)"><i class="icon-plus-circle" ></i></a></li>
            <!--<li><a></a></li>-->
        </ul>
    </div>
</div>
<!-- InstanceEndEditable -->
<div class="content-body" id="contentBody" access-element="list"> <!-- InstanceBeginEditable name="contentBody" -->
<div class="fixed-wrap clearfix">
    <div class="col-sm-12 p0 mb15 branches-list-view">
        <div class="batch-info-wrap">
            <div class="batch-info branches-border branch-green-bg-light mr5">
                <div class="branch-iocn-wrap branch-green-bg"><span class="branch-iocn-wrap-img green-bg"><img src="<?php echo BASE_URL; ?>assets/images/branchlist1.png" alt=""/></span><span class="branch-iocn-wrap-text">Branches</span></div>
                <div class="branch-info-right clearfix mCustomScrollbar">
                    <div class="border-bottom clearfix branch-info-right-text">
                        <div class="batch-text">Active</div>
                        <div class="batch-num" id="activeBranches">&nbsp;</div>
                    </div>
                    <div class="border-bottom clearfix branch-info-right-text">
                        <div class="batch-text">In-Active</div>
                        <div class="batch-num" id="inactiveBranches">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="batch-info-wrap" >
            <div class="batch-info branches-border branch-blue-bg-light mr5">
                <div class="branch-iocn-wrap branch-blue-bg"><span class="branch-iocn-wrap-img blue-bg"><img src="<?php echo BASE_URL; ?>assets/images/branchlist2.png" alt=""/></span><span class="branch-iocn-wrap-text">Enrolled Students</span></div>
                <div class="branch-info-right clearfix mCustomScrollbar" id="studentCourses">

                </div>
            </div>
        </div>
        <div class="batch-info-wrap" access-element="fee dues">
            <div class="batch-info branches-border branch-red-bg-light mr5" id="Fee-Due">
                <div class="branch-iocn-wrap branch-red-bg"><span class="branch-iocn-wrap-img red-bg"><img src="<?php echo BASE_URL; ?>assets/images/branchlist3.png" alt=""/></span><span class="branch-iocn-wrap-text">Fee Due</span></div>
                <div class="branch-info-right clearfix mCustomScrollbar" id="Fee-Due-List">
                    
                </div>
            </div>
        </div>
        <div class="batch-info-wrap" access-element="student types">
            <div class="batch-info branches-border branch-yellow-bg-light" id="Student-Types">
                <div class="branch-iocn-wrap branch-yellow-bg"><span class="branch-iocn-wrap-img yellow-bg"><img src="<?php echo BASE_URL; ?>assets/images/branchlist4.png" alt=""/></span><span class="branch-iocn-wrap-text">Student Types</span></div>
                <div class="branch-info-right clearfix mCustomScrollbar" id="Student-Types-List">
                    
                </div>

            </div>
        </div>
    </div>

    <div class="col-sm-12 p0">
        <table class="table table-inside table-custom" id="branches-list">
            <thead>
            <tr>
                <th rowspan="2" class="valign-middle">Branch</th>
                <th rowspan="2" class="valign-middle">Courses </th>
                <th colspan="3" class="text-center">Current Batch</th>
                <th colspan="2" class="text-center">Next Batch</th>
                <th rowspan="2" class="valign-middle line-height21" width="7%">Overall Students (All Batches)</th>
            </tr>
            <tr>
                <th>Academic Start Date</th>
                <th>Marketing End Date</th>
                <th>Enrolled Students</th>
                <th>Academic Start Date</th>
                <th class="bborder-right">Marketing Start Date</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
<!--Modal-->
<div class="modal fade" id="branchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeBranchModal(this)"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Create Branch</h4>
            </div>
            <form action="/" method="post" id="BranchForm" novalidate="novalidate">
                <div class="modal-body modal-scroll clearfix">
                    <div class="col-sm-12">
                        <div class="input-field">
                            <input id="name" type="text" name="name" class="validate" class="formSubmit" required>
                            <label for="name">Branch Name <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <input id="code" type="text" class="validate" name="code" class="validate" class="formSubmit">
                            <label for="code">Branch Code <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <select id="branchHead" name="branchHead" class="validate" class="formSubmit">
                                <option value="" disabled selected>--Select--</option>
                            </select>
                            <label class="select-label">Branch Head <em>*</em></label>
                        </div>
                    </div>
                    <!--Changed on 16Mar2016-->
                    <!--<div id="displayImage"></div>-->
                    <div class="col-sm-12">
                        <div class="file-field input-field">
                            <div class="btn file-upload-btn">
                                <span>Upload Branch Photo</span>
                                <input type="file" id="imageUpload">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" placeholder="Upload Image" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <textarea id="address" class="materialize-textarea" name="address" class="validate" class="formSubmit"></textarea>
                            <label for="address">Branch Address <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-field">
                            <select id="country" name="country" class="validate" class="formSubmit">
                                <option selected >--Select--</option>
                            </select>
                            <label class="select-label">Country <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-field">
                            <select id="state" name="state" class="validate" class="formSubmit">
                                <option value="" selected>--Select--</option>
                            </select>
                            <label class="select-label">State <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-field">
                            <select id="city" name="city" class="validate" class="formSubmit">
                                <option value="" selected>--Select--</option>
                            </select>
                            <label class="select-label">Cities <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-field">
                            <input onkeypress="return isNumberKey(event)" maxlength='6' id="zip" type="text" class="validate" name="zip" class="validate" class="formSubmit">
                            <label for="zip">Zip <em>*</em></label>
                        </div>

                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <select id="course" multiple name="course" class="multiple formSubmit">
                                <option value="" >--Select All--</option>
                            </select>
                            <label class="select-label">Courses <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <select id="primaryCity" multiple name="primaryCity" class="multiple formSubmit">
                                <option value="" >--Select All--</option>
                            </select>
                            <label class="select-label">Primary Cities <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <select id="secondaryCity" multiple name="secondaryCity" class="multiple formSubmit">
                                <option value="" >--Select All--</option>
                            </select>
                            <label class="select-label">Secondary Cities</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-field">
                            <input onkeypress="return isPhoneNumber(event)" maxlength='15' id="phone" type="text" class="validate" name="phone" class="validate" class="formSubmit">
                            <label for="phone">Phone <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-field">
                            <input id="email" type="email" class="validate" name="email" class="validate" class="formSubmit">
                            <label for="email">Email <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-field">
                            <input id="lsequence" onkeypress="return isNumberKey(event)" type="text"  class="validate" name="lsequence" class="validate" class="formSubmit">
                            <label for="lsequence">Lead Sequence <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="input-field">
                            <input id="bsequence" onkeypress="return isNumberKey(event)" type="text" class="validate" name="bsequence" class="validate" class="formSubmit">
                            <label for="bsequence">Batch Sequence <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <textarea id="desc" class="materialize-textarea" name="desc" class="validate" class="formSubmit"></textarea>
                            <label for="desc">Description</label>
                        </div>
                    </div>
                </div>

            </form>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button id="actionButton" type="submit" class="btn blue-btn" onclick="AddBranch(this)"><i class="icon-right mr8"></i>Add</button>
                <button type="button" class="btn blue-light-btn" onclick="closeBranchModal(this)"><i class="icon-times mr8"></i>Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- InstanceEndEditable -->
</div>
</div>
<script type="text/javascript">
    docReady(function(){
        $('.BranchName-dropdown').html('');
        branchPageLoad();
        branchesList();
        if($("#Fee-Due").length != 0) {
            var userId = $('#userId').val();
            var action = 'fee dues';
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

            var ajaxurl=API_URL+'index.php/Branch/feeDueWidget';
            var params = {};
            commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
                var resp = '';
                resp += '<div class="border-bottom clearfix branch-info-right-text">';
                        resp += '<div class="batch-text">Retail</div>';
                        resp += '<div class="batch-num">'+ moneyFormat(response.data[0].retail) +'</div>';
                resp += '</div>';
                resp += '<div class="border-bottom clearfix branch-info-right-text">';
                        resp += '<div class="batch-text">Corporate</div>';
                        resp += '<div class="batch-num">'+ moneyFormat(response.data[0].corporate) +'</div>';
                resp += '</div>';
                resp += '<div class=" clearfix branch-info-right-text">';
                        resp += '<div class="batch-text">University</div>';
                        resp += '<div class="batch-num">'+ moneyFormat(response.data[0].institute) +'</div>';
                resp += '</div>';
                $('#Fee-Due-List').html(resp);
            }});
        }
        
        if($("#Student-Types").length != 0) {
            var userId = $('#userId').val();
            var action = 'student types';
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

            var ajaxurl=API_URL+'index.php/Branch/studentTypeWidget';
            var params = {};
            commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
                var resp = '';
                resp += '<div class="border-bottom clearfix branch-info-right-text">';
                        resp += '<div class="batch-text">Retail</div>';
                        resp += '<div class="batch-num">'+ response.data[0].retail +'</div>';
                resp += '</div>';
                resp += '<div class="border-bottom clearfix branch-info-right-text">';
                        resp += '<div class="batch-text">Corporate</div>';
                        resp += '<div class="batch-num">'+ response.data[0].corporate +'</div>';
                resp += '</div>';
                resp += '<div class=" clearfix branch-info-right-text">';
                        resp += '<div class="batch-text">University</div>';
                        resp += '<div class="batch-num">'+ response.data[0].institute +'</div>';
                resp += '</div>';
                $('#Student-Types-List').html(resp);
            }});
        }
    });
    function branchWidgets(widgetsResponse)
    {
        if(widgetsResponse.status==true)
        {
            //Branches related data
            $("#activeBranches").html(widgetsResponse.data['branches'].active);
            $("#inactiveBranches").html(widgetsResponse.data['branches'].inActive);

            //Enrolled students
            var enrolled_students='';
            if(widgetsResponse.data['enrolled_students'].length>0)
            {
                for(var s=0;s<widgetsResponse.data['enrolled_students'].length;s++)
                {
                    enrolled_students += '<div class="border-bottom clearfix branch-info-right-text">';
                    enrolled_students += '<div class="batch-text">' + widgetsResponse.data['enrolled_students'][s].name + '</div>';
                    enrolled_students += '<div class="batch-num">' + widgetsResponse.data['enrolled_students'][s].student + '</div>';
                    enrolled_students += '</div>';
                }
            }
            $("#studentCourses").html(enrolled_students);

        }
    }
    function branchPageLoad()
    {
        var userId = $('#userId').val();
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

        var ajaxurl=API_URL+'index.php/Branch/getBranchDashboardWidget';
        var params = {};
        commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:branchWidgets});
    }
    function branchesList()
    {
//        $(document).ready(function() {
        var ajaxurl=API_URL+'index.php/Branch/getBranchesList';
        var userID=$('#userId').val();
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#branches-list").dataTable().fnDestroy();
        $tableobj = $('#branches-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }


                verifyAccess();
                buildpopover();
                var branch_classes='',branchcode='',flag= 0,td_flag=0,td_branchcode='',td_custom_branchcode='';
                $('#branches-list a.branch_code').each(function()
                {
                    $(this).parent().parent().addClass('rowspan-group-tr');
                    branch_classes=($(this).attr("class")).split(" ");
                    branchcode = branch_classes[branch_classes.length-1];
                    if(flag == 0)
                        td_branchcode=branchcode;
                    if(branchcode == td_branchcode)
                    {
                        //td_flag++;
                    }
                    else
                    {
                        td_flag++;
                        td_branchcode=branchcode;
                    }
                    flag++;
                    td_custom_branchcode=branchcode+'_'+td_flag;
                    $(this).parent().addClass('rowspan-edit');
                    $(this).parent().attr("data-branch",td_custom_branchcode);
                    $(this).parent().addClass(td_custom_branchcode);
                    $(this).parent().addClass(branchcode);
                });
                MergeBranchCourses();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No branches found",
                "sZeroRecords": "No branches found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            "bProcessing": true,
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            columns: [
                {
                    data: null,className:"branch_code text-center",render: function ( data, type, row )
                    {
                        //return '<a  class="font-bold branch_code '+data.branch_code+'" href="'+BASE_URL+'app/branchinfo/'+data.branch_id+'"> '+data.branch_code+'</a>';
                        return '<a class="font-bold text-uppercase branch_code '+data.branch_code+'" href="'+BASE_URL+'app/branchinfo/'+data.branch_id+'"> '+data.branch_name+'<span class="display-block">'+data.branch_code+'<i class="ml5 fa fa-tag"></i></span></a>';
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.course_name;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.current_acadamics_start;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.current_marketing_end;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.current_enrolled_students;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.next_acadamics_start;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.next_marketing_start;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var b_status = data.branch_status==1?'Inactive':'Active';
                    b_status='Branch '+b_status;
                    var c_status = data.branch_course_status==1?'Inactive':'Active';
                    c_status='Course '+c_status;

                    var branchActions=data.all_students;
                    branchActions += '<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                    branchActions += '<a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    branchActions += '<i class="fa fa-ellipsis-v f18 pr10 pl10"></i>';
                    branchActions += '</a>';
                    branchActions += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    branchActions += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    branchActions += '<li><a href="javascript:;" access-element="edit" onclick="manageBranch(this, \''+data.branch_id+'\')">Edit</a></li>';
                    branchActions += '<li><a href="javascript:;" access-element="status" onclick="changeStatusBranch(this, \''+data.branch_id+'\', '+data.branch_status+')">'+b_status+'</a></li>';
                    branchActions += '<li><a href="javascript:;" access-element="status"  onclick="changeBranchCourseStatus(this, \''+data.branch_id+'\', \''+data.course_id+'\', \''+data.branch_course_status+'\')">'+c_status+'</a></li>';
                    branchActions += '</ul>';
                    branchActions += '</div>';
                    return branchActions;
                }
                }
            ],
            "createdRow": function ( row, data, index )
            {
                if(data.branch_status == 0 || data.branch_course_status == 0)
                {
                    $(row).addClass('disabled-branchview');
                }
            }
        } );
        DTSearchOnKeyPressEnter();
    }

    function MergeBranchCourses()
    {
        var table_td_text='',flag= 0,td_flag=0,counter = 0,total_records=$('table#branches-list tr td.branch_code').length,td_indexflag=0;
        $('table#branches-list tr td.branch_code').each(function()
        {
            //var td_text = $(this).text();
            var td_text = $(this).attr("data-branch");
            if(flag == 0)
                table_td_text=td_text;
            if(td_text == table_td_text)
            {
                if(td_indexflag == 0)
                    td_indexflag=flag;
                td_flag=0;
                counter++;
            }
            else
            {
                td_flag=1;
            }
            flag++;
            if(flag == total_records || td_flag == 1)
            {
                $('table#branches-list tr td.'+ $.trim(table_td_text)).prop('rowspan',counter);
                $('table#branches-list tr td.'+$.trim(table_td_text)+':not(:first)').remove();
                table_td_text=td_text;
                counter=1;
                td_indexflag=0;
            }
        });
    }
</script>