<div class="contentpanel">
<input type="hidden" id="branchId" value="<?php echo $Id ?>" />
<input type="hidden" id="branchUEId" value="0" />
<input type="hidden" id="batchId" value="0" />
<input type="hidden" id="userBranchId" value="0" />
<input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
<div class="content-header-wrap">
    <h3 class="content-header">No Branch Found</h3>
    <div class="content-header-btnwrap">
        <ul>
            <li><a class="tooltipped" data-position="left" data-tooltip="Close" href="<?php echo BASE_URL; ?>app/branches" class="text-dec-none"><i class="icon-times"></i></a></li>
            <!--<li><a data-toggle="modal" data-target="#filters" class=""><i class="icon-filter"></i></a></li>-->
            <!--<li><a href="javascript:;" onclick="openBranchFilter(this)"><i class="icon-filter"></i></a></li>-->
            <li access-element="batch add" id="addBatch"><a data-position="left" data-tooltip="Add Batch" class="viewsidePanel tooltipped" onclick="branchBatchInfo(this)"><i class="icon-plus-circle" ></i></a></li>
            <!-- Removed on 23 Mar 2016 -->
            <!--<li><a href="javascript:;" class="branch-head-data">Branch Head</a></li>-->
            <!--<li><a href="javascript:;" class="super-admin-data">Super Admin</a></li>-->
        </ul>
    </div>
</div>
<!-- InstanceEndEditable -->
<div class="content-body" id="contentBody">
<!-- InstanceBeginEditable name="contentBody" -->
<div class="fixed-wrap clearfix">
    <div class="col-sm-12 p0">
        <div class="col-sm-8 batch-data">
            <div class="clearfix">
                <div class="branch-img-wrap pull-left"> <img id="branchImage" src="<?php echo BASE_URL; ?>assets/images/branch-default.jpg" alt=""/> </div>
                <div class="branch-info-wrap">

                </div>
            </div>
        </div>
        <div class="col-sm-4 pr0"> <img src="<?php echo BASE_URL; ?>assets/images/graph.png" class="max-width100p"> </div>
    </div>
    <div class="col-sm-12 p0 branches-info">

    </div>
    <div class="col-sm-9 p0 mt2 batch-dtables">
        <div class="upcoming-batch-wrapper">
        <!--<h4 class="heading-uppercase">Upcoming Batch</h4>-->
        <table class="table table-responsive table-striped table-custom mt5 mp" id="branch-upcoming-batch">
            <thead>
            <tr class="">
                <th>Batch</th>
                <th>Academic Start</th>
                <th>Marketing Start</th>
                <th>Marketing End</th>
                <th>Dropouts</th>
                <th class="text-right">Due/Overdue</th>
                <th>Min Target</th>
                <th>Enrolled</th>
            </tr>
            </thead>
        </table>
        </div>
        <div class="current-batch-wrapper">
        <!--<h4 class="heading-uppercase">Current Batch</h4>-->
        <table class="table table-responsive table-striped table-custom mt10" id="branch-current-batch">
            <thead>
            <tr class="">
                <th>Batch</th>
                <th>Academic Start</th>
                <th>Marketing Start</th>
                <th>Marketing End</th>
                <th>Dropouts</th>
                <th class="text-right">Due/Overdue</th>
                <th>Min Target</th>
                <th>Enrolled</th>
            </tr>
            </thead>
        </table>
        </div>
        <div class="completed-batch-wrapper">
        <!--<h4 class="heading-uppercase">Completed Batch</h4>-->
        <table class="table table-responsive table-striped table-custom mt10" id="branch-completed-batch">
            <thead>
            <tr class="">
                <th>Batch</th>
                <th>Academic Start</th>
                <th>Marketing Start</th>
                <th>Marketing End</th>
                <th>Dropouts</th>
                <th class="text-right">Due/Overdue</th>
                <th>Min Target</th>
                <th>Enrolled</th>
            </tr>
            </thead>
        </table>
    </div>
    </div>
    <div class="col-sm-3 mt10 pr0">
        <div class="widget-right border border-blue">
            <div class="widget-header light-dark-blue relative">
                <h4 class="heading-uppercase f14 m0 p10">Manage Users <a access-element="manage users" data-position="left" data-tooltip="Manage Users" href="javascript:;" class="tooltipped pull-right slide-down"><i class="icon-plus-circle" ></i></a></h4>
            </div>
            <div class="widget-contnet bg-white clearfix">
                <div class="hide-data clearfix pb10" style="display:none;">
                    <div class="col-sm-12 mt10">
                        <div class="input-field">
                            <input id="autocomplete" class="validate autocomplete" type="text">
                            <label for="autocomplete">Name</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <input onkeypress="return isNumberKey(event)" maxlength="3" id="days" class="validate" type="text" >
                            <label for="days">No. of Days</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button class="btn blue-btn btn-xs" type="button" onclick="saveBranchUsers(this)"> <i class="icon-right mr8"></i> Save </button>
                        <button class="btn blue-light-btn btn-xs" onClick="closeBranchManageUser()" type="button"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
                <span id="branchActiveUser"></span>
<!--                <h5 id="branchActiveUser" class="heading-uppercase pl10">Active Members</h5>


                <h5 id="branchInActiveUser" class="heading-uppercase pl10">Inactive Members</h5>-->

            </div>
        </div>
    </div>
</div>
<!--Modal-->
<div class="modal fade" id="createBatch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeBatchModal()"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Create Batch</h4>
            </div>
            <form id="batchCreateForm">
                <div class="modal-body modal-scroll clearfix">
                    <div class="col-sm-12">
                        <div class="input-field">
                            <select multiple id="course">
                                <option value="" disabled selected>--Select--</option>
                            </select>
                            <label class="select-label">Courses <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label>Batch Code</label>
                        <div class="input-field mt0">
                            <input id="autogenerate" type="text" class="validate" value="" disabled="">
                            <label for="autogenerate" class="active">Auto generated</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <input id="txtAlias" type="text" class="validate">
                            <label for="txtAlias">Alias Name</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <label class="datepicker-label datepicker">Marketing Start Date <em>*</em></label>
                            <input id="msd" type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <label class="datepicker-label datepicker">Marketing End Date <em>*</em></label>
                            <input id="med" type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <input onkeypress="return isNumberKey(event)" id="target" type="text" class="validate">
                            <label for="target">Min Target <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <input onkeypress="return isNumberKey(event)" id="visitorTarget" type="text" class="validate">
                            <label for="visitorTarget">Visitor`s Target <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <label class="datepicker-label datepicker">Academic Start Date <em>*</em></label>
                            <input id="asd" type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <label class="datepicker-label datepicker">Academic End Date <em>*</em></label>
                            <input type="date" id="aed" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy">
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button id="actionButton" onclick="addBatch(this)" type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save</button>
                <button type="button" class="btn blue-light-btn" onclick="closeBatchModal()"><i class="icon-times mr8"></i>Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="filters" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="400">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Filters</h4>
            </div>
            <div class="modal-body modal-scroll clearfix">
                <div class="">
                    <ul class="accordian-container">
                        <li class="accordian-wrapper">
                            <div class="accordian-heading"> Branches <span class="accord-arrow"><i class="icon-angle-down"></i></span> </div>
                            <div class="accordian-content" id="branchFilter">
                                <div class="input-field">
                                    <input class="filled-in" id="nodata" type="checkbox">
                                    <label for="nodata">No Data Found</label>
                                </div>
                            </div>
                        </li>
                        <li class="accordian-wrapper">
                            <div class="accordian-heading"> Fee <span class="accord-arrow"><i class="icon-angle-down"></i></span> </div>
                            <div class="accordian-content">
                                <div class="input-field">
                                    <input class="filled-in" id="fee1" type="checkbox">
                                    <label for="fee1"> -- </label>
                                </div>
                                <div class="input-field">
                                    <input class="filled-in" id="fee2" type="checkbox">
                                    <label for="fee2"> -- </label>
                                </div>
                                <div class="input-field">
                                    <input class="filled-in" id="fee3" type="checkbox">
                                    <label for="fee3"> -- </label>
                                </div>
                                <div class="input-field">
                                    <input class="filled-in" id="fee4" type="checkbox">
                                    <label for="fee4"> -- </label>
                                </div>
                                <div class="input-field">
                                    <input class="filled-in" id="fee5" type="checkbox">
                                    <label for="fee5"> -- </label>
                                </div>
                            </div>
                        </li>
                        <li class="accordian-wrapper">
                            <div class="accordian-heading"> Course Name <span class="accord-arrow"><i class="icon-angle-down"></i></span> </div>
                            <div id="courseFilter" class="accordian-content">
                                <div class="input-field">
                                    <input class="filled-in" id="nodata" type="checkbox">
                                    <label for="nodata">No Data Found</label>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Apply</button>
                <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="batchedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Batch Edit</h4>
            </div>
            <div class="modal-body modal-scroll clearfix">
                <div class="col-sm-12">
                    <div class="input-field">
                        <select multiple>
                            <option value="" disabled selected>--Select--</option>
                            <option value="1">CPA</option>
                            <option value="2">CMA</option>
                            <option value="3">PGDA</option>
                        </select>
                        <label class="select-label">Courses</label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <label>Batch Code</label>
                    <div class="input-field mt0">
                        <input type="text" class="validate" id="disabled" value="" disabled="">
                        <label for="disabled" class="active">Auto generated</label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                        <label class="datepicker-label datepicker">Marketing Start Date</label>
                        <input type="date" class="datepicker relative" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                        <label class="datepicker-label datepicker">Marketing End Date</label>
                        <input type="date" class="datepicker relative" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                        <label class="datepicker-label datepicker">Academic Start Date</label>
                        <input type="date" class="datepicker relative" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                        <label class="datepicker-label datepicker">Academic End Date</label>
                        <input type="date" class="datepicker relative" placeholder="dd/mm/yyyy">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save Changes</button>
                <button type="button" class="btn blue-light-btn"><i class="icon-times mr8"></i>Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- InstanceEndEditable --></div>
</div>


<script>
$context = 'Branches';
$pageUrl = 'branches';
$serviceurl = 'branches';
var glbBranchId='';
docReady(function () {
    getAllBranchInfoDetails();
    loadBranchUsersAutocompleteNew();   /*For autocomplete*/
});
$("#userBranchList" ).change(function() {
    glbBranchId = $('#userBranchList').val();
    getAllBranchInfoDetails();
    closeBranchManageUser();
});

function getAllBranchInfoDetails(){
    branchInfoPageLoad();
    buildBatchDataTable();
}

function buildBatchDataTable() {
    $Counter = 0;
    var branch = glbBranchId;
    if(glbBranchId.length <= 0){
        branch = '<?php echo $Id ?>';
    }
    var ajaxurl = API_URL + 'index.php/Batch/getAllBatches/time/upcoming/branch_id/'+branch;
    var userID = $('#hdnUserId').val();
    var headerParams = {action: 'batch list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};

    $("#branch-upcoming-batch").dataTable().fnDestroy();
    var currentObject = $('#branch-upcoming-batch').DataTable({
        "fnDrawCallback": function () {
            $("#branch-upcoming-batch thead th").removeClass("icon-rupee");
            buildpopover();
            verifyAccess();
            var $api = this.api();
            var pages = $api.page.info().pages;
            if(pages > 1)
            {
                $('.upcoming-batch-wrapper .dataTables_paginate').css("display", "block");
                $('.upcoming-batch-wrapper .dataTables_length').css("display", "block");
                //$('.upcoming-batch-wrapper .dataTables_filter').css("display", "block");
            } else {
                $('.upcoming-batch-wrapper .dataTables_paginate').css("display", "none");
                $('.upcoming-batch-wrapper .dataTables_length').css("display", "none");
                //$('.upcoming-batch-wrapper .dataTables_filter').css("display", "none");
            }
        },
        "footerCallback": function ( row, data, start, end, display ) {
            if(data.length >0 && typeof data[0].counter !== 'undefined'){
                $Counter = data[0].counter;
            }else{
                $Counter = 0;
            }
            $('#branch-upcoming-batch_wrapper div.DTHeader').html('Upcoming Batches ('+$Counter+')');
        },
        dom: "<'DTHeader'>Bfrtip",
        bInfo: false,
        "serverSide": true,
        "bProcessing": true,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No upcoming batches found",
            "sZeroRecords": "No upcoming batches found",
            "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
        },
        ajax: {
            url: ajaxurl,
            type: 'GET',
            headers: headerParams,
            error: function (response) {
                DTResponseerror(response);
            }
        },
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false
        }],
        columns: [
            {
                data: null, render: function (data, type, row)
            {
                return '<a  class="font-bold" href="'+BASE_URL+'app/batch_details/<?php echo $Id ?>/'+data.batch_id+'"> '+data.code+'</a>';
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.acedemic_startdate;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.marketing_startdate;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.marketing_enddate;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.dropout;
            }
            },
            {
                data: null,className:"text-right icon-rupee", render: function (data, type, row)
            {
                return moneyFormat(data.due);
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.target;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                var dashboard = data.enrolled + ' ';
                if(data.branch_active == '1' && data.course_active == '1' && data.b_course_active == '1'){
                    dashboard += '<div class="dropdown feehead-list pull-right custom-dropdown-style"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v f18 plr10"></i> </a>';
                    dashboard += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    dashboard += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
    //                if(data.branch_active == '1' && data.course_active == '1' && data.b_course_active == '1'){
                        dashboard += '<li><a access-element="batch edit" href="javascript:;"  onclick="getBatchDetailsById(\'' + data.batch_id + '\')">Edit</a></li>';
    //                }
                    /*dashboard += '<li><a access-element="delete" class="tooltipped" data-position="top" data-tooltip="Delete Batch" href="javascript:;"  onclick="deleteBatch(\''+data.batch_id+'\')">Delete</a></li>';*/
    //                dashboard += '<li><a access-element="delete" class="tooltipped" data-position="top" data-tooltip="View 2" href="'+BASE_URL+'app/batch_details/<?php // echo $Id ?>/'+data.batch_id+'" ">View 2</a></li>';
                    dashboard += '</ul>';
                    dashboard += '</div>';
                }
                return dashboard;
            }
            }
        ],
        "createdRow": function (row, data, index)
        {
            if(data.branch_active == '0' || data.course_active == '0' || data.b_course_active == '0')
            {
                $(row).addClass('disabled-branchview');
            }
        }
    });
    MultipleDTSearchEnter(currentObject, "#branch-upcoming-batch");

    ajaxurl = API_URL + 'index.php/Batch/getAllBatches/time/current/branch_id/'+branch;
    $("#branch-current-batch").dataTable().fnDestroy();
    var currentObject = $('#branch-current-batch').DataTable({
        "fnDrawCallback": function () {
            $("#branch-current-batch thead th").removeClass("icon-rupee");
            buildpopover();
            verifyAccess();
            var $api = this.api();
            var pages = $api.page.info().pages;
            if(pages > 1)
            {
                $('.current-batch-wrapper .dataTables_paginate').css("display", "block");
                $('.current-batch-wrapper .dataTables_length').css("display", "block");
                //$('.current-batch-wrapper .dataTables_filter').css("display", "block");
            } else {
                $('.current-batch-wrapper .dataTables_paginate').css("display", "none");
                $('.current-batch-wrapper .dataTables_length').css("display", "none");
                //$('.current-batch-wrapper .dataTables_filter').css("display", "none");
            }
        },
        "footerCallback": function ( row, data, start, end, display ) {
            if(data.length >0 && typeof data[0].counter !== 'undefined'){
                $Counter = data[0].counter;
            }else{
                $Counter = 0;
            }
            $('#branch-current-batch_wrapper div.DTHeader').html('Current Batches ('+$Counter+')');
        },
        dom: "<'DTHeader'>Bfrtip",
        bInfo: false,
        "serverSide": true,
        "bProcessing": true,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No current batches found",
            "sZeroRecords": "No current batches found",
            "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
        },
        ajax: {
            url: ajaxurl,
            type: 'GET',
            headers: headerParams,
            error: function (response) {
                DTResponseerror(response);
            }
        },
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false
        }],
        columns: [
            {
                data: null, render: function (data, type, row)
            {
//                return data.code;
//                return '<a  class="font-bold" href="'+BASE_URL+'app/batchinfo/<?php echo $Id ?>/'+data.batch_id+'"> '+data.code+'</a>';
                return '<a  class="font-bold" href="'+BASE_URL+'app/batch_details/<?php echo $Id ?>/'+data.batch_id+'"> '+data.code+'</a>';
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.acedemic_startdate;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.marketing_startdate;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.marketing_enddate;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.dropout;
            }
            },
            {
                data: null,className:"text-right icon-rupee", render: function (data, type, row)
            {
                return moneyFormat(data.due);
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.target;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                var dashboard = data.enrolled + ' ';
                if(data.branch_active == '1' && data.course_active == '1' && data.b_course_active == '1'){
                dashboard += '<div class="dropdown feehead-list pull-right custom-dropdown-style"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v f18 plr10"></i> </a>';
                dashboard += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                dashboard += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
//                if(data.branch_active == '1' && data.course_active == '1' && data.b_course_active == '1'){
                    dashboard += '<li><a access-element="batch edit" href="javascript:;"  onclick="getBatchDetailsById(\'' + data.batch_id + '\')">Edit</a></li>';
//                }
                /*dashboard += '<li><a access-element="delete" class="tooltipped" data-position="top" data-tooltip="Delete Batch" href="javascript:;"  onclick="deleteBatch(\''+data.batch_id+'\')">Delete</a></li>';*/
//                dashboard += '<li><a access-element="list" class="tooltipped" data-position="top" data-tooltip="View 2" href="'+BASE_URL+'app/batch_details/<?php // echo $Id ?>/'+data.batch_id+'" ">View 2</a></li>';
                dashboard += '</ul>';
                dashboard += '</div>';
                }
                return dashboard;
            }
            }
        ],
        "createdRow": function (row, data, index)
        {
            if(data.branch_active == '0' || data.course_active == '0' || data.b_course_active == '0')
            {
                $(row).addClass('disabled-branchview');
            }
        }
    });
    MultipleDTSearchEnter(currentObject, "#branch-current-batch");

    ajaxurl = API_URL + 'index.php/Batch/getAllBatches/time/completed/branch_id/'+branch;
    $("#branch-completed-batch").dataTable().fnDestroy();
    var currentObject = $('#branch-completed-batch').DataTable({
        "fnDrawCallback": function () {
            $("#branch-completed-batch thead th").removeClass("icon-rupee");
            buildpopover();
            verifyAccess();
            var $api = this.api();
            var pages = $api.page.info().pages;
            if(pages > 1)
            {
                $('.completed-batch-wrapper .dataTables_paginate').css("display", "block");
                $('.completed-batch-wrapper .dataTables_length').css("display", "block");
                //$('.completed-batch-wrapper .dataTables_filter').css("display", "block");
            } else {
                $('.completed-batch-wrapper .dataTables_paginate').css("display", "none");
                $('.completed-batch-wrapper .dataTables_length').css("display", "none");
                //$('.completed-batch-wrapper .dataTables_filter').css("display", "none");
            }
        },
        "footerCallback": function ( row, data, start, end, display ) {
            if(data.length >0 && typeof data[0].counter !== 'undefined'){
                $Counter = data[0].counter;
            }else{
                $Counter = 0;
            }
            $('#branch-completed-batch_wrapper div.DTHeader').html('Completed Batches ('+$Counter+')');
        },
        dom: "<'DTHeader'>Bfrtip",
        bInfo: false,
        "serverSide": true,
        "bProcessing": true,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No completed batches found",
            "sZeroRecords": "No completed batches found",
            "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
        },
        ajax: {
            url: ajaxurl,
            type: 'GET',
            headers: headerParams,
            error: function (response) {
                DTResponseerror(response);
            }
        },
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false
        }],
        columns: [
            {
                data: null, render: function (data, type, row)
            {
//                return data.code;
//                return '<a  class="font-bold" href="'+BASE_URL+'app/batchinfo/<?php echo $Id ?>/'+data.batch_id+'"> '+data.code+'</a>';
                return '<a  class="font-bold" href="'+BASE_URL+'app/batch_details/<?php echo $Id ?>/'+data.batch_id+'"> '+data.code+'</a>';
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.acedemic_startdate;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.marketing_startdate;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.marketing_enddate;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.dropout;
            }
            },
            {
                data: null,className:"text-right icon-rupee", render: function (data, type, row)
            {
                return moneyFormat(data.due);
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                return data.target;
            }
            },
            {
                data: null, render: function (data, type, row)
            {
                var dashboard = data.enrolled + ' ';
                return dashboard;
            }
            }
        ],
        "createdRow": function (row, data, index)
        {
            if(data.branch_active == '0' || data.course_active == '0' || data.b_course_active == '0')
            {
                $(row).addClass('disabled-branchview');
            }
        }
    });
    MultipleDTSearchEnter(currentObject, "#branch-completed-batch");
    
    /*If no data found*/
//    $('#branch-current-batch_wrapper div.DTHeader').html('Current Batch (0)');
//    $('#branch-upcoming-batch_wrapper div.DTHeader').html('Upcoming Batch (0)');
//    $('#branch-completed-batch_wrapper div.DTHeader').html('Completed Batch (0)');
}

function branchInfoPageLoad() {
    var branch = glbBranchId;
    if(glbBranchId.length <= 0){
        branch = '<?php echo $Id ?>';
    }
    var userId = $('#hdnUserId').val();
    var action = 'batch list';
    var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

    var ajaxurl = API_URL + 'index.php/Branch/getBranchById';
    var params = {'branch_id': branch};
    commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: branchInfo});
}

function branchInfo(response) {
    var dashboard = '';
    if (response.data.length == 0 || response == -1 || response['status'] == false) {
        dashboard += '<div class="text-center">No Data found</div>';
    } else {
        $('#branchUEId').val(response.data.branchId);
        $('#branchId').val(response.data.branchIdEnc);
        
        if(response.data.branch_active == '0'){
            $('#addBatch').hide();
        }
        $('.content-header').html(response.data.branchName+'<span class="sky-blue">(' + response.data.branchCode + ')</span>');
        $('.super-admin-data').html(response.data.userName);
        var imgurl = BASE_URL + 'uploads/branches/' + response.data.branchImage;
        if (response.data.branchImage && CheckFileExists(imgurl)) {
            $('#branchImage').attr('src', BASE_URL + 'uploads/branches/' + response.data.branchImage);
        } else {
            $('#branchImage').attr('src', BASE_URL + 'assets/images/branch-default.jpg');
        }
        
        if(glbBranchId.length <= 0){
            $('#userBranchList option:contains(' + response.data.branchName + ')').each(function(){
                if ($(this).text() == response.data.branchName) {
                    $(this).prop('selected', 'selected');
                }
            });
            $('#userBranchList').material_select();
        }
        glbBranchId = response.data.branchId;
        dashboard = '';
        dashboard += '<div class="col-sm-8 p0">';
            dashboard += '<div class="col-sm-6 mtb10">';
              dashboard += '<p class=" label-text">Branch Head</p>';
              dashboard += '<label class="label-data ellipsis">' + response.data.userName + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-6 mtb10">';
              dashboard += '<p class=" label-text">Phone No.</p>';
              dashboard += '<label class="label-data ellipsis">' + response.data.branchPhone + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-6 mtb10">';
              dashboard += '<p class=" label-text">Branch Email</p>';
              dashboard += '<label class="label-data ellipsis">' + response.data.branchEmail + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-6 mtb10">';
              dashboard += '<p class=" label-text">Branch Head Email</p>';
              dashboard += '<label class="label-data ellipsis">' + response.data.userEmailId + '</label>';
            dashboard += '</div>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-4 mtb10 p0">';
              dashboard += '<p class=" label-text">Address</p>';
              dashboard += '<label class="label-data" style="word-wrap:break-word;">' + response.data.branchAddress + '</label>';
            dashboard += '</div>';
        
        $('.branch-info-wrap').html(dashboard);
        dashboard = '';
        for (var index in response.data.courses) {
            dashboard += '<div class="col-sm-4 p0 relative">';
            dashboard += '<div class="mt5">';
            dashboard += '<h4 class="heading-uppercase mb5 pl5 display-inline-block">' + response.data.courses[index]['name'] + '</h4>';
            dashboard += '<div class="pull-right input-field search-filter custom-select m0 pr3">';
        
            var courseDropdown = '<select onchange="branchCourseSelect(this, '+response.data.courses[index]['course_id']+')" class="branchCourse">';
            courseDropdown += '<option value="all" selected>All</option>';
            var courseCode = response.data.courses[index]['branch_code'];
            for (var a in courseCode) {

		if(courseCode[a].length >0 ){
                    var bathcodesplit=courseCode[a].split("-");
                	courseDropdown += '<option value="'+bathcodesplit[0]+'">'+bathcodesplit[0]+'</option>';
                }
            }
            courseDropdown += '</select>';
            
            dashboard +=  courseDropdown;
            
            dashboard +=  '</div><!-- div Pr3 Ends -->'
                + ' </div><!-- div mt5 Ends -->'
                + '<div class=" p0 student-custom <?php if (strtolower($userData['Role']) == 'superadmin') {
    echo 'addpaid';
} ?> ">'
                + '<div class="student-wrap p0 display-inline-block">'
                + '<div class="students-info clearfix white-bg">'
                + '<h4 class="text-center">Students</h4>'
                + '<div class="student-details clearfix">'
                + '<div class=" student-icon pl5"> <i class="icon-users1"></i> </div>'
                + '<div class=" student-data p0">'
                + '<p class="label-text p-clr">Enrolled</p>'
                + '<label class="label-data f16 ">' + response.data.courses[index]['enrolled'] + '</label>'
                + '</div>'
                + '<div class="student-data">'
                + '<p class="label-text p-clr">Dropouts</p>'
                + '<label class="label-data f16 ">' + response.data.courses[index]['dropout'] + '</label>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<div class="students-info clearfix white-bg">'
                + '<h4 class="text-center">Due/Overdue</h4>'
                + '<div class="student-details clearfix">'
                + '<div class=" student-icon pl5"> <i class="fa fa-thumbs-o-down"></i> </div>'
                + '<div class=" student-data p0">'
                + '<p class="label-text p-clr">Amount</p>'
                + '<label class="label-data f16">' + moneyFormat(response.data.courses[index]['amountDue']) + '</label>'
                + '</div>'
                + '<div class="student-data">'
                + '<p class="label-text p-clr">Percentage</p>'
                + '<label class="label-data f16 ">' + Math.floor(parseFloat(response.data.courses[index]['amountDuePercentage'])) + '%</label>'
                + '</div>'
                + ' </div>'
                + '</div>'
                + '</div>'
                <?php if (strtolower($userData['Role']) == 'superadmin') { ?>
                + '<div class="student-paid p0">'
                + '<div class="paid-info  white-bg ml0" style="display:block;">'
                + '<h4 class="text-center">Paid</h4>'
                + '<div class="paid-icon text-center"> <i class="icon-file"></i> </div>'
                + '<div class="paid-data pt10">'
                + '<p class="label-text p-clr text-center">Amount</p>'
                + '<label class="label-data f16 text-center ">' + moneyFormat(response.data.courses[index]['totalAmount']) + '</label>'
                + '</div>'
                + '</div>'
                + '</div>'
                <?php } ?>
                + '</div>'
                + '</div>';
        }
        $('.branches-info').html(dashboard);
        /* Active User*/
        var activeUser = '';
        if(typeof response.data.user!='undefined' && typeof response.data.user.active !='undefined'){
            activeUser = '<h5 class="heading-uppercase pl10">Active Members</h5>';
            for (var index in response.data.user.active) {
                activeUser += '<div class="col-sm-12 mtb10 userworking-info-wrap c-userworking-info-wrap">';
                if(response.data.user.active[index]['role_name'] == 'Superadmin'){
                    activeUser += '';   /*No option for any superadmin*/
                }else{
                    if(('<?php echo $userData['Role']; ?>'=='Superadmin' || '<?php echo $userData['Role']; ?>'=='Branch Head') && response.data.user.active[index]['user_id']!=<?php echo $userData['userId']; ?>){
                        activeUser += '<div class="dropdown feehead-list pull-right custom-dropdown-style"> <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="javascript:;" data-target="#" id="dLabel"> <i class="fa fa-ellipsis-v f18 plr10"></i> </a>';
                        activeUser += '<ul aria-labelledby="dLabel" class="dropdown-menu pull-right">';
                        activeUser += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        activeUser += '<li access-element="manage users"><a onclick="extendBranchUsers(\''+response.data.user.active[index]['user_id']+'\', \''+response.data.user.active[index]['name']+'\',this)" href="javascript:;">Extend</a></li>';
                        activeUser += '<li access-element="manage users"><a onclick="deactiveBranchUsers(\''+response.data.user.active[index]['user_id']+'\', this)" href="javascript:;">Deactivate</a></li>';
                        activeUser += '</ul>';
                        activeUser += '</div>';
                    }
                }
                var userImage='';
                if(response.data.user.active[index]['image'])
                {
                    userImage += '<img src="'+response.data.user.active[index]['image']+'" class="mCS_img_loaded">';

                }
                else
                {
                    userImage += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png" class="mCS_img_loaded">';
                }
                activeUser += '<div class="userworking-img"> '+userImage+' </div>';
                activeUser += '<div class="pull-left pl5 inline-lable">';
                activeUser += '<label class="label-data ellipsis">' + response.data.user.active[index]['name'] + '</label>';
                activeUser += '<p class="label-text ellipsis">' + response.data.user.active[index]['role_name'] + '</p>';
                
                activeUser += '<div class="label-hr col-sm-12 p0">';
                activeUser += '<label class="label-text ellipsis">Added on : </label>';
                activeUser += '<span>' + response.data.user.active[index]['alotted_date'] + '</span> </div>';
                activeUser += '</div>';
                if (response.data.user.active[index]['days'] != 0) {
                    activeUser += '<span title="Number of days left" class="usersdays-left">' + response.data.user.active[index]['daysLeft'] + '</span>';
                }else if(response.data.user.active[index]['days'] === undefined){
                    activeUser += '<span title="Number of days left" class="usersdays-left"> -- </span>';
                }
                activeUser += '</div>';
            }
        }
        if(typeof response.data.user!='undefined' && typeof response.data.user.inactive !== 'undefined' && response.data.user.inactive.length > 0){
            activeUser += '<h5 class="heading-uppercase pl10">Inactive Members</h5>';
            for (var index in response.data.user.inactive) {
                var userImage='';
                if(response.data.user.inactive[index]['image'] && CheckFileExists(response.data.user.inactive[index]['image']))
                {
                    userImage += '<img src="'+response.data.user.inactive[index]['image']+'" class="mCS_img_loaded">';

                }
                else
                {
                    userImage += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png" class="mCS_img_loaded">';
                }
                activeUser += '<div data-trigger="hover" data-placement="top" data-toggle="popover" class="col-sm-12 mtb10 userworking-info-wrap c-userworking-info-wrap inactive-users popper" data-original-title="' + response.data.user.inactive[index]['name'] + '" title="' + response.data.user.inactive[index]['name'] + '">'
                    + '<div class="userworking-img"> '+userImage+' </div>'
                    + '<div class="pull-left pl5 pt5">'
                    + '<label class="label-data ellipsis">' + response.data.user.inactive[index]['name'] + '</label>'
                    + '<p class="label-text ellipsis">' + response.data.user.inactive[index]['value'] + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="popper-content hide">'
                    + '<div class="plr15"><p>'
                    + '<label class="popover-label ash f14">From :</label>'
                    + response.data.user.inactive[index]['created_date'] + '</p>'
                    + '<p>'
                    + '<label class="popover-label ash f14">To :</label>'
                    + response.data.user.inactive[index]['end_date'] + '</p>'
                    + '<p>'
                    + '<label class="popover-label ash f14">Days :</label>'
                    + response.data.user.inactive[index]['inactiveDays'] + '</p>'
                    + '</div></div>';
            }
        }
        $('#branchActiveUser').html();
        $('#branchActiveUser').html(activeUser);
        
        buildpopover();
        $('.branchCourse').material_select();
    }
}

function branchCourseSelect(a, courseId){
    notify('Processing..', 'warning', 10);
    var selectedVal = $(a).val();
    var branchId = $('#userBranchList').val();
    /*if(glbBranchId.length <= 0){
        branch = '<?php //echo $Id ?>';
    }*/
    var userId = $('#hdnUserId').val();
    var action = 'batch list';
    var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

    var ajaxurl = API_URL + 'index.php/Batch/getStudentDetailsByBatch';
    var params = {'batch_id': selectedVal, 'branch_id': branchId, 'courseId': courseId};
    commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
        var rawHtml = '';
        if (response.data.length == 0 || response == -1 || response['status'] == false) {
//            rawHtml += '<center>No Data found</center>';
        } else {
            rawHtml += '<div class="student-wrap p0 display-inline-block">'
            rawHtml += '<div class="students-info clearfix white-bg">'
                    + '<h4 class="text-center">Students</h4>'
                    + '<div class="student-details clearfix">'
                    + '<div class=" student-icon pl5"> <i class="icon-users1"></i> </div>'
                    + '<div class=" student-data p0">'
                    + '<p class="label-text p-clr">Enrolled</p>'
                    + '<label class="label-data f16 ">' + response.data.enrolled + '</label>'
                    + '</div>'
                    + '<div class="student-data">'
                    + '<p class="label-text p-clr">Dropouts</p>'
                    + '<label class="label-data f16 ">' + response.data.dropout + '</label>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '<div class="students-info clearfix white-bg">'
                    + '<h4 class="text-center">Due/Overdue</h4>'
                    + '<div class="student-details clearfix">'
                    + '<div class=" student-icon pl5"> <i class="fa fa-thumbs-o-down"></i> </div>'
                    + '<div class=" student-data p0">'
                    + '<p class="label-text p-clr">Amount</p>'
                    + '<label class="label-data f16">' + moneyFormat(response.data.amountDue) + '</label>'
                    + '</div>'
                    + '<div class="student-data">'
                    + '<p class="label-text p-clr">Percentage</p>'
                    + '<label class="label-data f16 ">' + Math.floor(parseFloat(response.data.amountDuePercentage)) + '%</label>'
                    + '</div>'
                    + ' </div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    <?php if (strtolower($userData['Role']) == 'superadmin') { ?>
                    + '<div class="student-paid p0">'
                    + '<div class="paid-info  white-bg ml0" style="display:block;">'
                    + '<h4 class="text-center">Paid</h4>'
                    + '<div class="paid-icon text-center"> <i class="icon-file"></i> </div>'
                    + '<div class="paid-data pt10">'
                    + '<p class="label-text p-clr text-center">Amount</p>'
                    + '<label class="label-data f16 text-center ">' + moneyFormat(response.data.totalAmount) + '</label>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    <?php } ?>
                    + '</div>';
            alertify.dismissAll();
            $(a).parent().parent().parent().parent().find('.student-custom').addClass('<?php if (strtolower($userData['Role']) == 'superadmin') { echo 'addpaid';}else{ echo ''; } ?>');
            $(a).parent().parent().parent().parent().find('.student-custom').html(rawHtml);
        }
    }});
}
</script>
