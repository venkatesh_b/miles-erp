<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 17/2/16
 * Time: 12:31 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Call Order Confirmation</h3>
        <div class="content-header-btnwrap">
            <ul>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <h4 class="heading-uppercase">Leads Stage</h4>
            <div class="col-sm-12 p0" access-element="list">
                <table class="table table-responsive table-striped table-custom table-info">
                    <thead>
                    <tr>
                        <th>Current Batch</th>
                        <th class="text-center">Day</th>
                        <th class="text-center">Probability</th>
                        <th>Old Batch</th>
                        <th class="text-center">Day</th>
                        <th class="text-center">Probability</th>
                    </tr>
                    <tr><td class="border-none p0 lh10">&nbsp;</td></tr>
                    </thead>
                    <tbody id="lead_stages">

                    </tbody>
                </table>
                <div class="fixed-wrap clearfix">
                    <h4 class="heading-uppercase">DND Configuration</h4>
                    <div class="col-sm-12 p0">
                        <div class="col-sm-12 p0" access-element="list">
                            <div class="col-sm-1 p0">
                                <div class="input-field">
                                    <input name="dndConfiguration"  id="txtdndConfiguration" type="text" value="" maxlength="3" onkeypress="return isNumberKey(event)"/>
                                    <!--<label for="txtdndConfiguration" class="select-label">DND CONFIGURATION <em>*</em></label>-->
                                </div>

                                <!--<label for="txtdndConfiguration"> <em>*</em></label>-->
                            </div>
                        </div>
                        <div class="col-sm-4 p0"> <button onclick="saveDndConfiguration(this)" type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Update</button></div>
                    </div>
                </div>
            </div>
            <!--<div class="col-sm-12 p0"> <button onclick="saveLeadStages(this)" type="Submit" class="btn blue-btn"><i class="icon-right mr8"></i>Update</button></div>-->
        </div>

        <!--<div class="fixed-wrap clearfix">
            <h4 class="heading-uppercase">DND Configuration</h4>
            <div class="col-sm-12 p0">
            <div class="col-sm-12 p0" access-element="list">
                <div class="col-sm-1 p0">
                <input name="dndConfiguration"  id="txtdndConfiguration" type="text" value="" maxlength="3" onkeypress="return isNumberKey(event)">
                <label for="txtdndConfiguration"> <em>*</em></label>
                </div>
            </div>
            <div class="col-sm-4 p0"> <button onclick="saveDndConfiguration(this)" type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Update</button></div>
            </div>
          </div>-->

        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    $context = 'Call Order Confirmation';
    $pageUrl = 'callorderconfirmation';
    $serviceurl = 'callorderconfirmation';
    docReady(function () {
        leadLeadStagesLoad();
        getDndConfiguaration(this);
        });
    function getDndConfiguaration(This){
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/CallOrderConfirmation/getDNDCountConstant';
        var params = {};
        commonAjaxCall({This:This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: getDndConfiguarationResponse});
    }
    function getDndConfiguarationResponse(response){
        if(response.status===true){
            $('#txtdndConfiguration').val(response.data);
        }
        else{
            $('#txtdndConfiguration').val(0);
        }
    }
    function saveDndConfiguration(This){
        notify('Processing..', 'warning', 10);
        saveLeadStages(This);
        if($('#txtdndConfiguration').val().trim()==''){

        }
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/CallOrderConfirmation/saveDndConfiguration';
        var params = {dndConfigCount:$('#txtdndConfiguration').val()};
        commonAjaxCall({This:This,method: 'POST', params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: saveDndConfigurationResponse});
    }
    function saveDndConfigurationResponse(response){
        //alertify.dismissAll();
        if(response.status===true){
            //notify(response.message,'success',10);
            getDndConfiguaration(this);
        }
        else{
            //notify(response.message,'error',10);
        }
    }
    function leadLeadStagesLoad(This) {
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/CallOrderConfirmation/getAllLeadStages';
        var params = {};
        commonAjaxCall({This:This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: leadStagesResponse});

    }

    function leadStagesResponse(response){
          var leads="";
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            leads += '<div class="text-center">No Data found</div>';
        }
        else
        {

            for (var i = 0;i<response.data.length;i++) {
                var left_days=(response['data'][i]['left_days'] == null || response['data'][i]['left_days'] == '') ? "0" :response['data'][i]['left_days'];
                var right_days=(response['data'][i]['right_days'] == null || response['data'][i]['right_days'] == '') ? "0" :response['data'][i]['right_days'];
                var left_stage_id=response['data'][i]['left_id'];
                var right_stage_id=response['data'][i]['right_id'];
                var left_id=response['data'][i]['left_side']+""+response['data'][i]['left_id'];
                var right_id=response['data'][i]['right_side']+""+response['data'][i]['right_id'];
                var left_conversion_rate=(response['data'][i]['left_conversion_rate'] == null || response['data'][i]['left_conversion_rate'] == '') ? "0" :response['data'][i]['left_conversion_rate'];
                var right_conversion_rate=(response['data'][i]['right_conversion_rate'] == null || response['data'][i]['right_conversion_rate'] == '') ? "0" :response['data'][i]['right_conversion_rate'];
                leads+='<tr class=""><td>';
                if(response['data'][i]['left_side']!="")
                {
                leads+='<span class="leadStage-level">'+response['data'][i]['left_side']+'</span>';
                leads+='<input  type="hidden" id="hid'+left_id.replace("+","plus")+'"  value="'+response['data'][i]['left_id']+'">';
                    leads+='<textarea name="leadDescription" class="leadStage-description" id="txa' + left_id.replace("+", "plus") + '" placeholder="Description">' + response['data'][i]['left_desc'] + '</textarea>';
                }
                leads+='</td><td class="lead-day">';
                if(response['data'][i]['left_side']!="") {
                    leads += '<input  name="leadsStage"  class="leadsStage-input" id="txt' + left_id.replace("+", "plus") + '" type="text" value="' + left_days + '" maxlength="3" onkeypress="return isNumberKey(event)">';
                }
                    leads+='</td><td class="lead-day">';
                if(response['data'][i]['left_side']!="") {
                    leads += '<input name="leadsConversionRate"  class="leadsStage-input" id="txtConversionRate_' + left_stage_id + '" type="text" value="' + left_conversion_rate + '" maxlength="2" onkeypress="return isNumberKey(event)">';
                }
                    leads+='</td><td>';

                leads+='<span class="OldleadStage-level">'+response['data'][i]['right_side']+'</span>'
                        +'<textarea name="leadDescription" class="leadStage-description" id="txa'+right_id.replace("+","plus")+'" placeholder="Description">'+response['data'][i]['right_desc']+'</textarea>'
                        +'<input type="hidden" id="hid'+right_id.replace("+","plus")+'"  value="'+response['data'][i]['right_id']+'">'
                        +'</td><td class="lead-day">'
                        +'<input name="leadsStage" class="leadsStage-input" id="txt'+right_id.replace("+","plus")+'" type="text" value="'+right_days+'" maxlength="3" onkeypress="return isNumberKey(event)">'
                        +'</td><td class="lead-day">'
                    +'<input name="leadsConversionRate" class="leadsStage-input" id="txtConversionRate_'+right_stage_id+'" type="text" value="'+right_conversion_rate+'" maxlength="2" onkeypress="return isNumberKey(event)">'
                    +'</td></tr>';
            }
         }
        $("#lead_stages").html(leads);
    }

</script>