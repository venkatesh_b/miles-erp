<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-25
 * Time: 10:20 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Campaign</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="" href="<?php echo BASE_URL; ?>app/manage_campaign"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0 mb15">
                <div class="col-sm-2 p0 ">
                    <div class="contact-wrap branch-green-bg">
                        <span class="green-bg"><img src="<?php echo BASE_URL; ?>assets/images/bounced.png"></span>
                    </div>
                    <div class="contact-wrap-text mr5 branch-green-bg-light">
                        <div class="display-inline-block contact-green-color f13">Bounced</div>
                        <div class="contact-blue" id="cntBounced">----</div>
                    </div>
                </div>
                <div class="col-sm-2 p0 ">
                    <div class="contact-wrap branch-red-bg">
                        <span class="red-bg"><img src="<?php echo BASE_URL; ?>assets/images/opend.png"></span>
                    </div>
                    <div class="contact-wrap-text mr5 branch-red-bg-light f13">
                        <div class="display-inline-block contact-red-color">Opened</div>
                        <div class="contact-blue" id="cntOpened">----</div>
                    </div>
                </div>
                <div class="col-sm-2 p0 ">
                    <div class="contact-wrap branch-blue-bg">
                        <span class="blue-bg"><img src="<?php echo BASE_URL; ?>assets/images/clicked.png"></span>
                    </div>
                    <div class="contact-wrap-text mr5 branch-blue-bg-light f13">
                        <div class="display-inline-block contact-blue-color">Clicked</div>
                        <div class="contact-blue" id="cntClicked">----</div>
                    </div>
                </div>
                <div class="col-sm-2 p0 ">
                    <div class="contact-wrap branch-yellow-bg">
                        <span class="yellow-bg"><img src="<?php echo BASE_URL; ?>assets/images/unsubscribed.png"></span>
                    </div>
                    <div class="contact-wrap-text mr5 branch-yellow-bg-light f13">
                        <div class="display-inline-block contact-yellow-color">Unsubscribed</div>
                        <div class="contact-blue" id="cntUnsubscribed">----</div>
                    </div>
                </div>
                <div class="col-sm-2 p0 ">
                    <div class="contact-wrap orange-bg">
                        <span class="branch-orange-bg"><img src="<?php echo BASE_URL; ?>assets/images/spam-complaint.png"></span>
                    </div>
                    <div class="contact-wrap-text mr5 branch-orange-bg-light f13">
                        <div class="display-inline-block orange-color line-height15">Spam Complaints</div>
                        <div class="contact-blue" id="cntSpam">----</div>
                    </div>
                </div>
                <div class="col-sm-2 p0 ">
                    <div class="contact-wrap branch-ash-bg">
                        <span class="ash-bg"><img src="<?php echo BASE_URL; ?>assets/images/dont-open.png"></span>
                    </div>
                    <div class="contact-wrap-text branch-ash-bg-light f13">
                        <div class="display-inline-block ash">Didn't Open</div>
                        <div class="contact-blue" id="cntNotopened">----</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="campaign-list">
                    <thead>
                    <tr>
                        <th>Campaign Name</th>
                        <th>Branch</th>
                        <th class="text-right pr10">#Contacts </th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th class="border-r-none">Status</th>
                        <!--<th class="no-sort" style="width: 8%;">&nbsp;</th>-->
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildCampaignDataTable();
        buildCampaignCounts();
    });
    function buildCampaignDataTable()
    {
        var ajaxurl=API_URL+'index.php/Campaign/campaignList';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#campaign-list").dataTable().fnDestroy();
        $tableobj=$('#campaign-list').DataTable( {
            "fnDrawCallback": function()
            {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                }
                else
                {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No campaigns found",
                "sZeroRecords": "No campaigns found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax:
            {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response)
                {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [
                {
                    "targets": 'no-sort',
                    "orderable": false
                }
            ],
            "order": [[ 0, "desc" ]],
            columns:
                [
                    {
                        data: null, render: function ( data, type, row )
                    {

                        var html='';
                        html+='<a class="font-bold" access-element="list" href="<?php echo BASE_URL; ?>app/campaign_info/'+data.campaign_id+'">'+data.campaign+'</a>';
                        return html;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.branchName;
                    }
                    },
                    {
                        data: null,className:"text-right",render: function ( data, type, row )
                    {
                        return data.totalContacts;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.start_date;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.end_date;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        var roleUsersData = '';
                        if(data.is_editable==1 || data.is_deletable==1) {
                            roleUsersData = '<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                            roleUsersData += '<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                            roleUsersData += '<i class="fa fa-ellipsis-v f18 plr10"></i>';
                            roleUsersData += '</a>';
                            roleUsersData += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                            roleUsersData += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                            if(data.is_editable==1) {
                                roleUsersData += '<li><a access-element="edit" href="<?php echo BASE_URL; ?>app/manage_campaign/' + data.campaign_id + '">Edit</a></li>';
                            }
                            if(data.is_deletable==1) {
                                roleUsersData += '<li><a access-element="delete" href="javascript:;" onclick="deleteCampaignConfirm(this,' + data.campaign_id + ')">Delete</a></li>';
                            }
                            roleUsersData += '</ul>';
                            roleUsersData += '</div>';
                        }
                        else{
                            roleUsersData = ' ';
                        }
                        return data.status+roleUsersData;
                    }
                    }
                    /*{
                        data: null, render: function ( data, type, row )
                    {

                        var roleUsersData = '';
                        if(data.is_editable==1 || data.is_deletable==1) {
                            roleUsersData = '<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                            roleUsersData += '<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                            roleUsersData += '<i class="fa fa-ellipsis-v f18 plr10"></i>';
                            roleUsersData += '</a>';
                            roleUsersData += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                            roleUsersData += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                            if(data.is_editable==1) {
                                roleUsersData += '<li><a access-element="edit" href="<?php echo BASE_URL; ?>app/manage_campaign/' + data.campaign_id + '">Edit</a></li>';
                            }
                            if(data.is_deletable==1) {
                                roleUsersData += '<li><a access-element="delete" href="javascript:;" onclick="deleteCampaign(this,' + data.campaign_id + ')">Delete</a></li>';
                            }
                            roleUsersData += '</ul>';
                            roleUsersData += '</div>';
                        }
                        else{
                            roleUsersData = ' ';
                        }
                        return data.status+roleUsersData;
                    }
                    }*/
                ]
        });
        DTSearchOnKeyPressEnter();
    }
    function deleteCampaignConfirm(This,Id){
        customConfirmAlert('Campaign Status', 'Are you sure want to delete the campaign ?');
        $("#popup_confirm #btnTrue").attr("onclick","deleteCampaign(this,"+Id+")");
    }
    function deleteCampaign(This,Id){
        alertify.dismissAll();
        notify('Processing','warning',50);
        var ajaxurl=API_URL+'index.php/Campaign/deleteCampaign';
        var userID=$('#hdnUserId').val();
        var action='delete';
        var params = {campaignId:Id,userID:userID};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: This, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: deleteCampaignResponse});
    }
    function deleteCampaignResponse(response){
        $('#popup_confirm').modal('hide');
        alertify.dismissAll();
        if(response.status===true){
            notify(response.message,'success',10);
            buildCampaignDataTable();
            buildCampaignCounts();
        }
        else{

            notify(response.message,'error',10);
        }
    }
    function buildCampaignCounts(){
        $('#cntBounced').html('----');
        $('#cntOpened').html('----');
        $('#cntClicked').html('----');
        $('#cntUnsubscribed').html('----');
        $('#cntSpam').html('----');
        $('#cntNotopened').html('----');
    }
</script>