<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.v
 * Date: 08-07-2016
 * Time: 11:43 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" name="hdnUserId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Preview Template</h3>
        <div class="content-header-btnwrap">
            <ul>
                <!--<li><a class="viewsidePanel"  data-target="#myModal" data-toggle="modal"><i class="icon-plus-circle" ></i></a></li>-->
                <li>
                </li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable  -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">

                <div class="col-sm-12 p20 grey-bg mb20">
                    <div class="attached-template">
                        <iframe class="template_preview" name="ifrm_camp_template" id="ifrm_camp_template" src="" frameborder="0"></iframe>

                    </div>
                </div>

        </div>

        <!-- InstanceEndEditable --></div>
</div>
<script>
    $context='Campaign';
    $pageUrl='campaign';
    var campaignPath=decodeURI('<?php echo $Id; ?>');

    docReady(function () {

        var strs=campaignPath.replace(/\^/g, ".");
        strs=strs.replace(/\~/g, "/");
        $('#ifrm_camp_template').attr('src', API_URL+strs);



    });
</script>
