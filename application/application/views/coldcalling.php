<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 18-03-2016
 * Time: 09:52 AM
 */

?>
<input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
<input type="hidden" id="hdnLeadId" value="0" readonly />
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">Cold Calling</h3>

        <div class="content-header-btnwrap">
            <ul>
                <li><a access-element="filters" data-toggle="modal" data-target="#filters" class="tooltipped" data-position="top" data-tooltip="Filters"><i class="icon-filter"></i></a></li>
                <li><a access-element="add" href="javascript:;"  class="tooltipped" data-position="top" data-tooltip="Add Contact" onclick="ManageColdCallContact(this,0)"><i class="icon-plus-circle"></i></a></li>
                <!--<li>
                    <input class="content-header-search" type="text" placeholder="search"/>
                    <a class="content-header-search-btn"><i class="icon-search"></i></a>
                </li>-->
            </ul>
        </div>
    </div>


    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix" access-element="list">
            <div>
                <div class="col-5">
                    <div class="contact-wrap branch-green-bg">
                        <span class="green-bg"><img src="<?php echo BASE_URL;?>assets/images/overall-scheduled.png"></span>
                    </div>
                    <div class="contact-wrap-text mr5 branch-green-bg-light">
                        <div class="display-inline-block contact-green-color">Overall Scheduled</div>
                        <div class="contact-blue" id="overallScheduledCountWrapper">----</div>
                    </div>
                </div>
                <div class="col-5">
                    <div class="contact-wrap branch-red-bg">
                        <span class="red-bg"><img src="<?php echo BASE_URL;?>assets/images/calls-due.png"></span>
                    </div>
                    <div class="contact-wrap-text mr5 branch-red-bg-light">
                        <div class="display-inline-block contact-red-color">Calls Due</div>
                        <div class="contact-blue" id="callsDueCountWrapper">----</div>
                    </div>
                </div>
                <div class="col-5">
                    <div class="contact-wrap branch-blue-bg">
                        <span class="blue-bg"><img src="<?php echo BASE_URL;?>assets/images/converted-leads.png"></span>
                    </div>
                    <div class="contact-wrap-text mr5 branch-blue-bg-light">
                        <div class="display-inline-block contact-blue-color">Converted Leads</div>
                        <div class="contact-blue" id="convertedLeadsCountWrapper">----</div>
                    </div>
                </div>
                <div class="col-5">
                    <div class="contact-wrap branch-yellow-bg">
                        <span class="yellow-bg"><img src="<?php echo BASE_URL;?>assets/images/contacts4.png"></span>
                    </div>
                    <div class="contact-wrap-text mr5 branch-yellow-bg-light">
                        <div class="display-inline-block contact-yellow-color">Database(DND)</div>
                        <div class="contact-blue" id="dndCountWrapper">----</div>
                    </div>
                </div>
                <div class="col-5">
                    <div class="contact-wrap orange-bg">
                        <span class="branch-orange-bg"><img src="<?php echo BASE_URL;?>assets/images/rejected.png"></span>
                    </div>
                    <div class="contact-wrap-text branch-orange-bg-light">
                        <div class="display-inline-block orange-color">Rejected</div>
                        <div class="contact-blue" id="rejectedCountWrapper">----</div>
                    </div>
                </div>

            </div>

            <div class="col-sm-12 p0 mt15">
                <table class="table table-responsive table-striped table-custom" id="coldCalls-list">
                    <thead>
                    <tr class="">
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>City</th>
                        <th>Tags</th>
                        <th class="text-center no-sort">Action</th>
                    </tr>

                    </thead>

                </table>
            </div>
        </div>
        <!--Create Contact Modal-->

        <div class="modal fade" id="createContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <form id="ManageConcatForm" method="post">

                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Contact</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtContactName" type="text" class="validate">
                                    <label for="txtContactName">Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtContactEmail" type="email">
                                    <label for="txtContactEmail" data-error="wrong" data-success="right">Email <!--<em>*</em>--></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtContactPhone" type="text" class="validate" onkeypress="return isPhoneNumber(event)" maxlength="15">
                                    <label for="txtContactPhone">Phone <!--<em>*</em>--></label>
                                </div>
                            </div>

                            <div class="col-sm-6 mb10">
                                <div class="input-field">
                                    <select id="txtContactCountry">
                                        <option value="" selected>--Select--</option>

                                    </select>
                                    <label class="select-label">Country <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-6 mb10">
                                <div class="input-field">
                                    <select id="txtContactCity">
                                        <option value=""  selected>--Select--</option>

                                    </select>
                                    <label class="select-label">City <em>*</em></label>
                                </div>
                            </div>
                            <!--<div class="col-sm-12">
                                <div class="input-field">
                                    <select multiple id="txtContactTag">
                                        <option value="" disabled selected>--Select--</option>

                                    </select>
                                    <label class="select-label">Tags <em>*</em></label>
                                </div>
                            </div>-->
                            <div class="col-sm-12 mb20" id="contactType-corporate-tags">
                                <div class="input-field col-sm-11 p0 custom-multiselect-wrap">
                                    <select multiple class="chosen-select" data-placeholder="--Select--" id="txtContactTag">
                                        <option value="" disabled selected>--Select--</option>

                                    </select>
                                    <label class="select-label">Tags <!--<em>*</em>--></label>
                                </div>
                                <div class="col-sm-1 p0">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetTagWrapper(this)" href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ContactTagWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtContactTagWrapper" type="text" class="validate" value="">
                                            <label for="txtContactTagWrapper">Tag Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="addTagWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" id="contactType-source">
                                <div class="input-field col-sm-11 p0 custom-select-wrap">
                                    <select class="chosen-select" id="txtContactSource">
                                        <option value="" disabled selected>--Select--</option>

                                    </select>
                                    <label class="select-label">Source <em>*</em></label>
                                </div>
                                <div class="col-sm-1 p0">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetContactSourceWrapper(this)" href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ContactSourceWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtContactSourceWrapper" type="text" class="validate" value="">
                                            <label for="txtContactSourceWrapper">Source <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="addContactSourceWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue-btn" onclick="saveColdCallAddContact(this)" id="btnSaveContact"><i class="icon-right mr8"></i>Save
                            </button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                    class="icon-times mr8"></i>Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


        <!--View Contact Model-->

        <div class="modal fade" id="viewContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title">Contact Details</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Name</span>
                                <p>Alex</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Email</span>
                                <p>Test@gmail.com</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <span class="display-block ash">Phone</span>
                                <p>(025)-244-1258</p>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Company</span>
                                <p>Google</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Branch</span>
                                <p>Hyderabad</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Country</span>
                                <p>India</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">City</span>
                                <p>Hyderabad</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <span class="display-block ash">Tags</span>
                                <p>Student, Deloitte, B.Com</p>
                            </div>
                        </div>
                        <!--<div class="col-sm-12">
                            <div class="input-field">
                                <select>
                                    <option value="" disabled selected>&#45;&#45;Select&#45;&#45;</option>
                                    <option value="1">Branch 1</option>
                                    <option value="2">Branch 2</option>
                                    <option value="3">Branch 3</option>
                                </select>
                                <label class="select-label">Select Branch</label>
                            </div>
                        </div>-->
                        <div class="col-sm-12">
                            <button class="btn blue-btn mt15" type="button"><i class="icon-users1 mr8"></i>Convert to Lead</button>
                        </div>




                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save
                        </button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                class="icon-times mr8"></i>Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!--Filter Modal-->
        <div class="modal fade" id="filters" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Filters</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="filter-wrap">
                            <div class="input-field mt0">
                                <input class="filled-in" id="all" type="checkbox" value="history" name="filtersColdCall">
                                <label class="f14" for="all">History</label>
                            </div>
                            <div class="input-field mt0">
                                <input class="filled-in" id="dnd_contact" type="checkbox" value="DND" name="filtersColdCall">
                                <label class="f14" for="dnd_contact">DND</label>
                            </div>
                            <div class="input-field mt0">
                                <input class="filled-in" id="contact_student" type="checkbox" value="callDue" name="filtersColdCall">
                                <label class="f14" for="contact_student">Call Due</label>
                            </div>
                            <div class="input-field mt0">
                                <input class="filled-in" id="dbDND" type="checkbox" value="released" name="filtersColdCall">
                                <label class="f14" for="dbDND">Released</label>
                            </div>
                            <div class="input-field mt0">
                                <input class="filled-in" id="leadConverted" type="checkbox" value="leadConverted" name="filtersColdCall">
                                <label class="f14" for="leadConverted">Lead Converted</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn" onclick="applyColdCallFilters()"><i class="icon-right mr8"></i>Apply</button>
                        <button type="button" class="btn blue-light-btn" onclick="resetColdCallFilters()"><i class="icon-times mr8"></i>Clear</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <!--Cold Calling  Modal-->
        <div class="modal fade" id="coldCalling" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <form id="ManageColdCallForm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Cold Calling</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12">
                            <div class="col-sm-12 p0">
                                <div class="input-field">
                                    <input id="txtColdCallContactName" type="text" class="validate" value=""  >
                                    <label for="txtColdCallContactName">Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12 p0">
                                <div class="input-field">
                                    <input id="txtColdCallContactEmail" type="text" class="validate" value=""  >
                                    <label for="txtColdCallContactEmail">Email <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-6 pl0">
                                <div class="input-field">
                                    <input id="txtColdCallContactPhone" type="text" class="validate" value="" onkeypress="return isPhoneNumber(event)" maxlength="15" >
                                    <label for="txtColdCallContactPhone">Phone</label>
                                </div>
                            </div>
                            <div class="col-sm-6 pr0">
                                <div class="input-field">
                                    <input id="txtColdCallContactAltPhone" type="text" class="validate" value="" onkeypress="return isPhoneNumber(event)" maxlength="15" >
                                    <label for="txtColdCallContactAltPhone">Alternate Phone</label>
                                </div>
                            </div>
                            <div class="col-sm-12 p0">
                                <div class="input-field">
                                    <select id="txtColdCallContactCity" disabled>

                                    </select>
                                    <label class="select-label">City</label>
                                </div>
                            </div>
                            <div class="col-sm-12 p0">
                                <div class="input-field">
                                    <select id="txtColdCallContactCountry" disabled>
                                    </select>
                                    <label class="select-label">Country</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <select id="ddlCalltype">
                                    <option value="Outgoing">Outgoing</option>
                                    <option value="Incoming">Incoming</option>
                                </select>
                                <label for="ddlCalltype" class="select-label">Call Type</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <select id="txtColdCallStatus">
                                    <option value="" selected>--Select--</option>
                                    <option value="switchoff">Switch Off / Not Reachable</option>
                                    <option value="notlifted">Not Lifted</option>
                                    <option value="notexist">Number Not Exist</option>
                                    <option value="answered">Answered</option>
                                </select>
                                <label class="select-label">Call Status</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="multiple-radio" id="callStatusInfo">
                                <span class="inline-radio">
                                    <input class="with-gap" name="txtColdcallStatusInfo" type="radio" id="callStatusInfoIntrested" value="callStatusInfoIntrested" checked />
                                    <label for="callStatusInfoIntrested">Interested</label>
                                </span>
                                <span class="inline-radio">
                                    <input class="with-gap" name="txtColdcallStatusInfo" type="radio" id="callStatusInfoIntrestedTransfer" value="callStatusInfoIntrestedTransfer" />
                                    <label for="callStatusInfoIntrestedTransfer">Interested &amp; Transfer</label>
                                </span>
                                 <span class="inline-radio">
                                    <input class="with-gap" name="txtColdcallStatusInfo" type="radio" id="callStatusInfoNotintrested" value="callStatusInfoNotintrested"  />
                                    <label for="callStatusInfoNotintrested">Not interested</label>
                                </span>
                                 <span class="inline-radio">
                                    <input class="with-gap" name="txtColdcallStatusInfo" type="radio" id="callStatusInfoDND"  value="callStatusInfoDND" />
                                    <label for="callStatusInfoDND">DND</label>
                                </span>
                            </div>
                        </div>
                        <div class="coldcall-info">
                            <div class="col-sm-12" id="coldcalling-branch">
                                <div class="input-field">
                                    <select id="txtColdCallContactBranch">

                                    </select>
                                    <label class="select-label">Branch <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtColdCallContactCourse">

                                    </select>
                                    <label class="select-label">Course <em>*</em></label>
                                </div>
                            </div>

                            <!--<div class="col-sm-12" id="coldCallingContactTypeWrapper">

                            </div>-->

                            <div class="col-sm-12" id="coldCallingContactTypeWrapper" >

                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtColdCallContactPostQualification">

                                    </select>
                                    <label class="select-label">Qualification Level<!--<em>*</em>--></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtColdCallContactQualification">

                                    </select>
                                    <label class="select-label">Qualification <!--<em>*</em>--></label>
                                </div>
                            </div>
                            <div class="col-sm-12 mb10" id="contactType-corporate-wrap">
                                <div class="input-field col-sm-11 p0">
                                    <select id="txtColdCallContactCompany" class="custom-select-nomargin">

                                    </select>
                                    <label class="select-label">Company <em>*</em></label>
                                </div>

                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetCompanyWrapper(this)"><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ComapnyWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtComapnyNameWrapper" type="text" class="validate" value="">
                                            <label for="txtComapnyNameWrapper">Company Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="addCompanyWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mb10" id="contactType-Institution-wrap">
                                <div class="input-field col-sm-11 p0">
                                    <select id="txtColdCallContactInstitution" class="custom-select-nomargin">

                                    </select>
                                    <label class="select-label">Institution <em>*</em></label>
                                </div>
                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddInstituteWrap-btn" onclick="resetInstitutionWrapper(this)"><span class="icon-plus-circle f24 mt20 diplay-inline-block"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="InstitutionWrapper">
                                    <div class="col-sm-8 p0">
                                        <div class="input-field mt15">
                                            <input id="txtInstitutionNameWrapper" type="text" class="validate" value="">
                                            <label for="txtInstitutionNameWrapper">Institution Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="addInstitutionWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 pt5" id="FollowupDatePicker" style="display: none;">
                            <div class="col-sm-12 p0">
                                <div class="input-field">
                                    <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="ColdCallNextFollowUpData">
                                    <label for ="ColdCallNextFollowUpData" class="datepicker-label">Next Follow Up Date  <em>*</em></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 mt10">
                            <div class="input-field">
                                <textarea id="ColdCallDescription" name="ColdCallDescription" class="materialize-textarea"></textarea>
                                <label for="ColdCallDescription">Description <em>*</em></label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn" onclick="saveColdCalling(this)"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
            </form>
        </div>




        <!-- InstanceEndEditable --></div>
</div>
<script>
    docReady(function(){
        buildcoldCallContactsDataTable();

        getColdCallCourses($('#userBranchList').val());
        getColdCallBranches();
        getColdCallPostQualifications();
        //getColdCallQualifications();
        getColdCallCompanies();
        getColdCallInstitutions();
        getColdCallContactTypes();
        buildcoldCallContactsCounts();

    });
    $('#userBranchList').change(function(){
        changeDefaultBranch(this);
        resetColdCallFilters();
        getColdCallBranches();
        buildcoldCallContactsDataTable();
        buildcoldCallContactsCounts();
    });
    var appliedFiltersString='';
    function applyColdCallFilters(){
        notify('Processing..', 'warning', 10);
        var appliedFilters=[];
        $('input[name="filtersColdCall"]:checked').each(function() {

            appliedFilters.push(this.value)
        });
        appliedFiltersString=appliedFilters.join(',');
        buildcoldCallContactsDataTable();
        $('#filters').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }
    function resetColdCallFilters(){
        $('input[name="filtersColdCall"]').attr('checked',false);
        applyColdCallFilters();
    }

    //Cold Calling related functions - coldcalling.php in view (starts here)//
    function buildcoldCallContactsDataTable()
    {
        var ajaxurl=API_URL+'index.php/ColdCalling/getColdCallsList';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        var params={branchId:branchId,appliedFiltersString:appliedFiltersString};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#coldCalls-list").dataTable().fnDestroy();
        $tableobj=$('#coldCalls-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No leads found",
                "sZeroRecords": "No leads found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    data: null, render: function ( data, type, row ) {
                        //return data.name;
                        var leadsData = '';
                        leadsData += data.name;
                        return leadsData;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.email;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.phone;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.city;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    //return data.email;
                    var leadTagsData='';
                    leadTagsData+='<p class="max-width130px label-tags ellipsis popper" data-toggle="popover" data-title="Tags" data-placement="top" data-trigger="hover">';
                    var tagsList=data.tags.split(',');
                    for(var i=0;i<2;i++)
                    {
                        if(tagsList[i] && i==0)
                        {
                            leadTagsData+='<label class="label label-success font-normal p5 f12 mr5">'+tagsList[i]+'</label>';
                        }
                        else if(tagsList[i] && i==1)
                        {
                            leadTagsData+='<label class="label label-info font-normal p5 f12 mr5">'+tagsList[i]+'</label>';
                        }
                    }
                    leadTagsData+='</p>';
                    leadTagsData+='<div class="popper-content hide"><div class="plr15">';
                    /*for(var i=0;i<tagsList.length;i++){
                     leadTagsData+='<p><label class="popover-label">'+tagsList[i]+'</label></p>';
                     }*/
                    leadTagsData+='<p><label class="popover-label ash f14">'+data.tags+'</label></p>';
                    leadTagsData+='</div></div>';
                    return leadTagsData;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var actionHtml='';
                    if(data.is_hist_avail==1){
                        actionHtml+="<div class=\"text-center\"><a class=\"\" href=\"javascript:;\" access-element='status'  onclick=\"ManageColdCall(this,'"+data.lead_id+"')\"><i class=\"fa fa-phone green mr5 valign-middle f16\"></i></a></div>";
                    }
                    else{
                        actionHtml+="<div class=\"text-center\"><a class=\"\" href=\"javascript:;\" access-element='status'><i class=\"fa fa-ban red mr5 valign-middle f16\"></i></a></div>";
                    }

                    return actionHtml;
                }
                }
            ],
            "createdRow": function ( row, data, index )
            {

                if(data.status == 0)
                {
                    $(row).addClass('disabled-branchview');
                }
            }
        } );
        DTSearchOnKeyPressEnter();
        alertify.dismissAll();
    }


</script>

