<div class="contentpanel" id="courseview"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Course</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Add Course" class="viewsidePanel"  onclick="ManageCourse(this,0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>   
    </div>
  <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
         <div access-element="list" class="fixed-wrap clearfix"> 
                <div class="col-sm-12 p0">
                    <table class="table table-responsive table-striped table-custom" id="courses-list">
                      <thead>
                        <tr>
                          <th>Course Name</th>
                          <th>Branches</th>
<!--                          <th>Created By</th>-->
                          <th>Description</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                    </table>
                </div>
          </div>
          <!--Modal1-->
          <div class="modal fade" id="myCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageCourseForm"> 
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Course</h4>
                  </div>
                  <div class="modal-body modal-scroll clearfix">
                      <input type="hidden" id="hdnCourseId" value="0"/>
                    <div class="col-sm-12">
                    	<div class="input-field">
                            <input type="text" class="" id="txtCourseName">
                            <label for="txtCourseName"> Course Name <em>*</em></label>
                        </div>
                   </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                        <select multiple id="txtCourseBranches">
                          <option value="" disabled selected>--Select--</option>
                        </select>
                        <label class="select-label">Branches </label>
                      </div>
                   </div>

                      <input type="hidden" readonly value="1" id="txtCourseStatusValue"/>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <textarea class="materialize-textarea" id="txtCourseDescription"></textarea>
                            <label for="txtCourseDescription">Description <em>*</em></label>
                         </div>
                    </div>
                      <div class="col-sm-12" access-element="status">
                          <div class="switch display-inline-block">
                              <label class="display-block">Status</label>
                              <label>
                                  Off
                                  <input type="checkbox" id="txtCourseStatus" checked>
                                  <span class="lever"></span>
                                  On
                              </label>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn blue-btn" onclick="saveCourse(this)" id="btnSaveCourse"><i class="icon-right mr8"></i>Submit</button>
                     <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
          
        <!-- InstanceEndEditable --></div>
  </div>
<script>
/*$(document).ready(function(){
    getCourses(this);
});*/
docReady(function(){
    //renderCoursesData();
    buildCoursesDataTable();
});

function buildCoursesDataTable()
{
        var userID=$("#hdnUserId").val();
        var ajaxurl=API_URL+'index.php/Courses/getAllCourses';
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#courses-list").dataTable().fnDestroy();
        $tableobj = $('#courses-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.course_name;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        var roleUsersData='';
                        if(data.branchesCount == 0)
                        {
                            roleUsersData+='<span>';
                            roleUsersData+='<a href="javascript:;" class="anchor-blue p10">'+data.branchesCount+'</a>';
                            roleUsersData+='</span>';
                        }
                        else
                        {
                            roleUsersData+='<span class="popper p10" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Branches">';
                            roleUsersData+='<a href="javascript:;" class="anchor-blue p10">'+data.branchesCount+'</a>';
                            roleUsersData+='</span>';
                            roleUsersData+='<div class="popper-content hide">';
                            /*var branchesList=data.Branches.split(',');
                            for(var u=0;u<branchesList.length;u++)
                            {
                                roleUsersData+='<p>'+branchesList[u]+'</p>';
                            }*/
                            roleUsersData+='<p>'+data.Branches+'</p>';
                            roleUsersData+='</div>';
                        }
                        return roleUsersData;
                    }
                },
                /*{
                    data: null, render: function ( data, type, row )
                    {
                        //return data.created_by;
                    }
                },*/
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.description;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        //return data.is_active;
                        var status='',course_status='';
                        var status = ''
                        var statusText = '';
                        if(data.is_active == 1)
                        {
                            status="Active";
                            statusText = "Inactive";
                        }
                        else{
                            status="Inactive";
                            statusText = "Active";
                        }
                        course_status+=status;
                        course_status+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        course_status+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                        course_status+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        course_status+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        course_status+='<li><a access-element="edit" href="javascript:;" onclick="ManageCourse(this,\''+data.course_id+'\')">Edit</a></li>';
                        course_status+='<li><a access-element="delete" href="javascript:;" onclick="DeleteCourse(this,\''+data.course_id+'\','+data.is_active+')">'+statusText+'</a></li>';
                        course_status+='</ul></div>';
                        return course_status;
                    }
                }

            ],
            "createdRow": function ( row, data, index )
            {

                if(data.is_active == 0)
                {
                    $(row).addClass('disabled-branchview');
                }
            }
        } );
        DTSearchOnKeyPressEnter();
}
    
    function renderCoursesData(){
        var userID=$("#hdnUserId").val();
        var ajaxurl=API_URL+'index.php/Courses/getAllCourseList';
        var params = {};
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response=
        commonAjaxCall({This:this, requestUrl:ajaxurl,headerParams:headerParams,params:params,action:'list',onSuccess:renderDataToCourseGrid});
    }
    function renderDataToCourseGrid(response){
       var rowHtml='';
        if(response == -1 || response.status == false)
        {
            $.each(response.data, function(i, item) {
                   alertify.dismissAll();
                   notify(item,'error' ,5);
                   return false;
                   });
            rowHtml+="<tr class=''><td colspan='5' align='center'><h5> No Data Found </h5></td></tr>";
            $("#myTable > tbody").append(rowHtml);
        }
        else
        {
            var courseData = response.data;
            if(courseData.length<=0){
                 rowHtml+="<tr class=''><td colspan='5'><h3> No Data Found </h3></td></tr>";
            }
            else{
                $.each(courseData, function( key, value ) {
                            var CourseID=value.courseId;
                            var CourseName=value.name;
                            var CourseDescription=value.description;
                            var CreatedBy=value.createdBy;
                            var BranchesCount=value.branchesCount;
                            
                            var Branches='';
                            var checkboxEvent='';
                            if(BranchesCount!=null && BranchesCount!='' && BranchesCount>0){
                                Branches+=" <a href='javascript:;' class='anchor-blue p10 popper'  data-toggle='popover' data-placement='top' data-trigger='click' data-title='Branches'>"+BranchesCount+"</a>";
                                Branches+="<div class='popper-content hide'>";
                                $.each(value.branches, function( Branchkey, Branchvalue ) {
                                    Branches+="<p>"+Branchvalue.name+"</p>";
                                });
                                Branches+='</div>';
                            }
                            else{
                                Branches+=" <a href='javascript:;' class='anchor-blue p10'>"+BranchesCount+"</a>";
                            }
                            if(value.is_active==1){
                                checkboxEvent=" checked disabled ";
                            }
                            else{
                                checkboxEvent=" disabled ";
                            }
                            
                    rowHtml+="<tr class=''>\n\
                     <td>"+CourseName+"</td>\n\
                     <td class='text-right'>\n\
                     "+Branches+"</td>\n\
                     <td>"+CreatedBy+"</td>\n\
                     <td>"+CourseDescription+"</td>\n\
                     <td><div class='switch display-inline-block'><label>Off<input type='checkbox' "+checkboxEvent+">\n\
                      <span class='lever'></span>On</label></div>\n\
                      <div class='dropdown feehead-list pull-right custom-dropdown-style'>\n\
                        <a id='dLabel' data-target='#' href='javascript:;' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'><i class='fa fa-ellipsis-v f18 pr10 pl10'></i></a>\n\
                        <ul class='dropdown-menu pull-right' aria-labelledby='dLabel'>\n\
                         <li class='text-right pr10'><i class='fa fa-ellipsis-v f18'></i></li>\n\
                         <li><a access-element='manage' href='javascript:;' onclick='ManageCourse(this,\""+CourseID+"\")'>Manage Course</a></li>\n\
                         <li><a access-element='delete' href='javascript:;' onclick='DeleteCourse(this,\""+CourseID+"\")'>Delete</a></li>\n\
                       </ul>\n\
                     </div>\n\
                   </td>\n\
                   </tr>";
                    });
            }
            $(this).attr("data-dismiss","modal");
            $("#myTable > tbody").html(rowHtml);
            buildpopover();
        }
    }
</script>
