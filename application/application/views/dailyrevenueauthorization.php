<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 25/4/16
 * Time: 11:01 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">Daily Revenue Authorization</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><!--<a class=""><i class="fa fa-file-excel-o"></i></a>--></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix ">
            <div class="col-sm-12 revenue-post p0">
                <div class="col-sm-7 pl0">
                    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Company</label>
                        </div>
                        <div class="boxed-tags radio-tags" id="feeCompanyList">

                        </div>
                    </div>
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Mode of Payment</label>
                        </div>
                        <div class="boxed-tags radio-tags" id="feePaymentType">
                            <a data-value="cash" class="active">Cash</a>
                            <a data-value="other">Other</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check mb5">

                        </div>
                        <div class="input-field col-sm-6 mt0 p0">
                            <input id="txtRevenuePostDate" type="date" placeholder="dd/mm/yyyy" class="datepicker relative enablePastDatesWithCurrentDate" />
                            <label class="datepicker-label datepicker" for="txtRevenuePostDate">Date</label>
                        </div>
                    </div>
                    <div class="col-sm-6 pl0 mt15">
                        <a class="btn blue-btn btn-sm" href="javascript:;" onclick="getRevenueAuthorization()">Proceed</a>
                    </div>
                </div>
                <div class="col-sm-5 pr0">
                    <div class="white-bg">
                        <table class="table balance-table m0" id="financeTable">
                            <tbody>
                                <tr>
                                    <td>Opening Balance</td>
                                    <td><i class="fa fa-inr pull-left pt5 pl20"></i><span id="opening_bal">0</span></td>
                                </tr>
                                <tr>
                                    <td>(+) Amount Collected</td>
                                    <td><i class="fa fa-inr pull-left pt5 pl20"></i><span id="amt_collected">0</span></td>
                                </tr>
                                <tr>
                                    <td>(-) Amount Deposited</td>
                                    <td><i class="fa fa-inr pull-left pt5 pl20"></i><span id="amt_deposited">0</span></td>
                                </tr>
                                <tr>
                                    <td>(-) Signoff Amount</td>
                                    <td><i class="fa fa-inr pull-left pt5 pl20"></i><span id="signoff_amt">0</span></td>
                                </tr>
                                <tr>
                                    <td>Closing Balance</td>
                                    <td><i class="fa fa-inr pull-left  pt5 pl20"></i><span id="closing_bal">0</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="postingDetails">

            </div>
            <div class="col-sm-6 pl0 mt15 hidden" id="UpdateBtn">
                <a class="btn blue-btn btn-sm" href="javascript:;" onclick="updateRevenueAuthorization()">Authorize</a>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        dailyRevenueAuthorization();
    });
    var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
    function dailyRevenueAuthorization()
    {
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/Company/getFeeCompanyList';
        commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess:dailyRevenueAuthorizationResponse});
    }
    function dailyRevenueAuthorizationResponse(response)
    {
        var feeCompanies='';
        for(var i=0;i<response['data'].length;i++)
        {
            if(i==0)
                feeCompanies+='<a data-id="'+response['data'][i]['company_id']+'" class="active">';
            else
                feeCompanies+='<a data-id="'+response['data'][i]['company_id']+'">';
            feeCompanies+=response['data'][i]['name'];
            feeCompanies+='</a>';
        }
        $("#feeCompanyList").html(feeCompanies);
    }

    function getRevenueAuthorization()
    {
        alertify.dismissAll();
        var userId = $('#hdnUserId').val();
        var selectedFeeCompany=$("#feeCompanyList > .active").attr('data-id');
        selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
        var selectedRevenueDate=$("#txtRevenuePostDate").val();
        if(selectedFeeCompany == '' || selectedFeeCompany == undefined)
        {
            notify('Select any one company','error');
            return false;
        }
        else if(selectedPaymentType == '' || selectedPaymentType == undefined)
        {
            notify('Select any one payment type','error');
            return false;
        }
        else
        {
            var action = 'list';
            var selectedBranch = $("#userBranchList").val();
            var params = {branchId:selectedBranch,companyId:selectedFeeCompany,PaymentMode:selectedPaymentType,selectedDate:selectedRevenueDate};
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var ajaxurl = API_URL + 'index.php/DailyRevenue/getRevenueAuthorizationDetails';
            commonAjaxCall({This: this, headerParams: headerParams, params:params,requestUrl: ajaxurl, action: action, onSuccess:revenueAuthorizationDetailsResponse });
        }
    }

    function revenueAuthorizationDetailsResponse(response)
    {
        alertify.dismissAll();
        if(response['status'] == true)
        {
            //var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
            if(selectedPaymentType == 'cash')
            {
                buildCashRevenueAuthorizationResponse(response);
            }
            else
            {
                buildOtherRevenueAuthorizationResponse(response);
            }
        }
        else
        {
            notify(response.message, 'error', 10);
            $("#postingDetails").hide();
            $("#postingDetails").slideUp(function()
            {
                $("#postingDetails").html("");
                $("#UpdateBtn").addClass('hidden');
            });
            $("#financeTable span").html(0);
        }
    }

    function buildCashRevenueAuthorizationResponse(response)
    {
        FillDrsBalance(response['data']);
        //var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
        var revenuePostingData='';
        revenuePostingData+='<div class="col-sm-12 p0">';
        revenuePostingData+='<h4 class="heading-uppercase mb0">Cash Amount</h4>';
        revenuePostingData+='</div>';
        revenuePostingData+='<div class="col-sm-12 clearfix p0">';
        revenuePostingData+='<table class="table table-responsive table-striped table-custom mt5">';
        revenuePostingData+='<thead>';
        revenuePostingData+='<tr>';
        //revenuePostingData+='<th style="width: 4%;"></th>';
        revenuePostingData+='<th>Bank Name</th>';
        revenuePostingData+='<th>City</th>';
        revenuePostingData+='<th>Branch</th>';
        revenuePostingData+='<th>Account No.</th>';
        revenuePostingData+='<th class="text-right">Amount</th>';
        revenuePostingData+='<th>Bank Voucher No.</th>';
        revenuePostingData+='<th>Comments</th>';
        revenuePostingData+='</tr>';
        revenuePostingData+='<tr class=""><td class="border-none p0 lh10">&nbsp;</td></tr>';
        revenuePostingData+='</thead>';
        revenuePostingData+='<tbody>';
        var revenuePosting=response['data']['revenueAuthorization'];

        for(var i=0;i<revenuePosting.length;i++)
        {
            revenuePostingData+='<tr>';
            revenuePostingData+='<td>'+revenuePosting[i]['bank_name']+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['bank_city']+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['bank_branch']+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['acc_no']+'</td>';
            revenuePostingData+='<td class="text-right icon-rupee">'+moneyFormat(revenuePosting[i]['amount'])+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['bank_voucher_number']+'</td>';
            revenuePostingData+='<td>';
            revenuePostingData+='<div class="col-sm-8 p0">';
            revenuePostingData+='<textarea class="form-control table-textarea" rows="2" name="txaAuthorizeComments['+i+']"></textarea>';
            revenuePostingData+='</div>';
            revenuePostingData+='<span class="inline-checkbox table-checkbox">';
            revenuePostingData+='<input class="filled-in chkAuthorize" type="checkbox" name="chkAuthorize['+i+']" id="chkAuthorize['+i+']" value="'+revenuePosting[i]['drs_bank_deposit_id']+'">';
            revenuePostingData+='<label for="chkAuthorize['+i+']"></label>';
            revenuePostingData+='</span>';
            //revenuePostingData+='<span class="col-sm-8 p0"><textarea name="txaAuthorizeComments['+i+']" id="txaAuthorizeComments['+i+']" class="materialize-textarea"></textarea>';
            //revenuePostingData+='<label for="txaAuthorizeComments['+i+']"></label></span>';
            //revenuePostingData+='<span class="p5"><input type="checkbox" class="filled-in chkAuthorize" name="chkAuthorize['+i+']" id="chkAuthorize['+i+']" value="'+revenuePosting[i]['drs_bank_deposit_id']+'" />';
            //revenuePostingData+='<label for="chkAuthorize['+i+']"></label></span>';
            //revenuePostingData+='<input type="hidden" name="hdnDRSBankId['+i+']" value="'+revenuePosting[i]['drs_bank_deposit_id']+'" >';
            revenuePostingData+='</td>';
            revenuePostingData+='</tr>';
        }
        revenuePostingData+='</tbody>';
        revenuePostingData+='</table>';
        $("#postingDetails").hide();
        $("#postingDetails").html(revenuePostingData);
        $("#postingDetails").slideDown(function()
        {
            $("#UpdateBtn").removeClass('hidden');
        });
    }
    function buildOtherRevenueAuthorizationResponse(response)
    {
        FillDrsBalance(response['data']);
        //var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
        var revenuePostingData='';
        revenuePostingData+='<div class="col-sm-12 p0">';
        revenuePostingData+='<h4 class="heading-uppercase mb0">Cheque/DD/Credit Card Amount</h4>';
        revenuePostingData+='</div>';
        revenuePostingData+='<div class="col-sm-12 clearfix p0">';
        revenuePostingData+='<table class="table table-responsive table-striped table-custom mt5">';
        revenuePostingData+='<thead>';
        revenuePostingData+='<tr>';
        revenuePostingData+='<th>Receipt No.</th>';
        revenuePostingData+='<th>Receipt Date</th>';
        revenuePostingData+='<th>Cheque/DD/Card No.</th>';
        revenuePostingData+='<th>Bank Name</th>';
        revenuePostingData+='<th>Bank Voucher No.</th>';
        revenuePostingData+='<th>Comments</th>';
        revenuePostingData+='</tr>';
        revenuePostingData+='<tr class=""><td class="border-none p0 lh10">&nbsp;</td></tr>';
        revenuePostingData+='</thead>';
        revenuePostingData+='<tbody>';
        var revenuePosting=response['data']['revenueAuthorization'];
        var cc_number=0;
        for(var i=0;i<revenuePosting.length;i++)
        {
            if(revenuePosting[i]['payment_mode']=='Credit Card')
                cc_number='XXXX-XXXX-XXXX-'+revenuePosting[i]['credit_card_number'];
            else
                cc_number=revenuePosting[i]['cheque_number'];

            revenuePostingData+='<tr>';
            revenuePostingData+='<td>'+revenuePosting[i]['receipt_number']+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['receipt_date']+'</td>';
            revenuePostingData+='<td>'+cc_number+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['bank_name']+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['bank_voucher_number']+'</td>';
            revenuePostingData+='<td>';
            revenuePostingData+='<div class="col-sm-8 p0">';
            revenuePostingData+='<textarea class="form-control table-textarea" rows="2" name="txaAuthorizeComments['+i+']"></textarea>';
            revenuePostingData+='</div>';
            revenuePostingData+='<span class="inline-checkbox table-checkbox">';
            revenuePostingData+='<input class="filled-in chkAuthorize" type="checkbox" name="chkAuthorize['+i+']" id="chkAuthorize['+i+']" value="'+revenuePosting[i]['drs_bank_deposit_id']+'">';
            revenuePostingData+='<label for="chkAuthorize['+i+']"></label>';
            revenuePostingData+='</span>';
            revenuePostingData+='</td>';
            /*revenuePostingData+='<td><div class="col-sm-8 p0">';//fee_collection_id
            revenuePostingData+='<input type="hidden" name="hdnAmount['+i+']" value="'+revenuePosting[i]['amount_paid']+'" >';
            revenuePostingData+='<input type="hidden" name="hdnFeeCollectionId['+i+']" value="'+revenuePosting[i]['fee_collection_id']+'" >';
            revenuePostingData+='<input class="modal-table-textfiled m0" type="text" name="txtBankVoucher['+i+']" value="" onkeypress="return isAlphaNumericKey(event)" onkeyup="revenuePostAmount(this)" />';
            revenuePostingData+='</div></td>';*/
            revenuePostingData+='</tr>';
        }
        revenuePostingData+='</tbody>';
        revenuePostingData+='</table>';
        $("#postingDetails").hide();
        $("#postingDetails").html(revenuePostingData);
        $('select').material_select();
        $("#postingDetails").slideDown(function()
        {
            $("#UpdateBtn").removeClass('hidden');
        });
    }

    function FillDrsBalance(response)
    {
        $("#financeTable span#opening_bal").html(response['openingBalance']);
        $("#financeTable span#amt_collected").html(response['amountCollection']);
        $("#financeTable span#amt_deposited").html(response['amountDeposited']);
        $("#financeTable span#signoff_amt").html(response['signoffAmount']);
        $("#financeTable span#closing_bal").html(response['closingBalance']);
    }
    function revenuePostAmount(This)
    {
        var amountDeposited=0;
        //var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
        if(selectedPaymentType == 'cash')
        {
            $('input[name^="txtAmount"]').each(function()
            {
                var closing_amt=checkClosingBalance();
                if($(this).val()!='')
                {
                    if(closing_amt > 0)
                    {
                        amountDeposited=(amountDeposited+parseInt($(this).val()));
                    }
                    else
                    {
                        notify("Amount exceeds the closing amount","error",10);
                        $(This).val(0);
                    }
                }

            });
        }
        else
        {
            var index=0;
            $('input[name^="hdnAmount"]').each(function()
            {
                if($('input[name="txtBankVoucher['+index+']"]').val() != '' || $('select[name="ddlFCBankList['+index+']"]').val() != '')
                    amountDeposited=(amountDeposited+parseInt($(this).val()));
                index++;
            });
        }
        $("#amt_deposited").html(amountDeposited);
        calculateClosingBalance();
    }
    function checkClosingBalance()
    {
        return (parseInt($("#opening_bal").html())+parseInt($("#amt_collected").html()))-parseInt($("#amt_deposited").html());
    }
    function calculateClosingBalance()
    {
        var closing_bal=(parseInt($("#opening_bal").html())+parseInt($("#amt_collected").html()))-parseInt($("#amt_deposited").html());
        $("#closing_bal").html(closing_bal);
    }
    function updateRevenueAuthorization()
    {
        updateCashRevenueAuthorization();
    }

    function updateCashRevenueAuthorization()
    {
        alertify.dismissAll();
        var flag = 0,index= 0,SelectedItem='',AuthorizeItemIds =[],comment = '';
        $('input:checkbox.chkAuthorize').each(function ()
        {
            SelectedItem=$.trim(this.checked ? $(this).val() : "");
            comment = $.trim($('textarea[name="txaAuthorizeComments['+index+']"]').val());
            if(SelectedItem != '' && comment !='')
            {
                flag = 0;
                AuthorizeItemIds.push(SelectedItem+'@@@@@@'+comment);
            }
            else if(SelectedItem == '' && comment =='')
            {
                flag = 1;
            }
            else if(SelectedItem == '' && comment !='')
            {
                flag = 2;
            }
            else if(SelectedItem != '' && comment =='')
            {
                flag = 3;
            }
            if(flag != 0)
            {
                return false;
            }
            index++;
        });
        if(AuthorizeItemIds.length<=0 || flag >1)
        {
            if(flag == 1)
                notify("Select atleast one item to authorize","error",10);
            else if(flag == 2)
                notify("Select item to authorize","error",10);
            else if(flag == 3)
                notify("Enter comments for selected authorize items","error",10);
        }
        else
        {
            var userId = $('#hdnUserId').val();
            var action = 'save';
            var ajaxurl = API_URL + 'index.php/DailyRevenue/updateDailyRevenueAuthorization';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var params= {rawRevenueAuthorization:AuthorizeItemIds};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess:updateDailyRevenueAuthorizationResponse});
        }
    }

    function updateDailyRevenueAuthorizationResponse(response)
    {
        alertify.dismissAll();
        if(response['status'] == true)
        {
            $("#postingDetails").hide();
            $("#postingDetails").slideUp(function()
            {
                $("#postingDetails").html("");
                $("#UpdateBtn").addClass('hidden');
            });
            $("#financeTable span").html(0);
            notify(response.message, 'success', 10);
        }
        else
        {
            notify(response.message, 'error', 10);
        }
    }

    $("#userBranchList").change(function()
    {
        changeDefaultBranch(this);
    });

</script>