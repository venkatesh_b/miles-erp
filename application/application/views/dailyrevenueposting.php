<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 25/4/16
 * Time: 11:01 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">Daily Revenue Posting</h3>
        <div class="content-header-btnwrap" access-element="Report">
            <ul>
                <li><a class=""><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix ">
            <div class="col-sm-12 revenue-post p0">
                <div class="col-sm-7 pl0">
                    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Company</label>
                        </div>
                        <div class="boxed-tags radio-tags" id="feeCompanyList">

                        </div>
                    </div>
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Mode of Payment</label>
                        </div>
                        <div class="boxed-tags radio-tags" id="feePaymentType">
                            <a data-value="cash" class="active">Cash</a>
                            <a data-value="other">Other</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check mb5">
                            <!--<label class="heading-uppercase" >Date</label>-->
                        </div>
                        <div class="input-field col-sm-6 mt0 p0">
                            <!--<input id="date" class="validate m0" type="text">-->
                            <!--<input type="date" class="datepicker relative enableDates" placeholder="dd/mm/yyyy" id="txtRevenuePostDate">-->
                            <input id="txtRevenuePostDate" type="date" placeholder="dd/mm/yyyy" class="datepicker relative enablePastDatesWithCurrentDate" />
                            <label class="datepicker-label datepicker" for="txtRevenuePostDate">Date</label>
                        </div>
                    </div>
                    <div class="col-sm-6 pl0 mt15">
                        <a class="btn blue-btn btn-sm" href="javascript:;" onclick="getRevenuePosting()">Proceed</a>
                    </div>
                </div>
                <div class="col-sm-5 pr0">
                    <div class="white-bg">
                        <table class="table balance-table m0" id="financeTable">
                            <tbody>
                                <tr>
                                    <td>Opening Balance</td>
                                    <td><i class="fa fa-inr pull-left pt5 pl20"></i><span id="opening_bal">0</span></td>
                                </tr>
                                <tr>
                                    <td>(+) Amount Collected</td>
                                    <td><i class="fa fa-inr pull-left pt5 pl20"></i><span id="amt_collected">0</span></td>
                                </tr>
                                <tr>
                                    <td>(-) Amount Deposited</td>
                                    <td><i class="fa fa-inr pull-left pt5 pl20"></i><span id="amt_deposited">0</span></td>
                                </tr>
                                <tr access-element="SignOff">
                                    <td>(-) Signoff Amount</td>
                                    <td><i class="fa fa-inr pull-left pt5 pl20"></i><span id="signoff_amt">0</span></td>
                                </tr>
                                <tr>
                                    <td>Closing Balance</td>
                                    <td><i class="fa fa-inr pull-left  pt5 pl20"></i><span id="closing_bal">0</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="postingDetails">

            </div>

            <div class="col-sm-6 p0 hidden" access-element="SignOff" id="signoffSection">
                <div class="col-sm-12 pl0">
                    <p>Click <a href="Javascript:;" onclick="showSignOffData()" class="sign-off-btn"> here </a> to give Sign Off Amount</p>
                </div>
                <div class="revenue-field col-sm-8 p0 signoffDetails" style="display: none;">
                    <div class="col-sm-12 pl0">
                        <div class="input-field">
                            <input id="txtSignoffAmt" class="validate" type="text" onkeypress="return isNumberKey(event)" onkeyup="revenueSignoffAmount(this)">
                            <label class="" for="txtSignoffAmt">Amount <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12 pl0">
                        <div class="input-field">
                            <textarea id="txaSignoffComments" class="materialize-textarea"></textarea>
                            <label for="txaSignoffComments">Comments <em>*</em></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 pl0 mt15 hidden" id="UpdateBtn">
                <a class="btn blue-btn btn-sm" href="javascript:;" onclick="updateRevenuePosting()">Post</a>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        dailyRevenuePosting();
    });
    var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');

    function dailyRevenuePosting()
    {
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/Company/getFeeCompanyList';
        commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess:revenuePostingResponse });
    }
    function revenuePostingResponse(response)
    {
        var feeCompanies='';
        for(var i=0;i<response['data'].length;i++)
        {
            if(i==0)
                feeCompanies+='<a data-id="'+response['data'][i]['company_id']+'" class="active">';
            else
                feeCompanies+='<a data-id="'+response['data'][i]['company_id']+'">';
            feeCompanies+=response['data'][i]['name'];
            feeCompanies+='</a>';
        }
        $("#feeCompanyList").html(feeCompanies);

    }
    function getRevenuePosting()
    {
        alertify.dismissAll();
        var userId = $('#hdnUserId').val();
        $("#signoffSection").addClass('hidden');
        var selectedFeeCompany=$("#feeCompanyList > .active").attr('data-id');
        selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
        var selectedRevenueDate=$("#txtRevenuePostDate").val();
        if(selectedFeeCompany == '' || selectedFeeCompany == undefined)
        {
            notify('Select any one company','error');
            return false;
        }
        else if(selectedPaymentType == '' || selectedPaymentType == undefined)
        {
            notify('Select any one payment type','error');
            return false;
        }
        else
        {
            var action = 'list';
            var selectedBranch = $("#userBranchList").val();
            var params = {branchId:selectedBranch,companyId:selectedFeeCompany,PaymentMode:selectedPaymentType,selectedDate:selectedRevenueDate};
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var ajaxurl = API_URL + 'index.php/DailyRevenue/getRevenuePostingDetails';
            commonAjaxCall({This: this, headerParams: headerParams, params:params,requestUrl: ajaxurl, action: action, onSuccess:revenuePostingDetailsResponse });
        }
    }
    function buildCashRevenuePostingResponse(response)
    {
        //var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
        var revenuePostingData='';
        revenuePostingData+='<div class="col-sm-12 p0">';
        revenuePostingData+='<h4 class="heading-uppercase mb0">Cash Amount</h4>';
        revenuePostingData+='</div>';
        revenuePostingData+='<div class="col-sm-12 clearfix p0">';
        revenuePostingData+='<table class="table table-responsive table-striped table-custom mt5">';
        revenuePostingData+='<thead>';
        revenuePostingData+='<tr>';
        revenuePostingData+='<th>Bank Name</th>';
        revenuePostingData+='<th>City</th>';
        revenuePostingData+='<th>Branch</th>';
        revenuePostingData+='<th>Account No.</th>';
        revenuePostingData+='<th class="text-right">Amount</th>';
        revenuePostingData+='<th>Bank Voucher No.</th>';
        revenuePostingData+='<th>Bank Receipt</th>';
        revenuePostingData+='</tr>';
        revenuePostingData+='<tr class=""><td class="border-none p0 lh10">&nbsp;</td></tr>';
        revenuePostingData+='</thead>';
        revenuePostingData+='<tbody>';
        var revenuePosting=response['data']['revenuePosting'];
        for(var i=0;i<revenuePosting.length;i++)
        {
            revenuePostingData+='<tr>';
            revenuePostingData+='<td>'+revenuePosting[i]['bank_name']+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['bank_city']+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['bank_branch']+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['acc_no']+'</td>';
            revenuePostingData+='<td><div class="col-sm-8 p0 pull-right">';
            revenuePostingData+='<input type="hidden" name="hdnCompanyBankId['+i+']" value="'+revenuePosting[i]['company_bank_detail_id']+'" >';
            revenuePostingData+='<input class="modal-table-textfiled m0" type="text" name="txtAmount['+i+']" value="" onkeyup="revenuePostAmount(this)" onkeypress="return isNumberKey(event)" />';
            revenuePostingData+='</div></td>';
            revenuePostingData+='<td><div class="col-sm-8 p0"><input class="modal-table-textfiled m0" type="text" name="txtBankVoucher['+i+']" value="" onkeypress="return isAlphaNumericKey(event)" /></div></td>';
            revenuePostingData+='<td>';
            revenuePostingData+='<div class="col-sm-8 p0 file-field input-field"><div class="btn file-upload-btn"><span class="white-color pt2">Bank Receipt</span><input type="file" id="bankReceipt['+i+']" name="bankReceipt['+i+']" onchange="loadbankReceipt(this,'+i+')"></div>';
            revenuePostingData+='<div class="file-path-wrapper">';
            revenuePostingData+='<input class="file-path validate" placeholder="Upload Bank Receipt" type="button" name="inpBankReceipt['+i+']">';
            revenuePostingData+='<input type="hidden" name="hdnBankReceiptName['+i+']">';
            revenuePostingData+='<input type="hidden" name="hdnBankReceiptPath['+i+']">';
            revenuePostingData+='<input type="hidden" name="hdnTemplatePath['+i+']" value="" readonly />';
            revenuePostingData+='</div>';
            revenuePostingData+='</div>';
            revenuePostingData+='</td>';
            revenuePostingData+='</tr>';
        }
        revenuePostingData+='</tbody>';
        revenuePostingData+='</table>';
        $("#postingDetails").hide();
        $("#postingDetails").html(revenuePostingData);
        $("#postingDetails").slideDown(function()
        {
            $("#UpdateBtn").removeClass('hidden');
            $("#signoffSection").removeClass('hidden');
            $("#signoffSection .signoffDetails").hide();
        });
    }
    function buildOtherRevenuePostingResponse(response)
    {
        //var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
        var revenuePostingData='';
        revenuePostingData+='<div class="col-sm-12 p0">';
        revenuePostingData+='<h4 class="heading-uppercase mb0">Cheque/DD/Credit Card Amount</h4>';
        revenuePostingData+='</div>';
        revenuePostingData+='<div class="col-sm-12 clearfix p0">';
        revenuePostingData+='<table class="table table-responsive table-striped table-custom mt5">';
        revenuePostingData+='<thead>';
        revenuePostingData+='<tr>';
        revenuePostingData+='<th>Receipt No.</th>';
        revenuePostingData+='<th>Receipt Date</th>';
        revenuePostingData+='<th>Cheque/DD/Card No.</th>';
        revenuePostingData+='<th class="text-right">Bank Name</th>';
        revenuePostingData+='<th>Bank Voucher No.</th>';
        revenuePostingData+='<th>Bank Receipt</th>';
        revenuePostingData+='</tr>';
        revenuePostingData+='<tr class=""><td class="border-none p0 lh10">&nbsp;</td></tr>';
        revenuePostingData+='</thead>';
        revenuePostingData+='<tbody>';
        var revenuePosting=response['data']['revenuePosting'];
        var cc_number=0;
        for(var i=0;i<revenuePosting.length;i++)
        {

            if(revenuePosting[i]['payment_mode']=='Credit Card')
                cc_number=revenuePosting[i]['credit_card_number'];
            else
                cc_number=revenuePosting[i]['cheque_number'];
            revenuePostingData+='<tr>';
            revenuePostingData+='<td>'+revenuePosting[i]['receipt_number']+'</td>';
            revenuePostingData+='<td>'+revenuePosting[i]['receipt_date']+'</td>';
            revenuePostingData+='<td>'+cc_number+'</td>';
            revenuePostingData+='<td><div class="col-sm-10 p0 pull-right">';
            var feeCompanyList=response['data']['fee_company_list'];
            if(feeCompanyList!='')
            {
                revenuePostingData+='<select name="ddlFCBankList['+i+']" onchange="revenuePostAmount(this)" class="select-dropdown modal-table-selectfield m0">';
                revenuePostingData+='<option value="">--Choose Bank--</option>';
                for(var fc=0;fc<feeCompanyList.length;fc++)
                {
                    revenuePostingData+='<option value="'+feeCompanyList[fc]['company_bank_detail_id']+'" >'+feeCompanyList[fc]['bank_name']+' '+feeCompanyList[fc]['acc_no']+'</option>';
                }
                revenuePostingData+='</select>';
            }
            revenuePostingData+='</div></td>';
            revenuePostingData+='<td><div class="col-sm-8 p0">';
            revenuePostingData+='<input type="hidden" name="hdnAmount['+i+']" value="'+revenuePosting[i]['amount_paid']+'" >';
            revenuePostingData+='<input type="hidden" name="hdnFeeCollectionId['+i+']" value="'+revenuePosting[i]['fee_collection_id']+'" >';
            revenuePostingData+='<input class="modal-table-textfiled m0" type="text" name="txtBankVoucher['+i+']" value="" onkeypress="return isAlphaNumericKey(event)" onkeyup="revenuePostAmount(this)" />';
            revenuePostingData+='</div></td>';
            revenuePostingData+='<td>';
            revenuePostingData+='<div class="col-sm-8 p0 file-field input-field"><div class="btn file-upload-btn"><span class="white-color pt2">Bank Receipt</span><input type="file" id="bankReceipt['+i+']" name="bankReceipt['+i+']" onchange="loadbankReceipt(this,'+i+')"></div>';
            revenuePostingData+='<div class="file-path-wrapper">';
            revenuePostingData+='<input class="file-path validate" placeholder="Upload Bank Receipt" type="button" name="inpBankReceipt['+i+']">';
            revenuePostingData+='<input type="hidden" name="hdnBankReceiptName['+i+']">';
            revenuePostingData+='<input type="hidden" name="hdnBankReceiptPath['+i+']">';
            revenuePostingData+='<input type="hidden" name="hdnTemplatePath['+i+']" value="" readonly />';
            revenuePostingData+='</div>';
            revenuePostingData+='</div>';
            revenuePostingData+='</td>';
            revenuePostingData+='</tr>';
        }
        revenuePostingData+='</tbody>';
        revenuePostingData+='</table>';
        $("#postingDetails").hide();
        $("#postingDetails").html(revenuePostingData);
        $('select').material_select();
        $("#postingDetails").slideDown(function()
        {
            $("#UpdateBtn").removeClass('hidden');
            $("#signoffSection").addClass('hidden');
        });
    }
    function revenuePostingDetailsResponse(response)
    {
        console.log(selectedPaymentType);
        alertify.dismissAll();
        if(response['status'] == true)
        {
            //selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
            $("#opening_bal").html(response['data']['opening_balance']);
            $("#amt_collected").html(response['data']['amount_collected']);
            $("#txtSignoffAmt").val('');
            $("#txaSignoffComments").val('');
            Materialize.updateTextFields();
            if(selectedPaymentType == 'cash')
            {
                buildCashRevenuePostingResponse(response);
            }
            else
            {
                buildOtherRevenuePostingResponse(response);
            }
        }
        else
        {
            notify(response.message, 'error', 10);
            $("#postingDetails").hide();
            $("#postingDetails").slideUp(function()
            {
                $("#postingDetails").html("");
                $("#UpdateBtn").addClass('hidden');
                $("#signoffSection").addClass('hidden');
            });
            $("#financeTable span").html(0);
        }
    }
    function revenuePostAmount(This)
    {
        var amountDeposited=0;
        //var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
        if(selectedPaymentType == 'cash')
        {
            $('input[name^="txtAmount"]').each(function()
            {
                var closing_amt=checkClosingBalance();
                if($(this).val()!='')
                {
                    if(closing_amt > 0)
                    {
                        amountDeposited=(amountDeposited+parseInt($(this).val()));
                    }
                    else
                    {
                        notify("Amount exceeds the closing amount","error",10);
                        $(This).val(0);
                    }
                }
            });
        }
        else
        {
            var index=0;
            $('input[name^="hdnAmount"]').each(function()
            {
                if($('input[name="txtBankVoucher['+index+']"]').val() != '' || $('select[name="ddlFCBankList['+index+']"]').val() != '')
                    amountDeposited=(amountDeposited+parseInt($(this).val()));
                index++;
            });
        }
        $("#amt_deposited").html(amountDeposited);
        calculateClosingBalance();
    }
    function checkClosingBalance()
    {
        var ClosingBalance=(parseInt($("#opening_bal").html())+parseInt($("#amt_collected").html()))-parseInt($("#amt_deposited").html());
        if($("#signoff_amt").length>0)
        {
            ClosingBalance=ClosingBalance-parseInt($("#signoff_amt").html());
        }
        return ClosingBalance;
    }
    function calculateClosingBalance()
    {
        var closing_bal=(parseInt($("#opening_bal").html())+parseInt($("#amt_collected").html()))-parseInt($("#amt_deposited").html());
        if($("#signoff_amt").length>0)
        {
            closing_bal=closing_bal-parseInt($("#signoff_amt").html());
        }
        $("#closing_bal").html(closing_bal);
    }
    function updateRevenuePosting()
    {
        //var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');

        if(selectedPaymentType == 'cash')
        {
            updateCashRevenuePosting();
        }
        else
        {
            updateOtherRevenuePosting();
        }
    }

    function updateOtherRevenuePosting()
    {
        alertify.dismissAll();
        var flag = 0,index= 0,revenueOtherPosting =[],prevFlag=0;
        $('select[name^="ddlFCBankList"]').each(function()
        {
            if(($(this).val() == '') && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) == '') && ($('input[name="bankReceipt['+index+']"]').val() == ''))
            {
                $(this).parent().find('.select-dropdown').addClass("required");
                $('input[name="txtBankVoucher['+index+']"]').addClass("required");
                $('input[name="inpBankReceipt['+index+']"]').addClass("required");
                flag=1;
            }
            else if(($(this).val() != '') && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) == '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) == ''))
            {
                $('input[name="txtBankVoucher['+index+']"]').addClass("required");
                $('input[name="inpBankReceipt['+index+']"]').addClass("required");
                flag=2;
            }
            else if(($(this).val() == '') && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) != '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) == ''))
            {
                $(this).parent().find('.select-dropdown').addClass("required");
                $('input[name="inpBankReceipt['+index+']"]').addClass("required");
                flag=3;
            }
            else if(($(this).val() == '') && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) == '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) != ''))
            {
                $(this).parent().find('.select-dropdown').addClass("required");
                $('input[name="txtBankVoucher['+index+']"]').addClass("required");
                flag=4;
            }
            else if(($(this).val() != '') && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) != '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) == ''))
            {
                $('input[name="inpBankReceipt['+index+']"]').addClass("required");
                flag=5;
            }
            else if(($(this).val() != '') && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) == '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) != ''))
            {
                $('input[name="txtBankVoucher['+index+']"]').addClass("required");
                flag=6;
            }
            else if(($(this).val() == '') && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) != '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) != ''))
            {
                $(this).parent().find('.select-dropdown').addClass("required");
                flag=7;
            }
            if(prevFlag ==0)
            {
                prevFlag=flag;
            }
            if(flag == 0)
            {
                $(this).parent().find('.select-dropdown').removeClass("required");
                $('input[name="txtBankVoucher['+index+']"]').removeClass("required");
                $('input[name="inpBankReceipt['+index+']"]').removeClass("required");
                revenueOtherPosting.push($(this).val()+'@@@@@@'+$('input[name="txtBankVoucher['+index+']"]').val()+'@@@@@@'+$('input[name="hdnAmount['+index+']"]').val()+'@@@@@@'+$('input[name="hdnFeeCollectionId['+index+']"]').val()+'@@@@@@'+$('input[name="hdnBankReceiptName['+index+']"]').val());
            }
            index++;
        });
        if(prevFlag == 0)
        {
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var userId = $('#hdnUserId').val();
            var selectedBranch = $("#userBranchList").val();
            var selectedRevenueDate=$("#txtRevenuePostDate").val();
            var action = 'save';
            var ajaxurl = API_URL + 'index.php/DailyRevenue/updateOtherRevenuePosting';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var params= {branch_id:selectedBranch,selected_date:selectedRevenueDate,rawRevenueOtherPosting:revenueOtherPosting,opening_balance:$("#opening_bal").html(),amount_collected:$("#amt_collected").html(),amount_deposited:$("#amt_deposited").html(),signoff_amount:$("#signoff_amt").html(),closing_balance:$("#closing_bal").html(),company_id:$("#feeCompanyList > .active").attr('data-id'),paymentType:selectedPaymentType};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess:RevenuePostingResponse});
        }
        else if(flag == 1)
        {
            notify('Bank Name, Bank Voucher and Bank Receipt cannot be empty', 'error', 10);
        }
        else if(flag == 2)
        {
            notify('Bank Voucher and Bank Receipt cannot be empty', 'error', 10);
        }
        else if(flag == 3)
        {
            notify('Bank Name and Bank Receipt cannot be empty', 'error', 10);
        }
        else if(flag == 4)
        {
            notify('Bank Name and Bank Voucher cannot be empty', 'error', 10);
        }
        else if(flag == 5)
        {
            notify('Bank Receipt cannot be empty', 'error', 10);
        }
        else if(flag == 6)
        {
            notify('Bank Voucher cannot be empty', 'error', 10);
        }
        else if(flag == 7)
        {
            notify('Bank Name cannot be empty', 'error', 10);
        }
    }
    function updateCashRevenuePosting()
    {
        alertify.dismissAll();
        var flag = 0,index= 0,revenueCashPosting =[],prevFlag=0;
        $('input').removeClass("required");
        $('input[name^="txtAmount"]').each(function()
        {
            if(($(this).val() == '' || $(this).val()<=0) && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) == '') && ($('input[name="bankReceipt['+index+']"]').val() == ''))
            {
                $(this).addClass("required");
                $('input[name="txtBankVoucher['+index+']"]').addClass("required");
                $('input[name="inpBankReceipt['+index+']"]').addClass("required");
                flag=1;
            }
            else if(($(this).val() >0) && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) == '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) == ''))
            {
                $('input[name="txtBankVoucher['+index+']"]').addClass("required");
                $('input[name="inpBankReceipt['+index+']"]').addClass("required");
                flag=2;
            }
            else if(($(this).val() == '' || $(this).val()<=0) && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) != '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) == ''))
            {
                $(this).addClass("required");
                $('input[name="inpBankReceipt['+index+']"]').addClass("required");
                flag=3;
            }
            else if(($(this).val() == '' || $(this).val()<=0) && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) == '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) != ''))
            {
                $(this).addClass("required");
                $('input[name="txtBankVoucher['+index+']"]').addClass("required");
                flag=4;
            }
            else if(($(this).val()>0) && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) != '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) == ''))
            {
                $('input[name="inpBankReceipt['+index+']"]').addClass("required");
                flag=5;
            }
            else if(($(this).val()>0) && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) == '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) != ''))
            {
                $('input[name="txtBankVoucher['+index+']"]').addClass("required");
                flag=6;
            }
            else if(($(this).val() == '' || $(this).val()<=0) && ($.trim($('input[name="txtBankVoucher['+index+']"]').val()) != '') && ($.trim($('input[name="bankReceipt['+index+']"]').val()) != ''))
            {
                $(this).addClass("required");
                flag=7;
            }
            if(prevFlag ==0)
            {
                prevFlag=flag;
            }
            if(flag == 0)
            {
                $(this).removeClass("required");
                $('input[name="txtBankVoucher['+index+']"]').removeClass("required");
                $('input[name="inpBankReceipt['+index+']"]').removeClass("required");
                revenueCashPosting.push($(this).val()+'@@@@@@'+$('input[name="txtBankVoucher['+index+']"]').val()+'@@@@@@'+$('input[name="hdnCompanyBankId['+index+']"]').val()+'@@@@@@'+$('input[name="hdnBankReceiptName['+index+']"]').val());
            }
            index++;
        });
        if(prevFlag == 0)
        {
            var userId = $('#hdnUserId').val();
            var selectedBranch = $("#userBranchList").val();
            var selectedRevenueDate=$("#txtRevenuePostDate").val();
            var signoffAmount=0;
            var signoffComments='';
            var signoff_flag=0;
            if($("#signoffSection").length>0)
            {
                $("#txtSignoffAmt").removeClass("required");
                $("#txaSignoffComments").removeClass("required");
                signoffAmount=$("#txtSignoffAmt").val();
                signoffComments= $.trim($("#txaSignoffComments").val());
                if(signoffAmount != '' && signoffComments == '')
                {
                    notify('Please enter signoff comments', 'error', 10);
                    $("#txaSignoffComments").addClass("required");
                    signoff_flag=1;
                }
                else if(signoffAmount == '' && signoffComments != '')
                {
                    notify('Please enter signoff amount', 'error', 10);
                    $("#txtSignoffAmt").addClass("required");
                    signoff_flag=1;
                }
            }
            if(signoff_flag == 0)
            {
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                var action = 'save';
                var ajaxurl = API_URL + 'index.php/DailyRevenue/updateCashRevenuePosting';
                var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
                var params= {branch_id:selectedBranch,selected_date:selectedRevenueDate,rawRevenueCashPosting:revenueCashPosting,opening_balance:$("#opening_bal").html(),amount_collected:$("#amt_collected").html(),amount_deposited:$("#amt_deposited").html(),signoff_amount:$("#signoff_amt").html(),closing_balance:$("#closing_bal").html(),company_id:$("#feeCompanyList > .active").attr('data-id'),paymentType:selectedPaymentType,signoffAmount:signoffAmount,signoffComments:signoffComments};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess:RevenuePostingResponse});
            }
        }
        else if(flag == 1)
        {
            notify('Amount, Bank Voucher and Bank Receipt cannot be empty', 'error', 10);
        }
        else if(flag == 2)
        {
            notify('Bank Voucher and Bank Receipt cannot be empty', 'error', 10);
        }
        else if(flag == 3)
        {
            notify('Amount and Bank Receipt cannot be empty', 'error', 10);
        }
        else if(flag == 4)
        {
            notify('Amount and Bank Voucher cannot be empty', 'error', 10);
        }
        else if(flag == 5)
        {
            notify('Bank Receipt cannot be empty', 'error', 10);
        }
        else if(flag == 6)
        {
            notify('Bank Voucher cannot be empty', 'error', 10);
        }
        else if(flag == 7)
        {
            notify('Amount cannot be empty', 'error', 10);
        }
    }

    function RevenuePostingResponse(response)
    {
        alertify.dismissAll();
        if(response['status'] == true)
        {
            $("#postingDetails").hide();
            $("#postingDetails").slideUp(function()
            {
                $("#postingDetails").html("");
                $("#UpdateBtn").addClass('hidden');
                $("#signoffSection").addClass('hidden');

                $("#txtSignoffAmt").val('');
                $("#txaSignoffComments").val('');
                Materialize.updateTextFields();
            });
            $("#financeTable span").html(0);
            notify(response.message, 'success', 10);
        }
        else
        {
            notify(response.message, 'error', 10);
        }
    }

    function showSignOffData()
    {
        $("#signoffSection .signoffDetails").slideToggle();
        $("#txtSignoffAmt").val('');
        $("#txaSignoffComments").val('');
        Materialize.updateTextFields();
    }
    function revenueSignoffAmount(This)
    {
        if($(This).val()!='')
        {
            $("#signoff_amt").html(parseInt($(This).val()));
            var closing_amt=checkClosingBalance();
            if(closing_amt < 0)
            {
                notify("Signoff amount exceeds the closing amount","error",10);
                $(This).val(0);
                $("#signoff_amt").html(0);
            }
        }
        calculateClosingBalance();
    }

    $("#userBranchList").change(function()
    {
        changeDefaultBranch(this);
    });

    function loadbankReceipt(This,index)
    {
        var userID=$('#hdnUserId').val();
        var action='list';
        var fileUpload = document.getElementById("bankReceipt["+index+"]");
        var selectedFile = $(This).val();
        var extension = selectedFile.split('.');
        $('input[name="hdnBankReceiptName['+index+']"]').val('');
        $('input[name="hdnBankReceiptPath['+index+']"]').val('');
        var ajaxurl=API_URL+'index.php/DailyRevenue/uploadBankReceipt';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        if (fileUpload.value != null)
        {
            var uploadFile = new FormData();
            var files = $(This).get(0).files;
            if (files.length > 0) {
                uploadFile.append("file", files[0]);
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    dataType: "json",
                    data: uploadFile,
                    contentType: false,
                    processData: false,
                    headers: headerParams,
                    beforeSend:function()
                    {
                        $(This).attr("disabled","disabled");
                    },
                    success: function (response)
                    {
                        $(This).removeAttr("disabled");
                        var templateUrl = response.data.template_path;
                        var fileName = response.data.fileName;
                        $('input[name="hdnBankReceiptName['+index+']"]').val(fileName);
                        $('input[name="hdnBankReceiptPath['+index+']"]').val(templateUrl);
                        $('input[name="hdnTemplatePath['+index+']"]').val(fileName);
                    },
                    error:function(response)
                    {
                        alertify.dismissAll();
                        if(response.responseJSON.message=="Access Token Expired")
                        {
                            logout();
                        }
                        else if(response.responseJSON.message=="Invalid credentials")
                        {
                            logout();
                        }
                        else
                        {
                            notify('Something went wrong. Please try again', 'error', 10);
                        }
                    }
                });
            }
            else
            {
                $(This).parent().find('.file-path-wrapper').addClass("required");
                $(This).parent().find('.file-path-wrapper').after('<span class="required-msg">required field</span>');
            }
        }
    }
</script>