<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
<input type="hidden" id="eid" name="eid" value="0"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Email Template </h3>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" access-element="list"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom table-info" id="emailList">
                    <thead>
                        <tr>
                            <th width="30%">Title</th>
                            <th width="60%">Subject</th>
                            <th width="5%">Attachment</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="emailTemplateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="750">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeEmailModal(this)"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="countryModalLabel">Edit Template</h4>
                    </div>
                    <form action="/" method="post" id="emailTemplateform" novalidate="novalidate">
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="title" type="text" name="title" class="validate" class="formSubmit" required>
                                    <label for="title">Title <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="subject" type="text" name="key" class="validate" class="formSubmit" required>
                                    <label for="subject">Subject <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="content" class="materialize-textarea" name="content" class="validate" class="formSubmit" style="height:15rem"></textarea>
                                    <label for="content">Content <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="file-field input-field">
                                    <div class="btn file-upload-btn">
                                        <span>Upload Attachment</span>
                                        <input type="file" id="imageUpload" name="imageUpload[]" multiple="multiple">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" placeholder="Upload" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" id="filesPresent"></div>
                            <!-- Start -->
                            <div class="col-sm-12 mt15 template-table modal-scrol">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Description</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{leadName}}</td>
                                            <td>Lead Name</td>
                                        </tr>
                                        <tr>
                                            <td>{{leadEmail}}</td>
                                            <td>Lead Email</td>
                                        </tr>
                                        <tr>
                                            <td>{{enrollNo}}</td>
                                            <td>Enrollment Number</td>
                                        </tr>
                                        <tr>
                                            <td>{{branchName}}</td>
                                            <td>Branch Name</td>
                                        </tr>
                                        <tr>
                                            <td>{{courseName}}</td>
                                            <td>Course Name</td>
                                        </tr>
                                        <tr>
                                            <td>{{batchCode}}</td>
                                            <td>Batch Code</td>
                                        </tr>
                                        <tr>
                                            <td>{{companyName}}</td>
                                            <td>Lead Company Name</td>
                                        </tr>
                                        <tr>
                                            <td>{{institutionName}}</td>
                                            <td>Lead Institution Name</td>
                                        </tr>
                                        <tr>
                                            <td>{{feeheadName}}</td>
                                            <td>Fee Head Detail</td>
                                        </tr>
                                        <tr>
                                            <td>{{amountPayable}}</td>
                                            <td>Amount Payable</td>
                                        </tr>
                                        <tr>
                                            <td>{{amountPaid}}</td>
                                            <td>Amount Paid</td>
                                        </tr>
                                        <tr>
                                            <td>{{due_days_amt}}</td>
                                            <td>Due Amount</td>
                                        </tr>
                                        <tr>
                                            <td>{{due_days_html}} </td>
                                            <td>Due Day status</td>
                                        </tr>
                                        <tr>
                                            <td>{{currentDate}} </td>
                                            <td>Current Date</td>
                                        </tr>
                                        <tr>
                                            <td>{{last_modified}} </td>
                                            <td>Last Modified Date</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- Ends -->
                            <div class="col-sm-12 clearfix mt15 p0">
                                <div class="col-sm-6">
                                    <button type="button" class="btn blue-btn" onclick="javascript:$('#testMailField').toggle();"><i class="icon-right mr8"></i>Test Mail</button>
                                </div>
                                <div class="col-sm-12 mt15 p0 pb15 pr15 pl15" id="testMailField" style="display: none; border: 1px solid #eee;">
                                    <div class="col-sm-12 mt15">
                                        <div class="input-field">
                                            <input id="testmailid" type="text" name="testmailid" class="validate" class="formSubmit" required>
                                            <label for="testmailid">Email <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="button" class="btn blue-btn" onclick="testMail(this)"><i class="icon-right mr8"></i>Send Mail</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="actionButton" type="submit" class="btn blue-btn" onclick="SaveEmail(this)"><i class="icon-right mr8"></i>Add</button>
                        <button access-element="preview" type="button" class="btn blue-btn" onclick="createPreview(this)"><i class="icon-right mr8"></i>Preview</button>
                        <!--<button type="button" class="btn blue-btn" onclick="testMail(this)"><i class="icon-right mr8"></i>Test Mail</button>-->
                        <button type="button" class="btn blue-light-btn" onclick="closeEmailModal(this)"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildEmailDataTable();
    });
    
    function createPreview(This){
        var id = $('#eid').val();
        var win = window.open('<?php echo BASE_URL; ?>app/template_preview/'+id, '_blank');
        if(win){
            //Browser has allowed it to be opened
            win.focus();
        }else{
            //Broswer has blocked it
            customAlert('Error', 'Please allow popups for this site');
        }
    }
    
    function testMail(This){
        $('#testMailField span.required-msg').remove();
        $('#testMailField input').removeClass("required");
        var email = $('#testmailid').val().trim();
        var regx_txtEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var flag = 0;
        if(email.length <= 0){
            $("#testmailid").addClass("required");
            $("#testmailid").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtEmail.test(email) === false) {
            $("#testmailid").addClass("required");
            $("#testmailid").after('<span class="required-msg">Invalid</span>');
            flag = 1;
        }
        if(flag != 1){
            notify('Processing..', 'warning', 10);
            var id = $('#eid').val();
            var userId = $('#userId').val();
            var ajaxurl = API_URL + "index.php/EmailTemplates/sendTestMail/";
            var params = {eid: id, email: email};
            var action = 'edit';

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: This, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: function (response){
                alertify.dismissAll();
                if (response == -1 || response['status'] == false) {
                    //error
                    notify('Something went wrong', 'error', 10);
                } else {
                    notify('Test mail has been send', 'success', 10);
                    $('#testMailField').val('');
                    $('#testMailField span.required-msg').remove();
                    $('#testMailField input').removeClass("required");
                    $('#testMailField').hide();
                }
            }});
        } else {
            alertify.dismissAll();
            notify('Validation Error', 'error', 10);
        }
    }

    function buildEmailDataTable() {
        var userId = $('#userId').val();
        var ajaxurl=API_URL+'index.php/EmailTemplates/getAllEmail';
        var params = {'reference_type':$pageUrl};
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        $("#emailList").dataTable().fnDestroy();
        var EmailTableObj = $('#emailList').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                                $('.dataTables_paginate').css("display", "block");
                                $('.dataTables_length').css("display", "block");
                                //$('.dataTables_filter').css("display", "block");
                } else {
                                $('.dataTables_paginate').css("display", "none");
                                $('.dataTables_length').css("display", "none");
                                //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            columns: [
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.title;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.subject;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        var emailRawData='<span>'+data.total+'</span>';
                        emailRawData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        emailRawData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                        emailRawData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                        emailRawData+='</a>';
                        emailRawData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        emailRawData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        emailRawData+='<li><a access-element="edit" href="javascript:;" onclick="getEmailDetails(this,'+data.eid+',0)">Edit</a></li>';
                        emailRawData+='</ul>';
                        emailRawData+='</div>';
                        return emailRawData;
                    }
                }
            ]
        } );
        DTSearchOnKeyPressEnter(EmailTableObj);
    }
    
    function closeEmailModal(){
        $('#eid').val(0);
        
        $('#emailTemplateform')[0].reset();
        
        $('#emailTemplateModal input, textarea').removeClass("required");
        $('#emailTemplateModal textarea').find('label').removeClass("active");
        $('#emailTemplateModal label').removeClass("active");
        $('#emailTemplateModal span.required-msg').remove();
        
        $('#filesPresent').html('');
        $('#testMailField span.required-msg').remove();
        $('#testMailField input').removeClass("required");
        $('#testMailField').hide();
        
        $('#emailTemplateModal').modal('toggle');
    }
    
    function getEmailDetails(This, id, status){
        notify('Processing..', 'warning', 10);
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Update');
        
        $('#emailTemplateModal input').removeClass("required");
        $('#emailTemplateModal textarea').find('label').addClass("active");
        $('#emailTemplateModal label').addClass("active");
        
        $('#filesPresent').html('');
        
        $('#eid').val(id);
        
        var userId = $('#userId').val();
        var ajaxurl = API_URL + "index.php/EmailTemplates/getEmailTemplateById/";
        var params = {eid: id};
        var action = 'edit';

        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: This, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: function (response){
            if (response == -1 || response['status'] == false) {
                //error
            } else {
                alertify.dismissAll();
                var rawData = response.data;
                $('#title').val(rawData.title);
                $('#subject').val(rawData.subject);
                /*$('#key').attr('disabled',true);*/
                $('#content').val(rawData.content);
                $("#content").next('label').addClass("active");
                
                if(rawData.attachment.length > 0){
                    var filesPresent = '';
                    for(var index=0; index<rawData.attachment.length; index++){
                        filesPresent += '<div class="col-md-12 p0" style="word-wrap:break-word">'+rawData.attachment[index].original_name+'<span class="ml15"><a class="reportFilter-unselectall" href="javascript:;" onclick="deleteFiles(this,'+rawData.attachment[index].etid+')"><i class="icon-times red"></i></a></span></div>';
                    }
                    $('#filesPresent').html(filesPresent);
                }
                if(status == 0)
                    $('#emailTemplateModal').modal('toggle');
            }
        }});
    }
    
    function SaveEmail(This){
        notify('Processing..', 'warning', 10);
        var flag = 0;
        $('#emailTemplateModal input, textarea').removeClass("required");
        $('#emailTemplateModal input').parent().find('label').addClass("active");
        $('#emailTemplateModal span.required-msg').remove();
        var content = $('#content').val().trim();
        var subject = $('#subject').val().trim();
        var title = $('#title').val().trim();
        
        if (content == '') {
            $("#content").addClass("required");
            $("#content").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
            flag = 1;
        }
        
        if (title == '') {
            $("#title").addClass("required");
            $("#title").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
            flag = 1;
        } else if(title.length > 250){
            $("#title").addClass("required");
            $("#title").after('<span class="required-msg">Can\'t exceed more than 250 characters</span>');
            flag = 1;
        } 
        if (subject == '') {
            $("#subject").addClass("required");
            $("#subject").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
            flag = 1;
        } else if(subject.length > 250){
            $("#subject").addClass("required");
            $("#subject").after('<span class="required-msg">Can\'t exceed more than 250 characters</span>');
            flag = 1;
        } 
        
        var max_fileUpload_size = 2.048;    /*For max file size for uploaded file(In MB)*/
        var files = $("#imageUpload").get(0).files;
        var selectedFile = $("#imageUpload").val(); /*Orginal name*/
        var extArray = ['gif', 'jpg', 'jpeg', 'png', 'pdf'];
        var extension = selectedFile.split('.');
        /*if (files.length > 0 && $.inArray(extension[extension.length - 1], extArray) <= -1) {
            $(".file-path").focus();
            $(".file-path").addClass("required");
            $(".file-path").after('<span class="required-msg">Allowed only ' + extArray.toString() + '</span>');
            flag = 1;
        } else if (files.length > 0 && ((files[0].size / 1024) / 1024) > max_fileUpload_size)
        {
            var sizeOfDoc = parseInt(max_fileUpload_size)==0?max_fileUpload_size:parseInt(max_fileUpload_size);
            $(".file-path").focus();
            $(".file-path").addClass("required");
            $(".file-path").after('<span class="required-msg">File can\'t be more than '+sizeOfDoc+'Mb</span>');
            flag = 1;
        }*/
        if(flag != 1){
            var id = $('#eid').val();
            var action = 'edit';
            var userId = $('#userId').val();

            var ajaxurl = API_URL + "index.php/EmailTemplates/saveTemplates/eid/"+id;
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            
            var fileUpload = document.getElementById("imageUpload");
            if (fileUpload.value != null) {
                var uploadFile = new FormData();
                if (files.length > 0) {
                    for(var index=0; index<files.length; index++){
                        var extension = files[index].name.split('.');
                        uploadFile.append("file[]", files[index]);
                        uploadFile.append("image[]", 'email_attachment_' + (new Date().getTime()*(Math.floor((Math.random() * 100) + 1)))+'.'+extension[extension.length - 1]);
                        uploadFile.append('orginalName[]', files[index].name);
                    }
                }
                uploadFile.append('content', content);
                uploadFile.append('title', title);
                uploadFile.append('subject', subject);
                
                $.ajax({
                    /*type: 'POST',*/
                    type: 'POST',
                    url: ajaxurl,
                    dataType: "json",
                    data: uploadFile,
                    contentType: false,
                    processData: false,
                    headers: headerParams,
                    beforeSend: function () {
                        $(This).attr("disabled", "disabled");
                    }, success: function (response) {
                        $(This).removeAttr("disabled");
//                        console.log(response);
                        savedTemplateResposnse(response);
                    }, error: function (response) {
                        alertify.dismissAll();
                        notify('Something went wrong. Please try again', 'error', 10);
                    }
                });
            }
            
        }else{
//            console.log('asdasd');
            alertify.dismissAll();
            notify('Validation Errors', 'error', 10);
        }
    }
    
    function savedTemplateResposnse(response) {
        alertify.dismissAll();
        $('#emailTemplateModal label').removeClass("active");
        $('#emailTemplateModal span.required-msg').remove();
        if (response == -1 || response['status'] == false){
            var id = '';
            notify('Validation Error', 'error', 10);
            for (var a in response.data) {
                id = '#' + a;
                if (a == 'imageUpload') {
                    $(".file-path").addClass("required");
                    $(".file-path").after('<span class="required-msg">' + response.data[a] + '</span>');
                } else {
                    $(id).addClass("required");
                    $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                }
            }
        } else {
            notify('Data Saved Successfully', 'success', 10);
            buildEmailDataTable();
            closeEmailModal(this);
        }
    }
    
    function deleteFiles(This, filesId){
        var conf = "Are you sure want to remove this attachment ?";
        customConfirmAlert('Attachment Status' , conf);
        $("#popup_confirm #btnTrue").attr("onclick","deleteFilesConfirm('+This+','"+filesId+"')");
    }
    
    function deleteFilesConfirm(This, filesId){
        var id = $('#eid').val();
        
        var userId = $('#userId').val();
        var ajaxurl = API_URL + "index.php/EmailTemplates/deleteAttachment/eid/"+id+"/etid/"+filesId;
        var action = 'edit';
        var method = 'POST';

        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: This, method: method, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response){
            if (response == -1 || response['status'] == false){
                alertify.dismissAll();
                notify(response.message, 'error', 10);
            } else {
                alertify.dismissAll();
                notify('Attachment Deleted Successfully', 'success', 10);
                getEmailDetails(This, id, 1);
                buildEmailDataTable();
            }
        }});
    }
</script>
