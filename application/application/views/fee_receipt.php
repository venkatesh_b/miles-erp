<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 4/4/16
 * Time: 3:14 PM
 */
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo BASE_URL;?>assets/images/favicon.ico" type="image/x-icon">
    <title>::MILES::</title>
    <script src="<?php echo BASE_URL;?>assets/js/jquery.min.js"></script>
    <script src="<?php echo BASE_URL;?>assets/js/jquery-ui-1.11.0.js"></script>
    <script>
        var BASE_URL='<?php echo BASE_URL; ?>';
        var API_URL='<?php echo API_URL; ?>';
        $context = 'Fee Collection';
        $pageUrl = 'feecollection';
        $serviceurl = 'feecollection';
        $accessToken ='<?php echo $accessToken; ?>';

    </script>
    <script src="<?php echo BASE_URL;?>assets/js/commonajax.js"></script>
    <style>
        .wrapper:nth-child(2n+2) {page-break-after: always !important;}
        @media print{
            #FeeReceipts
            {
                display:block;
            }
            .wrapper:nth-child(2n+2) {page-break-after: always !important;}
            .wrapper:nth-child(2n+2){
                margin-top: 100px !important;
            }
        }
    </style>
</head>
<body style="margin:0px;padding:0;">
<input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
<input type="hidden" id="hdnFeeCollectionId" value="<?php echo $Id; ?>" readonly />

<div id="FeeReceipts">

</div>


<script>
    $(document).ready(function()
    {
        getFeeCollectionDetails();

    });
    function moneyFormat(x) {
        x = x.toString();
        var lastThree = x.substring(x.length - 3);
        var otherNumbers = x.substring(0, x.length - 3);
        if (otherNumbers != '')
            lastThree = ',' + lastThree;
        return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
    }

    function getFeeCollectionDetails()
    {
        var ajaxurl = API_URL + 'index.php/FeeCollection/FeeCollectionDetails';
        var userID=$('#hdnUserId').val();
        var feeCollectionID=$('#hdnFeeCollectionId').val();
        var params = {feeCollectionID: feeCollectionID};
        var headerParams = {action:'list',context:$context,serviceurl:$serviceurl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $.ajax({
            type: "GET",
            url: ajaxurl,
            dataType: "json",
            data: params,
            headers: headerParams,
            success: function (response){
                if(response['status']==true)
                {
                    var courseName = (response['data'][0]['courseName'] == null || response['data'][0]['courseName'] == '') ? "----" :response['data'][0]['courseName'];
                    var feeReceiptData='';
                    for(var r=0;r<2;r++)
                    {
                        //cityName
                        feeReceiptData+='<div data-reciept="assets" class="wrapper" style="width: 788px;height: 26.750em;padding: 100px 40px 0;margin: 0 auto;position: relative;font-family: Arial, Helvetica, sans-serif;background:transparent;">';
                        feeReceiptData+='<img class="logo" alt="logo" src="'+BASE_URL+'assets/images/logo.png" style="position:absolute;top:100px;left:44px;width:15%;">';
                        feeReceiptData+='<p class="campus" style="font-size: 24px;font-weight: bold;letter-spacing: 3px;color: #000;position: absolute;top: 95px;right: 150px;"></p>';
                        feeReceiptData+='<h1 style="text-align: center;font-size: 21px;color: #000;margin: 15px auto 2px;margin-bottom: 2px !important;width: 480px;">'+response['data'][0]['companyName']+' </h1>';
                        feeReceiptData+='<p style="text-align: center;font-size: 14px;color: #000;text-transform: uppercase;margin-top: 0;">'+response['data'][0]['branchName']+'</p>';
                        feeReceiptData+='<table cellspacing="0" cellpadding="4" border="0" class="studentDetails" style="width: 100% !important;">';
                        feeReceiptData+='<tbody>';
                        feeReceiptData+='<tr style="font-size: 14px;">';
                        feeReceiptData+='<td style="font-size: 14px;width:25%;">Student/Lead No:</td>';
                        feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+response['data'][0]['lead_number']+'</td>';
                        feeReceiptData+='<td style="width:25%;">Course:</td>';
                        feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+courseName+'</td>';
                        feeReceiptData+='</tr>';
                        feeReceiptData+='<tr style="font-size: 14px;">';
                        feeReceiptData+='<td style="width:25%;">Batch:</td>';

                        if(response['data'][0]['alias_name'] == null || response['data'][0]['alias_name'] == '')
                            feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+response['data'][0]['batchInfo']+'</td>';
                        else
                            feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+response['data'][0]['batchInfo']+'('+response['data'][0]['alias_name']+')</td>';

                        feeReceiptData+='<td style="width:25%;">Receipt No:</td>';
                        feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+response['data'][0]['receipt_number']+'</td>';
                        feeReceiptData+='</tr>';
                        feeReceiptData+='<tr style="font-size: 14px;">';
                        feeReceiptData+='<td style="width:25%;">Name:</td>';
                        feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+response['data'][0]['studentName']+'</td>';
                        feeReceiptData+='<td style="width:25%;">Date:</td>';
                        if(response['data'][0]['payment_type']=='Online')
                            feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+response['data'][0]['receipt_date_time']+'</td>';
                        else
                            feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+response['data'][0]['receipt_date']+'</td>';

                        feeReceiptData+='</tr>';
                        feeReceiptData+='<tr style="font-size: 14px;">';
                        feeReceiptData+='<td style="width:25%;">Payment Mode:</td>';
                        feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+response['data'][0]['payment_type']+'</td>';
                        feeReceiptData+='<td style="width:25%;">Payment Type:</td>';
                        feeReceiptData+='<td class="font-bold" colspan="7" style="font-weight:600;width:25%;">'+response['data'][0]['payment_mode']+'</td>';
                        feeReceiptData+='</tr>';
                        feeReceiptData+='</tbody>';
                        feeReceiptData+='</table>';
                        feeReceiptData+='<table cellspacing="0" cellpadding="2" border="1" class="feeDetails mt20" data-genre="null" style="width:100% !important;margin-top:20px;border-collapse: collapse;">';
                        feeReceiptData+='<thead>';
                        feeReceiptData+='<tr style="font-size: 14px;">';
                        feeReceiptData+='<td class="font-bold" data-genre="nullType" style="font-weight:600;padding:5px;">Fee head</td>';
                        feeReceiptData+='<td class="font-bold" data-genre="nullInstallment" data-content="installment-header" style="font-weight:600;padding:5px;">Installment</td>';
                        feeReceiptData+='<td class="font-bold" width="150" align="right" data-genre="nullTotal" style="font-weight:600;padding:5px;">Total Amount(<span class="icon-rupee"></span>)</td>';
                        feeReceiptData+='</tr>';
                        feeReceiptData+='</thead>';
                        feeReceiptData+='<tbody>';
                        feeReceiptData+='<tr style="font-size: 14px;">';
                        feeReceiptData+='<td data-genre="nullType" style="padding:5px;">'+response['data'][0]['feeHead']+'</td>';
                        feeReceiptData+='<td data-genre="nullInstallment" data-content="installment" style="padding:5px;">1</td>';
                        feeReceiptData+='<td align="right" data-genre="nullTotal" style="padding:5px;">'+moneyFormat(response['data'][0]['amount_paid'])+'</td>';
                        feeReceiptData+='</tr>';
                        feeReceiptData+='</tbody>';
                        feeReceiptData+='</table>';
                        feeReceiptData+='<table cellspacing="0" cellpadding="4" border="0" class="total" style="width:100% !important;">';
                        feeReceiptData+='<tbody>';
                        feeReceiptData+='<tr style="font-size: 14px;">';
                        feeReceiptData+='<td colspan="6" style="text-transform: capitalize;">Rupees '+NumberinWords(Math.abs(response['data'][0]['amount_paid']))+'</td>';
                        feeReceiptData+='<td class="font-bold" width="150" align="right" style="text-align: right;font-weight:bold;"><span>&#8377;</span> '+moneyFormat(response['data'][0]['amount_paid'])+'</td>';
                        feeReceiptData+='</tr>';
                        feeReceiptData+='<tr></tr>';
                        feeReceiptData+='</tbody>';
                        feeReceiptData+='</table>';
                        feeReceiptData+='<div style="font-size: 14px;" >';
                        feeReceiptData+='<div style="display: inline-block;position: absolute;left: 40px;bottom: 0;">';
                        if(r == 0)
                            feeReceiptData+='Student Copy';
                        else
                            feeReceiptData+='Office Copy';
                        feeReceiptData+='</div>';
                        /*feeReceiptData+='<div style="display: inline-block;position: absolute;right: 40px;bottom: 0;">For WB</div>';*/
                        feeReceiptData+='</div>';
                        feeReceiptData+='</div>';
                    }
                    $("#FeeReceipts").html(feeReceiptData);
                }
            },
            complete:function(){
                window.print();
            }
        });
    }
</script>
</body>
</html>