<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 25/3/16
 * Time: 5:13 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
        <h3 class="content-header">Fee Collection</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="Fee Collect"><a class="tooltipped" data-position="top" data-tooltip="Collect Fee" href="<?php echo APP_REDIRECT;?>feecollection_create"><i class="icon-plus-circle"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom table-info" id="fee-collection-list">
                    <thead>
                        <tr>
                            <th>Receipt Number</th>
                            <th>Company</th>
                            <th>Student Number</th>
                            <th>Name</th>
                            <th>Receipt Date</th>
                            <th>Payment Status</th>
                            <th>Created By</th>
                            <th>Created Date</th>
                            <th class="border-r-none text-right">Amount</th>
                            <th class="no-sort">&nbsp;</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>

<script type="text/javascript">
    docReady(function(){
        buildFeeCollectionDataTable();
    });

    function buildFeeCollectionDataTable()
    {
        var ajaxurl=API_URL+'index.php/FeeCollection/feeCollectionList';
        var userID=$('#hdnUserId').val();
        var branchID=$('#userBranchList').val();
        var params = {branchID: branchID};
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#fee-collection-list").dataTable().fnDestroy();
        $tableobj=$('#fee-collection-list').DataTable( {
            "fnDrawCallback": function() {
                $("#fee-collection-list thead th").removeClass("icon-rupee");
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            "order": [[ 0, "desc" ]],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                    {
                        var receipt_data='<a class="font-bold text-uppercase" target="_blank" href="'+BASE_URL+'app/feereceipt/'+data.fee_collection_id+'">'+data.receipt_number+'</a>';
                        return receipt_data;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.Company;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.lead_number;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.studentName;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.receipt_date;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.payment_status;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.createdBy;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.created_on;
                    }
                },
                {
                    data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        return moneyFormat(data.amount_paid);
                    }
                }
                ,
                {
                    data: null, render: function ( data, type, row )
                    {
                        var roleUsersData='';
                        roleUsersData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        roleUsersData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                        roleUsersData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                        roleUsersData+='</a>';
                        roleUsersData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        roleUsersData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        roleUsersData+='<li access-element="Cancel Receipt" ><a onclick="cancelFeeReceiptModel('+data.fee_collection_id+')">Cancel Receipt</a></li>';
                        roleUsersData+='</ul>';
                        roleUsersData+='</div>';
                        return roleUsersData;
                    }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }

    function cancelFeeReceiptModel(feeCollectionId)
    {
        var userID=$('#hdnUserId').val();
        customConfirmAlert('Cancel Fee Receipt','Are you sure want to cancel this receipt?');
        $("#popup_confirm #btnTrue").attr('onClick','cancelFeeReceipt('+feeCollectionId+','+userID+')');
    }

    function cancelFeeReceipt(feeCollectionId,userId)
    {
        alertify.dismissAll();
        notify('Processing', 'warning', 50);
        var ajaxurl=API_URL+'index.php/FeeCollection/cancelFeeCollection';
        var action='cancel receipt';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userId};
        var params = {feeCollectionID: feeCollectionId,userID:userId};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: cancelFeeReceiptResponse});
    }
    function cancelFeeReceiptResponse(response)
    {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false)
        {
            if(response['message']==null || response['message']=='')
                notify('Something went wrong', 'error', 10);
            else
                notify(response['message'], 'error', 10);
        }
        else
        {
            notify('Receipt cancelled successfully', 'success', 10);
            buildFeeCollectionDataTable();
        }
    }


    $( "#userBranchList" ).change(function() {
        buildFeeCollectionDataTable();
        changeDefaultBranch();
    });
</script>