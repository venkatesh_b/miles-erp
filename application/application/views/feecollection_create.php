<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 28/3/16
 * Time: 3:21 PM
 */
$exp=0;
if($Id!='' && $Sub_id!='') {
    $exp = explode('-',urldecode($Id));
    $branchText = explode('~',urldecode($Sub_id));
    if(is_array($branchText) && count($branchText)>1){
        $branchText = $branchText[1]; 
    }
    $branchUrl = str_replace('~', '/', $Sub_id);
}
else{
    $exp=0;
}
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">Fee Collection Create</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="" href="<?php echo APP_REDIRECT;?><?php echo (($Id!='' && $Sub_id!='' && isset($exp[1]) && trim($exp[1])!='0' && trim($exp[1])!='') ? $exp[1].'/'.$branchUrl : 'feecollection'); ?>"><i class="icon-times"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <form id="ManageFeeCollectionForm" method="post"  ><!--onsubmit="showLeadFeeDetails(); return false; "-->
                <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
                <div class="col-sm-12 p0" id="proceedWrapper">
                        <div class="col-sm-4">
                            <div class="input-field">
                                <select id="feeCollectionType">
                                    <!--<option value="" disabled selected>--select--</option>-->
                                    <option value="1">Regular Fee</option>
                                    <option value="2">UnAssigned Fee</option>
                                    <!--<option value="2">Other</option>-->
                                </select>
                                <label class="select-label">Collection Type</label>
                            </div>
                        </div>
                        <div class="col-sm-4 pl0">
                            <div class="input-field">
                                <input id="txtLeadNumber" name="txtLeadNumber" type="text" class="validate" />
                                <label for="txtLeadNumber">Mobile Number</label>
                                <input id="hdnLeadCourse" name="hdnLeadCourse" type="hidden" class="validate" />
                            </div>
                        </div>

                        <div class="col-sm-2 pl0">
                            <a href="javascript:;" id="ShowLeadFeeDetails" class="btn blue-btn btn-sm mt5" onclick="showLeadFeeDetails();">Proceed</a>
                            <!--<input type="submit" class="btn blue-btn btn-sm mt5" value="Proceed"  >-->
                        </div>
                </div>

                <div class="col-sm-12 p0" id="feeCollectionCreate-studentDetails">
                    <div class="col-sm-12 white-bg pl0 clearfix p15">
                        <div class="img-container">
                            <div class="img-wrapper"> <img id="userImage" src="<?php echo BASE_URL ?>assets/images/male-icon.jpg"> <span id="leadNumber" class="img-text">CPA1001</span> </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3 mtb10">
                                    <p class=" label-text">Name</p>
                                    <label class="label-data" id="leadName"></label>
                                    <input id="hdnBranchXrefLeadId" name="hdnBranchXrefLeadId" type="hidden" value="">
                                    <input id="hdnCandidateFeeId" name="hdnCandidateFeeId" type="hidden" value="">
                                </div>
                                <div class="col-sm-3 mtb10">
                                    <p class=" label-text">Phone</p>
                                    <label class="label-data" id="leadPhone"></label>
                                </div>
                                <div class="col-sm-3 mtb10">
                                    <p class=" label-text">Email</p>
                                    <label class="label-data" id="leadEmail"></label>
                                </div>
                                <!--<div class="col-sm-3 mtb10">
                                    <p class=" label-text">Batch</p>
                                    <label class="label-data" id="leadBatch"></label>
                                </div>-->
                                <div class="col-sm-3 mtb10">
                                    <p class=" label-text">Batch</p>
                                    <div id="leadBatch"></div>
                                </div>
                                <div class="col-sm-3 mtb10">
                                    <p class=" label-text">Course</p>
                                    <label class="label-data" id="leadCourseName"></label>
                                </div>
                                <div class="col-sm-3 mtb10">
                                    <p class=" label-text">Due</p>
                                    <label class="label-data red" id="leadDueAmount"></label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p0 mt15" id="feeStructureTypeWrapper" style="display: none">
                        <div class="col-sm-4 p0"  >
                            <div class="input-field">
                                <select id="feeStructureType">
                                    <option value="">--Select--</option>
                                </select>
                                <label class="select-label">Fee Structure</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <a class="mt8 display-block" href="javascript:;" onclick="showLeadUnassignedFeeDetails()"><i class="icon-plus-circle f26"></i></a>
                        </div>
                    </div>
                    <div class="col-sm-12 p0 mt15 feeCollectionCreate-paymentTable" id="feeCollectionCreate-paymentTable-wrapper">
                        <table class="table table-responsive table-striped table-custom table-info mb10" id="FeecollectionItemsList">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Feehead</th>
                                <th>Company</th>
                                <th class="text-right">Payable</th>
                                <th class="text-right">Paid</th>
                                <th class="text-right">Balance</th>
                                <th class="text-right">Paying Now</th>
                            </tr>
                            <tr>
                                <td class="border-none p0 lh10">&nbsp;</td>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <div class="col-sm-12  text-right pr0">
                            <button class="btn blue-btn" id="processPayment" type="button"><i class="icon-right mr8"></i>Make Payment</button>
                            <!--<a href="javascript:;" class="btn blue-btn" id="processPayment" ><i class="icon-right mr8"></i>Make Payment</a>-->
                        </div>
                    </div>
                </div>

                <!--Fee collection create tabs-->
                <div class="col-sm-12 mt15 p0 p15 feeCollectioncreate-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs feecollection-tabs pull-left white-bg p10" role="tablist" id="PayableAmountsList">
                        <li role="presentation" class="active">
                            <a href="#publicationCreate" aria-controls="publicationCreate" role="tab" data-toggle="tab">Publication <span class="pull-right"><span class="fee-creation-amount">5,000</span><span class="fee-creation-greenCheck white-color"><i class="icon-right"></i></span></span> </a>
                        </li>
                        <li role="presentation">
                            <a href="#professionalCreate" aria-controls="professionalCreate" role="tab" data-toggle="tab">Professional <span class="pull-right"><span class="fee-creation-amount">15,000</span></span> </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content white-bg pull-right pt10 pb20" id="PayableAmountsModels">

                    </div>

                </div>
            </form>

        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="application/javascript">
    $context = 'Fee Collection';
    $pageUrl = 'feecollection';
    $serviceurl = 'feecollection';
    docReady(function(){
        verifyAccess();
        <?php if(isset($branchId)){ ?>
            $("#userBranchList").val('<?php echo $branchId; ?>');
            $("#userBranchList").material_select();
        <?php } ?>
    });

    $( "#userBranchList" ).change(function() {
        changeDefaultBranch();
    });

    function assignBatchId(This)
    {
        $("#leadBatch").attr('data-id',$(This).val());
    }
    function getUnassignedFeeStructures(){
        var ajaxurl = API_URL + 'index.php/FeeCollection/getBranchLeadInfoByLeadNumber';
        var branchID=$('#userBranchList').val();
        var lead_number=$("#txtLeadNumber").val();
        var lead_course=$("#hdnLeadCourse").val();
        var params = {branchID: branchID,leadNumber: lead_number};
        var userID = $('#hdnUserId').val();
        var action = 'fee collect';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: GetLeadInformationDetails});
    }

    $(function(){
        var headerParams = {
            action: 'add',
            context: $context,
            serviceurl: $serviceurl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $('input#txtLeadNumber').jsonSuggest({
            onSelect:function(item){
                $('input#txtLeadNumber').val(item.id);
                $('input#hdnLeadCourse').val(item.fk_course_id);
            },
            headers:headerParams,url:API_URL + 'index.php/FeeAssign/getLeadsInformation' , minCharacters: 2});
    });
</script>