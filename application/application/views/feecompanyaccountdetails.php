<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 17/2/16
 * Time: 12:31 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Fee Company Account Details</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Add Company Account Details" class="viewsidePanel" onclick="ManageCompanyAccounts(this,0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="companies-list">
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Account Number</th>
                            <th>Bank Name</th>
                            <th>IFSC Code</th>
                            <th>Branch Name</th>
                            <th>City</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="myCompanyDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageCompanyBankDetailsForm" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Company Details</h4>
                            <input type="hidden" id="hdnCompanyDetailsId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="ddlCompanies">
                                        <option value=""  selected>--Select--</option>

                                    </select>
                                    <label class="select-label">Company <em>*</em></label>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input onkeypress="return isNumberKey(event)" maxlength="30" id="txtAccountNumber" type="text" class="validate">
                                    <label for="txtAccountNumber">Account Number <em>*</em></label>
                                </div>
                            </div>



                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtBankName" type="text" class="validate">
                                    <label for="txtBankName">Bank Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtIFSC" type="text" class="validate">
                                    <label for="txtIFSC">IFSC Code</label>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtBranchName" type="text" class="validate">
                                    <label for="txtBranchName"> Branch Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtCity" type="text" class="validate">
                                    <label for="txtCity">City <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12" access-element="status">
                                <div class="switch display-inline-block">
                                    <label class="display-block">Status</label>
                                    <label>
                                        Off
                                        <input type="checkbox" id="txtBanksStatus" checked>
                                        <span class="lever"></span>
                                        On
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" readonly value="1" id="hdnBanksStatusValue"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="companyDetailsButton" class="btn blue-btn" onclick="SaveCompanyDetails(this)"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildCompanyAccountsListDataTable();
    });

    function buildCompanyAccountsListDataTable()
    {
        $(document).ready(function() {
            var ajaxurl=API_URL+'index.php/CompanyAccountDetails/getCompanyAccounts';
            var userID=$('#hdnUserId').val();
            var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            $("#companies-list").dataTable().fnDestroy();
            $tableobj=$('#companies-list').DataTable( {
                "fnDrawCallback": function() {
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                    }
                    buildpopover();
                    verifyAccess();
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": true,
                "bProcessing": true,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No records found",
                    "sZeroRecords": "No records found",
                    "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                },
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    headers:headerParams,
                    error:function(response) {
                        DTResponseerror(response);

                    }
                },
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false
                } ],

                columns: [
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.name;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.acc_no;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            return data.bank_name;
                        }
                    },

                    {
                        data: null, render: function ( data, type, row )
                    {
                        if(data.bank_ifsc==null || data.bank_ifsc.trim()=='')
                            data.bank_ifsc='----';
                        return data.bank_ifsc;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.bank_branch;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.bank_city;
                    }
                    },

                    {
                        data: null, render: function ( data, type, row )
                        {
                            var roleUsersData='';

                            var status="";
                            if(data.status==0){
                                status= "In Active";

                            }else{
                                status= "Active";
                            }
                            roleUsersData+=status;
                            roleUsersData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                            roleUsersData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                            roleUsersData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                            roleUsersData+='</a>';
                            roleUsersData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                            roleUsersData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                            roleUsersData+='<li><a access-element="edit" href="javascript:;" onclick="ManageCompanyAccounts(this,\''+data.company_bank_detail_id+'\')">Edit</a></li>';
                            roleUsersData+='<li><a access-element="delete" href="javascript:;" onclick="DeleteCompanyDetails(this,\''+data.company_bank_detail_id+'\')">Delete</a></li>';
                           // roleUsersData+='<li><a  href="javascript:;" onclick="StatusCompany(this,\''+data.company_id+'\')">'+(data.status == 1) ? "Active":"In Active";+'</a></li>';
                            roleUsersData+='</ul>';
                            roleUsersData+='</div>';
                            return roleUsersData;
                        }
                    },


                ]
            } );
            DTSearchOnKeyPressEnter();

        } );
    }

</script>