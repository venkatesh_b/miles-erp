<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 14/4/16
 * Time: 11:19 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnUserRole" value="<?php echo $userData['Role']; ?>" readonly />

    <div class="content-header-wrap">
        <h3 class="content-header">Fee Concession</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="Concession Request"><a class="tooltipped" data-position="top" data-tooltip="Create Request" class="viewsidePanel" onclick="ManageFeeConcession(this,0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="feeassign-list">
                    <thead>
                    <tr>
                        <th>Candidate Name</th>
                        <th>Candidate Number</th>
                        <th>Branch Name</th>
                        <th class="text-right">Amount Payable</th>
                        <th class="text-right">Amount Paid</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="FeeAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageFeeAssignForm" method="post" onsubmit="showConcessionLeadInfo(this);return false;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Fee Concession</h4>
                            <input type="hidden" id="hdnFeeAssignId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="col-sm-11 pl0">
                                    <div class="input-field">
                                        <input id="txtLeadNumber" type="text" class="validate">
                                        <label for="txtLeadNumber">Mobile Number <em>*</em></label>

                                    </div>
                                </div>
                                <div class="col-sm-1 p0 mt2 sky-blue">
                                    <!--<a href="javascript:;" class="btn blue-btn"  onclick="showConcessionLeadInfo(this)">GO</a>-->
                                    <input type="submit" value="GO" class="btn blue-btn">
                                </div>
                            </div>
                            <div id="leadInfoSnapShot" >
                            </div>
                        </div>
                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveModifyFeeButton"  class="btn blue-btn" onclick="saveModifedConcessionFee()"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelModifyFeeButton"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
docReady(function(){
    buildFeeAssignListDataTable();
    var headerParams = {
        action: 'list',
        context: $context,
        serviceurl: $pageUrl,
        pageurl: $pageUrl,
        Authorizationtoken: $accessToken,
        user: '<?php echo $userData['userId']; ?>'
    };
    $('input#txtLeadNumber').jsonSuggest({
        onSelect:function(item){
            $('input#txtLeadNumber').val(item.id);
        },
        headers:headerParams,url:API_URL + 'index.php/FeeAssign/getLeadsInformation' , minCharacters: 2});
});

    function buildFeeAssignListDataTable()
    {
        var ajaxurl=API_URL+'index.php/FeeAssign/getFeeConcessionList';
        var userID=$('#hdnUserId').val();
        var branchID=$('#userBranchList').val();
        var params = {branchID: branchID};
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#feeassign-list").dataTable().fnDestroy();
        $tableobj= $('#feeassign-list').DataTable( {
            "fnDrawCallback": function()
            {
                $("#feeassign-list thead th").removeClass("icon-rupee");
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                }
                else
                {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No concessions found",
                "sZeroRecords": "No concessions found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.lead_name;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.lead_number;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.branchName;
                    }
                },
                {
                    data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        var amount_payable=(data.amount_payable == null || data.amount_payable == '') ? "0.00":data.amount_payable;
                        return moneyFormat(amount_payable);
                    }
                },
                {
                    //concession
                    data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        var amount_paid=(data.amount_paid == null || data.amount_paid == '') ? "0.00":data.amount_paid;
                        return moneyFormat(amount_paid);
                    }
                },
                {
                    data:null, render: function (data, type, row )
                    {
                        return data.approved_status;
                    }
                }

            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function saveModifedConcessionFee()
    {
        alertify.dismissAll();
        var UserId = $('#hdnUserId').val();
        var userRole = $("#hdnUserRole").val();
        var flag=0;
        var index=0;
        var ConcessionFeeData = [];
        var candidateFeeId=$("#hdnCandidateFeeId").val();
        $('input[name^="txtConcessionAmount"]').removeClass("required");
        $('input[name^="txtConcessionAmount"]').each(function()
        {
            if(parseInt($(this).val()) > parseInt($('input[name^="hdnAmountPayable"]:eq('+index+')').val()))
            {
                $(this).addClass("required");
                flag=1;
            }
            else
            {
                if($(this).val() !='' && $(this).val() >=0)
                {
                    ConcessionFeeData.push($('input[name^="hdnCandidateFeeId"]:eq('+index+')').val()+'@@@@@@'+$('input[name^="hdnCandidateFeeItemId"]:eq('+index+')').val()+'@@@@@@'+$('input[name^="hdnAmountPayable"]:eq('+index+')').val()+'@@@@@@'+$(this).val());
                }
            }
            index++;
        });
        if(flag==1)
        {
            notify('Concession amount cannot be more than payable amount', 'error', 10);
            return false;
        }
        else
        {
            notify('Processing..', 'warning', 50);
            var ajaxurl = API_URL + 'index.php/FeeAssign/UpdateCandidateFeeConcession';
            var params = {candidateFeeId:candidateFeeId,feeConcessionData: ConcessionFeeData};
            var action='concession request';
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: UserId,
                userRole:userRole
            };
            commonAjaxCall({
                method: 'POST',
                requestUrl: ajaxurl,
                params: params,
                headerParams: headerParams,
                action: action,
                onSuccess: function (response) {
                    //console.log(response);
                    alertify.dismissAll();
                    buildFeeAssignListDataTable();
                    notify(response.message,'success',10);
                    $('#FeeAssign').modal('hide');
                    $('body').removeClass('modal-open');
                }
            });
        }
    }
    $( "#userBranchList" ).change(function() {
        buildFeeAssignListDataTable();
        changeDefaultBranch();
    });
</script>