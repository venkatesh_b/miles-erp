<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 14/4/16
 * Time: 11:19 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnUserRole" value="<?php echo $userData['Role']; ?>" readonly />

    <div class="content-header-wrap">
        <h3 class="content-header">Fee Concession Request</h3>
        <div class="content-header-btnwrap">

        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="feeassign-list">
                    <thead>
                    <tr>
                        <th>Candidate Name</th>
                        <th>Candidate Number</th>
                        <th>Branch Name</th>
                        <th class="text-right">Amount Payable</th>
                        <th class="text-right">Amount Paid</th>
                        <th class="border-r-none">Status</th>
                        <th class="no-sort"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="FeeAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageFeeAssignForm" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Approve Fee Concession</h4>
                            <input type="hidden" id="hdnFeeAssignId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div id="leadInfoSnapShot" >

                            </div>
                        </div>
                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveFeeConcessionButton"  class="btn blue-btn"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelModifyFeeButton"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildFeeAssignListDataTable();
    });
    function buildFeeAssignListDataTable()
    {
        var ajaxurl=API_URL+'index.php/FeeAssign/getFeeConcessionRequestList';
        var userID=$('#hdnUserId').val();
        var branchID=$('#userBranchList').val();
        var params = {branchID: branchID};
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#feeassign-list").dataTable().fnDestroy();
        $tableobj= $('#feeassign-list').DataTable( {
            "fnDrawCallback": function() {
                $("#feeassign-list thead th").removeClass("icon-rupee");
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No concession requests found",
                "sZeroRecords": "No concession requests found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.lead_name;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.lead_number;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.branchName;
                    }
                },
                {
                    data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        var amount_payable=(data.amount_payable == null || data.amount_payable == '') ? "0.00":data.amount_payable;
                        return moneyFormat(amount_payable);
                    }
                },
                {
                    data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        var amount_paid=(data.amount_paid == null || data.amount_paid == '') ? "0.00":data.amount_paid;
                        return moneyFormat(amount_paid);
                    }
                },
                {
                    data:null, render: function (data, type, row )
                    {
                        return data.approved_status;
                    }
                },
                {
                    data:null,render:function (data, type, row )
                    {
                        var feeConcessionReq='';
                        feeConcessionReq+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        feeConcessionReq+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                        feeConcessionReq+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                        feeConcessionReq+='</a>';
                        feeConcessionReq+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        feeConcessionReq+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        feeConcessionReq+='<li><a access-element="Manage Concession" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick="ConcessionRequest('+data.candidate_fee_id+',\'approve\')">Approve Request</a></li>';
                        feeConcessionReq+='<li><a access-element="Manage Concession" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick="ConcessionRequest('+data.candidate_fee_id+',\'reject\')">Reject Request</a></li>';
                        feeConcessionReq+='</ul>';
                        feeConcessionReq+='</div>';
                        return feeConcessionReq;
                    }
                }

            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function ConcessionRequest(candidateFeeId,status)
    {
        var ModelTitle='Concession Request',Description='';
        if(status=='approve')
        {
            Description = 'Are you sure want to approve the concession request';
            $("#popup_confirm #btnTrue").attr('onclick','CandidateFeeConcession('+candidateFeeId+')');
        }
        else
        {
            Description = 'Are you sure want to reject the concession request';
            $("#popup_confirm #btnTrue").attr('onclick','RejectCandidateFeeConcession('+candidateFeeId+')');
        }
        customConfirmAlert(ModelTitle,Description);
    }
    function RejectCandidateFeeConcession(candidateFeeId)
    {
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var userID = $('#hdnUserId').val();
        var action = 'manage concession';
        var ajaxurl=API_URL+'index.php/FeeAssign/RejectCandidateFeeConcessionRequest';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var params = {candidateFeeId:candidateFeeId,requestType:'reject'};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: RejectCandidateFeeConcessionResponse});
    }
    function RejectCandidateFeeConcessionResponse(response)
    {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            buildFeeAssignListDataTable();
            notify(response.message,'success',10);
        }
    }
    function CandidateFeeConcession(candidateFeeId)
    {
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var UserId = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/FeeAssign/getFeeAssignInfoByCandidateFeeId';
        var type = "GET";
        var action = 'manage concession';
        var params={candidateFeeId:candidateFeeId};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl,params:params, headerParams: headerParams, action: action, onSuccess:CandidateFeeConcessionResponse });
    }
    function CandidateFeeConcessionResponse(response)
    {
        alertify.dismissAll();
        if(response.status===false){
            notify(response.message,'error',10);
        }
        else
        {
            rawData=response.data[0];
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }
            if(rawData.companyName==null || rawData.companyName==''){
                rawData.companyName='----';
            }
            if(rawData.institution==null || rawData.institution==''){
                rawData.institution='----';
            }
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }

            var content = '<div class="col-sm-12 ">';
            content += '<div class="light-blue3-bg clearfix pt10 pb10">';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Name</span>';
            content += rawData.name;
            content += '<input type="hidden" id="hdnCandidateFeeId" value="' + rawData.candidate_fee_id + '" />';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Email</span>';
            content += '<p>' + rawData.email + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Phone</span>';
            content += '<p>' + rawData.phone + '</p>';
            content += '</div>';
            content += ' </div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Branch</span>';
            content += '<p>' + rawData.branchName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Course</span>';
            content += '<p>' + rawData.courseName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Qualification</span>';
            content += '<p>' + rawData.qualification + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Company</span>';
            content += '<p>' + rawData.companyName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Institution</span>';
            content += '<p>' + rawData.institution + '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            var disableAmnt='';
            //rawData.is_concession_allowed=1;
            if(rawData.is_concession_pending==0)
            {
                disableAmnt=' readonly disabled ';
            }
            else
            {
                disableAmnt='';
            }
            var Amounts = "";//amount_paid
            Amounts += '<div class="col-sm-12 mt15">'
                + '<table class="table table-responsive table-striped table-custom" id="feeassign-list">'
                + '<thead><tr>'
                + '<th>Fee Head</th><th>Due days</th><th class="text-right">Amount</th><th class="text-right">Amount Paid</th><th>Concession</th></tr>'
                + '</thead><tbody>';
            for(var i=0;i<response.data.length;i++)
            {
                Amounts += '<tr>';
                Amounts += '<td>' + response.data[i].feeHeadName + '</td>';
                Amounts += '<td>'+  response.data[i].due_days + '</td>';
                Amounts += '<td><span class="icon-rupee"></span>' + moneyFormat(response.data[i].amount_payable) + '</td>';
                Amounts += '<td><span class="icon-rupee"></span>' + moneyFormat(response.data[i].amount_paid) + '</td>';
                Amounts += '<td><div class="col-sm-8 pl0">';
                Amounts += '<input type="hidden" name="hdnCandidateFeeItemId[]" value="' + response.data[i].candidate_fee_item_id + '" />';
                Amounts += '<input type="hidden" name="hdnAmountPayable[]" value="' + response.data[i].amount_payable + '" />';
                Amounts += '<input class="modal-table-textfiled m0" type="text" name="txtConcessionAmount[]" value="' + response.data[i].modified_concession + '" onkeypress="return isNumberKey(event)" '+disableAmnt+' />';
                Amounts += '</div></td>';
                Amounts += '</tr>';
            }

            Amounts += '</tbody></table></div><div id="DataMessagePayment" class="col-sm-12 mt15 text-center"></div>';
            $('#leadInfoSnapShot').html(content+Amounts);
            $("#DataMessagePayment").hide();
            $("#DataMessagePayment").html('');
            /*if(rawData.is_amount_paid==1)
            {
                $("#DataMessagePayment").show();
                $("#DataMessagePayment").html("<b style='color:red;'>Already fee collected from this lead</b>");
                $('#saveFeeConcessionButton').attr('disabled',true);
            }
            else if(rawData.is_concession_pending==0)
            {
                $("#DataMessagePayment").show();
                $("#DataMessagePayment").html("<b style='color:red;'>Concession already given to this lead</b>");
                $('#saveFeeConcessionButton').attr('disabled',true);
            }
            else*/
            {
                $('#saveFeeConcessionButton').attr('disabled',false);
                $('#saveFeeConcessionButton').attr('onclick','ApproveCandidateFeeConcession('+rawData.candidate_fee_id+')');
            }
            $('#FeeAssign').modal('show');
        }
    }
    function ApproveCandidateFeeConcession(candidateFeeId)
    {
        var UserId = $('#hdnUserId').val();
        var userRole = $("#hdnUserRole").val();
        var flag=0;
        var index=0;
        var inflag=0;
        var ConcessionFeeData = [];
        //var candidateFeeId=$("#hdnCandidateFeeId").val();
        $('input[name^="txtConcessionAmount"]').removeClass("required");
        $('input[name^="txtConcessionAmount"]').each(function()
        {
            /*if($(this).val()== '' || parseInt($(this).val())==0)
            {
                inflag++;
            }*/
            if(($(this).val()!='') && (parseInt($(this).val()) > parseInt($('input[name^="hdnAmountPayable"]:eq('+index+')').val())))
            {
                $(this).addClass("required");
                flag=1;
            }
            else
            {
                if($(this).val() >0 )
                    ConcessionFeeData.push($('input[name^="hdnCandidateFeeItemId"]:eq('+index+')').val()+'@@@@@@'+$('input[name^="hdnAmountPayable"]:eq('+index+')').val()+'@@@@@@'+$(this).val());
            }
            index++;
        });
        if(flag==1)
        {
            notify('Concession amount cannot be more than existing amount', 'error', 10);
            return false;
        }
        else if(ConcessionFeeData.length == 0)
        {
            $('input[name^="txtConcessionAmount"]').addClass("required");
            notify('Please enter concession amount', 'error', 10);
            return false;
        }
        else
        {
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var ajaxurl = API_URL + 'index.php/FeeAssign/ApproveCandidateFeeConcession';
            var params = {candidateFeeId:candidateFeeId,feeConcessionData: ConcessionFeeData};
            var action='manage concession';
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: UserId,
                userRole:userRole
            };
            commonAjaxCall({
                method: 'POST',
                requestUrl: ajaxurl,
                params: params,
                headerParams: headerParams,
                action: action,
                onSuccess: function (response) {
                    alertify.dismissAll();
                    if (response == -1 || response['status'] == false)
                    {
                        notify(response.message, 'error', 10);
                    }
                    else
                    {
                        buildFeeAssignListDataTable();
                        notify(response.message,'success',10);
                    }
                    $('#FeeAssign').modal('hide');
                    $('body').removeClass('modal-open');

                }
            });
        }
    }
    $( "#userBranchList" ).change(function() {
        buildFeeAssignListDataTable();
        changeDefaultBranch();
    });
</script>