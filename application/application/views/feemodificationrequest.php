<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 17/2/16
 * Time: 12:31 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnUserRole" value="<?php echo $userData['Role']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Fee Modification Request</h3>
        <div class="content-header-btnwrap">
            <ul>

            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="feeassign-list">
                    <thead>
                    <tr>
                        <th>Candidate Name</th>
                        <th>Candidate Number</th>
                        <th>Branch Name</th>
                        <th class="text-right">Amount Payable</th>
                        <th class="text-right">Amount Paid</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="FeeAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageFeeAssignForm" method="post" onsubmit="showLeadInfo();return false;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Fee Assign</h4>
                            <input type="hidden" id="hdnFeeAssignId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                            <div class="col-sm-11 pl0">
                                <div class="input-field">
                                    <input id="txtLeadNumber" type="text" class="validate">
                                    <label for="txtLeadNumber">Mobile Number <em>*</em></label>

                                </div>
                            </div>
                                <div class="col-sm-1 p0 mt2 sky-blue">
                                    <!--<a href="javascript:;" class="btn blue-btn"  onclick="showLeadInfo(this)">GO</a>-->
                                    <input type="submit" value="GO" class="btn blue-btn">
                                </div>
                            </div>

                            <input type="hidden" id="hdnCourseId" value="">
                            <input type="hidden" id="hdnBranchId" value="">
                            <input type="hidden" id="hdnBranchxceflead" value="">
                            <input type="hidden" id="payableAmount" value="">
                            <input type="hidden" id="feeStructureId" value="">
                            <input type="hidden" id="typeOfFee" value="">
                            <input type="hidden" id="company" value="">
                            <input type="hidden" id="Institutional" value="">
                            <div id="LeadData">
                          <div class="col-sm-12 mtb10">

                                <div id="leadInfoData" >

                                  </div>
                            </div>
                                <div class="col-sm-12" id="typeOfClass" style="display:none">
                                    <div class="multiple-checkbox">
                          <span class="inline-checkbox">
                              <input type="checkbox" class="filled-in" id="clasRoom" />
                              <label for="clasRoom">Class Room Training</label>
                          </span>
                          <span class="inline-checkbox">
                              <input type="checkbox" class="filled-in" id="online" />
                              <label for="online">Online Training</label>


                          </span>
                                    </div>
                                </div>

                                <div class="col-sm-12" id="feetypes" style="display:none">
                                 <div class="input-field">
                                     <select id="ddlFeetypes"  name="ddlFeetypes">
                                         <option value="" selected>--Select--</option>
<!--                                         <option value="11">Retail</option>
                                         <option value="12">Corporate</option>
                                         <option value="13">Group</option>-->
                                     </select>
                                     <label class="select-label">Fee Type</label>

                                 </div>
                             </div>
                                <div class="col-sm-12" id="feeStructureType" style="display:none">
                                    <div class="input-field">
                                        <select id="ddlFeeStructuetypes"  name="ddlFeeStructuetypes">
                                            <option value="" selected>--select--</option>
                                            <option value="11">Retail</option>
                                            <option value="12">Corporate</option>
                                            <option value="13">Group</option>
                                        </select>
                                        <label class="select-label">Fee Type</label>

                                    </div>
                                </div>


                                <div class="col-sm-12 mt0 feeStructureCreate-companyname" id="Corporatetypes" style="display:none">
                                <div class="input-field">
                                    <select id="corporate"  name="corporate" class="validate custom-select-nomargin formSubmit">
                                        <option value="default" disabled selected>---Select---</option>
                                    </select>
                                    <label class="select-label">Corporate Company <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12 mt0 feeStructureCreate-groupname" id="groupTypes" style="display:none">
                                <div class="input-field">
                                    <select id="groupvalue"  name="groupvalue" class="validate formSubmit custom-select-nomargin">
                                        <option value="default" disabled selected>---Select---</option>
                                    </select>
                                    <label class="select-label">Institute Name <em>*</em></label>
                                </div>

                            </div>
                            <div id="tabs" class="mt10"></div>
                                <div id="DataMessage" class="p5 text-center" style="display:none;"></div>
                                </div>
                          </div>


                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveButton"  class="btn blue-btn" onclick="SaveCandidateDetails(this)"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelButton"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ModifyFee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ModifyFee" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Fee Modification</h4>
                            <input type="hidden" id="hdnCandidateFeeItemId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">

                                <div id="leadInfoSnapShot" >
                                </div>
                        </div>


                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveModifyFeeButton"  class="btn blue-btn" onclick="saveModifyFee(this)"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelModifyFeeButton"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildFeeAssignListDataTable();
        loadFeeStructure();
    });
    function loadFeeStructure()
    {  
        var userId = $('#hdnUserId').val();
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        $.when(
            commonAjaxCall
            (
                {
                    This: this,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                    params: {name: 'Company'},
                    action: action
                }
            ),commonAjaxCall
              (
        {
            This: this,
            headerParams: headerParams,
            requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
            params: {name: 'Institution'},
            action: action
        }
         ),
            commonAjaxCall
            (
                {
                    This: this,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                    params: {name: 'Fee Type'},
                    action: action
                }
            )
        ).then(function (response,response1,response2) {

            var corporate = response[0];
            var groupData = response1[0];
            var feeTypeData = response2[0];
            corporateDropDown(corporate);
            groupDropDown(groupData);
            $('#ddlFeetypes').find('option:gt(0)').remove();
            if (feeTypeData.data.length == 0 || feeTypeData == -1 || feeTypeData['status'] == false) {
                
            } else {
                var branchRawData = feeTypeData.data.reverse();
                for (var b = 0; b < branchRawData.length; b++) {
                    var o = $('<option/>', {value: branchRawData[b]['reference_type_value_id']})
                            .text(branchRawData[b]['value']);
                    o.appendTo('#ddlFeetypes');
                }
                $('#ddlFeetypes').material_select();
            }
        });
    }
    function getDropDownFeeTypeByTextFilter(filter, identifier){
        var userId = $('#hdnUserId').val();
        var action = 'add';
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Fee Type'};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This:this,method:'GET',requestUrl:ajaxurl,params:params,headerParams:headerParams,action:'add',onSuccess:function(response){
            $(identifier).find('option:gt(0)').remove();
            if (response.data.length == 0 || response == -1 || response['status'] == false) {
                
            } else {
                var filterArray = filter.split(',');
                var RawData = response.data.reverse();
                for (var b = 0; b < RawData.length; b++) {
                    for (var a = 0; a < filterArray.length; a++) {
                        if(filterArray[a] == RawData[b]['value']){
                            var o = $('<option/>', {value: RawData[b]['reference_type_value_id']})
                                    .text(RawData[b]['value']);
                            o.appendTo(identifier);
                        }
                    }
                }
                $(identifier).material_select();
            }
        }});
    }
    function buildFeeAssignListDataTable()
    {
            var ajaxurl=API_URL+'index.php/FeeAssign/getFeeAssignModificationsList';
            var userID=$('#hdnUserId').val();
            var branchID=$('#userBranchList').val();
            var params = {branchID: branchID};
            var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            $("#feeassign-list").dataTable().fnDestroy();
            $tableobj= $('#feeassign-list').DataTable( {
                "fnDrawCallback": function() {
                    $("#feeassign-list thead th").removeClass("icon-rupee");
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                    }
                    buildpopover();
                    verifyAccess();
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": true,
                "bProcessing": true,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No records found",
                    "sZeroRecords": "No records found"
                },
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    headers:headerParams,
                    data:params,
                    error:function(response) {
                        DTResponseerror(response);
                    }
                },
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false
                } ],
                columns: [
                    {
                        data: null, render: function ( data, type, row )
                        {
                            return data.lead_name;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            return data.lead_number;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            return data.branch_name;
                        }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                        {
                            var amount_payable=(data.amount_payable == null || data.amount_payable == '') ? "0.00":data.amount_payable;
                            return moneyFormat(amount_payable);
                        }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                        {
                            var amount_paid=(data.amount_paid == null || data.amount_paid == '') ? "0.00":data.amount_paid;
                            return moneyFormat(amount_paid);
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {

                            var CandidateData="";
                            CandidateData+=data.approved_status;
                            CandidateData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                            CandidateData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                            CandidateData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                            CandidateData+='</a>';
                            CandidateData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                            CandidateData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                            CandidateData+='<li><a  href="javascript:;" access-element="manage" onclick="ModificationRequest(\''+data.candidate_fee_id+'\',\'approve\')">Approve Request</a></li>';
                            CandidateData+='<li><a  href="javascript:;" access-element="manage" onclick="ModificationRequest(\''+data.candidate_fee_id+'\',\'reject\')">Reject Request</a></li>';
                            CandidateData+='</ul>';
                            CandidateData+='</div>';
                            return CandidateData;
                        }
                    }
                ]
            } );
            DTSearchOnKeyPressEnter();
    }
    function ModificationRequest(Id,status)
    {
        var ModelTitle='Fee Modification Request',Description='';
        if(status=='approve')
        {
            Description = 'Are you sure want to approve the fee modification request';
            $("#popup_confirm #btnTrue").attr('onclick','ApproveFeeModification(this,'+Id+')');
        }
        else
        {
            Description = 'Are you sure want to reject the fee modification request';
            $("#popup_confirm #btnTrue").attr('onclick','RejectFeeModification(this,'+Id+')');
        }
        customConfirmAlert(ModelTitle,Description);
    }
    function RejectFeeModification(This,candidateFeeId)
    {
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var userID = $('#hdnUserId').val();
        var action = 'manage';
        var ajaxurl=API_URL+'index.php/FeeAssign/RejectCandidateFeeModificationRequest';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var params = {candidateFeeId:candidateFeeId,requestType:'reject'};
        commonAjaxCall({This: This, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: RejectFeeModificationResponse});
    }
    function RejectFeeModificationResponse(response)
    {
        //console.log(response);
        alertify.dismissAll();
        if (response == -1 || response['status'] == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            buildFeeAssignListDataTable();
            loadFeeStructure();
            notify(response.message,'success',10);
            $('#ModifyFee').modal('hide');
            $('#popup_confirm').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }
    }
    function ApproveFeeModification(This,Id){
        $("#hdnCandidateFeeItemId").val(Id);
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var UserId = $('#hdnUserId').val();
        var candidate_fee_item_id=$('#hdnCandidateFeeItemId').val();
        var ajaxurl = API_URL + 'index.php/FeeAssign/getFeeAssignByCandidateItem';
        var type = "GET";
        var action = 'manage';
        var params={candidate_fee_item_id:candidate_fee_item_id};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl,params:params, headerParams: headerParams, action: action, onSuccess: function(response){

            ModifyFeeAssignResponse(response);

        }});
        $(This).attr("data-target", "#ModifyFee");
        $(This).attr("data-toggle", "modal");
    }
    function ModifyFeeAssignResponse(response){
        alertify.dismissAll();
        if(response.status===false){
            notify(response.message,'error',10);
        }
        else
        {
            rawData=response.data[0];
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }
            if(rawData.companyName==null || rawData.companyName==''){
                rawData.companyName='----';
            }
            if(rawData.institution==null || rawData.institution==''){
                rawData.institution='----';
            }
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }

            var content = '<div class="col-sm-12 ">';
            content += '<div class="light-blue3-bg clearfix pt10 pb10">';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Name</span>';
            content += '<p>' + rawData.name + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Email</span>';
            content += '<p>' + rawData.email + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Phone</span>';
            content += '<p>' + rawData.phone + '</p>';
            content += '</div>';
            content += ' </div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Branch</span>';
            content += '<p>' + rawData.branchName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Course</span>';
            content += '<p>' + rawData.courseName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Qualification</span>';
            content += '<p>' + rawData.qualification + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Company</span>';
            content += '<p>' + rawData.companyName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Institution</span>';
            content += '<p>' + rawData.institution + '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            var disableAmnt='';
            if(rawData.is_modification_allowed==0) {
                var disableAmnt=' readyonly disabled ';
            }
            if(rawData.is_concession_pending==1) {
                var disableAmnt=' readyonly disabled ';
            }
            if(rawData.is_modification_allowed==1 && rawData.is_concession_pending==0){
                disableAmnt='';
            }
            var Amounts = "";
            Amounts += '<div class="col-sm-12 mt15">'
                + '<table class="table table-responsive table-striped table-custom" id="feeassign-list">'
                + '<thead><tr>'
                + '<th>Fee Head</th><th>Due days</th><th>Amount</th><th>Modification</th></tr>'
                + '</thead><tbody>';

            $.each(response.data, function (key, value) {
                //$('#txtContactBranch').append($('<option></option>').val(value.id).html(value.name));
                Amounts += '<tr>'
                    + '<td>' + value.feeHeadName + '</td>'
                    + '<td>'+ value.due_days+'</td>'
                    + '<td>'+ value.amount_payable+'</td>'
                    + '<td>'
                    + '<input type="hidden" id="hdnActualAmountPayable_'+value.candidate_fee_item_id+'" name="hdnActualAmountPayable[]" value="' + (parseFloat(value.concession_amount)+parseFloat(value.right_of_amount)) + '" /> '
                    +'<div class="col-sm-12 pl0"><input style="width: 80%;" class="modal-table-textfiled m0" type="text" placeholder="Amount" name="txtNewAmountPayable" id="txtNewAmountPayable_'+value.candidate_fee_item_id+'" value="' + value.modified_amount + '" onkeypress="return isNumberKey(event)" '+disableAmnt+'  /></div></td></tr>';
            });

            Amounts += '</tbody></table></div><div id="DataMessagePayment" class="col-sm-12 mt15 text-center"></div>';
            $('#leadInfoSnapShot').html(content+Amounts);
            $("#DataMessagePayment").hide();
            $("#DataMessagePayment").html('');
            if(rawData.is_modification_allowed==0) {
                $("#DataMessagePayment").show();
                $("#DataMessagePayment").html("<b style='color:red;'>Already Fee Collected</b>");
                $('#saveModifyFeeButton').attr('disabled',true);
            }
            if(rawData.is_concession_pending==1) {
                $("#DataMessagePayment").show();
                $("#DataMessagePayment").html("<b style='color:red;'>Concession request is in pending.</b>");
                $('#saveModifyFeeButton').attr('disabled',true);
            }
            if(rawData.is_modification_allowed==1 && rawData.is_concession_pending==0){
                $('#saveModifyFeeButton').attr('disabled',false);
            }


        }

    }
    function saveModifyFee(This){
        var UserId = $('#hdnUserId').val();
        var userRole = $("#hdnUserRole").val();
        $("input[name='txtNewAmountPayable']").removeClass("required");
        $("input[name='txtNewAmountPayable']").next('span.required-msg').remove();
        $("input[name='txtNewAmountPayable']").removeClass("active");
        var flag=0;
        var feeModifications=[];
        var sub_flag=0;
        $.each($("input[name='txtNewAmountPayable']"),function(key, value){
            if($(this).val()=='' || $(this).val()==0) {
                $(this).addClass("required");
                $(this).after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag=1;

            }
            else{
                var res = $(this).attr('id').split("_");
                if($(this).val()<parseFloat($('#hdnActualAmountPayable_'+res[1]).val())){
                    $(this).addClass("required");
                    $(this).after('<span class="required-msg"> >=' + $('#hdnActualAmountPayable_'+res[1]).val() + '</span>');
                    sub_flag=1;

                }
                else{
                    feeModifications.push(res[1] + '-' + $(this).val());
                }

            }
            if(sub_flag==1){
                flag=1;
            }

        });
        var feeModifications=feeModifications.join('|');
        if(flag==0) {
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var candidate_fee_item_id = $('#hdnCandidateFeeItemId').val();
            var ajaxurl = API_URL + 'index.php/FeeAssign/modifyFeeAssign';
            var type = "POST";
            var action = 'manage';
            var params = {feeModifications:feeModifications,candidate_fee_id:candidate_fee_item_id,type:'approval'};
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: UserId,
                userrole:userRole
            };
            commonAjaxCall({
                This: This,
                method: type,
                requestUrl: ajaxurl,
                params: params,
                headerParams: headerParams,
                action: action,
                onSuccess: function (response) {

                    saveModifyFeeResponse(response);

                }
            });
        }
    }
    function saveModifyFeeResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            buildFeeAssignListDataTable();
            loadFeeStructure();
            notify(response.message,'success',10);
            $('#ModifyFee').modal('hide');
            $('#popup_confirm').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }
        else{
            notify(response.message,'error',10)
        }
    }

    $( "#userBranchList" ).change(function() {
        buildFeeAssignListDataTable();
        changeDefaultBranch();
    });
</script>