<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 14/4/16
 * Time: 11:19 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnUserRole" value="<?php echo $userData['Role']; ?>" readonly />

    <div class="content-header-wrap">
        <h3 class="content-header">Fee Request</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="filters"><a class="tooltipped" data-position="top" data-tooltip="Filters" data-toggle="modal" data-target="#filters" class=""><i class="icon-filter"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="feeassign-list">
                    <thead>
                        <tr>
                            <th>Candidate Name</th>
                            <th>Branch Name</th>
                            <th>Amount Payable</th>
                            <th>Request Type</th>
                            <th class="border-r-none">Status</th>
                            <th class="no-sort"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="FeeAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageFeeAssignForm" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Approve Request</h4>
                            <input type="hidden" id="hdnFeeAssignId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div id="leadInfoSnapShot" >

                            </div>
                        </div>
                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveFeeConcessionButton"  class="btn blue-btn"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelModifyFeeButton"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--Filter Modal-->
        <div class="modal fade" id="filters" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Filters</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="filter-wrap">
                            <div class="input-field mt0">
                                <input class="filled-in" id="all" type="checkbox" name="filtersContact" value="All">
                                <label class="f14" for="all">All</label>
                            </div>
                            <div class="input-field mt0">
                                <input class="filled-in" id="dnd_contact" type="checkbox" name="filtersContact" value="DND">
                                <label class="f14" for="dnd_contact">Pending</label>
                            </div>
                            <div class="input-field mt0">
                                <input class="filled-in" id="contact_OutStation" type="checkbox" name="filtersContact" value="OutStation">
                                <label class="f14" for="contact_OutStation">Approved</label>
                            </div>
                            <div class="input-field mt0">
                                <input class="filled-in" id="contact_student" type="checkbox" name="filtersContact" value="ContactStudent">
                                <label class="f14" for="contact_student">Rejected</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Apply</button>
                        <button type="button" class="btn blue-light-btn"><i class="icon-right mr8"></i>Clear</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
docReady(function(){
    buildFeeAssignListDataTable();
});

function buildFeeAssignListDataTable()
{
    var ajaxurl=API_URL+'index.php/FeeAssign/getFeeRequestList';
    var userID=$('#hdnUserId').val();
    var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
    $("#feeassign-list").dataTable().fnDestroy();
    $tableobj= $('#feeassign-list').DataTable( {
        "fnDrawCallback": function() {
            buildpopover();
            verifyAccess();
        },
        dom: "Bfrtip",
        bInfo: false,
        "serverSide": true,
        "bProcessing": true,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No records found",
            "sZeroRecords": "No records found",
            "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
        },
        ajax: {
            url:ajaxurl,
            type:'GET',
            headers:headerParams,
            error:function(response) {
                DTResponseerror(response);
            }
        },
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false
        } ],
        columns: [
            {
                data: null, render: function ( data, type, row )
                {
                    return data.lead_name;
                }
            },
            {
                data: null, render: function ( data, type, row )
                {
                    return data.branchName;
                }
            },
            {
                data: null, render: function ( data, type, row )
                {
                    var amount_payable=(data.amount_payable == null || data.amount_payable == '') ? "0.00":data.amount_payable;
                    return amount_payable;
                }
            },
            {
                data: null, render: function ( data, type, row )
                {
                    var amount_paid=(data.amount_paid == null || data.amount_paid == '') ? "0.00":data.amount_paid;
                    return amount_paid;
                }
            },
            {
                data:null, render: function (data, type, row )
                {
                    return data.approved_status;
                }
            },
            {
                data:null,render:function (data, type, row )
                {
                    var feeConcessionReq='';
                    feeConcessionReq+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                    feeConcessionReq+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    feeConcessionReq+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                    feeConcessionReq+='</a>';
                    feeConcessionReq+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    feeConcessionReq+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    feeConcessionReq+='<li><a access-element="Manage Concession" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick="ConcessionRequest('+data.candidate_fee_id+',\'approve\')">Approve</a></li>';
                    feeConcessionReq+='<li><a access-element="Manage Concession" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick="ConcessionRequest('+data.candidate_fee_id+',\'reject\')">Reject</a></li>';
                    feeConcessionReq+='</ul>';
                    feeConcessionReq+='</div>';
                    return feeConcessionReq;
                }
            }

        ]
    } );
    DTSearchOnKeyPressEnter();
}

    function ConcessionRequest(candidateFeeId,status)
    {
        var ModelTitle='Concession Request',Description='';
        if(status=='approve')
        {
            Description = 'Are you sure want to approve the concession request';
            $("#popup_confirm #btnTrue").attr('onclick','CandidateFeeConcession('+candidateFeeId+')');
        }
        else
        {
            Description = 'Are you sure want to reject the concession request';
            $("#popup_confirm #btnTrue").attr('onclick','RejectCandidateFeeConcession('+candidateFeeId+')');
        }
        customConfirmAlert(ModelTitle,Description);
    }
    function RejectCandidateFeeConcession(candidateFeeId)
    {
        var userID = $('#hdnUserId').val();
        var action = 'manage concession';
        var ajaxurl=API_URL+'index.php/FeeAssign/RejectCandidateFeeConcessionRequest';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var params = {candidateFeeId:candidateFeeId,requestType:'reject'};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: RejectCandidateFeeConcessionResponse});
    }
    function RejectCandidateFeeConcessionResponse(response)
    {
        if (response == -1 || response['status'] == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            buildFeeAssignListDataTable();
            notify(response.message,'success',10);
        }
    }
    function CandidateFeeConcession(candidateFeeId)
    {
        var UserId = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/FeeAssign/getFeeAssignInfoByCandidateFeeId';
        var type = "GET";
        var action = 'manage concession';
        var params={candidateFeeId:candidateFeeId};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl,params:params, headerParams: headerParams, action: action, onSuccess:CandidateFeeConcessionResponse });
    }
    function CandidateFeeConcessionResponse(response)
    {
        if(response.status===false){
            notify(response.message,'error',10);
        }
        else
        {
            rawData=response.data[0];
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }
            if(rawData.companyName==null || rawData.companyName==''){
                rawData.companyName='----';
            }
            if(rawData.institution==null || rawData.institution==''){
                rawData.institution='----';
            }
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }

            var content = '<div class="col-sm-12 ">';
            content += '<div class="light-blue3-bg clearfix pt10 pb10">';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Name</span>';
            content += rawData.name;
            content += '<input type="hidden" id="hdnCandidateFeeId" value="' + rawData.candidate_fee_id + '" />';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Email</span>';
            content += '<p>' + rawData.email + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Phone</span>';
            content += '<p>' + rawData.phone + '</p>';
            content += '</div>';
            content += ' </div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Branch</span>';
            content += '<p>' + rawData.branchName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Course</span>';
            content += '<p>' + rawData.courseName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Qualification</span>';
            content += '<p>' + rawData.qualification + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Company</span>';
            content += '<p>' + rawData.companyName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Institution</span>';
            content += '<p>' + rawData.institution + '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            var disableAmnt='';
            if(rawData.is_concession_allowed==0)
            {
                disableAmnt=' readyonly disabled ';
            }
            else
            {
                disableAmnt='';
            }
            var Amounts = "";
            Amounts += '<div class="col-sm-12 mt15">'
                + '<table class="table table-responsive table-striped table-custom" id="feeassign-list">'
                + '<thead><tr>'
                + '<th>Fee Head</th><th>Due days</th><th>Amount</th><th>Concession</th></tr>'
                + '</thead><tbody>';
            for(var i=0;i<response.data.length;i++)
            {
                Amounts += '<tr>';
                Amounts += '<td>' + response.data[i].feeHeadName + '</td>';
                Amounts += '<td>'+  response.data[i].due_days + '</td>';
                Amounts += '<td>' + response.data[i].amount_payable + '</td>';
                Amounts += '<td><div class="col-sm-8 pl0">';
                Amounts += '<input type="hidden" name="hdnCandidateFeeItemId[]" value="' + response.data[i].candidate_fee_item_id + '" />';
                Amounts += '<input type="hidden" name="hdnAmountPayable[]" value="' + response.data[i].amount_payable + '" />';
                Amounts += '<input class="modal-table-textfiled m0" type="text" name="txtConcessionAmount[]" value="' + response.data[i].modified_concession + '" onkeypress="return isNumberKey(event)" '+disableAmnt+' />';
                Amounts += '</div></td>';
                Amounts += '</tr>';
            }

            Amounts += '</tbody></table></div><div id="DataMessagePayment" class="col-sm-12 mt15"></div>';
            $('#leadInfoSnapShot').html(content+Amounts);
            $("#DataMessagePayment").hide();
            $("#DataMessagePayment").html('');
            if(rawData.is_concession_allowed==0)
            {
                $("#DataMessagePayment").show();
                $("#DataMessagePayment").html("<b style='color:red;text-align:center;'>Concession already given to this lead</b>");
                $('#saveFeeConcessionButton').attr('disabled',true);
            }
            else
            {
                $('#saveFeeConcessionButton').attr('disabled',false);
                $('#saveFeeConcessionButton').attr('onclick','ApproveCandidateFeeConcession('+rawData.candidate_fee_id+')');
            }
            $('#FeeAssign').modal('show');
        }
    }
    function ApproveCandidateFeeConcession(candidateFeeId)
    {
        var UserId = $('#hdnUserId').val();
        var userRole = $("#hdnUserRole").val();
        var flag=0;
        var index=0;
        var inflag=0;
        var ConcessionFeeData = [];
        //var candidateFeeId=$("#hdnCandidateFeeId").val();
        $('input[name^="txtConcessionAmount"]').removeClass("required");
        $('input[name^="txtConcessionAmount"]').each(function()
        {
            if($(this).val()== '' || parseInt($(this).val())==0)
            {
                inflag++;
            }
            if(parseInt($(this).val()) > parseInt($('input[name^="hdnAmountPayable"]:eq('+index+')').val()))
            {
                $(this).addClass("required");
                flag=1;
            }
            else
            {
                if($(this).val() >0 )
                    ConcessionFeeData.push($('input[name^="hdnCandidateFeeItemId"]:eq('+index+')').val()+'@@@@@@'+$('input[name^="hdnAmountPayable"]:eq('+index+')').val()+'@@@@@@'+$(this).val());
            }
            index++;
        });
        if(flag==1)
        {
            notify('Concession amount cannot be more than existing amount', 'error', 10);
            return false;
        }
        else if(inflag == 2)
        {
            $('input[name^="txtConcessionAmount"]').addClass("required");
            notify('Please enter concession amount', 'error', 10);
            return false;
        }
        else
        {
            var ajaxurl = API_URL + 'index.php/FeeAssign/ApproveCandidateFeeConcession';
            var params = {candidateFeeId:candidateFeeId,feeConcessionData: ConcessionFeeData};
            var action='manage concession';
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: UserId,
                userRole:userRole
            };
            commonAjaxCall({
                method: 'POST',
                requestUrl: ajaxurl,
                params: params,
                headerParams: headerParams,
                action: action,
                onSuccess: function (response) {
                    if (response == -1 || response['status'] == false)
                    {
                        notify(response.message, 'error', 10);
                    }
                    else
                    {
                        buildFeeAssignListDataTable();
                        notify(response.message,'success',10);
                    }
                    $('#FeeAssign').modal('hide');
                    $('body').removeClass('modal-open');

                }
            });
        }
    }
</script>