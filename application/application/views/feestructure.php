<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Fee Structure</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="tooltipped" data-position="left" data-tooltip="Add Fee Structure" href="<?php echo BASE_URL; ?>app/createfeestructure"><i class="icon-plus-circle"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" access-element="list"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="clearfix">
                <!--<h4 class="heading-uppercase text-center">Upcoming Fee Structure</h4>-->
                <div class="col-sm-12 p0" id="feeStructure-list-future-wrapper">
                    <table class="table table-responsive table-striped table-custom table-info" id="feeStructure-list-future">
                        <thead>
                        <tr>
                            <th>Course</th>
                            <th>Name</th>
                            <th>Effective From</th>
                            <th>Training Type</th>
                            <th>Fee Type</th>
                            <th>Company/Group</th>
                            <th>Regular Fee</th>
                            <th class="text-right">Amount</th>
                            <th>Branch</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="clearfix mt20">
                <!--<h4 class="heading-uppercase text-center ">Current Fee Structure</h4>-->
                <div class="col-sm-12 p0" id="feeStructure-list-wrapper">
                    <table class="table table-responsive table-striped table-custom table-info" id="feeStructure-list">
                        <thead>
                        <tr>
                            <th>Course</th>
                            <th>Name</th>
                            <th>Effective From</th>
                            <th>Training Type</th>
                            <th>Fee Type</th>
                            <th>Company/Group</th>
                            <th>Regular Fee</th>
                            <th class="text-right">Amount</th>
                            <th>Branch</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
    <!--Modal-->
    <div class="modal fade" id="feeStructureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="950">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                    <h4 class="modal-title" id="myModalLabel">Fee Structure History</h4>
                </div>
                <div class="modal-body modal-scroll clearfix" style="height: 508px; overflow-y: auto;">
                <!-- -->
                <table class="table table-responsive table-striped table-custom table-future">
                    <thead>
                    <tr>
                        <th>Course</th>
                        <th>Name</th>
                        <th>Effective From</th>
                        <th>Training Type</th>
                        <th>Fee Type</th>
                        <th>Company/Group</th>
                        <th>Regular Fee</th>
                        <th class="text-right">Amount</th>
                        <th>Branch</th>
                        <th>Created Date</th>
                        <th>End Date</th>
                    </tr>
                    <tr><td class="border-none p0 lh10">&nbsp;</td></tr>
                    </thead>
                    <tbody id="historyDetails">
                    </tbody>
                </table>
                <!-- -->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$context = 'Fee Structure';
$pageUrl = 'feestructure';
$serviceurl = 'feestructure';
docReady(function () {
    buildFeeStructuresDataTable();
});

function buildFeeStructuresDataTable()
{
    var ajaxurl = API_URL + 'index.php/FeeStructure/getFeeStructureList/?time=future';
    var userID = $('#userId').val();
    var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    $("#feeStructure-list-future").dataTable().fnDestroy();
    var futureObject = $('#feeStructure-list-future').DataTable({
        "fnDrawCallback": function () {
            $("#feeStructure-list-future thead th").removeClass("icon-rupee");
            var $api = this.api();
            var pages = $api.page.info().pages;
            if(pages > 1)
            {
                $('#feeStructure-list-future-wrapper .dataTables_paginate').css("display", "block");
                $('#feeStructure-list-future-wrapper .dataTables_length').css("display", "block");
                //$('#feeStructure-list-future-wrapper .dataTables_filter').css("display", "block");
            } else {
                $('#feeStructure-list-future-wrapper .dataTables_paginate').css("display", "none");
                $('#feeStructure-list-future-wrapper .dataTables_length').css("display", "none");
                //$('#feeStructure-list-future-wrapper .dataTables_filter').css("display", "none");
            }
            buildpopover();
            verifyAccess();
        },
        dom: "<'DTHeader'>Bfrtip",
        bInfo: false,
        "serverSide": true,
        "bProcessing": true,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No records found",
            "sZeroRecords": "No records found",
            "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
        },
        ajax: {
            url: ajaxurl,
            type: 'GET',
            /*data: params,*/
            headers: headerParams,
            error: function (response) {
                DTResponseerror(response);
            }
        },
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false
        }],
        columns: [
            {
                data: null, render: function (data, type, row)
                {
                    var online = data.is_allowed_for_online_training == 1 ? '' : 'ash';
                    var onlineText = data.is_allowed_for_online_training == 1 ? '' : '';
                    var unversity = data.is_allowed_for_class_room_training == 1 ? '' : 'ash';
                    var unversityText = data.is_allowed_for_class_room_training == 1 ?  '' : '';
                    return '<a href="javascript:;" class="tooltipped" data-position="top" data-tooltip="'+unversityText+'Class Room Training"><i class="fa fa-university mr5 ' + unversity + '"></i></a><a href="javascript:;" class="tooltipped" data-position="top" data-tooltip="'+onlineText+'Online Training" ><i class="fa fa-globe mr5 f13 ' + online + '"></i></a>'+data.courseName;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.name;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.effective_from;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.trainingType;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.type_name;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.inst_name.length > 0 ? data.inst_name : ' -- ';
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.is_regular_fee.length > 0 ? data.is_regular_fee : ' -- ';
                }
            },
            {
                data: null,className:"text-right icon-rupee", render: function (data, type, row)
                {
                    return moneyFormat(data.tot);
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    var htmlData = '';
                    var b_status = data.is_active == 1 ? 'Inactive' : 'Active';
                    b_status = 'Fee ' + b_status;
                    var online = data.is_allowed_for_online_training == 1 ? '' : 'ash';
                    var unversity = data.is_allowed_for_class_room_training == 1 ? '' : 'ash';
                    htmlData += data.branch.length > 0 ? data.branch : ' All '
                    htmlData += '<div class="dropdown feehead-list custom-dropdown-style pull-right">';
                    htmlData += '<a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    htmlData += '<i class="fa fa-ellipsis-v f18 pr10 pl10"></i>';
                    htmlData += '</a>';
                    htmlData += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    htmlData += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    htmlData += '<li access-element="edit"><a href="' + APP_REDIRECT + 'createfeestructure/' + data.fee_structure_id + '">Edit</a></li>';
                    htmlData += '<li access-element="edit"><a href="' + APP_REDIRECT + 'copyfeestructure/' + data.fee_structure_id + '">Copy</a></li>';
                    htmlData += '<li access-element="list"><a onclick="getHistory('+ data.fee_structure_id +')" href="javascript:;">History</a></li>';
                    /*htmlData += '<li access-element="list"><a data-position="top" data-tooltip="Show History" data-toggle="modal" data-target="#feeStructureModal" class="tooltipped viewsidePanel">History</a></li>';*/
                    htmlData += '<li access-element="delete"><a onclick="changeFeeStructureStatus(this,' + data.fee_structure_id + ' , ' + data.is_active + ')" href="#">' + b_status + '</a></li>';
                    htmlData += '</ul>';
                    htmlData += '</div>';
                    return htmlData;
                    /*feeStructureModal*/
                }
            }
        ],
        "createdRow": function (row, data, index)
        {
            if (data.is_active == 0)
            {
                $(row).addClass('disabled-branchview');
            }
        }
    });
    MultipleDTSearchEnter(futureObject,"#feeStructure-list-future");

    var ajaxurl = API_URL + 'index.php/FeeStructure/getFeeStructureList/?time=current';
    var userID = $('#userId').val();
    var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    $("#feeStructure-list").dataTable().fnDestroy();
    var currentObject = $('#feeStructure-list').DataTable({
        "fnDrawCallback": function () {
            $("#feeStructure-list thead th").removeClass("icon-rupee");
            var $api = this.api();
            var pages = $api.page.info().pages;
            if(pages > 1)
            {
                $('#feeStructure-list-wrapper .dataTables_paginate').css("display", "block");
                $('#feeStructure-list-wrapper .dataTables_length').css("display", "block");
                //$('#feeStructure-list-wrapper .dataTables_filter').css("display", "block");
            } else {
                $('#feeStructure-list-wrapper .dataTables_paginate').css("display", "none");
                $('#feeStructure-list-wrapper .dataTables_length').css("display", "none");
                //$('#feeStructure-list-wrapper .dataTables_filter').css("display", "none");
            }
            buildpopover();
            verifyAccess();
        },
        dom: "<'DTHeader'>Bfrtip",
        bInfo: false,
        "serverSide": true,
        "bProcessing": true,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No records found",
            "sZeroRecords": "No records found",
            "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
        },
        ajax: {
            url: ajaxurl,
            type: 'GET',
            headers: headerParams,
            error: function (response) {
                DTResponseerror(response);
            }
        },
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false
        }],
        columns: [
            {
                data: null, render: function (data, type, row)
                {
                    var online = data.is_allowed_for_online_training == 1 ? '' : 'ash';
                    var onlineText = data.is_allowed_for_online_training == 1 ? '' : '';
                    var unversity = data.is_allowed_for_class_room_training == 1 ? '' : 'ash';
                    var unversityText = data.is_allowed_for_class_room_training == 1 ?  '' : '';
                    return '<a href="javascript:;" class="tooltipped" data-position="top" data-tooltip="'+unversityText+'Class Room Training"><i class="fa fa-university mr5 ' + unversity + '"></i></a><a href="javascript:;" class="tooltipped" data-position="top" data-tooltip="'+onlineText+'Online Training" ><i class="fa fa-globe mr5 f13 ' + online + '"></i></a>'+data.courseName;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.name;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.effective_from;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.trainingType;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.type_name;
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.inst_name.length > 0 ? data.inst_name : ' -- ';
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    return data.is_regular_fee.length > 0 ? data.is_regular_fee : ' -- ';
                }
            },
            {
                data: null,className:"text-right icon-rupee", render: function (data, type, row)
                {
                    return moneyFormat(data.tot);
                }
            },
            {
                data: null, render: function (data, type, row)
                {
                    var htmlData = '';
                    var b_status = data.is_active == 1 ? 'Inactive' : 'Active';
                    b_status = 'Fee ' + b_status;
                    var online = data.is_allowed_for_online_training == 1 ? '' : 'ash';
                    var unversity = data.is_allowed_for_class_room_training == 1 ? '' : 'ash';
                    htmlData += data.branch.length > 0 ? data.branch : ' All '
                    htmlData += '<div class="dropdown feehead-list custom-dropdown-style pull-right">';
                    htmlData += '<a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    htmlData += '<i class="fa fa-ellipsis-v f18 pr10 pl10"></i>';
                    htmlData += '</a>';
                    htmlData += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    htmlData += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    htmlData += '<li access-element="edit"><a href="' + APP_REDIRECT + 'createfeestructure/' + data.fee_structure_id + '">Edit</a></li>';
                    htmlData += '<li access-element="edit"><a href="' + APP_REDIRECT + 'copyfeestructure/' + data.fee_structure_id + '">Copy</a></li>';
                    htmlData += '<li access-element="list"><a onclick="getHistory('+ data.fee_structure_id +')" href="javascript:;">History</a></li>';
                    htmlData += '<li access-element="delete"><a onclick="changeFeeStructureStatus(this,' + data.fee_structure_id + ' , ' + data.is_active + ')" href="#">' + b_status + '</a></li>';
                    htmlData += '</ul>';
                    htmlData += '</div>';
                    return htmlData;
                }
            }
        ],
        "createdRow": function (row, data, index)
        {
            if (data.is_active == 0)
            {
                $(row).addClass('disabled-branchview');
            }
        }
    });
    MultipleDTSearchEnter(currentObject,"#feeStructure-list");

    $('#feeStructure-list_wrapper div.DTHeader').html('Current Fee Structure');
    $('#feeStructure-list-future_wrapper div.DTHeader').html('Upcoming Fee Structure');

}
</script>