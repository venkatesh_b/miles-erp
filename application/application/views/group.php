<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 23/6/16
 * Time: 10:48 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Groups</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="" href="<?php echo BASE_URL; ?>app/manage_group"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div><!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="group-list">
                    <thead>
                        <tr>
                            <th>Group Name</th>
                            <th>Description</th>
                            <th class="border-r-none">#Contacts</th>
                            <th class="no-sort" style="width: 8%;">&nbsp;</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildCampaignGroupDataTable();
    });
    function buildCampaignGroupDataTable()
    {
        var ajaxurl=API_URL+'index.php/Campaign/GroupList';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#group-list").dataTable().fnDestroy();
        $tableobj=$('#group-list').DataTable( {
            "fnDrawCallback": function()
            {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                }
                else
                {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No groups found",
                "sZeroRecords": "No groups found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax:
            {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response)
                {
                    DTResponseerror(response);
                }
            },
            "columnDefs":
                [
                    {
                        "targets": 'no-sort',
                        "orderable": false
                    }
                ],
            columns:
                [
                    {
                        data: null, render: function ( data, type, row )
                        {
                            return data.groupName;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            return (data.description == null || data.description == '') ? "----":data.description;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            /*var roleUsersData='';
                            if(data.totalCount == 0)
                            {
                                roleUsersData+='<span>';
                                roleUsersData+='<a href="javascript:;" class="anchor-blue p10">'+data.totalCount+'</a>';
                                roleUsersData+='</span>';
                            }
                            else
                            {
                                roleUsersData+='<span class="popper p10" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Users">';
                                roleUsersData+='<a href="javascript:;" class="anchor-blue p10">'+data.totalCount+'</a>';
                                roleUsersData+='</span>';
                                roleUsersData+='<div class="popper-content hide">';
                                roleUsersData+='<p>'+data.Users+'</p>';
                                roleUsersData+='</div>';
                            }
                            roleUsersData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                            roleUsersData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                            roleUsersData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                            roleUsersData+='</a>';
                            roleUsersData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                            roleUsersData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                            roleUsersData+='<li><a access-element="manage" href="'+BASE_URL+'app/manage_role/'+data.reference_type_value_id+'">Manage Roles</a></li>';
                            roleUsersData+='<li><a access-element="edit" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick=\'ManageRole(this,"'+data.reference_type_value_id+'")\'>Edit</a></li>';
                            roleUsersData+='</ul>';
                            roleUsersData+='</div>';
                            return roleUsersData;*/
                            return data.contacts;
                        }
                    }
                    ,
                    {
                        data: null, render: function ( data, type, row )
                        {
                            var roleUsersData='';
                            roleUsersData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                            roleUsersData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                            roleUsersData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                            roleUsersData+='</a>';
                            roleUsersData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                            roleUsersData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                            roleUsersData+='<li><a access-element="edit" href="'+BASE_URL+'app/manage_group/'+data.group_id+'">Edit</a></li>';
                            //roleUsersData+='<li><a access-element="edit" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick=\'ManageRole(this,"'+data.reference_type_value_id+'")\'>Delete</a></li>';
                            roleUsersData+='</ul>';
                            roleUsersData+='</div>';
                            return roleUsersData;
                        }
                    }
                ]
        });
        DTSearchOnKeyPressEnter();
    }
</script>