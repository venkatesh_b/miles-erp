<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 29/7/16
 * Time: 6:06 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Lead Conversion</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="manage"><a class="tooltipped" data-position="top" data-tooltip="Convert Leads" href="#" onclick="convertToOldLeads(this)"><i class="icon-plus-circle"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="wrap-left col-sm-12 pt10">
                <div access-element="list" class="col-sm-12 p0" id="Students-list-wrapper">
                    <table class="table table-responsive table-striped table-custom" id="leadconversion-list">
                        <thead >
                            <tr class="">
                                <th>Course</th>
                                <th>Created By</th>
                                <th>Date</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<div class="modal fade" id="leadStageChange" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="ManageLeadStageForm">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                    <h4 class="modal-title" id="myModalLabel">Convert Lead Stages</h4>
                </div>
                <div class="modal-body modal-scroll clearfix">
                    <input type="hidden" id="hdnCourseId" value="0"/>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <select id="ddlCourses" name="ddlCourses">
                                <option value=""  selected>--Select--</option>
                            </select>
                            <label class="select-label">Courses <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <textarea class="materialize-textarea" id="txaLeadConvertDescription"></textarea>
                            <label for="txaLeadConvertDescription">Comments <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <span class="inline-checkbox">
                            <input type="checkbox" class="filled-in" id="chkTerms" checked />
                            <label for="chkTerms">I agree terms</label>
                        </span>
                    </div>
                    <div class="col-sm-12 mt15" id="leadsCountStage">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn blue-btn" onclick="saveToOldLeadStages()" id="btnSaveLeads"><i class="icon-right mr8"></i>Submit</button>
                    <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="leadStagesList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="400">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="ManageLeadStageForm">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                    <h4 class="modal-title" id="myModalLabel">Converted Leads</h4>
                </div>
                <div class="modal-body modal-scroll clearfix">
                    <div class="col-sm-12">
                        <p class="pl5 pt0 m0"> <lable class="clabel">Course : </lable><span id="LeadConversionCourseName"></span></p>
                    </div>
                    <div class="col-sm-12">
                        <p class="pl5 pt0 m0"> <lable class="clabel">Description : </lable><span id="LeadConversionDescription"></span></p>
                    </div>
                    <div class="col-sm-12 mt15" id="leadsCountAllStages">

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>

    $context = 'Lead Conversion';
    $pageUrl = 'lead_conversion';
    $serviceurl = 'lead_conversion';

    docReady(function(){
        //renderCoursesData();
        buildLeadConversionsDataTable();
    });

    function buildLeadConversionsDataTable()
    {
        var userID=$("#hdnUserId").val();
        var branchId=$("#userBranchList").val();
        var ajaxurl=API_URL+'index.php/LeadStageChange/getConvertedLeads';
        var params = {branchId:branchId};
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#leadconversion-list").dataTable().fnDestroy();
        $tableobj = $('#leadconversion-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                }
                else
                {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                    {
                        return '<a onclick="buildLeadAllConversions(this,'+data.lead_stage_management_id+')">'+data.course_name+'</a>';
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.created_by;
                    }
                },
                {
                     data: null, render: function ( data, type, row )
                     {
                         return data.created_date;
                     }
                 },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.comments;
                    }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function convertToOldLeads(This)
    {
        $(This).attr("data-target", "#leadStageChange");
        $(This).attr("data-toggle", "modal");
        $("#leadsCountStage").html('');
        changeCoursepopover();
    }

    function changeCoursepopover()
    {
        alertify.dismissAll();
        notify('Processing..', 'warning', 10);
        //$('#popup_data').modal('show');
        var userId = $('#hdnUserId').val();
        var ajaxurl=API_URL + 'index.php/Courses/getAllCourseList';
        var action = "list";
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: getCourseResponse});
    }
    function getCourseResponse(response)
    {
        alertify.dismissAll();
        $('#ddlCourses').empty();
        $('#ddlCourses').append($('<option></option>').val('').html('--Select--'));
        if (response.data.length > 0)
        {
            var LeadCourses = response.data;
            for (var b = 0; b < LeadCourses.length; b++) {
                var o = $('<option/>', {value: LeadCourses[b]['courseId'], 'parent-id': LeadCourses[b]['courseId']})
                    .text(LeadCourses[b]['name']);
                o.appendTo('#ddlCourses');
            }
        }
        $('#ddlCourses').val($("#hdnCourseId").val());
        $('#ddlCourses').material_select();
    }
    function saveToOldLeadStages()
    {
        alertify.dismissAll();
        var branchId=$("#userBranchList").val();
        var courseId=$("#ddlCourses").val();
        var comments=$("#txaLeadConvertDescription").val();

        $('#ManageLeadStageForm span.required-msg').remove();
        $('#ManageLeadStageForm input,#ManageLeadStageForm textarea').removeClass("required");
        $('#ManageLeadStageForm select').removeClass("required");
        var flag=0;
        if(courseId == '' || courseId == null)
        {
            $("#ddlCourses").parent().find('.select-dropdown').addClass("required");
            $("#ddlCourses").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if(comments == '' || comments == null)
        {
            $("#txaLeadConvertDescription").addClass("required");
            $("#txaLeadConvertDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if(flag == 1)
        {
            return false;
        }
        else
        {
            if(!$('#chkTerms').is(':checked'))
            {
                notify('Please check the terms & conditions', 'error', 10);
            }
            else
            {
                notify('Processing..', 'warning', 10);
                var userId = $('#hdnUserId').val();
                var ajaxurl=API_URL + 'index.php/LeadStageChange/ChangeLeadStage';
                var action = "manage";
                var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
                var params = {branchId:branchId,courseId:courseId,comments:comments};
                commonAjaxCall({This: this,  method: 'POST',params: params, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: getLeadChangeResponse});

            }
        }
    }
    function getLeadChangeResponse(response)
    {
        alertify.dismissAll();
        if(response.status == true)
        {
            notify("Lead stages updated successfully","success",10);
            buildLeadConversionsDataTable();
            $('#leadStageChange').modal('hide');
        }
        else
        {
            notify(response.message,"error",10);
        }
    }
    $('#userBranchList').change(function()
    {
        buildLeadConversionsDataTable();
    });

    $("#ddlCourses").change(function()
    {
        buildLeadConversions($(this).val());
    });

    function buildLeadConversions(courseId)
    {
        var branchId=$("#userBranchList").val();
        var userId = $('#hdnUserId').val();
        var ajaxurl=API_URL + 'index.php/LeadStageChange/LeadsCountByStage';
        var action = "manage";
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var params = {branchId:branchId,courseId:courseId};
        commonAjaxCall({This: this,  method: 'POST',params: params, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess:getLeadCountByStageResponse});
    }

    function getLeadCountByStageResponse(response)
    {
        if(response.status == false)
        {
            notify(response.message,'error',10);
        }
        else
        {
            var leads_stage_data = response.data;
            if(leads_stage_data.length == 0)
            {
                notify('No records found','error',10);
            }
            else
            {
                var leadstage_content = '<table class="table table-responsive table-striped table-custom">';
                leadstage_content += '<thead><tr>';
                leadstage_content += '<th>Lead Stage</th><th>Leads Count</th></tr>';
                leadstage_content += '</thead><tbody>';
                for(var ls=0;ls<leads_stage_data.length;ls++)
                {
                    leadstage_content += '<tr>';
                    leadstage_content += '<td>'+leads_stage_data[ls]['lead_stage']+'</td>';
                    leadstage_content += '<td>'+leads_stage_data[ls]['leads_count']+'</td>';
                    leadstage_content += '</tr>';
                }
                leadstage_content += '</tbody>';
                leadstage_content += '</table>';
                $("#leadsCountStage").html(leadstage_content);
            }
        }
    }
    function buildLeadAllConversions(This,leadstageId)
    {
        $(This).attr("data-target", "#leadStagesList");
        $(This).attr("data-toggle", "modal");
        $("#leadsCountAllStages").html('');
        var branchId=$("#userBranchList").val();
        var userId = $('#hdnUserId').val();
        var ajaxurl=API_URL + 'index.php/LeadStageChange/LeadsCountByAllStages';
        var action = "manage";
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var params = {leadstageId:leadstageId};
        commonAjaxCall({This: this,  method: 'POST',params: params, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess:getLeadCountByALLStagesResponse});
    }

    function getLeadCountByALLStagesResponse(response)
    {
        if(response.status == false)
        {
            notify(response.message,'error',10);
        }
        else
        {
            var leads_stage_data = response.data['leadstage_count'];
            var lead_conversion_data = response.data['conversion_data'];
            $("#LeadConversionCourseName").html(lead_conversion_data[0].courseName);
            $("#LeadConversionDescription").html(lead_conversion_data[0].comments);
            if(leads_stage_data.length == 0)
            {
                notify('No records found','error',10);
            }
            else
            {
                var leadstage_content = '';
                leadstage_content += '<table class="table table-responsive table-striped table-custom border">';
                leadstage_content += '<thead>';
                leadstage_content += '<tr>';
                leadstage_content += '<th colspan="3" class="text-center">Leads Converted</th>';
                leadstage_content += '</tr>';
                leadstage_content += '<tr>';
                leadstage_content += '<th>From Stage</th><th>To Stage</th><th>Count</th>';
                leadstage_content += '</tr>';
                leadstage_content += '</thead><tbody>';
                for(var ls=0;ls<leads_stage_data.length;ls++)
                {
                    leadstage_content += '<tr>';
                    leadstage_content += '<td>'+leads_stage_data[ls].from_lead_stage+'</td>';
                    leadstage_content += '<td>'+leads_stage_data[ls].to_lead_stage+'</td>';
                    leadstage_content += '<td>'+leads_stage_data[ls].leads_count+'</td>';
                    leadstage_content += '</tr>';
                }
                leadstage_content += '</tbody>';
                leadstage_content += '</table>';
                $("#leadsCountAllStages").html(leadstage_content);
            }
        }
    }
</script>