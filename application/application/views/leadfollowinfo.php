<?php
/**
 * Created by PhpStorm.
 * User: satyanarayanaraju
 * Date: 18/03/16
 * Time: 12:46 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="leadId" value="<?php echo $Id ?>" />
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnLeadId" value="">
    <input type="hidden" id="hdnBranchXrefId" value="">
    <div class="content-header-wrap">
        <h3 class="content-header">Lead Follow Up Info</h3>

        <div class="content-header-btnwrap">
            <ul>
                <li><a class="" href="<?php echo APP_REDIRECT;?>leadfollowup"><i class="icon-times"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" access-element="list"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="wrap-left col-sm-8 p0">
                <div class="wrap-head">
                    <table class="table table-responsive table-customised mb10">
                        <tr>
                            <td class="w140 text-center">
                                <div class="img-wrapper img-wrapper-leadinfo light-blue3-bg"> <img src="<?php echo BASE_URL; ?>assets/images/male-icon.jpg"> <span class="img-text" id="sequence_number"></span> </div>
                            </td>
                            <td class="p0 border-none" id="lead-info"></td>

                        </tr>

                    </table>
                </div>
                <div class="col-sm-12 p0 f13">
                    <div class="col-sm-3 pr0" id="createdDate"></div>
                    <div class="col-sm-3 p0" id="createdBv"></div>
                    <div class="col-sm-6 p0" id="secondCounsellerComments" style=""></div>
                </div>
                <div class="wrap-body">
                    <div class="tabs-section">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#timeline" aria-controls="timeline" role="tab"
                                   data-toggle="tab">Timeline</a>
                            </li>
                            <li role="presentation">
                                <a href="#personal_info" aria-controls="personal_info" role="tab" data-toggle="tab">Personal
                                    Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#education_info" aria-controls="education_info" role="tab"
                                   data-toggle="tab">Education Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#other_info" aria-controls="other_info" role="tab" data-toggle="tab">Other
                                    Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#eligibility_info" aria-controls="eligibility_info" role="tab" data-toggle="tab">Eligibility</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="timeline">
                                <div class="tab-data">
                                    <div class="col-sm-12 mtb10 clearfix p0">
                                        <!--<div class="pull-left search-filter custom-select input-field">
                                            <select>
                                                <option>All type of messages</option>
                                                <option>Email</option>
                                                <option>Call</option>
                                            </select>
                                        </div>-->
                                        <div class="pull-right mt10">
                                            <a onclick="ManageTimeLine(this)" class="btn blue-btn" href="javascript:;" data-target="#addNotification" data-toggle="modal" ><i class="icon-plus-circle mr8"></i>Add Notes or Interaction</a>
                                        </div>

                                    </div>
                                    <div class="table-list">
                                        <table class="table table-responsive table-custom">
                                            <colgroup>
                                                <col width="20%">
                                                <col width="30%">
                                                <col width="40%">
                                                <col width="10%">
                                            </colgroup>
                                            <tbody id="timelines-list">

                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="personal_info">
                                <div class="col-sm-12 white-bg relative">
                                    <!--<a  class="lead-pinfo-edit-btn" onclick="ManageLeadsPersonalInformation(0)" data-target="#editPersonalinfo" data-toggle="modal"><i class="icon-edit f18"></i></a>-->
                                    <a onclick="ManagePersonalInfo(this)" class="lead-pinfo-edit-btn" data-target="#editPersonalinfo" data-toggle="modal"><i class="icon-edit f18"></i></a>
                                    <div class="col-sm-4 mtb10">

                                        <p class=" label-text">Name</p>
                                        <label class="label-data" id="leadName"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">

                                        <p class=" label-text">Gender</p>
                                        <label class="label-data" id="gender"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Course</p>
                                        <label class="label-data" id="leadCourse"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Mobile</p>
                                        <label class="label-data" id="leadmobile"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Alternative Mobile</p>
                                        <label class="label-data" id="leadAltNumber"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Company</p>
                                        <label class="label-data" id="leadCompany"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Email</p>
                                        <label class="label-data" id="leadEmail"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Designation</p>
                                        <label class="label-data" id="leadDesignation"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Education</p>
                                        <label class="label-data" id="leadEducation"></label>
                                    </div>
                                </div>


                            </div>
                            <div role="tabpanel" class="tab-pane" id="education_info">

                                <div class="col-sm-12 white-bg" id="LatestEducationalInfo-list">
                                </div>
                                <div class="col-sm-12 white-bg" id="AllEducationalInfo-list">

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="other_info">
                                <div class="col-sm-12 white-bg ptb10">
                                    <ul class="other-info-check-list">
                                        <li>
                                        <span class="inline-checkbox">
                                          <input type="checkbox" class="filled-in" id="chkSignificanceOfCPA"/>
                                          <label for="chkSignificanceOfCPA">Significance of CPA course in your career progressive?</label>
                                        </span>
                                        </li>
                                        <li>
                                        <span class="inline-checkbox">
                                          <input type="checkbox" class="filled-in" id="chkPursueTheCPA"/>
                                          <label for="chkPursueTheCPA">Willingness to pursue the CPA designation?</label>
                                        </span>
                                        </li>
                                        <li>
                                        <span class="inline-checkbox">
                                          <input type="checkbox" class="filled-in" id="chkMilesCPA"/>
                                          <label for="chkMilesCPA">Response on Miles CPA Review course offerings?</label>
                                        </span>
                                        </li>
                                        <li>
                                        <span class="inline-checkbox">
                                          <input type="checkbox" class="filled-in" id="chkValueAddition"  />
                                          <label for="chkValueAddition">Value-addition from today's presentation/discussion?</label>
                                        </span>
                                        </li>
                                        <li>
                                        <span class="inline-checkbox">
                                          <input type="checkbox" class="filled-in" id="chkCmaNo"/>
                                          <label for="chkCmaNo">CMA No.</label>
                                        </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-12 white-bg p0">

                                    <div class="col-sm-12 pb10">
                                        <button onclick="SaveOtherInfo(this)" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Save</button>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="eligibility_info">
                                <div class="col-sm-12 white-bg ptb10">
                                    <form id="eligibilityForm" enctype="multipart/form-data">
                                        <div class="col-sm-12 p0">
                                            <div class="col-sm-5 white-bg p0">
                                                <div class="input-field">
                                                    <select id="eligibility" name="eligibility">
                                                        <option value=""  selected>--Select--</option>
                                                        <option value="Clear">Clear</option>
                                                        <option value="PGDA">PGDA</option>
                                                        <option value="Eligibility Check sent">Eligibility Check sent</option>
                                                    </select>
                                                    <label class="select-label">Eligibility <em>*</em></label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- list -->
                                        <div class="col-sm-12 p0" >
                                            <h5>Downloaded Doccuments</h5>
                                            <table id="eligibility_list" class="table table-responsive table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>FIle Name</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- list end -->
                                        <div class="col-sm-12 p0">
                                            <div class="file-field input-field">
                                                <div class="btn file-upload-btn">
                                                    <span>Document Upload </span>
                                                    <input type="file" id="eligibilityAttachemnt">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input  class="file-path validate" id="" placeholder="Upload single file" type="text">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 white-bg p0">
                                            <div class="col-sm-12 pb10">
                                                <button onclick="SaveEligibility(this)" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>


            </div>
            <div class="sidebar-widget col-sm-4 pr0">
                <div class="sidebar-head light-dark-blue">
                    <h4 class="heading-uppercase m0">Team</h4>
                </div>

                <div class="sidebar-contenet  white-bg clearfix" id="History-list">


                </div>

            </div>


        </div>
        <!--Add Notification Modal-->
        <div class="modal fade" id="addNotification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="">Add Notifications or Interaction</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="form-wrapper clearfix">
                            <form id="manageTimeline" enctype="multipart/form-data">
                                <div class="icons-list">
                                    <ul>
                                        <li id="message" onclick="LeadTimelineTypes(this)" class="">
                                            <a href="javascript:;">
                                                <i class="icon-mail"></i>
                                                <span  class="f12 display-block">Message</span>
                                            </a>
                                        </li>
                                        <li id="call" onclick="LeadTimelineTypes(this)">
                                            <a href="javascript:;">
                                                <i class="icon-phone"></i>
                                                <span  class="f12 display-block">Call</span>
                                            </a>
                                        </li>
                                        <li id="Remainder" onclick="LeadTimelineTypes(this)">
                                            <a href="javascript:;">
                                                <i class="icon-clock"></i>
                                                <span  class="f12 display-block">Remainder</span>
                                            </a>
                                        </li>
                                        <li id="Meeting" onclick="LeadTimelineTypes(this)">
                                            <a href="javascript:;">
                                                <i class="icon-users1"></i>
                                                <span  class="f12 display-block">Meeting</span>
                                            </a>
                                        </li>
                                        <li id="Note" onclick="LeadTimelineTypes(this)">
                                            <a href="javascript:;">
                                                <i class="icon-edit-sqare"></i>
                                                <span  class="f12 display-block">Note</span>
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12 p0" id="timelineDp">
                                        <div class="input-field">
                                            <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="dpTimeline">
                                            <label for ="dpTimeline" class="datepicker-label">Date  <em>*</em></label>
                                        </div>
                                    </div>
                                    <input type="hidden" id="TypeOfTimeline" value="">
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <textarea id="txainforamtion" class="materialize-textarea"></textarea>
                                            <label for="txainformation">Information</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="file-field input-field">
                                            <div class="btn file-upload-btn">
                                                <span>Attachments</span>
                                                <input  type="file" id="attachemntUpload">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input  class="file-path validate" id="" placeholder="Upload single file" type="text">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>


                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" onclick="AddLeadTimeLine(this)" class="btn blue-btn"><i class="icon-right mr8"></i>Save
                        </button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                class="icon-times mr8"></i>Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editPersonalinfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="">Edit Personal Info</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="form-wrapper clearfix">
                            <form id="ManageLeadsForm">
                                <div class="col-sm-12">
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input id="txtPinfoName" type="text" class="">
                                            <label for="txtPinfoName">Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <select id="ddlLeadInfoCourses" name="ddlLeadInfoCourses">
                                                <option value=""  selected>--Select--</option>

                                            </select>
                                            <label class="select-label">Courses <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input onkeypress="return isNumberKey(event)" maxlength="10" id="txtPinfoMobile" type="text" class="">
                                            <label for="txtPinfoMobile">Mobile</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0" id="gender">
                                        <div class="multiple-radio" ><label>Gender<em>*</em></label>
                                <span class="inline-radio">
                                    <input class="with-gap"  name="txtUserGender" type="radio" id="male" value="male" />
                                    <label for="male">MALE</label>
                                </span>
                                 <span class="inline-radio">
                                    <input class="with-gap" checked name="txtUserGender" type="radio" id="female" value="female"  />
                                    <label for="female">FEMALE</label>
                                </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input onkeypress="return isNumberKey(event)" maxlength="10" id="txtPinfoAltmobile" type="text" class="">
                                            <label for="txtPinfoAltmobile">Alternative Mobile <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <select id="ddlLeadInfoCompanies" name="ddlLeadInfoCompanies">
                                                <option value=""  selected>--Select--</option>

                                            </select>
                                            <label class="select-label">Companies <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input id="txtPinfoEmail" type="text" class="">
                                            <label for="txtPinfoEmail">Email <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0 mb20" >

                                        <div class="input-field col-sm-11 p0">
                                            <select class="custom-select-nomargin" id="ddlLeadInfoDesignation" name="ddlLeadInfoDesignation">
                                                <option value=""  selected>--Select--</option>

                                            </select>
                                            <label class="select-label">Designation <em>*</em></label>

                                        </div>
                                        <div class="col-sm-1 p0">
                                            <a onclick="resetDesignationWrapper(this)" class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn"  href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                        </div>
                                        <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ContactTagWrapper">
                                            <div class="col-sm-8 p0 mt15">
                                                <div class="input-field">
                                                    <input id="txtLeadInfoDesignation" type="text" class="validate" value="">
                                                    <label for="txtLeadInfoDesignation">Designation <em>*</em></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 p0 ">
                                                <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="SaveLeadPersonalInfoDesignation(this)"></span></a>
                                                <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <select id="ddlLeadInfoEducation" name="ddlLeadInfoEducation">
                                                <option value=""  selected>--Select--</option>

                                            </select>
                                            <label class="select-label">Education <em>*</em></label>
                                        </div>
                                    </div>


                                </div>
                            </form>

                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button"  onclick="UpdatePersonalInfo(this)" class="btn blue-btn"><i class="icon-right mr8"></i>Save
                        </button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                class="icon-times mr8"></i>Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade"  id="EducationModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEducational"
             data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Education Info</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="form-wrapper clearfix">
                            <form id="ManageEducationalInfoForm">
                                <div class="col-sm-12">
                                    <input type="hidden" id="hdnEducationInfoId" value="0">
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input id="txtEduName" type="text" class="">
                                            <label for="txtEduName">Course  <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input id="txtEduUniversity" type="text" class="">
                                            <label for="txtEduUniversity">University  <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input onkeypress="return isNumberKey(event)" maxlength="2" id="txtEduPercentage" type="text" class="">
                                            <label for="txtEduPercentage">Percentage  <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">

                                            <input type="date" class="datepicker relative enablePastDates" placeholder="dd/mm/yyyy" id="dpEduYearCompletion">
                                            <label for ="dpEduYearCompletion" class="datepicker-label">Year of Completion  <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <textarea id="txaEduComments" class="materialize-textarea"></textarea>
                                            <label for="txaEduComments">Comments</label>
                                        </div>
                                    </div>

                                </div>
                            </form>

                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" onclick="SaveEducationalInfoDetails(this)" class="btn blue-btn"><i class="icon-right mr8"></i>Save
                        </button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                class="icon-times mr8"></i>Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>


        <!-- InstanceEndEditable --></div>
</div>
<script>

    $context = 'Lead Follow Up';
    $pageUrl = 'leadfollowup';
    $serviceurl = 'leadfollowup';
    docReady(function () {
        leadInfoPageLoad();
    });
    function leadInfoPageLoad(This)
    {
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/LeadFollowUp/getLeadHistoryById';
        var params = {'lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This:This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: leadHistoryResponse});

        ajaxurl = API_URL + 'index.php/LeadFollowUp/getLeadEducationalInfo';
        params = {'lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This:This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: leadEducationalInfoResponse});

        ajaxurl = API_URL + 'index.php/LeadFollowUp/getleadById';
        params = {'lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This:This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess:leadInfoResponse});

        ajaxurl = API_URL + 'index.php/LeadFollowUp/getOtherInfoDetailsByLeadId';
        params = {'lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This:This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess:leadOtherInfoResponse});

        ajaxurl = API_URL + 'index.php/LeadFollowUp/getTimelinesById';
        params = {'lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This:This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess:leadTimelinesResponse});

    }
    function leadTimelinesResponse(response){

        var timelineInfo="";
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            timelineInfo+='<div class="text-center">No Data found</div>';
        }
        else
        {
            for(var i=0;i<response.data.length;i++){
                var CreatedOn = (response.data[i].created_on == null || response.data[i].created_on == '') ? "----" :response.data[i].created_on;
                var Name = (response.data[i].name == null || response.data[i].name == '') ? "----" :response.data[i].name;
                var Designation = (response.data[i].Designation == null || response.data[i].Designation == '') ? "----" :response.data[i].Designation;
                var Description = (response.data[i].description == null || response.data[i].description == '') ? "----" :response.data[i].description;
                var TimelineType = (response.data[i].type == null || response.data[i].type == '') ? "----" :response.data[i].type;
                timelineInfo+='<tr><td>'
                    +'<p class="">'+CreatedOn+'</p></td><td>'
                    +'<p class="f14 font-bold">'+Name+'</p><span class="pl0">'+Designation+'</span></td>'
                    +'<td><p class="dark-sky-blue f12">'+Description+'</p></td>'
                    +'<td><p class="dark-sky-blue f12">'+TimelineType+'</p></td>';
                if(response.data[i].interaction_file != null){
                    timelineInfo+='<td><p class="dark-sky-blue f12"><a href="'+BASE_URL+'uploads/'+response.data[i].interaction_file+'"><i class="icon-arrow-down-circle f16"></i></a></p></td>';
                }else{
                    timelineInfo+='<td><p class="dark-sky-blue f12">----</p></td>';
                }
                timelineInfo+='</tr>';
            }

        }
        $('#timelines-list').html(timelineInfo);
    }
    function leadOtherInfoResponse(response){
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
        }
        else
        {
            if (response.data[0].Significance_of_CPA == 1) {
                $("#chkSignificanceOfCPA").attr("checked",true);
            } else {
                $("#chkSignificanceOfCPA").attr("checked",false);
            }
            if (response.data[0].Willingness_to_pursue_the_CPA == 1) {
                $("#chkPursueTheCPA").attr("checked",true);
            } else {
                $("#chkPursueTheCPA").attr("checked",false);
            }
            if (response.data[0].Response_on_Miles_CPA == 1) {
                $("#chkMilesCPA").attr("checked",true);
            } else {
                $("#chkMilesCPA").attr("checked",false);
            }
            if (response.data[0].Value_addition == 1) {
                $("#chkValueAddition").attr("checked",true);
            } else {
                $("#chkValueAddition").attr("checked",false);
            }
            if (response.data[0].CMA_No == 1) {
                $("#chkCmaNo").attr("checked",true);
            } else {
                $("#chkCmaNo").attr("checked",false);
            }
        }


    }
    function leadInfoResponse(response){ console.log(response.data);
        var leadinfo = '';
        var userId = $('#hdnUserId').val();
        var action="list";
        var headerParams = {action:action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            leadinfo += '<div class="text-center">No Data found</div>';
        } else
        {
            $("#hdnBranchXrefId").val(response.data[0].branch_xref_lead_id);
            $.when(
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name: 'Company'},
                        action:action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This:this,
                        headerParams:headerParams,
                        requestUrl:API_URL+'index.php/Courses/getAllCourseList',
                        action:action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name:'Designation'},
                        action:action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name:'Qualification'},
                        action:action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/LeadsInfo/getBranchVisitorById',
                        params: {branchXrefId:$('#hdnBranchXrefId').val()},
                        action:action
                    }
                )
            ).then(function(leadInfoDataCompanies,leadInfoDataCourses,leadInfoDesignations,leadInfoQualification,branchVisit) {
                leadInfoDataCompanies=leadInfoDataCompanies[0];
                leadInfoDataCourses=leadInfoDataCourses[0];
                leadInfoDesignations=leadInfoDesignations[0];
                leadInfoQualification=leadInfoQualification[0];
                var BranchVisitor=branchVisit[0];
                if(typeof BranchVisitor !== 'undefined' && BranchVisitor.data.length > 0) {
                    (BranchVisitor.data[0].second_level_counsellor_comments.length>0) ? $("#secondCounsellerComments").html("<span><b>Comments:</b></span> "+BranchVisitor.data[0].second_level_counsellor_comments) : '';
                    (BranchVisitor.data[0].name.length>0) ? $("#createdBv").html("<span><b>Created By:</b></span> "+BranchVisitor.data[0].name) : '';
                    (BranchVisitor.data[0].created_date.length>0) ? $("#createdDate").html("<span><b>Created Date:</b></span> "+BranchVisitor.data[0].created_date) : '';
                }
                getLeadPersonalInfoCompanies(leadInfoDataCompanies);
                getLeadPersonalInfoCourses(leadInfoDataCourses);
                getLeadPersonalInfoDesignations(leadInfoDesignations);
                getLeadPersonalInfoQualification(leadInfoQualification);

                var last_modified = (response.data[0].last_modified == null || response.data[0].last_modified == '') ? "----" :response.data[0].last_modified;
                var last_follow_up = (response.data[0].created_on == null || response.data[0].created_on == '') ? "----" :response.data[0].created_on;
                var next_follow_up = (response.data[0].next_followup_date == null || response.data[0].next_followup_date == '') ? "----" :response.data[0].next_followup_date;
                var Lead_name = (response.data[0].name == null || response.data[0].name == '') ? "----" :response.data[0].name;
                var Lead_email = (response.data[0].email == null || response.data[0].email == '') ? "----" :response.data[0].email;
                var Lead_company = (response.data[0].company == null || response.data[0].company == '') ? "----" :response.data[0].company;
                var Lead_phone = (response.data[0].phone == null || response.data[0].phone == '') ? "----" :response.data[0].phone;
                var Lead_course = (response.data[0].course == null || response.data[0].course == '') ? "----" :response.data[0].course;
                var Lead_stage = (response.data[0].leadStage == null || response.data[0].leadStage == '') ? "----" :response.data[0].leadStage;

                leadinfo+= '<div class="col-sm-6 p0"><p class="f14 font-bold p5 m0">' + Lead_name + '</p><p class="pl5 pt0 m0">'
                    + '<lable class="clabel">Email:</lable>'
                    + '<a href="mailto:' + Lead_email +'" class="dark-sky-blue">' + Lead_email + '</a></p>'
                    + '<p class="pl5 pt0 m0"><lable class="clabel">Mobile:</lable>'
                    + '<a href="callto:' + Lead_email + '" class="dark-sky-blue">' + Lead_phone + '</a></p>'
                    + '<p class="pl5 pt0 m0"> <lable class="clabel">Company:</lable>' +Lead_company+ '</p></div>'
                    + '<div class="col-sm-3 p15 text-center blue-border-left blue-border-left">'
                    + '<p class="mt30">Course</p>'
                    + '<span class="f14 font-bold pl0">' + Lead_course + '</span></div>'
                    + '<div class="col-sm-3 text-center p15 blue-border-left"> <p class="mt30">Lead Stage</p><span class="f14 font-bold pl0">' + Lead_stage + '</span></div>'
                    + '<ul class="list-info col-sm-12 mt0 p0">'
                    + '<li class=""><label>Last Modified By:</label> '+last_modified+'</li>'
                    + '<li class=""><label>Last Follow Up:</label> '+last_follow_up+'</li>'
                    + '<li class=""><label>Next Follow Up:</label> '+next_follow_up+'</li></ul>';
                (response.data[0].lead_number == null || response.data[0].lead_number == '') ? "----" :$("#sequence_number").text(response.data[0].lead_number);


                $('#lead-info').html(leadinfo);
                (response.data[0].name == null || response.data[0].name == '') ? $('#leadName').html("----"):$('#leadName').html(response.data[0].name);
                (response.data[0].course == null || response.data[0].course == '') ? $('#leadCourse').html("----"):$('#leadCourse').html(response.data[0].course);
                (response.data[0].phone == null || response.data[0].phone == '') ? $('#leadmobile').html("----"):$('#leadmobile').html(response.data[0].phone);
                (response.data[0].alternate_mobile == null || response.data[0].alternate_mobile == '') ? $('#leadAltNumber').html("----"):$('#leadAltNumber').html(response.data[0].alternate_mobile);
                (response.data[0].company == null || response.data[0].company == '') ? $('#leadCompany').html("----"):$('#leadCompany').html(response.data[0].company);
                (response.data[0].email == null || response.data[0].email == '') ? $('#leadEmail').html("----"):$('#leadEmail').html(response.data[0].email);
                (response.data[0].company == null || response.data[0].company == '') ?$('#leadCompany').html("----"):$('#leadCompany').html(response.data[0].company);
                (response.data[0].qualification == null || response.data[0].qualification == '') ?$('#leadEducation').html("----"):$('#leadEducation').html(response.data[0].qualification);
                (response.data[0].designation == null || response.data[0].designation == '') ?$('#leadDesignation').html("----"):$('#leadDesignation').html(response.data[0].designation);
                $("#hdnLeadId").val(response.data[0].lead_id);


                $('#txtPinfoName').val(response.data[0].name)
                $("#txtPinfoName").next('label').addClass("active");

                $('#txtPinfoMobile').val(response.data[0].phone)
                $("#txtPinfoMobile").next('label').addClass("active");
                $("#txtPinfoMobile").prop('readonly',true);
                if(response.data[0].gender == 1){
                    $("#gender").text("Male");
                    $("#male").attr('checked', 'checked');
                }
                if(response.data[0].gender == 0){
                    $("#gender").text("Female");
                    $("#female").attr('checked', 'checked');
                }

                $('#txtPinfoAltmobile').val(response.data[0].alternate_mobile)
                $("#txtPinfoAltmobile").next('label').addClass("active");

                $('#txtPinfoEmail').val(response.data[0].email)
                $("#txtPinfoEmail").next('label').addClass("active");

                $('#ddlLeadInfoCompanies').val(response.data[0].companyID);
                $('#ddlLeadInfoCompanies').material_select();

                $('#ddlLeadInfoCourses').val(response.data[0].course_id);
                $('#ddlLeadInfoCourses').material_select();

                $('#ddlLeadInfoDesignation').val(response.data[0].desi_id);
                $('#ddlLeadInfoDesignation').material_select();

                $('#ddlLeadInfoEducation').val(response.data[0].qual_id);
                $('#ddlLeadInfoEducation').material_select();

            });
        }
    }
    function leadHistoryResponse(response){
        var History="";
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            History += '<div class="text-center">No Data found</div>';
        }
        else
        {
            for(var i=0;i<response.data.length;i++)
            {
                if(i==0)
                {
                    var LeadHis_mail = (response.data[i].email == null || response.data[i].email == '') ? "----" :response.data[i].email;
                    var LeadHis_designation = (response.data[i].Designation == null || response.data[i].Designation == '') ? "----" :response.data[i].Designation;
                    var LeadHis_name = (response.data[i].name == null || response.data[i].name == '') ? "----" :response.data[i].name;
                    var LeadHis_phone = (response.data[i].phone == null || response.data[i].phone == '') ? "----" :response.data[i].phone;
                    History += '<div class="p-following clearfix">'
                        + '<h4 class="text-uppercase anchor-blue pl10 f14">Present Following</h4>'
                        + '<div class="col-sm-12 ptb10 userworking-info-wrap">'
                        + '<div class="userworking-img"> <img src="'+response.data[i].image+'" class="mCS_img_loaded"> </div>'
                        + '<div class="pull-left pl5 pt5 inline-lable clearfix">'
                        + '<label class="label-data ellipsis">'+LeadHis_name+'</label>'
                        + '<p class="label-text ellipsis">'+LeadHis_designation+'</p>'
                        + '</div><div class="col-sm-12 mt5 p0 user-mail-custom">'
                        + '<p class="m0 clearfix">'
                        + '<lable class="clabel">Email :</lable>'
                        + '<a class="dark-sky-blue" href="mailto:'+LeadHis_mail+'">'+LeadHis_mail+'</a></p>'
                        + '<p class="m0">'
                        + '<lable class="clabel">Mobile :</lable>'
                        + '<a class="dark-sky-blue" href="callto:'+LeadHis_phone+'">'+LeadHis_phone+'</a></p>'
                        + '</div>'
                        + '</div>'
                        + '</div>';
                }
                else
                {
                    LeadHis_mail = (response.data[i].name == null || response.data[i].name == '') ? "----" :response.data[i].name;
                    LeadHis_designation = (response.data[i].Designation == null || response.data[i].Designation == '') ? "----" :response.data[i].Designation;

                    History +='<div class="col-sm-12 ptb10 userworking-info-wrap border-bottom">'
                        +'<div class="userworking-img"> <img src="'+response.data[i].image+'" class="mCS_img_loaded"> </div>'
                        +'<div class="pull-left pl5 pt5 inline-lable">'
                        +'<label class="label-data ellipsis">'+LeadHis_mail+'</label>'
                        +'<p class="label-text ellipsis">'+LeadHis_designation+'</p>'
                        +'</div>'
                        +'</div>';

                }
            }

        }
        $('#History-list').html(History);

    }
    function leadEducationalInfoResponse(response){

        var LatestEducationalInfo="";
        var AllEducationalInfo="";

        LatestEducationalInfo+='<a access-element="add" class="lead-pinfo-add-btn" onclick="ManageEducationalInfo(this,0)" ><i class="icon-plus-circle f18"></i></a>';
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            LatestEducationalInfo += '<center class="ptb10">No Data found</center>';

        }
        else {

            LatestEducationalInfo += '<div class="col-sm-4 mtb10">'
                + '<p class=" label-text">Course</p><label class="label-data">' + response.data[0].course + '</label></div>'
                + '<div class="col-sm-4 mtb10">'
                + '<p class=" label-text">University</p><label class="label-data">' + response.data[0].university + '</label>'
                + '</div>'
                + '<div class="col-sm-4 mtb10">'
                + '<p class=" label-text">Year of Completion</p><label class="label-data">' + response.data[0].year_of_completion + '</label></div>'
                + '<div class="col-sm-4 mtb10">'
                + '<p class=" label-text">Percentage</p><label class="label-data">' + response.data[0].percentage + '% </label></div>'
                + '<div class="col-sm-8 mtb10"><p class=" label-text">comments</p> <label class="label-data">' + response.data[0].comments + '</label>'
                + '</div>'
                + '<a access-element="edit" class="lead-pinfo-edit-btn" onclick="ManageEducationalInfo(this,' + response.data[0].lead_educational_info_id + ')"><i class="icon-edit f16"></i></a>'
                + '<a access-element="delete" class="lead-pinfo-delete-btn" onclick="DeleteEducatonalInfoById(this,' + response.data[0].lead_educational_info_id + ')"><i class="icon-trash f16"></i></a>';
            if(response.data.length>1) {
                AllEducationalInfo += '<table id="EducationInfoTable" class="table table-responsive table-striped table-custom"><thead>'
                    + '<tr> <th>Course</th>'
                    + '<th>University</th>'
                    + '<th>Year of Completion</th>'
                    + '<th>Percentage</th>'
                    + '<th>Comments</th>'
                    + '<th></th>'
                    + '</tr>'
                    + '<tr><td class="border-none p0 lh10">&nbsp;</td></tr> </thead><tbody>';

                for (var i = 1; i < response.data.length; i++) {
                    AllEducationalInfo += '<tr>'
                        + '<td>' + response.data[i].course + '</td>'
                        + '<td>' + response.data[i].university + '</td>'
                        + '<td>' + response.data[i].year_of_completion + '</td>'
                        + '<td>' + response.data[i].percentage + '% </td>'
                        + '<td>' + response.data[i].comments + '</td>'
                        + ' <td>'
                        + '<div class="dropdown feehead-list pull-right custom-dropdown-style">'
                        + '<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#" data-target="#" id="dLabel"> <i class="fa fa-ellipsis-v f18 plr10"></i> </a>'
                        + '<ul aria-labelledby="dLabel" class="dropdown-menu pull-right">'
                        + '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>'
                        + '<li access-element="edit"><a onclick="ManageEducationalInfo(this,' + response.data[i].lead_educational_info_id + ')" href="#">Edit</a></li>'
                        + '<li access-element="delete"><a onclick="DeleteEducatonalInfoById(this,' + response.data[i].lead_educational_info_id + ')" href="#">Delete</a></li>'
                        + '</ul></div></td></tr>';
                }

            }
            AllEducationalInfo += '</tbody></table>';


        }
        $('#LatestEducationalInfo-list').html(LatestEducationalInfo);
        $('#AllEducationalInfo-list').html(AllEducationalInfo);
    }
    function clearEligibility(){
        var $parent = $('#eligibilityForm');

        $parent.find('span.required-msg').remove();
        $parent.find('input, select').removeClass("required");
        $parent.find('textarea').removeClass("required");
    }
    function SaveEligibility(This)
    {
        var $parent = $('#eligibilityForm');
        var userId = $('#hdnUserId').val();
        var LeadId = $('#leadId').val();
        var branch_xref_lead_id = '<?php echo $Id; ?>';

        var eligibility = $("#eligibility").val();

        $parent.find('span.required-msg').remove();
        $parent.find('input, select').removeClass("required");
        $parent.find('textarea').removeClass("required");
        var flag = 0;
        var max_fileUpload_size = 100.048;
        /*For max file size for uploaded file(In MB)*/
        var files = $("#eligibilityAttachemnt").get(0).files;
        var selectedFile = $("#eligibilityAttachemnt").val();
        //console.log(selectedFile);
        var extArray = ['gif','jpg','png','jpeg','doc','docz'];
        var extension = selectedFile.split('.');
        //console.log(files);
        if (files.length > 0)
        {
            if(extension[extension.length - 1] == 'exe')
            {
                $parent.find(".file-path").addClass("required");
                $parent.find(".file-path").after('<span class="required-msg">Upload valid file</span>');
                flag = 1;
            }
        }
        if(eligibility==""){
            $parent.find("#eligibility").parent().find('.select-dropdown').addClass("required");
            $parent.find("#eligibility").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if (flag == 0) {
            var action = "add";
            var headerParams = {
                action: action,
                contentType: false,
                processData: false,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: userId
            };
            var ajaxurl = API_URL + 'index.php/LeadsInfo/saveEligibility';
            var fileUpload = document.getElementById("attachemntUpload");

            var uploadFile = new FormData();
            if (files.length > 0) {
                uploadFile.append("file", files[0]);
                uploadFile.append("orginalFile", selectedFile);
                uploadFile.append("image", 'resume_' + new Date().getTime() +'.'+extension[1]);
            }
            uploadFile.append('eligibility', eligibility);
            uploadFile.append('LeadId', LeadId);
            uploadFile.append('branch_xref_lead_id', branch_xref_lead_id);
            uploadFile.append('userID', '<?php echo $userData['userId']; ?>');

            $.ajax({
                type: "POST",
                url: ajaxurl,
                dataType: "json",
                data: uploadFile,
                contentType: false,
                processData: false,
                headers: headerParams,
                beforeSend: function () {
                    $(This).attr("disabled", "disabled");
                    alertify.dismissAll();
                    notify('Processing..', 'warning', 10);
                }, success: function (response) {
                    if(response.status){
                        leadInfoPageLoadMain(branch_xref_lead_id);
                        alertify.dismissAll();
                        notify(response.message, 'success', 10);
                        $(This).removeAttr("disabled");
                        $parent[0].reset();
                    }else{
                        alertify.dismissAll();
                        notify('Something went wrong. Please try again', 'error', 10);
                    }
                }, error: function (response) {
                    alertify.dismissAll();
                    notify('Something went wrong. Please try again', 'error', 10);
                }
            });

        }
    }
    function getEligibilityFileList(id){
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/LeadsInfo/getEligibilityFileList';
        var params = {'branch_xref_lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
            var html = '';
            if(response.status){
                $('#eligibility_list tbody').html('');
                if(response.data.length>0){
                    for(var a in response.data){
                        html += '<tr>';
                        html += '<td>';
                        html += '<span>'+response.data[a].orginal_filename+'</span>';
                        html += '</td>';
                        html += '<td>' +
                            '<a download target="_blank" href="' + BASE_URL + 'uploads/' + response.data[a].filepath + '"><i class="icon-arrow-down-circle f16"></i></a>' +
                            "<a href='javascript:;' onclick='deleteEligibilityFile(&quot;"+response.data[a].branch_xref_lead_uploads_id+"&quot;)'><i class='icon-trash mr8 pl10'></i></a>" +
                            '</td>';
                        html += '</td>';
                        html += '</tr>';
                    }
                }else{
                    html += '<tr>';
                    html += '<td colspan=2><p class="text-center">No data found</p>';
                    html += '</td>';
                    html += '</tr>';
                }

            }
            $('#eligibility_list tbody').html(html);
        }});
    }

    function deleteEligibilityFile(id) {
        var conf = "Are you sure want to delete?";
        customConfirmAlert('Delete file', conf);
        $("#popup_confirm #btnTrue").attr("onclick", "deleteEligibilityFileConfirm(this,'" + id + "')");
    }
    function deleteEligibilityFileConfirm(This, id){
        var userId = $('#hdnUserId').val();
        var action = 'delete';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/LeadsInfo/deleteEligibilityFile';
        var params = {'branch_xref_lead_uploads_id': id};
        commonAjaxCall({This: This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
            //console.log(response);
            alertify.dismissAll();
            if (response == -1 || response['status'] == false) {
                notify(response.message, 'error', 10);
            } else {
                notify(response.message, 'success', 10);
                $('#popup_confirm').modal('hide');
                getEligibilityFileList(id);
            }
        }});
    }

</script>