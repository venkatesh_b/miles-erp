<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Lead Report by City</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="report"><a class="" onclick="javascript:filterCityWiseReportExport(this)" href="javascript:;"><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <div class="col-sm-5 reports-filter-wrapper pl0">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Branch</label>
                        <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:;"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:;"><i class="icon-times red"></i></a></span>
                    </div>
                    <div class="boxed-tags" id="branchFilter"></div>
                </div>

                <div class="col-sm-5 reports-filter-wrapper">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Course</label>
                        <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:;"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:;"><i class="icon-times red"></i></a></span>
                    </div>
                    <div class="boxed-tags" id="courseFilter">  </div>
                </div>

                <div class="col-sm-2 mt15">
                    <a access-element="report" class="btn blue-btn btn-sm" href="javascript:;" onclick="filterLeadReportByCity(this)">Proceed</a>
                </div>
            </div>
<!--            <div class="col-sm-12 text-right p0" id="report-hint" style="display:none;">
                <ul class="hints list-inline m0" >
                    <li>
                        <i style="background:#de0000;margin-right:-3px"></i>
                        <i style="background:#ff0000;margin-right:-3px"></i>
                        <i style="background:#ff4949;margin-right:-3px"></i>
                        <i style="background:#ff7474;margin-right:-3px"></i>
                        <i style="background:#ff9696"></i>
                        0-30(%)
                    </li>
                    <li>
                        <i style="background:#ffbc09;margin-right:-3px"></i>
                        <i style="background:#ff9907;margin-right:-3px"></i>
                        <i style="background:#ff8106;margin-right:-3px"></i>
                        <i style="background:#ff7474;margin-right:-3px"></i>
                        <i style="background:#d86d05;margin-right:-3px"></i>
                        <i style="background:#B65C04"></i>
                        31-74(%)
                    </li>
                    <li>
                        <i style="background:#04f382;margin-right:-3px"></i>
                        <i style="background:#04dc76;margin-right:-3px"></i>
                        <i style="background:#04c76b;margin-right:-3px"></i>
                        <i style="background:#05a355;margin-right:-3px"></i>
                        <i style="background:#048b48"></i>
                        75-100(%)
                    </li>
                </ul>
            </div> -->
            <div class="col-sm-12 clearfix mt15 p0 horizontal-scroll" id="report-list">
                <table class="table table-responsive table-striped table-custom">
                    <thead>
                        
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                <!--<div class="col-sm-12 pr0">
                        <a href="#" class="pull-right download-excel" title="Download Excel"><i class="fa fa-file-excel-o f18 p10"></i></a>
            </div>-->
                
                
            </div>

        </div>
        <!-- InstanceEndEditable --></div>
</div>

<script>
docReady(function () {
    $("#report-list").hide();
    leadReportByCityPageLoad();
});
    
function leadReportByCityPageLoad(){
    var userId = $('#userId').val();
    var action = 'report';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

    var ajaxurl = API_URL + 'index.php/BranchReport/getAllConfigurationsByUserId';
    commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
        var dashboard = '';
        if (response.data.length == 0 || response == -1 || response['status'] == false) {
        } else {
            var RawData = response.data;
            var html = '';
            if (RawData.branch.length > 0) {
                for (var a in RawData.branch) {
                    html += '<a class="active" data-id="' + RawData.branch[a].id + '" href="javascript:;">' + RawData.branch[a].name + '</a>'
                }
                $('#branchFilter').html(html);
            }
            html = '';
            if (RawData.course.length > 0) {
                for (var a in RawData.course) {
                    html += '<a class="active" data-id="' + RawData.course[a].courseId + '" href="javascript:;">' + RawData.course[a].name + '</a>'
                }
                $('#courseFilter').html(html);
            }
            html = '';
            if (RawData.leadStage.length > 0) {
                for (var a in RawData.leadStage) {
                    html += '<a class="active" data-id="' + RawData.leadStage[a].lead_stage_id + '" href="javascript:;">' + RawData.leadStage[a].lead_stage + '</a>'
                }
                $('#stageFilter').html(html);
            }
        }
    }});
}
    
function filterCityWiseReportExport(This){
    var branchId = [];
    var courseId = [];
    var branchIdText = [];
    var courseIdText = [];
    var error = true;
    $("#branchFilter > .active").each(function () {
        error = false;
        branchId.push($(this).attr('data-id'));
        branchIdText.push($(this).text());
    });
    $("#courseFilter > .active").each(function () {
        error = false;
        courseId.push($(this).attr('data-id'));
        courseIdText.push($(this).text());
    });
    var branchId=branchId.join(',');
    var courseId=courseId.join(',');
    if(branchId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one branch','error');
        return false;
    }
    else if(courseId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one course','error');
        return false;
    }
    else{
        error=false;
    }

    if (!error) {
        var total=0;
        var pageTotal=0;
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var action = 'report';
        var ajaxurl=API_URL+'index.php/CityReport/getCityReportExport';
        var params = {'branch': branchId, 'course': courseId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '), 'stageText': ''};
        var userID=$('#userId').val();
        var type='GET';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterLeadReportByCityExportResponse});
    }else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}
    
function filterLeadReportByCityExportResponse(response){
    alertify.dismissAll();
    if(response.status===true){
        var download=API_URL+'Download/downloadReport/'+response.file_name;
        window.location.href=(download);
    }
    else{
        notify(response.message,'error');
    }
}
    
function filterLeadReportByCity(This) {
    var branchId = [];
    var courseId = [];
    var branchIdText = [];
    var courseIdText = [];
    var error = true;
    $("#branchFilter > .active").each(function () {
        error = false;
        branchId.push($(this).attr('data-id'));
        branchIdText.push($(this).text());
    });
    $("#courseFilter > .active").each(function () {
        error = false;
        courseId.push($(this).attr('data-id'));
        courseIdText.push($(this).text());
    });
    var branchId=branchId.join(',');
    var courseId=courseId.join(',');
    if(branchId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one branch','error');
        return false;
    }
    else if(courseId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one course','error');
        return false;
    }
    else{
        error=false;
    }

    if (!error) {
        var ajaxurl = API_URL + "index.php/CityReport/getCityReport?branch="+branchId+"&course="+encodeURI(courseId)+"&branchText="+branchIdText.join(', ')+"&courseText="+courseIdText.join(', ');
        var method = 'GET';
        var action = 'report';
        var userId = $('#userId').val();
        var headerParams = {action: action, contentType: false, processData: false, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        $.ajax({
            type: method,
            url: ajaxurl,
            dataType: "json",
            contentType: false,
            processData: false,
            headers: headerParams,
            beforeSend: function () {
                $(This).attr("disabled", "disabled");
            }, success: function (response) {
                $(This).removeAttr("disabled");
                createLeadReportByCityList(response);
            }, error: function (response) {
                notify('Something went wrong. Please try again', 'error', 10);
            }
        });
        /**/
    } else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}
function createLeadReportByCityList(rawData){
    var rawHtml = '';
    var rawHtmlTableHead = '';
    if (rawData == -1 || rawData['status'] == false || rawData.length <= 0){
        alertify.dismissAll();
        notify('No Records Found', 'error', 10);
    }else{
              rawHtmlTableHead += '<tr>';
                rawHtmlTableHead += '<th rowspan="2" class="valign-middle text-center">Branch</th>';
        for(var index in rawData.data.course){
                rawHtmlTableHead += '<th colspan="4" class="text-center">'+rawData.data.course[index]+'</th>';
        }
                rawHtmlTableHead += '<th colspan="4" class="text-center">Total</th>';
                rawHtmlTableHead += '</tr>';
                rawHtmlTableHead += '<tr>';
        for(var index in rawData.data.course){
                    rawHtmlTableHead += '<th >Lead</th>';
                    rawHtmlTableHead += '<th class="text-right">Count</th>';
                    rawHtmlTableHead += '<th class="text-right">Probability</th>';
                    rawHtmlTableHead += '<th class="text-right">Estimate</th>';
        }
                    rawHtmlTableHead += '<th >Lead</th>';
                    rawHtmlTableHead += '<th class="text-right">Count</th>';
                    rawHtmlTableHead += '<th class="text-right">Probability</th>';
                    rawHtmlTableHead += '<th class="text-right">Estimate</th>';
                rawHtmlTableHead += '</tr>';
                rawHtmlTableHead += '<tr><td class="border-none p0 lh10">&nbsp;</td></tr>';

                
        var main = rawData.data.data;
        for(var index in main){
            var details = main[index];
                rawHtml += '<tr class="rowspan-group-tr">';
                    rawHtml += '<td rowspan="7" class="rowspan-td">'+index+'</td>';
            var lead = 0;
            var leadCount = 0;
            var conversionCount = 0;
            var probCount = 0;
            
            for(var a=0; a<details.length; a++){
                var counter = 1;
                if(a != 0){
                    rawHtml += '<tr class="rowspan-group-tr">';
                }
                for(var i in rawData.data.course){
                    if(typeof details[a] !== 'undefined'){
                        rawHtml += '<td class="text-right">'+details[a]['lead_stage']+'</td>';
                        rawHtml += '<td class="text-right">'+details[a]['leadCount']+'</td>';
                        rawHtml += '<td class="text-right">'+details[a]['conversion_rate']+'</td>';
                        rawHtml += '<td class="text-right">'+details[a]['prob']+'</td>';
                        lead = details[a]['lead_stage'];
                        leadCount += parseInt(details[a]['leadCount']);
                        conversionCount = details[a]['conversion_rate'];
                        probCount += parseInt(details[a]['prob']);
                        if(rawData.data.course.length == counter){
                            break;
                        }
                        a = parseInt(a)+1;
                    } else {
                        rawHtml += '<td class="text-right"> 0 </td>';
                        rawHtml += '<td class="text-right"> 0 </td>';
                        rawHtml += '<td class="text-right"> 0 </td>';
                        rawHtml += '<td class="text-right"> 0 </td>';
                        lead = 0;
                        leadCount = 0;
                        conversionCount = 0;
                        probCount = 0;
                        if(rawData.data.course.length == counter){
                            break;
                        }
                        a = parseInt(a)+1;
                    }
                    counter++;
                }
                    rawHtml += '<td class="text-right">'+lead+'</td>';
                    rawHtml += '<td class="text-right">'+leadCount+'</td>';
                    rawHtml += '<td class="text-right">'+conversionCount+'</td>';
                    rawHtml += '<td class="text-right">'+probCount+'</td>';
                    leadCount = 0;
                    lead = '';
                    conversionCount = 0;
                    probCount = 0;
                rawHtml += '</tr><!-- rowspan-group-tr -->\n';
            }
            
        }
    }

    $('#report-list tbody').html(rawHtml);
    $('#report-list thead').html(rawHtmlTableHead);
    $("#report-list").show();
}
</script>