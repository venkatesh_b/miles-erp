<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Lead Report by Source</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="report"><a class="" onclick="javascript:filterSourceWiseReportExport(this)" href="javascript:;"><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <div class="col-sm-5 reports-filter-wrapper pl0">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Branch</label>
                        <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:;"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:;"><i class="icon-times red"></i></a></span>
                    </div>
                    <div class="boxed-tags" id="branchFilter"></div>
                </div>

                <div class="col-sm-5 reports-filter-wrapper">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Course</label>
                        <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:;"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:;"><i class="icon-times red"></i></a></span>
                    </div>
                    <div class="boxed-tags" id="courseFilter">  </div>
                </div>

                <div class="col-sm-2 mt15">
                    <a access-element="report" class="btn blue-btn btn-sm" href="javascript:;" onclick="filterLeadReportBySource(this)">Proceed</a>
                </div>
            </div>
            <div class="col-sm-12 text-right p0" id="report-hint" style="display:none;">
                <ul class="hints list-inline m0" >
                    <li>
                        <i style="background:#de0000;margin-right:-3px"></i>
                        <i style="background:#ff0000;margin-right:-3px"></i>
                        <i style="background:#ff4949;margin-right:-3px"></i>
                        <i style="background:#ff7474;margin-right:-3px"></i>
                        <i style="background:#ff9696"></i>
                        0-30(%)
                    </li>
                    <li>
                        <i style="background:#ffbc09;margin-right:-3px"></i>
                        <i style="background:#ff9907;margin-right:-3px"></i>
                        <i style="background:#ff8106;margin-right:-3px"></i>
                        <i style="background:#ff7474;margin-right:-3px"></i>
                        <i style="background:#d86d05;margin-right:-3px"></i>
                        <i style="background:#B65C04"></i>
                        31-74(%)
                    </li>
                    <li>
                        <i style="background:#04f382;margin-right:-3px"></i>
                        <i style="background:#04dc76;margin-right:-3px"></i>
                        <i style="background:#04c76b;margin-right:-3px"></i>
                        <i style="background:#05a355;margin-right:-3px"></i>
                        <i style="background:#048b48"></i>
                        75-100(%)
                    </li>
                </ul>
            </div> 
            <div class="col-sm-12 clearfix p0">
                <!--<div class="col-sm-12 pr0">
                        <a href="#" class="pull-right download-excel" title="Download Excel"><i class="fa fa-file-excel-o f18 p10"></i></a>
            </div>-->
<!--                <table class="table table-inside branchwise-report-table" id="report-list">
                    <thead>
                        <tr>
                            <th class="text-center">Branch</th>
                            <th width="90%"> <table class="branchwise-report-innertable" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="8%">Course</th>
                                            <th class="text-right" width="6%">M1</th>
                                            <th class="text-right" width="6%">M2</th>
                                            <th class="text-right" width="6%">M3</th>
                                            <th class="text-right" width="6%">M4</th>
                                            <th class="text-right" width="6%">M5</th>
                                            <th class="text-right" width="6%">M6</th>
                                            <th class="text-right" width="6%">M7</th>
                                            <th class="text-right" width="6%">L1</th>
                                            <th class="text-right" width="6%">L2</th>
                                            <th class="text-right" width="6%">L3</th>
                                            <th class="text-right" width="6%">L4</th>
                                            <th class="text-right" width="6%">L5</th>
                                            <th class="text-right" width="6%">L6</th>
                                            <th class="text-right" width="6%">L7</th>
                                            <th class="text-right">Total</th>
                                        </tr>
                                    </thead>
                                </table>
                            </th>
                        </tr>
                        <tr>
                            <td class="border-none p0 lh10">&nbsp;</td>
                        </tr>
                    </thead>
                    
                </table>-->
                <table class="table table-inside table-custom branchwise-report-table" id="report-list" style="display:none">
                    <thead>
                        <tr>
                            <th>Lead Source</th>
                            <th>M7</th>
                            <th>M6</th>
                            <th>M5</th>
                            <th>M4</th>
                            <th>M3+</th>
                            <th>M3</th>
                            <th>M2</th>
                            <th>L2</th>
                            <th>L3</th>
                            <th>L3+</th>
                            <th>L4</th>
                            <th>L5</th>
                            <th>L6</th>
                            <th>DnD</th>
                            <th>Conversion Rate</th>
                            <th>Total</th>
                        </tr>
                    </thead>
<!--                    <tfoot>
                        <tr>
                            <td width="30%">Total</td>
                            <td class="border-r-none"></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>

                        </tr>
                    </tfoot>-->
                </table>
            </div>

        </div>
        <!-- InstanceEndEditable --></div>
</div>

<script>
    docReady(function () {
        $("#report-list").hide();
        leadReportBySourcePageLoad();
    });
</script>