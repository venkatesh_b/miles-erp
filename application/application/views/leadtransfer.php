<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Lead Transfer</h3>
        <div class="content-header-btnwrap">
            <ul>

                <li access-element="add"><a class="tooltipped" data-position="left" data-tooltip="Lead Transfer" class="viewsidePanel" onclick="ManageLeadTransfer(this,0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom min-height-td" id="lead-transfer-list">
                    <thead>
                    <!--                    <tr>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Course</th>
                                            <th>Transfered From</th>
                                            <th>Transfered To</th>
                                        </tr>-->
                    <tr>
                        <th>Name</th>
                        <th>Course</th>
                        <th>Level </th>
                        <th>Transferred To</th>
                        <th class="no-sort border-r-none">Info</th>
                        <th class="no-sort"  style="width:10%;"> </th>
                        <!--<th style="width:5%;" class="no-sort">View</th>-->
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="fee-transfer-list-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManagefeetransferForm" method="post" onsubmit="getLeadFeeTransfer(this); return false;" >
                        <div class="modal-header">
                            <button onclick="closeLeadTransfer()" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Lead Transfer</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-10">
                                <div class="input-field">
                                    <input id="txtleadNumber" type="text" class="validate">
                                    <label for="txtleadNumber">Mobile number <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-2 p0 mt2">
                                <div class="input-field">
                                    <!--<button type="button" class="btn blue-btn" onclick="getLeadFeeTransfer(this)">G0</button>-->
                                    <input type="submit" class="btn blue-btn" value="GO">
                                </div>
                            </div>
                            <div id="leadTructurescreen"></div>
                        </div>
                        <div class="modal-footer">
                            <button onclick="saveLeadTransfer(this)" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Save</button>
                            <button onclick="closeLeadTransfer()" data-dismiss="modal" class="btn blue-light-btn" type="button"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
docReady(function(){
    buildLeadTransferListDataTable();
});
$('#userBranchList').change(function(){
    changeDefaultBranch(this);
    buildLeadTransferListDataTable();
});
function buildLeadTransferListDataTable(){
    var branchId=$('#userBranchList').val();
    if(branchId.trim()==''){
        branchId=0;
    }
    var ajaxurl=API_URL+'index.php/LeadTransfer/getLeadTransferList/branch_id/'+branchId;
    var userID=$('#hdnUserId').val();
    var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
    $("#lead-transfer-list").dataTable().fnDestroy();
    $tableobj= $('#lead-transfer-list').DataTable( {
        "fnDrawCallback": function() {
            buildpopover();
            verifyAccess();
            var $api = this.api();
            var pages = $api.page.info().pages;
            if(pages > 1)
            {
                $('.dataTables_paginate').css("display", "block");
                $('.dataTables_length').css("display", "block");
                //$('.dataTables_filter').css("display", "block");
            } else {
                $('.dataTables_paginate').css("display", "none");
                $('.dataTables_length').css("display", "none");
                //$('.dataTables_filter').css("display", "none");
            }
        },
        dom: "Bfrtip",
        bInfo: false,
        "serverSide": true,
        "bProcessing": true,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No records found",
            "sZeroRecords": "No records found",
            "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
        },
        ajax: {
            url:ajaxurl,
            type:'GET',
            headers:headerParams,
            error:function(response) {
                DTResponseerror(response);
            }
        },
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false
        } ],
        "order": [[ 0, "desc" ]],
        columns: [
            {
                data: null, render: function ( data, type, row )
            {
                var leadName="";
                var leadIcon='male-icon.jpg';
                if(data.gender==0){
                    var leadIcon='female-icon.jpg';
                }
                leadName += '<div class="img-wrapper img-wrapper-leadinfo light-blue3-bg" style="width:70px;height:63px;margin-left:-8px; position:absolute;top:50%;margin-top:-32px"><a class=\'tooltipped\' data-position="top" data-tooltip="Click here for lead info" href="'+BASE_URL+'app/leadinfo/'+data.branch_xref_lead_id_encode+'/'+$context+'~'+$pageUrl+'~'+$pageUrl+'"><img src="<?php echo BASE_URL; ?>assets/images/'+leadIcon+'" style="width:33px;height:33px"><span class="img-text" style="word-wrap:break-word">' + data.lead_number + '</span></a></div>';
                leadName+='<div style="padding-left:85px"><div class="f14 font-bold">';
                leadName+= data.name;
                /*if(data.comments.length>0){
                    leadName+='<span style="cursor: pointer;" class="popper" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Comments" style="cursor: pointer;"><i class="fa fa-street-view ml5"></i></span>';
                    leadName+='<div class="popper-content hide">';
                }
                if(data.comments.length>0)
                {
                    $.each(data.comments, function (key, value) {
                        if(value.branch_visit_comments==null || value.branch_visit_comments.trim()=='')
                            value.branch_visit_comments='----';
                        leadName+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">Branch Visitor Comments</h4>';
                        leadName+= '<p class="p0">'+value.branch_visit_comments+'</p>';
                        leadName+= '<p class="p0 mt5">'+value.branch_visited_date+'<span class="ml15">By<strong class="anchor-blue pl10">'+value.counseollor_name+'</strong></span> </p>';
                        leadName+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">Second Level Comments</h4>';
                        leadName+= '<p class="p0">'+value.second_level_comments+'</p>';
                        leadName+= '<p class="p0 mt5 border-bottom">'+value.counsellor_visited_date+'<span class="ml15">By<strong class="anchor-blue pl10">'+value.second_counseollor_name+'</strong></span> </p>';
                    });


                }
                if(data.comments.length>0){
                    leadName+='</div>';
                }*/
                leadName+='</div></div>';
                return leadName;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {
                return data.course_name;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {
                return data.lead_stage;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {
                return data.transfered_to;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {
                var company_name = (data.cName == null || data.cName == '') ? "" : '<p><lable class="clabel icon-label"><a class="dark-sky-blue tooltipped" data-position="top" data-tooltip="Company"  href="javascript:"><i class="icon-company ml5 mt2 f14"></i></a></lable>' + data.cName + '</p>';
                var Qualificatin_name = (data.qualificationName == null || data.qualificationName == '') ? "" : '<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Qualification" class="dark-sky-blue tooltipped"><i class="icon-qualification ml5 mt2"></i></a></lable>' + data.qualificationName + '</p>';
                var infoEmail = (data.email == null || data.email == '') ? "" : '<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Email" class="dark-sky-blue tooltipped"><i class="icon-mail ml5 mt2"></i></a></lable>' + data.email + '</p>';
                var infoPhone = (data.phone == null || data.phone == '') ? "" : '<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Phone" class="dark-sky-blue tooltipped"><i class="icon-phone ml5 mt2"></i></a></lable>' + data.phone + '</p>';
                var info = "";
                info += infoEmail;
                info += infoPhone;
                //<p class="popper" data-toggle="popover" data-placement="top" data-trigger="hover" data-title="Last Call">
                //info+='<div class="popper-content hide"> <span class="display-block mtb10">He is intrested but not very sure. He can join in Hyderabad. </span> <p>12 Jun,2016. <span class="ml15">By<strong class="anchor-blue pl10">Govind</strong></span> </p> </div></p>';
                //info+=''+company_name+'';
                info += '' + Qualificatin_name + '';
                return info;
            }
            },
            {
                    data: null, render: function ( data, type, row )
                {
                    var view='';
                    view+='<div class="leads-icons pull-right">';
//                    view+='<span data-tooltip="Comment" data-delay="50" data-position="top" class="tooltipped"><a href="#"><img src="<?php echo BASE_URL; ?>assets/images/comment.png"></a></span>';
                    if(data.comments.length>0){
                        //view+='<a href="javascript:" class="popper" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Comments"><span  ><i class="fa fa-street-view ml5"></i></span></a>';
                        view+='<a href="javascript:" class="popper" data-toggle="popover" data-placement="left" data-trigger="Click" data-title="Comments"><img src="<?php echo BASE_URL; ?>assets/images/comment.png"></a>';
                        view+='<div class="popper-content hide">';
                    }
                    if(data.comments.length>0)
                    {
                        $.each(data.comments, function (key, value) {
                            if(value.branch_visit_comments==null || value.branch_visit_comments.trim()=='')
                                value.branch_visit_comments='----';
                            view+='<div class="border-bottom plr15 pb20 mb5">';
                            view+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">First Level Comments</h4>';
                            view+= '<p class="p0">'+value.branch_visit_comments+'</p>';
                            view+= '<p class="p0 mt5 ash">'+value.branch_visited_date+'<span class="ml15">By<span class="anchor-blue pl10">'+value.counseollor_name+'</span></span> </p>';
                            view+='</div>';
                            view+='<div class="border-bottom plr15 pb20 mb5">';
                            view+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">Second Level Comments</h4>';
                            view+= '<p class="p0">'+value.second_level_comments+'</p>';
                            view+= '<p class="p0 mt5 ash">'+value.counsellor_visited_date+'<span class="ml15">By<span class="anchor-blue pl10">'+value.second_counseollor_name+'</span></span> </p>';
                            view+='</div>';
                        });


                    }
                    if(data.comments.length>0){
                        view+='</div>';
                    }
                    view+='</div>';
                    return view;
                }
            }
        ]
    } );
    DTSearchOnKeyPressEnter();
}

function getLeadFeeTransfer(){
    $('#ManagefeetransferForm span.required-msg').remove();
    $('#ManagefeetransferForm input').removeClass("required");

    var txtleadNumber = $('#txtleadNumber').val();
    var error = false;
    if (txtleadNumber == "") {
        $("#txtleadNumber").addClass('required');
        $("#txtleadNumber").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        error = true;
    } else if(!(/^[0-9]+$/.test(txtleadNumber))){
        $("#txtleadNumber").addClass("required");
        $("#txtleadNumber").after('<span class="required-msg">Must contain only Numbers</span>');
        error = true;
    }

    if(!error){
        notify('Processing..', 'warning', 10);
        var UserId = $('#hdnUserId').val();
        var branchId=$('#userBranchList').val();
        var ajaxurl = API_URL + 'index.php/LeadTransfer/getLeadSpanshot/lead_id/' + txtleadNumber +'/branch_id/'+branchId;
        var type = "GET";
        $action = 'list';
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: $action, onSuccess: function(response){
            alertify.dismissAll();
            if (response == -1 || response['status'] == false)
            {
                alertify.dismissAll();
                $("#txtleadNumber").addClass("required");
                $("#txtleadNumber").after('<span class="required-msg">'+response.message+'</span>');
                $('#leadTructurescreen').html('');
                notify(response.message, 'error', 10);
            } else {
                var rawData = response.data;
                var content = '<div class="col-sm-12">';
                content += '<div class="light-blue3-bg clearfix pt10 pb10">';
                content += '<div class="col-sm-6 mb5">';
                content += '<div class="input-field data-head-name">';
                content += '<span class="display-block ash">Name</span>';
                content += '<p>'+rawData.name+'</p>';
                content += '</div>';
                content += '</div>';
                content += '<div class="col-sm-6 mb5">';
                content += '<div class="input-field data-head-name">';
                content += '<span class="display-block ash">Email</span>';
                content += '<p>'+rawData.email+'</p>';
                content += '</div>';
                content += '</div>';
                content += '<div class="col-sm-6 mb5">';
                content += '<div class="input-field data-head-name">';
                content += '<span class="display-block ash">Phone</span>';
                content += '<p>'+rawData.phone+'</p>';
                content += '</div>';
                content += ' </div>';
                content += '<div class="col-sm-6 mb5">';
                content += '<div class="input-field data-head-name">';
                content += '<span class="display-block ash">Branch</span>';
                content += '<p>'+rawData.branch_name+'</p>';
                content += '</div>';
                content += '</div>';
                content += '<div class="col-sm-6 mb5">';
                content += '<div class="input-field data-head-name">';
                content += '<span class="display-block ash">Course</span>';
                content += '<p>'+rawData.course_name+'</p>';
                content += '</div>';
                content += '</div>';
                if(rawData.expected_visit_date !== null){
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Expected Visited Date</span>';
                    content += '<p>'+rawData.expected_visit_date+'</p>';
                    content += '</div>';
                    content += '</div>';
                }
                if(rawData.expected_enroll_date !== null){
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Expected Enrollment date</span>';
                    content += '<p>'+rawData.expected_enroll_date+'</p>';
                    content += '</div>';
                    content += '</div>';
                }
                /*if(rawData.transferred_to !== null){
                 content += '<div class="col-sm-6">';
                 content += '<div class="input-field">';
                 content += '<span class="display-block ash">Transferred To</span>';
                 content += '<p>'+rawData.transferred_to+'</p>';
                 content += '</div>';
                 content += '</div>';
                 }*/
                if(rawData.transferred_from !== null){
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Transferred From</span>';
                    content += '<p>'+rawData.transferred_from+'</p>';
                    content += '</div>';
                    content += '</div>';
                }
                content += '</div>';
                content += '</div>';
                content += '<div class="col-sm-12"><label>Transfer</label></div>';
                content += '<div class="col-sm-12">';
                content += '<div class="input-field">';
                content += '<select id="txtUserBranch" name="txtUserBranch" class="validate" class="formSubmit">';
                content += '<option value="" selected>--Select--</option>';
                content += '</select>';
                content += '<label class="select-label">Branch <em>*</em></label>';
                content += '</div>';
                content += '</div>';
                content += '<div class="col-sm-12">';
                content += '<div class="input-field">';
                content += '<textarea class="materialize-textarea" id="txttextarea1"></textarea>';
                content += '<label for="txttextarea1" class="">Comments <em>*<em></label>';
                content += '</div>';
                content += '</div>';
                $('.modal-footer').show();

                $('#leadTructurescreen').html(content);
                ajaxurl = API_URL + 'index.php/Branch/getBranchList';
                commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: $action, onSuccess: function(response){
                    if (response == -1 || response['status'] == false) {
                        $('#leadTructurescreen').html('');
                        alertify.dismissAll();
                        notify(response.message, 'error', 10);
                    } else {
                        $('#txtUserBranch').find('option:gt(0)').remove();
                        var branchRawData = response.data;
                        for (var b = 0; b < branchRawData.length; b++) {
                            var o = $('<option/>', {value: branchRawData[b]['id']})
                                .text(branchRawData[b]['name']);
                            o.appendTo('#txtUserBranch');
                        }
//                            branchList(response);
                        $("#txtUserBranch option:contains('"+rawData.branch_name+"')").remove();    /*Remove selected branch*/
                        $('#txtUserBranch').material_select();
                    }
                }});
            }
        }});
    }
}
</script>
<script>
    $(function(){
        var headerParams = {
            action: 'list',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $('input#txtleadNumber').jsonSuggest({
            onSelect:function(item){
                $('input#txtleadNumber').val(item.id);
            },
            headers:headerParams,url:API_URL + 'index.php/FeeAssign/getLeadsInformation' , minCharacters: 2});
    });

</script>