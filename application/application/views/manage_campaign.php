<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-25
 * Time: 10:30 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnCampaignId" value="<?php echo $Id; ?>" readonly />
    <input type="hidden" id="hdnTemplatePath" value="" readonly />

    <div class="content-header-wrap">
        <h3 class="content-header" id="headerName">Create Campaign</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="" href="<?php echo APP_REDIRECT;?>campaign"><i class="icon-times" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0 mb20">
                <form name="campaign_basic" id="campaign_basic_form">
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 pl0">
                        <div class="input-field">
                            <input id="campaign_name" class="validate" type="text">
                            <label class="" for="campaign_name">Campaign Name <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-6 p0">
                        <div class="input-field">
                            <select name="campaignBranch" id="campaignBranch">
                            </select>
                            <label class="select-label">Branch <em>*</em></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                <div class="col-sm-6 pl0">
                    <div class="input-field">
                        <label for="campaignStartDate" class="datepicker-label datepicker">Start Date <em>*</em> </label>
                        <input type="date" name="campaignStartDate" id="txtCampaignStartDate" class="datepicker relative enableFutureDates" placeholder="dd mmm,yyyy">
                    </div>
                </div>
                <div class="col-sm-6 pl0 pr0">
                    <div class="input-field">
                        <label for="campaignEndDate" class="datepicker-label datepicker">End Date <em>*</em></label>
                        <input type="date" name="campaignEndDate" id="txtCampaignEndDate" class="datepicker relative enableFutureDates" placeholder="dd mmm,yyyy">
                    </div>
                </div>
                </div>
                <div class="col-sm-12 p0">
                    <div class="input-field">
                        <textarea id="txaCampaignDescription" class="materialize-textarea"></textarea>
                        <label for="txaCampaignDescription">Description</label>
                    </div>
                </div>
                    </form>
                <div class="col-sm-12 text-center">
                    <button type="button" class="btn blue-btn" onclick="saveBasicDetails()">Step2: Target Audience</button>
                </div>
            </div>
            <div class="col-sm-12 mb20 white-bg" id="step2" >
                <h4 class="heading-uppercase mb5">Target Audience by Groups</h4>
                <div class="col-sm-12 p0">
                    <div class="input-field">
                        <select multiple class="custom-select-nomargin" name="ddlCampaignGroups" id="ddlCampaignGroups">
                        </select>
                        <label class="select-label">Add Groups</label>
                    </div>
                </div>
                <div>
                    <h4 class="heading-uppercase mb5 display-inline-block">Shortlisted list from the group</h4>
                </div>
                <div class="col-sm-12 p0">
                    <table class="table table-responsive table-striped table-custom" id="tagContactsList">
                        <thead>
                        <tr>
                            <th>Group Name</th>
                            <th>Total Contacts</th>
                        </tr>
                        <tr><td class="border-none p0 lh10">&nbsp;</td></tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-12 text-center">
                <button type="button" class="btn blue-btn" onclick="movetoStep3()">Step3: Select Template</button>
            </div>
            <div class="col-sm-12 p0" id="step3" >
                <h4 class="heading-uppercase mb5">Template Manager</h4>
                <div class="col-sm-12 white-bg">
                    <h4 class="heading-uppercase mb5">Attach Template</h4>
                    <form name="campaign_template" id="campaign_template">
                    <div class="file-field input-field">
                        <div class="btn file-upload-btn">
                            <span>Attach Template</span>
                            <input type="hidden" name="camp_template_name" id="camp_template_name" />
                            <input type="hidden" name="camp_template_path" id="camp_template_path" />
                            <input type="file" id="campTemplate" >
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="(browse)">
                        </div>
                    </div>
                    <h4 class="heading-uppercase mb5">Attached Template</h4>
                    <div class="col-sm-8 p20 grey-bg mb20">
                        <div class="attached-template">
                            <iframe class="template_preview" name="ifrm_camp_template" id="ifrm_camp_template" src="" frameborder="0"></iframe>
                            <div class="pt10" id="preview_temp">Template Preview will come here</div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h4 class="heading-uppercase mb5">To Preview Template</h4>
                        <button class="btn blue-btn" type="button" onclick="showPreviewCampaignTemplate(this)">
                            <i class="icon-eye mr8"></i>
                            Preview Template
                        </button>
                        <div class="col-sm-12 mt40 p0">
                            <h4 class="heading-uppercase mb5">To test the Email Template</h4>
                            <div class="input-field mt0">
                                <input id="template_email" class="" type="email">
                                <label data-success="right" data-error="wrong" for="template_email">Email Address for testing</label>
                            </div>
                            <button class="btn blue-btn" data-dismiss="modal" type="button" onclick="sendTestTemplate(this)">
                                Test Template
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="text-center mt10">
                    <button class="btn blue-btn" type="button" id="btnSave" onclick="saveCampaign(this)">
                        Add Campaign
                    </button>
                    <button class="btn blue-light-btn" data-dismiss="modal" type="button" id='btnCancel' onclick="gotocampaign()">
                        Cancel
                    </button>
                </div>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Create Batch</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate">
                                <label for="last_name">Batch Name</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <select multiple>
                                    <option value="" disabled selected>--Select--</option>
                                    <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option>
                                </select>
                                <label class="select-label">Courses</label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input-field">
                                <textarea id="textarea1" class="materialize-textarea"></textarea>
                                <label for="textarea1">Description</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Create Batch</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>

<script type="text/javascript">
    $context='Campaign';
    $pageUrl='campaign';
    var campaignId='<?php echo $Id; ?>';
    docReady(function(){
        enableDatePicker();
        if(campaignId != 0)
        {
            $("#headerName").html("EDIT CAMPAIGN");
            $("#btnSave").html("");
            $("#btnSave").html("<i class='icon-right mr8'></i>Update");
            getCampaignDetails();
        }
        else
        {
            $("#headerName").html("CREATE CAMPAIGN");
            $("#btnSave").html("");
            $("#btnSave").html("<i class='icon-right mr8'></i>Add");
            $("#campaign_template input").prop("disabled", true);
            getCampaignBranches(this);
        }
    });
    function getCampaignBranches(This,branchId=0) {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Campaign/getBranchList';
        var params = {};
        var headerParams = {context: 'default', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: function(response)
            {
                TotalCampaignBranchesResponse(response,branchId);
            }
            }
        );
    }
    function TotalCampaignBranchesResponse(response,branchId) {

        var totalBranchesResponse = response;
        $('#campaignBranch').empty();
        $('#campaignBranch').append($('<option></option>').val('').html('--Select--'));
        if (totalBranchesResponse == -1 || totalBranchesResponse.status == false)
        {
//            notify('Something went wrong', 'error', 10);
        } else
        {
            var totalBranchesData = totalBranchesResponse.data;
            $.each(totalBranchesData, function (key, value) {
                $('#campaignBranch').append($('<option></option>').val(value.id).html(value.name));

            });
        }
        if(branchId!=0)
        {
            $('#campaignBranch').val(branchId);
        }
        $('#campaignBranch').material_select();
    }

    function getCampaignGroupsDropdown(groupId=0) {
        var userID = $('#hdnUserId').val();
        var campaignBranch = $('#campaignBranch').val();
        var ajaxurl = API_URL + 'index.php/Campaign/getCampaignGroupByBranchId';
        var params = {branchId: 1};
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
            getCampaignGroupsResponse(response,groupId)
        }});
    }
    function getCampaignGroupsResponse(response,groupId)
    {
        $('#ddlCampaignGroups').empty();
        $('#ddlCampaignGroups').append($('<option></option>').val('').html('--Select All--'));
        if (response.status == true) {

            if (response.data.length > 0)
            {
                $.each(response.data, function (key, value)
                {
                    $('#ddlCampaignGroups').append($('<option></option>').val(value.group_id).html(value.name));
                });
            }
        }
        if(groupId !=0)
        {
            $("select[name=ddlCampaignGroups]").val(groupId);
            getGroupContactsList();
        }
        $('#ddlCampaignGroups').material_select();
        multiSelectDropdown();
    }
    $('#ddlCampaignGroups').change(function () {
        multiSelectDropdown($(this));
        getGroupContactsList();
    });
    function getGroupContactsList()
    {
        if($('#ddlCampaignGroups').val() == null || $('#ddlCampaignGroups').val() == '')
        {
            $("#tagContactsList tbody").html('');
        }
        else
        {
            var userID = $('#hdnUserId').val();
            var ajaxurl = API_URL + 'index.php/Campaign/getContactCountByGroupIds';
            var params = {groupIds: $('#ddlCampaignGroups').val()};
            var action = 'add';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getContactsByTagsResponse});
        }
    }
    function getContactsByTagsResponse(ContactResponse)
    {

        if (ContactResponse == -1 || ContactResponse.status == false)
        {
            alertify.dismissAll();
            notify('Something went wrong. Please try again', 'error', 5);
        }
        else
        {
            $("#tagContactsList tbody").html('');
            var contactTagData=ContactResponse.data;
            var tagData='';
            if(contactTagData.length == 0)
            {
                alertify.dismissAll();
                notify('No contacts found for selected group', 'warning', 5);
            }
            else
            {
                for(var i=0;i<contactTagData.length;i++)
                {
                    tagData='<tr>';
                    tagData+='<td>'+contactTagData[i]['groupName']+'</td>';
                    tagData+='<td>'+contactTagData[i]['contacts']+'</td>';
                    tagData+='</tr>';
                    $("#tagContactsList tbody").append(tagData);
                }
            }

            //$("#tagContactsList tbody").html(tagData);
        }
    }
    function saveBasicDetails()
    {
        $('#campaign_basic_form span.required-msg').remove();
        $('#campaign_basic_form input').removeClass("required");
        $('#campaign_basic_form select').removeClass("required");
        var flag = 0;
        var campaignName = $('#campaign_name').val();
        var campaignBranch = $('#campaignBranch').val();
        var campaignStart = $('#txtCampaignStartDate').val();
        var campaignEnd = $('#txtCampaignEndDate').val();
        var CampaignStartDate = dateConvertion(campaignStart);
        var CampaignEndDate = dateConvertion(campaignEnd);
        if (campaignName == '' || $.trim(campaignName) == '')
        {
            $('#campaign_name').val('');
            $("#campaign_name").addClass("required");
            $("#campaign_name").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (campaignBranch == '')
        {
            $("#campaignBranch").parent().find('.select-dropdown').addClass("required");
            $("#campaignBranch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if(campaignStart=='')
        {
            $("#txtCampaignStartDate").addClass("required");
            $("#txtCampaignStartDate").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if(campaignEnd=='')
        {
            $("#txtCampaignEndDate").addClass("required");
            $("#txtCampaignEndDate").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if( (campaignStart!='' && campaignEnd != '') && (CampaignStartDate > CampaignEndDate))
        {
            $("#txtCampaignEndDate").addClass("required");
            $("#txtCampaignEndDate").after('<span class="required-msg">Cannot be less than Start Date</span>');
            flag = 1;
        }
        if(flag==0)
        {
/*
            var fromDate = dateConvertion(campaignStart);
            var toDate = dateConvertion(campaignEnd);
            if (new Date(fromDate).getTime() > new Date(toDate).getTime()) {
                $("#txtCampaignStartDate").addClass("required");
                $("#txtCampaignStartDate").after('<span class="required-msg">Invalid</span>');
                $("#txtCampaignEndDate").addClass("required");
                $("#txtCampaignEndDate").after('<span class="required-msg">Invalid</span>');
            }
            else
*/
            {
                $("#campaign_basic_form input").prop("disabled", true);
                $("#campaign_basic_form textarea").prop("disabled", true);
                $("#campaign_template input").prop("disabled", false);
                getCampaignGroupsDropdown();
                $(".mCustomScrollbar").mCustomScrollbar("scrollTo","#step2");
            }
        }
    }
    function uploadCampaignTemplate(This){
        $('#camp_template_name').val('');
        $('#camp_template_path').val('');
        $('#ifrm_camp_template').attr('src','');
        $('#ifrm_camp_template').hide();
        $('#preview_temp').show();
        var ajaxurl=API_URL+'index.php/Campaign/uploadCampaignTemplate';
        var userID=$('#hdnUserId').val();
        var action='list';
        var fileUpload = document.getElementById("campTemplate");
        var selectedFile = $("#campTemplate").val();
        var extension = selectedFile.split('.');
        if (extension[1] != "html") {
            $("#campTemplate").focus();
            notify('Please choose a .html file', 'error', 5);
            return false;
        }
        else{
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

            if (fileUpload.value != null) {
                var uploadFile = new FormData();
                var files = $("#campTemplate").get(0).files;
                if (files.length > 0) {
                    uploadFile.append("file", files[0]);
                    //commonAjaxCall({This:This,method:'POST',requestUrl:ajaxurl,params:uploadFile,headerParams:headerParams,action:action,onSuccess:finalresp});

                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        dataType: "json",
                        data: uploadFile,
                        contentType: false,
                        processData: false,
                        headers: headerParams,
                        beforeSend:function(){
                            $(This).attr("disabled","disabled");
                        },success: function (response){
                            $(This).removeAttr("disabled");
                            var templateUrl = response.data.template_path;
                            var fileName = response.data.fileName;
                            $('#camp_template_name').val(fileName);
                            $('#camp_template_path').val(templateUrl);
                            $('#preview_temp').hide();
                            $('#ifrm_camp_template').attr('src',templateUrl);
                            $('#ifrm_camp_template').show();
                            $("#hdnTemplatePath").val(fileName);

                        },error:function(response){
                            alertify.dismissAll();
                            if(response.responseJSON.message=="Access Token Expired"){
                                logout();
                            }
                            else if(response.responseJSON.message=="Invalid credentials"){
                                logout();
                            }
                            else{
                                notify('Something went wrong. Please try again', 'error', 10);
                            }
                        }
                    });
                }
                else
                {
                    $("#campTemplate").parent().find('.file-path-wrapper').addClass("required");
                    $("#campTemplate").parent().find('.file-path-wrapper').after('<span class="required-msg">required field</span>');
                }
            }
        }
    }
    $('#campTemplate').change(function(){
        uploadCampaignTemplate(this);
    });
    function sendTestTemplate(This)
    {
        $('#campaign_template span.required-msg').remove();
        $('#campaign_template input').removeClass("required");
        var testEmail = $('#template_email').val();
        var regx_txtEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var templatePath =  $('#camp_template_path').val();
        var fileName = $('#camp_template_name').val();
        var flag = 0;
        if (testEmail == '')
        {
            $("#template_email").addClass("required");
            $("#template_email").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (testEmail!='' && regx_txtEmail.test($('#template_email').val()) === false) {
            $("#template_email").addClass("required");
            $("#template_email").after('<span class="required-msg">Invalid Email</span>');
            flag = 1;
        }
        if(templatePath == '')
        {
            $("#campTemplate").parent().find('.file-path-wrapper').addClass("required");
            $("#campTemplate").parent().find('.file-path-wrapper').after('<span class="required-msg">Please upload a template</span>');
            flag = 1;
        }
        if(flag == 1)
        {
            return false;
        }
        else
        {
            notify('Processing....', 'warning', 10);
            var ajaxurl=API_URL+'index.php/Campaign/sendTestEmailTemplate';
            var userID=$('#hdnUserId').val();
            var action='list';
            var params = {emailId: testEmail,templatePath:templatePath,fileName:fileName};
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: TestMailResponse});
        }
    }
    function TestMailResponse(response)
    {
        alertify.dismissAll();
        if (response == -1 || response.status == false)
        {
            notify('Something went wrong. Please try again', 'error', 10);
        }
        else
        {
            notify('Mail sent successfully', 'success', 10);
        }
        $('#template_email').val('');
        $('#campaign_template label').removeClass("active");
        $('#campaign_template span.required-msg').remove();
        $('#campaign_template input').removeClass("required");
    }
    function saveCampaign(This)
    {
        alertify.dismissAll();
        var campaignName = $('#campaign_name').val();
        var campaignBranch = $('#campaignBranch').val();
        var campaignStart = $('#txtCampaignStartDate').val();
        var campaignEnd = $('#txtCampaignEndDate').val();
        var campaignDescription = $('#txaCampaignDescription').val();
        var selectedGroups = $('#ddlCampaignGroups').val();
        var campaignID = $('#hdnCampaignId').val();

        var templatePath;
        if(campaignID == 0)
            templatePath = $('#camp_template_name').val();
        else
            templatePath = $('#hdnTemplatePath').val();

        var ajaxurl=API_URL+'index.php/Campaign/saveCampaign';
        var userID=$('#hdnUserId').val();
        var action='list';
        notify('Processing....', 'warning', 10);
        var params = {campaignId:campaignID,campaignName:campaignName,campaignBranch:campaignBranch,campaignStart:campaignStart,campaignEnd:campaignEnd,campaignDescription:campaignDescription,templatePath:templatePath,selectedGroups:selectedGroups,userID:userID};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: SaveCampaignResponse});
    }
    function SaveCampaignResponse(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            $.each(response.data, function (i, item) {
                alertify.dismissAll();
                notify(item, 'error', 10);
                if (response.data['campaignName'])
                {
                    $("#campaign_name").addClass("required");
                    $("#campaign_name").after('<span class="required-msg">' + response.data['campaignName'] + '</span>');
                    $("#campaign_name").prop("disabled", false);
                }
                if (response.data['campaignBranch'])
                {
                    $("#campaignBranch").addClass("required");
                    $("#campaignBranch").after('<span class="required-msg">' + response.data['campaignBranch'] + '</span>');
                }
                if (response.data['campaignStart'])
                {
                    $("#txtCampaignStartDate").addClass("required");
                    $("#txtCampaignStartDate").after('<span class="required-msg">' + response.data['campaignStart'] + '</span>');
                }
                if (response.data['campaignEnd'])
                {
                    $("#txtCampaignEndDate").addClass("required");
                    $("#txtCampaignEndDate").after('<span class="required-msg">' + response.data['campaignEnd'] + '</span>');
                }
                return false;
            });
        } else
        {
            alertify.dismissAll();
            notify(response.message, 'success', 10);
            window.location.href = APP_REDIRECT + "campaign";
            //$(this).attr("data-dismiss", "modal");
            //buildCampaignDataTable();
        }
    }

    function movetoStep3()
    {
        if($('#ddlCampaignGroups').val() == null || $('#ddlCampaignGroups').val() == '')
        {
            $("#ddlCampaignGroups").parent().find('.select-dropdown').addClass("required");
            $("#ddlCampaignGroups").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        }
        else
        {
            $('#step2 select,#step2 input').removeClass("required");
            $('#step2 span.required-msg').remove();
            $(".mCustomScrollbar").mCustomScrollbar("scrollTo","#step3");
        }

    }
    function getCampaignDetails()
    {
        var userID = $('#hdnUserId').val();
        var campaignID = $('#hdnCampaignId').val();
        var ajaxurl = API_URL + 'index.php/Campaign/getCampaignDetails';
        var params = {campaignId:campaignID};
        var headerParams = {context: 'default', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: CampaignDetailsResponse});
    }
    function CampaignDetailsResponse(response)
    {
        if(response.status == true)
        {
            var campaignData=response.data[0];
            $("#campaign_name").val(campaignData['name']);
            $("#hdnTemplatePath").val(campaignData['template_path']);
            $('#camp_template_path').val(API_URL+campaignData['template_path']);
            getCampaignBranches(this,campaignData['fk_branch_id']);
            setDatePicker('#txtCampaignStartDate', campaignData['start_date']);
            setDatePicker('#txtCampaignEndDate', campaignData['end_date']);
            $("#txaCampaignDescription").val(campaignData['description']);
            $('#ifrm_camp_template').attr('src',API_URL+campaignData['template_path']);
            $('#preview_temp').hide();
            var gData=campaignData['fk_group_id'].split(", ");
            var groupIds = [];
            for(var i=0;i<gData.length;i++)
                groupIds[i]=gData[i];
            getCampaignGroupsDropdown(groupIds);
            Materialize.updateTextFields();

            if(campaignData['is_editable']==1){

            }
            else {
                $('#contentBody input').attr('disabled', true);
                $('#contentBody select').attr('disabled', true);
                $('#contentBody textarea').attr('disabled', true);
                $('#contentBody button').attr('disabled', true);
                $('#btnCancel').attr('disabled', false);
                alertify.dismissAll();
                notify('This campaign cannot be updated','error',5);
                setTimeout(function () {
                    gotocampaign()
                }, 5000);
            }


        }
        if(response.status===false){
            alertify.dismissAll();
            notify(response.message,'error',5);
            setTimeout(function () {
                gotocampaign()
            }, 5000);

        }
    }
    function gotocampaign()
    {
        window.location.href = APP_REDIRECT + "campaign";
    }
    function showPreviewCampaignTemplate(This){

        if($('#camp_template_path').val().trim()!='') {
            $('#ifrm_camp_template').attr('src', $('#camp_template_path').val());
            $('#ifrm_camp_template').show();
            $('#preview_temp').hide();

            var strs=$('#hdnTemplatePath').val().replace(/\//g, "~");
            strs=strs.replace(/\./g, "^");
            var templateURL = APP_REDIRECT + "campaign_template_preview/"+strs;
            window.open(
                templateURL,
                '_blank' // <- This is what makes it open in a new window.
            );
        }
        else{
            alertify.dismissAll();
            notify('Please upload the template','error',10);
        }
    }
    function isElementExistInArray(Element,ElementArray){
        if ($.inArray(Element, ElementArray) != -1)
        {
            return true;
        }
        else{
            return false;
        }
    }
</script>