<?php
/**
 * Created by PhpStorm.
 * User: JAGADEESH.T
 * Date: 16/2/16
 * Time: 6:21 PM
 */
?>
<div class="no-access" style="position: absolute; top:20%;left:50%;width:392px;margin-left:-196px;box-shadow: 0px 10px 20px #888888;">
    <div class="text-center display-block" style="padding: 30px 10px 10px;font-size: 36px;font-weight: bold;background: #4c82bd;color: #fff;word-wrap: break-word;">No Access Assigned</div>
    <div style="height: 300px;text-align: center;background: #e6e6e6;"><img src="<?php echo BASE_URL;?>assets/images/no-access3.png" style="margin-top: 60px;"></div>
    <img src="<?php echo BASE_URL;?>assets/images/stick-pin.png" style="position: absolute;top:-30px;margin-left: -30px;left: 50%;">
</div>