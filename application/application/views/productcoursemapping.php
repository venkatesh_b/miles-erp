<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-11
 * Time: 11:22 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnProductMapId" value="0" />
    <div class="content-header-wrap">
        <h3 class="content-header">Product Course Mapping</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="viewsidePanel" data-target="#myProduct-mapping" data-toggle="modal" onclick="manageProductMapping(this,0)"><i class="icon-plus-circle" ></i></a></li>
                <li><a></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">

            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="productCourseMapping-list">
                    <thead>
                    <tr>
                        <th>Course Name</th>
                        <!--<th class="no-sort"></th>-->
                        <th>Products Mapped</th>
                        <th>Code</th>
                        <th>Type</th>
                        <th>Qty.</th>
                        <th>Publisher</th>
                        <th>Added on</th>
                    </tr>

                    </thead>
                </table>
            </div>
        </div>
        <!--Modal1-->
        <div class="modal fade" id="myProduct-mapping" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="700">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Create Course Product Mapping</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <form name="courseForm" id="courseForm">
                        <div class="col-sm-12">
                            <div class="input-field">
                                <select id="course" name="course">
                                </select>
                                <label class="select-label">Course <em>*</em></label>
                            </div>
                        </div>
                        </form>
                        <form name="productMappingForm" id="productMappingForm">
                            <!--<div class="col-sm-12">
                                <div class="input-field">
                                    <input id="product_code" name="product_code" type="text" class="validate">
                                    <label for="product_code">Product Code <em>*</em></label>
                                </div>
                            </div>-->
                        <div class="col-sm-12">
                            <div class="col-sm-11 p0 addNewProduct">
                                <div class="col-sm-4 pl0">
                                <div class="input-field">
                                    <input id="txtProductCode" name="txtProductCode" type="text" class="validate" />
                                    <label for="txtProductCode" >Product Code <em>*</em></label>
                                </div>
                            </div>
                                <div class="col-sm-4 pl0">
                                <div class="input-field">
                                    <input id="txtProductName" name="txtProductName" type="text" class="validate" disabled/>
                                    <label for="txtProductName">Product Name </label>
                                </div>
                            </div>
                                <div class="col-sm-4 pl0">
                                <div class="input-field">
                                    <input id="txtProductQuantity" name="txtProductQuantity" type="text" class="validate" maxlength="4" onkeypress="return isNumberKey(event)" />
                                    <label for="txtProductQuantity" >Quantity <em>*</em></label>
                                </div>
                            </div>
                            </div>
                            <div class="col-sm-1 pl0">
                                <a class="m10 ml0 display-inline-block cursor-pointer" onclick="addNewProduct()"> <i class="icon-plus-circle f24 sky-blue"></i> </a>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="product-map-table p0">
                                <table class="table table-responsive table-bordered mb0" id="products-list">
                                    <thead>
                                    <tr>
                                        <th>Product Code</th>
                                        <th>Product Name</th>
                                        <th class="border-r-none">Quantity</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                            </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" id="product_mapping" class="btn blue-btn" onclick="saveProductMapping(this)"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>

                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script>
    docReady(function(){
        buildProductCourseDataTable();
    });
    var rowNum=0;
    var productMappings = new Object();
    function buildProductCourseDataTable()
    {
        var ajaxurl=API_URL+'index.php/ProductCourseMapping/getProductCourseMappings';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#productCourseMapping-list").dataTable().fnDestroy();
        $tableobj=$('#productCourseMapping-list').DataTable( {
            "fnDrawCallback": function()
            {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
                var productcourse_classes='',productcourse='',flag= 0,td_flag=0,td_productcourse='',td_custom_productcourse='';
                $('#productCourseMapping-list div.product_course').each(function()
                {
                    $(this).parent().parent().addClass('rowspan-group-tr');
                    productcourse_classes=($(this).attr("class")).split(" ");
                    productcourse = productcourse_classes[productcourse_classes.length-1];
                    if(flag == 0)
                        td_productcourse=productcourse;
                    if(productcourse == td_productcourse)
                    {
                        //td_flag++;
                    }
                    else
                    {
                        td_flag++;
                        td_productcourse=productcourse;
                    }
                    flag++;
                    td_custom_productcourse=productcourse+'_'+td_flag;
                    $(this).parent().attr("data-productcourse",td_custom_productcourse);

                    $(this).parent().addClass('rowspan-edit');
                    $(this).parent().addClass(td_custom_productcourse);
                    $(this).parent().addClass(productcourse);
                });
                MergeProductCourses();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No Mapping found",
                "sZeroRecords": "No Mapping found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    data: null,className:"product_course", render: function ( data, type, row )
                    {
                        var courseData = '';
                        courseData += '<div class="dropdown product-map-edit product_course '+data.course_name+'"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v f18 pr10"></i> </a>';
                        courseData += '<ul class="dropdown-menu pull-right product-map-menu" ><li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        courseData += '<li><a href="#" access-element="edit" data-target="#myMap-edit" data-toggle="modal" onclick=\'manageProductMapping(this,"'+data.courseId+'")\'>Edit</a></li>';
                        courseData += '<li><a access-element="delete" href="javascript:;" onclick=\'deleteProductMapping(this,"'+data.courseId+'")\'>Delete</a></li></ul></div>';
                        courseData += '<div class="mt5"> <a href="#" class="anchor-blue">'+data.course_name+'</a> </div>';
                        courseData += '<div class=" mt2"><label class="label-text">Last Modified</label><p class="label-data">'+data.updated_date+'</p></div>';
                        courseData += '<div class="mt2"><label class="label-text">Total Products</label><p class="label-data">'+data.product_count+'</p></div>';
                        //return data.course_name+' | '+data.product_count+' | '+data.updated_date;
                        return courseData;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.product_name;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.code;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.product_type;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.quantity;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.publication;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return (data.created_date == null || data.created_date == '') ? "----":data.created_date;
                    }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function manageProductMapping(This,Id)
    {
        notify('Processing..', 'warning', 10);
        $('#product_mapping').html('');
        if(Id==0)
        {
            $('#product_mapping').html('<i class="icon-right mr8"></i>Add');
        }
        else
        {
            $('#product_mapping').html('<i class="icon-right mr8"></i>Update');
        }
        $('#courseForm')[0].reset();
        $('#courseForm span.required-msg').remove();
        $('#courseForm input').removeClass("required");
        $("#courseForm").removeAttr("disabled");
        $('#productMappingForm')[0].reset();
        Materialize.updateTextFields();
        $('#productMappingForm span.required-msg').remove();
        $('#productMappingForm input').removeClass("required");
        $("#productMappingForm").removeAttr("disabled");
        $('#products-list tbody').html('');
        $("#course").attr('disabled',false);
        productMappings =new Object();
        rowNum =0;
        var userID = $("#hdnUserId").val();
        if (Id == 0)
        {
            $("#myModalLabel").html("Create Product Course Mapping");
            getCourses();
        }
        else
        {
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            $.when(
                commonAjaxCall
                (
                    {
                        This: This,
                        headerParams: headerParams,
                        params: {},
                        requestUrl: API_URL + 'index.php/Courses/getAllCourseList',
                        action: 'edit'
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: This,
                        headerParams: headerParams,
                        params: {productCourseId: Id},
                        requestUrl: API_URL + 'index.php/ProductCourseMapping/getProductMappingDetails',
                        action: 'edit'
                    }
                )
            ).then(function (response1, response2)
            {
                getCoursesResponse(response1[0]);
                ManageProductMapResponse(response2[0]);
                var mappingCourseName = response2[0].data.name;
                //$("#myModalLabel").html("Edit "+mappingCourseName+" Product Mapping");
                $("#myModalLabel").html(mappingCourseName+" Product Mapping");
                $("#product_mapping").html("<i class=\"icon-right mr8\"></i>Update");
            });
        }
        $("#hdnProductMapId").val(Id);
        $(This).attr("data-target", "#myProduct-mapping");
        $(This).attr("data-toggle", "modal");
    }
    function addNewProduct()
    {
        var productCode = $("#txtProductCode").val();
        var productName = $("#txtProductName").val().trim();
        var quantity = $("#txtProductQuantity").val();
        $('#productMappingForm span.required-msg').remove();
        $('#productMappingForm input').removeClass("required");
        var flag = 0;
        var regx_productName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
        if (productCode == '')
        {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_productName.test($('#txtProductCode').val()) === false) {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (productCode != '' && productName == '')
        {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (quantity == '')
        {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (quantity <= 0) {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">Should be >0</span>');
            flag = 1;
        }
        if (flag == 1) {
            return false;
        } else {
            var products = "";
            if(typeof productMappings[productCode]!='undefined')
            {
                $("#txtProductCode").addClass("required");
                $("#txtProductCode").after('<span class="required-msg">Product already added</span>');
            }
            else
            {
                var obj = new Object();
                obj.quantity=quantity;
                productMappings[productCode] = obj;
                products +='<tr id=rownum'+rowNum+'><td>'+productCode+'</td>';
                products +='<td>'+productName+'</td>';
                products +='<td><div class="col-sm-11 p0">'+quantity+'</div><div class="col-sm-1 p0"> <span class="filter-selectall-wrap pull-right"><a href="#" class="reportFilter-unselectall mr0" onclick="removeProduct('+rowNum+',\''+productCode+'\')"><i class="icon-times red"></i></a></span> </div></td></tr>';
                $('#products-list tbody').append(products);
                $('#txtProductCode').val('');
                $('#txtProductName').val('');
                $('#txtProductQuantity').val('');
                $(".addNewProduct label").removeClass("active");
                rowNum = rowNum+1;
            }
        }
    }
    function removeProduct(rowNum,pcode)
    {
        delete productMappings[pcode];
        $('#rownum'+rowNum).remove();
    }
    function getCourses()
    {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Courses/getAllCourseList';
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: getCoursesResponse});
    }
    function getCoursesResponse(response)
    {
        $('#course').empty();
        $('#course').append($('<option disabled selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#course').append($('<option></option>').val(value.courseId).html(value.name));
                });
            }
        }
        $('#course').material_select();
    }
    function ManageProductMapResponse(response)
    {
        alertify.dismissAll();
        var details = response;
        if (details == -1 || details['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var productData = details.data;
            var products = '';
            $('#course').val(productData.courseId);
            $("#course").next('label').addClass("active");
            $("#course").attr('disabled',true);
            $('#course').material_select();
            $.each(productData.mappings, function (key, value) {
                var obj = new Object();
                obj.quantity= value.quantity;
                productMappings[value.code] = obj;
                products +='<tr id=rownum'+rowNum+'><td>'+value.code+'</td>';
                products +='<td>'+value.product_name+'</td>';
                products +='<td><div class="col-sm-11 p0">'+value.quantity+'</div><div class="col-sm-1 p0"> <span class="filter-selectall-wrap pull-right"><a href="#" class="reportFilter-unselectall mr0" onclick="removeProduct('+rowNum+',\''+value.code+'\')"><i class="icon-times red"></i></a></span> </div></td></tr>';
                rowNum = rowNum+1;
            });
            $('#products-list tbody').append(products);
        }
    }
    function saveProductMapping(This,Id)
    {
        var courseName = $('#course').val();
        var flag = 0;
        var productMapId = $('#hdnProductMapId').val();
        var userID=$('#hdnUserId').val();
        var finalMappings = [];
        $.each(productMappings, function (i, item)
        {
            finalMappings.push(i+'||'+item.quantity);
        });
        finalMappings= finalMappings.join(',');
        if(courseName =='' || courseName==null)
        {
            $("#course").parent().find('.select-dropdown').addClass("required");
            $("#course").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        else if(finalMappings == '')//products mapped
        {
            notify('No products mapped','error', 10);
            flag=1;
        }
        if(flag == 0)
        {
            notify('Processing..', 'warning', 10);
            if(productMapId == 0){
                var ajaxurl = API_URL + 'index.php/ProductCourseMapping/addProductCourseMapping';
                var params = {userID: userID, courseId: courseName, productMappings : finalMappings};
                var type = "POST";
                var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveProductMappingResponse});
            }
            else {
                var ajaxurl = API_URL + 'index.php/ProductCourseMapping/updateProductCourseMapping';
                var params = {userID: userID,ProductMapId :productMapId , courseId: courseName, productMappings : finalMappings};
                var type = "POST";
                var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveProductMappingResponse});
            }
        }
        else {
           return false;
       }
    }
    function SaveProductMappingResponse(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
           // buildProductCourseDataTable();
            $('#myProduct-mapping').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }
        buildProductCourseDataTable();
    }
    function deleteProductMapping(This,Id)
    {
        var ModelTitle="Mapping Status";
        var Description="Are you sure want to delete this product mapping ?";
        $("#popup_confirm #btnTrue").attr('onClick','deleteProductMappingFinal(this,"'+Id+'")');
        customConfirmAlert(ModelTitle,Description);
    }
    function deleteProductMappingFinal(This,courseId){
        var userID=$('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/ProductCourseMapping/deleteProductMapping';
        var params = {courseId: courseId};
        var action = 'delete';
        var type="POST";
        var headerParams = {
            action: action,
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: userID
        };
        commonAjaxCall({
            This: This,
            method:'POST',
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: action,
            onSuccess: ChangeProductMappingResponse
        });
    }
    function ChangeProductMappingResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            notify(response.message,'success',10);
            $('#popup_confirm').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildProductCourseDataTable();
        }
        else{
            notify(response.message,'error',10);
        }

    }
    function MergeProductCourses()
    {
        var table_td_text='',flag= 0,td_flag=0,counter = 0,total_records=$('table#productCourseMapping-list tr td.product_course').length,td_indexflag=0;
        $('table#productCourseMapping-list tr td.product_course').each(function()
        {
            //var td_text = $(this).text();
            var td_text = $(this).attr("data-productcourse");
            console
            if(flag == 0)
                table_td_text=td_text;
            if(td_text == table_td_text)
            {
                if(td_indexflag == 0)
                    td_indexflag=flag;
                td_flag=0;
                counter++;
            }
            else
            {
                td_flag=1;
            }
            flag++;
            if(flag == total_records || td_flag == 1)
            {
                $('table#productCourseMapping-list tr td.'+ $.trim(table_td_text)).prop('rowspan',counter);
                $('table#productCourseMapping-list tr td.'+$.trim(table_td_text)+':not(:first)').remove();
                table_td_text=td_text;
                counter=1;
                td_indexflag=0;
            }
        });
    }
    $(function(){
        var headerParams = {
            action: 'add',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $("#txtProductCode").jsonSuggest({
            onSelect:function(item){
                $("#txtProductCode").val(item.id);
                var product = item.text;
                var productName = product.split(' | ');
                $("#txtProductName").val(productName[1]);
                $("#txtProductName").next("label").addClass("active");
            },
            headers:headerParams,url:API_URL + 'index.php/ProductCourseMapping/getProductsInformation' , minCharacters: 2});
    });
</script>
