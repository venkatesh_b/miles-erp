<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-09
 * Time: 02:45 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Products</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="viewsidePanel tooltipped" data-position="top" data-target="#myProduct" data-toggle="modal" data-tooltip="Add Product" onclick="manageProducts(this,0)" ><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="product-list">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Product Type</th>
                        <th>Product Code</th>
                        <th>Publications</th>
                        <th class="no-sort">Version</th>
                        <!--<th class="no-sort">Description</th>-->
                        <th class="no-sort"></th>
                    </tr>

                    </thead>

                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="myProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageProductForm" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Create Product</h4>
                    </div>
                        <input type="hidden" id="hdnProductId" value="0" />
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12">
                            <div class="input-field">
                                <input id="product_name" name="product_name" type="text" class="validate">
                                <label for="product_name">Product Name <em>*</em></label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <select id="product_type" name="product_type" >
                                </select>
                                <label class="select-label">Product Type <em>*</em></label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <input id="product_code" name="product_code" type="text" class="validate">
                                <label for="product_code">Product Code <em>*</em></label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input-field">
                                <input id="version" name="version" type="text" class="validate">
                                <label for="version">Version</label>
                            </div>
                        </div>

                        <!--<div class="col-sm-12">
                            <div class="input-field">
                                <select id="publications" name="publications">
                                </select>
                                <label for="Publications">Publications</label>
                            </div>
                        </div>-->
                        <div class="col-sm-12" id="contactType-source">

                            <div class="input-field col-sm-11 p0">
                                <select class="custom-select-nomargin" id="publications" name="publications">
                                </select>
                                <label class="select-label">Publications <em>*</em></label>
                            </div>

                            <div class="col-sm-1 p0" access-element="add reference">
                                <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetPublicationWrapper(this)" href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                            </div>

                            <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="PublicationWrapper">
                                <div class="col-sm-8 p0 ">
                                    <div class="input-field mt15">
                                        <input id="txtPublication" type="text" class="validate" value="" maxlength="250">
                                        <label for="txtPublication">Publication <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-4 p0 ">
                                    <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="addPublicationWrapper(this)"></span></a>
                                    <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                </div>
                            </div>


                        </div>



                        <div class="col-sm-12 mt20">
                            <div class="input-field">
                                <textarea id="description" name="description" class="materialize-textarea"></textarea>
                                <label for="description">Description</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="product_button" class="btn blue-btn" onclick="saveProduct(this)"><i class="icon-right mr8"></i>Create Product</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildProductsDataTable();
    });

    function buildProductsDataTable()
    {
        var ajaxurl=API_URL+'index.php/Product/products';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#product-list").dataTable().fnDestroy();
        $tableobj=$('#product-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No products found",
                "sZeroRecords": "No products found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.name;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.product_type;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.code;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.publication;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return (data.version == null || data.version == '') ? "----":data.version;
                }
                },
              /*  {
                    data: null, render: function ( data, type, row )
                    {
                        return (data.description == null || data.description == '') ? "----":data.description;
                    }
                },*/
                {
                    data: null, render: function ( data, type, row )
                    {
                        var productData='';
                        productData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        productData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                        productData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                        productData+='</a>';
                        productData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        productData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        productData+='<li><a access-element="edit" href="javascript:;" data-target="#myProduct" data-toggle="modal" onclick=\'manageProducts(this,"'+data.product_id+'")\'>Edit</a></li>';
                        productData+='<li><a access-element="delete" href="javascript:;" onclick=\'deleteProduct(this,"'+data.product_id+'")\'>Delete</a></li>';
                        productData+='</ul>';
                        productData+='</div>';
                        return productData;
                    }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function deleteProduct(This,productId)
    {
        var ModelTitle="Delete Product";
        var Description="Are you sure want to delete this product ?";
        $("#popup_confirm #btnTrue").attr('onClick','deleteProductFinal(this,"'+productId+'")');
        customConfirmAlert(ModelTitle,Description);
    }
    function deleteProductFinal(This,productId){
        var userID=$('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Product/deleteProduct';
        var params = {productId: productId};
        var action = 'delete';
        var type="POST";
        var headerParams = {
            action: action,
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: userID
        };
        commonAjaxCall({
            This: This,
            method:'POST',
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: action,
            onSuccess: ChangeProductResponse
        });
    }
    function ChangeProductResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            notify(response.message,'success',10);
            $('#popup_confirm').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildProductsDataTable();
        }
        else{
            notify(response.message,'error',10);
        }

    }

</script>
