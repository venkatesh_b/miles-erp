<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->

    <div class="content-header-wrap">
        <h3 class="content-header">Qualification </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Add Qualification" class="viewsidePanel" onclick="getQualificationDetails(this,'')" href="javascript:;"  ><i class="icon-plus-circle" ></i></a></li>
                <!--<li><a></a></li>-->
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" > <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0" access-element="list">
                <table class="table table-responsive table-striped table-custom table-info" id="qualifications-list">
                    <thead>
                    <tr>
                        <th>Qualification</th>
                        <th>Post Qualification</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="qualificationsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeQualificationModal(this)"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="countryModalLabel">Create Qualification</h4>
                    </div>
                    <form action="/" method="post" id="qualificationForm" novalidate="novalidate">
                        <input type="hidden" id="reference_type_val_id" name="reference_type_val_id" value=""/>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="reference_type_val_parent" name="reference_type_val_parent" class="validate" class="formSubmit">

                                    </select>
                                    <label class="select-label">Qualification Level <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="reference_type_val" type="text" name="reference_type_val" class="validate" class="formSubmit" required>
                                    <label for="reference_type_val">Qualification Name <em>*</em></label>
                                </div>
                            </div>

                        </div>

                    </form>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="actionButton" type="submit" class="btn blue-btn" onclick="AddQualification(this)"><i class="icon-right mr8"></i>Add</button>
                        <button type="button" class="btn blue-light-btn" onclick="closeQualificationModal(this)"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>

<script type="text/javascript">

    docReady(function(){
        buildQualificationsReferenceDataTable();
        getPostQualificationsDropdown();
    });

    function buildQualificationsReferenceDataTable()
    {
        var userId = $('#userId').val();
        var ajaxurl=API_URL+'index.php/Referencevalues/getReferenceValuesList';
        var params = {'reference_type':'Qualification'};
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        $("#qualifications-list").dataTable().fnDestroy();
        $tableobj=$('#qualifications-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.qualification;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    //return data.country;
                    var referenceTypeData='<span class="p0">'+data.postqualification+'</span>';
                    referenceTypeData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                    referenceTypeData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    referenceTypeData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                    referenceTypeData+='</a>';
                    referenceTypeData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    referenceTypeData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    referenceTypeData+='<li><a access-element="edit" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick="getQualificationDetails(this,'+data.reference_type_value_id+')">Edit</a></li>';
                    referenceTypeData+='</ul>';
                    referenceTypeData+='</div>';
                    return referenceTypeData;

                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function getPostQualificationsDropdown() {
        /*Country*/
        var userId = $('#userId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Post Qualification'};
        /*var ajaxurl=API_URL+'index.php/Referencevalues/getAllReferenceValues';
         var params = {'reference_type_id':1};*/
        commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: postQualificationsList});
    }

    function postQualificationsList(response)
    {
        $('#reference_type_val_parent').empty();
        var oh = $('<option/>', {value: ''}).text('--Select--');
        oh.appendTo('#reference_type_val_parent');
        for (var b in response.data) {
            var oh = $('<option/>', {value: response.data[b]['reference_type_value_id'], "parent-id": response.data[b]['reference_type_value_id']})
                .text(response.data[b]['value']);
                oh.appendTo('#reference_type_val_parent');
        }
        $('#reference_type_val_parent').material_select();
    }
    function getQualificationDetails(This, id) {
        $('#qualificationForm')[0].reset();
        $('#reference_type_val_id').val('');
        $('#qualificationForm label').removeClass("active");
        $('#qualificationForm span.required-msg').remove();
        $('#qualificationForm input').removeClass("required");
        $("select[name=postqualification]").val(0);
        $('select').material_select();
        if(id==''){
            $('#actionButton').html('');
            $('#actionButton').html('<i class="icon-right mr8"></i>Add');
        } else {
            $('#actionButton').html('');
            $('#actionButton').html('<i class="icon-right mr8"></i>Update');
        }

        $('#qualificationForm .select-label').removeClass("active");
        $('#qualificationForm input').removeClass("required");
        if (id == 0) {
            $("#countryModalLabel").html("Create Qualification");
            $('#reference_type_val_id').val('');
            $('#qualificationsModal').modal('toggle');
        } else {
            notify('Processing..', 'warning', 10);
            $("#countryModalLabel").html("Edit Qualification");
            var userId = $('#userId').val();
            var ajaxurl = API_URL + "index.php/Referencevalues/getSingleReferenceValues";
            var params = {reference_type_value_id: id};
            var action = 'edit';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

            var response = commonAjaxCall({This: This, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: function (response) {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false)
                {
                } else
                {
                    $('#reference_type_val').val(response.data[0].value);
                    $("#reference_type_val").next('label').addClass("active");
                    $('#reference_type_val_parent').val(response.data[0].parent_id);
                    $('select').material_select();

                    $('#reference_type_val_id').val(id);

                    $('#qualificationsModal').modal('toggle');
                }
            }
            });
        }
    }

    function closeQualificationModal(This) {
        $('#qualificationForm')[0].reset();
        $('#reference_type_val_id').val('');
        $('#qualificationForm label').removeClass("active");
        $('#qualificationForm span.required-msg').remove();
        $('#qualificationForm input').removeClass("required");
        $("select[name=postqualification]").val(0);
        $('select').material_select();
        $('#qualificationsModal').modal('toggle');
        $("#countryModalLabel").html("Create Qualification");
    }
    function AddQualification(This) {
        $('#qualificationForm span.required-msg').remove();
        $('#qualificationForm input').removeClass("required");
        var id = $('#reference_type_val_id').val();
        var data = '';
        var url = '';
        var qualification = $('#reference_type_val').val().trim();
        var postqualification = $("select[name=reference_type_val_parent]").val().trim();
        var action = '';
        if (id != '')
        {
            data = {
                reference_type_id: 0,
                reference_type_val_id: id,
                reference_type_val: qualification,
                reference_type_val_parent: postqualification,
                reference_type_is_active: 1,
                reference_type:'Qualification'
            };
            url = API_URL + "index.php/Referencevalues/editReferenceValue";
            action = 'edit';
        } else {
            $("#countryModalLabel").html("Create Qualification");
            data = {
                reference_type_id: 0,
                reference_type_val: qualification,
                reference_type_val_parent: postqualification,
                reference_type_is_active: 1,
                reference_type:'Qualification'
            };
            url = API_URL + "index.php/Referencevalues/addReferenceValue";
            action = 'add';
        }

        var flag = 0;
        if (postqualification == '') {
            $("#reference_type_val_parent").parent().find('.select-dropdown').addClass("required");
            $("#reference_type_val_parent").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (qualification == '') {
            $("#reference_type_val").addClass("required");
            $("#reference_type_val").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (flag == 0) {
            notify('Processing..', 'warning', 50);
            var userId = $('#userId').val();
            var ajaxurl = url;
            var params = data;
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

            var response = commonAjaxCall({This: This, asyncType: true, headerParams: headerParams, method: 'POST', requestUrl: ajaxurl, params: params, action: action, onSuccess: function (response) {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false)
                {
                    var id = '';
                    for (var a in response.data) {
                        id = '#' + a;
                        if (a == 'Qualification') {
                            $(id).parent().find('.select-dropdown').addClass("required");
                            $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                        } else {
                            $(id).addClass("required");
                            $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                        }
                    }
                    $(This).removeAttr('disabled');
                } else
                {
                    buildQualificationsReferenceDataTable();
                    $('#qualificationForm')[0].reset();
                    $('#qualificationForm label').removeClass("active");
                    $('#qualificationsModal').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    notify('Data Saved Successfully', 'success', 10);
                }
            }
            });
        } else {
            return false;
        }
    }
</script>
