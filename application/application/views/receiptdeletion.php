<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Receipt Deletion Report</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="report"><a class="" href="javascript:" onclick="javascript:filterReceiptDeletionListReportExport(this)"><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Cash Counter / Branch</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="branchFilter"></div>
                    </div>
                    <div class="col-sm-6 reports-filter-wrapper mb5">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Company</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="companyFilter"></div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Receipt Mode</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="receiptmodeFilter">
                            <a class="active" data-id="Offline">Offline</a>
                            <a class="active" data-id="Online">Online</a>
                            <a class="active" data-id="Net Banking">Net Banking</a>
                        </div>

                    </div>
                    <div class="col-sm-6 reports-filter-wrapper">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Course</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="courseFilter"></div>
                    </div>
                </div>

            <div class="col-sm-12 p0">
                <div class="col-sm-6 reports-filter-wrapper pl0">
                    <div class="input-field col-sm-4 mt0 pl0">
                        <div class="input-field mt0">
                            <label class="datepicker-label datepicker">From Date </label>
                            <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtFromDate">
                        </div>
                    </div>
                    <div class="input-field col-sm-4 mt0 pl0">
                        <div class="input-field mt0">
                            <label class="datepicker-label datepicker">To Date </label>
                            <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtToDate">
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 reports-filter-wrapper">
                    <a class="btn blue-btn btn-sm" href="javascript:" onclick="javascript:filterReceiptDeletionReport(this)">Proceed</a>
                </div>
            </div>

            <div id="receiptdeletion-report-list-wrapper" access-element="report">
                <div class="col-sm-12 clearfix mt15 p0">
                    <table class="table table-inside table-custom branchwise-report-table" id="receiptdeletion-report-list">
                        <thead>
                        <tr class="">
                            <th>Enroll No.</th>
                            <th>Name</th>
                            <th>Receipt No.</th>
                            <th>Remark</th>
                            <th class="text-right">Amount</th>
                            <th>Username / Cashier</th>
                            <th>Deleted / Reversal By</th>
                            <th>Deleted / Reversal Date</th>
                        </tr>
                        </thead>

                    </table>
                </div>
                <div class="col-sm-12 clearfix p0">
                    <h4 class="mb30 heading-uppercase">Signatures</h4>
                    <div class="col-sm-4 mb30 pl0 ash">Accountant / Cashier</div>
                    <div class=" col-sm-4 ash">Branch Head</div>
                </div>
            </div>
        </div>

        <!-- InstanceEndEditable --></div>
</div>

<script>
    docReady(function () {
        $("#receiptdeletion-report-list-wrapper").hide();
        receiptDeletionListReportPageLoad();
        enableDatePicker();
    });

    /* Receipt Deletion list report starts here */
    function receiptDeletionListReportPageLoad()
    {
        var userId = $('#userId').val();
        var action = 'report';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

        var ajaxurl=API_URL+'index.php/ReceiptDeletion/getAllConfigurations';
        var params = {};
        commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
            var dashboard = '';
            //return false;
            if (response.data.length == 0 || response == -1 || response['status'] == false) {
            } else {
                var RawData = response.data;
                var html = '';
                if(RawData.branch.length > 0){
                    for(var a in RawData.branch){
                        html += '<a class="active" data-id="'+RawData.branch[a].id+'" href="javascript:;">'+RawData.branch[a].name+'</a>'
                    }
                    $('#branchFilter').html(html);
                }
                html = '';
                if(RawData.course.length > 0){
                    for(var a in RawData.course){
                        html += '<a class="active" data-id="'+RawData.course[a].courseId+'" href="javascript:;">'+RawData.course[a].name+'</a>'
                    }
                    $('#courseFilter').html(html);
                }
                html = '';
                if(RawData.company.length > 0){
                    for(var a in RawData.company){
                        html += '<a class="active" data-id="'+RawData.company[a].company_id+'" href="javascript:;">'+RawData.company[a].name+'</a>'
                    }
                    $('#companyFilter').html(html);
                }
                buildpopover();
            }
        }});
    }
    function filterReceiptDeletionReport(This){
        $("#txtFromDate").removeClass("required");
        $("#txtToDate").removeClass("required");
        $('#txtFromDate').parent().find('label').removeClass("active");
        $('#txtFromDate').parent().find('span.required-msg').remove();
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();
        var branchId = [];
        var courseId = [];
        var receiptmodeId = [];
        var companyId = [];
        var fromDate=$('#txtFromDate').val();
        var toDate=$('#txtToDate').val();
        if(fromDate.trim()!='') {
            var txtfromDate = dateConvertion(fromDate);
        }
        if(toDate.trim()!='') {
            var txttoDate = dateConvertion(toDate);
        }
        var error = true;
        $("#branchFilter > .active").each(function() {
            error = false;
            branchId.push($(this).attr('data-id'));
        });
        $("#companyFilter > .active").each(function() {
            error = false;
            companyId.push($(this).attr('data-id'));
        });
        $("#receiptmodeFilter > .active").each(function() {
            error = false;
            receiptmodeId.push("'"+$(this).attr('data-id')+"'");
        });
        $("#courseFilter > .active").each(function() {
            error = false;
            courseId.push($(this).attr('data-id'));
        });


        var branchId=branchId.join(', ');
        var companyId=companyId.join(', ');
        var receiptmodeId=receiptmodeId.join(', ');
        var courseId=courseId.join(', ');

        if(branchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one branch','error');
            return false;
        }
        else if(companyId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one company','error');
            return false;
        }
        else if(receiptmodeId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one receipt mode','error');
            return false;
        }
        else if(courseId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one course','error');
            return false;
        }
        else if($('#txtFromDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select from date','error');
            return false;

        }
        else if($('#txtToDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select to date','error');
            return false;

        }
        else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
            $("#txtFromDate").addClass("required");
            $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">Invalid</span>');
            alertify.dismissAll();
            notify('Invalid dates','error');
            error=true;
            return false;
        }
        else{
            error=false;
        }

        if(!error){
            var total=0;
            var pageTotal=0;

            var action = 'report';
            var ajaxurl=API_URL+'index.php/ReceiptDeletion/getReceiptDeletionListReport';
            var params = {'branch': branchId, 'course': courseId, 'receiptmode': receiptmodeId,company:companyId,fromDate:fromDate,toDate:toDate};
            var userID=$('#userId').val();
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            $("#receiptdeletion-report-list-wrapper").show();
            $("#receiptdeletion-report-list").dataTable().fnDestroy();
            $tableobj = $('#receiptdeletion-report-list').DataTable( {
                "fnDrawCallback": function() {
                    $("#receiptdeletion-report-list thead th").removeClass("icon-rupee");
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                    }
                    verifyAccess();
                    buildpopover();
                },
                "footerCallback": function ( row, data, start, end, display ) {
                    $('#receiptdeletion-report-list_wrapper div.DTHeader').html('Deleted / Cheque Reversals');
                },
                dom: "<'DTHeader'>Bfrtip",
                bInfo: false,
                "serverSide": false,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No Records Found",
                    "sZeroRecords": "No Records Found",
                    "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
                },
                "bProcessing": true,
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    data:params,
                    headers:headerParams,
                    error:function(response) {
                        DTResponseerror(response);
                    }
                },
                columns: [
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.enrollNo;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.leadName;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.receiptNo;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.remarks;
                    }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        return moneyFormat(data.amount);
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.userName;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.deletedBy;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.deletedOn;
                    }
                    }
                ]
            } );
            DTSearchOnKeyPressEnter();

        } else {
            alertify.dismissAll();
            notify('Select any filter', 'error', 10);
        }
    }
    function filterReceiptDeletionListReportExport(This){
        $("#txtFromDate").removeClass("required");
        $("#txtToDate").removeClass("required");
        $('#txtFromDate').parent().find('label').removeClass("active");
        $('#txtFromDate').parent().find('span.required-msg').remove();
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();
        var branchId = [];
        var branchIdText = [];
        var courseId = [];
        var courseIdText = [];
        var receiptmodeId = [];
        var receiptmodeIdText = [];
        var companyId = [];
        var companyIdText = [];
        var fromDate=$('#txtFromDate').val();
        var toDate=$('#txtToDate').val();
        if(fromDate.trim()!='') {
            var txtfromDate = dateConvertion(fromDate);
        }
        if(toDate.trim()!='') {
            var txttoDate = dateConvertion(toDate);
        }
        var error = true;
        $("#branchFilter > .active").each(function() {
            error = false;
            branchId.push($(this).attr('data-id'));
            branchIdText.push($(this).text());
        });
        $("#companyFilter > .active").each(function() {
            error = false;
            companyId.push($(this).attr('data-id'));
            companyIdText.push($(this).text());
        });
        $("#receiptmodeFilter > .active").each(function() {
            error = false;
            receiptmodeId.push("'"+$(this).attr('data-id')+"'");
            receiptmodeIdText.push($(this).text());
        });
        $("#courseFilter > .active").each(function() {
            error = false;
            courseId.push($(this).attr('data-id'));
            courseIdText.push($(this).text());
        });

        var branchId=branchId.join(', ');
        var companyId=companyId.join(', ');
        var receiptmodeId=receiptmodeId.join(', ');
        var courseId=courseId.join(', ');
        if(branchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one branch','error');
            return false;
        }
        else if(companyId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one company','error');
            return false;
        }
        else if(receiptmodeId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one receipt mode','error');
            return false;
        }
        else if(courseId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one course','error');
            return false;
        }
        else if($('#txtFromDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select from date','error');
            return false;

        }
        else if($('#txtToDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select to date','error');
            return false;

        }
        else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
            $("#txtFromDate").addClass("required");
            $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">Invalid</span>');
            alertify.dismissAll();
            notify('Invalid dates','error');
            error=true;
            return false;
        }
        else{
            error=false;
        }

        if(!error){
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var action = 'report';
            var ajaxurl=API_URL+'index.php/ReceiptDeletion/getReceiptDeletionListReportExport';
            var params = {'branch': branchId, 'course': courseId, 'receiptmode': receiptmodeId,'company':companyId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '), 'receiptmodeText': receiptmodeIdText.join(', '),'companyText': companyIdText.join(', '),fromDate:fromDate,toDate:toDate};
            var userID=$('#userId').val();
            var type='GET';
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            var response =
                commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterReceiptDeletionListReportExportResponse});
        } else {
            alertify.dismissAll();
            notify('Select any filter', 'error', 10);
        }
    }
    function filterReceiptDeletionListReportExportResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            var download=API_URL+'Download/downloadReport/'+response.file_name;
            window.location.href=(download);
        }
        else{
            notify(response.message,'error');
        }
    }
    /* Receipt Deletion list report ends here */

</script>

