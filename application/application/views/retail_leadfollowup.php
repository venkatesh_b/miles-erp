<?php
/**
 * Created by PhpStorm.
 * User: satyanarayanaraju
 * Date: 18/03/16
 * Time: 12:46 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Retail Leads Follow up</h3>

        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a href="javascript:;" onclick="ManageLead(this)" class="tooltipped" data-position="top" data-tooltip="Add Lead" ><i class="icon-plus-circle"></i></a></li>
                <li access-element="import" ><a class="tooltipped" data-position="top" data-tooltip="Import Leads" href="javascript:;" onclick="resetImport(this)"><i class="icon-import1"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="wrap-left col-sm-8 p0">
                <div class="col-sm-12 p0 mb10">
                    <div class="col-sm-3 p0 ">
                        <div class="contact-wrap branch-green-bg">
                            <span class="green-bg"><img src="<?php echo BASE_URL; ?>assets/images/new-leads1.png"></span>
                        </div>
                        <div class="contact-wrap-text mr5 branch-green-bg-light">
                            <div class="display-inline-block contact-green-color">New Leads</div>
                            <div class="contact-blue" id="newleads"></div>
                        </div>
                    </div>
                    <div class="col-sm-3 p0 ">
                        <div class="contact-wrap branch-red-bg">
                            <span class="red-bg"><img src="<?php echo BASE_URL; ?>assets/images/total-leads1.png"></span>
                        </div>
                        <div class="contact-wrap-text mr5 branch-red-bg-light">
                            <div class="display-inline-block contact-red-color">Total Leads</div>
                            <div class="contact-blue" id="totalleads"></div>
                        </div>
                    </div>
                    <div class="col-sm-3 p0 ">
                        <div class="contact-wrap branch-blue-bg">
                            <span class="blue-bg"><img src="<?php echo BASE_URL; ?>assets/images/completed1.png"></span>
                        </div>
                        <div class="contact-wrap-text mr5 branch-blue-bg-light">
                            <div class="display-inline-block contact-blue-color">Completed</div>
                            <div class="contact-blue" id="completed"></div>
                        </div>
                    </div>
                    <div class="col-sm-3 p0 ">
                        <div class="contact-wrap branch-yellow-bg">
                            <span class="yellow-bg"><img src="<?php echo BASE_URL; ?>assets/images/on-hold1.png"></span>
                        </div>
                        <div class="contact-wrap-text branch-yellow-bg-light">
                            <div class="display-inline-block contact-yellow-color">Transferred Leads</div>
                            <div class="contact-blue" id="transferred"></div>
                        </div>
                    </div>
                </div>
                <div access-element="list" class="col-sm-12 p0">
                    <table class="table table-responsive table-striped table-custom min-height-td" id="leads-list">
                        <thead >
                        <tr class="">
                            <th style="">Name</th>
                            <th style="width:80px;">Course</th>


                            <th style="width:50px;">Level </th>
                            <th class="no-sort border-r-none">Info</th>
                            <th style="width:40px;" class="no-sort"></th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
            <div class="sidebar-widget col-sm-4 pr0" access-element="add">
                <form id="quickLeadForm">
                    <div class="widget-right border border-blue">
                        <div class="widget-header light-dark-blue relative">
                            <h4 class="heading-uppercase text-center f14 m0 p10">Quick Add New Lead</h4>
                        </div>
                        <div class="widget-content bg-white clearfix pb20 pt10">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input type="text" class="validate" id="txtLeadName">
                                    <label for="txtLeadName">Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtQuickLeadCourses">
                                    </select>
                                    <label class="select-label">Course <em>*</em></label>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input type="text" class="validate" id="txtLeadPhone" onkeypress="return isPhoneNumber(event)" maxlength="15">
                                    <label for="txtLeadPhone">Mobile No. <!--<em>*</em>--></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input type="email" class="validate" id="txtLeadEmail">
                                    <label  for="txtLeadEmail">Email Address <!--<em>*</em>--></label>
                                </div>
                            </div>
								<div class="col-sm-12 mb20" id="contactType-corporate-tags">
                                    <div class="input-field col-sm-11 p0 custom-multiselect-wrap"><!-- class="custom-select-nomargin" -->
                                        <select multiple data-placeholder="Choose Tags" id="txtQuickContactTag" class="chosen-select">
                                            <option value="" disabled>--Select--</option>
                                        </select>
                                        <label class="select-label">Tags<!-- <em>*</em>--></label>
                                    </div>
                                    <div class="col-sm-1 p0" access-element="add reference">
                                        <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetTagWrapper(this)" href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                    </div>
                                    <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap  leadTag-wrap" id="ContactTagWrapper">
                                        <div class="col-sm-8 p0 ">
                                            <div class="input-field mt15">
                                                <input id="txtContactTagWrapper" type="text" class="validate" value="">
                                                <label for="txtContactTagWrapper">Tag Name <em>*</em></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 p0 ">
                                            <a class="mt20 pl10 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="addLeadTagWrapper(this, '#txtQuickContactTag')"></span></a>
                                            <a class="mt20 pl10 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 mb20" id="contactType-source">
                                    <div class="input-field col-sm-11 p0 custom-select-wrap">
                                        <select class="chosen-select" id="txtQuickContactSource">
                                            <option value="" disabled selected>--Select--</option>
                                        </select>
                                        <label class="select-label">Source <em>*</em></label>
                                    </div>
                                    <div class="col-sm-1 p0" access-element="add reference">
                                        <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetContactSourceWrapper(this)" href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                    </div>
                                    <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ContactSourceWrapper">
                                        <div class="col-sm-8 p0 ">
                                            <div class="input-field mt15">
                                                <input id="txtContactSourceWrapper" type="text" class="validate" value="">
                                                <label for="txtContactSourceWrapper">Source <em>*</em></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 p0 ">
                                            <a class="mt20 pl10 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="addLeadContactSourceWrapper(this, '#txtQuickContactSource')"></span></a>
                                            <a class="mt20 pl10 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="txtLeadDescription" class="materialize-textarea"></textarea>
                                    <label class="" for="txtLeadDescription">Known information about this lead <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="hint-msg">Note: Mobile No. or Email Address is mandatory <em>*</em></label>
                            </div>
                            <div class="col-sm-12 text-center">
                                <button class="btn blue-btn" type="button" onclick="addQuickLead(this)"><i class="icon-plus-circle mr8"></i>Add
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!--Create Lead Modal-->

        <div class="modal fade" id="createLead" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <form id="ManageCreateLeadForm">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="">Create Lead</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="leadName" type="text" class="validate">
                                    <label for="leadName">Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="LeadCourses">
                                    </select>
                                    <label class="select-label">Course <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="leadPhone" type="text" class="validate" onkeypress="return isPhoneNumber(event)" maxlength="15">
                                    <label for="leadPhone">Mobile No. <!--<em>*</em>--></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="leadEmail" type="email" class="">
                                    <label for="leadEmail" >Email Address <!--<em>*</em>--></label>
                                </div>
                            </div>
<div class="col-sm-12 mb20" id="contactType-corporate-tags">
                                <div class="input-field col-sm-11 p0 custom-multiselect-wrap"><!-- class="custom-select-nomargin" -->
                                    <select multiple data-placeholder="Choose Tags" id="txtContactTag" class="chosen-select">
                                        <option value="" disabled>--Select--</option>
                                    </select>
                                    <label class="select-label">Tags<!-- <em>*</em>--></label>
                                </div>
                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn pull-right" onclick="resetTagsWrapper(this)" href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap leadTag-wrap" id="ContactTagWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtContactTagWrapper" type="text" class="validate" value="">
                                            <label for="txtContactTagWrapper">Tag Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="addLeadTagWrapper(this, '#txtContactTag')"></span></a>
                                        <a class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" id="contactType-source">
                                <div class="input-field col-sm-11 p0 custom-select-wrap">
                                    <select class="chosen-select" id="txtContactSource">
                                        <option value="" disabled selected>--Select--</option>
                                    </select>
                                    <label class="select-label">Source <em>*</em></label>
                                </div>
                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn pull-right" onclick="resetLeadContactSourceWrapper(this)" href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ContactSourceWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtContactSourceWrapper" type="text" class="validate" value="">
                                            <label for="txtContactSourceWrapper">Source <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="addLeadContactSourceWrapper(this, '#ManageCreateLeadForm #txtContactSource')"></span></a>
                                        <a class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" id="LeadCreateContactTypeWrapper" >

                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtLeadContactPostQualification">

                                    </select>
                                    <label class="select-label">Qualification Level<em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtLeadContactQualification">

                                    </select>
                                    <label class="select-label">Qualification <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12 mb20" id="contactType-corporate-wrap">
                                <div class="input-field col-sm-11 p0">
                                    <select id="txtLeadContactCompany" class="custom-select-nomargin">

                                    </select>
                                    <label class="select-label">Company <em>*</em></label>
                                </div>

                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="LeadCreateresetCompanyWrapper(this)"><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap " id="ComapnyWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtComapnyNameWrapper" type="text" class="validate" value="">
                                            <label for="txtComapnyNameWrapper">Company Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="LeadCreateaddCompanyWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mb20" id="contactType-Institution-wrap">
                                <div class="input-field col-sm-11 p0">
                                    <select id="txtLeadContactInstitution" class="custom-select-nomargin">

                                    </select>
                                    <label class="select-label">Institution <em>*</em></label>
                                </div>
                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddInstituteWrap-btn" onclick="LeadCreateresetInstitutionWrapper(this)"><span class="icon-plus-circle f24 mt20 diplay-inline-block"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="InstitutionWrapper">
                                    <div class="col-sm-8 p0">
                                        <div class="input-field mt15">
                                            <input id="txtInstitutionNameWrapper" type="text" class="validate" value="">
                                            <label for="txtInstitutionNameWrapper">Institution Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="LeadCreateaddInstitutionWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea class="materialize-textarea mb0" id="LeadDescription"></textarea>
                                    <label for="LeadDescription" class="">Known information about this lead <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="hint-msg">Note: Mobile No. or Email Address is mandatory <em>*</em></label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue-btn" onclick="saveLead(this)"><i class="icon-right mr8"></i>Add
                            </button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                    class="icon-times mr8"></i>Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--calling-->
        <div class="modal fade" id="callStatus" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeLeadCallStatus(this)"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Call Status</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix" id="CallStatusData">
                        <div class="col-sm-12 grey-bg">
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Name</span>
                                    <p id="BxrefLeadName"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Email</span>
                                    <p id="BxrefLeadEmail"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Phone</span>
                                    <p id="BxrefLeadPhone"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">City</span>
                                    <p id="BxrefLeadCity"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Country</span>
                                    <p id="BxrefLeadCountry"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Last Follow up</span>
                                    <p id="BxrefLeadCreatedDate"></p>
                                </div>
                            </div>
                        </div>
                        <form id="ManageCallStatus">
                        <input type="hidden" id="hdnBxrefLeadId" value="">
                        <input type="hidden" id="hdnLeadStage" value="">
                        <input type="hidden" id="hdnLead_id" value="">
                         <input type="hidden" id="hdnLeadStageText" value="">
                        <div class="col-sm-12 pt5">
                            <div class="input-field">
                                <select id="ddlCalltype">
                                    <option value="Outgoing">Outgoing</option>
                                    <option value="Incoming">Incoming</option>
                                </select>
                                <label for="ddlCalltype" class="select-label">Call Type</label>
                            </div>
                        </div>
                        <div class="col-sm-12 pt5">
                            <div class="input-field">
                                <select id="callsTypes" onchange="CallStatusChange()">

                                </select>
                                <label id="callsTypes" for="callsTypes" class="select-label">Call Status</label>
                            </div>
                        </div>
                            <div class="col-sm-12 pt5" id="CommonCallStatusInformation">
                                <div class="input-field">
                                    <select id="CallAnswerTypes" onchange="getCallAnswerTypes(this)">

                                    </select>
                                    <label for="CallAnswerTypes" class="select-label">Answer</label>
                                </div>
                            </div>
                            <div class="col-sm-12 pt5" id="CommonCallStatusDatePicker" style="display:none;">
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="txtCommonCallStatusDatePicker">
                                        <label for ="txtCommonCallStatusDatePicker" id="txtLabelCommonCallStatusDatePicker" class="datepicker-label"></label>
                                    </div>
                                </div>
                            </div>
                        <div class="col-sm-12 pt5" id="FutureDatesPicker" style="display:none;">
                            <div class="col-sm-12 p0">
                                <div class="input-field">
                                    <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="dpNextFollowUp">
                                    <label for ="dpNextFollowUp" class="datepicker-label">Next Follow Up Date  <em>*</em></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" id="CallStatusInformation" style="display:none;">
                            <div class="multiple-radio" >
                                <span class="inline-radio">
                                    <input class="with-gap"  name="callStatusInfo_radio" type="radio" id="callStatusInfoIntrested" value="callStatusInfoIntrested" />
                                    <label for="callStatusInfoIntrested">Interested <span id="intrestedLevelLabel"></span></label>
                                </span>
                                 <span class="inline-radio">
                                    <input class="with-gap" checked name="callStatusInfo_radio" type="radio" id="callStatusInfoNotintrested" value="callStatusInfoNotintrested"  />
                                    <label for="callStatusInfoNotintrested">Not interested <span>{M2}</span></label>
                                </span>
                                 <span class="inline-radio">
                                    <input class="with-gap" name="callStatusInfo_radio" type="radio" id="callStatusInfoDND"  value="callStatusInfoDND" />
                                    <label for="callStatusInfoDND">DND <span>(L0)</span></label>
                                </span>
                            </div>
                        </div>
                            <div class="col-sm-12 pt5" id="VisitedDatesPicker" style="display:none;">
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="dpvisitedDate">
                                        <label for ="dpvisitedDate" class="datepicker-label">Next Visitied Date  <em>*</em></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12" id="callStatusComments" style="display:none;">
                            <div class="input-field">
                                <textarea id="txaCallStatusDescription" class="materialize-textarea"></textarea>
                                <label class="" for="txaCallStatusDescription">Comments <em>*</em></label>
                            </div>
                        </div>
                        <div class="coldcall-info" style="display:none;">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select>
                                        <option value="" disabled selected>--Select--</option>
                                        <option value="1">CPA</option>
                                        <option value="2">CMA</option>
                                        <option value="3">PGDA</option>
                                    </select>
                                    <label class="select-label">Course</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select>
                                        <option value="" disabled selected>--Select--</option>
                                        <option value="1">Hyderabad</option>
                                        <option value="2">Chennai</option>
                                        <option value="3">Bangalore</option>
                                    </select>
                                    <label class="select-label">Branch</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="qlf" type="text" class="validate" >
                                    <label for="qlf">Qualification</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="cmp" type="text" class="validate" >
                                    <label for="cmp">Company</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="inst" type="text" class="validate" >
                                    <label for="inst">Institution</label>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="changeCallStatus(this)" class="btn blue-btn"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" class="btn blue-light-btn" onclick="closeLeadCallStatus(this)"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Import Model-->

        <div class="modal fade" id="leadFollowUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="600">
            <form id="importForm" method="post">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title">Import</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="file-field input-field">
                                    <div class="btn file-upload-btn">
                                        <span>Upload Document</span>
                                        <input type="file" id="txtfilebrowser">
                                    </div>
                                    <div class="file-path-wrapper custom">
                                        <input type="text" placeholder="Upload single file" class="file-path validate">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mt20">
                                <a class="btn blue-btn btn-sm" href="<?php echo BASE_URL.'templates/LeadsTemplate.csv'; ?>" id="btnDownloadSample"><i class="icon-file mr8"></i>Download Sample</a>
                            </div>

                            <div class="col-sm-12 mb20 mt20">
                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info ml0 grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Total Contacts</div>
                                            <div class="camp-num red f14" id="TotalUploadedContacts">-</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Uploaded</div>
                                            <div class="camp-num thick-blue f14" id="UploadedContacts">-</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Failed</div>
                                            <div class="camp-num violet f14" id="FailedContacts">-</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info mr0 grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Duplicate</div>
                                            <div class="camp-num light-black f14" id="DuplicateContacts">-</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-responsive table-striped table-custom" id="uploadedContactsList">
                                    <colgroup>
                                        <col width="25%">
                                        <col width="40%">
                                        <col width="35%">
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th>Row No.</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                    </tr>
                                    <tr>
                                        <td class="border-none p0 lh10">&nbsp;</td>
                                    </tr>
                                    <!-- <tr><td class="border-none p0 lh5">&nbsp;</td></tr>-->
                                    </thead>
                                    <tbody>
                                    <tr class="">
                                        <td colspan="3" align='center'>No Data Found</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <a class="btn blue-btn" href="javascript:;" onclick="importContacts(this)"><i class="icon-right mr8"></i>Upload
                            </a>
                            <a class="btn blue-light-btn" data-dismiss="modal"><i
                                    class="icon-times mr8"></i>Cancel
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="FeeAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageFeeAssignForm" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Fee Assign</h4>
                            <input type="hidden" id="hdnFeeAssignId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12" id="FeeassignLeadNumber">
                                <div class="col-sm-11 pl0">
                                    <div class="input-field">
                                        <input id="txtLeadNumber" type="text" class="validate">
                                        <label for="txtLeadNumber">Mobile Number <em>*</em></label>

                                    </div>
                                </div>
                                <div class="col-sm-1 p0 mt2 sky-blue">
                                    <a href="javascript:;" class="btn blue-btn"  onclick="showLeadInfo(this)">GO</a>

                                </div>
                            </div>

                            <input type="hidden" id="hdnCourseId" value="">
                            <input type="hidden" id="hdnBranchId" value="">
                            <input type="hidden" id="hdnBranchxceflead" value="">
                            <input type="hidden" id="payableAmount" value="">
                            <input type="hidden" id="feeStructureId" value="">
                            <input type="hidden" id="typeOfFee" value="">
                            <input type="hidden" id="company" value="">
                            <input type="hidden" id="Institutional" value="">
                            <div id="LeadData">
                                <div class="col-sm-12 mtb10">

                                    <div id="leadInfoData" >

                                    </div>
                                </div>
                                <div class="col-sm-12" id="typeOfClass" style="display:none">
                                    <div class="multiple-checkbox" style="display: none">
                                    <span class="inline-checkbox">
                                    <input type="checkbox" class="filled-in" id="clasRoom" />
                                    <label for="clasRoom">Class Room Training</label>
                                    </span>
                                    <span class="inline-checkbox">
                                    <input type="checkbox" class="filled-in" id="online" />
                                    <label for="online">Online Training</label>


                                    </span>
                                    </div>
                                    <div class="input-field">
                                        <select id="Trainingtype">
                                            <option value="" selected>--select--</option>

                                        </select>
                                        <label class="select-label">Training Type <em>*</em></label>
                                    </div>
                                </div>

                                <div class="col-sm-12" id="feetypes" style="display:none">
                                    <div class="input-field">
                                        <select id="ddlFeetypes"  name="ddlFeetypes">
                                            <option value="" selected>--select--</option>

                                        </select>
                                        <label class="select-label">Fee Type</label>

                                    </div>
                                </div>
                                <div class="col-sm-12" id="feeStructureType" style="display:none">
                                    <div class="input-field">
                                        <select id="ddlFeeStructuetypes"  name="ddlFeeStructuetypes">
                                            <option value="" selected>--select--</option>
                                            <option value="11">Retail</option>
                                            <option value="12">Corporate</option>
                                            <option value="13">Group</option>
                                        </select>
                                        <label class="select-label">Fee Type</label>

                                    </div>
                                </div>


                                <div class="col-sm-12 mt0 feeStructureCreate-companyname" id="Corporatetypes" style="display:none">
                                    <div class="input-field">
                                        <select id="corporate"  name="corporate" class="validate custom-select-nomargin formSubmit">
                                            <option value="default" disabled selected>---Select---</option>
                                        </select>
                                        <label class="select-label">Corporate Company <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt0 feeStructureCreate-groupname" id="groupTypes" style="display:none">
                                    <div class="input-field">
                                        <select id="groupvalue"  name="groupvalue" class="validate formSubmit custom-select-nomargin">
                                            <option value="default" disabled selected>---Select---</option>
                                        </select>
                                        <label class="select-label">Institute Name <em>*</em></label>
                                    </div>

                                </div>
                                <div id="tabs" class="mt10"></div>
                                <div id="DataMessage" class="p5" style="display:none;"></div>
                            </div>
                        </div>


                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveButton"  class="btn blue-btn" onclick="SaveCandidateDetails(this)"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelButton"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        glbBranchId = $('#userBranchList').val();
        buildLeadsDataTable();
        getQuickLeadCourses();
        ShowLeadsCount();
		getLeadsTags(this, '#txtQuickContactTag');
        getLeadSourcesDropdown('#quickLeadForm #txtQuickContactSource');
        $('#txtQuickLeadCourses').material_select();
    });

    function buildLeadsDataTable()
    {
        $(document).ready(function() {
            var ajaxurl=API_URL+'index.php/LeadFollowUp/getLeadsList';
            var userID=$('#hdnUserId').val();
            var branchId=$('#userBranchList').val();
            var params={branchId:branchId,userID:userID,type:'Self Employee'}
            var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            $("#leads-list").dataTable().fnDestroy();
            $tableobj= $('#leads-list').DataTable( {
                "fnDrawCallback": function() {
                    buildpopover();
                    verifyAccess();
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                    }
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": true,
                "bProcessing": true,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No leads found",
                    "sZeroRecords": "No leads found",
                    "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                },
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    headers:headerParams,
                    data:params,
                    error:function(response) {
                        DTResponseerror(response);
                    }
                },
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false
                } ],
                "order": [[ 0, "desc" ]],
                columns: [
                    {
                        data: null, render: function ( data, type, row )
                    {
                        var leadName="";
                        var leadIcon='male-icon.jpg';
                        if(data.gender==0){
                            var leadIcon='female-icon.jpg';
                        }
                        leadName+='<div class="img-wrapper img-wrapper-leadinfo light-blue3-bg" style="width:70px;height:63px;margin-left:-8px; position:absolute;top:50%;margin-top:-32px"><a class=\'tooltipped\' data-position="top" data-tooltip="Click here for lead info" style="display:block;" href="'+BASE_URL+'app/leadinfo/'+data.branch_xref_lead_id_encode+'/'+$context+'~'+$pageUrl+'~'+$pageUrl+'"><img src="<?php echo BASE_URL; ?>assets/images/'+leadIcon+'" style="width:33px;height:33px"><span class="img-text" style="word-break:break-word">'+data.lead_number+'</span></a></div>';
                        leadName+='<div style="padding-left:85px"><div class="f14 font-bold">';
                        leadName+= data.lead_name;
                        leadName+='</div>';
                        leadName+='<span class="pl0">Last follow up : '+data.created_on+'</span></div>';
                        return leadName;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.course_name;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.level;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        var company_name = (data.company == null || data.company == '') ? "" :'<p><lable class="clabel icon-label"><a class="dark-sky-blue tooltipped" data-position="top" data-tooltip="Company"  href="javascript:"><i class="icon-company ml5 mt2 f14"></i></a></lable>'+data.company+'</p>';
                        var Qualificatin_name = (data.qualification == null || data.qualification == '') ? "" :'<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Qualification" class="dark-sky-blue tooltipped"><i class="icon-qualification ml5 mt2"></i></a></lable>'+data.qualification+'</p>';
                        var infoEmail=(data.email == null || data.email == '') ? "" :'<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Email" class="dark-sky-blue tooltipped"><i class="icon-mail ml5 mt2"></i></a></lable>'+data.email+'</p>';
                        var infoPhone=(data.phone == null || data.phone == '') ? "" :'<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Phone" class="dark-sky-blue tooltipped"><i class="icon-phone ml5 mt2"></i></a></lable>'+data.phone+'</p>';
                        var info="";
                        info+=infoEmail;
                        info+=infoPhone;
                        info+=''+Qualificatin_name+'';
                        return info;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            var view='';
                            view+='<div class="leads-icons pull-right">';
                            if(data.comments.length>0)
                            {
                                view+='<a href="javascript:" class="popper" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Comments"><img src="<?php echo BASE_URL; ?>assets/images/comment.png"></a>';
                                view+='<div class="popper-content hide">';
                                $.each(data.comments, function (key, value)
                                {
                                    if(value.branch_visit_comments==null || value.branch_visit_comments.trim()=='')
                                        value.branch_visit_comments='----';
                                    view+='<div class="border-bottom plr15 pb20 mb5">';
                                    view+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">First Level Comments</h4>';
                                    view+= '<p class="p0">'+value.branch_visit_comments+'</p>';
                                    view+= '<p class="p0 mt5 ash">'+value.branch_visited_date+'<span class="ml15">By<span class="anchor-blue pl10">'+value.counseollor_name+'</span></span> </p>';
                                    view+='</div>';
                                    view+='<div class="border-bottom plr15 pb20 mb5">';
                                    view+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">Second Level Comments</h4>';
                                    view+= '<p class="p0">'+value.second_level_comments+'</p>';
                                    view+= '<p class="p0 mt5 ash">'+value.counsellor_visited_date+'<span class="ml15">By<span class="anchor-blue pl10">'+value.second_counseollor_name+'</span></span> </p>';
                                    view+='</div>';
                                });
                                view+='</div>';
                            }
                            view+='<span data-tooltip="Change Call Status" data-delay="50" data-position="top" class="tooltipped"><a onclick="manageLeadCallStatus(this,'+data.branch_xref_lead_id+')" href="javascript:;"><img src="<?php echo BASE_URL; ?>assets/images/call.png"></a></span>';
                            //view+='<span data-tooltip="Fee Assign" data-delay="50" data-position="top" class="tooltipped"><a href="javascript:" onclick="ManageLeadsFeeAssign(this,'+data.lead_number+','+data.course_id+')"><img src="<?php echo BASE_URL; ?>assets/images/rupee.png"></a></span>';
                            view+='</div>';
                            return view;
                        }
                    }


                ]
            } );
            DTSearchOnKeyPressEnter();

        } );
    }
    function ShowLeadsCount(This){
        var userID=$('#hdnUserId').val();
        var action='list';
        var ajaxurl = API_URL + 'index.php/LeadsInfo/ShowLeadsCount';
        var params = {userId:userID,branchId:glbBranchId,type:'Self Employee'};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This:This,params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess:ShowLeadsCountResponse});


    }
    function ShowLeadsCountResponse(response)
    {

        if (response.length == 0 || response == -1 || response['status'] == false)
        {

        }else
        {

            var NewLeads = (response.new_leads == null || response.new_leads == '') ? "0" :response.new_leads;
            var TotalLeads = (response.total_leads == null || response.total_leads == '') ? "0" :response.total_leads;
            var transferd_leds = (response.transferd_leds == null || response.transferd_leds == '') ? "0" :response.transferd_leds;
            var completed_leds = (response.completed_leds == null || response.completed_leds == '') ? "0" :response.completed_leds;
            $("#newleads").text(NewLeads);
            $("#totalleads").text(TotalLeads);
            $("#transferred").text(transferd_leds);
            $("#completed").text(completed_leds);
        }
    }
    function addQuickLead(This){
        $('#quickLeadForm input').removeClass("required");
        $('#quickLeadForm select').removeClass("required");
        $('#quickLeadForm textarea').removeClass("required");
        $('#quickLeadForm span.required-msg').remove();
        var userID=$('#hdnUserId').val();
        var regx_txtContactName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
        var regx_txtContactEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var regx_txtContactPhone = /^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/;
        var contactName = $("#txtLeadName").val();
        var contactEmail = $("#txtLeadEmail").val();
        var contactPhone = $("#txtLeadPhone").val();
        var leadType = 'Self Employee';
        var contactCourseText = $("#txtQuickLeadCourses option:selected").text();
        var selMulti = $.map($("#txtQuickLeadCourses option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {

                return $(el).val();
            }
        });
        var contactCourse = selMulti.join(",");
		var selMulti = $.map($("#txtQuickContactTag option:selected"), function (el, i) {
            if ($(el).val() != null && $(el).val().trim() != '') {
                return $(el).text();
            }
        });
        var leadTags = selMulti.join(",");
        var LeadSource = $("#txtQuickContactSource option:selected").text();
        /*var contactCourse = selMulti.join(",");*/
        var txtLeadDescription = $('#txtLeadDescription').val();
        var branchId=glbBranchId;
        var flag = 0;
        if (contactName == '')
        {
            $("#txtLeadName").addClass("required");
            $("#txtLeadName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtContactName.test($('#txtLeadName').val()) === false) {
            $("#txtLeadName").addClass("required");
            $("#txtLeadName").after('<span class="required-msg">Invalid Name</span>');
            flag = 1;
        }
        if (contactCourse == '')
        {
            $("#txtQuickLeadCourses").parent().find('.select-dropdown').addClass("required");
            $("#txtQuickLeadCourses").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (contactPhone == '' && contactEmail == '')
        {
            $("#txtLeadPhone").addClass("required");
            $("#txtLeadPhone").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (contactPhone!='' && regx_txtContactPhone.test($('#txtLeadPhone').val()) === false) {
            $("#txtLeadPhone").addClass("required");
            $("#txtLeadPhone").after('<span class="required-msg">Invalid Phone Number</span>');
            flag = 1;
        }
        if (contactEmail == '' && contactPhone == '')
        {
            $("#txtLeadEmail").addClass("required");
            $("#txtLeadEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if (contactEmail!='' && regx_txtContactEmail.test($('#txtLeadEmail').val()) === false) {
            $("#txtLeadEmail").addClass("required");
            $("#txtLeadEmail").after('<span class="required-msg">Invalid E-mail</span>');
            flag = 1;
        }
        if (txtLeadDescription == '') {
            $("#txtLeadDescription").addClass("required");
            $("#txtLeadDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
		if (LeadSource == '--Select--')
        {
            $("#txtQuickContactSource").parent().addClass("required");
            $("#txtQuickContactSource").after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if(flag==1){
            return false
        }
        else{
            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/Lead/addQuickLead';
            var params = {userID: userID, contactName: contactName, contactEmail: contactEmail, contactPhone: contactPhone,contactCourse: contactCourse, contactCourseText: contactCourseText,contactDescription:txtLeadDescription,branchId:branchId,LeadSource: LeadSource, leadTags: leadTags};var params = {userID: userID, contactName: contactName, contactEmail: contactEmail, contactPhone: contactPhone,contactCourse: contactCourse, contactCourseText: contactCourseText,contactDescription:txtLeadDescription,branchId:branchId,leadType:leadType};
            var type = "POST";
            var action='add';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: This, method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: addQuickLeadResponse});
        }

    }
    function addQuickLeadResponse(response){
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
            if (response.data['contactName'])
            {
                $("#txtLeadName").addClass("required");
                $("#txtLeadName").after('<span class="required-msg">' + response.data['contactName'] + '</span>');
            }
            if (response.data['contactEmail'])
            {
                $("#txtLeadEmail").addClass("required");
                $("#txtLeadEmail").after('<span class="required-msg">' + response.data['contactEmail'] + '</span>');
            }
            if (response.data['contactPhone'])
            {
                $("#txtLeadPhone").addClass("required");
                $("#txtLeadPhone").after('<span class="required-msg">' + response.data['contactPhone'] + '</span>');
            }
            if (response.data['contactCourse'])
            {
                $("#txtQuickLeadCourses").parent().find('.select-dropdown').addClass("required");
                $("#txtQuickLeadCourses").parent().after('<span class="required-msg">' + response.data['contactCourse'] + '</span>');
            }
            if (response.data['contactDescription'])
            {
                $("#txtLeadDescription").addClass("required");
                $("#txtLeadDescription").after('<span class="required-msg">' + response.data['contactDescription'] + '</span>');
            }
        } else
        {
            notify(response.message, 'success', 10);
            resetQuickLeadForm();
        }

    }

    function getQuickLeadCourses() {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Courses/getBranchCoursesList';
        var params = {branchId:glbBranchId};
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getQuickLeadCoursesResponse});
    }

    function getQuickLeadCoursesResponse(response) {

        $('#txtQuickLeadCourses').empty();
        $('#txtQuickLeadCourses').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtQuickLeadCourses').append($('<option></option>').val(value.courseId).html(value.name));
                });
            }
        }
        $('#txtQuickLeadCourses').material_select();

    }



    function resetQuickLeadForm(){
        $('#quickLeadForm')[0].reset();
		$('#ManageCreateLeadForm .select-wrapper').removeClass("required");
        $('#quickLeadForm input').removeClass("required");
        $('#quickLeadForm select').removeClass("required");
        $('#quickLeadForm textarea').removeClass("required");
        $('#quickLeadForm span.required-msg').remove();
        $('#quickLeadForm input').next('label').removeClass("active");
        $('#quickLeadForm textarea').next('label').removeClass("active");
        getQuickLeadCourses();
		getLeadsTags(this, '#txtQuickContactTag');
        getLeadSourcesDropdown('#quickLeadForm #txtQuickContactSource');
    }
    var glbBranchId='';
    $("#userBranchList" ).change(function() {
        changeDefaultBranch();
        glbBranchId = $('#userBranchList').val();
        getQuickLeadCourses();

        buildLeadsDataTable();
		ShowLeadsCount(this);
    });

    function ManageLead(This){
        alertify.dismissAll();
        notify('Processing..', 'warning', 10);
        $('#ManageCreateLeadForm')[0].reset();
		$('#ManageCreateLeadForm .select-wrapper').removeClass("required");
        $('#ManageCreateLeadForm input').removeClass("required");
        $('#ManageCreateLeadForm select').removeClass("required");
        $('#ManageCreateLeadForm textarea').removeClass("required");
        $('#ManageCreateLeadForm span.required-msg').remove();
        $('#ManageCreateLeadForm input').next('label').removeClass("active");
        $('#ManageCreateLeadForm textarea').next('label').removeClass("active");
		$('#ManageCreateLeadForm .coldCall-addCompanyWrap').slideUp();
        getLeadContactTypes(This);
        getLeadCreatePostQualifications();
        //getLeadCreateQualifications();
        getLeadCreateCompanies();
        getLeadCreateInstitutions();
        getLeadCourses(This);
		//For Lead source and Tags
        getLeadsTags(This, '#txtContactTag');
        getContactSourcesDropdown('#txtContactSource');
        $(This).attr("data-target", "#createLead");
        $(This).attr("data-toggle", "modal");

    }
	function getLeadsTags(This, fieldName){
        var $field = $(fieldName);
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Lead/getContactConfigurations';
        var params = {};
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
            /*$('#txtContactTag').append($('<option></option>').val('').html('--Select All--'));*/
            $field.append($('<option></option>').val('').html('--Select All--'));
            if (response.data.tags.length > 0) {
                $.each(response.data.tags, function (key, value) {
                    $field.append($('<option></option>').val(value.id).html(value.tagName));
                });
            }
            $field.chosen({
                no_results_text: "Oops, nothing found!",
                width: "95%"
            });
            $field.trigger("chosen:updated");
        }});
    }
    function addLeadTagWrapper(This ,fieldName)
    {
        var $field = $(fieldName);
        $current = $(This).parent().parent().parent();
        $current.find('span.required-msg').remove();
        var url = '';
        var action = 'add';
        var name = $current.find('#txtContactTagWrapper').val();
        var description = '----';
        var data = '';

        action = 'add';

        data = {
            tagName: name,
            tagDescription: description
        };
        url = API_URL + "index.php/Tag/addTag";
        var regx_txtTagWrapper = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

        var flag = 0;
        if (name == '') {
            $current.find("#txtContactTagWrapper").addClass("required");
            $current.find("#txtContactTagWrapper").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if ((regx_txtTagWrapper.test(name)) === false)
        {
            $current.find("#txtContactTagWrapper").addClass("required");
            $current.find("#txtContactTagWrapper").after('<span class="required-msg">Invalid Tag Name.</span>');
            flag = 1;
        }

        if (flag == 0) {
            var ajaxurl = url;
            var params = data;

            var userId = $('#hdnUserId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                if (response == -1 || response['status'] == false)
                {
                    var id = '';
                    alertify.dismissAll();
                    notify(response['message'], 'error', 10);
                    for (var a in response.data) {
                        if (a == 'tagName') {
                            id = '#txtContactTagWrapper';
                            $current.addClass("required");
                            $current.next('label').addClass("active");
                            $current.after('<span class="required-msg">' + response.data[a] + '</span>');
                        } else {
                            id = '#' + a;
                        }

                    }

                } else
                {
                    alertify.dismissAll();
                    notify('Tag Saved Successfully', 'success', 10);
                    $current.find('#txtContactTagWrapper').val('');
                    $current.slideUp();
                    $current.next('label').removeClass("active");
                    $current.next('input').removeClass("required");
                    $current.next('span.required-msg').remove();
                    getLeadsTags(This, fieldName);
                }
            }});

        } else {
            return false;
        }
    }
    function resetTagsWrapper(This) {
        $current = $(This).parent().next('#ContactTagWrapper');
        $current.find('span.required-msg').remove();
        $current.find('label').removeClass("active");
        $current.find('input').removeClass("required");
        $current.find('#txtContactTagWrapper').val('');

    }
    function getLeadSourcesDropdown(fieldName) {
        var $field = $(fieldName);
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Contact Source'};
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
            $field.empty();
            $field.append($('<option selected></option>').val('').html('--Select--'));
            if (response.status == true) {
                if (response.data.length > 0) {
                    $.each(response.data, function (key, value) {
                        $field.append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                    });
                }
                $field.trigger("chosen:updated");
            }
            $field.chosen({
                no_results_text: "Oops, nothing found!",
                width: "95%"
            });
        }
        });
    }
    function addLeadContactSourceWrapper(This, fieldName)
    {
        $current = $(This).parent().parent().parent();
        $current.find('span.required-msg').remove();
        var url = '';
        var action = 'add';
        var name = $current.find('#txtContactSourceWrapper').val();
        var description = '----';
        var data = '';
        var referenceType = 'Contact Source';
        data = {
            referenceName: name,
            referenceDescription: description,
            referenceType: referenceType
        };
        url = API_URL + "index.php/Referencevalues/addReferenceValueWrapper";
        var regx_txtTagWrapper = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

        var flag = 0;
        if (name == '') {
            $current.find("#txtContactSourceWrapper").addClass("required");
            $current.find("#txtContactSourceWrapper").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if ((regx_txtTagWrapper.test(name)) === false)
        {
            $current.find("#txtContactSourceWrapper").addClass("required");
            $current.find("#txtContactSourceWrapper").after('<span class="required-msg">Invalid Source.</span>');
            flag = 1;
        }

        if (flag == 0) {
            var ajaxurl = url;
            var params = data;

            var userId = $('#hdnUserId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                if (response == -1 || response['status'] == false)
                {
                    var id = '';
                    alertify.dismissAll();
                    notify(response['message'], 'error', 10);
                    for (var a in response.data) {
                        if (a == referenceType) {
                            id = '#txtContactSourceWrapper';
                            $current.addClass("required");
                            $current.next('label').addClass("active");
                            $current.after('<span class="required-msg">' + response.data[a] + '</span>');
                        } else {
                            id = '#' + a;
                        }

                    }

                } else
                {
                    alertify.dismissAll();
                    notify('Saved Successfully', 'success', 10);
                    $current.find('#txtContactSourceWrapper').val('');
                    $current.slideUp();
                    $current.next('label').removeClass("active");
                    $current.next('input').removeClass("required");
                    $current.next('span.required-msg').remove();
                    getLeadSourcesDropdown(fieldName);
                }
            }});

        } else {
            return false;
        }
    }
    function resetLeadContactSourceWrapper(This) {
        $current = $(This).parent().next('#ContactSourceWrapper');
        $current.find('span.required-msg').remove();
        $current.find('label').removeClass("active");
        $current.find('input').removeClass("required");
        $current.find('#txtContactSourceWrapper').val('');
    }
    function getLeadCourses(This) {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Courses/getBranchCoursesList';
        var params = {branchId:glbBranchId};
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadCoursesResponse});
    }

    function getLeadCoursesResponse(response) {

        $('#LeadCourses').empty();
        $('#LeadCourses').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#LeadCourses').append($('<option></option>').val(value.courseId).html(value.name));
                });
            }
        }
        $('#LeadCourses').material_select();
        alertify.dismissAll();
    }

    $('#txtLeadContactPostQualification').change(function()
    {
        getLeadCreateQualifications();
    });

    function LeadCreateaddCompanyWrapper(This)
    {
        $('#ComapnyWrapper span.required-msg').remove();
        //$('#ContactTagWrapper label').removeClass("active");
        //$('#ContactTagWrapper input').removeClass("required");
        var url = '';
        var action = 'add';
        var name = $('#txtComapnyNameWrapper').val();
        var description = '----';
        var data = '';
        data = {
            companyName: name,
            companyDescription: description
        };
        url = API_URL + "index.php/Tag/addCompany";
        var regx_txtTagWrapper = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

        var flag = 0;
        if (name == '') {
            $("#txtComapnyNameWrapper").addClass("required");
            $("#txtComapnyNameWrapper").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if ((regx_txtTagWrapper.test(name)) === false)
        {
            $("#txtComapnyNameWrapper").addClass("required");
            $("#txtComapnyNameWrapper").after('<span class="required-msg">Invalid Company Name.</span>');
            flag = 1;
        }

        if (flag == 0) {
            notify('Processing..', 'warning', 10);
            var ajaxurl = url;
            var params = data;

            var userId = $('#hdnUserId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false)
                {

                    var id = '';
                    alertify.dismissAll();
                    notify(response['message'], 'error', 10);
                    for (var a in response.data) {
                        if (a == 'companyName') {
                            id = '#txtComapnyNameWrapper';
                            $(id).addClass("required");
                            $(id).next('label').addClass("active");
                            $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                        } else {
                            id = '#' + a;
                        }

                    }

                } else
                {
                    notify('Company Saved Successfully', 'success', 10);
                    $('#txtComapnyNameWrapper').val('');
                    $('#ComapnyWrapper label').removeClass("active");
                    $('#ComapnyWrapper input').removeClass("required");
                    $('#ComapnyWrapper span.required-msg').remove();
                    getLeadCreateCompanies(This);
                    LeadCreateresetCompanyWrapper(This);
                    $('#ComapnyWrapper').slideUp();
                }
            }});

        } else {
            return false;
        }
    }
    function LeadCreateaddInstitutionWrapper(This)
    {
        $('#InstitutionWrapper span.required-msg').remove();
        var url = '';
        var action = 'add';
        var name = $('#txtInstitutionNameWrapper').val();
        var description = '----';
        var data = '';
        data = {
            institutionName: name,
            institutionDescription: description
        };
        url = API_URL + "index.php/Tag/addInstitution";
        var regx_txtTagWrapper = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

        var flag = 0;
        if (name == '') {
            $("#txtInstitutionNameWrapper").addClass("required");
            $("#txtInstitutionNameWrapper").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if ((regx_txtTagWrapper.test(name)) === false)
        {
            $("#txtInstitutionNameWrapper").addClass("required");
            $("#txtInstitutionNameWrapper").after('<span class="required-msg">Invalid Institution Name.</span>');
            flag = 1;
        }

        if (flag == 0) {
            notify('Processing..', 'warning', 10);
            var ajaxurl = url;
            var params = data;

            var userId = $('#hdnUserId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false)
                {

                    var id = '';
                    notify(response['message'], 'error', 10);
                    for (var a in response.data) {
                        if (a == 'institutionName') {
                            id = '#txtInstitutionNameWrapper';
                            $(id).addClass("required");
                            $(id).next('label').addClass("active");
                            $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                        } else {
                            id = '#' + a;
                        }

                    }

                } else
                {
                    notify('Institution Saved Successfully', 'success', 10);
                    $('#txtInstitutionNameWrapper').val('');
                    $('#InstitutionWrapper label').removeClass("active");
                    $('#InstitutionWrapper input').removeClass("required");
                    $('#InstitutionWrapper span.required-msg').remove();
                    getLeadCreateInstitutions(This);
                    LeadCreateresetInstitutionWrapper(This);
                    $('#InstitutionWrapper').slideUp();
                }
            }});

        } else {
            return false;
        }
    }


    function saveLead(This){


        $('#ManageCreateLeadForm input').removeClass("required");
        $('#ManageCreateLeadForm select').removeClass("required");
        $('#ManageCreateLeadForm textarea').removeClass("required");
        $('#ManageCreateLeadForm span.required-msg').remove();
        var userID=$('#hdnUserId').val();
        var regx_txtContactName = /^[a-zA-Z][a-zA-Z0-9-\s]*$/;
        var regx_txtContactEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var regx_txtContactPhone = /^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/;
        var contactName = $("#leadName").val();
        var contactEmail = $("#leadEmail").val();
        var contactPhone = $("#leadPhone").val();
        var contactCourseText = $("#LeadCourses option:selected").text();
		var contactTagsText = $("#txtContactTag option:selected").text();
        var selMulti = $.map($("#LeadCourses option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {

                return $(el).val();
            }
        });
		var contactCourse = selMulti.join(",");
        var selMulti = $.map($("#txtContactTag option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {

                return $(el).text();
            }
        });
        var leadTags = selMulti.join(",");
        var Leadcontacttype = $("input[name=coldCallcontacttype]:checked").val();
        var LeadContactPostQualification = $("#txtLeadContactPostQualification option:selected").val();
        var LeadContactQualification = $("#txtLeadContactQualification option:selected").val();
        var LeadContactQualificationText = $("#txtLeadContactQualification option:selected").text();
        var LeadContactPostQualificationText = $("#txtLeadContactPostQualification option:selected").text();
        var LeadContactCompany = $("#txtLeadContactCompany option:selected").val();
        var LeadContactCompanyText = $("#txtLeadContactCompany option:selected").text();
        var LeadContactInstitution = $("#txtLeadContactInstitution option:selected").val();
        var LeadContactInstitutionText = $("#txtLeadContactInstitution option:selected").text();
        var contactCourse = selMulti.join(",");
        var LeadSource = $("#txtContactSource option:selected").text();	
        var txtLeadDescription = $('#LeadDescription').val();
        var branchId=glbBranchId;
        var flag = 0;
        if (contactName == '')
        {
            $("#leadName").addClass("required");
            $("#leadName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtContactName.test($('#leadName').val()) === false) {
            $("#leadName").addClass("required");
            $("#leadName").after('<span class="required-msg">Invalid Name</span>');
            flag = 1;
        }
        if (contactCourse == '')
        {
            $("#LeadCourses").parent().find('.select-dropdown').addClass("required");
            $("#LeadCourses").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (contactPhone == '' && contactEmail == '')
        {
            $("#leadPhone").addClass("required");
            $("#leadPhone").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (contactPhone!='' && regx_txtContactPhone.test($('#leadPhone').val()) === false) {
            $("#leadPhone").addClass("required");
            $("#leadPhone").after('<span class="required-msg">Invalid Phone Number</span>');
            flag = 1;
        }
        if (contactEmail == '' && contactPhone == '')
        {
            $("#leadEmail").addClass("required");
            $("#leadEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if (contactEmail!='' && regx_txtContactEmail.test($('#leadEmail').val()) === false) {
            $("#leadEmail").addClass("required");
            $("#leadEmail").after('<span class="required-msg">Invalid E-mail</span>');
            flag = 1;
        }
        if(LeadContactPostQualification==''){
            $("#txtLeadContactPostQualification").parent().find('.select-dropdown').addClass("required");
            $("#txtLeadContactPostQualification").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (LeadContactQualification == '') {
            $("#txtLeadContactQualification").parent().find('.select-dropdown').addClass("required");
            $("#txtLeadContactQualification").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (Leadcontacttype == 'Corporate' && LeadContactCompany == '') {
            $("#txtLeadContactCompany").parent().find('.select-dropdown').addClass("required");
            $("#txtLeadContactCompany").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if (Leadcontacttype == 'University' && LeadContactInstitution == '') {
            $("#txtLeadContactInstitution").parent().find('.select-dropdown').addClass("required");
            $("#txtLeadContactInstitution").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (txtLeadDescription == '') {
            $("#LeadDescription").addClass("required");
            $("#LeadDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
		if (LeadSource == '--Select--')
        {
            $("#txtContactSource").parent().addClass("required");
            $("#txtContactSource").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if(flag==1){
            return false
        }
        else{
            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/Lead/addLead';
            var params = {userID: userID, contactName: contactName, contactEmail: contactEmail, contactPhone: contactPhone,contactCourse: contactCourse, contactCourseText: contactCourseText,contactDescription:txtLeadDescription,branchId:branchId,Leadcontacttype:Leadcontacttype,LeadContactQualification:LeadContactQualificationText,LeadContactCompany:LeadContactCompanyText,LeadContactInstitution:LeadContactInstitutionText,LeadContactPostQualification:LeadContactPostQualificationText, LeadSource: LeadSource, leadTags: leadTags};
            var type = "POST";
            var action='add';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: This, method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: saveLeadResponse});
        }

    }
    function saveLeadResponse(response){
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
            if (response.data['contactName'])
            {
                $("#leadName").addClass("required");
                $("#leadName").after('<span class="required-msg">' + response.data['contactName'] + '</span>');
            }
            if (response.data['contactEmail'])
            {
                $("#leadEmail").addClass("required");
                $("#leadEmail").after('<span class="required-msg">' + response.data['contactEmail'] + '</span>');
            }
            if (response.data['contactPhone'])
            {
                $("#leadPhone").addClass("required");
                $("#leadPhone").after('<span class="required-msg">' + response.data['contactPhone'] + '</span>');
            }
            if (response.data['contactCourse'])
            {
                $("#LeadCourses").parent().find('.select-dropdown').addClass("required");
                $("#LeadCourses").parent().after('<span class="required-msg">' + response.data['contactCourse'] + '</span>');
            }
            if (response.data['contactDescription'])
            {
                $("#LeadDescription").addClass("required");
                $("#LeadDescription").after('<span class="required-msg">' + response.data['contactDescription'] + '</span>');
            }
            if (response.data['LeadContactQualification'])
            {
                $("#txtLeadContactQualification").parent().find('.select-dropdown').addClass("required");
                $("#txtLeadContactQualification").parent().after('<span class="required-msg">' + response.data['LeadContactQualification'] + '</span>');
            }
            if (response.data['LeadContactCompany'])
            {
                $("#txtLeadContactCompany").parent().find('.select-dropdown').addClass("required");
                $("#txtLeadContactCompany").parent().after('<span class="required-msg">' + response.data['LeadContactCompany'] + '</span>');
            }
            if (response.data['LeadContactInstitution'])
            {
                $("#txtLeadContactInstitution").parent().find('.select-dropdown').addClass("required");
                $("#txtLeadContactInstitution").parent().after('<span class="required-msg">' + response.data['LeadContactInstitution'] + '</span>');
            }
        } else
        {
            notify(response.message, 'success', 10);
            buildLeadsDataTable();
            $('#createLead').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }

    }

    function resetImport(This){

        $('#importForm')[0].reset();
        $('#TotalUploadedContacts').html('-');
        $('#UploadedContacts').html('-');
        $('#FailedContacts').html('-');
        $('#DuplicateContacts').html('-');
        var rowsHtml="<tr><td colspan='3' align='center'>No Data Found</td></tr>";
        $('#uploadedContactsList tbody').html(rowsHtml);
        $(This).attr("data-target","#importModal");
        $(This).attr("data-toggle","modal");
    }
    function importContacts(This){

        var ajaxurl=API_URL+'index.php/Lead/BulkAddLead';
        var userID=$('#hdnUserId').val();
        var action='add';
        var fileUpload = document.getElementById("txtfilebrowser");

        var selectedFile = $("#txtfilebrowser").val();
        var extension = selectedFile.split('.');
        if (extension[1] != "csv") {
            $("#txtfilebrowser").focus();
            notify('Please choose a .csv file', 'error', 5);
            return;
        }
        else{
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

            if (fileUpload.value != null) {
                var uploadFile = new FormData();
                var files = $("#txtfilebrowser").get(0).files;
                if (files.length > 0) {
                    uploadFile.append("file", files[0]);
                    uploadFile.append("branchId", glbBranchId);
                    //commonAjaxCall({This:This,method:'POST',requestUrl:ajaxurl,params:uploadFile,headerParams:headerParams,action:action,onSuccess:finalresp});

                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        dataType: "json",
                        data: uploadFile,
                        contentType: false,
                        processData: false,
                        headers: headerParams,
                        beforeSend:function(){
                            $(This).attr("disabled","disabled");
                            alertify.dismissAll();
                            notify('Processing..', 'warning', 10);
                        },success: function (response){
                            $(This).removeAttr("disabled");
                            alertify.dismissAll();
                            finalresp(response);
                        },error:function(response){
                            alertify.dismissAll();
                            if(response.responseJSON.message=="Access Token Expired"){
                                logout();
                            }
                            else if(response.responseJSON.message=="Invalid credentials"){
                                logout();
                            }
                            else{
                                notify('Something went wrong. Please try again', 'error', 10);
                            }

                        }
                    });
                }
                else{
                    $("#txtfilebrowser").parent().find('.file-path-wrapper').addClass("required");
                    $("#txtfilebrowser").parent().find('.file-path-wrapper').after('<span class="required-msg">required field</span>');
                }
            }
            else{

            }
        }
    }
    function finalresp(response){
        if(response.status===true){

            var counts=response.data.Counts;
            $('#TotalUploadedContacts').html(counts.total_count);
            $('#UploadedContacts').html(counts.successCount);
            $('#FailedContacts').html(counts.failedCount);
            $('#DuplicateContacts').html(counts.duplicateCount);
            var records=response.data.Records;
            var rowsHtml="";
            $.each(records, function( key, value ) {
                rowsHtml+="<tr>";
                rowsHtml+="<td>---</td>";
                rowsHtml+="<td>"+value.name+"</td>";
                rowsHtml+="<td>";
                if(value.status==0){
                    //pending
                    rowsHtml+="<span class='label f11 status-label font-normal label-warning'>Pending</span>";
                }
                if(value.status==1){
                    //success
                    rowsHtml+="<span class='label f11 status-label font-normal label-success'>Success</span>";
                    value.error_log='Add/Update Lead Successfully';
                }
                if(value.status==2){
                    //fail
                    rowsHtml+="<span class='label f11 font-normal status-label label-danger'>Failed</span>";
                }

                rowsHtml+="<span class='text-success display-inline-block valign-middle max-width100px ellipsis' title='"+value.error_log+"'>"+value.error_log+"</span>";
                rowsHtml+="</td>";
                rowsHtml+="</tr>";
            });
            $('#uploadedContactsList tbody').html(rowsHtml);
            alertify.dismissAll();
            notify("Uploaded Successfully","success",10);

        }
        else{
            alertify.dismissAll();
            notify(response.message,"error",10);
        }
    }
    $('#Trainingtype').on('change', function () {
        var mystructureVal = $(this).val();
        showFeeStructureInfo(this, '');
    });
    function getDropDownFeeTypeByTextFilter(filter, identifier){
        var userId = $('#hdnUserId').val();
        var action = 'add';
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Fee Type'};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This:this,method:'GET',requestUrl:ajaxurl,params:params,headerParams:headerParams,action:'add',onSuccess:function(response){
            $(identifier).find('option:gt(0)').remove();
            if (response.data.length == 0 || response == -1 || response['status'] == false) {

            } else {
                var filterArray = filter.split(',');
                var RawData = response.data.reverse();
                for (var b = 0; b < RawData.length; b++) {
                    for (var a = 0; a < filterArray.length; a++) {
                        if(filterArray[a] == RawData[b]['value']){
                            var o = $('<option/>', {value: RawData[b]['reference_type_value_id']})
                                .text(RawData[b]['value']);
                            o.appendTo(identifier);
                        }
                    }
                }
                $(identifier).material_select();
            }
        }});
    }
</script>