<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 17/2/16
 * Time: 12:31 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Roles</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Add Role" class="viewsidePanel" onclick="ManageRole(this,0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="roles-list">
                    <thead>
                    <tr>
                        <th>Role Name</th>
                        <th>Description</th>
                        <th>No.of Users</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageRoleForm" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Role</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <input type="hidden" id="hdnRoleId" value="0" />
                            
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtRoleName" type="text" class="validate">
                                    <label for="txtRoleName"> Role Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="txaRoleDescription" class="materialize-textarea"></textarea>
                                    <label for="txaRoleDescription">Description</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="actionButton" type="button" class="btn blue-btn" onclick="saveRole(this)"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildDataTable();
    });

    function buildDataTable()
    {
            var ajaxurl=API_URL+'index.php/UserRole/roles';
            var userID=$('#hdnUserId').val();
            var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            $("#roles-list").dataTable().fnDestroy();
            $tableobj=$('#roles-list').DataTable( {
                "fnDrawCallback": function()
                {
                    buildpopover();
                    verifyAccess();
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                            $('.dataTables_paginate').css("display", "block");
                            $('.dataTables_length').css("display", "block");
                    }
                    else
                    {
                            $('.dataTables_paginate').css("display", "none");
                            $('.dataTables_length').css("display", "none");
                    }
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": true,
                "bProcessing": true,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No roles found",
                    "sZeroRecords": "No roles found",
                    "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                },
                ajax:
                {
                    url:ajaxurl,
                    type:'GET',
                    headers:headerParams,
                    error:function(response)
                    {
                        DTResponseerror(response);
                    }
                },
                columns:
                    [
                        {
                            data: null, render: function ( data, type, row )
                            {
                                return data.value;
                            }
                        },
                        {
                            data: null, render: function ( data, type, row )
                            {
                                return (data.description == null || data.description == '') ? "----":data.description;
                            }
                        },
                        {
                            data: null, render: function ( data, type, row )
                            {
                                var roleUsersData='';
                                if(data.totalCount == 0)
                                {
                                    roleUsersData+='<span>';
                                    roleUsersData+='<a href="javascript:;" class="anchor-blue p10">'+data.totalCount+'</a>';
                                    roleUsersData+='</span>';
                                }
                                else
                                {
                                    roleUsersData+='<span class="popper p10" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Users">';
                                    roleUsersData+='<a href="javascript:;" class="anchor-blue p10">'+data.totalCount+'</a>';
                                    roleUsersData+='</span>';
                                    roleUsersData+='<div class="popper-content hide">';
                                    roleUsersData+='<p>'+data.Users+'</p>';
                                    roleUsersData+='</div>';
                                }
                                roleUsersData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                                roleUsersData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                                roleUsersData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                                roleUsersData+='</a>';
                                roleUsersData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                                roleUsersData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                                roleUsersData+='<li><a access-element="manage" href="'+BASE_URL+'app/manage_role/'+data.reference_type_value_id+'">Manage Roles</a></li>';
                                roleUsersData+='<li><a access-element="edit" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick=\'ManageRole(this,"'+data.reference_type_value_id+'")\'>Edit</a></li>';
                                roleUsersData+='</ul>';
                                roleUsersData+='</div>';
                                return roleUsersData;
                            }
                        }
                    ]
            });
            DTSearchOnKeyPressEnter();
    }

</script>