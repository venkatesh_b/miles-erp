<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 31-03-2016
 * Time: 02:13 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnBrachXrefLeadId" value="0" readonly />
    <input type="hidden" id="hdnBranchVisitorId" value="0" readonly />

    <div class="content-header-wrap">
        <h3 class="content-header">Second Level Entry</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a access-element="filters" data-toggle="modal" data-target="#filters" class="tooltipped" data-position="top" data-tooltip="Filters"><i class="icon-filter"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom min-height-td" id="counsellor-visiters-list">
                    <thead>
<!--                    <tr>
                        <th>Lead Number</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Course</th>
                        <th>Branch</th>
                        <th class='border-r-none' width="20%">Branch Visited Date</th>
                        <th class="text-center no-sort w25"></th>
                    </tr>-->
                        <tr>
                            <th  style="width:40%">Name</th>
                            <th  style="width: 5%;">Course</th>
                            <th style="width: 5%;">Level </th>
                            <th  class="no-sort" style="width: 25%;">Info</th>
                            <th  class="text-center" style="width: 25%;">First Level Comments</th>
                            <th  class="text-center" style="width: 25%;">Second Level Comments</th>
                            <th  style="width:3%;" class="no-sort"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="counsellor-comment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <form id="ManageCounsellorComments">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title">Counsellor Visit</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="grey-bg clearfix pt10 pb10">
                        <div class="col-sm-12">
                            <div class="input-field">
                                <span class="display-block ash">Lead Number</span>
                                <p id="viewlead_number">---</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Name</span>
                                <p id="viewlead_name">---</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Email</span>
                                <p id="viewlead_email">---</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Phone</span>
                                <p id="viewlead_phone">---</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Branch</span>
                                <p id="viewlead_branch_name">---</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Course</span>
                                <p id="viewlead_course_name">----</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Stage</span>
                                <p id="viewlead_stage">----</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Expected Visited Date</span>
                                <p id="viewlead_expectedVisitedDate">----</p>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-12" id="commentTypeWrapper" style="display: none">
                        <!--<div>
                            <div class="multiple-radio">
                                <span class="inline-radio">
                                    <input class="with-gap" name="txtCommentType" type="radio" id="txtCommentTypePositive" value="Positive" checked />
                                    <label for="txtCommentTypePositive">Positive</label>
                                </span>
                                <span class="inline-radio">
                                    <input class="with-gap" name="txtCommentType" type="radio" id="txtCommentTypeNextBatch" value="Next Batch Postpone" />
                                    <label for="txtCommentTypeNextBatch">Postpone to Next Batch</label>
                                </span>

                            </div>

                        </div>-->
                        <!--<div id="txtEnrollDateDiv">
                            <div class="input-field  mt0" >
                                <label class="datepicker-label datepicker">Expected Enrollment Date </label>
                                <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="txtEnrollDate">
                            </div>
                        </div>-->
<!--                        <div id="txtEnrollDateDiv">
                            <div class="input-field  mt0" >
                                <label class="datepicker-label datepicker">Visited Date <em>*</em></label>
                                <input type="date" class="datepicker relative enablePastDates" placeholder="dd/mm/yyyy" id="txtVistedDate">
                            </div>
                        </div>-->
                            <div class="">
                                <div class="input-field mt20">
                                    <textarea id="txtCounsellorComments" name="txtCounsellorComments" class="materialize-textarea"></textarea>
                                    <label for="txtCounsellorComments">Comments <em>*</em></label>
                                </div>
                            </div>
                        </div>
                        <div id="alreadyCommentsExist" style="display: none">
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Comments</span>
                                <p id="viewlead_comments">----</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-field">
                                <span class="display-block ash">Date</span>
                                <p id="viewlead_comments_date">----</p>
                            </div>
                        </div>
                        </div>


                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn" id="btnSaveCounsellorComments" onclick="saveCounsellorComments(this)"><i class="icon-right mr8"></i>Save
                        </button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                class="icon-times mr8"></i>Cancel
                        </button>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <!--Filter Modal-->
        <div class="modal fade" id="filters" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Filters</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="filter-wrap">
                            <div class="input-field mt0">
                                <input class="filled-in" id="filtersSecondLevelCounselHistory" type="checkbox" value="history" name="filtersSecondLevelCounsel">
                                <label class="f14" for="filtersSecondLevelCounselHistory">History</label>
                            </div>
                            <div class="input-field mt0">
                                <input class="filled-in" id="filtersSecondLevelCounselPending" type="checkbox" value="pending" name="filtersSecondLevelCounsel">
                                <label class="f14" for="filtersSecondLevelCounselPending">Dues</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn" onclick="applySecondLevelCounselFilters()"><i class="icon-right mr8"></i>Apply</button>
                        <button type="button" class="btn blue-light-btn" onclick="resetSecondLevelCounselFilters()"><i class="icon-times mr8"></i>Clear</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    var glbBranchId='';
    docReady(function(){
        if($('#userBranchList').val()==''){
            glbBranchId=0;
        }
        else {
            glbBranchId = $('#userBranchList').val();
        }
        buildCounsellorVisitorsListDataTable();

    });
    var appliedFiltersString='';
    function applySecondLevelCounselFilters(){
        var appliedFilters=[];
        $('input[name="filtersSecondLevelCounsel"]:checked').each(function() {

            appliedFilters.push(this.value)
        });
        appliedFiltersString=appliedFilters.join(',');
        buildCounsellorVisitorsListDataTable();
        $('#filters').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }
    function resetSecondLevelCounselFilters(){
        $('input[name="filtersSecondLevelCounsel"]').attr('checked',false);
        applySecondLevelCounselFilters();
    }
    $('#userBranchList').change(function(){
        if($('#userBranchList').val()==''){
            glbBranchId=0;
        }
        else {
            glbBranchId = $('#userBranchList').val();
        }
        changeDefaultBranch(this);
        resetSecondLevelCounselFilters();
    });
    function buildCounsellorVisitorsListDataTable(){
        var branchId=glbBranchId;
        var params={branch_id:branchId,appliedFiltersString:appliedFiltersString};
        var ajaxurl=API_URL+'index.php/CounsellorVisitors/getCounsellorVisitorsList';
        var userID=$('#hdnUserId').val();
        var action='list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#counsellor-visiters-list").dataTable().fnDestroy();
        $tableobj= $('#counsellor-visiters-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                }
                else
                {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            "order": [[ 0, "desc" ]],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                    {
                        var leadName="";
                        var leadIcon='male-icon.jpg';
                        if(data.gender==0){
                            var leadIcon='female-icon.jpg';
                        }
                        leadName += '<div class="img-wrapper img-wrapper-leadinfo light-blue3-bg" style="width:70px;height:63px;margin-left:-8px; position:absolute;top:50%;margin-top:-32px"><a class=\'tooltipped\' data-position="top" data-tooltip="Click here for lead info" href="'+BASE_URL+'app/leadinfo/'+data.branch_xref_lead_id_encode+'/'+$context+'~'+$pageUrl+'~'+$pageUrl+'"><img src="<?php echo BASE_URL; ?>assets/images/'+leadIcon+'" style="width:33px;height:33px"><span class="img-text" style="word-break:break-word">' + data.lead_number + '</span></a></div>';
                        leadName+='<div style="padding-left:85px"><div class="f14 font-bold">';
                        leadName+= data.name;
                        /*if(data.is_counsellor_visited == 1){
                            leadName+='<span style="cursor: pointer;" class="popper" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Comments"><i class="fa fa-street-view ml5"></i></span>';
                            leadName+='<div class="popper-content hide">';
                            leadName+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">Branch Visitor Comments</h4>';
                            leadName+= '<p class="p0">'+data.branch_visit_comments+'</p>';
                            leadName+= '<p class="p0 mt5">'+data.branch_visited_date+'<span class="ml15">By<strong class="anchor-blue pl10">'+data.branch_visit_name+'</strong></span> </p>';
                            leadName+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">Second Level Comments</h4>';
                            leadName+= '<p class="p0">'+data.second_level_counsellor_comments+'</p>';
                            leadName+= '<p class="p0 mt5">'+data.counsellor_visited_date+'<span class="ml15">By<strong class="anchor-blue pl10">'+data.counsellor_name+'</strong></span> </p>';
                            leadName+='</div>';
                        }*/
                        leadName+='</div></div>';
                        return leadName;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.course_name;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.lead_stage;
                    }
                },
                {
                    data: null, render: function ( data, type, row ) 
                    {

                        var company_name = (data.cName == null || data.cName == '') ? "" : '<p><lable class="clabel icon-label"><a class="dark-sky-blue tooltipped" data-position="top" data-tooltip="Company"  href="javascript:"><i class="icon-company ml5 mt2 f14"></i></a></lable>' + data.cName + '</p>';
                        var Qualificatin_name = (data.qualificationName == null || data.qualificationName == '') ? "" : '<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Qualification" class="dark-sky-blue tooltipped"><i class="icon-qualification ml5 mt2"></i></a></lable>' + data.qualificationName + '</p>';
                        var infoEmail = (data.email == null || data.email == '') ? "" : '<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Email" class="dark-sky-blue tooltipped"><i class="icon-mail ml5 mt2"></i></a></lable>' + data.email + '</p>';
                        var infoPhone = (data.phone == null || data.phone == '') ? "" : '<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Phone" class="dark-sky-blue tooltipped"><i class="icon-phone ml5 mt2"></i></a></lable>' + data.phone + '</p>';
                        var info = "";
                        info += infoEmail;
                        info += infoPhone;
                        //<p class="popper" data-toggle="popover" data-placement="top" data-trigger="hover" data-title="Last Call">
                        //info+='<div class="popper-content hide"> <span class="display-block mtb10">He is intrested but not very sure. He can join in Hyderabad. </span> <p>12 Jun,2016. <span class="ml15">By<strong class="anchor-blue pl10">Govind</strong></span> </p> </div></p>';
                        //info+=''+company_name+'';
                        info += '' + Qualificatin_name + '';
                        return info;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        /*var actionHtml=''
                        actionHtml+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        actionHtml+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                        actionHtml+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        actionHtml+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        actionHtml+='<li><a access-element="manage" href="'+BASE_URL+'app/leadinfo/'+data.lead_id+'">View</a></li>';
                        actionHtml+='<li><a access-element="counselling" href="javascript:;" onclick="manageCounsellorComments(this,'+data.branch_xref_lead_id+','+data.branch_visitor_id+')">Counselling</a></li>';
                        actionHtml+='</ul></div>';
                        return actionHtml;*/
                        var branch_comments='';
                        branch_comments+="<p>"+data.branch_visit_comments+"</p>";
                        branch_comments+="<p>"+data.counsellor_visited_date+" By ";
                        branch_comments+=""+data.branch_visit_name+"</p>";
                        return branch_comments;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        var counselor_comments='';
                        
                        if(data.is_counsellor_visited==1){
                            counselor_comments+="<p>"+data.second_level_counsellor_comments+"</p>";
                            counselor_comments+="<p>"+data.second_level_counsellor_updated_date+"";
                            counselor_comments+=" By "+data.counsellor_name+"</p>";
                        }
                        else{
                            counselor_comments+="<p>Pending</p>";
                            counselor_comments+="<p>"+data.counsellor_visited_date+"</p>";
                        }
                        return counselor_comments;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        var actionHtml;
                        actionHtml='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        actionHtml+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                        actionHtml+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        actionHtml+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        actionHtml+='<li><a access-element="counselling" href="javascript:;" onclick="manageCounsellorComments(this,'+data.branch_xref_lead_id+','+data.branch_visitor_id+')">Counselling</a></li>';
                        actionHtml+='</ul></div>';
                        return actionHtml;
                    }
                },
                
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function manageCounsellorComments(This,BranchXrefLeadId,BranchVisitorId) {
        $('#hdnBrachXrefLeadId').val(BranchXrefLeadId);
        $('#hdnBranchVisitorId').val(BranchVisitorId);
        var branchId=glbBranchId;
        var ajaxurl=API_URL+'index.php/CounsellorVisitors/getCounsellorVisitorInformation';
        var userID=$('#hdnUserId').val();
        var brachXrefLeadId=$('#hdnBrachXrefLeadId').val();
        var action='list';
        var params={branchId:branchId,brachXrefLeadId:brachXrefLeadId,hdnBranchVisitorId:BranchVisitorId};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response=
            commonAjaxCall({This:This, requestUrl:ajaxurl,params: params,headerParams:headerParams,action:action,onSuccess:manageCounsellorCommentsResponse});
        $(This).attr("data-target", "#counsellor-comment-modal");
        $(This).attr("data-toggle", "modal");

    }
    function manageCounsellorCommentsResponse(response){

        $('#ManageCounsellorComments')[0].reset();
        $('#ManageCounsellorComments label').removeClass("active");
        $('#ManageCounsellorComments span.required-msg').remove();
        $('#ManageCounsellorComments input').removeClass("required");
        $('#ManageCounsellorComments select').removeClass("required");
        $('#ManageCounsellorComments textarea').removeClass("required");

        $('#btnSaveCounsellorComments').attr('disabled',true);
        $('#viewlead_number').html('----');
        $('#viewlead_name').html('----');
        $('#viewlead_email').html('----');
        $('#viewlead_phone').html('----');
        $('#viewlead_branch_name').html('----');
        $('#viewlead_course_name').html('----');
        $('#viewlead_expectedVisitedDate').html('----');
        $('#alreadyCommentsExist').slideUp();
        $('#commentTypeWrapper').slideUp();
        if(response.status===true){
            if(response.data.length>0){

                if(response.data[0].lead_number){
                    $('#viewlead_number').html(response.data[0].lead_number);
                }
                if(response.data[0].name){
                    $('#viewlead_name').html(response.data[0].name);
                }
                if(response.data[0].email){
                    $('#viewlead_email').html(response.data[0].email);
                }
                if(response.data[0].phone){
                    $('#viewlead_phone').html(response.data[0].phone);
                }
                if(response.data[0].branch_name){
                    $('#viewlead_branch_name').html(response.data[0].branch_name);
                }
                if(response.data[0].course_name){
                    $('#viewlead_course_name').html(response.data[0].course_name);
                }
                if(response.data[0].lead_stage){
                    $('#viewlead_stage').html(response.data[0].lead_stage);
                }
                if(response.data[0].expected_visit_date){
                    $('#viewlead_expectedVisitedDate').html(response.data[0].expected_visit_date);
                }

                if(response.data[0].second_level_counsellor_comments){

                    if(response.data[0].second_level_counsellor_comments.trim()!=''){
                        $('#viewlead_comments').html(response.data[0].second_level_counsellor_comments);
                        $('#viewlead_comments_date').html(response.data[0].second_level_counsellor_updated_date);
                        $('#alreadyCommentsExist').slideDown();
                        $('#commentTypeWrapper').slideUp();
                    }
                    else{

                        $('#alreadyCommentsExist').slideUp();
                        $('#commentTypeWrapper').slideDown();
                    }
                }
                else{
                    $('#alreadyCommentsExist').slideUp();
                    $('#commentTypeWrapper').slideDown();
                    $('#btnSaveCounsellorComments').attr('disabled',false);
                }
            }
            else{
                notify("No lead details found",'error',10);
            }
        }
        else{
            notify(response.message,'error',10);
        }


    }
    function saveCounsellorComments(This){
        $('#ManageCounsellorComments input').removeClass("required");
        $('#ManageCounsellorComments select').removeClass("required");
        $('#ManageCounsellorComments textarea').removeClass("required");
        $('#ManageCounsellorComments span.required-msg').remove();
        var flag=0;
        var branchId=glbBranchId;
        var ajaxurl=API_URL+'index.php/CounsellorVisitors/saveCounsellorComments';
        var userID=$('#hdnUserId').val();
        var branchXrefLeadId=$('#hdnBrachXrefLeadId').val();
        var branchVisitorId=$('#hdnBranchVisitorId').val();
        var comments=$('#txtCounsellorComments').val();
        //var commentType=$('input[name=txtCommentType]:checked').val();
        //var enrollDate=$('#txtEnrollDate').val();
        //var visitDate=$('#txtVistedDate').val();
        /*if(commentType=='Positive'){
            if(enrollDate==''){
                $("#txtEnrollDate").addClass("required");
                $("#txtEnrollDate").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag = 1;
            }

        }else if(commentType=='Next Batch Postpone'){

        }
        else if(commentType=='Black List'){

        }*/
        if(comments==''){
            $("#txtCounsellorComments").addClass("required");
            $("#txtCounsellorComments").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if(flag==1){
            return false;
        }
        else {
            notify('Processing..', 'warning', 10);
            var action = 'list';
            var type = 'POST';
            var params = {
                branchId: branchId,
                branchXrefLeadId: branchXrefLeadId,
                branchVisitorId:branchVisitorId,
                comments: comments
                //commentType: commentType,
                //enrollDate: enrollDate,
                //visitDate: visitDate,
            };
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: userID
            };
            var response =
                commonAjaxCall({
                    This: This,
                    method: type,
                    requestUrl: ajaxurl,
                    params: params,
                    headerParams: headerParams,
                    action: action,
                    onSuccess: saveCounsellorCommentsResponse
                });
        }
    }
    function saveCounsellorCommentsResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            notify(response.message,'success',10);
            $('#counsellor-comment-modal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildCounsellorVisitorsListDataTable();
        }
        else{
            notify(response.message,'error',10);
        }
    }

    $('input[name=txtCommentType]').click(function(){
        $('#ManageCounsellorComments input').removeClass("required");
        $('#ManageCounsellorComments select').removeClass("required");
        $('#ManageCounsellorComments textarea').removeClass("required");
        $('#ManageCounsellorComments span.required-msg').remove();
        if($('input[name=txtCommentType]:checked').val()!='Positive'){

            $('#txtEnrollDateDiv').slideUp();
        }
        else{

            $('#txtEnrollDateDiv').slideDown();
        }

    });
</script>


