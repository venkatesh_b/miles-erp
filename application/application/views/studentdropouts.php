    <div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
        <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
        <div class="content-header-wrap">
            <h3 class="content-header">Student Dropouts</h3>
            <div class="content-header-btnwrap">
                <ul>
                    <li access-element="report"><a class="" href="javascript:" onclick="javascript:filterStudentDropoutsReportExport(this)"><i class="fa fa-file-excel-o"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- InstanceEndEditable -->
        <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
            <div class="fixed-wrap clearfix">
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Branch</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:" data-value="batch-filter"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:" data-value="batch-filter"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="branchFilter"></div>
                    </div>

                    <div class="col-sm-6 reports-filter-wrapper">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Course</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:" data-value="batch-filter"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:" data-value="batch-filter"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="courseFilter"></div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                    <!--<div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="input-field col-sm-11 p0 custom-multiselect-wrap col-sm-8">
                            <select multiple data-placeholder="Choose Batches" id="ddlBatchesList" class="chosen-select">
                                <option value="" disabled>--Choose Batch--</option>
                            </select>
                            <label class="select-label pl5">Batches</label>
                        </div>
                    </div>-->
                    <div class="col-sm-6 reports-filter-wrapper p0">
                        <div>
                            <label class="heading-uppercase">Batch Dates</label>
                        </div>
                        <div class="input-field col-sm-4 mt0 pl0">
                            <div class="input-field mt0">
                                <label class="datepicker-label datepicker">From Date </label>
                                <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtFromDate">
                            </div>
                        </div>
                        <div class="input-field col-sm-4 mt0 pl0">
                            <div class="input-field mt0">
                                <label class="datepicker-label datepicker">To Date </label>
                                <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtToDate">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 mt0 p15">
                        <a class="btn blue-btn btn-sm" href="javascript:" onclick="javascript:filterStudentDropoutsReport(this)">Proceed</a>
                    </div>
                </div>
                <div id="studentdropouts-report-list-wrapper" access-element="report">
                <div class="col-sm-12 clearfix mt15 p0">
                    <table class="table table-inside table-custom branchwise-report-table" id="studentdropouts-report-list">
                        <thead>
                        <tr class="">
                            <th>Branch</th>
                            <th>Enroll No.</th>
                            <th>Name</th>
                            <th>Batch</th>
                            <th class="text-right">Payable</th>
                            <th class="text-right">Paid</th>
                            <th class="text-right">Due From</th>
                        </tr>
                      </thead>

                    </table>
                </div>
                <div class="col-sm-12 clearfix p0">
                    <h4 class="mb30 heading-uppercase">Signatures</h4>
                    <div class="col-sm-4 mb30 pl0 ash">Accountant / Cashier</div>
                    <div class=" col-sm-4 ash">Branch Head</div>
                </div>
                </div>
            </div>

            <!-- InstanceEndEditable --></div>
    </div>

    <script>
        docReady(function () {
            $("#studentdropouts-report-list-wrapper").hide();
            studentDropoutsReportPageLoad();
        });
        var studentDropoutsConfigResponse='';
        $(document).ready(function () {

            $('input[name="batch_curr_prev"]').click(function () {
                var selectedCheck=[];
                $('input[name="batch_curr_prev"]').each(function () {
                    if ($(this).prop('checked')) {
                        selectedCheck.push($(this).val());
                    }

                });
                var html = '';
                if ($.inArray('current', selectedCheck) != -1)
                {
                    if(studentDropoutsConfigResponse.currentbatches.length > 0){
                        for(var a in studentDropoutsConfigResponse.currentbatches){
                            html += '<a class="active" data-id="'+studentDropoutsConfigResponse.currentbatches[a].batch_id+'" href="javascript:;">'+studentDropoutsConfigResponse.currentbatches[a].code+'</a>'
                        }

                    }
                }
                if ($.inArray('previous', selectedCheck) != -1)
                {
                    if(studentDropoutsConfigResponse.previousbatches.length > 0){
                        for(var a in studentDropoutsConfigResponse.previousbatches){
                            html += '<a class="active" data-id="'+studentDropoutsConfigResponse.previousbatches[a].batch_id+'" href="javascript:;">'+studentDropoutsConfigResponse.previousbatches[a].code+'</a>'
                        }

                    }
                }
                $('#batchFilter').html(html);
            });
        });

        /* Student Dropouts report starts here */
        function studentDropoutsReportPageLoad()
        {
            var userId = $('#userId').val();
            var action = 'report';
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

            var ajaxurl=API_URL+'index.php/StudentDropouts/getAllConfigurations';
            var params = {};
            commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
                var dashboard = '';
                //return false;
                if (response.data.length == 0 || response == -1 || response['status'] == false) {
                } else {
                    studentDropoutsConfigResponse=response.data;
                    var RawData = response.data;
                    var html = '';
                    if(RawData.branch.length > 0){
                        for(var a in RawData.branch){
                            html += '<a class="active" data-id="'+RawData.branch[a].id+'" href="javascript:;" data-value="batch-filter">'+RawData.branch[a].name+'</a>'
                        }
                        $('#branchFilter').html(html);
                    }
                    html = '';
                    if(RawData.course.length > 0){
                        for(var a in RawData.course){
                            html += '<a class="active" data-id="'+RawData.course[a].courseId+'" href="javascript:;" data-value="batch-filter">'+RawData.course[a].name+'</a>'
                        }
                        $('#courseFilter').html(html);
                    }
                    $('#ddlBatchesList').empty();
                    var batchesList= $.merge(RawData.previousbatches,RawData.currentbatches);
                    $('#ddlBatchesList').append($('<option></option>').val('').html('--Select All--'));
                    if (batchesList.length > 0)
                    {
                        $.each(batchesList, function (key, value)
                        {
                            $('#ddlBatchesList').append($('<option></option>').val(value.batch_id).html(value.code));
                        });
                    }
                    $("#ddlBatchesList").chosen({
                        no_results_text: "Oops, nothing found!",
                        width: "95%"
                    });
                    $('#ddlBatchesList').trigger("chosen:updated");
                    $("#ddlBatchesList").next('label').addClass("active");
                    buildpopover();
                }
            }});
        }
        function filterStudentDropoutsReport(This)
        {
            $("#txtFromDate").removeClass("required");
            $("#txtToDate").removeClass("required");
            $('#txtFromDate').parent().find('label').removeClass("active");
            $('#txtFromDate').parent().find('span.required-msg').remove();
            $('#txtToDate').parent().find('label').removeClass("active");
            $('#txtToDate').parent().find('span.required-msg').remove();
            var branchId = [];
            var courseId = [];
            var error = true;
            $("#branchFilter > .active").each(function() {
                error = false;
                branchId.push($(this).attr('data-id'));
            });
            $("#courseFilter > .active").each(function() {
                error = false;
                courseId.push($(this).attr('data-id'));
            });


            var branchId=branchId.join(', ');
            var courseId=courseId.join(', ');
            var fromDate=$('#txtFromDate').val();
            var toDate=$('#txtToDate').val();
            if(fromDate.trim()!='') {
                var txtfromDate = dateConvertion(fromDate);
            }
            if(toDate.trim()!='') {
                var txttoDate = dateConvertion(toDate);
            }
            /*var selMultiBatches = $.map($("#ddlBatchesList option:selected"), function (el, i)
            {
                if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All')
                {
                    //return $(el).text();
                    return $(el).val();
                }
            });
            var chosenBatchIds = selMultiBatches.join(",");*/

            if(branchId.trim()==''){
                error=true;
                alertify.dismissAll();
                notify('Select any one branch','error');
                return false;
            }
            else if(courseId.trim()==''){
                error=true;
                alertify.dismissAll();
                notify('Select any one course','error');
                return false;
            }
            else if($('#txtFromDate').val().trim()==''){
                error = true;
                alertify.dismissAll();
                notify('Select from date','error');
                return false;
            }
            else if($('#txtToDate').val().trim()==''){
                error = true;
                alertify.dismissAll();
                notify('Select to date','error');
                return false;

            }
            else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
                $("#txtFromDate").addClass("required");
                $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
                $("#txtToDate").addClass("required");
                $("#txtToDate").after('<span class="required-msg">Invalid</span>');
                alertify.dismissAll();
                notify('Invalid dates','error');
                error=true;
                return false;
            }
            else{
                error=false;
            }
            if(!error){
                var total=0;
                var pageTotal=0;

                var action = 'report';
                var ajaxurl=API_URL+'index.php/StudentDropouts/getStudentDropoutsReport';
                //var params = {'branch': branchId, 'course': courseId, 'batch': chosenBatchIds};
                var params = {'branch': branchId, 'course': courseId, fromDate:fromDate,toDate:toDate};
                var userID=$('#userId').val();
                var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
                $("#studentdropouts-report-list-wrapper").show();
                $("#studentdropouts-report-list").dataTable().fnDestroy();
                $tableobj = $('#studentdropouts-report-list').DataTable( {
                    "fnDrawCallback": function() {
                        $("#studentdropouts-report-list thead th").removeClass("icon-rupee");
                        var $api = this.api();
                        var pages = $api.page.info().pages;
                        if(pages > 1)
                        {
                            $('.dataTables_paginate').css("display", "block");
                            $('.dataTables_length').css("display", "block");
                            //$('.dataTables_filter').css("display", "block");
                        } else {
                            $('.dataTables_paginate').css("display", "none");
                            $('.dataTables_length').css("display", "none");
                            //$('.dataTables_filter').css("display", "none");
                        }
                        verifyAccess();
                        buildpopover();
                    },
                    dom: "Bfrtip",
                    bInfo: false,
                    "serverSide": false,
                    "oLanguage":
                    {
                        "sSearch": "<span class='icon-search f16'></span>",
                        "sEmptyTable": "No Records Found",
                        "sZeroRecords": "No Records Found",
                        "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                    },
                    "bProcessing": true,
                    ajax: {
                        url:ajaxurl,
                        type:'GET',
                        data:params,
                        headers:headerParams,
                        error:function(response) {
                            DTResponseerror(response);
                        }
                    },
                    columns: [
                        {
                            data: null, render: function ( data, type, row )
                        {
                            return data.branchName;
                        }
                        },
                        {
                            data: null, render: function ( data, type, row )
                        {
                            return data.enrollNo;
                        }
                        },
                        {
                            data: null, render: function ( data, type, row )
                        {

                            return data.leadName;
                        }
                        },
                        {
                            data: null, render: function ( data, type, row )
                        {

                            return data.batchCode;
                        }
                        },
                        {
                            data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                        {
                            return moneyFormat(data.amountPayable);
                        }
                        },
                        {
                            data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                        {
                            return moneyFormat(data.amountPaid);
                        }
                        },
                        {
                            data: null, render: function ( data, type, row )
                        {
                            var dueDayhtml='';
                            var dueDays=data.dueDays;
                            if(dueDays==0){
                                dueDayhtml+="<div class=\"text-right rprt-nodue\">";
                                dueDayhtml+=Math.abs(dueDays)+' Days';
                                dueDayhtml+="</div>";
                            }
                            else if(dueDays<0){
                                dueDayhtml+="<div class=\"text-right rprt-due\">";
                                dueDayhtml+=Math.abs(dueDays)+' Days <span class="f10"><i class="icon-arrow-down"></i></span>';
                                dueDayhtml+="</div>";
                            }
                            else if(dueDays>0){
                                dueDayhtml+="<div class=\"text-right rprt-todaydue\">";
                                dueDayhtml+=Math.abs(dueDays)+' Days <span class="f10"><i class="icon-arrow-up"></i></span>';
                                dueDayhtml+="</div>";
                            }
                            return dueDayhtml;
                        }
                        }

                    ]
                } );
                DTSearchOnKeyPressEnter();

            } else {
                alertify.dismissAll();
                notify('Select any filter', 'error', 10);
            }
        }
        function filterStudentDropoutsReportExport(This){
            $("#txtFromDate").removeClass("required");
            $("#txtToDate").removeClass("required");
            $('#txtFromDate').parent().find('label').removeClass("active");
            $('#txtFromDate').parent().find('span.required-msg').remove();
            $('#txtToDate').parent().find('label').removeClass("active");
            $('#txtToDate').parent().find('span.required-msg').remove();
            var branchId = [];
            var branchIdText = [];
            var courseId = [];
            var courseIdText = [];
            var error = true;
            var fromDate=$('#txtFromDate').val();
            var toDate=$('#txtToDate').val();
            if(fromDate.trim()!='') {
                var txtfromDate = dateConvertion(fromDate);
            }
            if(toDate.trim()!='') {
                var txttoDate = dateConvertion(toDate);
            }
            $("#branchFilter > .active").each(function() {
                error = false;
                branchId.push($(this).attr('data-id'));
                branchIdText.push($(this).text());
            });
            $("#courseFilter > .active").each(function() {
                error = false;
                courseId.push($(this).attr('data-id'));
                courseIdText.push($(this).text());
            });

            var branchId=branchId.join(', ');
            var courseId=courseId.join(', ');
            if(fromDate.trim()!='') {
                var txtfromDate = dateConvertion(fromDate);
            }
            if(toDate.trim()!='') {
                var txttoDate = dateConvertion(toDate);
            }
            /*var selMultiBatchIds = $.map($("#ddlBatchesList option:selected"), function (el, i)
            {
                if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All')
                {
                    return $(el).val();
                }
            });
            var selMultiBatchNames = $.map($("#ddlBatchesList option:selected"), function (el, i)
            {
                if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All')
                {
                    return $(el).text();
                }
            });
            var chosenBatchIds = selMultiBatchIds.join(",");
            var chosenbatchNames = selMultiBatchNames.join(",");*/
            /*else if(chosenBatchIds.length==0){
                error=true;
                alertify.dismissAll();
                notify('Select any one batch','error');
                return false;
            }*/
            if(branchId.trim()==''){
                error=true;
                alertify.dismissAll();
                notify('Select any one branch','error');
                return false;
            }
            else if(courseId.trim()==''){
                error=true;
                alertify.dismissAll();
                notify('Select any one course','error');
                return false;
            }
            else if($('#txtFromDate').val().trim()==''){
                error = true;
                alertify.dismissAll();
                notify('Select from date','error');
                return false;
            }
            else if($('#txtToDate').val().trim()==''){
                error = true;
                alertify.dismissAll();
                notify('Select to date','error');
                return false;

            }
            else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
                $("#txtFromDate").addClass("required");
                $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
                $("#txtToDate").addClass("required");
                $("#txtToDate").after('<span class="required-msg">Invalid</span>');
                alertify.dismissAll();
                notify('Invalid dates','error');
                error=true;
                return false;
            }
            else{
                error=false;
            }

            if(!error){
                var total=0;
                var pageTotal=0;
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                var action = 'report';
                var ajaxurl=API_URL+'index.php/StudentDropouts/getStudentDropoutsReportExport';
                //var params = {'branch': branchId, 'course': courseId, 'batch': chosenBatchIds,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '), 'batchText': chosenbatchNames};
                var params = {'branch': branchId, 'course': courseId, fromDate:fromDate,toDate:toDate,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', ')};
                var userID=$('#userId').val();
                var type='GET';
                var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
                var response =
                    commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterStudentDropoutsReportExportResponse});
            }
            else
            {
                alertify.dismissAll();
                notify('Select any filter', 'error', 10);
            }
        }

        function filterStudentDropoutsReportExportResponse(response)
        {
            alertify.dismissAll();
            if(response.status===true)
            {
                var download=API_URL+'Download/downloadReport/'+response.file_name;
                window.location.href=(download);
            }
            else
            {
                notify(response.message,'error');
            }
        }
        /* Student Dropouts report ends here */


    </script>

