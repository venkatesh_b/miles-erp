<?php
/**
 * User: Abhilash
 */
?>
  <div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Students</h3>

        <div class="content-header-btnwrap">
            <ul>
                <li access-element="list"><a class="" href="javascript:" onclick="javascript:StudentsListExport(this)"><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
            <div class="fixed-wrap clearfix">
                <div class="wrap-left col-sm-12 p0">
                    <div access-element="list" class="col-sm-12 p0" id="Students-list-wrapper">
                        <table class="table table-responsive table-striped table-custom" id="Students-list">
                            <thead >
                                <tr class="">
                                  <th>Name</th>
                                  <th>Enroll No.</th>
                                  <th>Batch</th>
                                  <th>Course</th>
                                  <th>Last Follow-up By</th>
                                  <th>Last Follow-up Date</th>
                                  <th class="no-sort border-r-none">Info</th>
                                  <th class="no-sort"> </th>
                                </tr>
                      		</thead>
                        </table>

                    </div>
                </div>

            </div>
            <!-- InstanceEndEditable --></div>
  </div>
<script type="text/javascript">
    var glbBranchId = '';
    var glbBranchText = '';
    docReady(function(){
        glbBranchId = $('#userBranchList').val();
        glbBranchText = $('#userBranchList option:selected').text();
        buildStudentsDataTable();
    });
    var feeDetailsConfigResponse='';
    
    $("#userBranchList" ).change(function() {
        glbBranchId = $('#userBranchList').val();
        glbBranchText = $('#userBranchList option:selected').text();
        buildStudentsDataTable();
        changeDefaultBranch();
    });

    function buildStudentsDataTable() {
        $('#Students-list-wrapper').show();

        var ajaxurl = API_URL + 'index.php/Students/getStudentsList';
        var userID = $('#hdnUserId').val();
        var params = {branch: glbBranchId};
        var action='list';
        var headerParams = {
            action: action,
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: userID
        };
        $("#Students-list").dataTable().fnDestroy();
        $tableobj = $('#Students-list').DataTable({
            "fnDrawCallback": function () {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1){
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage": {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url: ajaxurl,
                type: 'GET',
                headers: headerParams,
                data: params,
                error: function (response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false
            }],
            "order": [[ 0, "desc" ]],
            columns: [
                {
                    width:"15%",
                    data: null, render: function (data, type, row)
                    {
                        var leadName = "";
                        var leadIcon='male-icon.jpg';
                        if(data.gender==0){
                            var leadIcon='female-icon.jpg';
                        }
                        leadName += '<div class="img-wrapper img-wrapper-leadinfo light-blue3-bg" style="width:70px;height:63px;margin-left:-8px; position:absolute;top:50%;margin-top:-32px"><a class=\'tooltipped\' data-position="top" data-tooltip="Click here for student info" href="'+BASE_URL+'app/studentinfo/'+data.branch_xref_lead_id+'/'+$context+'-'+$pageUrl+'-list"><img src="<?php echo BASE_URL; ?>assets/images/'+leadIcon+'" style="width:33px;height:33px"><span class="img-text" style="word-break:break-word">' + data.lead_number + '</span></a></div>';
                        leadName += '<div style="padding-left:85px"><div class="f14 font-bold">';
                        leadName += data.lead_name;

                        leadName += '</div>';
                        leadName += '</div>';
                        return leadName;
                    }
                },
                {
                    width: "12%",
                    data: null, render: function (data, type, row)
                    {
                        return data.lead_enroll_number;
                    }
                },
                {
                    width: "12%",
                    data: null, render: function (data, type, row)
                    {
                        if(data.alias_name == null || data.alias_name == '')
                            return data.batch_code;
                        else
                            return data.batch_code+"("+data.alias_name+")";
                    }
                },
                {
                    width: "8%",
                    data: null, render: function (data, type, row)
                    {
                        return data.course_name;
                    }
                },
                {
                    width: "15%",
                    data: null, render: function (data, type, row)
                    {
                        return data.followup_username;
                    }
                },
                {
                    width: "15%",
                    data: null, render: function (data, type, row)
                    {
                        return data.followup_date;
                    }
                },
                {
                    width: "25%",
                    data: null, render: function (data, type, row)
                    {
                        var company_name = (data.company == null || data.company == '') ? "" :'<p><lable class="clabel icon-label"><a class="dark-sky-blue tooltipped" data-position="top" data-tooltip="Company"  href="javascript:"><i class="icon-company ml5 mt2 f14"></i></a></lable>'+data.company+'</p>';
                        var Qualificatin_name = (data.qualification == null || data.qualification == '') ? "" :'<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Qualification" class="dark-sky-blue tooltipped"><i class="icon-qualification ml5 mt2"></i></a></lable>'+data.qualification+'</p>';
                        var infoEmail=(data.email == null || data.email == '') ? "" :'<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Email" class="dark-sky-blue tooltipped"><i class="icon-mail ml5 mt2"></i></a></lable>'+data.email+'</p>';
                        var infoPhone=(data.phone == null || data.phone == '') ? "" :'<p><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Phone" class="dark-sky-blue tooltipped"><i class="icon-phone ml5 mt2"></i></a></lable>'+data.phone+'</p>';
                        var info="";
                        info+=infoEmail;
                        info+=infoPhone;
                        info+=''+Qualificatin_name+'';
                        return info;
                    }
                },
                {
                    width: "8%",
                    data: null, render: function ( data, type, row )
                    {
                        var view='';
                        view+='<div class="leads-icons pull-right" style="width: 40px;">';
                        if(data.comments.length>0)
                        {
                            view+='<a href="javascript:" class="popper" data-toggle="popover" data-placement="left" data-trigger="click" data-title="Comments"><img src="<?php echo BASE_URL; ?>assets/images/comment.png"></a>';
                            view+='<div class="popper-content hide">';
                        }
                        if(data.comments.length>0)
                        {
                            $.each(data.comments, function (key, value)
                            {
                                if(value.branch_visit_comments==null || value.branch_visit_comments.trim()=='')
                                    value.branch_visit_comments='----';
                                view+='<div class="border-bottom plr15 pb20 mb5">';
                                view+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">First Level Comments</h4>';
                                view+= '<p class="p0">'+value.branch_visit_comments+'</p>';
                                view+= '<p class="p0 mt5 ash">'+value.branch_visited_date+'<span class="ml15">By<span class="anchor-blue pl10">'+value.counseollor_name+'</span></span> </p>';
                                view+='</div>';
                                view+='<div class="border-bottom plr15 pb20 mb5">';
                                view+='<h4 class="col-md-12 anchor-blue p0 mt5 mb5">Second Level Comments</h4>';
                                view+= '<p class="p0">'+value.second_level_comments+'</p>';
                                view+= '<p class="p0 mt5 ash">'+value.counsellor_visited_date+'<span class="ml15">By<span class="anchor-blue pl10">'+value.second_counseollor_name+'</span></span> </p>';
                                view+='</div>';
                            });
                        }
                        if(data.comments.length>0)
                        {
                            view+='</div>';
                        }
                        
                        view+='</div>';
                        return view;
                    }
                 }
            ]
        });
        DTSearchOnKeyPressEnter();
    }
    function StudentsListExport(This)
    {
        notify('Processing..', 'warning', 10);
        var branchId = glbBranchId;
        var branchIdText = glbBranchText;
        var ajaxurl = API_URL + 'index.php/Students/getStudentsListExcel';
        var userID = $('#hdnUserId').val();
        var params = {branch: branchId,'branchText': branchIdText};
        var action = 'list';
        var type='GET';
        var headerParams = {
            action: action,
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: userID
        };
        commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: StudentsListExportResponse});
    }
    function StudentsListExportResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            alertify.dismissAll();
            var download=API_URL+'Download/downloadReport/'+response.file_name;
            window.location.href=(download);
        }
        else{
            notify(response.message,'error');
        }
    }

</script>