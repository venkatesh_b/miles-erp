<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 21/10/16
 * Time: 5:25 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Telecaller Report</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="report"><a class="" onclick="javascript:filterBranchWiseTelecallerReportExport(this)" href="javascript:;"><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <div class="col-sm-12 reports-filter-wrapper pl0">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Branch</label>
                    </div>
                    <div class="boxed-tags radio-tags" id="branchFilter"></div>
                </div>
            </div>
            <div class="col-sm-12 p0">
                <div class="col-sm-6 reports-filter-wrapper p0">
                    <div>
                        <label class="heading-uppercase">Dates</label>
                    </div>
                    <div class="input-field col-sm-4 mt0 pl0">
                        <div class="input-field mt0">
                            <label class="datepicker-label datepicker">From Date </label>
                            <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtFromDate">
                        </div>
                    </div>
                    <div class="input-field col-sm-4 mt0 pl0">
                        <div class="input-field mt0">
                            <label class="datepicker-label datepicker">To Date </label>
                            <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtToDate">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 mt15 p0">
                <a access-element="report" class="btn blue-btn btn-sm" href="javascript:;" onclick="filterBranchWiseTelecallerReport(this)">Proceed</a>
            </div>

            <div class="col-sm-12 clearfix p0">
                <table class="table table-inside table-custom branchwise-report-table" id="report-list" style="display:none;">
                    <thead>
                    <tr>
                        <th>Employee Name</th>
                        <th>Call Scheduled</th>
                        <th>Calls Due</th>
                        <th>Converted To Leads</th>
                        <th>DND</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>

<script>
    docReady(function () {
        $("#report-list").hide();
        teleCallerReportPageLoad();
    });
    /* lead branchwise report */
    function teleCallerReportPageLoad()
    {
        var userId = $('#userId').val();
        var action = 'report';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

        var ajaxurl=API_URL+'index.php/TelecallerReport/getAllConfigurationsByUserId';
        var params = {};
        commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
            var dashboard = '';
            if (response.data.length == 0 || response == -1 || response['status'] == false)
            {
                notify('There are no branches','error');
            }
            else
            {
                var RawData = response.data;
                var html = '';
                if(RawData.branch.length > 0)
                {
                    for(var i=0;i<RawData.branch.length;i++)
                    {
                        if(i==0)
                            html += '<a class="active" data-id="'+RawData.branch[i].id+'" href="javascript:;">'+RawData.branch[i].name+'</a>';
                        else
                            html += '<a data-id="'+RawData.branch[i].id+'" href="javascript:;">'+RawData.branch[i].name+'</a>';
                    }
                    $('#branchFilter').html(html);
                }
                buildpopover();
            }
        }});
    }

    function filterBranchWiseTelecallerReport(This){
        var branchId = [];
        $("#txtFromDate").removeClass("required");
        $("#txtToDate").removeClass("required");
        $('#txtFromDate').parent().find('label').removeClass("active");
        $('#txtFromDate').parent().find('span.required-msg').remove();
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();
        var fromDate=$('#txtFromDate').val();
        var toDate=$('#txtToDate').val();
        if(fromDate.trim()!='') {
            var txtfromDate = dateConvertion(fromDate);
        }
        if(toDate.trim()!='') {
            var txttoDate = dateConvertion(toDate);
        }
        var error = true;
        $("#branchFilter > .active").each(function() {
            error = false;
            branchId.push($(this).attr('data-id'));
        });

        var branchId=branchId.join(', ');

        if(branchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one branch','error');
            return false;
        }
        else if($('#txtFromDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select from date','error');
            return false;
        }
        else if($('#txtToDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select to date','error');
            return false;
        }
        else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
            $("#txtFromDate").addClass("required");
            $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">Invalid</span>');
            alertify.dismissAll();
            notify('Invalid dates','error');
            error=true;
            return false;
        }
        else{
            error=false;
        }

        if(!error){
            var total=0;
            var pageTotal=0;
            var action = 'report';
            var ajaxurl=API_URL+'index.php/TelecallerReport/getTelecallerReport';
            var params = {'branch': branchId,fromDate:fromDate,toDate:toDate};
            var userID=$('#userId').val();
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

            $("#report-list").show();
            $("#report-list").dataTable().fnDestroy();
            $tableobj = $('#report-list').DataTable( {
                "fnDrawCallback": function() {
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                    }
                    else
                    {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                    }
                    verifyAccess();
                    buildpopover();
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": false,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No Records Found",
                    "sZeroRecords": "No Records Found",
                    "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
                },
                "bProcessing": true,
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    data:params,
                    headers:headerParams,
                    error:function(response) {
                        DTResponseerror(response);
                    }
                },
                columns: [
                    {
                        data: null, render: function ( data, type, row )
                        {
                            return data.EmployeeName;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            if(data.calls_scheduled == null || data.calls_scheduled == '')
                                data.calls_scheduled =0;
                            return data.calls_scheduled;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            if(data.calls_due == null || data.calls_due == '')
                                data.calls_due =0;
                            return data.calls_due;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            if(data.converted_leads == null || data.converted_leads == '')
                                data.converted_leads =0;
                            return data.converted_leads;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            if(data.dnd == null || data.dnd == '')
                                data.dnd =0;
                            return data.dnd;
                        }
                    }
                ]
            } );
            DTSearchOnKeyPressEnter();

        } else {
            alertify.dismissAll();
            notify('Select any filter', 'error', 10);
        }
    }

    function filterBranchWiseTelecallerReportExport(This){
        var branchId = [];
        var branchIdText = [];
        $("#txtFromDate").removeClass("required");
        $("#txtToDate").removeClass("required");
        $('#txtFromDate').parent().find('label').removeClass("active");
        $('#txtFromDate').parent().find('span.required-msg').remove();
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();
        var fromDate=$('#txtFromDate').val();
        var toDate=$('#txtToDate').val();
        if(fromDate.trim()!='') {
            var txtfromDate = dateConvertion(fromDate);
        }
        if(toDate.trim()!='') {
            var txttoDate = dateConvertion(toDate);
        }
        var error = true;
        $("#branchFilter > .active").each(function() {
            error = false;
            branchId.push($(this).attr('data-id'));
            branchIdText.push($(this).text());
        });
        var branchId=branchId.join(', ');

        if(branchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one branch','error');
            return false;
        }
        else if($('#txtFromDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select from date','error');
            return false;
        }
        else if($('#txtToDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select to date','error');
            return false;
        }
        else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
            $("#txtFromDate").addClass("required");
            $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">Invalid</span>');
            alertify.dismissAll();
            notify('Invalid dates','error');
            error=true;
            return false;
        }
        else{
            error=false;
        }

        if(!error){
            var total=0;
            var pageTotal=0;
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var action = 'report';
            //var ajaxurl=API_URL+'index.php/BranchReport/getBranchReportExport';
            //var params = {'branch': branchId, 'course': courseId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', ') };
            var ajaxurl=API_URL+'index.php/TelecallerReport/getTelecallerReportExport';
            var params = {'branch': branchId,'branchText': branchIdText.join(', '),fromDate:fromDate,toDate:toDate};
            var userID=$('#userId').val();
            var type='GET';
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            var response =
                commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterBranchWiseTelecallerReportExportResponse});


        } else {
            alertify.dismissAll();
            notify('Select any filter', 'error', 10);
        }
    }

    function filterBranchWiseTelecallerReportExportResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            var download=API_URL+'Download/downloadReport/'+response.file_name;
            window.location.href=(download);

        }
        else{
            notify(response.message,'error');
        }
    }
    /*lead branchwise report ends here */

</script>