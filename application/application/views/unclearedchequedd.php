<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">UNCLEARED CHEQUE / DD REPORT</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="report"><a class="" href="javascript:" onclick="javascript:filterUnclearedChequeListReportExport(this)"><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Branch</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="branchFilter"></div>
                    </div>
                    <div class="col-sm-6 reports-filter-wrapper p0">
                        <div class="multiple-checkbox reports-filters-check mb5 mt10">
                        </div>
                        <div class="input-field col-sm-4 mt0 p0">
                            <input id="days" class="validate m0" type="text">
                            <label class="" for="days">No. of Days</label>
                        </div>
                    </div>
                </div>
            <div class="col-sm-12 p0">
            <div class="col-sm-6 reports-filter-wrapper pl0">
                <div class="multiple-checkbox reports-filters-check">
                    <label class="heading-uppercase">Mode</label>
                </div>
                <div>
                    <span class="inline-radio">
                   		 <input id="receive_date" class="with-gap" type="radio" checked="" name="group1">
                   		 <label class="bblue-text" for="receive_date">Received Date</label>
                    </span>
                    <span class="inline-radio">
                   		 <input id="post_date" class="with-gap" type="radio" name="group1">
                   		 <label class="bblue-text" for="post_date">Posting Date</label>
                    </span>
                </div>
            </div>

            <div class="col-sm-6 mt0 p0">
                <a class="btn blue-btn btn-sm" href="javascript:" onclick="javascript:filterUnclearedChequeReport(this)">Proceed</a>
            </div>
                </div>
            <div id="UnclearedCheque-report-list-wrapper" access-element="report">
                <div class="col-sm-12 clearfix mt15 p0">
                    <table class="table table-inside table-custom branchwise-report-table" id="UnclearedCheque-report-list">
                        <thead>
                        <tr class="">
                            <th>Student No.</th>
                            <th>Name</th>
                            <th>Receipt No.</th>
                            <th>Remarks</th>
                            <th>Cheque / DD Details</th>
                            <th>Receipt Date</th>
                            <th>Posted By</th>
                            <th>Posted Date</th>
                            <th>Posted Bank</th>
                            <th>Posted Voucher No.</th>
                        </tr>
                        </thead>

                    </table>
                </div>
                <div class="col-sm-12 clearfix p0">
                    <h4 class="mb30 heading-uppercase">Signatures</h4>
                    <div class="col-sm-4 mb30 pl0 ash">Accountant / Cashier</div>
                    <div class=" col-sm-4 ash">Branch Head</div>
                </div>
            </div>
        </div>

        <!-- InstanceEndEditable --></div>
</div>

<script>
    docReady(function () {
        $("#UnclearedCheque-report-list-wrapper").hide();
        UnclearedChequeListReportPageLoad();
    });
</script>

