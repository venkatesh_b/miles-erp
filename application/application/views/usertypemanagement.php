<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->

    <div class="content-header-wrap">
        <h3 class="content-header">User Type </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="viewsidePanel"  data-target="#countryModal" data-toggle="modal"><i class="icon-plus-circle" ></i></a></li>
                <!--<li><a></a></li>-->
            </ul>
        </div>   
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" > <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0 mb20 branches-list-view">

            </div>

            <div class="col-sm-12 p0" access-element="list">
                <table class="table table-responsive table-striped table-custom table-info">
                    <thead>
                        <tr>
                            <th>User Type</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody id="countryList">

                    </tbody>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeUserTypeModal(this)"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="countryModalLabel">Create User Type</h4>
                    </div>
                    <form action="/" method="post" id="countryForm" novalidate="novalidate">
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="UserTypeName" type="text" name="UserTypeName" class="validate" class="formSubmit" required>
                                    <label for="UserTypeName">User Type Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="UserTypeDescription" name="UserTypeDescription" class="materialize-textarea"></textarea>
                                    <label for="UserTypeDescription">Description</label>
                                </div>
                            </div> 
                        </div>
                        <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
                        <input type="hidden" id="reference_type_val_id" name="reference_type_val_id" value=""/>
                    </form>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue-btn" onclick="AddUserType(this)"><i class="icon-right mr8"></i>Add</button>
                        <button type="button" class="btn blue-light-btn" onclick="closeUserTypeModal(this)"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        userTypeManagementPageLoad();
    });
    
    function userTypeManagementPageLoad()
    {
        var userId = <?php echo $userData['userId']; ?>;
        var ajaxurl=API_URL+'index.php/UserType/getUserType';
        var params = {};
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        var response=
        commonAjaxCall({This:this, requestUrl:ajaxurl, headerParams:headerParams,action:'list',onSuccess:userTypeWidget});
    }
    
    function userTypeWidget(response)
    {
        var dashboard1 = '';
        if (response == - 1 || response['status'] == false)
        {
        dashboard1 += '<tr class="" ><td colspan="2" class="text-center">No Data found</td></tr>';
        }
        else
        {
            if (response.data.length> 0)
            {
                for (var a in response.data)
                {
                    var desc = response.data[a]['typeDescription']==null?'---':response.data[a]['typeDescription'];
                    dashboard1 += '<tr class="">'
                                    + '<td>' + response.data[a]['typeName'] + '</td>'
                                    + '<td>' + desc + ''
                                    + ''
                                    +'<div class="dropdown feehead-list custom-dropdown-style pull-right">'
                                    +'<a id="dLabel" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'
                                    +'<i class="fa fa-ellipsis-v f18 pr10 pl10"></i>'
                                    +'</a>'
                                    +'<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">'
                                    +'<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>'
                                    +'<li access-element="manage"><a href="javascript:;" onclick="getUserTypeDetails(this, ' + response.data[a]['typeId'] + ')">Edit</a></li>'
                                    +'</ul>'
                                    +'</div>'
                                    + '</td>'
                                +'</tr>';
                }
            }
            else
            {
                dashboard1 += '<tr class="" ><td colspan="3" class="text-center">No Data found</td></tr>';
            }
        }
        $('#countryList').html(dashboard1);
    }
</script>