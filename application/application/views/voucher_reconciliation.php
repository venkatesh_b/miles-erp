<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 4/8/16
 * Time: 3:50 PM
 */
?>
<div class="contentpanel" id="voucherview"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Voucher Reconciliation</h3>
    </div>
    <div class="content-body" id="contentBody">
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <div class="col-sm-6 reports-filter-wrapper pl0">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Branch</label>
                    </div>
                    <div class="boxed-tags radio-tags" id="branchFilter">

                    </div>
                </div>
                <div class="col-sm-6 reports-filter-wrapper pl0">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Fee Company</label>
                        <!--<span class="filter-selectall-wrap">
                            <a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a>
                            <a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a>
                        </span>-->
                    </div>
                    <div class="boxed-tags radio-tags" id="companyFilter"></div>
                </div>
            </div>
            <div class="col-sm-12 p0">
                <div class="col-sm-6 reports-filter-wrapper pl0">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Mode of Payment</label>
                    </div>
                    <div class="boxed-tags radio-tags" id="feePaymentType">
                        <a data-value="cash" class="active">Cash</a>
                        <a data-value="other">Other</a>
                    </div>
                </div>
                <div class="col-sm-6 reports-filter-wrapper pl0">
                    <!--<div class="multiple-checkbox reports-filters-check mb5">
                        <label class="heading-uppercase">Reconciliation Date</label>
                    </div>-->
                    <div class="input-field col-sm-6 mt0 p0">
                        <input id="txtReconcelationDate" type="date" placeholder="dd/mm/yyyy" class="datepicker relative enablePastDatesWithCurrentDate" />
                        <label class="datepicker-label datepicker" for="txtReconcelationDate">Reconciliation Date</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p0">
                <div class="col-sm-6 mt0 p0">
                    <a class="btn blue-btn btn-sm" href="javascript:" onclick="showVoucherReconcelation()">Proceed</a>
                </div>
            </div>

            <div class="col-sm-12 p0 mtb10 hidden" id="voucher-reconcel-data">
                <table class="table table-responsive table-striped table-custom" id="voucher-reconcel-list">
                    <thead>
                        <tr>
                            <th>Payment Mode</th>
                            <th>Company</th>
                            <th class="text-right">Amount</th>
                            <th>Reconciliation Amount</th>
                            <th>Voucher</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="col-sm-6 mt0 p0 hidden" id="btnSubmitReconcel">
                    <a class="btn blue-btn btn-sm" href="javascript:" onclick="submitVoucherReconcelation()">Submit</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $context = 'Voucher Reconciliation';
    $pageUrl = 'voucher_reconciliation';
    $serviceurl = 'voucher_reconciliation';
    docReady(function () {
        VoucherReconcelationPageLoad();
    });
    function VoucherReconcelationPageLoad()
    {
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/VoucherReconcelation/getConfigList';
        commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess:VoucherReconcelationConfigResponse });
    }
    function VoucherReconcelationConfigResponse(response)
    {
        var branchesList=response['data']['Branches'];
        var companiesList=response['data']['Companies'];
        var feeCompanies='',branches='';
        for(var i=0;i<branchesList.length;i++)
        {
            if(i==0)
                branches+='<a data-id="'+branchesList[i]['branch_id']+'" class="active">';
            else
                branches+='<a data-id="'+branchesList[i]['branch_id']+'">';
            branches+=branchesList[i]['name'];
            branches+='</a>';
        }
        for(var i=0;i<companiesList.length;i++)
        {
            if(i==0)
                feeCompanies+='<a data-id="'+companiesList[i]['company_id']+'" class="active">';
            else
                feeCompanies+='<a data-id="'+companiesList[i]['company_id']+'">';
            feeCompanies+=companiesList[i]['name'];
            feeCompanies+='</a>';
        }
        $("#branchFilter").html(branches);
        $("#companyFilter").html(feeCompanies);
    }
    function showVoucherReconcelation()
    {
        alertify.dismissAll();
        var userId = $('#hdnUserId').val();
        var selectedBranch=$("#branchFilter > .active").attr('data-id');
        var selectedFeeCompany=$("#companyFilter > .active").attr('data-id');
        var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
        var selectedRevenueDate=$("#txtReconcelationDate").val();
        /*console.log(selectedBranch,selectedFeeCompany,selectedPaymentType,selectedRevenueDate);
        return false;*/
        if(selectedBranch == '' || selectedBranch == undefined)
        {
            notify('Select any one branch','error');
            return false;
        }
        else if(selectedFeeCompany == '' || selectedFeeCompany == undefined)
        {
            notify('Select any one company','error');
            return false;
        }
        else if(selectedPaymentType == '' || selectedPaymentType == undefined)
        {
            notify('Select any one payment type','error');
            return false;
        }
        else
        {
            $('#voucher-reconcel-data').removeClass('hidden');
            var action = 'list';
            var ajaxurl = API_URL + 'index.php/VoucherReconcelation/getReconcelationList';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var params = {branchId:selectedBranch,companyId:selectedFeeCompany,PaymentMode:selectedPaymentType,selectedDate:selectedRevenueDate};
            commonAjaxCall({This: this, headerParams: headerParams, params:params,requestUrl: ajaxurl, action: action, onSuccess:VoucherReconcelationResponse });
        }
    }
    function VoucherReconcelationResponse(response)
    {
        if(response.status == false)
        {
            notify('Something went wrong. Please try again','error');
        }
        else
        {
            var reconcelData=response.data['reconcelations'];
            var vouchersData=response.data['vouchers'];
            if(vouchersData.length == 0)
            {
                notify('No vouchers present for selected date','error');
                $('#voucher-reconcel-data').addClass('hidden');
                $("#btnSubmitReconcel").addClass("hidden");
            }
            else if(reconcelData.length == 0)
            {
                $("#voucher-reconcel-list >tbody").html("<tr><td colspan='5' class='text-center'>No records present</td></tr>");
                $("#btnSubmitReconcel").addClass("hidden");
            }
            else
            {
                $("#btnSubmitReconcel").removeClass("hidden");
                var reconcel_content='';
                for(var i=0;i<reconcelData.length;i++)
                {
                    reconcel_content+='<tr>';
                    reconcel_content+='<td>'+reconcelData[i].payment_mode+'</td>';
                    reconcel_content+='<td>'+reconcelData[i].fee_company+'</td>';
                    reconcel_content+='<td class="text-right icon-rupee">'+moneyFormat(reconcelData[i].amount_paid)+'</td>';
                    reconcel_content+='<td>';
                    reconcel_content+='<input name="hdnPaidAmount['+i+']" type="hidden" value="'+reconcelData[i].amount_paid+'">';
                    reconcel_content+='<input name="hdnFeeCollection['+i+']" type="hidden" value="'+reconcelData[i].fee_collection_id+'">';
                    reconcel_content+='<input name="reconcelAmount['+i+']" class="modal-table-textfiled m0" type="text" maxlength="10" onkeypress="return isNumberKey(event)"/>';
                    reconcel_content+='</td>';
                    reconcel_content+='<td>';
                    reconcel_content+='<select class="voucherlist select-dropdown modal-table-selectfield m0" name="ddlVoucher['+i+']">';
                    reconcel_content+='<option value="">--Choose voucher--</option>';
                    for(var v=0;v<vouchersData.length;v++)
                    {
                        reconcel_content+='<option value="'+vouchersData[v].voucher_id+'" data-amount="'+vouchersData[v].amount+'">'+vouchersData[v].name+'</option>';
                    }
                    reconcel_content+='</select>';
                    reconcel_content+='</td>';
                    reconcel_content+='</tr>';
                }
                $("#voucher-reconcel-list >tbody").html(reconcel_content);
                $('select.voucherlist').material_select();
            }
        }
    }
    function submitVoucherReconcelation()
    {
        alertify.dismissAll();
        var index=0,reconcel_amt=0,paid_amt= 0,voucher_number= 0,total_reconcel_amount= 0,total_voucher_amt= 0,used_voucher=new Array(),reconcelation_records=new Array(),flag= 0,rc_flag= 0,reconce_validation=0;
        $('input[name^="reconcelAmount"]').each(function()
        {
            paid_amt=parseInt($('input[name="hdnPaidAmount['+index+']"]').val());
            reconcel_amt=$(this).val();
            voucher_number=$('select[name="ddlVoucher['+index+']"]').val();
            $(this).removeClass("required");
            $('select[name="ddlVoucher['+index+']"]').parent().find('.select-dropdown').removeClass("required");
            if(reconcel_amt == '' && voucher_number == '')
            {
                $(this).addClass("required");
                $('select[name="ddlVoucher['+index+']"]').parent().find('.select-dropdown').addClass("required");
                flag=1;
                rc_flag=1;
            }
            else if(reconcel_amt == '' || parseInt(reconcel_amt)<=0)
            {
                $(this).addClass("required");
                flag=1;
                rc_flag=1;
            }
            else if(parseInt(reconcel_amt)>paid_amt)
            {
                $(this).addClass("required");
                notify('Reconcel amount cannot be more than paid amount','error');
                flag=1;
                rc_flag=1;
            }
            else if(voucher_number == '')
            {
                $('select[name="ddlVoucher['+index+']"]').parent().find('.select-dropdown').addClass("required");
                flag=1;
                rc_flag=1;
            }
            if(flag == 0)
            {
                if(jQuery.inArray( voucher_number, used_voucher ) == -1)
                {
                    used_voucher.push(voucher_number);
                    total_voucher_amt+=parseInt($('select[name="ddlVoucher['+index+']"] option:selected').attr('data-amount'));
                }
                total_reconcel_amount+=parseInt(reconcel_amt);
                reconcelation_records.push(reconcel_amt+'@@@@@@'+voucher_number+'@@@@@@'+$('input[name="hdnFeeCollection['+index+']"]').val());
                reconce_validation++;
            }
            index++;
        });
        if(rc_flag == 0 || reconce_validation >0)
        {
            $('input[name^="reconcelAmount"]').removeClass("required");
            $('select[name^="ddlVoucher').parent().find('.select-dropdown').removeClass("required");
            if(total_voucher_amt != total_reconcel_amount)
            {
                reconce_validation--;
                notify('Voucher amount not matching with reconciled amount','error');
                return false;
            }
            else
            {
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                var userId = $('#hdnUserId').val();
                var selectedBranch=$("#branchFilter > .active").attr('data-id');
                var selectedFeeCompany=$("#companyFilter > .active").attr('data-id');
                var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
                var selectedRevenueDate=$("#txtReconcelationDate").val();
                var action = 'list';
                var ajaxurl = API_URL + 'index.php/VoucherReconcelation/ProcessVoucherReconcelation';
                var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
                var params= {branchId:selectedBranch,selectedDate:selectedRevenueDate,PaymentMode:selectedPaymentType,companyId:selectedFeeCompany,reconcelationData:reconcelation_records};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess:ReconcelationResponse});
                return false;
            }
        }
    }
    function ReconcelationResponse(response)
    {
        alertify.dismissAll();
        if (response.status == true)
        {
            notify('Voucher reconciled','success');
            $('#voucher-reconcel-data').addClass('hidden');
            $("#voucher-reconcel-list >tbody").html('');
        }
        else
        {

            notify(response['message'], 'error', 10);
        }
    }
</script>
