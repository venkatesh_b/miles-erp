/**
 * Created by VENKATESH.B on 18/2/16.
 */
function commonAjaxCall(fetchParam)
{
    $(fetchParam.This).attr("disabled","disabled");
    fetchParam= $.extend({},{asyncType:false,method:'GET',params:{},headerParams:{},action:''},fetchParam);
    if(fetchParam.params==undefined || fetchParam.params==null )
        fetchParam.params={};
    try{
        //fetchParam.params= $.extend({},fetchParam.params,{context:$context,action:fetchParam.action,pageUrl:$pageUrl,serviceUrl:fetchParam.requestUrl});
        fetchParam.params= $.extend({},fetchParam.params);
    }catch(e){sl(e);}

    if (typeof asyncType === "undefined" || asyncType === null)
        asyncType=false;
    if(fetchParam.action!='' && $actions.indexOf(fetchParam.action)==-1)
        return {status:false,errmsg:'No Access'};
    if(isBlank(fetchParam.requestUrl))return null;
    return $.ajax({
        type: fetchParam.method,
        url: fetchParam.requestUrl,
        dataType: "json",
        data: fetchParam.params,
        headers: fetchParam.headerParams,
        beforeSend:function(){
            $(document.body).css({ 'cursor': 'progress' });

        },success: function (response){
            $(document.body).css({ 'cursor': 'default' });
            $(fetchParam.This).removeAttr("disabled");
            if(fetchParam.onSuccess)
                fetchParam.onSuccess(response);
            verifyAccess();
        },error:function(response){
            $(document.body).css({ 'cursor': 'default' });
            if(response.responseJSON.message=="Access Token Expired"){
                logout();
            }
            else if(response.responseJSON.message=="Invalid credentials"){
                logout();
            }
            else{
                notify('Something went wrong. Please try again', 'error', 10);
                $(fetchParam.This).removeAttribute("disabled");
            }
        }
    });

}

function CheckFileExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}

function DTResponseerror(response)
{
    
    if(response.responseJSON.message=="Access Token Expired"){
        notify(response.responseJSON.message, 'error', 10);
        logout();
    }
    else if(response.responseJSON.message=="Invalid credentials"){
        notify(response.responseJSON.message, 'error', 10);
        logout();
    }
    else{
        notify('Something went wrong. Please try again', 'error', 10);
    }
}

function DTSearchOnKeyPressEnter(DTObject)
{
    var tableObject='';
    if(DTObject)
    {
        tableObject=DTObject;
    }
    else
    {
        tableObject=$tableobj;
    }
    $(".dataTables_filter input")
        .unbind()
        .bind('keyup change', function (e) {
            if (e.keyCode == 13 || this.value == "")
            {
                tableObject.search(this.value)
                    .draw();
            }
        });
    //var dt_name=$(".dataTables_filter").attr("id").replace("_filter", "");
    //$('#'+dt_name+' > thead:last').append('<tr><td class="border-none p0 lh10">&nbsp;</td></tr>');
}

function MultipleDTSearchEnter(DTObject,tableId)
{
    var tableObject='';
    if(DTObject)
    {
        tableObject=DTObject;
    }
    else
    {
        tableObject=$tableobj;
    }
    if(tableId!=undefined || tableId!='')
        tableId=tableId+'_wrapper';
    $(tableId+" .dataTables_filter input")
        .unbind()
        .bind('keyup change', function (e) {
            if (e.keyCode == 13 || this.value == "")
            {
                tableObject.search(this.value)
                    .draw();
            }
        });

    //var dt_name=$(".dataTables_filter").attr("id").replace("_filter", "");
    //$('#'+dt_name+' > thead:last').append('<tr><td class="border-none p0 lh10">&nbsp;</td></tr>');
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function isPhoneNumber(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var inp = String.fromCharCode(charCode);
    if(charCode == 8 || charCode == 46 || charCode ==32 || charCode ==37 || charCode ==39 ){
        return true;
    }
    if (!(/^[0-9-+()]$/.test(inp)))
        return false;
    return true;
}
function isAlphaNumericKey(evt)
{
    var inp = String.fromCharCode(event.keyCode);
    if (!/[a-zA-Z0-9-_ ]/.test(inp))
        return false;
    return true;
}

var ones = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
var tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
function NumberinWords(num)
{
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return;
    var str = '';
    str += (n[1] != 0) ? (ones[Number(n[1])] || tens[n[1][0]] + ' ' + ones[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (ones[Number(n[2])] || tens[n[2][0]] + ' ' + ones[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (ones[Number(n[3])] || tens[n[3][0]] + ' ' + ones[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (ones[Number(n[4])] || tens[n[4][0]] + ' ' + ones[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (ones[Number(n[5])] || tens[n[5][0]] + ' ' + ones[n[5][1]]) : '';
    if(str!='')
    {
        str=str + 'only ';
    }
    return str;
}

function customAlert(ModelTitle,Description)
{
    $("#popup_alert .modal-title").html(ModelTitle);
    $("#popup_alert .modal-body").html(Description);
    $('#popup_alert').modal('show');
}

function customConfirmAlert(ModelTitle,Description)
{
    $("#popup_confirm .modal-title").html(ModelTitle);
    $("#popup_confirm .modal-body").html(Description);
    $('#popup_confirm').modal('show');
}