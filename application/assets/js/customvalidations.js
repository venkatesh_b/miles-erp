/**
 * Created by VENKATESH.B on 10/2/16.
 */
var requestRunning = false;//for handling double clicks
function ManageRole(This, Id)
{
    if(Id==0){
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Add');
    } else {
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Update');
    }
    $('#ManageRoleForm')[0].reset();
    $('#ManageRoleForm label').removeClass("active");
    $('#ManageRoleForm span.required-msg').remove();
    $('#ManageRoleForm input').removeClass("required");
    $("#txtRoleName").removeAttr("disabled");
    $roleName = '';
    if (Id == 0)
    {
        $("#myModalLabel").html("Create Role");
    } else
    {
        notify('Processing..', 'warning', 10);
        $("#myModalLabel").html("Edit Role");
        var ajaxurl = API_URL + 'index.php/UserRole/getRoleById';
        var params = {roleId: Id};
        var userID = $('#hdnUserId').val();
        var headerParams = {action: 'edit', context: 'Role Management', serviceurl: 'roles', pageurl: 'roles', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: EditRoleresponse});
    }
    $("#hdnRoleId").val(Id);
    $(This).attr("data-target", "#myModal");
    $(This).attr("data-toggle", "modal");
}
function EditRoleresponse(response)
{
    if (response == -1 || response['status'] == false)
    {
        notify('Something went wrong', 'error', 10);
    } else
    {
        var rolesData = response['data'];
        $roleName = rolesData[0]['value'];
        if(rolesData[0]['value']=='Superadmin')
        $("#txtRoleName").attr("disabled", "disabled");
        $("#txtRoleName").val(rolesData[0]['value']);
        $("#txtRoleName").next('label').addClass("active");
        $("#txaRoleDescription").val(rolesData[0]['description']);
        if (rolesData[0]['description'] != null)
            $("#txaRoleDescription").next('label').addClass("active");
        alertify.dismissAll();
    }
}

function saveRole(This)
{
    notify('Processing..', 'warning', 10);
    if (requestRunning) { // don't do anything if an AJAX request is pending
        return;
    }
    var Name = $("#txtRoleName").val();
    var Description = $("#txaRoleDescription").val();
    var RoleId = $("#hdnRoleId").val();
    var userID = $('#hdnUserId').val();
    $('#ManageRoleForm span.required-msg').remove();
    $('#ManageRoleForm input').removeClass("required");
    var nameRegex = $rgx_allow_alpha_numeric_space;
    var flag = 0;
    if (Name == '')
    {
        $("#txtRoleName").addClass("required");
        $("#txtRoleName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (!nameRegex.test($.trim(Name)))
    {
        $("#txtRoleName").addClass("required");
        $("#txtRoleName").after('<span class="required-msg">'+$rgx_allow_alpha_numeric_space_message+'</span>');
        flag = 1;
    } else if (Name.length < 2)
    {
        $("#txtRoleName").addClass("required");
        $("#txtRoleName").after('<span class="required-msg">Can not be less than 2 characters</span>');
        flag = 1;
    }
    if (flag == 0)
    {
        var ajaxurl;
        var params;
        if (RoleId == 0)
        {
            var ajaxurl = API_URL + 'index.php/UserRole/addRole';
            var params = {roleName: Name, roleDescription: Description};
            var headerParams = {action: 'add', context: 'Role Management', serviceurl: 'roles', pageurl: 'roles', Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: Roleresponse});
        } else
        {
            //Name = $roleName;
            var ajaxurl = API_URL + 'index.php/UserRole/updateRole';
            var params = {roleId: RoleId, roleName: Name, roleDescription: Description};
            var headerParams = {action: 'add', context: 'Role Management', serviceurl: 'roles', pageurl: 'roles', Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: Roleresponse});
        }
    } else
    {
        alertify.dismissAll();
        return false;
    }
}
function Roleresponse(response)
{
    if (response.status == true)
    {
        alertify.dismissAll();
        notify('Saved successfully', 'success', 10);
        $('#ManageRoleForm')[0].reset();
        $('#ManageRoleForm label').removeClass("active");
        $('#ManageRoleForm span.required-msg').remove();
        $('#ManageRoleForm input').removeClass("required");
        $("#txtRoleName").removeAttr("disabled");
        requestRunning=false;
        //getRoleData();
        buildDataTable();
        $('#myModal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    } else
    {
        //if(response.data.length>0)
        {
            var id = '', flag = 0;
            alertify.dismissAll();
            //notify(response['message'], 'error', 10);
            for (var a in response.data) {
                id = '#' + a;
                if (a == 'roleName' || a == 'reference_type_val') {
                    $("#txtRoleName").addClass("required");
                    $("#txtRoleName").after('<span class="required-msg">' + response.data[a] + '</span>');
                } else {
                    $(id).addClass("required");
                    $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                }
            }
        }
    }
}
$("#ddlManageRoles").change(function () {
    if ($(this).val() != '')
    {
        //window.location=BASE_URL+'roles/manageRole/'+$(this).val();
        window.location = APP_REDIRECT + 'manage_role/' + $(this).val();
    }
});

function saveRolePermissions(roleId)
{
    var menuOrdersIds = $('input:text.menuOrder').map(function () {
        return $(this).attr("id") + '-' + $(this).val();
    }).get().join('|');

    var menuIds = $('input:text.menuOrder').map(function () {
        return $(this).attr("id");
    }).get().join(',');

    var accessIds = $('input:checkbox.role-manage:checked').map(function () {
        return this.value;
    }).get().join(',');
    var userID = $('#hdnUserId').val();

    if (accessIds == '')
    {
        notify('Choose atleast one action', 'error', 10);
    } else
    {
        var ajaxurl = API_URL + 'index.php/UserRole/saveRoleComponentPermissions';
        var headerParams = {action: 'manage', context: 'Role Management', serviceurl: 'roles', pageurl: 'roles', Authorizationtoken: $accessToken, user: userID};
        var params = {roleId: roleId, accessIds: accessIds, menuOrders: menuOrdersIds, menuIds: menuIds};
        commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'manage', onSuccess: saveRolePermissionsResponse});

    }

}
function saveRolePermissionsResponse(response) {

    if (response.status == true)
    {
        $('#popup_confirm').modal('hide');
        notify('Updated successfully', 'success', 10);
        getRolePermissions();

    } else {
        notify(response.message, 'error', 10);
    }
}

/**/
/*For Users*/
function ManageUser(This, Id) {
    alertify.dismissAll();
    notify('Processing..', 'warning', 10);
    clearDatePicker('#txtUserDob');
    clearDatePicker('#txtUserJoin');
    if(Id==0){
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Add');
        //setDatePicker('#txtUserDob', '0000-00-00');
        //setDatePicker('#txtUserJoin', '0000-00-00');
    } else {
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Update');
    }
    
    var userId = $('#userId').val();
    var action = 'add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
    $.when(
            commonAjaxCall
            (
                    {
                        This: This,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Branch/getBranchList',
                        action: action
                    }
            ),
            commonAjaxCall
            (
                    {
                        This: This,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/UserRole/rolesList',
                        action: action
                    }
            )
    ).then(function (response1, response2) {
        var branch = response1[0];
        var role = response2[0];

        branchList(branch);
        userRole(role);

        $('#ManageUserCreation')[0].reset();
        $('#txtUserRole').material_select();
        $('#txtUserBranch').material_select();
        $('#ManageUserCreation label').removeClass("active");
        $('#ManageUserCreation span.required-msg').remove();
        $('#ManageUserCreation input').removeClass("required");
        $('#ManageUserCreation select').removeClass("required");
        if (Id == 0)
        {
            $("#myModalLabel").html("Create User");
			var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();

                if(dd<10) {
                    dd='0'+dd
                } 

                if(mm<10) {
                    mm='0'+mm
                } 
                today = yyyy+'/'+mm+'/'+dd;
                setDatePicker('#txtUserJoin', today);
        }
        else
        {
            var userId = $('#userId').val();
            $("#myModalLabel").html("Edit User");
            var ajaxurl = API_URL + 'index.php/User/getSingleUser';
            var params = {user_id: Id};
            var action = 'edit';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, params: params, action: action, onSuccess: userDetails});
        }
        alertify.dismissAll();
        $("#hdnUserId").val(Id);
        $('#myModal').modal('toggle');
    });
}

function getRolesDropdown()
{
    var ajaxurl = API_URL + 'index.php/UserRole/rolesList';
    var params = {};
    var action = 'list';
    //var userID=<?php echo $userData['userId']; ?>;
    var userID = $('#userId').val();
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: userRole});
}

function getBranchesDropdown() {
    var userID = $('#userId').val();
    var ajaxurl = API_URL + 'index.php/Branch/getBranchList';
    var params = {};
    var action = 'list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: branchList});
}

function userRole(response)
{
    $('#txtUserRole').find('option:gt(0)').remove();
    if (response.data.length == 0 || response == -1 || response['status'] == false)
    {
        //
    } else
    {
        if (response.data.length > 0)
        {
            var userRawData = response.data;
            for (var b = 0; b < userRawData.length; b++) {
                var o = $('<option/>', {value: userRawData[b]['roleId']})
                        .text(userRawData[b]['roleName']);
                o.appendTo('#txtUserRole');
            }
        }
    }
    $('#txtUserRole').material_select();
}

function branchList(response)
{
    //$('#txtUserBranch').find('option:gt(0)').remove();
    $('#txtUserBranch').empty();
    $('#txtUserBranch').material_select();
    var o = $('<option/>', {value: ''})
            .text('--Select All--');
    o.appendTo('#txtUserBranch');
    if (response.data.length == 0 || response == -1 || response['status'] == false)
    {
        //
    } else
    {
        /*var o = $('<option/>', { value: '' })
         .text('--Select All--');
         o.appendTo('#txtUserBranch');*/
        var branchRawData = response.data;
        for (var b = 0; b < branchRawData.length; b++) {
            var o = $('<option/>', {value: branchRawData[b]['id']})
                    .text(branchRawData[b]['name']);
            o.appendTo('#txtUserBranch');
        }
    }
    $('#txtUserBranch').material_select();
}

function userDetails(response)
{
    if (response.data.length < 0 || response == -1 || response['status'] == false)
    {
        notify('Something went wrong', 'error', 10);
    } else
    {
        var usersData = response.data[0];
        $("#txtUserName").val(usersData.name);
        $("#txtUserName").next('label').addClass("active");
        $("#txtUserPhoneNumber").val(usersData.phone);
        $("#txtUserPhoneNumber").next('label').addClass("active");
        $("#txtUserEmail").val(usersData.email);
        $("#txtUserEmail").next('label').addClass("active");
        $("#txtCode").val(usersData.employee_code);
        $("#txtCode").next('label').addClass("active");
        if(usersData.dob!=null && usersData.dob!='')
            $("#txtUserDob").val(usersData.dob);
        if(usersData.join!=null && usersData.join!='')
        {
            setDatePicker('#txtUserJoin', usersData.join);
            //$("#txtUserJoin").val(usersData.join);
        }

        $('input[name="txtUserGender"][value="' + usersData.sex + '"]').prop('checked', true);
        $("#sCounselor").val(usersData.sCounselor);
        var check = usersData.sCounselor == 1 ? true : false;
        $('#sCounselor').prop("checked", check);
        if(usersData.dob!=null && usersData.dob!='') {
            var $input = $('.datepicker').pickadate();

            // Use the picker object directly.
            var picker = $input.pickadate('picker');
            picker.set('select', usersData.dob, {format: 'yyyy-mm-dd'});

            $("#txtUserDob").next('label').addClass("active");
        }
        //$('#txtUserRole option:contains(' + usersData.role + ')').prop('selected', true);
        $('#txtUserRole option:contains(' + usersData.role + ')').each(function(){
            if ($(this).text() == usersData.role) {
                $(this).prop('selected', 'selected');
            }

        });
        $('#txtUserRole').material_select();
        $("#txtUserRole").next('label').addClass("active");
        var sel_branches = [];
        $.each(usersData.branch, function (i, item) {
            sel_branches.push(item.branch_id);
        });
        
        $('#txtUserBranch').val(sel_branches);
        $('#txtUserBranch').material_select();
        $("#txtUserBranch").next('label').addClass("active");
    }
}

$('#txtUserBranch').change(function () {
    multiSelectDropdown($(this));
});
function saveUser(This) {
    var Name = $("#txtUserName").val();
    var UserId = $("#hdnUserId").val();
    var UserPhoneNumber = $("#txtUserPhoneNumber").val();
    var UserEmail = $("#txtUserEmail").val();
    var UserCode = $("#txtCode").val();
    var UserDob = $("#txtUserDob").val();
	var txtUserJoin = $("#txtUserJoin").val();
    var UserRole = $("#txtUserRole").val();
    var sCounselor = $('#sCounselor').is(":checked") ? '1' : '0';
    var UserRoleText = $("#txtUserRole option:selected").text().toUpperCase();
    var UserGender = $('[name=txtUserGender]:checked').val();
    var selMulti = $.map($("#txtUserBranch option:selected"), function (el, i) {
        if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'all' && $(el).val().trim() != 'default') {
            return $(el).val();
        }
    });
    var UserBranch = selMulti.join(",");
    var regx_txtUserName = /^[a-zA-Z\s]+$/;
    var regx_txtCode = /^[a-zA-Z0-9]+$/;
    var regx_txtUserPhoneNumber = /^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/;
    var regx_txtUserEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    $('#ManageUserCreation span.required-msg').remove();
    $('#ManageUserCreation input').removeClass("required");
    var flag = 0;
    if (Name == '')
    {
        $("#txtUserName").addClass("required");
        $("#txtUserName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (regx_txtUserName.test($('#txtUserName').val()) === false) {
        $("#txtUserName").addClass("required");
        $("#txtUserName").after('<span class="required-msg">Accepts only alphabets and spaces</span>');
        flag = 1;
    }

    if (UserPhoneNumber == '')
    {
        $("#txtUserPhoneNumber").addClass("required");
        $("#txtUserPhoneNumber").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (UserPhoneNumber.length < 8)
    {
        $("#txtUserPhoneNumber").addClass("required");
        $("#txtUserPhoneNumber").after('<span class="required-msg">'+$phoneMinLengthMessage+'</span>');
        flag = 1;
    } else if (UserPhoneNumber.length > 15)
    {
        $("#txtUserPhoneNumber").addClass("required");
        $("#txtUserPhoneNumber").after('<span class="required-msg">'+$phoneMaxLengthMessage+'</span>');
        flag = 1;
    } else if (regx_txtUserPhoneNumber.test($('#txtUserPhoneNumber').val()) === false) {
        $("#txtUserPhoneNumber").addClass("required");
        $("#txtUserPhoneNumber").after('<span class="required-msg">Invalid Phone Number</span>');
        flag = 1;
    }
    if (UserEmail == '')
    {
        $("#txtUserEmail").addClass("required");
        $("#txtUserEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (regx_txtUserEmail.test($('#txtUserEmail').val()) === false) {
        $("#txtUserEmail").addClass("required");
        $("#txtUserEmail").after('<span class="required-msg">Invalid Email</span>');
        flag = 1;
    }
    if (UserCode == '')
    {
        $("#txtCode").addClass("required");
        $("#txtCode").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (regx_txtCode.test($("#txtCode").val()) === false) {
        $("#txtCode").addClass("required");
        $("#txtCode").after('<span class="required-msg">Invalid Code</span>');
        flag = 1;
    }
    /*if(UserDob=='')
     {
     $("#txtUserDob").addClass("required");
     $("#txtUserDob").after('<span class="required-msg">required field</span>');
     flag=1;
     }*/
    if (UserRole == '' || UserRole == null)
    {

        $("#txtUserRole").parent().find('.select-dropdown').addClass("required");
        $("#txtUserRole").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if (UserGender == '')
    {
        $("[name=txtUserGender]").addClass("required");
        $("[name=txtUserGender]").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }
    if (UserBranch == '')
    {

        $("#txtUserBranch").parent().find('.select-dropdown').addClass("required");
        $("#txtUserBranch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }

    if (flag == 1) {
        return false;
    } else {
        if (UserRoleText == 'SUPER ADMIN' || UserRoleText == 'SUPERADMIN' || UserRoleText == 'Superadmin') {
            $("#popup_confirm #btnTrue").attr('onClick','saveUserFinal(this)');
            if (UserId == 0) {
                //var conf = confirm("Are you sure want to create the user as Super Admin ?");
                var ModelTitle="Create User";
                var Description='Are you sure want to create the user as Super Admin ?';
                customConfirmAlert(ModelTitle,Description);
            } else {
                //var conf = confirm("Are you sure want to update the user as Super Admin ?");
                var ModelTitle="Edit User";
                var Description='Are you sure want to update the user as Super Admin ?';
                customConfirmAlert(ModelTitle,Description);
            }
        } else {
            saveUserFinal(This);
        }

    }
}
function saveUserFinal(This){
    alertify.dismissAll();
    notify('Processing..', 'warning', 10);
    var Name = $("#txtUserName").val();
    var UserId = $("#hdnUserId").val();
    var UserPhoneNumber = $("#txtUserPhoneNumber").val();
    var UserEmail = $("#txtUserEmail").val();
    var UserCode = $("#txtCode").val();
    var UserDob = $("#txtUserDob").val();
    var txtUserJoin = $("#txtUserJoin").val();

    var UserRole = $("#txtUserRole").val();
    var sCounselor = $('#sCounselor').is(":checked") ? '1' : '0';
    var UserRoleText = $("#txtUserRole option:selected").text().toUpperCase();
    var UserGender = $('[name=txtUserGender]:checked').val();
    var selMulti = $.map($("#txtUserBranch option:selected"), function (el, i) {
        if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'all' && $(el).val().trim() != 'default') {
            return $(el).val();
        }
    });
    var UserBranch = selMulti.join(",");
    var regx_txtUserName = /^[a-zA-Z\s]+$/;
    var regx_txtCode = /^[a-zA-Z0-9]+$/;
    var regx_txtUserPhoneNumber = /^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/;
    var regx_txtUserEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    $('#ManageUserCreation span.required-msg').remove();
    $('#ManageUserCreation input').removeClass("required");
    var flag = 0;
            var action;
            if (UserId == 0) {
                action = 'add';
                var ajaxurl = API_URL + 'index.php/User/addUser';
                var params = {email: UserEmail, name: Name, sCounselor: sCounselor, phone: UserPhoneNumber, sex: UserGender, dob: UserDob, join:txtUserJoin, role: UserRole, branch: UserBranch, employee_code: UserCode};
                var type = "POST";
            } else {
                action = 'edit';
                var ajaxurl = API_URL + 'index.php/User/editUser/user_id/' + UserId;
                var params = {id: UserId, email: UserEmail, sCounselor: sCounselor, name: Name, phone: UserPhoneNumber, sex: UserGender, dob: UserDob, join:txtUserJoin, role: UserRole, branch: UserBranch, employee_code: UserCode};
                var type = "PUT";
            }
            var Id = $('#userId').val();
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: Id};
            var res = commonAjaxCall({
                This: This,
                asyncType: true,
                method: type,
                requestUrl: ajaxurl,
                params: params,
                headerParams: headerParams,
                action: action,
                onSuccess: userManageResponse
            });

}
function userManageResponse(response)
{
    alertify.dismissAll();
    var id = '';
    if (response == -1 || response.status == false)
    {
        notify('Something went wrong', 'error', 10);
        //notify(response.data.email,'error' ,10);
        $.each(response.data, function (i, item) {
            alertify.dismissAll();
            notify(item, 'error', 10);
            return false;
        });
        for (var a in response.data) {
            id = '#' + a;
            if (a == 'email') {
                $('#txtUserEmail').addClass("required");
                $('#txtUserEmail').after('<span class="required-msg">' + response.data[a] + '</span>');
            } else if (a == 'employee_code') {
                $('#txtCode').addClass("required");
                $('#txtCode').after('<span class="required-msg">' + response.data[a] + '</span>');
            } else if (a == 'branch') {
                $('#txtUserBranch').parent().find('.select-dropdown').addClass("required");
                $('#txtUserBranch').parent().after('<span class="required-msg">' + response.data[a] + '</span>');
            } else if (a == 'txtUserRole') {
                $('#txtUserRole').parent().find('.select-dropdown').addClass("required");
                $('#txtUserRole').parent().after('<span class="required-msg">' + response.data[a] + '</span>');
            } else {
                $(id).addClass("required");
                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
            }
        }
    } else
    {
        notify(response.message, 'success', 10);
        buildUsersDataTable();
        $('#myModal').modal('hide');
        $('#popup_confirm').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }
}
/*Add Branch and Edit branch*/
function AddBranch(This)
{
    var id = $('#branchId').val();
    if (id == 0)
    {
        var action = 'add';
        var method = 'POST';
        var ajaxurl = API_URL + 'index.php/Branch/createBranch';
    }
    else
    {
        var action = 'manage';
        var method = 'POST';
        var ajaxurl = API_URL + 'index.php/Branch/updateBranch/branch_id/' + id;
    }
    var flag = 0;

    var name = $('#name').val();
    var address = $('#address').val();
    var code = $('#code').val();
    var email = $('#email').val();
    var regx_txtEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var country = $("select[name=country]").val();
    var state = $("select[name=state]").val();
    var city = $("select[name=city]").val();
    var zip = $('#zip').val();
    var phone = $('#phone').val();
    var lsequence = $('#lsequence').val();
    var bsequence = $('#bsequence').val();
    var selMulti = $.map($("#course option:selected"), function (el, i)
    {
        if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'all')
        {
            return $(el).val();
        }
    });
    var course = selMulti.join(",");
    var primaryCity = '';
    var secondaryCity = '';
    if ($("select[name=primaryCity]").val() != null) {
        var primaryCity = $("select[name=primaryCity]").val().toString();
    }
    if ($("select[name=secondaryCity]").val() != null) {
        var secondaryCity = $("select[name=secondaryCity]").val().toString();
    }

    var desc = $('#desc').val();
    var userId = $('#userId').val();
    var branchHead = $("select[name=branchHead]").val() == null ? '' : $("select[name=branchHead]").val();

    $('#BranchForm span.required-msg').remove();
    $('#BranchForm input,#BranchForm .select-dropdown, .materialize-textarea').removeClass("required");

    if (name == '') {
        $("#name").addClass("required");
        $("#name").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (!($rgx_allow_alpha_numeric_space.test(name))) {
        $("#name").addClass("required");
        $("#name").after('<span class="required-msg">'+$rgx_allow_alpha_numeric_space_message+'</span>');
        flag = 1;
    }
    if (code == '') {
        $("#code").addClass("required");
        $("#code").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (!(/^[a-zA-Z0-9]+$/.test(code))) {
        $("#code").addClass("required");
        $("#code").after('<span class="required-msg">Invalid Branch Code, must contain only Alphabets & Numbers</span>');
        flag = 1;
    }

    if (lsequence == '') {
        $("#lsequence").addClass("required");
        $("#lsequence").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (!(/^[0-9]+$/.test(lsequence))) {
        $("#lsequence").addClass("required");
        $("#lsequence").after('<span class="required-msg">Invalid Sequence, must contain only Numbers</span>');
        flag = 1;
    }

    if (bsequence == '') {
        $("#bsequence").addClass("required");
        $("#bsequence").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (!(/^[0-9]+$/.test(lsequence))) {
        $("#bsequence").addClass("required");
        $("#bsequence").after('<span class="required-msg">Invalid Sequence, must contain only Numbers</span>');
        flag = 1;
    }
    /*Not mandatory*/
    /*if(branchHead == ''){
     $("#branchHead").parent().find('.select-dropdown').addClass("required");
     $("#branchHead").parent().after('<span class="required-msg">required field</span>');
     flag = 1;
     }else
     if(/^[a-z0-9]+$/.test(branchHead)){
     $("#branchHead").parent().find('.select-dropdown').addClass("required");
     $("#branchHead").parent().after('<span class="required-msg">Wrong option selected for Branch Head</span>');
     flag = 1;
     }*/
    if (email == '') {
        $("#email").addClass("required");
        $("#email").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (regx_txtEmail.test($('#email').val()) === false) {
        $("#email").addClass("required");
        $("#email").after('<span class="required-msg">Invalid Email</span>');
        flag = 1;
    }
    if (country === null) {
        $("#country").parent().find('.select-dropdown').addClass("required");
        $("#country").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if (state === null) {
        $("#state").parent().find('.select-dropdown').addClass("required");
        $("#state").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if (city === null) {
        $("#city").parent().find('.select-dropdown').addClass("required");
        $("#city").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if (zip == '') {
        $("#zip").addClass("required");
        $("#zip").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (!(/^[0-9]{6}$/.test(zip))) {
        $("#zip").addClass("required");
        $("#zip").after('<span class="required-msg">Invalid Zip, must contain number</span>');
        flag = 1;
    } else if (zip.length != 6) {
        $("#zip").addClass("required");
        $("#zip").after('<span class="required-msg">Accepts only 6 digits</span>');
        flag = 1;
    }
    if (phone == '')
    {
        $("#phone").addClass("required");
        $("#phone").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }
    else if (phone.length < 8)
    {
        $("#phone").addClass("required");
        $("#phone").after('<span class="required-msg">'+$phoneMinLengthMessage+'</span>');
        flag = 1;
    }
    else if (phone.length > 15)
    {
        $("#phone").addClass("required");
        $("#phone").after('<span class="required-msg">'+$phoneMaxLengthMessage+'</span>');
        flag = 1;
    }
    else if (!(/^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/.test(phone)))
    {
        $("#phone").addClass("required");
        $("#phone").after('<span class="required-msg">Invalid phone number</span>');
        flag = 1;
    }
    if (primaryCity == "") {
        $("#primaryCity").parent().find('.select-dropdown').addClass("required");
        $("#primaryCity").parent().after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }

    /*if(secondaryCity == ""){
     $("#secondaryCity").parent().find('.select-dropdown').addClass("required");
     $("#secondaryCity").parent().after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
     flag = 1;
     }*/

    if (course == "") {
        $("#course").parent().find('.select-dropdown').addClass("required");
        $("#course").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }

    /*if(desc == ''){
     $("#desc").addClass("required");
     $("#desc").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
     flag = 1;
     }*/
    if(address == ''){
     $("#address").addClass("required");
     $("#address").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
     flag = 1;
    }
    var max_fileUpload_size = 2.048;    /*For max file size for uploaded file(In MB)*/
    var files = $("#imageUpload").get(0).files;
    var selectedFile = $("#imageUpload").val();
    var extArray = ['gif', 'jpg', 'jpeg', 'png'];
    var extension = selectedFile.split('.');
    /*if (files.length <= 0 && id == 0) {
        $(".file-path").addClass("required");
        $(".file-path").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else */if (files.length > 0 && $.inArray(extension[extension.length - 1], extArray) <= -1) {
        $(".file-path").focus();
        $(".file-path").addClass("required");
        $(".file-path").after('<span class="required-msg">Allowed only ' + extArray.toString() + '</span>');
        flag = 1;
    } else if (files.length > 0 && ((files[0].size / 1024) / 1024) > max_fileUpload_size)
    {
        $(".file-path").focus();
        $(".file-path").addClass("required");
        $(".file-path").after('<span class="required-msg">File can\'t be more than '+parseInt(max_fileUpload_size)+'Mb</span>');
        flag = 1;
    }
    if (flag == 0)
    {
        notify('Processing..', 'warning', 10);
        var headerParams = {action: action, contentType: false, processData: false, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var fileUpload = document.getElementById("imageUpload");
        if (fileUpload.value != null) {
            var uploadFile = new FormData();
                if (files.length > 0) {
                    uploadFile.append("file", files[0]);
                    uploadFile.append("image", 'branch_' + new Date().getTime());
                }
                uploadFile.append('name', name);
                uploadFile.append('address', address);
                uploadFile.append('code', code);
                uploadFile.append('email', email);
                uploadFile.append('lsequence', lsequence);
                uploadFile.append('bsequence', bsequence);
                uploadFile.append('country', country);
                uploadFile.append('state', state);
                uploadFile.append('city', city);
                uploadFile.append('zip', zip);
                uploadFile.append('phone', phone);
                uploadFile.append('course', course);
                uploadFile.append('primaryCity', primaryCity);
                uploadFile.append('secondaryCity', secondaryCity);
                uploadFile.append('desc', desc);
                uploadFile.append('userId', userId);
                uploadFile.append('branchHead', branchHead);
                $.ajax({
                    /*type: 'POST',*/
                    type: method,
                    url: ajaxurl,
                    dataType: "json",
                    data: uploadFile,
                    contentType: false,
                    processData: false,
                    headers: headerParams,
                    beforeSend: function () {
                        $(This).attr("disabled", "disabled");
                    }, success: function (response) {
                        alertify.dismissAll();
                        $(This).removeAttr("disabled");
                        addNewBranch(response);
                    }, error: function (response) {
                        alertify.dismissAll();
                        notify('Something went wrong. Please try again', 'error', 10);
                    }
                });
//            }
        }
    } else {
        alertify.dismissAll();
        notify('Validation Error', 'error', 10);
    }
}

/*Response of Add and edit branch section*/
function addNewBranch(response)
{
    $('#BranchForm span.required-msg').remove();
    $('#BranchForm input').removeClass("required");
    if (response == -1 || response['status'] == false)
    {
        var id = '';
        alertify.dismissAll();
        notify('Validation Error', 'error', 10);
        for (var a in response.data) {
            id = '#' + a;
            if (a == 'country' || a == 'state' || a == 'city' || a == 'course' || a == 'primaryCity' || a == 'secondaryCity' || a == 'branchHead') {
                $(id).parent().find('.select-dropdown').addClass("required");
                $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
            } else if (a == 'imageUpload') {
                $(".file-path").addClass("required");
                $(".file-path").after('<span class="required-msg">' + response.data[a] + '</span>');
            } else {
                $(id).addClass("required");
                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
            }
        }
    } else
    {
        alertify.dismissAll();
        notify('Data Saved Successfully', 'success', 10);
        branchPageLoad();
        branchesList();
        closeBranchModal(this);
    }
}

/*Assigning data to respective fields*/
function manageBranch(This, id)
{
    notify('Processing..', 'warning', 10);
    if(id!=0){
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Update');
    }
    /*Delete all data before populating*/
    $('#course').find('option:gt(0)').remove();
    $('#course').material_select();

    $('#branchId').val(id);

    //$('#BranchForm input').parent().find('label').addClass("active");
    /*Remove Mandatory field for file upload*/
    $('.file-upload-btn').find('em').remove();

    //$('#BranchForm textarea').find('label').addClass("active");
    $('#branchModal input').removeClass("required");
    $('#branchModal input').removeClass("required");

    var userId = $('#userId').val();
    var action = 'edit';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

    var ajaxurl = API_URL + 'index.php/Branch/getBranchById';
    var params = {'branch_id': id};
    commonAjaxCall({This: This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
        var branchDetail = response.data;

        $.when(
                commonAjaxCall
                (
                        {
                            This: This,
                            headerParams: headerParams,
                            requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                            params: {name: 'country'},
                            action: action
                        }
                ),
                commonAjaxCall
                (
                        {
                            This: This,
                            headerParams: headerParams,
                            requestUrl: API_URL + 'index.php/Courses/getAllCourseList',
                            action: action
                        }
                ),
                commonAjaxCall
                (
                        {
                            This: This,
                            headerParams: headerParams,
                            requestUrl: API_URL + 'index.php/Branch/getBranchHead',
                            action: action
                        }
                ),
                commonAjaxCall
                (
                        {
                            This: This,
                            headerParams: headerParams,
                            requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesUsingName',
                            params: {name: 'state', 'parent_id': branchDetail.branchHeadCountryId},
                            action: action
                        }
                ),
                commonAjaxCall
                (
                        {
                            This: This,
                            headerParams: headerParams,
                            requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                            params: {name: 'city', 'parent_id': branchDetail.branchHeadId},
                            action: action
                        }
                ),
                commonAjaxCall
                (
                        {
                            This: This,
                            headerParams: headerParams,
                            requestUrl: API_URL + 'index.php/Branch/getPrimaryCity',
                            params: {'country_id': branchDetail.branchHeadCountryId},
                            action: action
                        }
                )
        ).then(function (response1, response2, response3, response4, response5, response6) {
            var country = response1[0];
            var course = response2[0];
            var users = response3[0];
            var state = response4[0];
            var city = response5[0];
            var primaryCity = response6[0];
            branchCountry(country);
            $("select[name=country]").val(branchDetail.branchHeadCountryId);
            $('#country').material_select();

            getCityList(primaryCity);
                            selectOption = 1;
            branchCourse(course);
                            selectOption = 0;
            var sel_courses = [];
            if (branchDetail.courses.length > 0) {
                $.each(branchDetail.courses, function (i, item) {
                    sel_courses.push(item.course_id);
                });
                $("select[name=course]").val(sel_courses);
                $("select[name=course]").material_select();
            }
            $("select[name=course]").val(sel_courses);
            $('#course').material_select();

            branchUserList(users);
            $("select[name=branchHead]").val(branchDetail.userId);
            $('#branchHead').material_select();

            getStateList(state);
            $('#state').val(branchDetail.branchHeadStateId);
            $('#state').material_select();

            getCityDropDown(city);
            $("#city").val(branchDetail.branchHeadId);
            $('#city').material_select();

            $("#myModalLabel").html("Edit Branch");

            var sel_primary = [];
            if (branchDetail.primary.length > 0) {
                $.each(branchDetail.primary, function (i, item) {
                    sel_primary.push(item.cityId);
                });
            }
            var sel_secondary = [];
            if (branchDetail.primary.length > 0) {
                $.each(branchDetail.secondary, function (i, item) {
                    sel_secondary.push(item.cityId);
                });
            }

            $('#name').val(branchDetail.branchName);
            $('#code').val(branchDetail.branchCode);
            $('#email').val(branchDetail.branchEmail);
            $('#zip').val(branchDetail.branchZipcode);
            $('#phone').val(branchDetail.branchPhone);
            $('#lsequence').val(branchDetail.sequence_number);
            $('#address').val(branchDetail.branchAddress);
            $('#bsequence').val(branchDetail.batch_sequence);
            $('#desc').val(branchDetail.description);
//            $("#address").next('label').addClass("active");
//            $("#desc").next('label').addClass("active");

            $("select[name=primaryCity]").val(sel_primary);
            $("select[name=secondaryCity]").val(sel_secondary);
            $('#primaryCity').material_select();
            $('#secondaryCity').material_select();

            Materialize.updateTextFields();
            alertify.dismissAll();
            $('#branchModal').modal('show');
        });
    }});
}
/*Change status of branch*/
function changeStatusBranch(This, branchId, currentStatus)
{
    var status = currentStatus == 1 ? 'inactivate' : 'activate';
    var conf = "Are you sure want to " + status + " this branch ?";
    customConfirmAlert('Branch Status' , conf);
    $("#popup_confirm #btnTrue").attr("onclick","changeStatusBranchConfirm(this,'"+branchId+"')");
}

function changeStatusBranchConfirm(This, branchId)
{
    notify('Processing..', 'warning', 10);
    var userId = $('#userId').val();
    var ajaxurl = API_URL + 'index.php/Branch/changeBranchStatus/branch_id/' + branchId;
    var type = "DELETE";
    var headerParams = {action: 'delete', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
    commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: 'delete', onSuccess: deleteBranchResponse});
}

/*Response of branch delete*/
function deleteBranchResponse(response)
{
    alertify.dismissAll();
    if (response == -1 || response['status'] == false)
    {
        notify(response.message, 'error', 10);
    } else
    {
        notify(response.message, 'success', 10);
        branchesList();
        branchPageLoad();
    }
}

/*Change syayus of the course*/
function changeBranchCourseStatus(This, branchId, courseId, currentStatus)
{
    var status = currentStatus == 1 ? 'inactivate' : 'activate';
    var conf = "Are you sure want to " + status + " this course from branch ?";
    customConfirmAlert('Branch Course Status' , conf);
    $("#popup_confirm #btnTrue").attr("onclick","changeBranchCourseStatusConfirm(this,'"+branchId+"','"+courseId+"')");
}

function changeBranchCourseStatusConfirm(This, branchId, courseId){
    notify('Processing..', 'warning', 10);
    var userId = $('#userId').val();
    var ajaxurl = API_URL + 'index.php/Branch/changeBranchCourseStatus/branch_id/' + branchId + '/course_id/' + courseId;
    var type = "DELETE";
    var headerParams = {action: 'delete', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
    commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: 'delete', onSuccess: deleteBranchCourseResponse});
}

/*Response of branch course delete*/
function deleteBranchCourseResponse(response)
{
    alertify.dismissAll();
    if (response == -1 || response['status'] == false)
    {
        notify(response.message, 'error', 10);
    } else
    {
        notify(response.message, 'success', 10);
        branchesList();
        branchPageLoad();
    }
}

/*Branch page*/
function getBranchDetails(This, id)
{
    if(id==0){
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Add');
    }
    notify('Processing..', 'warning', 10);
    $('#BranchForm span.required-msg').remove();
    $('#BranchForm input').removeClass("required");
    $('#BranchForm label').removeClass("active");
    var userId = $('#userId').val();
    var action = 'add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

    $.when(
	commonAjaxCall
	(
            {
                This: This,
                headerParams: headerParams,
                requestUrl: API_URL + "index.php/Referencevalues/getReferenceValuesByName",
                params: {name: 'country'},
                action: action
            }
	),
	commonAjaxCall
	(
            {
                This: This,
                headerParams: headerParams,
                requestUrl: API_URL + "index.php/Courses/getAllCourseList",
                action: action
            }
	),
	commonAjaxCall
	(
            {
                This: This,
                headerParams: headerParams,
                requestUrl: API_URL + "index.php/Branch/getBranchHead",
                action: action
            }
	)
    ).then(function (response1, response2, response3) {
        var country = response1[0];
        var course = response2[0];
        var branchHead = response3[0];
        
        branchCountry(country);
        
        selectOption = 1;
        branchCourse(course);
        selectOption = 0;
        
        branchUserList(branchHead);
        
        alertify.dismissAll();
        
        $('#branchModal').modal('show');
    });
}

/*Branch model close*/
function closeBranchModal(This) {

    $("#myModalLabel").html("Add Branch");
    $('#branchId').val('0');
    $('#BranchForm')[0].reset();

    $('#displayImage').html('');
    $("#country").empty().html('');
    var o = $('<option/>', {disabled: true, selected: true})
            .text('--Select--');
    o.appendTo('#country');

    $("#city").empty().html('');
    var o = $('<option/>', {disabled: true, selected: true})
            .text('--Select--');
    o.appendTo('#city');

    $("#primaryCity").empty().html('');
    var o = $('<option/>', {disabled: true, value: 'default', selected: true})
            .text('--Select--');
    o.appendTo('#primaryCity');

    $("#secondaryCity").empty().html('');
    var o = $('<option/>', {disabled: true, value: '', selected: true})
            .text('--Select--');
    o.appendTo('#secondaryCity');

    $("#state").empty().html('');
    var o = $('<option/>', {disabled: true, value: '', selected: true})
            .text('--Select--');
    o.appendTo('#state');

    $('select').material_select();

    $('#countryForm label').removeClass("active");
    $('#BranchForm span.required-msg').remove();
    $('#BranchForm input,#BranchForm .select-dropdown, .file-path, .materialize-textarea').removeClass("required");

    $('#branchModal').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

/*Batch*/
function branchBatchInfo(This)
{
    notify('Processing..', 'warning', 10);
    $('#actionButton').html('');
    $('#actionButton').html('<i class="icon-right mr8"></i>Add');
    
    $('#myModalLabel').html('Create Batch');

    $('#createBatch span.required-msg').remove();
    $('#createBatch input').removeClass("required");
    $('#createBatch label').removeClass("active");

    selectOption = 1;

    /*Clear date picker*/
    clearDatePicker('#msd');
    clearDatePicker('#med');
    clearDatePicker('#aed');
    clearDatePicker('#aed');
    var branchId = $('#branchUEId').val();
    var ajaxurl = '';
    var userId = $('#hdnUserId').val();
    var action = 'batch add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
    ajaxurl = API_URL + 'index.php/Courses/getBranchCoursesList';
    var params = {branchId:branchId};
    commonAjaxCall({This: this, params:params, asyncType: true, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: branchCourse});

//    var randomString = stringGen(10);
//    $('#autogenerate').val(randomString);
//    $("#autogenerate").next('label').addClass("active");
    alertify.dismissAll();
    $('#createBatch').modal('toggle');
//    $('#course').find('option:eq(1)').remove();
}

function addBatch(This)
{
    $('#createBatch span.required-msg').remove();
    $('#createBatch input').removeClass("required");

    var error = false;
    var errorMsg = '';
    var action = '';
    var branch_id = $('#branchId').val();
    var alias_name = $.trim($('#txtAlias').val());
    var msd = $("#msd").val();
    var med = $("#med").val();
    var asd = $("#asd").val();
    var aed = $("#aed").val();
    var target = $("#target").val();
    var visitorTarget = $("#visitorTarget").val();
    var sequence = $("#sequence").val();
    var autogenerate = $("#autogenerate").val();
    var selMulti = $.map($("#course option:selected"), function (el, i) {
        if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'all') {
            return $(el).val();
        }
    });
    var course = selMulti.join(",");
    if (msd == '')
    {
        error = true;
        $("#msd").addClass('required');
        $("#msd").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
    }

    if (med == '')
    {
        error = true;
        $("#med").addClass('required');
        $("#med").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
    }

    if (asd == '')
    {
        error = true;
        $("#asd").addClass('required');
        $("#asd").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
    }

    if (aed == '')
    {
        error = true;
        $("#aed").addClass('required');
        $("#aed").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
    }

    if (course == "") {
        $("#course").parent().find('.select-dropdown').addClass("required");
        $("#course").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        error = true;
    }

    /*if (sequence == "") {
     $("#sequence").addClass('required');
     $("#sequence").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
     error = true;
     }else if(!(/^[0-9]+$/.test(sequence))){
     $("#sequence").addClass("required");
     $("#sequence").after('<span class="required-msg">Invalid Sequence, must contain only Numbers</span>');
     error = true;
     }*/
    /*if(autogenerate == ""){
     $("#autogenerate").addClass('required');
     $("#autogenerate").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
     error = true;
     }*/

    if (target == "") {
        $("#target").addClass('required');
        $("#target").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        error = true;
    }
    if (visitorTarget == "") {
        $("#visitorTarget").addClass('required');
        $("#visitorTarget").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        error = true;
    }

    var url = '';
    var data = '';
    var userId = $('#hdnUserId').val();

    var id = $("#batchId").val();
    if (!error)
    {
        var error = true;
        var msdTimestamp = ((new Date(msd)).getTime()) / 1000;
        var medTimestamp = ((new Date(med)).getTime()) / 1000;
        var asdTimestamp = ((new Date(asd)).getTime()) / 1000;
        var aedTimestamp = ((new Date(aed)).getTime()) / 1000;

        //if (medTimestamp <= msdTimestamp)
        if (msdTimestamp > medTimestamp)
        {
            error = true;
            $("#med").addClass('required');
            $("#msd").addClass('required');
            $("#msd").after('<span class="required-msg">invalid</span>');
            $("#med").after('<span class="required-msg">invalid</span>');
            errorMsg = 'Marketing start date cannot be > marketing end date';
            notify(errorMsg, 'error', 10);
            return;
        }
        else
        {
            error = false;
        }

        if (asdTimestamp > aedTimestamp)
        {
            error = true;
            $("#aed").addClass('required');
            $("#asd").addClass('required');
            $("#asd").after('<span class="required-msg">invalid</span>');
            $("#aed").after('<span class="required-msg">invalid</span>');
            errorMsg = 'Academic start date cannot be > academic end date';
            notify(errorMsg, 'error', 10);
            return;
        }
        else
        {
            error = false;
        }

        //if (asdTimestamp < msdTimestamp)
        if (msdTimestamp > asdTimestamp)
        {
            error = true;
            $("#msd").addClass('required');
            $("#asd").addClass('required');
            $("#msd").after('<span class="required-msg">invalid</span>');
            $("#asd").after('<span class="required-msg">invalid</span>');
            //errorMsg = 'Marketing Start date should be greater than or equal to Academic Start date';
            errorMsg = 'Marketing start date cannot be > academic start date';
            notify(errorMsg, 'error', 10);
            return;
        }
        else
        {
            error = false;
        }

        //if (msdTimestamp <= asdTimestamp && asdTimestamp >= medTimestamp)
        if (asdTimestamp > medTimestamp)
        {
            error = true;
            $("#msd").addClass('required');
            $("#asd").addClass('required');
            $("#med").addClass('required');
            $("#msd").after('<span class="required-msg">invalid</span>');
            $("#asd").after('<span class="required-msg">invalid</span>');
            $("#med").after('<span class="required-msg">invalid</span>');
            errorMsg = 'Academic start date cannot be > marketing end date';
            notify(errorMsg, 'error', 10);
            return;
        }
        else
        {
            error = false;
        }

        if (aedTimestamp < msdTimestamp)
        {
            error = true;
            $("#msd").addClass('required');
            $("#asd").addClass('required');
            $("#med").addClass('required');
            $("#msd").after('<span class="required-msg">invalid</span>');
            $("#asd").after('<span class="required-msg">invalid</span>');
            $("#med").after('<span class="required-msg">invalid</span>');
            errorMsg = 'Academic end date cannot be < marketing start date';
            notify(errorMsg, 'error', 10);
            return;
        }
        else
        {
            error = false;
        }

        if (!error)
        {
            notify('Processing..', 'warning', 10);
            data = {
                msd: msd,
                med: med,
                asd: asd,
                aed: aed,
                alias_name: alias_name,
                target: target,
                visitorTarget:visitorTarget,
                course: course,
                branch_id: branch_id,
                code: autogenerate,
                userId: userId
            };
            if (id != 0)
            {
                url = API_URL + "index.php/Batch/editBatch/batch_id/" + id;
                action = 'batch edit';
            } else
            {
                url = API_URL + "index.php/Batch/addBatch";
                action = 'batch add';
            }

            var ajaxurl = url;
            var params = data;
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false)
                {
                    var id = '';
                    if(Object.size(response.data) > 0){
                        notify('Validation Error', 'error', 10);
                        for (var a in response.data) {
                            id = '#' + a;
                            if (a == 'course') {
                                $(id).parent().find('.select-dropdown').addClass("required");
                                $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else {
                                $(id).addClass("required");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            }
                        }
                    }else{
                        notify(response.message, 'error', 10);
                    }
                } else
                {
                    notify(response.message, 'success', 10);
                    branchInfoPageLoad();
                    buildBatchDataTable();
                    closeBatchModal();
                }
            }});
        } else
        {
            if(errorMsg.length > 0){
                notify(errorMsg, 'error', 10);
                errorMsg = '';
            }else {
                notify('Validation Error', 'error', 10);
            }
        }
    }
}
/*For getting object length*/
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function getBatchDetailsById(id)
{
    notify('Processing..', 'warning', 10);
    $('#actionButton').html('');
    $('#actionButton').html('<i class="icon-right mr8"></i>Update');
    
    $('#myModalLabel').html('Edit Batch');
    $('#batchCreateForm')[0].reset();

    clearDatePicker('#msd');
    clearDatePicker('#med');
    clearDatePicker('#aed');
    clearDatePicker('#aed');
//    $('#batchCreateForm label').addClass("active");
    $('#batchCreateForm span.required-msg').remove();
    $('#batchCreateForm input').removeClass("required");

    $('#batchId').val(id);

    var ajaxurl = '';
    var userId = $('#hdnUserId').val();
    var branchId = $('#branchId').val();
    var action = 'batch edit';
    var params = {'batch_id': id, 'branch_id': branchId};
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

    ajaxurl = API_URL + 'index.php/Courses/getAllCourseList';
    commonAjaxCall({This: this, asyncType: true, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: branchCourse});

    ajaxurl = API_URL + 'index.php/Batch/getBatchById';
    commonAjaxCall({This: this, params: params, asyncType: true, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
        $('#batchCreateForm')[0].reset();

//        $('#batchCreateForm label').addClass("active");

//        $('.select-label').removeClass("active");   /*Have to be below the add active class*/
        alertify.dismissAll();
        if(response.data['status'] == 'Completed'){
            $("#msd").val(response.data['marketing_startdate']);
            $("#med").val(response.data['marketing_enddate']);
            $("#asd").val(response.data['acedemic_startdate']);
            $("#aed").val(response.data['acedemic_enddate']);
            $("#msd").attr('disabled',true);
            $("#med").attr('disabled',true);
            $("#asd").attr('disabled',true);
            $("#aed").attr('disabled',true);
            $("#aed").attr('disabled',true);
            $('#actionButton').attr('disabled',true);
        } else {
            if(response.data['marketing_startdate_status'] == 'progress' || response.data['marketing_startdate_status'] == 'completed'){
                $("#msd").val(response.data['marketing_startdate']);
                $("#msd").attr('disabled',true);
            }else{
                setDatePicker('#msd', response.data['marketing_startdate']);
            }
            if(response.data['marketing_enddate_status'] == 'progress' || response.data['marketing_enddate_status'] == 'completed'){
                $("#med").val(response.data['marketing_enddate']);
            }else{
                setDatePicker('#med', response.data['marketing_enddate']);
            }
            if(response.data['acedemic_startdate_status'] == 'progress' || response.data['acedemic_startdate_status'] == 'completed'){
                $("#asd").val(response.data['acedemic_startdate']);
                $("#asd").attr('disabled',true);
            }else{
                setDatePicker('#asd', response.data['acedemic_startdate']);
            }
            if(response.data['acedemic_enddate_status'] == 'progress' && response.data['acedemic_enddate_status'] == 'completed'){
                $("#aed").val(response.data['acedemic_enddate']);
            }else{
                setDatePicker('#aed', response.data['acedemic_enddate']);
            }
        }
        if(response.data['alias_name']!='' || response.data['alias_name']!=null)
            $("#txtAlias").val(response.data['alias_name']);
        $("#target").val(response.data['target']);
        $("#visitorTarget").val(response.data['visitor_target']);
        $("#sequence").val(response.data['sequence_number']);
        $("#autogenerate").val(response.data['code']);
//        $("#autogenerate").next('label').addClass("active");

        var sel_courses = response.data['fk_course_id'];
        setTimeout(function () {
            $('#course').find('option:eq(1)').remove();
            $("#course").val(sel_courses);
            $("#course").attr('disabled', true);
            $("#course").material_select();
        }, 250);
        Materialize.updateTextFields();
        $('#createBatch').modal('toggle');
    }});
}

function setDatePicker(id, value)
{
    var $input = $(id).pickadate()
    // Use the picker object directly.
    var picker = $input.pickadate('picker');
    picker.set('select', value, {format: 'yyyy-mm-dd'})
}
function closeBatchModal()
{
    $('#batchId').val('0');
    $('#batchCreateForm')[0].reset();
    $('#myModalLabel').html('Create Batch');

    $('#batchCreateForm label').removeClass("active");
    $('#batchCreateForm span.required-msg').remove();
    $('#batchCreateForm input').removeClass("required");

    $("#msd").attr('disabled',false);
    $("#med").attr('disabled',false);
    $("#asd").attr('disabled',false);
    $("#aed").attr('disabled',false);

    /*Clear date picker*/
    clearDatePicker('#msd');
    clearDatePicker('#med');
    clearDatePicker('#aed');
    clearDatePicker('#asd');

    $('#createBatch').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();

    $("#myModalLabel").html("Create Batch");
    $("#course").attr('disabled', false);
    $('#actionButton').attr('disabled',false);
}

function highlight(string) {
    $('.autocomplete-content li').each(function () {
        var matchStart = $(this).text().toLowerCase().indexOf("" + string.toLowerCase() + ""),
            matchEnd = matchStart + string.length - 1,
            beforeMatch = $(this).text().slice(0, matchStart),
            matchText = $(this).text().slice(matchStart, matchEnd + 1),
            afterMatch = $(this).text().slice(matchEnd + 1);
        $(this).html("<span>" + beforeMatch + "<span class='highlight'>" + matchText + "</span>" + afterMatch + "</span>");
    });
}

function closeBranchManageUser(){
    $('.hide-data span.required-msg').remove();
    $('.hide-data input').removeClass("required");
    $(".hide-data").slideUp();
    $('#userBranchId').val('0');
    $('.autocomplete-content').remove();
    $('#autocomplete').val('');
    $('#days').val('');
    $('.hide-data label').removeClass("active");
}

function deactiveBranchUsers(uid){
    notify('Processing..', 'warning', 10);
    var id = $('#branchId').val();
    var userId = $('#hdnUserId').val();

    var data = {
        userid: uid
    };
    var action = 'manage users';
//            var userId = $('#userId').val();

    var ajaxurl = API_URL + "index.php/Branch/deactivateUser/branch_id/"+id;
    var params = data;
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
    commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
        if (response == -1 || response['status'] == false){
            //
        } else {
            $('.hide-data input').removeClass("required");
            $('.hide-data').parent().find('label').addClass("active");

            branchInfoPageLoad();

            closeBranchManageUser();

            alertify.dismissAll();
            notify(response.message, 'success', 10);
        }
    }});
}

function extendBranchUsers(id, name, This){
    $('#autocomplete').parent().find('label').addClass("active");
    $('.hide-data input').removeClass("required");
    $('#autocomplete').val(name);

    $('#userBranchId').val(id);

    $('.hide-data').slideDown();
}

function saveBranchUsers(This){
    notify('Processing..', 'warning', 10);
    $('.hide-data input').removeClass("required");
    $('.hide-data').parent().find('label').addClass("active");

    var id = $('#branchId').val();
    var uid = $('#userBranchId').val();

    var flag = 0;

    var name = $('#autocomplete').val();
    var days = $('#days').val();

    if (name == '') {
        $("#autocomplete").addClass("required");
        $("#autocomplete").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (!(/^[a-zA-Z ]+$/.test(name))) {
        $("#autocomplete").addClass("required");
        $("#autocomplete").after('<span class="required-msg">Must contain only Alphabets</span>');
        flag = 1;
    }

    if (days == '') {
        $("#days").addClass("required");
        $("#days").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if (!(/^[a-zA-Z0-9]+$/.test(days))) {
        $("#days").addClass("required");
        $("#days").after('<span class="required-msg">Must contain only Numbers</span>');
        flag = 1;
    }
    if(flag != 1){
        var data = {
            name: name,
            userid: uid,
            days: days
        };
        var action = 'manage users';
        var userId = $('#hdnUserId').val();

        var ajaxurl = API_URL + "index.php/Branch/extendUser/branch_id/"+id;
        var params = data;
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
            $('.hide-data input').removeClass("required");
            $('.hide-data').parent().find('label').addClass("active");
            alertify.dismissAll();
            if (response == -1 || response['status'] == false){
                //
            } else {
                $('.hide-data input').removeClass("required");
                $('.hide-data').parent().find('label').addClass("active");

                branchInfoPageLoad();

                closeBranchManageUser();

                alertify.dismissAll();
                notify(response.message, 'success', 10);
            }
        }})
    } else {
        alertify.dismissAll();
        notify('Validation Error', 'error', 10);
    }

}
/*For Generating Batch code*/
function stringGen(len)
{
    var text = " ";

    var charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (var i = 0; i < len; i++)
        text += charset.charAt(Math.floor(Math.random() * charset.length));

    return text;
}
/*Batch Ends*/

var objMenu = '';

var selectOption = 'none';

var stateParentId = 'none';

/*Common master, Add country*/
function AddCountry(This)
{
    notify('Processing..', 'warning', 10);
    $('#countryForm span.required-msg').remove();
    $('#countryForm input').removeClass("required");
    var id = $('#reference_type_val_id').val();

    var data = '';
    var url = '';
    var country = $('#reference_type_val').val();
    var action = '';
    if (id != '')
    {
        $("#countryModalLabel").html("Edit Country");
        data = {
            reference_type_id: 1,
            reference_type_val_id: id,
            reference_type_val: country,
            reference_type_is_active: 1
        };
        url = API_URL + "index.php/Referencevalues/editReferenceValue";
        action = 'edit';
    } else {
        $("#countryModalLabel").html("Create Country");
        data = {
            reference_type_id: 1,
            reference_type_val: country,
            reference_type_is_active: 1
        };
        url = API_URL + "index.php/Referencevalues/addReferenceValue";
        action = 'add';
    }

    var flag = 0;
    var country = $('#reference_type_val').val();

    if (country == '') {
        $("#reference_type_val").addClass("required");
        $("#reference_type_val").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }

    if (flag == 0) {
        var ajaxurl = url;

        var params = data;

        var userId = $('#userId').val();

        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var response = commonAjaxCall({This: This, params: params, headerParams: headerParams, asyncType: true, method: 'POST', requestUrl: ajaxurl, action: action, onSuccess: respCountry});

    } else {
        return false;
    }
}
/*Common master, Add country response*/
function respCountry(response)
{
    if (response == -1 || response['status'] == false)
    {
        var id = '';
        alertify.dismissAll();
        //notify(response['message'], 'error', 10);
        for (var a in response.data) {
            id = '#' + a;
            $(id).addClass("required");
            $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
        }
        $('#countryModal').removeAttr('disabled');
    } else
    {
        //countryPageLoad();
        buildCountriesReferenceDataTable();
        alertify.dismissAll();
        notify('Data Saved Successfully', 'success', 10);
        closeCountryModal();
    }
}

function getCountryDetails(This, id) {
    notify('Processing..', 'warning', 10);
    $('#actionButton').html('');
    $('#actionButton').html('<i class="icon-right mr8"></i>Update');
    
    $('#countryForm label').addClass("active");
    $('#countryForm input').removeClass("required");
    $("#countryModalLabel").html("Edit Country");
    var ajaxurl = API_URL + "index.php/Referencevalues/getSingleReferenceValues";
    var params = {reference_type_value_id: id};
    var userId = $('#userId').val();
    var action = 'edit';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
    var response = commonAjaxCall({This: This, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: 'edit', onSuccess: function (response) {
            if (response == -1 || response['status'] == false)
            {

            } else
            {
                alertify.dismissAll();
                $('#reference_type_val').val(response.data[0].value);
                $('#reference_type_val_id').val(id);
                alertify.dismissAll();
                $('#countryModal').modal('toggle');
            }
        }
    });
}

function closeCountryModal(This) {
	$('#actionButton').html('');
    $('#actionButton').html('<i class="icon-right mr8"></i>Add');
    
    $('#countryForm')[0].reset();

    $('#reference_type_val_id').val('');
    $('#countryForm label').removeClass("active");
    $('#countryForm span.required-msg').remove();
    $('#countryForm input').removeClass("required");

//    $('#countryModal').modal('toggle');
    $('#countryModal').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();

    $("#countryModalLabel").html("Create Country");
}

function getCountriesDropdown() {
    /*Country*/
    var userId = $('#userId').val();
    var action = 'list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
    var params = {name: 'country'};
    /*var ajaxurl=API_URL+'index.php/Referencevalues/getAllReferenceValues';
     var params = {'reference_type_id':1};*/
    commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: countryList});
}

function countryList(response)
{
    if ($("#country").length != 0) {
        $('#country').find('option:gt(0)').remove();
    }
    if ($("#reference_type_val_parent").length != 0) {
        $('#reference_type_val_parent').find('option:gt(0)').remove();
    }
    for (var b in response.data) {
        var oh = $('<option/>', {value: response.data[b]['reference_type_value_id'], "parent-id": response.data[b]['reference_type_value_id']})
                .text(response.data[b]['value']);
        if ($("#country").length != 0) {
            oh.appendTo('#country');
        }
        if ($("#reference_type_val_parent").length != 0) {
            oh.appendTo('#reference_type_val_parent');
        }
    }
    if ($("#country").length != 0) {
        $('#country').material_select();
    }

    if ($("#reference_type_val_parent").length != 0) {
        $('#reference_type_val_parent').material_select();
    }
}

function AddState(This) {
    $('#countryForm span.required-msg').remove();
    $('#countryForm input').removeClass("required");
    //reference_type_val_id
    var id = $('#reference_type_val_id').val();

    var data = '';
    var url = '';
    var state = $('#reference_type_val').val();
    var country = $("select[name=reference_type_val_parent]").val();
    var action = '';
    if (id != '')
    {
        data = {
            reference_type_id: 2,
            reference_type_val_id: id,
            reference_type_val: state,
            reference_type_val_parent: country,
            reference_type_is_active: 1
        };
        url = API_URL + "index.php/Referencevalues/editReferenceValue";
        action = 'edit';
    } else {
        $("#countryModalLabel").html("Create State");
        data = {
            reference_type_id: 2,
            reference_type_val: state,
            reference_type_val_parent: country,
            reference_type_is_active: 1
        };
        url = API_URL + "index.php/Referencevalues/addReferenceValue";
        action = 'add';
    }

    var flag = 0;

    if (state == '') {
        $("#reference_type_val").addClass("required");
        $("#reference_type_val").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }

    if (country === null) {
        $("#reference_type_val_parent").parent().find('.select-dropdown').addClass("required");
        $("#reference_type_val_parent").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }

    if (flag == 0) {
        var userId = $('#userId').val();
        var ajaxurl = url;
        var params = data;
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var response = commonAjaxCall({This: This, asyncType: true, headerParams: headerParams, method: 'POST', requestUrl: ajaxurl, params: params, action: action, onSuccess: function (response) {
                if (response == -1 || response['status'] == false)
                {
                    var id = '';
                    alertify.dismissAll();
                    //notify(response['message'], 'error', 10);
                    for (var a in response.data) {
                        id = '#' + a;
                        if (a == 'country') {
                            $(id).parent().find('.select-dropdown').addClass("required");
                            $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                        } else {
                            $(id).addClass("required");
                            $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                        }
                    }
                    $(This).removeAttr('disabled');
                } else
                {
                    //statePageLoad();
                    buildStatesReferenceDataTable();
                    $('#countryForm')[0].reset();
                    $('#countryForm label').removeClass("active");
                    $('#countryModal').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    alertify.dismissAll();
                    notify('Data Saved Successfully', 'success', 10);
                }
            }
        });
    } else {
        return false;
    }
}

function getStateDetails(This, id) {
    getCountriesDropdown();
    if(id==''){
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Add');
    } else {
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Update');
    }

    $('#countryForm .select-label').removeClass("active");
    $('#countryForm input').removeClass("required");
    if (id == 0) {
        $("#countryModalLabel").html("Create State");
        $('#reference_type_val_id').val('');
        $('#countryModal').modal('toggle');
    } else {
        notify('Processing..', 'warning', 10);
        $("#countryModalLabel").html("Edit State");
        var userId = $('#userId').val();
        var ajaxurl = API_URL + "index.php/Referencevalues/getSingleReferenceValues";
        var params = {reference_type_value_id: id};
        var action = 'edit';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var response = commonAjaxCall({This: This, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: function (response) {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false)
                {
                } else
                {
                    $('#reference_type_val').val(response.data[0].value);
                    $("#reference_type_val").next('label').addClass("active");
                    $('#reference_type_val_parent').val(response.data[0].parent_id);
                    $('select').material_select();

                    $('#reference_type_val_id').val(id);

                    $('#countryModal').modal('toggle');
                }
            }
        });
    }
}

function closeStateModal(This) {
    $('#countryForm')[0].reset();

    $('#reference_type_val_id').val('');
    $('#countryForm label').removeClass("active");
    $('#countryForm span.required-msg').remove();
    $('#countryForm input').removeClass("required");

    $("select[name=state]").val(0);
    $('select').material_select();

    $('#countryModal').modal('toggle');

    $("#countryModalLabel").html("Create State");
}

function AddCity(This) {
    $('#countryForm span.required-msg').remove();
    $('#countryForm input').removeClass("required");
    var id = $('#reference_type_val_id').val();
    var data = '';
    var url = '';
    var city = $('#reference_type_val').val();
    var state = $("select[name=state]").val();
    var country = $("select[name=country]").val();
    var action = '';
    if (id == 0)
    {
//        $("#countryModalLabel").html("Create City");
        data = {
            reference_type_id: 3,
            reference_type_val_id: id,
            reference_type_val: city,
            reference_type_val_parent: state,
            reference_type_is_active: 1
        };
        url = API_URL + "index.php/Referencevalues/addReferenceValue";
        action = 'add';
    } else
    {
//        $("#countryModalLabel").html("Edit City");
        data = {
            reference_type_id: 3,
            reference_type_val_id: id,
            reference_type_val: city,
            reference_type_val_parent: state,
            reference_type_is_active: 1
        };
        url = API_URL + "index.php/Referencevalues/editReferenceValue";
        action = 'edit';
    }
    var flag = 0;

    if (country == '' || country == null) {
        $("#country").parent().find('.select-dropdown').addClass("required");
        $("#country").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }

    if (state === '' || state == null) {
        $("#state").parent().find('.select-dropdown').addClass("required");
        $("#state").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }

    if (city == '') {
        $("#reference_type_val").addClass("required");
        $("#reference_type_val").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }
    if (flag == 0) {
        var userId = $('#userId').val();
        var ajaxurl = url;
        var params = data;
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
                if (response == -1 || response['status'] == false)
                {
                    var id = '';
                    alertify.dismissAll();
                    //notify(response['message'], 'error', 10);
                    for (var a in response.data) {
                        id = '#' + a;
                        if (a == 'country') {
                            $(id).parent().find('.select-dropdown').addClass("required");
                            $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                        } else {
                            $(id).addClass("required");
                            $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                        }
                    }
                    $(This).removeAttr('disabled');
                } else
                {
                    buildCitiesReferenceDataTable();
                    closeCityModal(This);
                    alertify.dismissAll();
                    notify('Data Saved Successfully', 'success', 10);
                }
            }
        });
    } else {
        return false;
    }
}

function getCityDetails(This, id) {
    if(id==''){
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Add');
    } else {
        $('#actionButton').html('');
        $('#actionButton').html('<i class="icon-right mr8"></i>Update');
    }
    
    $('#countryForm .select-label').removeClass("active");
    $('#countryForm input').removeClass("required");

    var userId = $('#userId').val();
    notify('Processing..', 'warning', 10);
    if (id == 0) {
        $("#countryModalLabel").html("Create City");
        $('#reference_type_val_id').val('');
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall
        (
            {
                This: This,
                headerParams: headerParams,
                requestUrl: API_URL + "index.php/Referencevalues/getReferenceValuesByName",
                params: {'name': 'country'},
                action: action,
                onSuccess: function (response) {
                    branchCountry(response);
                    $('#country').material_select();
                    alertify.dismissAll();
                    $('#countryModal').modal('toggle');
                }
            }
        )
    } else {
        var ajaxurl = API_URL + "index.php/Referencevalues/getSingleReferenceValues";
        var params = {reference_type_value_id: id, ref_type_value: 'country'};
        var action = 'edit';

        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: This, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: function (response)
            {
                if (response == -1 || response['status'] == false)
                {
                } else
                {
                    $.when(
                        commonAjaxCall
                        (
                            {
                                This: This,
                                headerParams: headerParams,
                                requestUrl: API_URL + "index.php/Referencevalues/getReferenceValuesByName",
                                params: {'name': 'country'},
                                action: action
                            }
                        ),
                        commonAjaxCall
                        (
                            {
                                This: This,
                                headerParams: headerParams,
                                requestUrl: API_URL + "index.php/Referencevalues/getReferenceValuesUsingName",
                                params: {parent_id: response.data[2].id, name: 'state'},
                                action: action
                            }
                        )
                    ).then(function (response1, response2) {
                        var country = response1[0];
                        var state = response2[0];

                        /*Country*/
                        branchCountry(country);
                        $('#country').material_select();
                        getStateList(state);
                        $('#state').material_select();
                        $('#country').val(response.data[2].id);
                        $("#state").val(response.data[1].id);
                        $('#reference_type_val').next('label').addClass("active");
                        $('#reference_type_val_id').val(response.data[0].id);
                        $('#reference_type_val').val(response.data[0].value);
                        $('#country').material_select();
                        $('#state').material_select();
                        alertify.dismissAll();
                        $('#countryModal').modal('toggle');


                    });
                }
            }});
    }
}

function respCityData(response)
{
    if (response == -1 || response['status'] == false)
    {
        //
    } else
    {
         alertify.dismissAll();
        $('#reference_type_val').val(response.data[0].value);

        $("#country").trigger("change");
        $('#country').val(response.data[2].id);
        $('#reference_type_val_parent').val(response.data[0].parent_id);
        $('#reference_type_val_id').val(response.data[0].id);

        $("select[name=state]").val(response.data[1].id);
//        $('select').material_select();
        $('#countryModal').modal('toggle');
    }
}

function closeCityModal(This) {
    $('#countryForm')[0].reset();

	$('#country').find('option:gt(0)').remove();
	$('#state').find('option:gt(0)').remove();
    $('#reference_type_val_id').val('');
    $('#countryForm label').removeClass("active");
    $('#countryForm span.required-msg').remove();
    $('#countryForm input').removeClass("required");

    $("select[name=state]").val(0);
    $("select[name=country]").val(0);
    $('select').material_select();

    $('#countryModal').modal('toggle');

    $("#countryModalLabel").html("Create City");
}

$('select').on('contentChanged', function () {
    // re-initialize (update)
    $(this).material_select();
});

function customStyleDropdown(objectName, objectValue, actionType) {
    if (actionType == 'append') {
        objectName.append(objectValue);
    } else if (actionType == 'insert') {
        objectName.html(objectValue);
    }

    objectName.trigger('contentChanged');
}

$("#country").change(function () {

    var parent = $('#country').val();

    $('#state').find('option:gt(0)').remove();
    $('#city').find('option:gt(0)').remove();
    $('#primaryCity').find('option:gt(0)').remove();
    $('#secondaryCity').find('option:gt(0)').remove();
	$('select').material_select();
    if (stateParentId != 'none')
    {
        parent = stateParentId;
        selectOption = 'none';
        stateParentId = 'none';
    }
    
    var userId = $('#userId').val();
    var action = 'list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
    
    $.when(
	commonAjaxCall
	(
            {
                This: this,
                headerParams: headerParams,
                requestUrl: API_URL + "index.php/Referencevalues/getReferenceValuesUsingName",
                params: {parent_id: parent, name: 'state'},
                action: action
            }
	),
	commonAjaxCall
	(
            {
                This: this,
                headerParams: headerParams,
                requestUrl: API_URL + "index.php/Branch/getPrimaryCity",
                params: {country_id: parent},
                action: action
            }
	)
    ).then(function (response1, response2) {
        var state = response1[0];
        var city = response2[0];
        getStateList(state);
        getCityList(city);
    });
});

function getStateList(response)
{
    var requiredClass = $('#state').parent().find('.select-dropdown').hasClass('required');
    $('#state').find('option:gt(0)').remove();
    if (response.data.length > 0){
        if (typeof response.data[0].reference_type_value_id !== 'undefined')
        {
            for (var b = 0; b < response.data.length; b++) {
                var oh = $('<option/>', {value: response.data[b]['reference_type_value_id'], "parent-id": response.data[b]['reference_type_value_id']})
                        .text(response.data[b]['value']);
                oh.appendTo('#state');
            }
        } else {
            for (var b = 0; b < response.data.length; b++) {
                var oh = $('<option/>', {value: response.data[b]['Id1'], "parent-id": response.data[b]['Id2']})
                        .text(response.data[b]['State']);
                oh.appendTo('#state');
            }
        }
    }
//    if (response.data.length > 0 && typeof response.data[0].reference_type_value_id !== 'undefined')
//    {
        $('#state').material_select();
//    }
    if (requiredClass) {
        $('#state').parent().find('.select-dropdown').addClass("required");
    }
}

$("#state").change(function () {
    var parent = $('#state').val();
    if (stateParentId != 'none')
    {
        parent = stateParentId;
        stateParentId = 'none';
    }
    var userId = $('#userId').val();
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesUsingName';
    var params = {parent_id: parent, name: 'city'};
    /*Old*/
    /*var ajaxurl=API_URL+'index.php/Referencevalues/getReferenceValues';
     //    var ajaxurl=API_URL+'index.php/Branch/getPrimaryCity';
     var params = {parent_id: parent, reference_type_id: 3};*/
    var action = 'list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

    commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: getCityDropDown});
});

function getCityDropDown(response) {
    var requiredClass = $('#city').parent().find('.select-dropdown').hasClass('required');
    $('#city').find('option:gt(0)').remove();
    for (var b = 0; b < response.data.length; b++) {
        if (typeof response.data[b]['value'] !== 'undefined') {
            var oh = $('<option/>', {value: response.data[b]['reference_type_value_id'], 'parent-id': response.data[b]['stateId']})
                    .text(response.data[b]['value']);
            oh.appendTo('#city');
        } else if (typeof response.data[b]['city'] !== 'undefined') {
            var oh = $('<option/>', {value: response.data[b]['reference_type_value_id'], 'parent-id': response.data[b]['stateId']})
                    .text(response.data[b]['city']);
            oh.appendTo('#city');
        }
    }

    $('#city').material_select();
    if (requiredClass) {
        $('#city').parent().find('.select-dropdown').addClass("required");
    }
}

$("#city").change(function () {
    var requiredPrimaryClass = $('#primaryCity').parent().find('.select-dropdown').hasClass('required');
    var stateId = $(this).val();
    $("select[name=primaryCity]").val(stateId);
    $('#primaryCity').material_select();
    if (requiredPrimaryClass) {
        $('#primaryCity').parent().find('.select-dropdown').addClass("required");
    }
});
function getCityList(response)
{
    var requiredClass = $('#city').parent().find('.select-dropdown').hasClass('required');
    var requiredPrimaryClass = $('#primaryCity').parent().find('.select-dropdown').hasClass('required');
    //var requiredSecondaryClass = $('#secondaryCity').parent().find('.select-dropdown').hasClass('required');

    if (response == -1 || response.data.length<=0 ||response['status'] == false)
    {
    } else
    {
        $('#primaryCity').find('option:gt(0)').remove();
        $('#secondaryCity').find('option:gt(0)').remove();

//        $('#primaryCity').append($('<option></option>').val('all').html('--Select All--'));
        for (var b = 0; b < response.data.length; b++) {
            var oh = $('<option/>', {value: response.data[b]['Id1'], "parent-id": response.data[b]['Id2']})
                    .text(response.data[b]['City'] + ' - ' + response.data[b]['State']);
            oh.appendTo('#primaryCity');
        }
//        $('#secondaryCity').append($('<option></option>').val('all').html('--Select All--'));
        for (var b = 0; b < response.data.length; b++) {
            var oh = $('<option/>', {value: response.data[b]['Id1'], "parent-id": response.data[b]['Id2']})
                    .text(response.data[b]['City'] + ' - ' + response.data[b]['State']);
            oh.appendTo('#secondaryCity');
        }
        $('#primaryCity').material_select();
        $('#secondaryCity').material_select();

        if (requiredPrimaryClass) {
            $('#primaryCity').parent().find('.select-dropdown').addClass("required");
        }
        /*if(requiredSecondaryClass){
         $('#secondaryCity').parent().find('.select-dropdown').addClass("required");
         }*/
        selectOption = 'none';
        stateParentId = 'none';
    }
}

$("#course").change(function () {
    multiSelectDropdown($(this));
});
$selectedOption = 0;
$("#primaryCity").change(function () {
    multiSelectDropdown($(this));
    if ($selectedOption == '1')
    {
        var cityId = $('#city').val();
        $('#primaryCity').val(cityId);
        $('#primaryCity').material_select();
        $selectedOption = '0';
    }
});

$("#secondaryCity").change(function () {
    multiSelectDropdown($(this));
});

/*Fee Structure*/
function buidFeeStructureData(response) {
    $('#contentBody input').parent().find('label').addClass("active");

    var data = response.data['feestructure'];
    var item = response.data['feeitems'];
    var tot = 0;
    var duedays = 0;
    var amt = 0;
    for (var b in item)
    {
        selectOption = item[b].name;
        addFeeHead(item[b].due_days, item[b].amount);
        //tot += parseInt(item[b].due_days);
        if(parseInt(item[b].due_days) > duedays)
            duedays = parseInt(item[b].due_days);
        amt += parseInt(item[b].amount);
    }
    if (data.fk_branch_id) {
        $('#branch').val(data.fk_branch_id);
        $('#branch').material_select();
    }
    else if(data.fk_branch_id==null){
        $('#branch').val('');
        $('#branch').material_select();
    }
    $('#branch').trigger('change');

    setTimeout(function () {
        $('#course').val(data.fk_course_id);
        $('#course').material_select();
    }, 1000);

    $('#name').val(data.name);

    if (data.type_id == '13') {
        $('#Feetype').val(data.type_id);
        $('.feeStructureCreate-groupname').show();
        $('#groupvalue').val(data.fk_institution_id);
        $('#groupvalue').material_select();
    } else if (data.type_id == '12') {
        $('#Feetype').val(data.type_id);
        $('.feeStructureCreate-companyname').show();
        $('#corporate').val(data.fk_corporate_company_id);
        $('#corporate').material_select();
    } else if (data.type_id == '11') {
        $('#Feetype').val(data.type_id);
    }
    //console.log(data);
    setDatePicker('#effective', data.effective_from);
    //effective_from
    $('#Feetype').material_select();

    /*var check = data.is_allowed_for_class_room_training == 1 ? true : false;
     $('#class-room').prop("checked", check);
     check = data.is_allowed_for_online_training == 1 ? true : false;
     $('#onlineC').prop("checked", check);*/
    var check = data.is_partial_payment_allowed == 1 ? true : false;
     $('#partial-payment').prop("checked", check);
     check = data.is_regular_fee == 1 ? true : false;
     $('#regular-fee').prop("checked", check);
    if(data.fk_training_type!=null && data.fk_training_type!=''){
        $('#Trainingtype').val(data.fk_training_type);
        $('#Trainingtype').material_select();
    }
    $('#decription').val(data.description);
    $("#decription").next('label').addClass("active");

    setTimeout(function () {
        $('#days').html(duedays);
        $('#total').html(moneyFormat(amt));
    }, 1000);
}

function addFeeHead(val1, val2) {
    var selectedFeeHead = $('#fee-head').val();
    
    if(selectedFeeHead.trim().length <= 0 && !(selectOption != 0))
    {
        alertify.dismissAll();
        notify('Please select Fee Head', 'error', 10);
    }
    else
    {
        if ($('#fee-item-list > tbody > tr').length > 1 && ($('#days').html() <= 0 || $('#days').html() == "") && ($('#total').html() <= 0 || $('#total').html() == ""))
        {
                alertify.dismissAll();
                notify('Please fill the Fee Structure Items', 'error', 10);
        }
        else
        {
            if (selectOption.length > 0)
            {
                selectedFeeHead = selectOption;
                selectOption = 0;
            }
            var userId = $('#userId').val();
            var action = 'add';
            var ajaxurl = API_URL + 'index.php/FeeHead/getSingleFeeHeadDetails';
            var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var params = {FeeHead: selectedFeeHead};
            commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                if (response == -1 || response['status'] == false)
                {
                } else {
                    var feeHeadData = response.data[0];
                    var htmlData = '<tr>';
                    htmlData += '<td class="w200">' + feeHeadData.fee_head_name + '</td>';
                    htmlData += '<td>' + feeHeadData.fee_company_name + '</td>';
                    if (val1 == 0)
                    {
                        htmlData += '<td class="w150 text-right"><input name="dueDays[]" maxlength="3" onkeypress="return isNumberKey(event)" onkeyup="calc()" value="0" type="text" data-head-id="' + feeHeadData.fee_head_id + '" class="noOfDays input-righttext"></td>';
                    }
                    else
                    {
                        htmlData += '<td class="w150 text-right"><input name="dueDays[]" maxlength="3" onkeypress="return isNumberKey(event)" onkeyup="calc()" value="' + val1 + '" type="text" data-head-id="' + feeHeadData.fee_head_id + '" class="noOfDays input-righttext"></td>';
                    }
                    if (val2 == 0)
                    {
                        htmlData += '<td class="w150 text-right"><input name="totAmt[]" onkeypress="return isNumberKey(event)" onkeyup="calc()" type="text" data-head-id="' + feeHeadData.fee_head_id + '" class="totAmt input-righttext"></td>';
                    }
                    else
                    {
                        htmlData += '<td class="w150 text-right"><input name="totAmt[]" onkeypress="return isNumberKey(event)" onkeyup="calc()" value="' + val2 + '" type="text" data-head-id="' + feeHeadData.fee_head_id + '" class="totAmt input-righttext"></td>';
                    }
                    htmlData += '<td class="w25"><div class="dropdown feehead-list custom-dropdown-style pull-right">';
                    htmlData += '<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#" data-target="#" id="dLabel">';
                    htmlData += '<i class="fa fa-ellipsis-v f18 pr10 pl10"></i>';
                    htmlData += '</a>';
                    htmlData += '<ul aria-labelledby="dLabel" class="dropdown-menu pull-right">';
                    htmlData += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    htmlData += '<li><a href="javascript:;" class="deleteForFeeHead" onclick="deleteAll(this)">Delete</a></li>';
                    htmlData += '</ul>';
                    htmlData += '</div>';
                    htmlData += '</td>';
                    htmlData += '</tr>';
                    var len = $('#fee-item-list > tbody > tr').length;
                    if(len == 1)
                    {
                        $(htmlData).prependTo("#fee-item-list");
                    }
                    else
                    {
                        len -= 1;
                        $("#fee-item-list > tbody > tr:nth-child(" + (len) + ")").after(htmlData);
                        calc();
                    }
                    $('#fee-head').val(0);
                    $('#fee-head').material_select();
                }
            }});
        }
    }
}

    function calc() {
        var tot = 0;
        var duedays = 0;
        var amt = 0;
        var re = /^[0-9]*$/;
        $('input[name="dueDays[]"]').each(function () {
            /*var data = $(this).val();
            $(this).removeClass('required');
            if (!re.test(data))
            {
                $(this).addClass('required')
            }
            else if (data.length <= 0)
            {
                $('#days').html(0);
            } else
            {
//            tot += parseInt(data);
                if (parseInt(data) > duedays)
                {
                    duedays = parseInt(data);
                }
            }*/
            if(duedays < $(this).val())
            {
                duedays = $(this).val();
            }
        });
        $('#days').html(duedays);

        $('input[name="totAmt[]"]').each(function () {
            var data = $(this).val();
            $(this).removeClass('required');
            if (!re.test(data)) {
                $(this).addClass('required')
            } else if (data.length <= 0) {
                $('#total').html(0);
            } else {
                amt += parseInt(data);
            }
        });
        if (re.test(amt))
            $('#total').html(moneyFormat(amt));

    }

function moneyFormat(x) {
    x = x.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
}

function deleteAll(This) {
    $(This).closest('tr').remove();
    calc();
}

function branchDropDown(response)
{
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            //
        } else
        {
            var branchRawData = response.data;
            $('#branch').find('option:gt(0)').remove();
//        $('#branch').append($('<option></option>').val('all').html('--Select All--'));
            for (var b = 0; b < branchRawData.length; b++) {
                var o = $('<option/>', {value: branchRawData[b]['id']})
                        .text(branchRawData[b]['name']);
                o.appendTo('#branch');
            }
            $('#branch').material_select();
        }
}

function feeTypeDropDown(response){
    $('#Feetype').find('option:gt(0)').remove();
    if (response.data.length == 0 || response == -1 || response['status'] == false)
    {
        //
    } else
    {
        var branchRawData = response.data.reverse();
        for (var b = 0; b < branchRawData.length; b++) {
            var o = $('<option/>', {value: branchRawData[b]['reference_type_value_id']})
                    .text(branchRawData[b]['value']);
            o.appendTo('#Feetype');
        }
        $('#Feetype').material_select();
    }
}
function trainingTypeDropDown(response){
    $('#Trainingtype').find('option:gt(0)').remove();
    if (response.data.length == 0 || response == -1 || response['status'] == false)
    {
        //
    } else
    {
        var branchRawData = response.data.reverse();
        for (var b = 0; b < branchRawData.length; b++) {
            var o = $('<option/>', {value: branchRawData[b]['reference_type_value_id']})
                .text(branchRawData[b]['value']);
            o.appendTo('#Trainingtype');
        }
        $('#Trainingtype').material_select();
    }
}

function corporateDropDown(response)
{
    $('#corporate').find('option:gt(0)').remove();
    if (response.data.length == 0 || response == -1 || response['status'] == false)
    {
        //
    } else
    {
        var branchRawData = response.data;
//        $('#branch').append($('<option></option>').val('all').html('--Select All--'));
        for (var b = 0; b < branchRawData.length; b++) {
            var o = $('<option/>', {value: branchRawData[b]['reference_type_value_id']})
                    .text(branchRawData[b]['value']);
            o.appendTo('#corporate');
        }
        $('#corporate').material_select();
    }
}

function groupDropDown(response)
{
	$('#groupvalue').find('option:gt(0)').remove();
    if (response.data.length == 0 || response == -1 || response['status'] == false)
    {
        //
    } else
    {
        var branchRawData = response.data;
//        $('#branch').append($('<option></option>').val('all').html('--Select All--'));
        for (var b = 0; b < branchRawData.length; b++) {
            var o = $('<option/>', {value: branchRawData[b]['reference_type_value_id']})
                    .text(branchRawData[b]['value']);
            o.appendTo('#groupvalue');
        }
        $('#groupvalue').material_select();
    }
}

function feeHeadDropDown(response)
{
    if (response.data.length == 0 || response == -1 || response['status'] == false)
    {
        //
    } else
    {
        var branchRawData = response.data;
        $('#fee-head').find('option:gt(0)').remove();
//        $('#branch').append($('<option></option>').val('all').html('--Select All--'));
        for (var b = 0; b < branchRawData.length; b++) {
            /*var o = $('<option/>', {value: branchRawData[b]['fee_head_id']})
                    .text(branchRawData[b]['name']);*/
            var o = $('<option/>', {value: branchRawData[b]['name']})
                    .text(branchRawData[b]['name']);
            o.appendTo('#fee-head');
        }
        $('#fee-head').material_select();
    }
}

function getItemDetails()
{
    var final = [];
    var duedate = [];
    var erroMsg = '';
    var noOfDays = $("input[name='dueDays[]']").map(function ()
    {
        if ($(this).val() == '' || $(this).val() == null)
        {
            $(this).addClass('required');
            return false;
        }
        else
        {
            duedate[$(this).val()] = {'data': $(this).val()};
            return {'value': $(this).val(), 'id': $(this).attr('data-head-id')}
        }
    }).get();

    for (var i in noOfDays)
    {
        if(!noOfDays[i])
        {
            erroMsg = 'blank';
            break;
        }
        else if(noOfDays[i] == 'duplicate')
        {
            erroMsg = 'duplicate';
            break;
        }
        else if(noOfDays[i] == 'blank')
        {
            erroMsg = 'blank';
        }
    }

    if (erroMsg != 'duplicate' && erroMsg != 'blank') {
        var totAmt = $("input[name='totAmt[]']").map(function ()
        {
            if ($(this).val() == '' || $(this).val() == null) {
                $(this).addClass('required');
                return false;
            } else if($(this).val() == 0){
                $(this).addClass('required');
                return 'zero';
            } else {
                return {'value': $(this).val(), 'id': $(this).attr('data-head-id')}
            }
        }).get();
        for (var i in totAmt){
            if(!totAmt[i]){
                erroMsg = 'blank';
                break;
            } else if(totAmt[i] == 'zero') {
                erroMsg = 'zero';
            }
        }
        if(erroMsg == 'blank'){
            return 'amtblank';
        }else if(erroMsg == 'zero'){
            return 'zero';
        }else{
            return {'noOfDays': noOfDays, 'totAmt': totAmt}
        }
    } else {
        return erroMsg;
    } 
}

    function addFeeStructure(This){
        var errorFor = '';
        $('#feeStructure span.required-msg').remove();
        $('#feeStructure input').removeClass("required");

        var item = getItemDetails();
        //console.log(item);
        var flag = 0;
        var id = $('#feeId').val();
        var selMulti = $.map($("#branch option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {
                return $(el).val();
            }
        });
        var branch = selMulti.join(",");
        if($('#branch > option').length-1 == selMulti.length){
            var branch=0;
        }
        var course = $('#course').val();
        var name = $('#name').val();
        var effective = $("#effective").val();
        var classRoom = $('#class-room').is(":checked") ? '1' : '0';
        var online = $('#onlineC').is(":checked") ? '1' : '0';
        var partialPayment = $('#partial-payment').is(":checked") ? 1 : 0;
        var regularFee = $('#regular-fee').is(":checked") ? 1 : 0;
        var trainingType=$("#Trainingtype").val();
        var feetype = $('#Feetype option:selected').text().toLowerCase();
        var companyName = $('#corporate').val();
        var groupName = $('#groupvalue').val();
        var description = $('#decription').val();

        if (name == '')
        {
            $("#name").addClass("required");
            $("#name").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (effective == '')
        {
            $("#effective").addClass("required");
            $("#effective").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (branch === '')
        {
            $("#branch").parent().find('.select-dropdown').addClass("required");
            $("#branch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
            branch = 0;
        }
        if (course == '' || course == null)
        {
            $("#course").parent().find('.select-dropdown').addClass("required");
            $("#course").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (feetype == '--select--' || feetype == null)
        {
            $("#Feetype").parent().find('.select-dropdown').addClass("required");
            $("#Feetype").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (trainingType == '--select--' || trainingType == null || trainingType == '')
        {
            $("#Trainingtype").parent().find('.select-dropdown').addClass("required");
            $("#Trainingtype").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (feetype == 'group') {
            if (groupName == '' || groupName == null)
            {
                $("#groupvalue").parent().find('.select-dropdown').addClass("required");
                $("#groupvalue").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;
            }
        } else if (feetype == 'corporate') {
            if (companyName == '' || companyName == null)
            {
                $("#corporate").parent().find('.select-dropdown').addClass("required");
                $("#corporate").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;
            }
        }

        if ($('#fee-item-list > tbody > tr').length <= 1) {
            errorFor = 'item';
            flag = 1;
        }

        if (flag != 1) {
            if (item == 'blank') {
                alertify.dismissAll();
                notify('Due Days can\'t be left blank', 'error', 10);
                flag = 1;
            }else if (item == 'amtblank') {
                alertify.dismissAll();
                notify('Amount can\'t be left blank', 'error', 10);
                flag = 1;
            } else if (item == 'duplicate') {
                alertify.dismissAll();
                notify('Due Date can\'t be duplicate', 'error', 10);
                flag = 1;
            } else if (item == 'noamt') {
                alertify.dismissAll();
                notify('Amount can\'t be blank', 'error', 10);
                flag = 1;
            } else if (item == 'zero') {
                alertify.dismissAll();
                notify('Amount can\'t be zero', 'error', 10);
                flag = 1;
            } else {
                if (feetype == 'group') {
                    var data = {
                        branch: branch,
                        course: course,
                        name: name,
                        classRoom: classRoom,
                        online: online,
                        feetype: feetype,
                        groupname: groupName,
                        description: description,
                        item: item,
                        effective: effective,
                        trainingType:trainingType,
                        partialPayment:partialPayment,
                        regularFee:regularFee
                    };
                } else if (feetype == 'corporate') {
                    var data = {
                        branch: branch,
                        course: course,
                        name: name,
                        classRoom: classRoom,
                        online: online,
                        feetype: feetype,
                        companyName: companyName,
                        description: description,
                        item: item,
                        effective: effective,
                        trainingType:trainingType,
                        partialPayment:partialPayment,
                        regularFee:regularFee
                    };
                } else if (feetype == 'retail') {
                    var data = {
                        branch: branch,
                        course: course,
                        name: name,
                        classRoom: classRoom,
                        online: online,
                        feetype: 'retail',
                        description: description,
                        item: item,
                        effective: effective,
                        trainingType:trainingType,
                        partialPayment:partialPayment,
                        regularFee:regularFee
                    };
                }

                var action = '';
                var ajaxurl = '';
                var params = '';
                var type = '';
                var id = $('#feeTypeId').val();
                if (id == 0) {
                    action = 'add';
                    ajaxurl = API_URL + 'index.php/FeeStructure/addFeeStructure';
                    params = data;
                    type = "POST";
                } else {
                    action = 'edit';
                    ajaxurl = API_URL + 'index.php/FeeStructure/updateFeeStructure/' + id;
                    params = data;
                    type = "POST";
                }
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                var Id = $('#userId').val();
                var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: Id};
                var res = commonAjaxCall({
                    This: this,
                    asyncType: true,
                    method: type,
                    requestUrl: ajaxurl,
                    params: params,
                    headerParams: headerParams,
                    action: action,
                    onSuccess: feeCollectionResponse
                });
            }
        } else {
             if (errorFor == 'item') {
                alertify.dismissAll();
                notify('Select Fee Head Item', 'error', 10);
                errorFor = '';
            } else {
                alertify.dismissAll();
                notify('Validation Error', 'error', 10);
            }
        }
    }

    function copyFeeStructure(This){
        var errorFor = '';
        $('#feeStructure span.required-msg').remove();
        $('#feeStructure input').removeClass("required");

        var item = getItemDetails();
        var selMulti = $.map($("#branch option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {
                return $(el).val();
            }
        });
        var branch = selMulti.join(",");
        if($('#branch > option').length-1 == selMulti.length){
            var branch=0;
        }
        var flag = 0;
        var id = $('#feeId').val();
        var course = $('#course').val();
        var name = $('#name').val();
        var effective = $("#effective").val();
        var classRoom = $('#class-room').is(":checked") ? '1' : '0';
        var online = $('#onlineC').is(":checked") ? '1' : '0';
        var partialPayment = $('#partial-payment').is(":checked") ? 1 : 0;
        var regularFee = $('#regular-fee').is(":checked") ? 1 : 0;
        var trainingType=$("#Trainingtype").val();
        var feetype = $('#Feetype option:selected').text().toLowerCase();
        var companyName = $('#corporate').val();
        var groupName = $('#groupvalue').val();

        var description = $('#decription').val();

        if (name == '')
        {
            $("#name").addClass("required");
            $("#name").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (effective == '')
        {
            $("#effective").addClass("required");
            $("#effective").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (branch === '')
        {
            $("#branch").parent().find('.select-dropdown').addClass("required");
            $("#branch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
            branch = 0;
        }
        if (course == '' || course == null)
        {
            $("#course").parent().find('.select-dropdown').addClass("required");
            $("#course").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (feetype == '--select--' || feetype == null)
        {
            $("#Feetype").parent().find('.select-dropdown').addClass("required");
            $("#Feetype").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (trainingType == '--select--' || trainingType == null || trainingType == '')
        {
            $("#Trainingtype").parent().find('.select-dropdown').addClass("required");
            $("#Trainingtype").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (feetype == 'group') {
            if (groupName == '' || groupName == null)
            {
                $("#groupvalue").parent().find('.select-dropdown').addClass("required");
                $("#groupvalue").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;
            }
        } else if (feetype == 'corporate') {
            if (companyName == '' || companyName == null)
            {
                $("#corporate").parent().find('.select-dropdown').addClass("required");
                $("#corporate").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;
            }
        }


        if (flag != 1) {
            
            if (item == 'blank') {
                alertify.dismissAll();
                notify('Due Days can\'t be left blank', 'error', 10);
                flag = 1;
            }else if (item == 'amtblank') {
                alertify.dismissAll();
                notify('Amount can\'t be left blank', 'error', 10);
                flag = 1;
            } else if (item == 'duplicate') {
                alertify.dismissAll();
                notify('Due Date can\'t be duplicate', 'error', 10);
                flag = 1;
            } else if (item == 'noamt') {
                alertify.dismissAll();
                notify('Amount can\'t be blank', 'error', 10);
                flag = 1;
            } else if (item == 'zero') {
                alertify.dismissAll();
                notify('Amount can\'t be zero', 'error', 10);
                flag = 1;
            } else {
                if (feetype == 'group') {
                    var data = {
                        branch: branch,
                        course: course,
                        name: name,
                        classRoom: classRoom,
                        online: online,
                        feetype: feetype,
                        groupname: groupName,
                        description: description,
                        item: item,
                        effective: effective,
                        trainingType:trainingType,
                        partialPayment:partialPayment,
                        regularFee:regularFee
                    };
                } else if (feetype == 'corporate') {
                    var data = {
                        branch: branch,
                        course: course,
                        name: name,
                        classRoom: classRoom,
                        online: online,
                        feetype: feetype,
                        companyName: companyName,
                        description: description,
                        item: item,
                        effective: effective,
                        trainingType:trainingType,
                        partialPayment:partialPayment,
                        regularFee:regularFee
                    };
                } else if (feetype == 'retail') {
                    var data = {
                        branch: branch,
                        course: course,
                        name: name,
                        classRoom: classRoom,
                        online: online,
                        feetype: feetype,
                        description: description,
                        item: item,
                        effective: effective,
                        trainingType:trainingType,
                        partialPayment:partialPayment,
                        regularFee:regularFee
                    };
                }

                var action = '';
                var ajaxurl = '';
                var params = '';
                var type = '';
                var id = $('#feeTypeId').val();

                action = 'edit';
                ajaxurl = API_URL + 'index.php/FeeStructure/copyFeeStructure/fee_id/' + id;
                params = data;
                type = "POST";
                
                var Id = $('#userId').val();
                var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: Id};
                var res = commonAjaxCall({
                    This: this,
                    asyncType: true,
                    method: type,
                    requestUrl: ajaxurl,
                    params: params,
                    headerParams: headerParams,
                    action: action,
                    onSuccess: feeCollectionResponse
                });
            }
            
        } else {
             if (errorFor == 'item') {
                alertify.dismissAll();
                notify('Select Fee Head Item', 'error', 10);
                errorFor = '';
            } else {
                alertify.dismissAll();
                notify('Validation Error', 'error', 10);
            }
        }
    }
    function feeCollectionResponse(response) {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false)
        {
            if (response.data.length > 0) {
                var id = '';
                notify('Validation Error', 'error', 10);
                for (var a in response.data) {
                    id = '#' + a;
                    if (a == 'course' || a == 'Feetype' || a == 'groupvalue' || a == 'corporate') {
                        $(id).parent().find('.select-dropdown').addClass("required");
                        $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                    } else if (a == 'forFeeHead') {
                        //
                    } else {
                        $(id).addClass("required");
                        $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                    }
                }
            } else {
                alertify.dismissAll();
                notify(response.message, 'error', 10);
            }
        } else {
            $('#feeTypeId').val('0');
            notify(response.message, 'success', 10);
            window.location.href = APP_REDIRECT + "feestructure";
        }
    }

function feeStructureCreateFeeTypeValue(This) {
    var id = $('#feeId').val();
    var Feetype = $('#Feetype').val();
    var flag = 0;

    $('.coldCall-addCompanyWrap span.required-msg').remove();
    $('.coldCall-addCompanyWrap input').removeClass("required");

    if (id != 'retail') {
        if (id == 'corporate') {
            var name = $('#addC_name').val();
            var paramName = 'Company';
        } else if (id == 'group') {
            var name = $('#addI_name').val();
            var paramName = 'Institution';
        } else {
            flag = 1;
        }

        if (name == '' && id == 'corporate') {
            $("#addC_name").addClass("required");
            $("#addC_name").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (!(/^([a-zA-Z ])/.test(name))) {
            $("#addC_name").addClass("required");
            $("#addC_name").after('<span class="required-msg">Accepts only alphabets</span>');
            flag = 1;
        }

        if (name == '' && id == 'group') {
            $("#addI_name").addClass("required");
            $("#addI_name").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (!(/^([a-zA-Z ])/.test(name))) {
            $("#addI_name").addClass("required");
            $("#addI_name").after('<span class="required-msg">Accepts only alphabets</span>');
            flag = 1;
        }

        if (flag == 0) {
            var userId = $('#userId').val();
            var ajaxurl = API_URL + "index.php/Referencevalues/getReferenceValuesByName";
            var params = {'name': paramName};
            var action = 'add';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            /*
            commonAjaxCall({This: This, asyncType: true,params:params, headerParams: headerParams, method: 'GET', requestUrl: ajaxurl, params: params, action: action, onSuccess: function (response) {
                if (response == -1 || response['status'] == false){

                } else {

                    var refId = response.data[0].reference_type_id;*/

                    var data = {
                        referenceType: paramName,
                        referenceName: name,
                        referenceDescription: '----'
                    };
                    params = data;
                    var url = API_URL + "index.php/Referencevalues/addReferenceValueWrapper";
                    var action = 'add';
                    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

                    commonAjaxCall({This: This, asyncType: true, headerParams: headerParams, method: 'POST', requestUrl: url, params: params, action: action, onSuccess: function (response) {
                        if (response == -1 || response['status'] == false)
                        {
                            alertify.dismissAll();
                            var textMsg = response.message;
                            for (var a in response.data) {
                                if (a == paramName && id == 'corporate') {
                                    $('#addC_name').addClass("required");
                                    $('#addC_name').after('<span class="required-msg">' + response.data[a] + '</span>');
                                } else if (a == paramName && id == 'group') {
                                    $('#addI_name').addClass("required");
                                    $('#addI_name').after('<span class="required-msg">' + response.data[a] + '</span>');
                                }
                            }
                        } else
                        {
                            if (id == 'corporate') {

                                commonAjaxCall
                                (
                                    {
                                        This: this,
                                        headerParams: headerParams,
                                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                                        params: {name: 'Company'},
                                        action: action,
                                        onSuccess: function (response) {
                                            var corporate = response;
                                            corporateDropDown(corporate);
                                        }
                                    }
                                )
                            }

                            if (id == 'group') {
                                commonAjaxCall
                                (
                                    {
                                        This: this,
                                        headerParams: headerParams,
                                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                                        params: {name: 'Institution'},
                                        action: action,
                                        onSuccess: function (response) {
                                            var groupData = response;
                                            groupDropDown(groupData);
                                        }
                                    }
                                )
                            }
                            feeStructureHideFeeTypeValue();

                            alertify.dismissAll();
                            notify('Data Saved Successfully', 'success', 10);
                        }
                    }});
             /* }
            }});*/
        }else {
            alertify.dismissAll();
            notify('Error', 'error', 10);
        }
    }
}

function feeStructureHideFeeTypeValue() {
        $('#addC_name').val('');
        $('#addI_name').val('');

        $('#addC_name input').removeClass("required");
        $('#addI_name input').removeClass("required");
        $('.coldCall-addCompanyWrap label').removeClass("active");
        $('.coldCall-addCompanyWrap span.required-msg').remove();
        $('.coldCall-addCompanyWrap input').removeClass("required");
        $(".coldCall-addCompanyWrap").hide();
    }

    $('#Feetype').on('change', function () {
        feeStructureHideFeeTypeValue();
        $('.coldCall-addCompanyWrap span.required-msg').remove();
        $('.coldCall-addCompanyWrap input').removeClass("required");
//        var mystructureVal = $(this).val();
        var mystructureVal = $("#Feetype option:selected").text().toLowerCase();
        if (mystructureVal == 'group') {
            $('.feeStructureCreate-companyname').hide();
            $('.feeStructureCreate-groupname').slideDown();
            $('#feeId').val(mystructureVal);
        }
        if (mystructureVal == 'corporate') {
            $('.feeStructureCreate-groupname').hide();
            $('.feeStructureCreate-companyname').slideDown();
            $('#feeId').val(mystructureVal);
        }
        if (mystructureVal == 'retail') {
            $('.feeStructureCreate-groupname').hide();
            $('.feeStructureCreate-companyname').hide();
            //$('#feeTypeId').val(0);
            $('#feeId').val(mystructureVal);
        }

    });

    function changeFeeStructureStatus(This, id, currentStatus) {
        var status = currentStatus == 1 ? 'inactivate' : 'activate';
        var conf = "Are you sure want to " + status + " this fee ?";
        customConfirmAlert('Fee Structure Status' , conf);
        $("#popup_confirm #btnTrue").attr("onclick","changeFeeStructureStatusConfirm(this,'"+id+"')");
    }
    
    function changeFeeStructureStatusConfirm(This, id){
        var userId = $('#userId').val();
        var ajaxurl = API_URL + 'index.php/FeeStructure/changeStatus/fee_id/' + id;
        var params = {};
        var type = "DELETE";
        var headerParams = {action: 'delete', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'delete', onSuccess: deleteFeeStructureResponse});
    }

    function deleteFeeStructureResponse(response)
    {
        if (response == -1 || response['status'] == false)
        {
            alertify.dismissAll();
            notify(response.message, 'error', 10);
        } else
        {
            alertify.dismissAll();
            notify(response.message, 'success', 10);
            buildFeeStructuresDataTable();
        }
    }
    
    function getHistory(id) {
        var userId = $('#userId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/FeeStructure/getFeeStructureHistory/fee_id/' + id;
        commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function (response) {
                var dashboard = '';
                if (response == -1 || response['status'] == false) {
                    dashboard = '';
                } else {
                    var feeRawData = response.data;
                    for (var a in feeRawData) {
                        var textClass= '';
                        if(a > 0){
                            textClass = 'disabled-branchview';
                        }
                        var online = feeRawData[a].is_allowed_for_online_training == 1 ? '' : 'ash';
                        var onlineText = feeRawData[a].is_allowed_for_online_training == 1 ? '' : ' ';
                        var unversity = feeRawData[a].is_allowed_for_class_room_training == 1 ? '' : 'ash';
                        var unversityText = feeRawData[a].is_allowed_for_class_room_training == 1 ?  '' : ' ';
                        dashboard += '<tr class="'+textClass+'">';
                        dashboard += '<td><a href="javascript:;" class="tooltipped" data-position="top" data-tooltip="'+unversityText+'Class Room Training"><i class="fa fa-university mr5 ' + unversity + '"></i></a><a href="javascript:;" class="tooltipped" data-position="top" data-tooltip="'+onlineText+'Online Training" ><i class="fa fa-globe mr5 f13 ' + online + '"></i></a>'+feeRawData[a].courseName+'</td>';
                        dashboard += '<td>' + feeRawData[a].name + '</td>';
                        dashboard += '<td>' + feeRawData[a].effective_from + '</td>';
                        dashboard += '<td>' + feeRawData[a].trainingType + '</td>';
                        dashboard += '<td>' + feeRawData[a].feeType + '</td>';
                        dashboard += '<td>';
                        dashboard += feeRawData[a].organisationName != null ? feeRawData[a].organisationName : ' -- '
                        dashboard += '</td>';
                        dashboard += '<td>';
                        dashboard += feeRawData[a].is_regular_fee != null ? feeRawData[a].is_regular_fee : ' -- '
                        dashboard += '</td>';
                        dashboard += '<td>Rs ' + moneyFormat(feeRawData[a].fees) + '</td>';
                        dashboard += '<td>';
                        dashboard += feeRawData[a].branchName===null?'All':feeRawData[a].branchName;
                        dashboard += '</td>';
                        dashboard += '<td>';
                        dashboard += feeRawData[a].created_date;
                        dashboard += '</td>';
                        dashboard += '<td>';
                        dashboard += feeRawData[a].end_date;
                        dashboard += '</td>';
                        dashboard += '</tr>';
                    }
                }
                $('#historyDetails').html(dashboard);
                $('#feeStructureModal').modal('show');
                buildpopover();
            }});
}

    $('#branch').change(function(){
        //var branchId = $(this).val();

        var selMulti = $.map($("#branch option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {
                return $(el).val();
            }
        });
        var branchId = selMulti.join(",");
        var userID = $('#userId').val();
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        //if(branchId.trim().length > 0){
            var ajaxurl = API_URL + 'index.php/Courses/getMultiBranchCommonCoursesList';
            var params = {branchId:branchId};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
                selectOption = 1;
                branchCourse(response);
                selectOption = 0;
            }});
            /*} else {
            var ajaxurl = API_URL + 'index.php/Courses/getAllCourseList';
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
                selectOption = 1;
                branchCourse(response);
                selectOption = 0;
            }});
        }*/
    });
/*Fee Stuture Ends*/

/*Lead Transfer Start*/
function saveLeadTransfer(This){
    $('#ManagefeetransferForm span.required-msg').remove();
    $('#ManagefeetransferForm input,#ManagefeetransferForm .select-dropdown, .materialize-textarea').removeClass("required");

    var txtleadNumber = $('#txtleadNumber').val();
    var txtBranch = $('#txtUserBranch').val();
    var txtDescription = $('#txttextarea1').val();
    var error = false;
    if (txtleadNumber == "") {
        $("#txtleadNumber").addClass('required');
        $("#txtleadNumber").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        error = true;
    } else if(!(/^[0-9]+$/.test(txtleadNumber))){
        $("#txtleadNumber").addClass("required");
        $("#txtleadNumber").after('<span class="required-msg">Must contain only Numbers</span>');
        error = true;
    }

    if (txtBranch === null || txtBranch == '') {
        $("#txtUserBranch").parent().find('.select-dropdown').addClass("required");
        $("#txtUserBranch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        error = true;
    }

    if (txtDescription == "") {
        $("#txttextarea1").addClass('required');
        $("#txttextarea1").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        error = true;
    } else if(!(/^[a-zA-Z0-9 ,.-]+$/.test(txtDescription))){
        $("#txttextarea1").addClass("required");
        $("#txttextarea1").after('<span class="required-msg">Must be alphanumeric</span>');
        error = true;
    }

    if(!error){
        var conf = "Are you sure want to transfer lead ?";
        customConfirmAlert('Branch Status' , conf);
        $("#popup_confirm #btnTrue").attr("onclick","saveLeadTransferConfirm('"+txtleadNumber+"', '"+txtBranch+"', '"+txtDescription+"')");
    } else {
        alertify.dismissAll();
        notify('Validation Error', 'error', 10);
    }
}

function saveLeadTransferConfirm(txtleadNumber, txtBranch, txtDescription){
    notify('Processing..', 'warning', 10);
    var UserId = $('#hdnUserId').val();
    var oldBranchId=$('#userBranchList').val();
    var ajaxurl = API_URL + 'index.php/LeadTransfer/leadTransfer';
    var params = {'lead_id': txtleadNumber, 'branch': txtBranch, 'desc': txtDescription, 'oldBranch': oldBranchId};
    var type = "POST";
    $action = 'add';
    var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
    commonAjaxCall({This: this, params:params, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: $action, onSuccess: function(response){
        alertify.dismissAll();
        if (response == -1 || response['status'] == false)
        {
            var id = '';
            notify('Validation Error', 'error', 10);
            for (var a in response.data) {
                id = '#' + a;
                if (a == 'txtUserBranch') {
                    $('#txtUserBranch').parent().find('.select-dropdown').addClass("required");
                    $('#txtUserBranch').parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                } else if (a == 'txtleadNumber') {
                    $('#txtleadNumber').addClass("required");
                    $('#txtleadNumber').after('<span class="required-msg">' + response.data[a] + '</span>');
                } else if (a == 'course') {
                    alertify.dismissAll();
                    notify(response.message, 'error', 10);
                }
            }
        } else {
            notify(response.message, 'success', 10);
            buildLeadTransferListDataTable();
            closeLeadTransfer();
        }
    }});
}

function ManageLeadTransfer(This, id){
    $('#ManagefeetransferForm span.required-msg').remove();
    $('#ManagefeetransferForm input,#ManagefeetransferForm .select-dropdown, .materialize-textarea').removeClass("required");
    $('#ManagefeetransferForm label').removeClass("active");
    $('#leadTructurescreen').html('');
    $('#txtleadNumber').val('');
    $('.modal-footer').hide();
    $('#fee-transfer-list-modal').modal('show');
}

function closeLeadTransfer(){
    $('#ManagefeetransferForm span.required-msg').remove();
    $('#ManagefeetransferForm input,#ManagefeetransferForm .select-dropdown, .materialize-textarea').removeClass("required");
    $('#ManagefeetransferForm label').removeClass("active");
    $('#leadTructurescreen').html('');
    $('#txtleadNumber').val('');
    $('.modal-footer').hide();
    $('#fee-transfer-list-modal').modal('toggle');
}
/*End Lead Transfer*/

function clearDatePicker(id){
    if(typeof id === 'undefined'){
        var $input = $('.datepicker').pickadate()
    } else if(typeof id !== 'undefined'){
        var $input = $(id).pickadate()
    }
    // Use the picker object directly.
    var picker = $input.pickadate('picker');
    picker.clear();
}

    function AddUserType(This) {
        $('#countryForm span.required-msg').remove();
        $('#countryForm input').removeClass("required");
        $('#countryForm textarea').removeClass("required");
        var id = $('#reference_type_val_id').val();


        var url = '';
        var action = 'add';
        var name = $('#UserTypeName').val();
        var description = $('#UserTypeDescription').val();
        var data = {
            UserTypeName: name,
            UserTypeDescription: description
        };

        if (id != '')
        {
            $("#countryModalLabel").html("Edit User Type");
            action = 'manage';
            url = API_URL + "index.php/UserType/editUserType/user_type_id/" + id;
        } else {
            action = 'add';
            $("#countryModalLabel").html("Create User Type");
            url = API_URL + "index.php/UserType/addUserType";
        }

        var flag = 0;
        if (name == '') {
            $("#UserTypeName").addClass("required");
            $("#UserTypeName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (!(/^([a-zA-Z ])/.test(name)))
        {
            $("#UserTypeName").addClass("required");
            $("#UserTypeName").after('<span class="required-msg">Invalid Name</span>');
            flag = 1;
        }
        if ($.trim(description) == '') {
            $("#UserTypeDescription").addClass("required");
            $("#UserTypeDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }/* else if (((/^[ A-Za-z0-9_@./#&$%*!+-]*$/).test(description)) == false)
        {
            $("#UserTypeDescription").addClass("required");
            $("#UserTypeDescription").after('<span class="required-msg">Invalid Description</span>');
            flag = 1;
        }*/

        if (flag == 0) {
            var ajaxurl = url;
            var params = data;

            var userId = $('#userId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                    if (response == -1 || response['status'] == false)
                    {
                        var id = '';
                        alertify.dismissAll();
                        notify('Validation Error<br/>Do check the responsepective fields.', 'error', 10);
                        for (var a in response.data) {
                            id = '#' + a;
                            $(id).addClass("required");
                            $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                        }
                        $('#countryModal').removeAttr('disabled');
                    } else
                    {
                        userTypeManagementPageLoad();
                        $('#countryForm')[0].reset();

                        $('#countryModal').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();

                        alertify.dismissAll();
                        notify('Data Saved Successfully', 'success', 10);
                    }
                }});

        } else {
            return false;
        }
    }

    function getUserTypeDetails(This, id) {

        $('#countryForm label').addClass("active");
        $('#countryForm input').removeClass("required");
        $('#countryForm textarea').removeClass("required");
        $("#countryModalLabel").html("Edit User Type");


        $("#countryModalLabel").html("Edit User Type");


        var ajaxurl = API_URL + "index.php/UserType/getUserTypeById";

        var params = {user_type_id: id};

        var userId = $('#userId').val();
        var action = 'manage';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var response = commonAjaxCall({This: This, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: 'manage', onSuccess: function (response) {
                if (response == -1 || response['status'] == false)
                {
                    //
                } else
                {
                    alertify.dismissAll();
                    $('#UserTypeDescription').val(response.data.typeDescription);
                    $('#UserTypeName').val(response.data.typeName);
                    $('#reference_type_val_id').val(response.data.typeId);
                    $('#countryModal').modal('toggle');
                }
            }
        });
    }

    function closeUserTypeModal(This) {
        $('#countryForm')[0].reset();

        $('#reference_type_val_id').val('');

        $('#countryForm span.required-msg').remove();
        $('#countryForm input').removeClass("required");
        $('#countryForm textarea').removeClass("required");

        $('#countryModal').modal('toggle');

        $("#countryModalLabel").html("Create User Type");
    }

    /*For Tags*/
    function AddTags(This) {
        $('#tagForm span.required-msg').remove();
        $('#tagForm input').removeClass("required");
        $('#tagForm textarea').removeClass("required");
        var id = $('#reference_type_val_id').val();

        var url = '';
        var action = 'add';
        var name = $('#tagName').val();
        var description = $('#tagDescription').val();
        var data = '';
        if (id != '')
        {
            $("#tagModalLabel").html("Edit Tag");
            data = {
                tagName: name,
                tagDescription: description,
                tagId: id
            };
            action = 'edit';
            url = API_URL + "index.php/Tag/editTag";
        } else {
            action = 'add';
            $("#tagModalLabel").html("Create Tag");
            data = {
                tagName: name,
                tagDescription: description
            };
            url = API_URL + "index.php/Tag/addTag";
        }

        var flag = 0;
        if (name == '') {
            $("#tagName").addClass("required");
            $("#tagName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (!(/^([a-zA-Z ])/.test(name)))
        {
            $("#tagName").addClass("required");
            $("#tagName").after('<span class="required-msg">Invalid Name</span>');
            flag = 1;
        }

        if ($.trim(description) == '') {
            $("#tagDescription").addClass("required");
            $("#tagDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } /*else if ((/^[ A-Za-z0-9_@./#&$%*!+-]*$/).test(description) == false)
        {
            $("#tagDescription").addClass("required");
            $("#tagDescription").after('<span class="required-msg">Invalid Description</span>');
            flag = 1;
        }*/

        if (flag == 0) {
            notify('Processing..', 'warning', 10);
            var ajaxurl = url;
            var params = data;

            var userId = $('#userId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                    alertify.dismissAll();
                    if (response == -1 || response['status'] == false)
                    {
                        var id = '';
                        //notify(response['message'], 'error', 10);
                        for (var a in response.data) {
                            if (a == 'reference_type_val') {
                                id = '#tagName';
                            } else {
                                id = '#' + a;
                            }
                            $(id).addClass("required");
                            $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                        }
                        $('#tagModal').removeAttr('disabled');
                    } else
                    {
                        //tagManagementPageLoad();
                        buildTagsReferenceDataTable();
                        $('#tagForm')[0].reset();

                        $('#tagModal').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();

                        notify('Data Saved Successfully', 'success', 10);
                    }
                }});

        } else {
            return false;
        }
    }

    function getTagDetails(This, id) {

        $('#tagForm')[0].reset();
        $('#tagForm input').removeClass("required");
        $('#tagForm textarea').removeClass("required");
        $('#tagForm label').removeClass("active");

        if (id == '' || id == 0) {
            $("#tagModalLabel").html("Create Tag");
            $("#reference_type_val_id").val(id);
            $('#tagModal').modal('toggle');
        } else {
            notify('Processing..', 'warning', 10);
            $("#tagModalLabel").html("Edit Tag");
            var userId = $('#userId').val();
            var ajaxurl = API_URL + "index.php/Tag/getTagById/tag_id/" + id;
            var params = {};
            var action = 'list';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: function (response) {
                    if (response == -1 || response['status'] == false)
                    {
                    } else
                    {
                        alertify.dismissAll();
                        $('#tagDescription').val(response.data[0].description);
                        $('#tagName').val(response.data[0].value);
                        $('#reference_type_val_id').val(response.data[0].id);
                        $('#tagDescription').next('label').addClass('active');
                        $('#tagName').next('label').addClass('active');
                        $('#tagModal').modal('toggle');
                    }
                }
            });
        }
    }

    function closeTagsModal(This) {
        $('#tagForm')[0].reset();

        $('#reference_type_val_id').val('');

        $('#tagForm span.required-msg').remove();
        $('#tagForm input').removeClass("required");
        $('#tagForm textarea').removeClass("required");

        $('#tagModal').modal('toggle');

        $("#tagModalLabel").html("Create Tag");
    }

    /**/

// courses methods //
    function TotalBranches(This, userID) {

        var ajaxurl = API_URL + 'index.php/Branch/getBranchList';
        var params = {};
        //var response=commonAjaxCall(This,'GET',ajaxurl,params);
        var headerParams = {context: 'default', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: TotalBranchesResponse});

        // return response;
    }
    /* created by : V Parameshwar, 
     * Dt: 08-03-2016
     * purpose: For select all and de select all option for multi select dropdown 
     * */
    function multiSelectDropdown(This) {
        var SelectBox = '#' + $(This).attr('id');


        $(SelectBox + ' > :selected').each(function () {
            if ($(this).text() == '--Select All--') {

                $(SelectBox + ' option').prop('selected', true);
                $(this).text('--De Select All--');
                $(this).prop('selected', false);
                $(SelectBox).material_select();
            } else if ($(this).text() == '--De Select All--') {
                $(SelectBox + ' option').prop('selected', false);
                $(this).text('--Select All--');
                $(this).prop('selected', false);
                $(SelectBox).material_select();

            }
        });
        var TotalCnt = $(SelectBox + ' option').size() - 1;
        var SelCnt = $(SelectBox + ' > :selected').length;
        $(SelectBox + ' > option').each(function () {

            if (SelCnt < TotalCnt && SelCnt > 0) {
                if ($(this).text() == '--De Select All--')
                {
                    $(this).text('--Select All--');
                    $(SelectBox).material_select();
                }

            }
            if (SelCnt == TotalCnt) {

                if ($(this).text() == '--Select All--')
                {
                    $(this).text('--De Select All--');
                    $(SelectBox).material_select();
                }
            }
            if (SelCnt == 0) {
                if ($(this).text() == '--De Select All--')
                {
                    $(this).text('--Select All--');
                    $(SelectBox).material_select();
                }

            }

        });
    }
    $('#txtCourseBranches').change(function () {
        multiSelectDropdown($(this));

    });
    function TotalBranchesResponse(response) {

        var totalBranchesResponse = response;
        $('#txtCourseBranches').empty();
        $('#txtCourseBranches').append($('<option></option>').val('').html('--Select All--'));
        if (totalBranchesResponse == -1 || totalBranchesResponse.status == false)
        {
//            notify('Something went wrong', 'error', 10);
        } else
        {
            var totalBranchesData = totalBranchesResponse.data;
            //$('#txtCourseBranches').append($('<option disabled selected></option>').val('').html('--Select--'));
            $.each(totalBranchesData, function (key, value) {
                $('#txtCourseBranches').append($('<option></option>').val(value.id).html(value.name));

            });
        }
            $('#txtCourseBranches').material_select();

    }
    function ManageCourse(This, Id) {

        $('#ManageCourseForm')[0].reset();
        $('#txtCourseBranches').empty();
        $('#txtCourseBranches').material_select();
        $('#ManageCourseForm label').removeClass("active");
        $('#ManageCourseForm span.required-msg').remove();
        $('#ManageCourseForm input').removeClass("required");
        $('#ManageCourseForm select').removeClass("required");
        $('#ManageCourseForm textarea').removeClass("required");
        $('#txtCourseStatusValue').val(1);
        var userID = $("#hdnUserId").val();
        //TotalBranches(This, userID);

        if (Id == 0)
        {
            TotalBranches(This, userID);
            $("#myModalLabel").html("Create Course");
            $("#btnSaveCourse").val("Add");
            $("#btnSaveCourse").html("<i class=\"icon-right mr8\"></i>Add");
        } else
        {
            alertify.dismissAll();
            notify('Processing..', 'warning', 10);
            var headerParams1 = {context: 'default', Authorizationtoken: $accessToken, user: userID};
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            $.when(
                commonAjaxCall
                (
                    {
                        This: This,
                        headerParams: headerParams1,
                        requestUrl: API_URL + 'index.php/Branch/getBranchList',
                        action: 'edit'
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: This,
                        headerParams: headerParams,
                        params: {courseId: Id},
                        requestUrl: API_URL + 'index.php/Courses/getSingleCourse',
                        action: 'edit'
                    }
                )
            ).then(function (response1, response2) {
                TotalBranchesResponse(response1[0]);
                ManageCourseResponse(response2[0]);
                $("#myModalLabel").html("Edit Course");
                $("#btnSaveCourse").val("Add");
                $("#btnSaveCourse").html("<i class=\"icon-right mr8\"></i>Update");
            });

           /* var ajaxurl = API_URL + 'index.php/Courses/getSingleCourse';
            var params = {courseId: Id};
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: ManageCourseResponse});*/
        }

        $("#hdnCourseId").val(Id);
        alertify.dismissAll();
        $(This).attr("data-target", "#myCourse");
        $(This).attr("data-toggle", "modal");
    }
    $('#txtCourseStatus').change(function () {
        if ($('#txtCourseStatus').is(':checked')) {
            $('#txtCourseStatusValue').val(1);
        } else {
            $('#txtCourseStatusValue').val(0);
        }
    });
    function ManageCourseResponse(response) {
        var CourseDetails = response;
        if (CourseDetails == -1 || CourseDetails['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var coursesData = CourseDetails.data[0];

            $("#txtCourseName").val(coursesData.name);
            $("#txtCourseName").next('label').addClass("active");

            var sel_branches = [];
            $.each(coursesData.branches, function (i, item) {
                sel_branches.push(item.fk_branch_id);
            });
            $('#txtCourseBranches').val(sel_branches);
            $('#txtCourseBranches').material_select();
            $('#txtCourseBranches').trigger('change');
            $("#txtCourseBranches").next('label').addClass("active");
            if (coursesData.is_active == 1) {
                if ($('#txtCourseStatus').length) {
                    $('#txtCourseStatus').prop('checked', true);
                }
                $('#txtCourseStatusValue').val(1);
            } else {
                if ($('#txtCourseStatus').length) {
                    $('#txtCourseStatus').prop('checked', false);
                }
                $('#txtCourseStatusValue').val(0);
            }
            $("#txtCourseDescription").val(coursesData.description);
            $("#txtCourseDescription").next('label').addClass("active");
        }
    }

    function saveCourse(This) {
        var courseName = $("#txtCourseName").val();
        var courseId = $("#hdnCourseId").val();
        var courseDescription = $("#txtCourseDescription").val();
        var courseStatus = $('#txtCourseStatusValue').val();
        var userID = $("#hdnUserId").val();
        /*if($("#txtCourseStatus").prop("checked") == true){
         courseStatus=1;
         }
         else if($("#txtCourseStatus").prop("checked") == false){
         courseStatus=0;
         }*/
        var selMulti = $.map($("#txtCourseBranches option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'All' && $(el).val().trim() != 'default') {
                return $(el).val();
            }
        });
        var courseBranch = selMulti.join(",");
        var regx_txtCourseName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

        $('#ManageCourseForm span.required-msg').remove();
        $('#ManageCourseForm input').removeClass("required");
        $('#ManageCourseForm textarea').removeClass("required");
        var flag = 0;
        if (courseName == '')
        {
            $("#txtCourseName").addClass("required");
            $("#txtCourseName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtCourseName.test($('#txtCourseName').val()) === false) {
            $("#txtCourseName").addClass("required");
            $("#txtCourseName").after('<span class="required-msg">Invalid Name</span>');
            flag = 1;
        }
        /*if (courseBranch == '')
        {

            $("#txtCourseBranches").parent().find('.select-dropdown').addClass("required");
            $("#txtCourseBranches").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }*/
        if (courseDescription == '')
        {
            $("#txtCourseDescription").addClass("required");
            $("#txtCourseDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (flag == 1) {
            return false;
        } else {
            if (courseId == 0) {
                var ajaxurl = API_URL + 'index.php/Courses/addCourse';
                var params = {userID: userID, courseName: courseName, courseBranches: courseBranch, courseStatus: courseStatus, courseDescription: courseDescription};
                var type = "POST";
                var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveCourseResponse});
            } else {
                var ajaxurl = API_URL + 'index.php/Courses/updateCourse';
                var params = {userID: userID, courseId: courseId, courseName: courseName, courseBranches: courseBranch, courseStatus: courseStatus, courseDescription: courseDescription};
                var type = "POST";
                var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: SaveCourseResponse});
            }

        }
    }
    function SaveCourseResponse(response) {
        var response = response;
        if (response == -1 || response.status == false)
        {
            $.each(response.data, function (i, item) {
                alertify.dismissAll();
                notify(item, 'error', 10);
                if (response.data['courseName'])
                {
                    alertify.dismissAll();
                    $("#txtCourseName").addClass("required");
                    $("#txtCourseName").after('<span class="required-msg">' + response.data['courseName'] + '</span>');
                }
                return false;
            });
        } else
        {
            alertify.dismissAll();
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            //renderCoursesData();
            buildCoursesDataTable();
            $('#myCourse').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }
    }
    function DeleteCourse(This, CourseID, currentStatus) {
        var status = currentStatus == 1 ? 'inactivate' : 'activate';
        var conf = "Are you sure want to " + status + " this course ?";
        customConfirmAlert('Course Status', conf);
        $("#popup_confirm #btnTrue").attr("onclick","ConfirmDeleteCourse(this,'"+CourseID+"','"+currentStatus+"')");

    }
    function ConfirmDeleteCourse(This, CourseID, status) {
        notify('Processing..', 'warning', 10);
        var userID = $("#hdnUserId").val();


            var ajaxurl = API_URL + 'index.php/Courses/deleteCourse/courseId/' + CourseID + '/userID/' + userID +'/status/'+ status;
            var params = {};
            var type = "DELETE";
            var headerParams = {action: 'delete', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'delete', onSuccess: DeleteCourseResponse});



    }
    function DeleteCourseResponse(response) {
        $('#popup_confirm').modal('hide');
        alertify.dismissAll();
        if (response == -1 || response.status == false)
        {
            $.each(response.data, function (i, item) {
                notify(item, 'error', 10);
                return false;
            });
        } else
        {
            notify(response.message, 'success', 10);
            //renderCoursesData();
            buildCoursesDataTable();
            $('#myCourse').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    }

    function saveDuplicateContacts(This) {
        var contactName = '';
        var contactEmail = '';
        var contactPhone = '';
        var contactCityCoutry = '';
        var contactCity = '';
        var contactCountry = '';
        var contactTags = '';
        var contactSource = '';
        var userID = $("#hdnUserId").val();
        if ($('#txtEmail').val())
            contactEmail = $('#txtEmail').val();
        if ($('input[name=contactName]:checked').val())
            contactName = $('input[name=contactName]:checked').val();
        if ($('input[name=contactPhone]:checked').val())
            contactPhone = $('input[name=contactPhone]:checked').val();
        if ($('input[name=contactCity]:checked').val()) {
            contactCityCoutry = $('input[name=contactCity]:checked').val();
            var splitCityCountry = contactCityCoutry.split("-");
            if (splitCityCountry[0]) {
                contactCity = splitCityCountry[0];
            }
            if (splitCityCountry[1]) {
                contactCountry = splitCityCountry[1];
            }
        }
        var selMulti = [];
        $('input[name=contactTag]:checked').each(function () {
            selMulti.push(this.value);
        });
        var contactTags = selMulti.join(",");
        if ($('input[name=contactSource]:checked').val())
            contactSource = $('input[name=contactSource]:checked').val();
        var action = 'merge';
        var ajaxurl = API_URL + 'index.php/Lead/mergeContact';
        var params = {userID: userID, contactName: contactName, contactEmail: contactEmail, contactPhone: contactPhone, contactCity: contactCity, contactCountry: contactCountry, contactTags: contactTags, contactSource: contactSource};

        var type = "POST";
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: saveDuplicateContactsResponse});

    }
    function saveDuplicateContactsResponse(response) {
        alertify.dismissAll();
        if (response.status === false) {
            notify(response.message, 'error', 10);
        } else {
            notify(response.message, 'success', 10);
            buildContactsDataTable();
            getLeadCountsData();
            $(this).attr("data-dismiss", "modal");
            $('#mergeDuplicateContacts').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    }
    function populateContactDuplicates(This, Id) {

        $('#formMergeDuplicateContacts')[0].reset();


        var userID = $("#hdnUserId").val();
        var ajaxurl = API_URL + 'index.php/Lead/getDuplicateContacts/contactId/' + Id;
        var params = {contactId: Id};
        var action = 'merge';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: fillContactDuplicatesResponse});
        $(This).attr("data-target", "#mergeDuplicateContacts");
        $(This).attr("data-toggle", "modal");

    }
    function fillContactDuplicatesResponse(response) {
        var ContactDuplicateDetails = response;
        if (ContactDuplicateDetails == -1 || ContactDuplicateDetails['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            $('#txtEmail').val(ContactDuplicateDetails.data.emailId);
            $('#txtEmail').next('label').addClass('active');
            var Html = '';
            if (ContactDuplicateDetails.data.duplicates.name) {
                Html += "<div class=\"multiple-radio\">";
                Html += "<label>Name</label>";
                var nameCnt = 0;
                $.each(ContactDuplicateDetails.data.duplicates.name, function (i, item) {
                    nameCnt = nameCnt + 1;
                    Html += "<span class=\"inline-radio\">";
                    Html += "<input class=\"with-gap\" name=\"contactName\" type=\"radio\" id=\"txtName" + nameCnt + "\" value='" + item + "' checked  />";
                    Html += "<label for=\"txtName" + nameCnt + "\">" + item + "</label>";
                    Html += "</span>";
                });
                Html += "</div>";
            }
            if (ContactDuplicateDetails.data.duplicates.phone) {
                Html += "<div class=\"multiple-radio\">";
                Html += "<label>Phone</label>";

                var phoneCnt = 0;
                $.each(ContactDuplicateDetails.data.duplicates.phone, function (i, item) {
                    phoneCnt = phoneCnt + 1;
                    Html += "<span class=\"inline-radio\">";
                    Html += "<input class=\"with-gap\" name=\"contactPhone\" type=\"radio\" id=\"txtPhone" + phoneCnt + "\"  value='" + item + "' checked  />";
                    Html += "<label for=\"txtPhone" + phoneCnt + "\">" + item + "</label>";
                    Html += "</span>";
                });
                Html += "</div>";
            }
            if (ContactDuplicateDetails.data.duplicates.city) {
                Html += "<div class=\"multiple-radio\">";
                Html += "<label>City</label>";
                var cityCnt = 0;
                $.each(ContactDuplicateDetails.data.duplicates.city, function (i, item) {
                    cityCnt = cityCnt + 1;
                    Html += "<span class=\"inline-radio\">";
                    Html += "<input class=\"with-gap\" name=\"contactCity\" type=\"radio\" id=\"txtCity" + cityCnt + "\" checked value='" + item + "'  />";
                    Html += "<label for=\"txtCity" + cityCnt + "\">" + item + "</label>";
                    Html += "</span>";
                });
                Html += "</div>";
            }
            if (ContactDuplicateDetails.data.duplicates.tags) {
                Html += "<div class=\"multiple-checkbox\">";
                Html += "<label>Tag</label>";

                var tagCnt = 0;
                $.each(ContactDuplicateDetails.data.duplicates.tags, function (i, item) {
                    tagCnt = tagCnt + 1;
                    Html += "<span class=\"inline-checkbox\"><input class=\"filled-in\" name=\"contactTag\" type=\"checkbox\" id=\"txtTag" + tagCnt + "\" checked value='" + item + "'  />";
                    Html += "<label for=\"txtTag" + tagCnt + "\">" + item + "</label></span>";
                });
                Html += "</div>";
            }
            if (ContactDuplicateDetails.data.duplicates.source) {
                Html += "<div class=\"multiple-radio\">";
                Html += "<label>Source</label>";
                var sourceCnt = 0;
                $.each(ContactDuplicateDetails.data.duplicates.source, function (i, item) {
                    sourceCnt = sourceCnt + 1;
                    Html += "<span class=\"inline-radio\">";
                    Html += "<input class=\"with-gap\" name=\"contactSource\" type=\"radio\" id=\"txtSource" + sourceCnt + "\" checked value='" + item + "'  />";
                    Html += "<label for=\"txtSource" + sourceCnt + "\">" + item + "</label>";
                    Html += "</span>";
                });
                Html += "</div>";
            }

            $('#fillDuplicatesContent').html(Html);





        }
    }
//contact management related functions //
    function ManageContact(This, Id) {
        notify('Processing..', 'warning', 10);
        $('#ManageConcatForm')[0].reset();
        $('#txtContactCity').empty();
        $('#txtContactCity').append($('<option selected></option>').val('').html('--Select--'));
        $('#txtContactCity').material_select();
        $('#txtContactCountry').empty();
        $('#txtContactCountry').append($('<option selected></option>').val('').html('--Select--'));
        $('#txtContactCountry').material_select();
        $('#txtContactTag').empty();
        //$('#txtContactTag').material_select();
        $('#ManageConcatForm label').removeClass("active");
        $('#ManageConcatForm span.required-msg').remove();
        $('#ManageConcatForm input').removeClass("required");
        $('#ManageConcatForm select').removeClass("required");
        $("#ManageConcatForm #txtContactSource").parent().find('div.chosen-container > a.chosen-single').removeClass("required");
        $('#ContactTagWrapper').slideUp();
        $('#ContactSourceWrapper').slideUp();
        /*if ($('#ContactTagWrapper').css('display') == 'block') {
            $('.showAddCompanyWrap-btn').click();
        }*/

        var userID = $("#hdnUserId").val();
        $("#hdnContactId").val(Id);
        var ajaxurl = API_URL + 'index.php/Lead/getContactConfigurations';
        var params = {};
        var headerParams = {context: 'default', Authorizationtoken: $accessToken, user: userID};
        $.when(
            commonAjaxCall({
                This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add'
            }),
            commonAjaxCall({
                This: this, requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName', params: {name: 'Contact Source'}, headerParams: headerParams, action: 'add'
            })).then(function (resp1,resp2){

            ManageContactResponse(resp1[0]);
            getContactSourcesResponse(resp2[0]);

        if (Id == 0) {
            //create
            $("#myModalLabel").html("Create Contact");
            $("#btnSaveContact").val("Add");
            $("#btnSaveContact").html("<i class=\"icon-right mr8\"></i>Add");
            alertify.dismissAll();
            //$(This).attr("data-target", "#createContact");
            //$(This).attr("data-toggle", "modal");
        } else {
            //edit
            //setTimeout(function () {
                $("#myModalLabel").html("Edit Contact");
                $("#btnSaveContact").val("Update");
                $("#btnSaveContact").html("<i class=\"icon-right mr8\"></i>Update");
                var ajaxurl = API_URL + 'index.php/Lead/getContacts/contactId/' + Id;
                var params = {contactId: Id};
                var headerParams = {
                    action: 'edit',
                    context: $context,
                    serviceurl: $pageUrl,
                    pageurl: $pageUrl,
                    Authorizationtoken: $accessToken,
                    user: userID
                };
                $.when(
                    commonAjaxCall({
                        This: This,
                        requestUrl: ajaxurl,
                        params: params,
                        headerParams: headerParams,
                        action: 'edit'
                    })).then(function (resp){
                    fillContactDetailsResponse(resp);
                    alertify.dismissAll();
                    //}, 100);

                });
            }
            $('#createContact').modal('show');
        });
    }
    function fillContactDetailsResponse(response) {

        var ContactDetails = response;
        if (ContactDetails == -1 || ContactDetails['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var contactData = ContactDetails.data.contacts[0];
            if (contactData.name) {
                $("#txtContactName").val(contactData.name);
                $("#txtContactName").next('label').addClass("active");
            }
            if (contactData.email) {
                $("#txtContactEmail").val(contactData.email);
                $("#txtContactEmail").next('label').addClass("active");
            }
            if (contactData.phone) {
                $("#txtContactPhone").val(contactData.phone);
                $("#txtContactPhone").next('label').addClass("active");
            }
            if (contactData.countryId) {
                $('#txtContactCountry').val(contactData.countryId);
                $('#txtContactCountry').material_select();
                $("#txtContactCountry").next('label').addClass("active");
                var userId = $('#hdnUserId').val();
                var ajaxurl = API_URL + 'index.php/Lead/getContactCities';
                var params = {countryId: contactData.countryId};
                var action = 'list';
                var headerParams = {action: action, context: 'default', serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
                var response =
                    $.when(
                        commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action})
                    ).then(function (response){
                        getContactCitiesList(response);
                        $('#txtContactCity').val(contactData.cityId);
                        $('#txtContactCity').material_select();
                        if (contactData.cityId) {
                            $("#txtContactCity").next('label').addClass("active");
                        }
                    });
            }
            var sel_tags = [];
            $.each(contactData.tags, function (i, item) {
                sel_tags.push(item.id);
            });
            if (sel_tags.length > 0)
            {
                $('#txtContactTag').val(sel_tags);
                $('#txtContactTag').trigger("chosen:updated");
                $("#txtContactTag").next('label').addClass("active");
            }
            if (contactData.fk_source_id)
            {
                $('#txtContactSource').val(contactData.fk_source_id);
                $('#txtContactSource').trigger("chosen:updated");
                $("#txtContactSource").next('label').addClass("active");
            }

        }
    }
    function ManageContactResponse(response) {
        $('#txtContactCountry').empty();
        $('#txtContactCity').empty();
        $('#txtContactCity').append($('<option selected></option>').val('').html('--Select--'));
        $('#txtContactCountry').append($('<option selected></option>').val('').html('--Select--'));
        if (response.data.countries.length > 0) {

            $.each(response.data.countries, function (key, value) {

                $('#txtContactCountry').append($('<option></option>').val(value.Id1).html(value.Country));
            });
        }
        $('#txtContactCountry').material_select();
        $('#txtContactCity').material_select();
        $('#txtContactTag').append($('<option></option>').val('').html('--Select All--'));
        if (response.data.tags.length > 0) {
            $.each(response.data.tags, function (key, value) {
                $('#txtContactTag').append($('<option></option>').val(value.id).html(value.tagName));
            });
        }
        $("#txtContactTag").chosen({
            no_results_text: "Oops, nothing found!",
            width: "95%"
        });
        $('#txtContactTag').trigger("chosen:updated");
    }
    $('#txtContactCountry').change(function () {
        getCitiesListByCountry($(this).val())
    });
    function getCitiesListByCountry(countryId)
    {
        var userId = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Lead/getContactCities';
        var params = {countryId: countryId};
        var action = 'list';
        var headerParams = {action: action, context: 'default', serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        if(countryId == 0)
            commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: getCitiesListResponse});
        else
            commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: getContactCitiesList});
    }
    function getCitiesListResponse(response)
    {
        $('#ddlContactCity').empty();
        $('#ddlContactCity').append($('<option></option>').val('').html('--Select All--'));
        if (response.data.data.length > 0)
        {
            $.each(response.data.data, function (key, value) {
                $('#ddlContactCity').append($('<option></option>').val(value.id).html(value.value));
            });
        }
        $("#ddlContactCity").chosen({
            no_results_text: "Oops, nothing found!",
            width: "95%"
        });
        $('#ddlContactCity').trigger("chosen:updated");
        $("#ddlContactCity").next('label').addClass("active");
    }

    function getContactCitiesList(response)
    {
        $('#txtContactCity').empty();
        $('#txtContactCity').append($('<option selected></option>').val('').html('--Select--'));
        if (response.data.data.length > 0)
        {
            $.each(response.data.data, function (key, value) {
                $('#txtContactCity').append($('<option></option>').val(value.id).html(value.value));
            });
        }
        $('#txtContactCity').material_select();
    }
    $('#ddlContactCity,#txtContactTag,#ddlBatchesList').change(function () {
        multiSelectChosenDropdown($(this));
    });
    function saveContact(This) {
        var contactId = $("#hdnContactId").val();
        var contactName = $("#txtContactName").val();
        var contactEmail = $("#txtContactEmail").val();
        var contactPhone = $("#txtContactPhone").val();
        var userID = $("#hdnUserId").val();
        var contactCountryText = $("#txtContactCountry option:selected").text();
        var contactCityText = $("#txtContactCity option:selected").text();
        var contactSourceText = $("#txtContactSource option:selected").text();
        var selMulti = $.map($("#txtContactCountry option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {

                return $(el).val();
            }
        });
        var contactCountry = selMulti.join(",");
        var selMulti = $.map($("#txtContactCity option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {
                return $(el).val();
            }
        });
        var contactCity = selMulti.join(",");
        var selMulti = $.map($("#txtContactTag option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).val();
            }
        });
        var contactTags = selMulti.join(",");

        var selMulti = $.map($("#txtContactTag option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).text();
            }
        });
        var contactTagsText = selMulti.join(",");
        var selMulti = $.map($("#txtContactSource option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).val();
            }
        });
        var contactSourse = selMulti.join(",");
        var regx_txtContactName = /^[a-zA-Z][ A-Za-z0-9_@./#&+-]*$/;
        var regx_txtContactEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var regx_txtContactPhone = /^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/;

        $('#ManageConcatForm span.required-msg').remove();
        $('#ManageConcatForm input').removeClass("required");
        var flag = 0;
        if (contactName == '')
        {
            $("#txtContactName").addClass("required");
            $("#txtContactName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtContactName.test($('#txtContactName').val()) === false) {
            $("#txtContactName").addClass("required");
            $("#txtContactName").after('<span class="required-msg">Accepts only alphanumeric with spaces</span>');
            flag = 1;
        }
        if (contactEmail == '' && contactPhone == '')
        {
            $("#txtContactEmail").addClass("required");
            $("#txtContactEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (contactEmail!='' && regx_txtContactEmail.test($('#txtContactEmail').val()) === false) {
            $("#txtContactEmail").addClass("required");
            $("#txtContactEmail").after('<span class="required-msg">Invalid E-mail</span>');
            flag = 1;
        }
        if (contactPhone == '' && contactEmail == '')
        {
            $("#txtContactPhone").addClass("required");
            $("#txtContactPhone").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (contactPhone != ''){
            if (contactPhone.length < 8) {
                $("#txtContactPhone").addClass("required");
                $("#txtContactPhone").after('<span class="required-msg">'+$phoneMinLengthMessage+'</span>');
                flag = 1;
            }else if (contactPhone.length > 15) {
                $("#txtContactPhone").addClass("required");
                $("#txtContactPhone").after('<span class="required-msg">Invalid Phone Number</span>');
                flag = 1;
            }else if (contactPhone!='' && regx_txtContactPhone.test($('#txtContactPhone').val()) === false) {
                $("#txtContactPhone").addClass("required");
                $("#txtContactPhone").after('<span class="required-msg">Invalid Phone Number</span>');
                flag = 1;
            }
        }
        if (contactCountry == '')
        {
            $("#txtContactCountry").parent().find('.select-dropdown').addClass("required");
            $("#txtContactCountry").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (contactCity == '')
        {
            $("#txtContactCity").parent().find('.select-dropdown').addClass("required");
            $("#txtContactCity").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        /*if (contactTags == '')
        {
            $("#txtContactTag").parent().find('.select-dropdown').addClass("required");
            $("#txtContactTag").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }*/
        if (contactSourse == '')
        {
            //$("#txtContactSource").parent().find('.select-dropdown').addClass("required");
            $("#txtContactSource").parent().find('div.chosen-container > a.chosen-single').addClass("required");
            $("#txtContactSource").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (flag == 1) {
            return false;
        } else {
            notify('Processing..', 'warning', 10);
            if (contactId == 0) {
                var ajaxurl = API_URL + 'index.php/Lead/addContact';
                var params = {userID: userID, contactName: contactName, contactEmail: contactEmail, contactPhone: contactPhone, contactCity: contactCityText, contactCountry: contactCountryText, contactTags: contactTagsText, contactSource: contactSourceText};
                var type = "POST";
                var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: This, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveContactResponse});
            } else {
                var ajaxurl = API_URL + 'index.php/Lead/editContact';
                var params = {userID: userID, contactId: contactId, contactName: contactName, contactEmail: contactEmail, contactPhone: contactPhone, contactCity: contactCityText, contactCountry: contactCountryText, contactTags: contactTagsText, contactSource: contactSourceText};
                var type = "POST";
                var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: This, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: SaveContactResponse});
            }
        }
    }
    function SaveContactResponse(response) {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            $.each(response.data, function (i, item) {
                notify(item, 'error', 10);
                if (response.data['contactName'])
                {
                    $("#txtContactName").addClass("required");
                    $("#txtContactName").after('<span class="required-msg">' + response.data['contactName'] + '</span>');
                }
                if (response.data['contactEmail'])
                {
                    $("#txtContactEmail").addClass("required");
                    $("#txtContactEmail").after('<span class="required-msg">' + response.data['contactEmail'] + '</span>');
                }
                if (response.data['contactPhone'])
                {
                    $("#txtContactPhone").addClass("required");
                    $("#txtContactPhone").after('<span class="required-msg">' + response.data['contactPhone'] + '</span>');
                }
                if (response.data['contactCity'])
                {
                    $("#txtContactCity").addClass("required");
                    $("#txtContactCity").after('<span class="required-msg">' + response.data['contactCity'] + '</span>');
                }
                if (response.data['contactTags'])
                {
                    $("#txtContactTag").addClass("required");
                    $("#txtContactTag").after('<span class="required-msg">' + response.data['contactCity'] + '</span>');
                }
                return false;
            });
        } else
        {
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            buildContactsDataTable();
            getLeadCountsData();

            $('#createContact').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }
    }
    function viewContactDetails(This, id) {

        var userID = $("#hdnUserId").val();
        var ajaxurl = API_URL + 'index.php/Lead/getContacts/contactId/' + id;
        var params = {contactId: id};
        var headerParams = {action: 'view', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'view', onSuccess: viewContactDetailsResponse});
        $(This).attr("data-target", "#viewContact");
        $(This).attr("data-toggle", "modal");

    }
    function viewContactDetailsResponse(response) {
        $("#viewContactName").html("----");
        $("#viewContactEmail").html("----");
        $("#viewContactPhone").html("----");
        $("#viewContactCountry").html('----');
        $("#viewContactCity").html("----");
        $("#viewContactTags").html("----");
        $("#viewContactSource").html("----");
        if (response.status === true) {
            if (response.data.contacts.length > 0) {
                $.each(response.data.contacts, function (key, value) {
                    if (value.name)
                        $("#viewContactName").html(value.name);
                    if (value.email)
                        $("#viewContactEmail").html(value.email);
                    if (value.phone)
                        $("#viewContactPhone").html(value.phone);
                    if (value.city)
                        $("#viewContactCity").html(value.city);
                    if (value.country)
                        $("#viewContactCountry").html(value.country);
                    if (value.sourceName)
                        $("#viewContactSource").html(value.sourceName);
                    var selMultiTags = [];
                    $.each(value.tags, function (keyTag, valueTag) {
                        selMultiTags.push(valueTag.tagName);
                    });
                    var contactTags = selMultiTags.join();
                    $("#viewContactTags").html(contactTags);



                });
            } else {
                $("#viewContactName").html("----");
                $("#viewContactEmail").html("----");
                $("#viewContactPhone").html("----");
                $("#viewContactCountry").html('----');
                $("#viewContactCity").html("----");
                $("#viewContactTags").html("----");
                $("#viewContactSource").html("----");
            }
        } else {
            $("#viewContactName").html("----");
            $("#viewContactEmail").html("----");
            $("#viewContactPhone").html("----");
            $("#viewContactCountry").html('----');
            $("#viewContactCity").html("----");
            $("#viewContactTags").html("----");
            $("#viewContactSource").html("----");
            alertify.dismissAll();
            notify(response.data.message, 'error', 10);
        }
    }
    $('#txtContactTag').change(function () {

        if ($.inArray("All", $(this).val()) === 0) {
            $('#txtContactTag option').prop('selected', true);
            //$('#txtContactTag').material_select();
        }
    });

    function branchCountry(resp)
    {
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false)
        {
        } else
        {
            var countryRawData = resp.data;
            for (var b = 0; b < countryRawData.length; b++) {
                var o = $('<option/>', {value: countryRawData[b]['reference_type_value_id'], 'parent-id': countryRawData[b]['reference_type_value_id']})
                        .text(countryRawData[b]['value']);
                o.appendTo('#country');
            }
            $('#country').material_select();
        }
    }

    function branchCity(resp)
    {
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false)
        {
            var o = $('<option/>', {value: 'default', disabled: 'disabled', selected: 'selected'})
                    .text('---Select---');
            var ob = $('<option/>', {value: 'default', disabled: 'disabled', selected: 'selected'})
                    .text('---Select---');
            o.appendTo('#country');
            ob.appendTo('#primaryCity');
            $('#country').material_select();
            $('#primaryCity').material_select();
        } else
        {
            var branchRawData = resp.data;
            for (var b = 0; b < branchRawData.length; b++) {
                var o = $('<option/>', {value: branchRawData[b]['id'], 'parent-id': branchRawData[b]['id']})
                        .text(branchRawData[b]['name']);
                var ob = $('<option/>', {value: branchRawData[b]['id'], 'parent-id': branchRawData[b]['id']})
                        .text(branchRawData[b]['name']);
                o.appendTo('#country');
                ob.appendTo('#primaryCity');
            }
            $('#country').material_select();
            $('#primaryCity').material_select();
        }
    }

    function branchCourse(resp)
    {
        $('#course').find('option:gt(0)').remove();
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false)
        {
//            var o = $('<option/>', {value: 'default', disabled: 'disabled', selected: 'selected'})
//                    .text('---Select---');
//            o.appendTo('#course');
//            $('#course').material_select();
        } else
        {
            var branchRawData = resp.data;
            if (selectOption != 1) {
                $('#course').append($('<option></option>').val('all').html('--Select All--'));
            }
            selectOption = 0;
            for (var b = 0; b < branchRawData.length; b++) {
                var o = $('<option/>', {value: branchRawData[b]['courseId']})
                        .text(branchRawData[b]['name']);
                o.appendTo('#course');
            }
        }
            $('#course').material_select();
        }

    function buildSubjectDropdown(resp){
        $('#subject').find('option:gt(0)').remove();
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false) {
        } else {
            var RawData = resp.data;
            for (var b = 0; b < RawData.length; b++) {
                var o = $('<option/>', {value: RawData[b]['subject_id']})
                        .text(RawData[b]['name']);
                o.appendTo('#subject');
            }
        }
        $('#subject').material_select();
    }
    
    function buildVenueDropdown(resp){
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false){
        } else {
            var RawData = resp.data;
            $('#venue').find('option:gt(0)').remove();
            for (var b = 0; b < RawData.length; b++) {
                var o = $('<option/>', {value: RawData[b]['venue_id']})
                        .text(RawData[b]['name']+' - '+RawData[b]['city']);
                o.appendTo('#venue');
            }
            $('#venue').material_select();
        }
    }
    
    function buildFacultyDropdown(resp){
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false){
        } else {
            var RawData = resp.data;
            $('#faculty').find('option:gt(0)').remove();
            for (var b = 0; b < RawData.length; b++) {
                var o = $('<option/>', {value: RawData[b]['faculty_id']})
                        .text(RawData[b]['name']);
                o.appendTo('#faculty');
            }
            $('#faculty').material_select();
        }
    }
    
    function branchUserList(response)
    {
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {

        } else
        {
            $('#branchHead').find('option:gt(0)').remove();
            var branchRawData = response.data;
            for (var b = 0; b < branchRawData.length; b++) {
                if (branchRawData[b]['is_super_admin'] == '1')
                {
                    var o = $('<option/>', {value: branchRawData[b]['user_id'], selected: 'selected'})
                            .text(branchRawData[b]['name']+' ('+branchRawData[b]['employee_code']+')');
                    o.appendTo('#branchHead');
                } else
                {
                    var o = $('<option/>', {value: branchRawData[b]['user_id']})
                            .text(branchRawData[b]['name']+' ('+branchRawData[b]['employee_code']+')');
                    o.appendTo('#branchHead');
                }

            }
            $('#branchHead').material_select();
        }
    }
    function resetTagWrapper(This) {
        $('#ContactTagWrapper span.required-msg').remove();
        $('#ContactTagWrapper label').removeClass("active");
        $('#ContactTagWrapper input').removeClass("required");
        $('#txtContactTagWrapper').val('');

    }
    function addTagWrapper(This)
    {
        $('#ContactTagWrapper span.required-msg').remove();
        //$('#ContactTagWrapper label').removeClass("active");
        //$('#ContactTagWrapper input').removeClass("required");
        var url = '';
        var action = 'add';
        var name = $('#txtContactTagWrapper').val();
        var description = '----';
        var data = '';

        action = 'add';

        data = {
            tagName: name,
            tagDescription: description
        };
        url = API_URL + "index.php/Tag/addTag";
        var regx_txtTagWrapper = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

        var flag = 0;
        if (name == '') {
            $("#txtContactTagWrapper").addClass("required");
            $("#txtContactTagWrapper").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if ((regx_txtTagWrapper.test(name)) === false)
        {
            $("#txtContactTagWrapper").addClass("required");
            $("#txtContactTagWrapper").after('<span class="required-msg">Invalid Tag Name.</span>');
            flag = 1;
        }

        if (flag == 0) {
            var ajaxurl = url;
            var params = data;

            var userId = $('#hdnUserId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                    if (response == -1 || response['status'] == false)
                    {

                        var id = '';
                        alertify.dismissAll();
                        notify(response['message'], 'error', 10);
                        for (var a in response.data) {
                            if (a == 'tagName') {
                                id = '#txtContactTagWrapper';
                                $(id).addClass("required");
                                $(id).next('label').addClass("active");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else {
                                id = '#' + a;
                            }

                        }

                    } else
                    {
                        alertify.dismissAll();
                        notify('Tag Saved Successfully', 'success', 10);
                        $('#txtContactTagWrapper').val('');
                        $('#ContactTagWrapper').slideUp();
                        $('#ContactTagWrapper label').removeClass("active");
                        $('#ContactTagWrapper input').removeClass("required");
                        $('#ContactTagWrapper span.required-msg').remove();
                        getTagsDropdown(This);
                    }
                }});

        } else {
            return false;
        }
    }
    function getTagsDropdown(This) {


        var ajaxurl = API_URL + "index.php/Tag/getTags";
        var params = '';

        var userId = $('#hdnUserId').val();
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var response = commonAjaxCall({This: This, headerParams: headerParams, method: 'GET', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                if (response == -1 || response.status == false)
                {
                    alertify.dismissAll();
                    notify(response['message'], 'error', 10);
                } else
                {
                    if (response.data.length > 0) {

                        $.each(response.data, function (key, value) {

                            if ($("#txtContactTag option[value=" + value.id + "]").length == 0) {
                                $('#txtContactTag').append($('<option></option>').val(value.id).html(value.tagName));
                            }

                        });
                    }
                   // $('#txtContactTag').material_select();
                    $('#txtContactTag').trigger("chosen:updated");
                    $("#txtContactTag").chosen({
                        no_results_text: "Oops, nothing found!",
                        width: "95%"
                    });
                }
            }});
    }

    /* Company Start */

    function ManageCompany(This, Id)
    {
        $('#ManageCompanyForm')[0].reset();
        $('#ManageCompanyForm label').removeClass("active");
        $('#ManageCompanyForm span.required-msg').remove();
        $('#ManageCompanyForm input,#ManageCompanyForm textarea').removeClass("required");
        $('#hdnCompanyStatusValue').val(1);
        var userID = $('#hdnUserId').val();
        if (Id == 0)
        {
            $("#companyButton").html("<i class='icon-right mr8'></i>Add");
            $("#myModalLabel").html("Create Company");
        } else
        {  $("#companyButton").html("<i class='icon-right mr8'></i>Update");
            $("#myModalLabel").html("Edit Company");
            var ajaxurl = API_URL + 'index.php/Company/getSingleCompany';
            var params = {companyID: Id};
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: GetCompanyresponse});
        }
        $("#hdnCompanyId").val(Id);
        $(This).attr("data-target", "#myCompany");
        $(This).attr("data-toggle", "modal");
    }


    $('#txtCompanyStatus').change(function ()
    {
        if ($('#txtCompanyStatus').is(':checked'))
        {
            $('#hdnCompanyStatusValue').val(1);
        } else
        {
            $('#hdnCompanyStatusValue').val(0);
        }
    });


    function GetCompanyresponse(response)
    {
        if (response == -1 || response['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var companyData = response['data'];
            $("#txtCompanyName").val(companyData[0]['name']);
            $("#txtCompanyName").next('label').addClass("active");
            $("#txtCompanyCode").val(companyData[0]['code']);
            $("#txtCompanyCode").next('label').addClass("active");
            $("#txtVoucherCode").val(companyData[0]['voucher_code']);
            $("#txtVoucherCode").next('label').addClass("active");
            if (companyData[0]['status'] == 1) {
                if ($('#txtCompanyStatus').length) {
                    $('#txtCompanyStatus').prop('checked', true);
                }
                $('#hdnCompanyStatusValue').val(1);
            } else {
                if ($('#txtCompanyStatus').length) {
                    $('#txtCompanyStatus').prop('checked', false);
                }
                $('#hdnCompanyStatusValue').val(0);
            }
            $("#txtCompanyDes").val(companyData[0]['description']);
            if (companyData[0]['description'] != null)
            {
                $('textarea#txtCompanyDes').next('label').addClass("active");
            }
        }
    }
    function SaveCompany(This) {

        var Name = $("#txtCompanyName").val();
        var company_code = $("#txtCompanyCode").val();
        var voucher_code = $("#txtVoucherCode").val();
        var Description = $("#txtCompanyDes").val();
        var hdnCompanyId = $("#hdnCompanyId").val();
        var Status = $('#hdnCompanyStatusValue').val();
        var userID = $("#hdnUserId").val();
        var RegexName = /^(\w+\s)*\w+$/;
        var regx_company_code = $rgx_allow_alpha_numeric_space;

        $('#ManageCompanyForm span.required-msg').remove();
        $('#ManageCompanyForm input').removeClass("required");
        $('#ManageCompanyForm textarea').removeClass("required");
        var flag = 0;

        if (Name == '')
        {
            $("#txtCompanyName").addClass("required");
            $("#txtCompanyName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (RegexName.test(Name) === false)
        {
            $("#txtCompanyName").addClass("required");
            $("#txtCompanyName").after('<span class="required-msg">Accepts only alphanumeric with spaces</span>');
            flag = 1;
        }
        if (company_code == '')
        {
            $("#txtCompanyCode").addClass("required");
            $("#txtCompanyCode").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_company_code.test(company_code) === false)
        {
            $("#txtCompanyCode").addClass("required");
            $("#txtCompanyCode").after('<span class="required-msg">'+$rgx_allow_alpha_numeric_space_message+'</span>');
            flag = 1;
        } else if (company_code.length < 3 || company_code.length > 30)
        {
            $("#txtCompanyCode").addClass("required");
            $("#txtCompanyCode").after('<span class="required-msg">Should be greater than two digits and less than 30 digits</span>');
            flag = 1;
        }
        if (voucher_code == '')
        {
            $("#txtVoucherCode").addClass("required");
            $("#txtVoucherCode").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_company_code.test(voucher_code) === false)
        {
            $("#txtVoucherCode").addClass("required");
            $("#txtVoucherCode").after('<span class="required-msg">'+$rgx_allow_alpha_numeric_space_message+'</span>');
            flag = 1;
        } else if (voucher_code.length < 2 || voucher_code.length > 30)
        {
            $("#txtVoucherCode").addClass("required");
            $("#txtVoucherCode").after('<span class="required-msg">Should be greater than two and less than 30 characters</span>');
            flag = 1;
        }
        if (flag == 0)
        {
            var ajaxurl;
            var params;
            if (hdnCompanyId == 0)
            {
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                var ajaxurl = API_URL + 'index.php/Company/addCompany';
                var params = {UserID: userID, Name: Name, companyCode: company_code,voucherCode: voucher_code, Description: Description, status: Status};
                var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveCompanyResponse});
            } else
            {
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                var ajaxurl = API_URL + 'index.php/Company/updateCompany';
                var params = {CompanyID: hdnCompanyId, UserID: userID, Name: Name, companyCode: company_code,voucherCode: voucher_code, Description: Description, status: Status};
                var headerParams = {action: 'Edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: SaveCompanyResponse});
            }
        } else
        {
            return false;
        }
    }
    function SaveCompanyResponse(response) {
        alertify.dismissAll();
        if (response.status == true)
        {
            notify(response.message, 'success', 10);
            $('#myCompany').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildCompanyListDataTable();
        } else
        {
            var id = '', flag = 0;
            notify(response.message, 'error', 10);
            for (var a in response.data)
            {
                id = '#' + a;
                if (a == 'name')
                {
                    $("#txtCompanyName").addClass("required");
                    $("#txtCompanyName").after('<span class="required-msg">' + response.data[a] + '</span>');
                }
                else if (a == 'code')
                {
                    $("#txtCompanyCode").addClass("required");
                    $("#txtCompanyCode").after('<span class="required-msg">' + response.data[a] + '</span>');
                }
                else if (a == 'voucher_code')
                {
                    $("#txtVoucherCode").addClass("required");
                    $("#txtVoucherCode").after('<span class="required-msg">' + response.data[a] + '</span>');
                }
                else
                {
                    $(id).addClass("required");
                    $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                }
            }
        }
    }
    function DeleteCompany(This, id)
    {
        customConfirmAlert('Delete Company', 'Are you sure want to delete this company ?');
        $("#popup_confirm #btnTrue").attr("onclick","DeletingCompany(this,"+id+")");
    }
    function DeletingCompany(This,id){
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var CompanyID = id;
        var userID = $("#hdnUserId").val();
        var ajaxurl = API_URL + 'index.php/Company/deleteCompany';
        var params = {companyId: CompanyID};

        var headerParams = {action: 'delete', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'delete', onSuccess: DeleteCompanyResponse});

    }
    function DeleteCompanyResponse(response) {
        alertify.dismissAll();
        if (response.status === true)
        {
            notify(response.message, 'success', 10);
            buildCompanyListDataTable();

        } else
        {
            alertify.dismissAll();
            notify(response.message, 'error', 10);
        }
    }
    /* Company End */
    /* Company Bank Account Start */

    function AllCompanies(This, userID)
    {
        var ajaxurl = API_URL + 'index.php/CompanyAccountDetails/getAllCompanies';
        var params = {};
        var headerParams = {context: 'default', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: AllCompaniesResponse});
    }
    function AllCompaniesResponse(response)
    {
        $('#ddlCompanies').append($('<option></option>').val('').html('--Select--'));
        var totalCompanyResponse = response;
        if (totalCompanyResponse == -1 || totalCompanyResponse.status == false)
        {
            notify('No companies found', 'error', 10);
        } else
        {
            var totalCompanyData = totalCompanyResponse.data;
            $.each(totalCompanyData, function (key, value)
            {
                $('#ddlCompanies').append($('<option></option>').val(value.company_id).html(value.name));
            });
            $('#ddlCompanies').material_select();
        }
        $('#ddlCompanies').material_select();
    }
    function ManageCompanyAccounts(This, Id)
    {
        $('#ManageCompanyBankDetailsForm')[0].reset();
        $('#ManageCompanyBankDetailsForm label').removeClass("active");
        $('#ManageCompanyBankDetailsForm span.required-msg').remove();
        $('#ManageCompanyBankDetailsForm input').removeClass("required");
        $('#ManageCompanyBankDetailsForm select').removeClass("required");
        $('#ddlCompanies').empty();
        $('#ddlCompanies').material_select();
        $('#hdnBanksStatusValue').val(1);
        var userID = $('#hdnUserId').val();
        AllCompanies(This, userID);
        if (Id == 0)
        {
            $("#ddlCompanies").attr("disabled",false);
            $("#companyDetailsButton").html("<i class='icon-right mr8'></i>Add");
            $("#myModalLabel").html("Create Account Details");
        } else
        {
            $("#ddlCompanies").attr("disabled",true);
            $("#companyDetailsButton").html("<i class='icon-right mr8'></i>Update");
            $("#myModalLabel").html("Edit Account Details");
            var ajaxurl = API_URL + 'index.php/CompanyAccountDetails/getSingleCompanyDetails';
            var params = {companyID: Id};
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: getCompanyDetailsresponse});
        }
        $("#hdnCompanyDetailsId").val(Id);
        $(This).attr("data-target", "#myCompanyDetails");
        $(This).attr("data-toggle", "modal");
    }

    $('#txtBanksStatus').change(function ()
    {
        if ($('#txtBanksStatus').is(':checked'))
        {
            $('#hdnBanksStatusValue').val(1);
        } else
        {
            $('#hdnBanksStatusValue').val(0);
        }
    });
    function getCompanyDetailsresponse(response)
    {
        if (response == -1 || response['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var companyData = response['data'];
            $('#ddlCompanies').val(companyData[0]['fk_company_id']);
            $('#ddlCompanies').material_select();
            $("#ddlCompanies").next('label').addClass("active");
            $("#txtAccountNumber").val(companyData[0]['acc_no']);
            $("#txtAccountNumber").next('label').addClass("active");
            if (companyData[0]['status'] == 1)
            {
                if ($('#txtBanksStatus').length)
                {
                    $('#txtBanksStatus').prop('checked', true);
                }
                $('#hdnBanksStatusValue').val(1);
            } else
            {
                if ($('#txtBanksStatus').length)
                {
                    $('#txtBanksStatus').prop('checked', false);
                }
                $('#hdnBanksStatusValue').val(0);
            }
            $("#txtBankName").val(companyData[0]['bank_name']);
            $("#txtBankName").next('label').addClass("active");

            $("#txtIFSC").val(companyData[0]['bank_ifsc']);
            $("#txtIFSC").next('label').addClass("active");

            $("#txtBranchName").val(companyData[0]['bank_branch']);
            $("#txtBranchName").next('label').addClass("active");

            $("#txtCity").val(companyData[0]['bank_city']);
            $("#txtCity").next('label').addClass("active");
        }
    }
    function SaveCompanyDetails(This) {

        var company_name = $("#ddlCompanies").val();
        var account_number = $("#txtAccountNumber").val();
        var bank_name = $("#txtBankName").val();
        var IFSC = $("#txtIFSC").val();
        var branch_name = $("#txtBranchName").val();
        var city = $("#txtCity").val();
        var Status = $('#hdnBanksStatusValue').val();
        var hdnCompanyDetailsId = $("#hdnCompanyDetailsId").val();
        var userID = $("#hdnUserId").val();
        var regx_company_code = /^[0-9]+$/;
        var RegexName = /^(\w+\s)*\w+$/;
        var RegexAlphaNumeric = /^[a-zA-Z0-9]+$/;

        $('#ManageCompanyBankDetailsForm span.required-msg').remove();
        $('#ManageCompanyBankDetailsForm input').removeClass("required");
        $('#ManageCompanyBankDetailsForm textarea').removeClass("required");

        var flag = 0;

        if (company_name == '')
        {
            $("#ddlCompanies").parent().find('.select-dropdown').addClass("required");
            $("#ddlCompanies").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (account_number == '')
        {
            $("#txtAccountNumber").addClass("required");
            $("#txtAccountNumber").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_company_code.test(account_number) === false)
        {
            $("#txtAccountNumber").addClass("required");
            $("#txtAccountNumber").after('<span class="required-msg">Accepts only numbers(0-9)</span>');
            flag = 1;
        } else if (account_number.length < 5 || account_number.length > 30)
        {
            $("#txtAccountNumber").addClass("required");
            $("#txtAccountNumber").after('<span class="required-msg">Should be > 5 digits and < 30 digits</span>');
            flag = 1;
        }

        if (bank_name == '')
        {
            $("#txtBankName").addClass("required");
            $("#txtBankName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (RegexName.test(bank_name) == false)
        {
            $("#txtBankName").addClass("required");
            $("#txtBankName").after('<span class="required-msg"> Accepts only alphanumeric with spaces </span>');
            flag = 1;
        }
        /*if (IFSC == '')
        {
            $("#txtIFSC").addClass("required");
            $("#txtIFSC").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (RegexAlphaNumeric.test(IFSC) == false)
        {
            $("#txtIFSC").addClass("required");
            $("#txtIFSC").after('<span class="required-msg">Accepts only alphanumeric</span>');
            flag = 1;
        }*/
        if (branch_name == '')
        {
            $("#txtBranchName").addClass("required");
            $("#txtBranchName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (RegexName.test(branch_name) == false)
        {
            $("#txtBranchName").addClass("required");
            $("#txtBranchName").after('<span class="required-msg">Accepts only alphanumeric with spaces </span>');
            flag = 1;
        }
        if (city == '')
        {
            $("#txtCity").addClass("required");
            $("#txtCity").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (RegexName.test(city) == false)
        {
            $("#txtCity").addClass("required");
            $("#txtCity").after('<span class="required-msg">Accepts only alphanumeric with spaces</span>');
            flag = 1;
        }

        if (flag == 0)
        {
            var ajaxurl;
            var params;
            if (hdnCompanyDetailsId == 0)
            {
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                var ajaxurl = API_URL + 'index.php/CompanyAccountDetails/AddCompanyAccountDetails';
                var params = {UserID: userID, companyName: company_name, accountNumber: account_number, bankName: bank_name, Ifsc: IFSC, branchName: branch_name, City: city, status: Status};
                var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveCompanyDetailsResponse});
            } else
            {
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                var ajaxurl = API_URL + 'index.php/CompanyAccountDetails/updateCompanyAccountDetails';
                var params = {CompanyID: hdnCompanyDetailsId, UserID: userID, companyName: company_name, accountNumber: account_number, bankName: bank_name, Ifsc: IFSC, branchName: branch_name, City: city, status: Status};
                var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: SaveCompanyDetailsResponse});
            }
        } else
        {
            return false;
        }


    }
    function SaveCompanyDetailsResponse(response) {
        alertify.dismissAll();
        if (response.status == true)
        {
            notify(response.message, 'success', 10);
            $('#myCompanyDetails').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildCompanyAccountsListDataTable();

        } else
        {
            var id = '', flag = 0;
            notify(response.message, 'error', 10);
            for (var a in response.data)
            {
                id = '#' + a;
                if (a == 'name') {
                    $("#txtAccountNumber").addClass("required");
                    $("#txtAccountNumber").after('<span class="required-msg">' + response.data[a] + '</span>');
                }
                else
                {
                    $(id).addClass("required");
                    $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                }
                if (a == 'ifsc')
                {
                    $("#txtIFSC").addClass("required");
                    $("#txtIFSC").after('<span class="required-msg">' + response.data[a] + '</span>');
                }
                else
                {
                    $(id).addClass("required");
                    $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                }
            }

        }
    }
    function DeleteCompanyDetails(This, id) {
        customConfirmAlert('Delete Account Details', 'Are you sure want to delete this company bank account details ?');
        $("#popup_confirm #btnTrue").attr("onclick","deletingAccountDetails(this,"+id+")");

    }
    function deletingAccountDetails(This,id){
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var CompanyID = id;
        var userID = $("#hdnUserId").val();
        var ajaxurl = API_URL + 'index.php/CompanyAccountDetails/deleteCompanyAccountDetails';
        var params = {companyId: CompanyID};
        var headerParams = {action: 'delete', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'delete', onSuccess: DeleteCompanyDetailsResponse});

    }
    function DeleteCompanyDetailsResponse(response) {
        alertify.dismissAll();
        if (response.status === true)
        {
            notify(response.message, 'success', 10);
            buildCompanyAccountsListDataTable();
        } else
        {
            notify(response.message, 'error', 10);
        }
    }
    /* Company Bank Account End */
    /* Fee Head Start */

    function ManageFeeHead(This, Id)
    {
        $('#ManageFeeHeadForm')[0].reset();
        $('#ManageFeeHeadForm label').removeClass("active");
        $('#ManageFeeHeadForm span.required-msg').remove();
        $('#ManageFeeHeadForm input').removeClass("required");
        $('#ManageFeeHeadForm select').removeClass("required");
        $('#ddlCompanies').empty();
        $('#ddlCompanies').material_select();

        $('#txtFreeHeadStatusValue').val(1);
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/CompanyAccountDetails/getAllCompanies';
        var action = 'add';
        var headerParams = 'feehead';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: function(response){
            AllCompaniesResponse(response);
            if (Id == 0)
            {
                $("#SaveButton").html("<i class='icon-right mr8'></i>Add");
                $("#myModalLabel").html("Create FeeHead");
            } else
            {
                $("#SaveButton").html("<i class='icon-right mr8'></i>Update");
                $("#myModalLabel").html("Edit FeeHead");
                var ajaxurl = API_URL + 'index.php/FeeHead/getSingleFeeHead';
                var params = {FeeHead: Id};
                var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: getFeeHeadresponse});
            }
            $('#myFeeHead').modal('toggle');
        }});
        $("#hdnFeeHeadId").val(Id);
    }
    $('#chkFreeHeadStatus').change(function () {
        if ($('#chkFreeHeadStatus').is(':checked'))
        {
            $('#hdnFreeHeadStatusValue').val(1);
        } else
        {
            $('#hdnFreeHeadStatusValue').val(0);
        }
    });
    function getFeeHeadresponse(response)
    {
        if (response == -1 || response['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var feeHead = response['data'];
            $('#ddlCompanies').val(feeHead[0]['fk_company_id']);
            $('#ddlCompanies').material_select();
            $("#ddlCompanies").next('label').addClass("active");
            if (feeHead[0]['status'] == 1) {
                if ($('#chkFreeHeadStatus').length)
                {
                    $('#chkFreeHeadStatus').prop('checked', true);
                }
                $('#hdnFreeHeadStatusValue').val(1);
            } else
            {
                if ($('#chkFreeHeadStatus').length)
                {
                    $('#chkFreeHeadStatus').prop('checked', false);
                }
                $('#hdnFreeHeadStatusValue').val(0);
            }
            $("#txtFeeHeadName").val(feeHead[0]['name']);
            $("#txtFeeHeadName").next('label').addClass("active");

            $("#txaDescription").val(feeHead[0]['description']);
            $("#txaDescription").next('label').addClass("active");

        }
    }
    function SaveFeeHead(This)
    {
        var company_name = $("#ddlCompanies").val();
        var feeHeadname = $("#txtFeeHeadName").val();
        var description = $("#txaDescription").val();
        var hdnFeeHeadId = $("#hdnFeeHeadId").val();
        var feeHeadStatus = $('#hdnFreeHeadStatusValue').val();
        var userID = $("#hdnUserId").val();
        var RegexName = /^(\w+\s)*\w+$/;

        $('#ManageFeeHeadForm span.required-msg').remove();
        $('#ManageFeeHeadForm input').removeClass("required");
        $('#ManageFeeHeadForm textarea').removeClass("required");

        var flag = 0;

        if (company_name == '')
        {
            $("#ddlCompanies").parent().find('.select-dropdown').addClass("required");
            $("#ddlCompanies").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if (feeHeadname == '')
        {
            $("#txtFeeHeadName").addClass("required");
            $("#txtFeeHeadName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        /*else if (RegexName.test(feeHeadname) === false)
        {
            $("#txtFeeHeadName").addClass("required");
            $("#txtFeeHeadName").after('<span class="required-msg">Accepts only alphanumeric with spaces</span>');
            flag = 1;
        }*/


        if (flag == 0)
        {
            var ajaxurl = '', headerParams = '', params = '';
            if (hdnFeeHeadId == 0)
            {
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                ajaxurl = API_URL + 'index.php/FeeHead/AddFeeHead';
                params = {UserID: userID, name: feeHeadname, companyName: company_name, description: description, status: feeHeadStatus};
                headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveFeeHeadResponse});
            } else
            {
                alertify.dismissAll();
                notify('Processing..', 'warning', 50);
                ajaxurl = API_URL + 'index.php/FeeHead/updateFeeHead';
                params = {CompanyID: hdnFeeHeadId, UserID: userID, name: feeHeadname, companyName: company_name, description: description, status: feeHeadStatus};
                headerParams = {action: 'Edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: SaveFeeHeadResponse});
            }
        } else
        {
            return false;
        }


    }
    function SaveFeeHeadResponse(response) {
        alertify.dismissAll();
        if (response.status == true)
        {
            notify(response.message, 'success', 10);
            $('#myFeeHead').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildFeeHeadListDataTable();

        } else
        {
            var id = '', flag = 0;
            notify(response.message, 'error', 10);
            for (var a in response.data)
            {
                id = '#' + a;
                if (a == 'name')
                {
                    $("#txtFeeHeadName").addClass("required");
                    $("#txtFeeHeadName").after('<span class="required-msg">' + response.data[a] + '</span>');
                } else
                {
                    $(id).addClass("required");
                    $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                }
            }
        }
    }
    function DeleteFeeHead(This, id)
    {
        customConfirmAlert('Delete Fee Head', 'Are you sure want to delete this feed head ?');
        $("#popup_confirm #btnTrue").attr("onclick","DeletingFeeHead(this,"+id+")");
    }
    function DeletingFeeHead(This,id){
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var FeeHeadId = id;
        var userID = $("#hdnUserId").val();
        var ajaxurl = API_URL + 'index.php/FeeHead/deleteFeeHead';
        var params = {feeHeadId: FeeHeadId};
        var headerParams = {action: 'delete', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'delete', onSuccess: DeleteFeeHeadResponse});
    }
    function DeleteFeeHeadResponse(response)
    {
        alertify.dismissAll();
        if (response.status === true)
        {
            notify(response.message, 'success', 10);
            buildFeeHeadListDataTable();
        } else
        {
            alertify.dismissAll();
            notify(response.message, 'error', 10);
        }
    }
    /* Fee Head End */

//cold calling contact management related functions //
    function ManageColdCall(This, Id) {

        $('#ManageColdCallForm')[0].reset();
        //$("#ColdCallDescriptionWrapper").slideUp();
        $('#ManageColdCallForm select').material_select();
        $('#txtColdCallContactCity').empty();
        $('#txtColdCallContactCity').append($('<option selected></option>').val('').html('--Select--'));
        $('#txtColdCallContactCity').material_select();
        $('#txtColdCallContactCountry').empty();
        $('#txtColdCallContactCountry').append($('<option selected></option>').val('').html('--Select--'));
        $('#txtColdCallContactCountry').material_select();

        $('#ManageColdCallForm label').removeClass("active");
        $('#ManageColdCallForm span.required-msg').remove();
        $('#ManageColdCallForm input').removeClass("required");
        $('#ManageColdCallForm select').removeClass("required");
        $('#ComapnyWrapper').slideUp();
        $('#InstitutionWrapper').slideUp();
        var userID = $("#hdnUserId").val();
        $("#hdnLeadId").val(Id);
        var ajaxurl = API_URL + 'index.php/Lead/getContactConfigurations';
        var params = {};
        var action = 'status';
        var headerParams = {context: 'default', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: ManageColdCallContactResponse});
        if (Id == 0) {

        } else {
            //edit
            setTimeout(function () {

                var ajaxurl = API_URL + 'index.php/Lead/getContacts/contactId/' + Id;
                var params = {contactId: Id};
                var action = 'status';
                var headerParams = {
                    action: action,
                    context: $context,
                    serviceurl: $pageUrl,
                    pageurl: $pageUrl,
                    Authorizationtoken: $accessToken,
                    user: userID
                };
                commonAjaxCall({
                    This: This,
                    requestUrl: ajaxurl,
                    params: params,
                    headerParams: headerParams,
                    action: action,
                    onSuccess: fillColdCallDetailsResponse
                });

            }, 100);
            $(This).attr("data-target", "#coldCalling");
            $(This).attr("data-toggle", "modal");

        }

    }
    function fillColdCallDetailsResponse(response) {

        var ContactDetails = response;
        if (ContactDetails == -1 || ContactDetails['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var contactData = ContactDetails.data.contacts[0];

            if (contactData.name) {
                $("#txtColdCallContactName").val(contactData.name);
                $("#txtColdCallContactName").next('label').addClass("active");
            }

            if (contactData.email) {
                $("#txtColdCallContactEmail").val(contactData.email);
                $("#txtColdCallContactEmail").next('label').addClass("active");
            }

            if (contactData.phone) {
                $("#txtColdCallContactPhone").val(contactData.phone);
                $("#txtColdCallContactPhone").next('label').addClass("active");
            }

            if (contactData.alternate_mobile) {
                $("#txtColdCallContactAltPhone").val(contactData.alternate_mobile);
                $("#txtColdCallContactAltPhone").next('label').addClass("active");
            }

            if (contactData.countryId) {
                $('#txtColdCallContactCountry').val(contactData.countryId);
                $('#txtColdCallContactCountry').material_select();
                $("#txtColdCallContactCountry").next('label').addClass("active");
            }
            $('#txtColdCallContactCountry').trigger('change');
            setTimeout(function () {
                $('#txtColdCallContactCity').val(contactData.cityId);
                $('#txtColdCallContactCity').material_select();
                if (contactData.cityId) {
                    $("#txtColdCallContactCity").next('label').addClass("active");
                }
            }, 500);

            $('#txtColdCallStatus').trigger('change');


        }
    }
    function ManageColdCallContactResponse(response) {
        $('#txtColdCallContactCountry').empty();
        $('#txtColdCallContactCity').empty();
        $('#txtColdCallContactCity').append($('<option selected></option>').val('').html('--Select--'));
        $('#txtColdCallContactCountry').append($('<option selected></option>').val('').html('--Select--'));
        if (response.data.countries.length > 0) {

            $.each(response.data.countries, function (key, value) {

                $('#txtColdCallContactCountry').append($('<option></option>').val(value.Id1).html(value.Country));
            });
        }
        $('#txtColdCallContactCountry').attr('disabled', true);
        $('#txtColdCallContactCountry').material_select();
        $('#txtColdCallContactCity').material_select();


    }
    $('#txtColdCallContactCountry').change(function () {

        var userId = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Lead/getContactCities';

        var params = {countryId: $(this).val()};
        var action = 'status';
        var headerParams = {action: action, context: 'default', serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var response =
                commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, params: params, action: action, onSuccess: getColdCallContactCitiesList});


    });
    function getColdCallContactCitiesList(response) {

        $('#txtColdCallContactCity').empty();
        $('#txtColdCallContactCity').append($('<option selected></option>').val('').html('--Select--'));
        if (response.data.data.length > 0) {

            $.each(response.data.data, function (key, value) {

                $('#txtColdCallContactCity').append($('<option></option>').val(value.id).html(value.value));
            });
        }
        $('#txtColdCallContactCity').material_select();

    }

//cold call status
    $('#txtColdCallStatus').on('change', function () {

        $('#ManageColdCallForm input').removeClass("required");
        $('#ManageColdCallForm select').removeClass("required");
        $('#ManageColdCallForm textarea').removeClass("required");
        $('#ManageColdCallForm span.required-msg').remove();
        $('#ComapnyWrapper').slideUp();
        $('#InstitutionWrapper').slideUp();
        var coldCallStatusVal = $(this).val();
        var contacttypeVal = $(this).val();

        if ($("input[name='txtColdcallStatusInfo']:checked").val() == 'callStatusInfoIntrested') {
            $(".coldcall-info").slideDown();
            getColdCallCourses($('#userBranchList').val());

        }

        if ($("input[name='contacttype']:checked").val() == 'contacttype-corp') {
            $("#contactType-corporate-wrap").slideDown();
        }

        if (coldCallStatusVal == 'answered') {
            $('#callStatusInfo').slideDown();
            $("#FollowupDatePicker").slideDown();
            clearDatePicker("#leadNextFollowUpData");
            $("input[name='txtColdcallStatusInfo']:visible:first").prop('checked', true);
            coldCallDropdowns();
        }
        if (coldCallStatusVal != 'answered') {
            $('#callStatusInfo').slideUp();
            $(".coldcall-info").slideUp();
            $("#FollowupDatePicker").slideUp();
        }
    });
    function coldCallDropdowns() {
        $('#ManageColdCallForm input').removeClass("required");
        $('#ManageColdCallForm select').removeClass("required");
        $('#ManageColdCallForm textarea').removeClass("required");
        $('#ManageColdCallForm span.required-msg').remove();
        $(".coldcall-info").hide();
        $('#ComapnyWrapper').slideUp();
        $('#InstitutionWrapper').slideUp();
        //$("#ColdCallDescriptionWrapper").slideUp();
        if ($("input[name='txtColdcallStatusInfo']:checked").val() == 'callStatusInfoIntrested') {
            $(".coldcall-info").slideDown();
            $(".coldcall-info").find('#coldcalling-branch').hide();
            getColdCallCourses($('#userBranchList').val());

        }
        else if ($("input[name='txtColdcallStatusInfo']:checked").val() == 'callStatusInfoIntrestedTransfer') {
            $(".coldcall-info").slideDown();
            $(".coldcall-info").find('#coldcalling-branch').slideDown();
            getColdCallCourses($('#txtColdCallContactBranch').val());
        }
    }
//
    $("input[name='txtColdcallStatusInfo']").click(function () {
        coldCallDropdowns();
    });
    $("input[name='coldCallcontacttype']").click(function () {
        $('#ManageColdCallForm input').removeClass("required");
        $('#ManageColdCallForm select').removeClass("required");
        $('#ManageColdCallForm textarea').removeClass("required");
        $('#ManageColdCallForm span.required-msg').remove();
        $('#ComapnyWrapper').slideUp();
        $('#InstitutionWrapper').slideUp();
        var contacttypeVal = $(this).val();
        if (contacttypeVal == 'Corporate') {
            $("#contactType-Institution-wrap").hide();
            $("#contactType-corporate-wrap").slideDown();
        }
        if (contacttypeVal == 'University') {
            $("#contactType-corporate-wrap").hide();
            $("#contactType-Institution-wrap").slideDown();
        }
        if (contacttypeVal == 'Retail') {
            $("#contactType-corporate-wrap").hide();
            $("#contactType-Institution-wrap").hide();
        }
        $("input[name='coldCallcontacttype']:checked").click();
    });
    function getColdCallCourses(branchId) {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Courses/getAllCourseList';
        var ajaxurl = API_URL + 'index.php/Courses/getBranchCoursesList';
        var params = {branchId:branchId};
        var action = 'status';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getColdCallCoursesResponse});
    }
    function getColdCallCoursesResponse(response) {

        $('#txtColdCallContactCourse').empty();
        $('#txtColdCallContactCourse').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtColdCallContactCourse').append($('<option></option>').val(value.courseId).html(value.name));
                });
            }
        }
        $('#txtColdCallContactCourse').material_select();


    }

    function getColdCallBranches() {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Branch/getBranchList';
        var params = {};
        var action = 'status';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
                commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: getColdCallBranchesResponse});
    }
    function getColdCallBranchesResponse(response) {
        $('#txtColdCallContactBranch').empty();
        $('#txtColdCallContactBranch').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    if($('#userBranchList').length>0 && value.id!=$('#userBranchList').val()) {
                    $('#txtColdCallContactBranch').append($('<option></option>').val(value.id).html(value.name));
                    }
                    if($('#userBranchList').length==0){
                        $('#txtColdCallContactBranch').append($('<option></option>').val(value.id).html(value.name));
                    }
                });
            }
        }
        $('#txtColdCallContactBranch').material_select();
    }
    $('#txtColdCallContactBranch').change(function(){
        getColdCallCourses($('#txtColdCallContactBranch').val());
    });
    function getColdCallPostQualifications() {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Post Qualification'};
        var action = 'status';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getColdCallPostQualificationsResponse});
    }
    function getColdCallPostQualificationsResponse(response) {
        $('#txtColdCallContactPostQualification').empty();
        $('#txtColdCallContactPostQualification').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtColdCallContactPostQualification').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                });
            }
        }
        $('#txtColdCallContactPostQualification').material_select();
        $('#txtColdCallContactPostQualification').trigger('change');
    }
    $('#txtColdCallContactPostQualification').change(function() {
        getColdCallQualifications();
    });
    function getColdCallQualifications() {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var parent_id='';
        if($('#txtColdCallContactPostQualification').length>0){
            parent_id=$('#txtColdCallContactPostQualification').val();
            if(parent_id==''){
                parent_id='N/A';
            }
        }
        var params = {name: 'Qualification',parent_id:parent_id};
        var action = 'status';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
                commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getColdCallQualificationsResponse});
    }
    function getColdCallQualificationsResponse(response) {
        $('#txtColdCallContactQualification').empty();
        $('#txtColdCallContactQualification').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtColdCallContactQualification').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                });
            }
        }
        $('#txtColdCallContactQualification').material_select();
    }
    function getColdCallCompanies() {

        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Company'};
        var action = 'status';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
                commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getColdCallCompaniesResponse});

    }
    function getColdCallCompaniesResponse(response) {
        $('#txtColdCallContactCompany').empty();
        $('#txtColdCallContactCompany').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtColdCallContactCompany').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                });
            }
        }
        $('#txtColdCallContactCompany').material_select();
    }
    function getColdCallInstitutions() {

        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Institution'};
        var action = 'status';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
                commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getColdCallInstitutionsResponse});

    }
    function getColdCallInstitutionsResponse(response) {

        $('#ManageColdCallForm span.required-msg').remove();
        $('#ManageColdCallForm input').removeClass("required");
        $('#ManageColdCallForm select').removeClass("required");

        $('#txtColdCallContactInstitution').empty();
        $('#txtColdCallContactInstitution').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtColdCallContactInstitution').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                });
            }
        }
        $('#txtColdCallContactInstitution').material_select();
    }
    function getColdCallContactTypes() {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Contact Type'};
        var action = 'status';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
                commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getColdCallContactTypesResponse});
    }
    function getColdCallContactTypesResponse(response) {

        var rowHtml = '';
        if (response.status === true) {

            if (response.data.length > 0) {
                var rdo_checked='';
                rowHtml += "<div class=\"multiple-radio\" id=\"contacatTypes\">";
                rowHtml += "<label>Lead Type</label>";
                $.each(response.data, function (key, value) {

                    rowHtml += "<span class=\"inline-radio\">";
                    if(value.value == 'Retail')
                    {
                        rdo_checked="checked";
                    }
                    else
                    {
                        rdo_checked="";
                    }
                    rowHtml += "<input class=\"with-gap\" name=\"coldCallcontacttype\" type=\"radio\" id=\"contacttype" + value.reference_type_value_id + "\" value=\"" + value.value + "\" "+rdo_checked+" />";
                    rowHtml += "<label for=\"contacttype" + value.reference_type_value_id + "\">" + value.value + "</label>";
                    rowHtml += "</span>";

                });
                rowHtml += "</div>";
            }

        }
        $('#coldCallingContactTypeWrapper').html(rowHtml);
        $("input[name='coldCallcontacttype']").click(function () {
            $('#ManageColdCallForm input').removeClass("required");
            $('#ManageColdCallForm select').removeClass("required");
            $('#ManageColdCallForm textarea').removeClass("required");
            $('#ManageColdCallForm span.required-msg').remove();
            var contacttypeVal = $(this).val();
            $('#ComapnyWrapper').slideUp();
            $('#InstitutionWrapper').slideUp();
            if (contacttypeVal == 'Corporate') {
                $("#contactType-Institution-wrap").hide();
                $("#contactType-corporate-wrap").slideDown();
            }
            if (contacttypeVal == 'University') {
                $("#contactType-corporate-wrap").hide();
                $("#contactType-Institution-wrap").slideDown();
            }
            if (contacttypeVal == 'Retail') {
                $("#contactType-corporate-wrap").hide();
                $("#contactType-Institution-wrap").hide();
            }
        });
    }
    function saveColdCalling(This) {

        $('#ManageColdCallForm input').removeClass("required");
        $('#ManageColdCallForm select').removeClass("required");
        $('#ManageColdCallForm textarea').removeClass("required");
        $('#ManageColdCallForm span.required-msg').remove();
        var leadId = $("#hdnLeadId").val();
        var userID = $("#hdnUserId").val();

        //var regx_txtContactName = /^[a-zA-Z][a-zA-Z0-9-\s]*$/;
        var regx_txtContactName = /^[a-zA-Z][ A-Za-z0-9_@./#&+-]*$/;
        var regx_txtContactEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var ColdCallContactName = $("#txtColdCallContactName").val();
        var ColdCallContactEmail = $("#txtColdCallContactEmail").val();
        var ColdCallContactPhone = $("#txtColdCallContactPhone").val();
        var ColdCallContactAltPhone = $("#txtColdCallContactAltPhone").val();
        var ColdCallStatus = $("#txtColdCallStatus option:selected").val();
        var ColdcallStatusInfo = $("input[name=txtColdcallStatusInfo]:checked").val();
        var ColdCallContactCourse = $("#txtColdCallContactCourse option:selected").val();
        var ColdCallContactBranch = $("#txtColdCallContactBranch option:selected").val();
        if (ColdcallStatusInfo == 'callStatusInfoIntrested') {
            var ColdCallContactBranch = $("#userBranchList option:selected").val();
        }
        var callType = $("#ddlCalltype").val();
        var coldCallcontacttype = $("input[name=coldCallcontacttype]:checked").val();
        var ColdCallContactPostQualification = $("#txtColdCallContactPostQualification option:selected").val();
        var ColdCallContactQualification = $("#txtColdCallContactQualification option:selected").val();
        var ColdCallContactCompany = $("#txtColdCallContactCompany option:selected").val();
        var ColdCallContactInstitution = $("#txtColdCallContactInstitution option:selected").val();
        var ColdcallNextFollowupData = $("#ColdCallNextFollowUpData").val();
        var ColdCallDescription = $('#ColdCallDescription').val();

        var flag = 0;
        if (ColdCallContactName == '') {
            $("#txtColdCallContactName").addClass("required");
            $("#txtColdCallContactName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtContactName.test($('#txtColdCallContactName').val()) === false) {
            $("#txtColdCallContactName").addClass("required");
            $("#txtColdCallContactName").after('<span class="required-msg">Invalid Name</span>');
            flag = 1;
        }
        if (ColdCallContactEmail == '') {
            $("#txtColdCallContactEmail").addClass("required");
            $("#txtColdCallContactEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtContactEmail.test($('#txtColdCallContactEmail').val()) === false) {
            $("#txtColdCallContactEmail").addClass("required");
            $("#txtColdCallContactEmail").after('<span class="required-msg">Invalid E-mail</span>');
            flag = 1;
        }
        if (ColdCallStatus == '') {

            $("#txtColdCallStatus").parent().find('.select-dropdown').addClass("required");
            $("#txtColdCallStatus").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;

        } else {

            if (ColdCallStatus == 'answered') {



                if (ColdcallStatusInfo == 'callStatusInfoIntrested' || ColdcallStatusInfo == 'callStatusInfoIntrestedTransfer') {

                    if (ColdCallContactCourse == '') {

                        $("#txtColdCallContactCourse").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactCourse").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;

                    }
                    if (ColdcallStatusInfo == 'callStatusInfoIntrestedTransfer' && ColdCallContactBranch == '') {
                        $("#txtColdCallContactBranch").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactBranch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }

                    /*if (ColdCallContactPostQualification == '') {
                        $("#txtColdCallContactPostQualification").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactPostQualification").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }
                    if (ColdCallContactQualification == '') {
                        $("#txtColdCallContactQualification").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactQualification").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }*/

                    if (coldCallcontacttype == 'Corporate' && ColdCallContactCompany == '') {
                        $("#txtColdCallContactCompany").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactCompany").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }

                    if (coldCallcontacttype == 'University' && ColdCallContactInstitution == '') {
                        $("#txtColdCallContactInstitution").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactInstitution").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }

                } else if (ColdcallStatusInfo == 'callStatusInfoNotintrested') {



                } else {

                }
                if(ColdcallNextFollowupData == '')
                {
                    $("#ColdCallNextFollowUpData").addClass("required");
                    $("#ColdCallNextFollowUpData").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                    flag = 1;
                }

            } else {

            }


        }
        if (ColdCallDescription == '' || $.trim(ColdCallDescription) == '') {
            $("#ColdCallDescription").addClass("required");
            $("#ColdCallDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        /*else if ((/^[ A-Za-z0-9_@./#&$%*!+-]*$/).test(ColdCallDescription) == false)
        {
            $("#ColdCallDescription").addClass("required");
            $("#ColdCallDescription").after('<span class="required-msg">Invalid Description</span>');
            flag = 1;
        }*/
        if (flag == 0) {
            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/ColdCalling/saveColdCall';

            var params = {leadId: leadId, ColdCallContactName: ColdCallContactName, ColdCallContactEmail: ColdCallContactEmail,ColdCallContactPhone: ColdCallContactPhone,ColdCallContactAltPhone: ColdCallContactAltPhone, ColdCallStatus: ColdCallStatus, ColdcallStatusInfo: ColdcallStatusInfo, ColdCallContactCourse: ColdCallContactCourse, ColdCallContactBranch: ColdCallContactBranch, coldCallcontacttype: coldCallcontacttype, ColdCallContactQualification: ColdCallContactQualification, ColdCallContactCompany: ColdCallContactCompany, ColdCallContactInstitution: ColdCallContactInstitution,ColdcallNextFollowupData:ColdcallNextFollowupData, ColdCallDescription: ColdCallDescription,callType:callType};
            var action = 'status';
            var type = "POST";
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            var response =
                    commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: saveColdCallingResponse});

        }


    }
    function saveColdCallingResponse(response) {

        alertify.dismissAll();
        if (response.status === true) {
            buildcoldCallContactsDataTable();
            buildcoldCallContactsCounts();
            notify(response.message, 'success', 10);
            $('#coldCalling').modal('hide');
            $('body').find('#coldCalling').removeClass('modal-open');
            $('.modal-backdrop').remove();

        } else {
            notify(response.message, 'error', 10);
        }
    }
    function buildcoldCallContactsCounts() {
        var ajaxurl = API_URL + 'index.php/ColdCalling/coldCallCounts';
        var userID = $('#hdnUserId').val();
        var action = 'list';
        var branchId = $('#userBranchList').val();
        var params = {branchId: branchId};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
                commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: coldCallContactCountResponse});
    }
    function coldCallContactCountResponse(response) {

        var overallScheduledCount = 0;
        var callsDueCount = 0;
        var convertedLeadsCount = 0;
        var dndCount = 0;
        var rejectedCount = 0;
        if (response.status === true) {
            overallScheduledCount = response.data.overallScheduledCount;
            callsDueCount = response.data.callsDueCount;
            convertedLeadsCount = response.data.convertedLeadsCount;
            dndCount = response.data.dndCount;
            rejectedCount = response.data.rejectedCount;
        }
        $('#overallScheduledCountWrapper').html(overallScheduledCount);
        $('#callsDueCountWrapper').html(callsDueCount);
        $('#convertedLeadsCountWrapper').html(convertedLeadsCount);
        $('#dndCountWrapper').html(dndCount);
        $('#rejectedCountWrapper').html(rejectedCount);
    }
//Cold Calling related functions - coldcalling.php in view (ends here)//

    function addCompanyWrapper(This)
    {
        $('#ComapnyWrapper span.required-msg').remove();
        //$('#ContactTagWrapper label').removeClass("active");
        //$('#ContactTagWrapper input').removeClass("required");
        var url = '';
        var action = 'add reference';
        var name = $('#txtComapnyNameWrapper').val();
        var description = '----';
        var data = '';
        data = {
            companyName: name,
            companyDescription: description
        };
        url = API_URL + "index.php/Tag/addCompany";
        var regx_txtTagWrapper = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

        var flag = 0;
        if (name == '') {
            $("#txtComapnyNameWrapper").addClass("required");
            $("#txtComapnyNameWrapper").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if ((regx_txtTagWrapper.test(name)) === false)
        {
            $("#txtComapnyNameWrapper").addClass("required");
            $("#txtComapnyNameWrapper").after('<span class="required-msg">Invalid Company Name.</span>');
            flag = 1;
        }

        if (flag == 0) {
            var ajaxurl = url;
            var params = data;

            var userId = $('#hdnUserId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                    if (response == -1 || response['status'] == false)
                    {

                        var id = '';
                        alertify.dismissAll();
                        notify(response['message'], 'error', 10);
                        for (var a in response.data) {
                            if (a == 'companyName') {
                                id = '#txtComapnyNameWrapper';
                                $(id).addClass("required");
                                $(id).next('label').addClass("active");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else {
                                id = '#' + a;
                            }

                        }

                    } else
                    {
                        alertify.dismissAll();
                        notify('Company Saved Successfully', 'success', 10);
                        $('#txtComapnyNameWrapper').val('');
                        $('#ComapnyWrapper label').removeClass("active");
                        $('#ComapnyWrapper input').removeClass("required");
                        $('#ComapnyWrapper span.required-msg').remove();
                        $('#ComapnyWrapper').slideUp();
                        getColdCallCompanies(This);
                        $("#ComapnyWrapper").hide();
                    }
                }});

        } else {
            return false;
        }
    }
    function resetCompanyWrapper(This) {
        $('#ComapnyWrapper span.required-msg').remove();
        $('#ComapnyWrapper label').removeClass("active");
        $('#ComapnyWrapper input').removeClass("required");
        $('#txtComapnyNameWrapper').val('');

    }
    function addInstitutionWrapper(This)
    {
        $('#InstitutionWrapper span.required-msg').remove();
        //$('#ContactTagWrapper label').removeClass("active");
        //$('#ContactTagWrapper input').removeClass("required");
        var url = '';
        var action = 'status';
        var name = $('#txtInstitutionNameWrapper').val();
        var description = '----';
        var data = '';
        data = {
            institutionName: name,
            institutionDescription: description
        };
        url = API_URL + "index.php/Tag/addInstitution";
        var regx_txtTagWrapper = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

        var flag = 0;
        if (name == '') {
            $("#txtInstitutionNameWrapper").addClass("required");
            $("#txtInstitutionNameWrapper").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if ((regx_txtTagWrapper.test(name)) === false)
        {
            $("#txtInstitutionNameWrapper").addClass("required");
            $("#txtInstitutionNameWrapper").after('<span class="required-msg">Invalid Institution Name.</span>');
            flag = 1;
        }

        if (flag == 0) {
            var ajaxurl = url;
            var params = data;

            var userId = $('#hdnUserId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                    if (response == -1 || response['status'] == false)
                    {

                        var id = '';
                        alertify.dismissAll();
                        notify(response['message'], 'error', 10);
                        for (var a in response.data) {
                            if (a == 'institutionName') {
                                id = '#txtInstitutionNameWrapper';
                                $(id).addClass("required");
                                $(id).next('label').addClass("active");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else {
                                id = '#' + a;
                            }

                        }

                    } else
                    {
                        alertify.dismissAll();
                        notify('Institution Saved Successfully', 'success', 10);
                        $('#txtInstitutionNameWrapper').val('');
                        $('#InstitutionWrapper label').removeClass("active");
                        $('#InstitutionWrapper input').removeClass("required");
                        $('#InstitutionWrapper span.required-msg').remove();
                        $('#InstitutionWrapper').slideUp();
                        getColdCallInstitutions(This);
                    }
                }});

        } else {
            return false;
        }
    }
    function resetInstitutionWrapper(This) {
        $('#InstitutionWrapper span.required-msg').remove();
        $('#InstitutionWrapper label').removeClass("active");
        $('#InstitutionWrapper input').removeClass("required");
        $('#txtInstitutionNameWrapper').val('');

    }
// contact creation and auto call schedule of created contacts in cold calling - starts here //
    function ManageColdCallContact(This, Id) {
        notify('Processing..', 'warning', 10);
        $('#ManageConcatForm')[0].reset();
        $('#txtContactCity').empty();
        $('#txtContactCity').append($('<option selected></option>').val('').html('--Select--'));
        $('#txtContactCity').material_select();
        $('#txtContactCountry').empty();
        $('#txtContactCountry').append($('<option selected></option>').val('').html('--Select--'));
        $('#txtContactCountry').material_select();
        $('#txtContactTag').empty();
        //$('#txtContactTag').material_select();
        $('#ManageConcatForm label').removeClass("active");
        $('#ManageConcatForm span.required-msg').remove();
        $('#ManageConcatForm input').removeClass("required");
        $('#ManageConcatForm select').removeClass("required");
        $('#ContactTagWrapper').slideUp();
        $('#ContactSourceWrapper').slideUp();
        /*if ($('#ContactTagWrapper').css('display') == 'block') {
            $('.showAddCompanyWrap-btn').click();
        }*/

        var userID = $("#hdnUserId").val();
        $("#hdnLeadId").val(Id);
        var action = 'add';
        var branchId = $('#userBranchList').val();
        var ajaxurl = API_URL + 'index.php/ColdCalling/getContactConfigurations';
        var params = {branchId: branchId};
        var headerParams = {context: 'default', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: ManageColdCallAddContactResponse});
        getContactSourcesDropdown();
        if (Id == 0) {
            //create
            $("#myModalLabel").html("Create Contact");
            $("#btnSaveContact").val("Add");
            $("#btnSaveContact").html("<i class=\"icon-right mr8\"></i>Add");
            alertify.dismissAll();
            $(This).attr("data-target", "#createContact");
            $(This).attr("data-toggle", "modal");
        }
    }

    function ManageColdCallAddContactResponse(response) {
        $('#txtContactCountry').empty();
        $('#txtContactCity').empty();
        $('#txtContactCity').append($('<option selected></option>').val('').html('--Select--'));
        $('#txtContactCountry').append($('<option selected></option>').val('').html('--Select--'));
        var branchId = $('#userBranchList').val();
        if (response.data.countries.length > 0) {

            $.each(response.data.countries, function (key, value) {

                $('#txtContactCountry').append($('<option></option>').val(value.countryId).html(value.countryName).attr({
                    selected: "selected"
                }));
            });
        }
        $('#txtContactCountry').attr('disabled', true);
        $('#txtContactCountry').material_select();
        if (response.data.cities.length > 0) {

            $.each(response.data.cities, function (key, value) {
                var Cityselected = '';
                if (branchId == value.fk_city_id) {
                    Cityselected = 'selected';
                }
                $('#txtContactCity').append($('<option></option>').val(value.fk_city_id).html(value.City).attr('selected', Cityselected));

            });
        }
        $('#txtContactCity').material_select();
        $('#txtContactTag').append($('<option></option>').val('').html('--Select All--'));
        if (response.data.tags.length > 0) {

            $.each(response.data.tags, function (key, value) {
                $('#txtContactTag').append($('<option></option>').val(value.id).html(value.tagName));
            });
        }
        $('#txtContactTag').trigger("chosen:updated");
        $("#txtContactTag").chosen({
            no_results_text: "Oops, nothing found!",
            width: "95%"
        });
        //$('#txtContactTag').material_select();

    }
    function saveColdCallAddContact(This) {
        var contactId = $("#hdnLeadId").val();
        var branchId = $('#userBranchList').val();
        var contactName = $("#txtContactName").val();
        var contactEmail = $("#txtContactEmail").val();
        var contactPhone = $("#txtContactPhone").val();
        var userID = $("#hdnUserId").val();
        var contactCountryText = $("#txtContactCountry option:selected").text();
        var contactCityText = $("#txtContactCity option:selected").text();
        var contactSourceText = $("#txtContactSource option:selected").text();
        var selMulti = $.map($("#txtContactCountry option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {

                return $(el).val();
            }
        });
        var contactCountry = selMulti.join(",");
        var selMulti = $.map($("#txtContactCity option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '') {
                return $(el).val();
            }
        });
        var contactCity = selMulti.join(",");
        var selMulti = $.map($("#txtContactTag option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).val();
            }
        });
        var contactTags = selMulti.join(",");

        var selMulti = $.map($("#txtContactTag option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).text();
            }
        });
        var contactTagsText = selMulti.join(",");
        var selMulti = $.map($("#txtContactSource option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All')
            {
                return $(el).val();
            }
        });
        var contactSourse = selMulti.join(",");
        var regx_txtContactName = /^[a-zA-Z][ A-Za-z0-9_@./#&+-]*$/;
        var regx_txtContactEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var regx_txtContactPhone = /^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/;

        $('#ManageConcatForm span.required-msg').remove();
        $('#ManageConcatForm input').removeClass("required");
        var flag = 0;
        if (contactName == '')
        {
            $("#txtContactName").addClass("required");
            $("#txtContactName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtContactName.test($('#txtContactName').val()) === false) {
            $("#txtContactName").addClass("required");
            $("#txtContactName").after('<span class="required-msg">Accepts only alphanumeric with spaces</span>');
            flag = 1;
        }
        if (contactEmail == '' && contactPhone == '')
        {
            $("#txtContactEmail").addClass("required");
            $("#txtContactEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (contactEmail!='' && regx_txtContactEmail.test($('#txtContactEmail').val()) === false) {
            $("#txtContactEmail").addClass("required");
            $("#txtContactEmail").after('<span class="required-msg">Invalid E-mail</span>');
            flag = 1;
        }
        if (contactPhone == '' && contactEmail == '')
        {
            $("#txtContactPhone").addClass("required");
            $("#txtContactPhone").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (contactPhone != '')
        {
            if (contactPhone.length < 8) {
                $("#txtContactPhone").addClass("required");
                $("#txtContactPhone").after('<span class="required-msg">'+$phoneMinLengthMessage+'</span>');
                flag = 1;
            }
            else if (contactPhone.length > 15) {
                $("#txtContactPhone").addClass("required");
                $("#txtContactPhone").after('<span class="required-msg">'+$phoneMaxLengthMessage+'</span>');
                flag = 1;
            }else if (contactPhone!='' && regx_txtContactPhone.test($('#txtContactPhone').val()) === false) {
                $("#txtContactPhone").addClass("required");
                $("#txtContactPhone").after('<span class="required-msg">Invalid Phone Number</span>');
                flag = 1;
            }
        }

        if (contactCountry == '')
        {
            $("#txtContactCountry").parent().find('.select-dropdown').addClass("required");
            $("#txtContactCountry").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (contactCity == '')
        {
            $("#txtContactCity").parent().find('.select-dropdown').addClass("required");
            $("#txtContactCity").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        /*if (contactTags == '')
        {
            $("#txtContactTag").parent().find('.select-dropdown').addClass("required");
            $("#txtContactTag").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }*/
        if (contactSourse == '')
        {
            $("#txtContactSource").parent().find('.select-dropdown').addClass("required");
            $("#txtContactSource").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (flag == 1) {
            return false;
        } else {
            notify('Processing..', 'warning', 10);
            if (contactId == 0) {
                var ajaxurl = API_URL + 'index.php/ColdCalling/addContact';
                var params = {userID: userID, contactName: contactName, contactEmail: contactEmail, contactPhone: contactPhone, contactCity: contactCityText, contactCountry: contactCountryText, contactTags: contactTagsText, branchId: branchId, contactSource: contactSourceText};
                var type = "POST";
                var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: This, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: saveColdCallAddContactResponse});
            }

        }
    }
    function saveColdCallAddContactResponse(response) {

        var response = response;
        alertify.dismissAll();
        if (response == -1 || response.status == false)
        {
            $.each(response.data, function (i, item) {
                notify(item, 'error', 10);
                if (response.data['contactName'])
                {
                    $("#txtContactName").addClass("required");
                    $("#txtContactName").after('<span class="required-msg">' + response.data['contactName'] + '</span>');
                }
                if (response.data['contactEmail'])
                {
                    $("#txtContactEmail").addClass("required");
                    $("#txtContactEmail").after('<span class="required-msg">' + response.data['contactEmail'] + '</span>');
                }
                if (response.data['contactPhone'])
                {
                    $("#txtContactPhone").addClass("required");
                    $("#txtContactPhone").after('<span class="required-msg">' + response.data['contactPhone'] + '</span>');
                }
                if (response.data['contactCity'])
                {
                    $("#txtContactCity").addClass("required");
                    $("#txtContactCity").after('<span class="required-msg">' + response.data['contactCity'] + '</span>');
                }
                if (response.data['contactTags'])
                {
                    $("#txtContactTag").addClass("required");
                    $("#txtContactTag").after('<span class="required-msg">' + response.data['contactCity'] + '</span>');
                }
                return false;
            });
        } else
        {
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            buildcoldCallContactsDataTable();
            buildcoldCallContactsCounts();

            $('#createContact').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }
    }
// contact creation and auto call schedule of created contacts in cold calling - ends here //



    function addContactSourceWrapper(This)
    {
        $('#ContactSourceWrapper span.required-msg').remove();
        //$('#ContactTagWrapper label').removeClass("active");
        //$('#ContactTagWrapper input').removeClass("required");
        var url = '';
        var action = 'add';
        var name = $('#txtContactSourceWrapper').val();
        var description = '----';
        var data = '';
        var referenceType = 'Contact Source';
        data = {
            referenceName: name,
            referenceDescription: description,
            referenceType: referenceType
        };
        url = API_URL + "index.php/Referencevalues/addReferenceValueWrapper";
        var regx_txtTagWrapper = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

        var flag = 0;
        if (name == '') {
            $("#txtContactSourceWrapper").addClass("required");
            $("#txtContactSourceWrapper").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if ((regx_txtTagWrapper.test(name)) === false)
        {
            $("#txtContactSourceWrapper").addClass("required");
            $("#txtContactSourceWrapper").after('<span class="required-msg">Invalid Source.</span>');
            flag = 1;
        }

        if (flag == 0) {
            var ajaxurl = url;
            var params = data;

            var userId = $('#hdnUserId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                    if (response == -1 || response['status'] == false)
                    {

                        var id = '';
                        alertify.dismissAll();
                        notify(response['message'], 'error', 10);
                        for (var a in response.data) {
                            if (a == referenceType) {
                                id = '#txtContactSourceWrapper';
                                $(id).addClass("required");
                                $(id).next('label').addClass("active");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else {
                                id = '#' + a;
                            }

                        }

                    } else
                    {
                        alertify.dismissAll();
                        notify('Saved Successfully', 'success', 10);
                        $('#txtContactSourceWrapper').val('');
                        $('#ContactSourceWrapper').slideUp();
                        $('#ContactSourceWrapper label').removeClass("active");
                        $('#ContactSourceWrapper input').removeClass("required");
                        $('#ContactSourceWrapper span.required-msg').remove();
                        getContactSourcesDropdown(this);

                    }
                }});

        } else {
            return false;
        }
    }
    function resetContactSourceWrapper(This) {
        $('#ContactSourceWrapper span.required-msg').remove();
        $('#ContactSourceWrapper label').removeClass("active");
        $('#ContactSourceWrapper input').removeClass("required");
        $('#txtContactSourceWrapper').val('');

    }
    function getContactSourcesDropdown() {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Contact Source'};
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
                commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getContactSourcesResponse});
    }
    function getContactSourcesResponse(response) {
        $('#txtContactSource').empty();
        $('#txtContactSource').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtContactSource').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                });
            }
            $('#txtContactSource').trigger("chosen:updated");
        }
        $("#txtContactSource").chosen({
            no_results_text: "Oops, nothing found!",
            width: "95%"
        });
        //$('#txtContactSource').material_select();
    }

    /*Leads start*/

    $('#txtPinfoAltmobile').keyup(function ()
    {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    function getLeadPersonalInfoCompanies(resp)
    {
        $('#ddlLeadInfoCompanies').empty();
        $('#ddlLeadInfoCompanies').material_select();
        $('#ddlLeadInfoCompanies').append($('<option></option>').val('').html('--Select--'));

        if (resp.data.length == 0 || resp == -1 || resp['status'] == false)
        {
        } else
        {
            var LeadCompanies = resp.data;
            for (var b = 0; b < LeadCompanies.length; b++) {
                var o = $('<option/>', {value: LeadCompanies[b]['reference_type_value_id'], 'parent-id': LeadCompanies[b]['reference_type_value_id']})
                        .text(LeadCompanies[b]['value']);
                o.appendTo('#ddlLeadInfoCompanies');
            }
            $('#ddlLeadInfoCompanies').material_select();
        }
    }
    function getLeadPersonalInfoCourses(resp)
    {
        $('#ddlLeadInfoCourses').empty();
        $('#ddlLeadInfoCourses').material_select();
        $('#ddlLeadInfoCourses').append($('<option></option>').val('').html('--Select--'));
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false)
        {
        } else
        {
            var LeadCourses = resp.data;
            for (var b = 0; b < LeadCourses.length; b++) {
                var o = $('<option/>', {value: LeadCourses[b]['courseId'], 'parent-id': LeadCourses[b]['courseId']})
                        .text(LeadCourses[b]['name']);
                o.appendTo('#ddlLeadInfoCourses');
            }
            $('#ddlLeadInfoCourses').material_select();
        }
    }
    function getLeadPersonalInfoDesignations(resp)
    {
        $('#ddlLeadInfoDesignation').empty();
        //$('#ddlLeadInfoDesignation').material_select();
        $('#ddlLeadInfoDesignation').append($('<option></option>').val('').html('--Select--'));
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false)
        {
        } else
        {
            var LeadDesignation = resp.data;
            for (var b = 0; b < LeadDesignation.length; b++) {
                var o = $('<option/>', {value: LeadDesignation[b]['reference_type_value_id'], 'parent-id': LeadDesignation[b]['reference_type_value_id']})
                        .text(LeadDesignation[b]['value']);
                o.appendTo('#ddlLeadInfoDesignation');
            }
            $('#ddlLeadInfoDesignation').trigger("chosen:updated");
            $("#ddlLeadInfoDesignation").chosen({
                no_results_text: "Oops, nothing found!",
                width: "95%"
            });
            //$('#ddlLeadInfoDesignation').material_select();
        }
    }
    function getLeadPersonalInfoQualification(resp)
    {
        $('#ddlLeadInfoEducation').empty();
        $('#ddlLeadInfoEducation').material_select();
        $('#ddlLeadInfoEducation').append($('<option></option>').val('').html('--Select--'));
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false)
        {
        } else
        {
            var LeadEducation = resp.data;
            for (var b = 0; b < LeadEducation.length; b++) {
                var o = $('<option/>', {value: LeadEducation[b]['reference_type_value_id'], 'parent-id': LeadEducation[b]['reference_type_value_id']})
                        .text(LeadEducation[b]['value']);
                o.appendTo('#ddlLeadInfoEducation');
            }
            $('#ddlLeadInfoEducation').material_select();
        }
    }
    function getLeadPersonalInfoPostQualification(resp,parent_id,qual_id)
    {
        $('#ddlLeadInfoPostEducation').empty();
        $('#ddlLeadInfoPostEducation').material_select();
        $('#ddlLeadInfoPostEducation').append($('<option></option>').val('').html('--Select--'));
        if (resp.data.length == 0 || resp == -1 || resp['status'] == false)
        {
        } else
        {
            var LeadEducation = resp.data;
            for (var b = 0; b < LeadEducation.length; b++) {


                    var o = $('<option/>', {
                        value: LeadEducation[b]['reference_type_value_id'],
                        'parent-id': LeadEducation[b]['reference_type_value_id']
                    }).text(LeadEducation[b]['value']);
                    o.appendTo('#ddlLeadInfoPostEducation');
            }
            if(parent_id!=''){
                $('#ddlLeadInfoPostEducation').val(parent_id);
            }
            $('#ddlLeadInfoPostEducation').material_select();
        }
        getLeadPersonalInfoQualificationList(parent_id,qual_id);

    }
    $('#ddlLeadInfoPostEducation').change(function(){
        var parent_id='';
        if($('#ddlLeadInfoPostEducation').length>0){
            parent_id=$('#ddlLeadInfoPostEducation').val();
            if(parent_id==''){
                parent_id='N/A';
            }
        }
        getLeadPersonalInfoQualificationList(parent_id,'');
    });
    function getLeadPersonalInfoQualificationList(parent_id,qual_id) {

        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';

        var params = {name: 'Qualification',parent_id:parent_id};
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
                $('#ddlLeadInfoEducation').empty();
                $('#ddlLeadInfoEducation').append($('<option selected></option>').val('').html('--Select--'));
                if (response.status == true) {

                    if (response.data.length > 0) {

                        $.each(response.data, function (key, value) {

                            $('#ddlLeadInfoEducation').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                        });
                    }
                }
                if(qual_id!=''){
                    $('#ddlLeadInfoEducation').val(qual_id);
                }
                $('#ddlLeadInfoEducation').material_select();
            }});
    }

    function SaveLeadPersonalInfoDesignation(This) {
        $('#ContactTagWrapper span.required-msg').remove();
        //$('#ContactTagWrapper label').removeClass("active");

        var Designation = $('#txtLeadInfoDesignation').val();
        var userID = $("#hdnUserId").val();
        var flag = 0;
        var RegexName = /^(\w+\s)*\w+$/;
        if (Designation == '')
        {
            $("#txtLeadInfoDesignation").addClass("required");
            $("#txtLeadInfoDesignation").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (RegexName.test(Designation) === false)
        {
            $("#txtLeadInfoDesignation").addClass("required");
            $("#txtLeadInfoDesignation").after('<span class="required-msg">Accepts only alphanumeric with spaces</span>');
            flag = 1;
        }
        if (flag == 0)
        {

            var ajaxurl = API_URL + 'index.php/LeadsInfo/AddDesignation';
            var params = {UserID: userID, DesignationName: Designation, ReferenceType: 'Designation'};
            var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: This, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: SaveDesignation});

        } else
        {
            return false;
        }
    }
    function SaveDesignation(response) {
        alertify.dismissAll();
        if(response.status===true) {
        notify(response.message, 'success', 10);
        $('#txtLeadInfoDesignation').val('');
        leadInfoPageLoad();
        $('#ContactTagWrapper').hide();
    }
        else{
            $("#txtLeadInfoDesignation").addClass("required");
            $("#txtLeadInfoDesignation").next('label').addClass("active");
            $("#txtLeadInfoDesignation").after('<span class="required-msg">'+response.message+'</span>');
        }
    }
    function resetDesignationWrapper(This) {
        $('#ContactTagWrapper span.required-msg').remove();
        $('#ContactTagWrapper label').removeClass("active");
        $('#ContactTagWrapper input').removeClass("required");

    }
    var requestRunningUpdatePersonalInfo = false;
    function UpdatePersonalInfo(This)
    {
        if (requestRunningUpdatePersonalInfo) { // don't do anything if an AJAX request is pending
            return;
        }
        requestRunningUpdatePersonalInfo=true;
        var leadId = $("#hdnLeadId").val();
        var PinfoName = $("#txtPinfoName").val();
        var pinfoCourse = $("#ddlLeadInfoCourses").val();
        var LeadGender = $('[name=txtUserGender]:checked').val();
        if(LeadGender=="male"){ LeadGender=1;}else{LeadGender=0;}
        var PinfoMobile = $("#txtPinfoMobile").val();
        var PinfoAltMobile = $("#txtPinfoAltmobile").val();
        var PinfoCompany = $("#ddlLeadInfoCompanies").val();
        var PinfoEmail = $("#txtPinfoEmail").val();
        var PinfoDesignation = $("#ddlLeadInfoDesignation").val();
        var PinfoPostEducation = $("#ddlLeadInfoPostEducation").val();
        var PinfoEducation = $("#ddlLeadInfoEducation").val();
        var ColdCallContactCompany = $("#txtColdCallContactCompany option:selected").val();
        var ColdCallContactInstitution = $("#txtColdCallContactInstitution option:selected").val();
        var coldCallcontacttype = $("input[name=coldCallcontacttype]:checked").val();
        var leadAddress = $("#ddlLeadAddress").val();
        var regx_txtUserEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var userID = $("#hdnUserId").val();
        var RegexName = /^(\w+\s)*\w+$/;
        var regx_txtUserPhoneNumber = /^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/;
        $('#ManageLeadsForm span.required-msg').remove();
        $('#ManageLeadsForm input').removeClass("required");
        $('#ManageLeadsForm textarea').removeClass("required");
        var flag = 0;
        if (PinfoName == '')
        {
            $("#txtPinfoName").addClass("required");
            $("#txtPinfoName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (RegexName.test(PinfoName) === false)
        {
            $("#txtPinfoName").addClass("required");
            $("#txtPinfoName").after('<span class="required-msg">Accepts only alphanumeric with spaces</span>');
            flag = 1;
        }

        if (pinfoCourse == "")
        {
            $("#ddlLeadInfoCourses").parent().find('.select-dropdown').addClass("required");
            $("#ddlLeadInfoCourses").parent().after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }

        if (PinfoMobile == "")
        {
            $("#txtPinfoMobile").addClass("required");
            $("#txtPinfoMobile").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if (PinfoMobile.length < 8)
        {
            $("#txtPinfoMobile").addClass("required");
            $("#txtPinfoMobile").after('<span class="required-msg">'+$phoneMinLengthMessage+'</span>');
            flag = 1;
        }else if (PinfoMobile.length > 15)
        {
            $("#txtPinfoMobile").addClass("required");
            $("#txtPinfoMobile").after('<span class="required-msg">'+$phoneMaxLengthMessage+'</span>');
            flag = 1;
        }
        else if (regx_txtUserPhoneNumber.test(PinfoMobile) == false)
        {
            $("#txtPinfoMobile").addClass("required");
            $("#txtPinfoMobile").after('<span class="required-msg">Invalid Phone Number</span>');
            flag = 1;
        }

        if (PinfoAltMobile == "")
        {
            $("#txtPinfoAltmobile").addClass("required");
            $("#txtPinfoAltmobile").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (PinfoAltMobile.length < 8)
        {
            $("#txtPinfoAltmobile").addClass("required");
            $("#txtPinfoAltmobile").after('<span class="required-msg">'+$phoneMinLengthMessage+'</span>');
            flag = 1;
        } else if (PinfoAltMobile.length > 15)
        {
            $("#txtPinfoAltmobile").addClass("required");
            $("#txtPinfoAltmobile").after('<span class="required-msg">'+$phoneMaxLengthMessage+'</span>');
            flag = 1;
        } else if (regx_txtUserPhoneNumber.test(PinfoAltMobile) === false)
        {
            $("#txtPinfoAltmobile").addClass("required");
            $("#txtPinfoAltmobile").after('<span class="required-msg">Invalid Phone Number</span>');
            flag = 1;
        }
        else if(PinfoMobile == PinfoAltMobile)
        {
            $("#txtPinfoAltmobile").addClass("required");
            $("#txtPinfoAltmobile").after("<span class='required-msg'>Mobile and alternate number can't be same</span>");
            flag = 1;
        }
        if (PinfoCompany == "")
        {
            $("#ddlLeadInfoCompanies").parent().find('.select-dropdown').addClass("required");
            $("#ddlLeadInfoCompanies").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (PinfoEmail == "")
        {
            $("#txtPinfoEmail").addClass("required");
            $("#txtPinfoEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtUserEmail.test(PinfoEmail) === false)
        {
            $("#txtPinfoEmail").addClass("required");
            $("#txtPinfoEmail").after('<span class="required-msg">Invalid Email</span>');
            flag = 1;
        }
        if (coldCallcontacttype == 'Corporate' && ColdCallContactCompany == '') {
            $("#txtColdCallContactCompany").parent().find('.select-dropdown').addClass("required");
            $("#txtColdCallContactCompany").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (coldCallcontacttype == 'University' && ColdCallContactInstitution == '') {
            $("#txtColdCallContactInstitution").parent().find('.select-dropdown').addClass("required");
            $("#txtColdCallContactInstitution").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (PinfoDesignation == "") {
            $("#ddlLeadInfoDesignation").parent().find('.select-dropdown').addClass("required");
            $("#ddlLeadInfoDesignation").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if(PinfoPostEducation==""){
            $("#ddlLeadInfoPostEducation").parent().find('.select-dropdown').addClass("required");
            $("#ddlLeadInfoPostEducation").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (PinfoEducation == "") {
            $("#ddlLeadInfoEducation").parent().find('.select-dropdown').addClass("required");
            $("#ddlLeadInfoEducation").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (flag == 0)
        {
            notify('Processing..', 'warning', 10);
            $processedStatus = 1;
            var ajaxurl;
            var params;
            if (leadId == 0)
            {
                /* var ajaxurl = API_URL + 'index.php/LeadsInfo/addPersonalInfo';
                 var params = {UserID:userID,pInfoName:PinfoName,pInfoCourse:pinfoCourse,pInfoAltMobile:PinfoAltMobile,
                 pInfoCompany:PinfoCompany,pInfoEmail:PinfoEmail,pInfoDesignation:PinfoDesignation,pInfoEducation:PinfoEducation};
                 var headerParams = {action:'add',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
                 commonAjaxCall({This:this,method:'POST',requestUrl:ajaxurl,params:params,headerParams:headerParams,action:'add',onSuccess:SaveInfoResponse});*/
            } else
            {
                ajaxurl = API_URL + 'index.php/LeadsInfo/UpdatePersonalInfo';
                params = {pInfoName: PinfoName,LeadGender:LeadGender, pInfoCourse: pinfoCourse,PinfoMobile:PinfoMobile, pInfoAltMobile: PinfoAltMobile,
                    pInfoCompany: PinfoCompany, pInfoEmail: PinfoEmail, pInfoDesignation: PinfoDesignation, pInfoEducation: PinfoEducation, LeadID: leadId, UserID: userID,coldCallcontacttype:coldCallcontacttype,ColdCallContactCompany:ColdCallContactCompany,ColdCallContactInstitution:ColdCallContactInstitution, leadAddress: leadAddress};
                var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: SavePersonalInfoResponse});
            }
        } else
        {
            requestRunningUpdatePersonalInfo = false;
            return false;
        }
    }
    function SavePersonalInfoResponse(response) {
        alertify.dismissAll();
        requestRunningUpdatePersonalInfo=false;
        if (response.status == true)
        {
            notify(response.message, 'success', 10);
            $('#editPersonalinfo').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            leadInfoPageLoad();
        } else
        {
            var id = '', flag = 0;
            notify(response.message, 'error', 10);
            for (var a in response.data)
            {
                id = '#' + a;
                if (a == 'alternate')
                {
                    $("#txtPinfoAltmobile").addClass("required");
                    $("#txtPinfoAltmobile").after('<span class="required-msg">' + response.data[a] + '</span>');
                }
                if (a == 'mobile')
                {
                    $("#txtPinfoMobile").addClass("required");
                    $("#txtPinfoMobile").after('<span class="required-msg">' + response.data[a] + '</span>');
                }
                else
                {
                    $(id).addClass("required");
                    $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                }
            }
        }
    }
    function ManageEducationalInfo(This, Id)
    {
        //alertify.dismissAll();
        //notify('Processing..', 'warning', 10);
        //clearDatePicker("#dpEduYearCompletion");
        $('#ManageEducationalInfoForm')[0].reset();
        $('#ManageEducationalInfoForm label').removeClass("active");
        $('#ManageEducationalInfoForm span.required-msg').remove();
        $('#ManageEducationalInfoForm input').removeClass("required");
        $('#ManageEducationalInfoForm select').removeClass("required");
        $('#txtLeadInfotQualification').empty();
        $('#txtLeadInfotQualification').material_select();
        var userID = $('#hdnUserId').val();
        var currentTime = new Date();
        var year = currentTime.getFullYear();
        $('#dpEduYearCompletion').empty();
        $('#dpEduYearCompletion').append($('<option></option>').val('').html('--Select--'));
        for(var y=year;y>year-50;y--){

            $('#dpEduYearCompletion').append($('<option></option>').val(y).html(y));
        }
        $('#dpEduYearCompletion').material_select();
        if (Id == 0)
        {
            $("#myModalLabel").html("Create Education information Details");
            getLeadInfoPostQualifications('','');
            $('#txtCourseDropdown').show();
            $('#txtCourseText').hide();
        } else
        {
            $("#myModalLabel").html("Edit Education information Details");
            var ajaxurl = API_URL + 'index.php/LeadsInfo/getEducationInfoById';
            var params = {EducationInfoId: Id};
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: ManageEnducationalInforesponse});
        }
        $("#hdnEducationInfoId").val(Id);
        $(This).attr("data-target", "#EducationModel");
        $(This).attr("data-toggle", "modal");
    }
    function ManageStudentEducationalInfo(This, Id)
    {
        notify('Processing..', 'warning', 10);
        //clearDatePicker("#dpEduYearCompletion");
        $('#ManageEducationalInfoForm')[0].reset();
        $('#ManageEducationalInfoForm label').removeClass("active");
        $('#ManageEducationalInfoForm span.required-msg').remove();
        $('#ManageEducationalInfoForm input').removeClass("required");
        $('#ManageEducationalInfoForm select').removeClass("required");
        $('#txtLeadInfotQualification').empty();
        $('#txtLeadInfotQualification').material_select();
        var userID = $('#hdnUserId').val();
        var currentTime = new Date();
        var year = currentTime.getFullYear();
        $('#dpEduYearCompletion').empty();
        $('#dpEduYearCompletion').append($('<option></option>').val('').html('--Select--'));
        for(var y=year;y>year-50;y--){

            $('#dpEduYearCompletion').append($('<option></option>').val(y).html(y));
        }
        $('#dpEduYearCompletion').material_select();
        if (Id == 0)
        {
            $("#myModalLabel").html("Create Education information Details");
            getLeadInfoPostQualifications('','');
            $('#txtCourseDropdown').show();
            $('#txtCourseText').hide();
        } else
        {
            $("#myModalLabel").html("Edit Education information Details");
            var ajaxurl = API_URL + 'index.php/LeadsInfo/getEducationInfoById';
            var params = {EducationInfoId: Id};
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: ManageEnducationalInforesponse});
        }
        alertify.dismissAll();
        $("#hdnEducationInfoId").val(Id);
        $(This).attr("data-target", "#EducationModel");
        $(This).attr("data-toggle", "modal");
    }
    function ManageEnducationalInforesponse(response)
    {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var InfoData = response['data'];
                if(InfoData[0]['fk_course_id']!=null && InfoData[0]['fk_course_id']>0){
                $('#txtCourseText').hide();
                $('#txtCourseDropdown').show();
                //getLeadInfoQualifications(InfoData[0]['fk_course_id']);
                getLeadInfoPostQualifications(InfoData[0]['fk_course_parent_id'],InfoData[0]['fk_course_id']);
            }
            else
            {
                $('#txtCourseDropdown').hide();
                $('#txtCourseText').show();
            }
            if(InfoData[0]['course']!=null)
            {
                $("#txtEduName").val(InfoData[0]['course']);
                $("#txtEduName").next('label').addClass("active");
            }
            if(InfoData[0]['university']!=null)
            {
                $("#txtEduUniversity").val(InfoData[0]['university']);
                $("#txtEduUniversity").next('label').addClass("active");
            }
            if(InfoData[0]['percentage']!=null)
            {
                $("#txtEduPercentage").val(InfoData[0]['percentage']);
                $("#txtEduPercentage").next('label').addClass("active");
            }
            if(InfoData[0]['comments']!=null)
            {
                $("#txaEduComments").val(InfoData[0]['comments']);
                $("#txaEduComments").next('label').addClass("active");
            }
            if(InfoData[0]['year_of_completion']!=null && InfoData[0]['year_of_completion']!=0)
            {
                $("#dpEduYearCompletion").val(InfoData[0]['year_of_completion']);
                $('#dpEduYearCompletion').material_select();
            }
        }
    }

    function ManagePersonalInfo(This) {
        notify('Processing..', 'warning', 10);
        $('#ManageLeadsForm span.required-msg').remove();
        $('#ManageLeadsForm input,#ManageLeadsForm textarea').removeClass("required");
        $('#ManageLeadsForm select').removeClass("required");
        leadInfoPageLoad();
    }

    function SaveEducationalInfoDetails(This)
    {
        if (requestRunning) { // don't do anything if an AJAX request is pending
            return;
        }
        requestRunning = true;
        var leadId = $("#hdnLeadId").val();
        var Name = $("#txtEduName").val().trim();
        var University = $("#txtEduUniversity").val().trim();
        var Percentage = $("#txtEduPercentage").val();
        var YearOfCompletion = $("#dpEduYearCompletion").val();
        var Comments = $("#txaEduComments").val();
        var hdnEduId = $("#hdnEducationInfoId").val();
        var userID = $("#hdnUserId").val();
        var RegexName = /^(\w+\s)*\w+$/;
        var RegexNumbers = /^[0-9]+$/;
        var RegexPercentage=/^(\d\d?(\.\d\d?)?|100(\.00?)?)$/;

        $('#ManageEducationalInfoForm span.required-msg').remove();
        $('#ManageEducationalInfoForm input').removeClass("required");
        $('#ManageEducationalInfoForm textarea').removeClass("required");
        var LeadInfoPostQualification = $("#txtLeadInfoPostQualification option:selected").val();
        var LeadInfoQualification = $("#txtLeadInfotQualification option:selected").val();
        var LeadInfoQualificationText = $("#txtLeadInfotQualification option:selected").text();
        var YearOfCompletion = $("#dpEduYearCompletion option:selected").val();
        var flag = 0;
        var checkDropdown=0;
        var selDropDown=0;
        if($('#txtCourseDropdown').css('display')=='block'){
            checkDropdown=1;
            Name=LeadInfoQualificationText;
            selDropDown=LeadInfoQualification;
        }
        if (checkDropdown==0 && Name == '')
        {
            $("#txtEduName").addClass("required");
            $("#txtEduName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (checkDropdown==1 && LeadInfoPostQualification == '') {
            $("#txtLeadInfoPostQualification").parent().find('.select-dropdown').addClass("required");
            $("#txtLeadInfoPostQualification").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (checkDropdown==1 && LeadInfoQualification == '') {
            $("#txtLeadInfotQualification").parent().find('.select-dropdown').addClass("required");
            $("#txtLeadInfotQualification").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        /*else if (RegexName.test(Name) === false)
        {
            $("#txtEduName").addClass("required");
            $("#txtEduName").after('<span class="required-msg">Accepts only alphanumeric with spaces</span>');
            flag = 1;
        }*/

        if (University == "")
        {

            $("#txtEduUniversity").addClass("required");
            $("#txtEduUniversity").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        /*else if (RegexName.test(University) === false)
        {
            $("#txtEduUniversity").addClass("required");
            $("#txtEduUniversity").after('<span class="required-msg">Accepts only alphanumeric with spaces</span>');
            flag = 1;
        }*/
        if (Percentage == "")
        {
            $("#txtEduPercentage").addClass("required");
            $("#txtEduPercentage").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (RegexPercentage.test(Percentage) === false)
        {
            $("#txtEduPercentage").addClass("required");
            $("#txtEduPercentage").after('<span class="required-msg">Invalid Percentage</span>');
            flag = 1;
        }


        /*if (YearOfCompletion == "")
        {
            $("#dpEduYearCompletion").parent().find('.select-dropdown').addClass("required");
            $("#dpEduYearCompletion").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }*/
        if (flag == 0)
        {
            notify('Processing..', 'warning', 10);
            var ajaxurl = '';
            var params = {};
            var headerParams = {};
            if (hdnEduId == 0)
            {
                ajaxurl = API_URL + 'index.php/LeadsInfo/addEducationalInfoDetails';
                params = {Name: Name, University: University, Percentage: Percentage, YearOfCompletion: YearOfCompletion, Comments: Comments, LeadID: leadId, UserID: userID,selDropDown:selDropDown};
                headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: This, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveInfoResponse});
            }
            else
            {

                ajaxurl = API_URL + 'index.php/LeadsInfo/UpdateEducationalInfo';
                params = {Name: Name, University: University, Percentage: Percentage, YearOfCompletion: YearOfCompletion, Comments: Comments, LeadID: leadId, UserID: userID, EducationId: hdnEduId,selDropDown:selDropDown};
                var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: This, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: SaveInfoResponse});
            }
        }
        else
        {
            requestRunning = false;
            return false;
        }
    }
    function SaveInfoResponse(response)
    {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false)
        {
            requestRunning = false;
            notify(response.message, 'error', 10);
        }
        else
        {
            $('#ManageEducationalInfoForm')[0].reset();
            $('#ManageEducationalInfoForm label').removeClass("active");
            $('#ManageEducationalInfoForm span.required-msg').remove();
            $('#ManageEducationalInfoForm input').removeClass("required");
            $('#ManageEducationalInfoForm select').removeClass("required");
            $('#txtLeadInfotQualification').empty();
            $('#txtLeadInfotQualification').material_select();
            notify(response.message, 'success', 10);
            $('#EducationModel').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            requestRunning = false;
            leadInfoPageLoad();
        }
    }
    function DeleteEducatonalInfoById(This, id) {
        var InfoId = id;
        var userID = $("#hdnUserId").val();

        $("#popup_confirm #btnTrue").attr('onClick','DeleteEducatonalInfoByIdFinal(this,'+id+')');
        var ModelTitle="Delete Qualification";
        var Description='Are you sure want to delete this Qualification ?';
        customConfirmAlert(ModelTitle,Description);

    }

    function DeleteEducatonalInfoByIdFinal(This, id) {
    var InfoId = id;
    var userID = $("#hdnUserId").val();

            var ajaxurl = API_URL + 'index.php/LeadsInfo/deleteEducationalInfoData';
            var params = {EduInfoId: InfoId};
            var headerParams = {action: 'delete', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'delete', onSuccess: DeleteEducationInfoDataResponse});

    }
    function DeleteEducationInfoDataResponse(response)
    {
        alertify.dismissAll();
        if (response.status === true)
        {

            notify(response.message, 'success', 10);
            $('#myModal').modal('hide');
            $('#popup_confirm').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            leadInfoPageLoad();
        } else
        {

            notify(response.message, 'error', 10);
        }
    }
    var requestRunningSaveOtherInfo=false;
    function SaveOtherInfo(This) {
        if(requestRunningSaveOtherInfo){
            return ;
        }
        requestRunningSaveOtherInfo=true;
        var leadId = $("#hdnLeadId").val();
        var branch_xref_lead_id = $("#hdnBranchXrefId").val();
        var userID = $("#hdnUserId").val();
        if ($('#chkSignificanceOfCPA').prop('checked') == true) {
            var SignificanceOfCPA = 1;
        } else {
            SignificanceOfCPA = 0;
        }
        if ($('#chkPursueTheCPA').prop('checked') == true) {
            var PursueTheCPA = 1;
        } else {
            PursueTheCPA = 0;
        }
        if ($('#chkMilesCPA').prop('checked') == true) {
            var MilesCPA = 1;
        } else {
            MilesCPA = 0;
        }
        if ($('#chkValueAddition').prop('checked') == true) {
            var ValueAddition = 1;
        } else {
            ValueAddition = 0;
        }
        if ($('#chkCmaNo').prop('checked') == true) {
            var CmaNo = 1;
        } else {
            CmaNo = 0;
        }
        notify('Processing..', 'warning', 10);
        /*var otherInfoEligibility=$('#otherInfoEligibility').val().trim();*/
        var ajaxurl = API_URL + 'index.php/LeadsInfo/addOtherInfo';
        /*var params = {CpaCourse: SignificanceOfCPA, CpaDesignation: PursueTheCPA, CpaReview: MilesCPA, Presentation: ValueAddition, CmaNumber: CmaNo, leadId: leadId, UserID: userID,otherInfoEligibility:otherInfoEligibility,branch_xref_lead_id:branch_xref_lead_id};*/
        var params = {CpaCourse: SignificanceOfCPA, CpaDesignation: PursueTheCPA, CpaReview: MilesCPA, Presentation: ValueAddition, CmaNumber: CmaNo, leadId: leadId, UserID: userID,branch_xref_lead_id:branch_xref_lead_id};
        var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveOthersInfoResponse});


    }
    function SaveOthersInfoResponse(response) {
        alertify.dismissAll();
        requestRunningSaveOtherInfo=false;
        if (response.status === true)
        {
            notify(response.message, 'success', 10);
            leadInfoPageLoad();
        } else
        {
            notify(response.message, 'error', 10);
        }

    }
    /*Leads End*/

//change default branch of the user//
    function changeDefaultBranch() {
        var branchId = $('#userBranchList').val();
        if (branchId != '') {
            var userID = $('#hdnUserId').val();
            var ajaxurl = API_URL + 'index.php/User/updateUserDefaultBranch';
            var params = {branchId: branchId};
            var action = 'list';
            var type = "POST";
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: userID
            };
            var response =
                    commonAjaxCall({
                        This: this,
                        method: type,
                        requestUrl: ajaxurl,
                        params: params,
                        headerParams: headerParams,
                        action: action,
                        onSuccess: changeDefaultBranchResponse
                    });
        }
    }
    function changeDefaultBranchResponse(response)
    {
        var branchRawData = $.parseJSON(localStorage.getItem("Branches"));
        if(branchRawData.length > 0 )
        {
            for (var b = 0; b < branchRawData.length; b++) {
                if($('#userBranchList').val()==branchRawData[b]['branch_id']){
                    branchRawData[b]['is_default_branch']=1;
                }
                else{
                    branchRawData[b]['is_default_branch']=0;
                }

            }

        }
        localStorage.setItem("Branches", JSON.stringify(branchRawData));
    }
/*fee assigns start*/
$('#clasRoom').on('change', function () {
    showFeeStructureInfo("","");
    //$("#ddlFeetypes").val(0);
    //$('#ddlFeetypes').material_select();
    $("#tabs").hide();
});
$('#online').on('change', function () {
    showFeeStructureInfo("","");
    //$("#ddlFeetypes").val(0);
    //$('#ddlFeetypes').material_select();
    $("#tabs").hide();
});
/* fee assign from lead  */
function ManageLeadsFeeAssign(This,Id,courseId){
    ManageFeeAssign(This);
    $("#txtLeadNumber").val(Id);
    $("#hdnCourseId").val(courseId);
    showLeadInfo(This);
    $("#FeeassignLeadNumber").hide();
}
/* fee assign from lead  */
function ManageFeeAssign(This){
    $("#LeadData").hide();
    $('#ManageFeeAssignForm')[0].reset();
    $('#ManageFeeAssignForm label').removeClass("active");
    $('#ManageFeeAssignForm span.required-msg').remove();
    $('#ManageFeeAssignForm input').removeClass("required");
    $('#ManageFeeAssignForm select').removeClass("required");

    $("select[name=ddlFeetypes]").val(0);
    $('#ddlFeetypes').material_select();
    $(This).attr("data-target", "#FeeAssign");
    $(This).attr("data-toggle", "modal");


}
function feeassignReset(){
    $('#ddlFeetypes').find('option:gt(0)').remove();

    var userId = $('#hdnUserId').val();
    var action = 'list';
    var headerParams = {action: action, context: 'default', serviceurl: '', pageurl: '', Authorizationtoken: $accessToken, user: userId};
    $.when(
        commonAjaxCall
        (
            {
                This: this,
                headerParams: headerParams,
                requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                params: {name: 'Fee Type'},
                action: action
            }
        ),
        commonAjaxCall
        (
            {
                This: this,
                headerParams: headerParams,
                requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                params: {name: 'Training Type'},
                action: action
            }
        )
    ).then(function (response2,response3) {
        var feeTypeData = response2[0];
        var trainingTypeData = response3[0];
        trainingTypeDropDown(trainingTypeData);
        if (feeTypeData.data.length == 0 || feeTypeData == -1 || feeTypeData['status'] == false) {

        } else {
            var branchRawData = feeTypeData.data.reverse();
            for (var b = 0; b < branchRawData.length; b++) {
                var o = $('<option/>', {value: branchRawData[b]['reference_type_value_id']})
                    .text(branchRawData[b]['value']);
                o.appendTo('#ddlFeetypes');
            }
            $('#ddlFeetypes').material_select();
        }
    });
    /*var r = $('<option/>', {value:"11"}).text("Retail");
    var c = $('<option/>', {value:"12"}).text("Corporate");
    var g = $('<option/>', {value:"13"}).text("Group");
    r.appendTo('#ddlFeetypes');
    c.appendTo('#ddlFeetypes');
    g.appendTo('#ddlFeetypes');*/
    $("#feetypes").show();
    $('#ddlFeetypes').material_select();
}
function showLeadInfo(This)
{
    var userID = $('#hdnUserId').val();
    var leadNumber=$("#txtLeadNumber").val();
    var courseId=$("#hdnCourseId").val();
    var RegexNumbers = /^[0-9]+$/;
    feeassignReset();
    $("#ddlFeetypes").attr("disabled",false);
    $("#Trainingtype").attr("disabled",false);
    $('#ManageFeeAssignForm span.required-msg').remove();
    $('#ManageFeeAssignForm input').removeClass("required");
    $('#ManageFeeAssignForm textarea').removeClass("required");
    $("#online").attr("disabled",false);
    $("#clasRoom").attr("disabled",false);

    $("#ShowfeeDetails").hide();
    var flag=0;
    if(leadNumber == '')
    {
        $("#txtLeadNumber").addClass("required");
        $("#txtLeadNumber").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
        flag=1;
    }
    else if (RegexNumbers.test(leadNumber) === false)
    {
        $("#txtLeadNumber").addClass("required");
        $("#txtLeadNumber").after('<span class="required-msg">Accepts Numbers only</span>');
        flag = 1;
    }

    if(flag==0){
        var ajaxurl = API_URL + 'index.php/FeeAssign/getLeadInfoByLeadNumber';
        var params = {UserID: userID,LeadNumber:leadNumber,branch:$('#userBranchList').val(),courseId:courseId};
        var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: GetLeadInfoResponse});
    }

}
function GetLeadInfoResponse(response)
{
    alertify.dismissAll();
    if (response == -1 || response['status'] == false)
    {
        var id = '', flag = 0;
        notify(response.message, 'error', 10);
        $("#LeadData").hide();
        for (var a in response.data)
        {
            id = '#' + a;
            if (a == 'name')
            {
                $("#txtLeadNumber").addClass("required");
                $("#txtLeadNumber").after('<span class="required-msg">' + response.data[a] + '</span>');
            } else
            {
                $(id).addClass("required");
                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
            }
        }
    }
    else
    {
        $("#LeadData").show();
        $("#CourseList").hide();
        $("#AssignNewFee").hide();
        var LeadInfo = response['data'];
        var leads="";
        var Company = (LeadInfo[0]['company'] == null || LeadInfo[0]['company'] == '') ? "---" :LeadInfo[0]['company'];
        var Course = (LeadInfo[0]['course'] == null || LeadInfo[0]['course'] == '') ? "---" :LeadInfo[0]['course'];
        var institute = (LeadInfo[0]['institute'] == null || LeadInfo[0]['institute'] == '') ? "---" :LeadInfo[0]['institute'];
        var phone = (LeadInfo[0]['phone'] == null || LeadInfo[0]['phone'] == '') ? "---" :LeadInfo[0]['phone'];
        var email = (LeadInfo[0]['email'] == null || LeadInfo[0]['email'] == '') ? "---" :LeadInfo[0]['email'];
        var qualification = (LeadInfo[0]['qualification'] == null || LeadInfo[0]['qualification'] == '') ? "---" :LeadInfo[0]['qualification'];
        leads+='<div class="col-sm-12 p0"><div class="light-blue3-bg clearfix">'
            +'<div class="col-sm-6 mb5"><div class="input-field data-head-name"><span class="display-block ash">Name</span>'
            +'<p>'+LeadInfo[0]['name']+'</p></div></div>'
            +'<div class="col-sm-6 mb5"><div class="input-field data-head-name"><span class="display-block ash">Phone</span>'
            +'<p>'+phone+'</p></div></div>'
            +'<div class="col-sm-6 mb5"><div class="input-field data-head-name"><span class="display-block ash">Company</span>'
            + '<p>' + Company + '</p></div></div>'
            +'<div class="col-sm-6 mb5"><div class="input-field data-head-name"><span class="display-block ash">Course</span>'
            + '<p>' + Course + '</p></div></div>'
            +'<div class="col-sm-6 mb5"><div class="input-field data-head-name"><span class="display-block ash">Email</span><p>'+email+'</p></div></div>'
            +'<div class="col-sm-6 mb5"><div class="input-field data-head-name"><span class="display-block ash">Institute</span><p>'+institute+'</p></div></div>'
            +'<div class="col-sm-6 mb5"><div class="input-field data-head-name"><span class="display-block ash">Qualification</span><p>'+qualification+'</p></div></div>'
            +'<div class="col-sm-6 mb5"><div class="input-field data-head-name"><span class="display-block ash">Branch</span><p>'+LeadInfo[0]['branch']+'</p></div></div></div></div>';
        $("#leadInfoData").html(leads);
        $("#hdnBranchId").val(LeadInfo[0]['branch_id']);
        $("#hdnBranchxceflead").val(LeadInfo[0]['branch_xref_lead_id']);
        $("#typeOfClass").show();
        $("#hdnCourseId").val(LeadInfo[0]['fk_course_id']);
        $('input:checkbox').removeAttr('checked');
        $('#ddlCourse').val(LeadInfo[0]['fk_course_id']);
        $('#ddlCourse').material_select();
        $("#DataMessage").hide();

        if(LeadInfo[0]['fk_fee_structure_id'] != null && (LeadInfo[0]['is_regular_fee']!=null || LeadInfo[0]['is_regular_fee']!=0))
        {
            if(LeadInfo[0]['fk_type_id']==11 || LeadInfo[0]['fk_type_id']==12 || LeadInfo[0]['fk_type_id']==13)
            {
                $("#ddlFeetypes").val(LeadInfo[0]['fk_type_id']);
                $("#ddlFeetypes").attr("disabled",true);
                $('#ddlFeetypes').material_select(); $("#feetypes").show();
                $("#ddlFeetypes").next('label').addClass("active");
                $("#typeOfClass").hide();
            }
            $("#Trainingtype").val(LeadInfo[0]['fk_training_type']);
            $("#Trainingtype").attr("disabled",true);
            $("#feetypes").hide();
            $('#Trainingtype').material_select();
            $("#Trainingtype").next('label').addClass("active");
            $("#saveButton").attr("disabled",true);
            $("#closeButton").attr("disabled",true);
            $("#DataMessage").show();
            $("#DataMessage").html("<b style='color:red;'>Already fee assigned to this lead</b>");
            $("#AssignNewFee").show();
            var Amounts = "";
            Amounts += '<div class="col-sm-12">'
                + '<table class="table table-responsive table-striped table-custom" id="feeassign-tb-list">'
                + '<thead><tr>'
                + '<th>Fee Head</th><th>Due days</th><th>Amount</th></tr>'
                + '</thead><tbody>';

            if (LeadInfo[0]['ammounts'] != "")
            {
                var rows = LeadInfo[0]['ammounts'];

                var total = 0;
                for (var i = 0; i < rows.length; i++) {
                    Amounts += '<tr>'
                        + '<td>' + rows[i]['feeHeadName'] + '</td>'
                        + '<td>' + rows[i]['due_days'] + '</td>'
                        + '<td>' + rows[i]['amount_payable'] + '</td>'

                    total += parseInt(rows[i]['amount_payable']);
                }
                Amounts += '<tr><td></td><td>Total</td></td><td>' + total + '</td></tr>';

            }

            Amounts += '</tbody></table></div>';
            $("#tabs").show();
            $("#tabs").html(Amounts);
        }
        else
        {

            $("#tabs").hide();
            $("#buttons").show();

            if(LeadInfo[0]['contact_type']=="Retail"){
                $("#typeOfFee").val("Group");
            }
            if(LeadInfo[0]['contact_type']=="Corporate"){
                $("#typeOfFee").val("Corporate");
                $("#company").val(LeadInfo[0]['fk_company_id']);

            }
            if(LeadInfo[0]['contact_type']=="University"){
                $("#typeOfFee").val("Institution");
                $("#Institutional").val(LeadInfo[0]['inisti_id']);

            }
            if(LeadInfo[0]['contact_type']=="Retail"){
                $("#feetypes").show();
                getDropDownFeeTypeByTextFilter('Retail','#ddlFeetypes');
            }

            if(LeadInfo[0]['contact_type']=="Corporate"){
                getDropDownFeeTypeByTextFilter('Retail,Corporate','#ddlFeetypes');
                $("#feetypes").show();
            }

            if(LeadInfo[0]['contact_type']=="University"){
                getDropDownFeeTypeByTextFilter('Retail,Group','#ddlFeetypes');
                $("#feetypes").show();
            }
        }
    }
}

$('#ddlFeetypes').on('change', function () {
    var mystructureVal = $(this).val();
    showFeeStructureInfo(this, '');
});

function showFeeStructureInfo(This, selectedVal)
{

    var course=$("#hdnCourseId").val();
    var branch=$("#hdnBranchId").val();
    //var classRoom = $('#clasRoom').is(":checked") ? '1' : '0';
    //var online = $('#online').is(":checked") ? '1' : '0';
    var userID = $('#hdnUserId').val();
    var trainingType=$("#Trainingtype").val();
    var feeType=$("#ddlFeetypes").val();
    var corporate=$("#company").val();
    var groupvalue=$("#Institutional").val();
    $("#tabs").hide();
    $('#ManageFeeAssignForm span.required-msg').remove();
    $('#ManageFeeAssignForm input').removeClass("required");
    $('#ManageFeeAssignForm span.required-msg').remove();
    $('#ManageFeeAssignForm select').removeClass("required");

    var flag=0;

    if ((trainingType == '' || trainingType == null))
    {
        $("#Trainingtype").parent().find('.select-dropdown').addClass("required");
        $("#Trainingtype").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if ((feeType == '' || feeType == null))
    {
        $("#ddlFeetypes").parent().find('.select-dropdown').addClass("required");
        $("#ddlFeetypes").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }

    if (feeType == '13')
    {
        if (groupvalue == '' || groupvalue == null)
        {
            $("#groupvalue").parent().find('.select-dropdown').addClass("required");
            $("#groupvalue").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
    }
    else if (feeType == '12')
    {
        if (corporate == '' || corporate == null)
        {
            $("#corporate").parent().find('.select-dropdown').addClass("required");
            $("#corporate").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
    }

    if(flag==0){

        if (feeType == '13')
        {
            var data = {
                branch:branch,
                course:course,
                feeType:feeType,
                groupvalue:groupvalue,
                UserID:userID,
                trainingType:trainingType
            };
        }
        else if (feeType == '12' && corporate != "" && corporate != null)
        {
            var data = {
                branch:branch,
                course:course,
                feeType:feeType,
                corporate:corporate,
                UserID:userID,
                trainingType:trainingType
            };
        }
        else if (feeType == '11')
        {
            var data = {
                branch:branch,
                course:course,
                feeType:feeType,
                UserID:userID,
                trainingType:trainingType
            };

        }
        var ajaxurl = API_URL + 'index.php/FeeAssign/getFeeStructureByLead';
        var params = data;
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: GetFeeStructureInfoResponse});

    }

}
function GetFeeStructureInfoResponse(response){
    alertify.dismissAll();
    if (response == -1 || response['status'] == false)
    {
        $("#buttons").show();
        $("#tabs").hide();
        $("#saveButton").attr('disabled',true);
        notify('No fee structure found for this lead', 'error', 10);
    }
    else
    {
        var feeStructureInfo = response['data'];

        $("#hdnFeeStructureId").val(feeStructureInfo[0]['fee_structure_id']);


        if(feeStructureInfo[0]['fk_type_id']==11 || feeStructureInfo[0]['fk_type_id']==12 || feeStructureInfo[0]['fk_type_id']==13)
        {

            $("#ddlFeetypes").val(feeStructureInfo[0]['fk_type_id']);
            $('#ddlFeetypes').material_select();
            $("#ddlFeetypes").next('label').addClass("active"); $("#feetypes").show();
        }

        $("#feeStructureId").val(feeStructureInfo[0]['fee_structure_id']);
        var Amounts="";
        Amounts+='<div class="col-sm-12 p0">'
            +'<table class="table table-responsive table-striped table-custom" id="feeassign-tb-list">'
            +'<thead><tr>'
            +'<th>Fee Head</th><th>Due Days</th><th>Amount</th></tr>'
            +'</thead><tbody>';

        if(feeStructureInfo[0]['ammounts']!="")
        {
            var rows=feeStructureInfo[0]['ammounts'];
            var total=0;

            for(var i=0;i<rows.length;i++){

                Amounts+='<tr>'
                    +'<td>'+rows[i]['fee_head_name']+'</td>'
                    +'<td>'+rows[i]['due_days']+'</td>'
                    +'<td>'+rows[i]['amount']+'</td></tr>';
                total+= parseInt(rows[i]['amount']);
            }

            Amounts+='<tr><td></td><td>Total</td></td><td>'+total+'</td></tr>';


        }
        Amounts+='</tbody></table></div>';
        $("#saveButton").removeAttr('disabled',false);
        $("#tabs").show();
        $("#tabs").html(Amounts);

    }


}

function SaveCandidateDetails(This)
{
    var brachxcerflead= $("#hdnBranchxceflead").val();
    var FeeStructureId= $("#feeStructureId").val();
    var userID = $('#hdnUserId').val();
    alertify.dismissAll();
    notify('Processing..', 'warning', 50);
    var ajaxurl = API_URL + 'index.php/FeeAssign/AddCandidate';
    var params = {brachxcerflead:brachxcerflead,FeeStructureId:FeeStructureId,userID:userID};
    var headerParams = {action:'add',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
    commonAjaxCall({This:this,method:'POST',requestUrl:ajaxurl,params:params,headerParams:headerParams,action:'add',onSuccess:SaveCandidateResponse});
}
function SaveCandidateResponse(response)
{
    alertify.dismissAll();
    if (response.status === true)
    {
        $('#FeeAssign').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        notify(response.message, 'success', 10);
        if(typeof buildFeeAssignListDataTable == 'function'){
            buildFeeAssignListDataTable();
        }
        if(typeof buildLeadsDataTable == 'function'){
            buildLeadsDataTable();
            getQuickLeadCourses();
            ShowLeadsCount();
        }
    }
    else
    {

        notify(response.message, 'error', 10);
    }

}
function saveLeadStages(This){
    var flag = 0;
    var regx_company_code = /^[0-9]+$/;
    $('#lead_stages span.required-msg').remove();
    $('#lead_stages input').removeClass("required");
    $('input[name="leadsStage"]').each(function()
    {
        if($(this).val()=="")
        {
            $('#'+this.id).addClass("required");
            //$('#'+this.id).after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag=1;
        }
        else if (regx_company_code.test($(this).val()) === false)
        {
            $('#'+this.id).addClass("required");
            // $('#'+this.id).after('<span class="required-msg">Number only</span>');
            flag = 1;
        }

    });
    var leadsConversionRate=[];
    $('input[name="leadsConversionRate"]').each(function()
    {
        if($(this).val()!='' && $(this).val()>100)
        {
            $('#'+this.id).addClass("required");
            $('#'+this.id).after('<span class="required-msg"><100</span>');
            flag=1;
        }
        else if ($(this).val()!='' && $(this).val()<=100)
        {
            var leadsConversionRateStr=this.id.split('_');
            leadsConversionRate.push(leadsConversionRateStr[1]+'#'+$(this).val());
        }

    });
    leadsConversionRate=leadsConversionRate.join('|');
    //console.log(leadsConversionRate);
    //return false;

    var m6=$("#txaM618").val()+'###'+$("#txtM618").val()+'###'+$("#hidM618").val();
    var m5=$("#txaM517").val()+'###'+$("#txtM517").val()+'###'+$("#hidM517").val();
    var m4=$("#txaM416").val()+'###'+$("#txtM416").val()+'###'+$("#hidM416").val();
    var M3plus=$("#txaM3plus15").val()+'###'+$("#txtM3plus15").val()+'###'+$("#hidM3plus15").val();
    var m3=$("#txaM314").val()+'###'+$("#txtM314").val()+'###'+$("#hidM314").val();
    var m2=$("#txaM213").val()+'###'+$("#txtM213").val()+'###'+$("#hidM213").val();
    var L6=$("#txaL611").val()+'###'+$("#txtL611").val()+'###'+$("#hidL611").val();
    var L5=$("#txaL510").val()+'###'+$("#txtL510").val()+'###'+$("#hidL510").val();
    var L4=$("#txaL49").val()+'###'+$("#txtL49").val()+'###'+$("#hidL49").val();
    var L3plus=$("#txaL3plus6").val()+'###'+$("#txtL3plus6").val()+'###'+$("#hidL3plus6").val();
    var L3=$("#txaL35").val()+'###'+$("#txtL35").val()+'###'+$("#hidL35").val();
    var L2=$("#txaL24").val()+'###'+$("#txtL24").val()+'###'+$("#hidL24").val();
    var L0=$("#txaL01").val()+'###'+$("#txtL01").val()+'###'+$("#hidL01").val();
    var L1=$("#txaL12").val()+'###'+$("#txtL12").val()+'###'+$("#hidL12").val();
    var L2min=$("#txaL2-3").val()+'###'+$("#txtL2-3").val()+'###'+$("#hidL2-3").val();
    var L4min=$("#txaL4--7").val()+'###'+$("#txtL4--7").val()+'###'+$("#hidL4--7").val();
    var L4mins=$("#txaL4-8").val()+'###'+$("#txtL4-8").val()+'###'+$("#hidL4-8").val();
    var Ent=$("#txaENT12").val()+'###'+$("#txtENT12").val()+'###'+$("#hidENT12").val();
    var Os=$("#txaOS19").val()+'###'+$("#txtOS19").val()+'###'+$("#hidOS19").val();
    var userID = $('#hdnUserId').val();

    if(flag==0){
        var ajaxurl = API_URL + 'index.php/CallOrderConfirmation/saveLeadStages';
        var params = {m6:m6,m5:m5,m4:m4,M3plus:M3plus,m3:m3,m2:m2,L6:L6,L5:L5,L4:L4,L3plus:L3plus,L3:L3,L2:L2,L0:L0,L1:L1,L2min:L2min,L4min:L4min,L4mins:L4mins,Ent:Ent,Os:Os,userID:userID,leadsConversionRate:leadsConversionRate};
        var headerParams = {action:'add',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This:this,method:'POST',requestUrl:ajaxurl,params:params,headerParams:headerParams,action:'add',onSuccess:SaveLeadStagesResponse});
    }



}
function SaveLeadStagesResponse(response){
    alertify.dismissAll();
    if (response.status === false) {
        notify(response.message, 'error', 10);
    } else {
        notify(response.message, 'success', 10);
        leadLeadStagesLoad();
    }
}
/*fee assigns end*/

//Fee collection starts
//$( "#ShowLeadFeeDetails" ).click(function()
function showLeadFeeDetails()
{
    $('#ManageFeeCollectionForm span.required-msg').remove();
    $('#ManageFeeCollectionForm input').removeClass("required");
    var lead_number=$("#txtLeadNumber").val();
    var lead_course=$("#hdnLeadCourse").val();
    var num_regex = /[^0-9]/g;
    var flag=0;
    var feeCollectionType=$('#feeCollectionType').val();
    if(lead_number == '')
    {
        $("#txtLeadNumber").addClass("required");
        $("#txtLeadNumber").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag=1;
    }
    else if ( num_regex.test(lead_number) )
    {
        $("#txtLeadNumber").addClass("required");
        $("#txtLeadNumber").after('<span class="required-msg"> Accepts only numbers</span>');
        flag=1;
    }
    else if(lead_number == 0)
    {
        $("#txtLeadNumber").addClass("required");
        $("#txtLeadNumber").after('<span class="required-msg"> Cannot be zero</span>');
        flag=1;
    }

    if(flag == 0)
    {
        if(feeCollectionType==1)
        {
            var ajaxurl = API_URL + 'index.php/FeeCollection/getBranchLeadInfoByLeadNumber';
            var branchID=$('#userBranchList').val();
            var params = {branchID: branchID,leadNumber: lead_number,leadCourseId:lead_course};
            var userID = $('#hdnUserId').val();
            var action = 'fee collect';
            var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: GetLeadInformationDetails});
        }
        else if(feeCollectionType==2)
        {
            var ajaxurl = API_URL + 'index.php/FeeCollection/getLeadFeeInfoDetailsUnassignedFee';
            var branchID=$('#userBranchList').val();
            var params = {branchID: branchID,leadNumber: lead_number,leadCourseId:lead_course};
            var userID = $('#hdnUserId').val();
            var action = 'fee collect';
            var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: GetLeadInformationDetailsUnassignedFee});
        }

    }
}

function GetLeadInformationDetails(LeadFeeResponse)
{
    alertify.dismissAll();
    if (LeadFeeResponse == -1 || LeadFeeResponse['status'] == false)
    {
        notify(LeadFeeResponse['message'], 'error', 10);
    }
    else
    {
        var LeadInfo=LeadFeeResponse['data']['LeadInfo'];
        var FeeItems=LeadFeeResponse['data']['FeeItems'];
        var Batches=LeadFeeResponse['data']['Batches'][0];
        var BatchesList=LeadFeeResponse['data']['Batches'];

        $("#leadNumber").html(LeadInfo[0]['lead_number']);
        $("#leadName").html(LeadInfo[0]['name']);
        $("#leadPhone").html((LeadInfo[0]['phone']) ? LeadInfo[0]['phone'] : '----');
        $("#leadEmail").html((LeadInfo[0]['email']) ? LeadInfo[0]['email'] : '----');
        //$("#leadBatchNumber").html(LeadInfo[0]['batch']);
        $("#leadCourseName").html((LeadInfo[0]['courseName']) ? LeadInfo[0]['courseName'] : '----');
        $("#leadDueAmount").html('<span class="icon-rupee"></span>'+moneyFormat(parseInt(LeadInfo[0]['amount_payable']-LeadInfo[0]['concession_amount']-LeadInfo[0]['amount_paid'])+parseInt(LeadInfo[0]['refunded_amount'])));
        $("#hdnBranchXrefLeadId").val(LeadInfo[0]['branch_xref_lead_id']);
        $("#hdnCandidateFeeId").val(LeadInfo[0]['candidate_fee_id']);

        if(LeadInfo[0]['fk_batch_id']=="" || LeadInfo[0]['fk_batch_id']==null)
        {
            var BatchListData='';
            BatchListData='<div class="input-field">';
            BatchListData+='<select class="select-dropdown" id="ddlLeadBatchesList" onchange="assignBatchId(this)">';
            BatchListData+='<option value="" disabled selected>--Choose Batch--</option>';
            for(var b=0;b<Batches.length;b++)
            {
                if(Batches[b]['alias_name'] == null || Batches[b]['alias_name'] == '')
                    BatchListData+='<option value="'+Batches[b]['batch_id']+'">'+Batches[b]['code']+'</option>';
                else
                    BatchListData+='<option value="'+Batches[b]['batch_id']+'">'+Batches[b]['code']+'('+Batches[b]["alias_name"]+')</option>';
            }
            BatchListData+='</select>';
            BatchListData+='<label class="select-label">Batch <em>*</em></label>';
            BatchListData+='</div>';
            $("#leadBatch").html(BatchListData);
            $('select#ddlLeadBatchesList').material_select();
        }
        else
        {
            var leadBatchCode;
            if(LeadInfo[0]['alias_name'] == '' || LeadInfo[0]['alias_name'] == null)
                leadBatchCode=LeadInfo[0]['code'];
            else
                leadBatchCode=LeadInfo[0]['code']+"("+LeadInfo[0]['alias_name']+")";
            $("#leadBatch").html(leadBatchCode);
            $("#leadBatch").attr('data-id',LeadInfo[0]['batch_id']);
        }

        var feeitemsList='';
        var flagItem=0;

        for(var i= 0,n=0;i<FeeItems.length;i++)
        {
            for(var j=0;j<FeeItems[i].length;j++)
            {
                feeitemsList='<tr class="">';
                feeItemDate=(FeeItems[i][j]['FeeItem_Date']) ? FeeItems[i][j]['FeeItem_Date'] : '----';
                feeitemsList+='<td>'+feeItemDate+'</td>';
                feeitemsList+='<td>'+FeeItems[i][j]['feeHead']+'</td>';
                if(LeadInfo[0]['fk_batch_id']=="" || LeadInfo[0]['fk_batch_id']==null)
                    feeitemsList+='<td>'+FeeItems[i][j]['feeCompany']+'('+FeeItems[i][j]['courseName']+')</td>';
                else
                {
                    if(LeadInfo[i]['batch_id'] == '' || LeadInfo[i]['batch_id'] == null )
                        feeitemsList+='<td>'+FeeItems[i][j]['feeCompany']+'('+BatchesList[i][0]['code']+')</td>';
                    else
                        feeitemsList+='<td>'+FeeItems[i][j]['feeCompany']+'('+LeadInfo[i]['code']+')</td>';
                }

                Payable=(FeeItems[i][j]['Payable']) ? FeeItems[i][j]['Payable'] : '0';
                Paid=(FeeItems[i][j]['Paid']) ? FeeItems[i][j]['Paid'] : '0';
                Concession=(FeeItems[i][j]['Concession']) ? FeeItems[i][j]['Concession'] : '0';
                Balance=(FeeItems[i][j]['balance']) ? FeeItems[i][j]['balance'] : '0';
                disablePay='';
                if(Balance == 0)
                {
                    flagItem++;
                    disablePay='disabled="disabled"';
                }
                feeitemsList+='<td class="text-right"><span class="icon-rupee"></span>'+moneyFormat(Payable - Concession)+'</td>';
                feeitemsList+='<td class="text-right"><span class="icon-rupee"></span>'+moneyFormat(Paid)+'</td>';
                feeitemsList+='<td class="text-right"><span class="icon-rupee"></span>'+moneyFormat(Balance)+'</td>';
                feeitemsList+='<td class="w150 text-right">';
                feeitemsList+='<input name="PayableAmount['+n+']" type="hidden" value="'+(Payable - Concession)+'">';
                feeitemsList+='<input name="balanceAmount['+n+']" type="hidden" value="'+Balance+'">';
                feeitemsList+='<input name="candidateFeeItemId['+n+']" type="hidden" value="'+FeeItems[i][j]['candidate_fee_item_id']+'">';
                feeitemsList+='<input name="courseId['+n+']" type="hidden" value="'+FeeItems[i][j]['CourseId']+'">';
                feeitemsList+='<input name="branchxrefLeadId['+n+']" type="hidden" value="'+FeeItems[i][j]['branchXrefLeadId']+'">';
                feeitemsList+='<input name="companyId['+n+']" type="hidden" value="'+FeeItems[i][j]['CompanyId']+'">';
                feeitemsList+='<input name="feeCompany['+n+']" type="hidden" value="'+FeeItems[i][j]['feeCompany']+'">';
                feeitemsList+='<input name="feeHeadId['+n+']" type="hidden" value="'+FeeItems[i][j]['feeHeadId']+'">';
                if(FeeItems[i][j]['is_partial_payment_allowed']==1)
                {
                    feeitemsList += '<input name="payingAmount[]" class="calls-schedule-input input-righttext" type="text" maxlength="10" onkeypress="return isNumberKey(event)" '+disablePay+' />';
                }
                else
                {
                    feeitemsList += '<input name="payingAmount[]" class="calls-schedule-input input-righttext" type="hidden" maxlength="10" onkeypress="return isNumberKey(event)" value="'+Balance+'" readonly '+disablePay+'  />';
                    feeitemsList +=moneyFormat(Balance);
                }
                feeitemsList+='</td>';
                feeitemsList+='</tr>';
                $("#FeecollectionItemsList tbody").append(feeitemsList);
                n++;
            }
        }
        if(flagItem==FeeItems.length)
        {
            $("#processPayment").remove();
        }
        $('#proceedWrapper').slideUp();
        $('#feeCollectionCreate-studentDetails').slideDown();
    }

}
function GetLeadInformationDetailsUnassignedFee(LeadFeeResponse)
{
    alertify.dismissAll();
    if (LeadFeeResponse == -1 || LeadFeeResponse['status'] == false)
    {
        notify(LeadFeeResponse['message'], 'error', 10);
    }
    else
    {
        var LeadInfo=LeadFeeResponse['data']['LeadInfo'];
        var FeeStructures=LeadFeeResponse['data']['FeeStructures'];


        $("#leadNumber").html(LeadInfo[0]['lead_number']);
        $("#leadName").html(LeadInfo[0]['name']);
        $("#leadPhone").html((LeadInfo[0]['phone']) ? LeadInfo[0]['phone'] : '----');
        $("#leadEmail").html((LeadInfo[0]['email']) ? LeadInfo[0]['email'] : '----');
        $("#leadCourseName").html((LeadInfo[0]['courseName']) ? LeadInfo[0]['courseName'] : '----');
        $("#hdnBranchXrefLeadId").val(LeadInfo[0]['branch_xref_lead_id']);
        $("#hdnCandidateFeeId").val(LeadInfo[0]['candidate_fee_id']);
        $("#leadDueAmount").html('----');


        if(LeadInfo[0]['fk_batch_id']=="" || LeadInfo[0]['fk_batch_id']==null)
        {
            $("#leadBatch").html('----');
            $("#leadBatch").attr('data-id','0');
        }
        else
        {
            $("#leadBatch").html(LeadInfo[0]['code']);
            $("#leadBatch").attr('data-id',LeadInfo[0]['batch_id']);
        }

        var feeitemsList='';

        $('#feeStructureType').empty();
        $('#feeStructureType').append($('<option selected></option>').val('').html('--Select--'));


        if (FeeStructures.length > 0) {

            $.each(FeeStructures, function (key, value) {

                $('#feeStructureType').append($('<option></option>').val(value.fee_structure_id).html(value.name));
            });
        }

        $('#feeStructureType').material_select();
        $('#feeStructureTypeWrapper').slideDown();
        $('#proceedWrapper').slideUp();
        $('#feeCollectionCreate-studentDetails').slideDown();
        $('#feeCollectionCreate-paymentTable-wrapper').slideUp();


    }
}
function showLeadUnassignedFeeDetails()
{
    $('#ManageFeeCollectionForm span.required-msg').remove();
    $('#ManageFeeCollectionForm input').removeClass("required");
    $('#ManageFeeCollectionForm select').removeClass("required");
    var lead_number=$("#txtLeadNumber").val();
    var flag=0;
    var feeCollectionType=$('#feeCollectionType').val();
    var feeStructureType=$('#feeStructureType').val();
    if(feeStructureType == '')
    {
        $("#feeStructureType").parent().find('.select-dropdown').addClass("required");
        $("#feeStructureType").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag=1;
    }
    if(flag == 0)
    {
        if(feeCollectionType==2){
            var ajaxurl = API_URL + 'index.php/FeeCollection/getBranchLeadInfoByLeadNumberUnassignedFee';
            var branchID=$('#userBranchList').val();
            var params = {branchID: branchID,leadNumber: lead_number,feeStructureType:feeStructureType};
            var userID = $('#hdnUserId').val();
            var action = 'fee collect';
            var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: GetLeadInformationDetailsUnassignedFeeItems});
        }

    }
}
function GetLeadInformationDetailsUnassignedFeeItems(LeadFeeResponse){
    alertify.dismissAll();
    if (LeadFeeResponse == -1 || LeadFeeResponse['status'] == false)
    {
        notify(LeadFeeResponse['message'], 'error', 10);
    }
    else
    {
        var LeadInfo=LeadFeeResponse['data']['LeadInfo'];
        $("#hdnBranchXrefLeadId").val(LeadInfo[0]['branch_xref_lead_id']);
        $("#hdnCandidateFeeId").val(LeadInfo[0]['candidate_fee_id']);
        var FeeItems=LeadFeeResponse['data']['FeeItems'];
        var feeitemsList='';
        $("#FeecollectionItemsList tbody").empty();
        for(var i=0;i<FeeItems.length;i++)
        {
            feeitemsList='<tr class="">';
            feeItemDate=(FeeItems[i]['FeeItem_Date']) ? FeeItems[i]['FeeItem_Date'] : '----';
            feeitemsList+='<td>'+feeItemDate+'</td>';
            feeitemsList+='<td>'+FeeItems[i]['feeHead']+'</td>';
            feeitemsList+='<td>'+FeeItems[i]['feeCompany']+'</td>';
            Payable=(FeeItems[i]['Payable']) ? FeeItems[i]['Payable'] : '0';
            Paid=(FeeItems[i]['Paid']) ? FeeItems[i]['Paid'] : '0';
            Concession=(FeeItems[i]['Concession']) ? FeeItems[i]['Concession'] : '0';
            Balance=(FeeItems[i]['balance']) ? FeeItems[i]['balance'] : '0';
            feeitemsList+='<td class="text-right"><span class="icon-rupee"></span>'+moneyFormat(Payable - Concession)+'</td>';
            feeitemsList+='<td class="text-right"><span class="icon-rupee"></span>'+moneyFormat(Paid)+'</td>';
            feeitemsList+='<td class="text-right"><span class="icon-rupee"></span>'+moneyFormat(Balance)+'</td>';
            feeitemsList+='<td class="w150 text-right">';
            feeitemsList+='<input name="PayableAmount['+i+']" type="hidden" value="'+(Payable - Concession)+'">';
            feeitemsList+='<input name="balanceAmount['+i+']" type="hidden" value="'+Balance+'">';
            feeitemsList+='<input name="candidateFeeItemId['+i+']" type="hidden" value="'+FeeItems[i]['candidate_fee_item_id']+'">';
            feeitemsList+='<input name="is_candidateFeeItemId['+i+']" type="hidden" value="'+FeeItems[i]['is_candidate_fee_item']+'">';
            feeitemsList+='<input name="fee_structure_id['+i+']" type="hidden" value="'+FeeItems[i]['feeStructureType']+'">';
            feeitemsList+='<input name="companyId['+i+']" type="hidden" value="'+FeeItems[i]['CompanyId']+'">';
            feeitemsList+='<input name="feeCompany['+i+']" type="hidden" value="'+FeeItems[i]['feeCompany']+'">';
            feeitemsList+='<input name="feeHeadId['+i+']" type="hidden" value="'+FeeItems[i]['feeHeadId']+'">';
            if(FeeItems[i]['is_partial_payment_allowed']==1)
            {
                feeitemsList += '<input name="payingAmount[]" class="calls-schedule-input input-righttext" type="text" maxlength="10" onkeypress="return isNumberKey(event)" />';
            }
            else
            {
                feeitemsList += '<input name="payingAmount[]" class="calls-schedule-input input-righttext" type="hidden" maxlength="10" onkeypress="return isNumberKey(event)" value="'+Balance+'" readonly />';
                feeitemsList +=moneyFormat(Balance);
            }
            feeitemsList+='</td>';
            feeitemsList+='</tr>';
            $("#FeecollectionItemsList tbody").append(feeitemsList);

            $('select#ddlBatches').material_select();
        }

        $('#feeCollectionCreate-paymentTable-wrapper').slideDown();

    }
}
$( "#processPayment" ).click(function()
{
    alertify.dismissAll();
    var flag = -1,index= 0,bflag=0;
    var payAmounts = [];
    var branchXrefLeadId=$("#hdnBranchXrefLeadId").val();
    var CandidateFeeId=$("#hdnCandidateFeeId").val();
    var payableList='';
    var payableListModel='';
    var feeCollectionType=$('#feeCollectionType').val();
    $("#ddlLeadBatchesList").parent().find('.select-dropdown').removeClass("required");
    $('#ManageFeeCollectionForm span.required-msg').remove();

    if ($('#ddlLeadBatchesList').length >0)
    {
        var batchId = $("#ddlLeadBatchesList").val();
        if(batchId == null || batchId == '')
        {
            bflag=1;
            $("#ddlLeadBatchesList").parent().find('.select-dropdown').addClass("required");
            $("#ddlLeadBatchesList").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            notify('Please choose the batch', 'error', 10);
        }
    }

    if(bflag == 0)
    {
        $('input[name^="payingAmount"]').each(function()
        {
            if(flag == -1)
                $('input').removeClass("required");
            if(($(this).val() == '' || $(this).val()<=0))
            {
                if(flag == -1)
                    flag=1;
                if(flag!=2)
                    $(this).addClass("required");
            }
            else if( (parseInt($(this).val()) > parseInt($('input[name="balanceAmount['+index+']"]').val()) ) )
            {
                flag=2;
                $(this).addClass("required");
            }
            else
            {
                flag=0;
                $(this).removeClass("required");
                if(feeCollectionType==1)
                    payAmounts.push($(this).val()+'@@@@@@'+$('input[name="PayableAmount['+index+']"]').val()+'@@@@@@'+$('input[name="balanceAmount['+index+']"]').val()+'@@@@@@'+$('input[name="candidateFeeItemId['+index+']"]').val()+'@@@@@@'+$('input[name="companyId['+index+']"]').val()+'@@@@@@'+$('input[name="feeCompany['+index+']"]').val()+'@@@@@@'+$('input[name="feeHeadId['+index+']"]').val()+'@@@@@@'+$('input[name="courseId['+index+']"]').val()+'@@@@@@'+$('input[name="branchxrefLeadId['+index+']"]').val());
                else if(feeCollectionType==2)
                    payAmounts.push($(this).val()+'@@@@@@'+$('input[name="PayableAmount['+index+']"]').val()+'@@@@@@'+$('input[name="balanceAmount['+index+']"]').val()+'@@@@@@'+$('input[name="candidateFeeItemId['+index+']"]').val()+'@@@@@@'+$('input[name="companyId['+index+']"]').val()+'@@@@@@'+$('input[name="feeCompany['+index+']"]').val()+'@@@@@@'+$('input[name="feeHeadId['+index+']"]').val()+'@@@@@@'+$('input[name="is_candidateFeeItemId['+index+']"]').val()+'@@@@@@'+$('input[name="fee_structure_id['+index+']"]').val());
            }
            index++;
        });
        if(flag == 1)
        {
            notify('Paying now cannot be empty', 'error', 10);
        }
        else if(flag == 2)
        {
            notify('Paying now cannot be more than balance', 'error', 10);
        }
        else
        {
            $('input').removeClass("required");
            $('#feeStructureTypeWrapper').slideUp();
            //Fee collection inserting logic starts here
            var payableData='',payingAmt='',payableAmt='',balanceAmt='',candidateFeeItemId='',companyId='',companyName='',feeHeadId='',is_candidateFeeItemId='',feeStructureId='',courseId='',feeItemLeadId='';
            for(var p=0;p<payAmounts.length;p++)
            {
                payableData = payAmounts[p].split("@@@@@@");
                payingAmt = payableData[0];
                payableAmt = payableData[1];
                balanceAmt = payableData[2];
                candidateFeeItemId = payableData[3];
                companyId = payableData[4];
                companyName = payableData[5];
                feeHeadId = payableData[6];

                if(feeCollectionType==1)
                {
                    courseId  = payableData[7];
                    feeItemLeadId  = payableData[8];
                }
                else if(feeCollectionType==2)
                {
                    is_candidateFeeItemId = payableData[7];
                    feeStructureId = payableData[8];
                }

                if(p == 0) {
                    if (feeCollectionType == 1)
                        payableList += '<li role="' + companyName + '" class="active" id="' + candidateFeeItemId + '" data-payableAmount="' + payableAmt + '" data-payingAmount="' + payingAmt + '" data-feecompanyId="' + companyId + '" data-branchXrefLeadId="' + branchXrefLeadId + '" data-feeHeadId="' + feeHeadId + '" data-CandidateFeeId="' + CandidateFeeId + '" data-CourseId="'+courseId+'" data-LeadId="'+feeItemLeadId+'" >';
                    if (feeCollectionType == 2)
                        payableList += '<li role="' + companyName + '" class="active" id="' + candidateFeeItemId + '" data-payableAmount="' + payableAmt + '" data-payingAmount="' + payingAmt + '" data-feecompanyId="' + companyId + '" data-branchXrefLeadId="' + branchXrefLeadId + '" data-feeHeadId="' + feeHeadId + '" data-CandidateFeeId="' + CandidateFeeId + '" data-feeStructureId="' + feeStructureId + '" data-IsCandidateFeeId="' + is_candidateFeeItemId + '" >';
                }
                else {
                    if (feeCollectionType == 1)
                        payableList += '<li role="' + companyName + '" id="' + candidateFeeItemId + '" data-payableAmount="' + payableAmt + '" data-payingAmount="' + payingAmt + '" data-feecompanyId="' + companyId + '" data-branchXrefLeadId="' + branchXrefLeadId + '" data-feeHeadId="' + feeHeadId + '" data-CandidateFeeId="' + CandidateFeeId + '" data-CourseId="'+courseId+'" data-LeadId="'+feeItemLeadId+'">';
                    if (feeCollectionType == 2)
                        payableList += '<li role="' + companyName + '" id="' + candidateFeeItemId + '" data-payableAmount="' + payableAmt + '" data-payingAmount="' + payingAmt + '" data-feecompanyId="' + companyId + '" data-branchXrefLeadId="' + branchXrefLeadId + '" data-feeHeadId="' + feeHeadId + '" data-CandidateFeeId="' + CandidateFeeId + '" data-feeStructureId="' + feeStructureId + '" data-IsCandidateFeeId="' + is_candidateFeeItemId + '" >';
                }
                payableList+='<a href="#'+companyName+'_'+candidateFeeItemId+'" aria-controls="'+companyName+'_'+candidateFeeItemId+'" role="tab" data-toggle="tab">';
                payableList+=companyName;
                payableList+='<span class="pull-right">';
                payableList+='<span class="fee-creation-amount">'+moneyFormat(payingAmt)+'</span>';
                payableList+='</span> </a></li>';

                //models for all the companies for which amount is entered
                if(p == 0)
                    payableListModel+='<div role="tabpanel" class="tab-pane active" id="'+companyName+'_'+candidateFeeItemId+'">';
                else
                    payableListModel+='<div role="tabpanel" class="tab-pane" id="'+companyName+'_'+candidateFeeItemId+'">';

                payableListModel+='<div class="col-sm-12 p0" id="feecollectionCreate-tform">';

                payableListModel+='<div class="col-sm-12 p0">';
                payableListModel+='<div class="col-sm-6">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<select class="select-dropdown" id="ddlFeePaymentType" onchange="changeFeePaymentType(this,\''+companyName+'_'+candidateFeeItemId+'\')" >';
                payableListModel+='<option value="" disabled selected>--select--</option>';
                payableListModel+='<option value="Online">Online</option>';
                payableListModel+='<option value="Offline">Offline</option>';
                payableListModel+='</select>';
                payableListModel+='<label class="select-label">Payment Mode <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-6 feePaidDate" style="display: none;">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<label class="datepicker-label datepicker">Paid Date <em>*</em></label>';
                payableListModel+='<input type="date" class="datepicker relative enablePastDates" placeholder="dd/mm/yyyy" id="txtPaidDate">';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-12 p0">';
                payableListModel+='<div class="col-sm-6">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<input id="txtManualReceipt" type="text" class="validate">';
                payableListModel+='<label for="txtManualReceipt">Manual Receipt<em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-12 p0">';

                payableListModel+='<div class="col-sm-6">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<select class="feeCollectionType select-dropdown" id="ddlFeePaymentMode" onchange="changeFeePaymentMode(this,\''+companyName+'_'+candidateFeeItemId+'\')">';
                payableListModel+='<option value="" disabled selected>--select--</option>';
                payableListModel+='<option value="Cash">Cash</option>';
                payableListModel+='<option value="Cheque">Cheque</option>';
                payableListModel+='<option value="DD">DD</option>';
                payableListModel+='<option value="Credit Card">Credit Card</option>';
                payableListModel+='<option value="PDC">PDC</option>';
                payableListModel+='<option value="Online Payment">Online Payment</option>';
                payableListModel+='<option value="NEFT">NEFT</option>';
                payableListModel+='<option value="RTGS">RTGS</option>';
                payableListModel+='</select>';
                payableListModel+='<label class="select-label">Payment Type <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='</div>';


                payableListModel+='<div class="feeCollectionCreat-hiddenFrm" id="paymentTypeDetails">';
                payableListModel+='<div class="col-sm-12 cheque-details">';
                payableListModel+='<div class="col-sm-6 pl0">';
                payableListModel+='<div class="input-field col-sm-11 p0">';
                payableListModel+='<select id="txtChequeBankName" name="ChequeBankName" class="custom-select-nomargin">';
                payableListModel+='</select>';
                payableListModel+='<label class="select-label">Bank Name <em>*</em></label>';
                payableListModel+='</div>';

                payableListModel+='<div class="col-sm-1 p0">';
                payableListModel+='<a class="mt10 ml5 display-inline-block cursor-pointer showAddBankWrap-btn" onclick="resetBankWrapper(this,\''+companyName+'_'+candidateFeeItemId+'\',\'txtChequeBankNameWrapper\')">';
                payableListModel+='<span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue" ></span></a>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="BankWrapper">';
                payableListModel+='<div class="col-sm-8 p0 ">';
                payableListModel+='<div class="input-field mt15">';
                payableListModel+='<input id="txtChequeBankNameWrapper" type="text" class="validate" value="">';
                payableListModel+='<label for="txtChequeBankNameWrapper">Bank Name <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-4 p0 ">';
                payableListModel+='<a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="addNewBankWrapper(this,\''+companyName+'_'+candidateFeeItemId+'\',\'txtChequeBankNameWrapper\')"></span></a>';
                payableListModel+='<a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red" onclick="RemoveNewBankWrapper(this,\''+companyName+'_'+candidateFeeItemId+'\')" ></span></a>';
                payableListModel+='</div>';
                payableListModel+='</div>';


                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-3 pl0">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<input id="txtChequeNumber" type="text" class="validate" maxlength="6" onkeypress="return isNumberKey(event)">';
                payableListModel+='<label for="txtChequeNumber">Cheque No. <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-3 pl0">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<label class="datepicker-label datepicker">Cheque Date <em>*</em></label>';
                payableListModel+='<input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="txtChequeIssueData">';
                payableListModel+='</div>';
                payableListModel+='</div>';

                payableListModel+='</div>';

                payableListModel+='<div class="col-sm-12 dd-details">';

                payableListModel+='<div class="col-sm-6 pl0">';

                payableListModel+='<div class="input-field col-sm-11 p0">';
                payableListModel+='<select id="txtDDBankName" name="DDBankName" class="custom-select-nomargin">';
                payableListModel+='</select>';
                payableListModel+='<label class="select-label">Bank Name <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-1 p0">';
                payableListModel+='<a class="mt10 ml5 display-inline-block cursor-pointer showAddBankWrap-btn" onclick="resetBankWrapper(this,\''+companyName+'_'+candidateFeeItemId+'\',\'txtDDBankNameWrapper\')">';
                payableListModel+='<span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue" ></span></a>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="BankWrapper">';
                payableListModel+='<div class="col-sm-8 p0 ">';
                payableListModel+='<div class="input-field mt15">';
                payableListModel+='<input id="txtDDBankNameWrapper" type="text" class="validate" value="">';
                payableListModel+='<label for="txtDDBankNameWrapper">Bank Name <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-4 p0 ">';
                payableListModel+='<a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="addNewBankWrapper(this,\''+companyName+'_'+candidateFeeItemId+'\',\'txtDDBankNameWrapper\')"></span></a>';
                payableListModel+='<a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red" onclick="RemoveNewBankWrapper(this,\''+companyName+'_'+candidateFeeItemId+'\')" ></span></a>';
                payableListModel+='</div>';
                payableListModel+='</div>';

                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-3 pl0">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<input id="txtDDNumber" type="text" class="validate" onkeypress="return isNumberKey(event)">';
                payableListModel+='<label for="txtDDNumber">DD No.<em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-3 pl0">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<label class="datepicker-label datepicker">DD Date <em>*</em></label>';
                payableListModel+='<input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="txtDDIssueData">';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='</div>';

                payableListModel+='<div class="col-sm-12 cc-details">';
                payableListModel+='<div class="col-sm-6 pl0">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<input id="txtCreditCard" type="text" class="validate" maxlength="4" onkeypress="return isNumberKey(event)" >';
                payableListModel+='<label for="txtCreditCard">Enter credit card last 4 digits <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='</div>';


                payableListModel+='<div class="col-sm-12 onlinepayment-details">';
                payableListModel+='<div class="col-sm-6 pl0">';
                payableListModel+='<div class="input-field col-sm-11 p0">';
                payableListModel+='<select id="txtOnlineBankName" name="OnlineBankName" class="custom-select-nomargin">';
                payableListModel+='</select>';
                payableListModel+='<label class="select-label">Bank Name <em>*</em></label>';
                payableListModel+='</div>';

                payableListModel+='<div class="col-sm-1 p0">';
                payableListModel+='<a class="mt10 ml5 display-inline-block cursor-pointer showAddBankWrap-btn" onclick="resetBankWrapper(this,\''+companyName+'_'+candidateFeeItemId+'\',\'txtOnlineBankNameWrapper\')">';
                payableListModel+='<span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue" ></span></a>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="BankWrapper">';
                payableListModel+='<div class="col-sm-8 p0 ">';
                payableListModel+='<div class="input-field mt15">';
                payableListModel+='<input id="txtOnlineBankNameWrapper" type="text" class="validate" value="">';
                payableListModel+='<label for="txtOnlineBankNameWrapper">Bank Name <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-4 p0 ">';
                payableListModel+='<a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="addNewBankWrapper(this,\''+companyName+'_'+candidateFeeItemId+'\',\'txtOnlineBankNameWrapper\')"></span></a>';
                payableListModel+='<a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red" onclick="RemoveNewBankWrapper(this,\''+companyName+'_'+candidateFeeItemId+'\')" ></span></a>';
                payableListModel+='</div>';
                payableListModel+='</div>';

                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-3 pl0">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<input id="txtReferenceNumber" type="text" class="validate" onkeypress="return isNumberKey(event)">';
                payableListModel+='<label for="txtReferenceNumber">Reference No. <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='<div class="col-sm-3 pl0">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<label class="datepicker-label datepicker">Payment Date <em>*</em></label>';
                payableListModel+='<input type="date" class="datepicker relative enableDates" placeholder="dd/mm/yyyy" id="txtOnlinePaymentData">';
                payableListModel+='</div>';
                payableListModel+='</div>';

                payableListModel+='</div>';
                payableListModel+='</div>';

                payableListModel+='<div class="col-sm-12 p0">';
                payableListModel+='<div class="col-sm-6">';
                payableListModel+='<div class="input-field">';
                payableListModel+='<textarea id="txaRemarks" class="materialize-textarea"></textarea>';
                payableListModel+='<label for="txaRemarks">Remarks <em>*</em></label>';
                payableListModel+='</div>';
                payableListModel+='</div>';
                payableListModel+='</div>';

                payableListModel+='<div class="col-sm-12 mb15">';
                payableListModel+='<button id="finalizePayment" onclick="finalizeFeePayment(this,\''+companyName+'_'+candidateFeeItemId+'\')" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Submit</button>';
                payableListModel+='</div>';

                payableListModel+='</div>';

                payableListModel+='<div class="col-sm-12 receipt-printable-wrapper">';
                payableListModel+='<div class="receipt-printable">';
                payableListModel+='</div>';
                payableListModel+='</div>';

                payableListModel+='</div>';
            }
            $("#PayableAmountsList").html(payableList);
            $("#PayableAmountsModels").html(payableListModel);
            $('select').material_select();
            getBanksList();
            enableDatePicker();
            $('.feeCollectionCreate-paymentTable').slideUp();
            $('.feeCollectioncreate-tabs').slideDown();
        }
    }
    return false;
});

function finalizeFeePayment(This,modelId)
{
    alertify.dismissAll();
    var batchId=$("#leadBatch").attr("data-id");
    if(batchId == null || batchId == "")
    {
        notify('Batch not exists for payment', 'error', 10);
    }
    else
    {
        var popModelId='#'+modelId;
        var feePaymentType=$(popModelId+" #ddlFeePaymentType").val();
        var feePaidDate=$(popModelId+" #txtPaidDate").val();
        var manualReceipt=$(popModelId+" #txtManualReceipt").val();

        var feePaymentMode=$(popModelId+" #ddlFeePaymentMode").val();
        var PaymentRemarks=$(popModelId+" #txaRemarks").val();

        var chq_bankName=$(popModelId+" #txtChequeBankName").val();
        var chq_chequeNumber=$(popModelId+" #txtChequeNumber").val();
        var chq_chequeDate=$(popModelId+" #txtChequeIssueData").val();

        var dd_bankName=$(popModelId+" #txtDDBankName").val();
        var dd_Number=$(popModelId+" #txtDDNumber").val();
        var dd_DDDate=$(popModelId+" #txtDDIssueData").val();

        var cc_cardNumber=$(popModelId+" #txtCreditCard").val();
        $(popModelId+" #txtCreditCard").trigger('autoresize');

        var online_bankName=$(popModelId+" #txtOnlineBankName").val();
        var online_referenceNumber=$(popModelId+" #txtReferenceNumber").val();
        var online_paymentDate=$(popModelId+" #txtOnlinePaymentData").val();

        var flag=0;
        var regx_receipt = /^[a-zA-Z0-9-]*$/;
        $('#ManageFeeCollectionForm '+popModelId+' span.required-msg').remove();
        $('#ManageFeeCollectionForm '+popModelId+' input,#ManageFeeCollectionForm '+popModelId+' textarea,#ManageFeeCollectionForm '+popModelId+' select').removeClass("required");

        if(feePaymentType == '' || feePaymentType == null)
        {
            $(popModelId+" #ddlFeePaymentType").parent().find('.select-dropdown').addClass("required");
            $(popModelId+" #ddlFeePaymentType").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        else if(feePaymentType == 'Offline')
        {
            if(feePaidDate == '')
            {
                $(popModelId+" #txtPaidDate").addClass("required");
                $(popModelId+" #txtPaidDate").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag = 1;
            }
        }
        if(manualReceipt == '')
        {
            $(popModelId+" #txtManualReceipt").addClass("required");
            $(popModelId+" #txtManualReceipt").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if (regx_receipt.test(manualReceipt) === false)
        {
            $(popModelId+" #txtManualReceipt").addClass("required");
            $(popModelId+" #txtManualReceipt").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if(feePaymentMode == '' || feePaymentMode == null)
        {
            $(popModelId+" #ddlFeePaymentMode").parent().find('.select-dropdown').addClass("required");
            $(popModelId+" #ddlFeePaymentMode").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        else
        {
            if(feePaymentMode == 'Cheque' || feePaymentMode == 'PDC')
            {
                if(chq_bankName == "" || chq_bankName == null)
                {
                    $(popModelId+" #txtChequeBankName").parent().find('.select-dropdown').addClass("required");
                    $(popModelId+" #txtChequeBankName").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                    flag = 1;
                }
                if(chq_chequeNumber == "")
                {
                    $(popModelId+" #txtChequeNumber").addClass("required");
                    $(popModelId+" #txtChequeNumber").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                    flag = 1;
                }
                else if(chq_chequeNumber.length != 6)
                {
                    $(popModelId+" #txtChequeNumber").addClass("required");
                    $(popModelId+" #txtChequeNumber").after('<span class="required-msg">Should contain 6 digits</span>');
                    flag = 1;
                }
                if(chq_chequeDate == "")
                {
                    $(popModelId+" #txtChequeIssueData").addClass("required");
                    $(popModelId+" #txtChequeIssueData").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                    flag = 1;
                }
            }
            else if(feePaymentMode == 'DD')
            {
                if(dd_bankName == "" || dd_bankName == null)
                {
                    $(popModelId+" #txtDDBankName").parent().find('.select-dropdown').addClass("required");
                    $(popModelId+" #txtDDBankName").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                    flag = 1;
                }
                if(dd_Number == "")
                {
                    $(popModelId+" #txtDDNumber").addClass("required");
                    $(popModelId+" #txtDDNumber").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                    flag = 1;
                }
                if(dd_DDDate == "")
                {
                    $(popModelId+" #txtDDIssueData").addClass("required");
                    $(popModelId+" #txtDDIssueData").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                    flag = 1;
                }
            }
            else if(feePaymentMode == 'Credit Card')
            {
                if(cc_cardNumber == "")
                {
                    $(popModelId+" #txtCreditCard").addClass("required");
                    $(popModelId+" #txtCreditCard").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                    flag = 1;
                }
                else if(cc_cardNumber.length != 4)
                {
                    $(popModelId+" #txtCreditCard").addClass("required");
                    $(popModelId+" #txtCreditCard").after('<span class="required-msg">Should contain 4 digits</span>');
                    flag = 1;
                }
            }
            else if(feePaymentMode == 'Online Payment' || feePaymentMode == 'NEFT' || feePaymentMode == 'RTGS')
            {
                if(online_bankName == "" || online_bankName == null)
                {
                    $(popModelId+" #txtOnlineBankName").parent().find('.select-dropdown').addClass("required");
                    $(popModelId+" #txtOnlineBankName").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                    flag = 1;
                }
                if(online_referenceNumber == "")
                {
                    $(popModelId+" #txtReferenceNumber").addClass("required");
                    $(popModelId+" #txtReferenceNumber").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                    flag = 1;
                }
                else if(online_referenceNumber.length != 6)
                {
                    $(popModelId+" #txtReferenceNumber").addClass("required");
                    $(popModelId+" #txtReferenceNumber").after('<span class="required-msg">Should contain 6 digits</span>');
                    flag = 1;
                }
                if(online_paymentDate == "")
                {
                    $(popModelId+" #txtOnlinePaymentData").addClass("required");
                    $(popModelId+" #txtOnlinePaymentData").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                    flag = 1;
                }
            }
        }

        if(PaymentRemarks == '' || PaymentRemarks == null)
        {
            $(popModelId+" #txaRemarks").addClass("required");
            $(popModelId+" #txaRemarks").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }

        if(flag == 1)
        {
            notify('Please enter valid values', 'error', 10);
        }
        else
        {
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var feeCollectionType=$('#feeCollectionType').val();
            if(feeCollectionType==1)
                var ajaxurl = API_URL + 'index.php/FeeCollection/addLeadFeeCollection';
            if(feeCollectionType==2)
                var ajaxurl = API_URL + 'index.php/FeeCollection/addLeadFeeCollection';
            var branchID=$('#userBranchList').val();
            var userID = $('#hdnUserId').val();
            var params={};
            if(feePaymentMode == 'Cheque' || feePaymentMode == 'PDC')
            {
                params=
                {
                    branchID: branchID,
                    batchID:batchId,
                    user: userID,
                    candidate_fee_item_id:$('#PayableAmountsList li.active').attr("id"),
                    candidate_fee_id:$('#PayableAmountsList li.active').attr("data-CandidateFeeId"),
                    branch_xref_lead_id:$('#PayableAmountsList li.active').attr("data-branchxrefleadid"),
                    courseId:$('#PayableAmountsList li.active').attr("data-CourseId"),
                    paymentType:feePaymentType,
                    paymentMode:feePaymentMode,
                    feePaidDate:feePaidDate,
                    manualReceipt:manualReceipt,
                    payingAmount:$('#PayableAmountsList li.active').attr("data-payingAmount"),
                    feecompanyId:$('#PayableAmountsList li.active').attr("data-feecompanyId"),
                    feeHeadId:$('#PayableAmountsList li.active').attr("data-feeHeadId"),
                    feeItem_Amount:$('#PayableAmountsList li.active').attr("data-payableAmount"),
                    IssueBankId:chq_bankName,
                    IssueChequeDDNumber:chq_chequeNumber,
                    ChequeDDDated:chq_chequeDate,
                    feeCollectionType:feeCollectionType,
                    PaymentRemarks:PaymentRemarks
                };
            }
            else if(feePaymentMode == 'DD')
            {
                params=
                {
                    branchID: branchID,
                    batchID:batchId,
                    user: userID,
                    candidate_fee_item_id:$('#PayableAmountsList li.active').attr("id"),
                    candidate_fee_id:$('#PayableAmountsList li.active').attr("data-CandidateFeeId"),
                    branch_xref_lead_id:$('#PayableAmountsList li.active').attr("data-branchxrefleadid"),
                    courseId:$('#PayableAmountsList li.active').attr("data-CourseId"),
                    paymentType:feePaymentType,
                    paymentMode:feePaymentMode,
                    feePaidDate:feePaidDate,
                    manualReceipt:manualReceipt,
                    payingAmount:$('#PayableAmountsList li.active').attr("data-payingAmount"),
                    feecompanyId:$('#PayableAmountsList li.active').attr("data-feecompanyId"),
                    feeHeadId:$('#PayableAmountsList li.active').attr("data-feeHeadId"),
                    feeItem_Amount:$('#PayableAmountsList li.active').attr("data-payableAmount"),
                    IssueBankId:dd_bankName,
                    IssueChequeDDNumber:dd_Number,
                    ChequeDDDated:dd_DDDate,
                    feeCollectionType:feeCollectionType,
                    PaymentRemarks:PaymentRemarks
                };
            }
            else if(feePaymentMode == 'Credit Card')
            {
                params=
                {
                    branchID: branchID,
                    batchID:batchId,
                    user: userID,
                    candidate_fee_item_id:$('#PayableAmountsList li.active').attr("id"),
                    candidate_fee_id:$('#PayableAmountsList li.active').attr("data-CandidateFeeId"),
                    branch_xref_lead_id:$('#PayableAmountsList li.active').attr("data-branchxrefleadid"),
                    courseId:$('#PayableAmountsList li.active').attr("data-CourseId"),
                    paymentType:feePaymentType,
                    paymentMode:feePaymentMode,
                    feePaidDate:feePaidDate,
                    manualReceipt:manualReceipt,
                    payingAmount:$('#PayableAmountsList li.active').attr("data-payingAmount"),
                    feecompanyId:$('#PayableAmountsList li.active').attr("data-feecompanyId"),
                    feeHeadId:$('#PayableAmountsList li.active').attr("data-feeHeadId"),
                    feeItem_Amount:$('#PayableAmountsList li.active').attr("data-payableAmount"),
                    cardNumber:cc_cardNumber,
                    feeCollectionType:feeCollectionType,
                    PaymentRemarks:PaymentRemarks
                };
            }
            else if(feePaymentMode == 'Online Payment' || feePaymentMode == 'NEFT' || feePaymentMode == 'RTGS')
            {
                params=
                {
                    branchID: branchID,
                    batchID:batchId,
                    user: userID,
                    candidate_fee_item_id:$('#PayableAmountsList li.active').attr("id"),
                    candidate_fee_id:$('#PayableAmountsList li.active').attr("data-CandidateFeeId"),
                    branch_xref_lead_id:$('#PayableAmountsList li.active').attr("data-branchxrefleadid"),
                    courseId:$('#PayableAmountsList li.active').attr("data-CourseId"),
                    paymentType:feePaymentType,
                    paymentMode:feePaymentMode,
                    feePaidDate:feePaidDate,
                    manualReceipt:manualReceipt,
                    payingAmount:$('#PayableAmountsList li.active').attr("data-payingAmount"),
                    feecompanyId:$('#PayableAmountsList li.active').attr("data-feecompanyId"),
                    feeHeadId:$('#PayableAmountsList li.active').attr("data-feeHeadId"),
                    feeItem_Amount:$('#PayableAmountsList li.active').attr("data-payableAmount"),
                    IssueBankId:online_bankName,
                    IssueChequeDDNumber:online_referenceNumber,
                    ChequeDDDated:online_paymentDate,
                    feeCollectionType:feeCollectionType,
                    PaymentRemarks:PaymentRemarks
                };
            }
            else
            {
                params=
                {
                    branchID: branchID,
                    batchID:batchId,
                    user: userID,
                    candidate_fee_item_id:$('#PayableAmountsList li.active').attr("id"),
                    candidate_fee_id:$('#PayableAmountsList li.active').attr("data-CandidateFeeId"),
                    branch_xref_lead_id:$('#PayableAmountsList li.active').attr("data-branchxrefleadid"),
                    courseId:$('#PayableAmountsList li.active').attr("data-CourseId"),
                    paymentType:feePaymentType,
                    paymentMode:feePaymentMode,
                    feePaidDate:feePaidDate,
                    manualReceipt:manualReceipt,
                    payingAmount:$('#PayableAmountsList li.active').attr("data-payingAmount"),
                    feecompanyId:$('#PayableAmountsList li.active').attr("data-feecompanyId"),
                    feeHeadId:$('#PayableAmountsList li.active').attr("data-feeHeadId"),
                    feeItem_Amount:$('#PayableAmountsList li.active').attr("data-payableAmount"),
                    feeCollectionType:feeCollectionType,
                    PaymentRemarks:PaymentRemarks
                };
            }
            if(feeCollectionType==2){
                    params.is_candidate_fee_item_id=$('#PayableAmountsList li.active').attr("data-iscandidatefeeid");
                    params.feeStructureId=$('#PayableAmountsList li.active').attr("data-feestructureid");
            }
            var action = 'fee collect';
            var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(FeeCollectionResponse){
                GetFeeCollectionSubmitInfo(FeeCollectionResponse,modelId)
            }
            });
        }
    }
}

function GetFeeCollectionSubmitInfo(FeeCollectionResponse,modelId)
{
    alertify.dismissAll();
    var popModelId='#'+modelId;
    if (FeeCollectionResponse.status == true)
    {
        $('#PayableAmountsList li.active span.fee-creation-amount').after( '<span class="fee-creation-greenCheck white-color"><i class="icon-right"></i></span>' );
        var feeReceiptData=FeeCollectionResponse['data'];
        var payableListModel='';
        payableListModel+='<div class="receipt-header">';
        payableListModel+='<span class="left-logo">';
        payableListModel+='<img src="'+BASE_URL+'assets/images/logo.png" />';
        payableListModel+='</span>';
        payableListModel+='</div>';
        payableListModel+='<div class="print_reciept p5">';
        payableListModel+='<div data-reciept="assets" class="wrapper">';
        payableListModel+='<p class="campus"></p>';
        payableListModel+='<h1>'+feeReceiptData[0]['companyName']+'</h1>';
        payableListModel+='<p>'+feeReceiptData[0]['branchName']+'</p>';
        payableListModel+='<table cellspacing="0" cellpadding="4" border="0" class="studentDetails">';
        payableListModel+='<tbody>';
        payableListModel+='<tr>';
        payableListModel+='<td>Student/Lead No:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['lead_number']+'</td>';
        payableListModel+='<td>Course:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['courseName']+'</td>';
        payableListModel+='</tr>';
        payableListModel+='<tr>';
        payableListModel+='<td>Batch:</td>';
        if(feeReceiptData[0]['alias_name'] == null || feeReceiptData[0]['alias_name'] == '')
            payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['batchInfo']+'</td>';
        else
            payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['batchInfo']+'('+feeReceiptData[0]['alias_name']+')</td>';
        payableListModel+='<td>Receipt No:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['receipt_number']+'</td>';
        payableListModel+='</tr>';
        payableListModel+='<tr>';
        payableListModel+='<td>Name:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['studentName']+'</td>';
        payableListModel+='<td>Date:</td>';

        if(feeReceiptData[0]['payment_type']=='Online')
            payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['receipt_date_time']+'</td>';
        else
            payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['receipt_date']+'</td>';


        //payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['receipt_date']+'</td>';
        payableListModel+='</tr>';
        payableListModel+='<tr>';
        payableListModel+='<td>Payment Mode:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['payment_type']+'</td>';
        payableListModel+='<td>Payment Type:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feeReceiptData[0]['payment_mode']+'</td>';
        payableListModel+='</tr>';
        payableListModel+='</tbody>';
        payableListModel+='</table>';
        payableListModel+='<table cellspacing="0" cellpadding="2" border="1" class="feeDetails mt20" data-genre="null">';
        payableListModel+='<thead>';
        payableListModel+='<tr>';
        payableListModel+='<td class="font-bold" data-genre="nullType">Feehead</td>';
        payableListModel+='<td class="font-bold" data-genre="nullInstallment" data-content="installment-header">Installment</td>';
        payableListModel+='<td class="font-bold" width="150" align="right" data-genre="nullTotal">Total Amount(Rs.)</td>';
        payableListModel+='</tr>';
        payableListModel+='</thead>';
        payableListModel+='<tbody>';
        payableListModel+='<tr>';
        payableListModel+='<td data-genre="nullType">'+feeReceiptData[0]['feeHead']+'</td>';
        payableListModel+='<td data-genre="nullInstallment" data-content="installment">1</td>';
        payableListModel+='<td align="right" data-genre="nullTotal">'+moneyFormat(feeReceiptData[0]['amount_paid'])+'</td>';
        payableListModel+='</tr>';
        payableListModel+='</tbody>';
        payableListModel+='</table>';
        payableListModel+='<table cellspacing="0" cellpadding="4" border="0" class="total">';
        payableListModel+='<tbody>';
        payableListModel+='<tr>';
        payableListModel+='<td colspan="6" style="text-transform: capitalize;"> Rupees '+NumberinWords(Math.abs(feeReceiptData[0]['amount_paid']))+'</td>';
        payableListModel+='<td class="font-bold" width="150" align="right"><span class="icon-rupee"></span>'+moneyFormat(feeReceiptData[0]['amount_paid'])+'</td>';
        payableListModel+='</tr>';
        payableListModel+='<tr></tr>';
        payableListModel+='</tbody>';
        payableListModel+='</table>';
        payableListModel+='</div>';
        payableListModel+='</div>';
        payableListModel+='<div class="col-sm-12 pl0" style="margin-top:120px;">';
        //payableListModel+='<button class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Print</button>';
        payableListModel+='<a class="btn blue-btn" target="_blank" href="'+APP_REDIRECT+'fee_receipt/'+feeReceiptData[0]['fee_collection_id']+'"><i class="icon-right mr8"></i>Print</a>';
        payableListModel+='</div>';
        payableListModel+='</div>';
        $(popModelId+' .receipt-printable').html(payableListModel);
        $(popModelId+' #feecollectionCreate-tform').slideUp();
        $(popModelId+' .receipt-printable').slideDown();
    }
    else
    {
        alertify.dismissAll();
        notify(FeeCollectionResponse['message'], 'error', 10);
    }
}
function changeFeePaymentType(This,modelId)
{
    var popModelId='#'+modelId;
    var myVal = $(This).val();
    clearDatePicker(popModelId+" #txtPaidDate");
    $(popModelId+" #txtPaidDate,"+popModelId+" #txtManualReceipt").removeClass("required");
    $(popModelId+" #txtPaidDate,"+popModelId+" #txtManualReceipt").next('span.required-msg').remove();
    $(popModelId+" #txtManualReceipt").val('');
    if(myVal=='Online')
    {
        $(popModelId+' .feePaidDate').slideUp();
    }
    else if(myVal=='Offline')
    {
        $(popModelId+' .feePaidDate').slideDown();
    }
}
function changeFeePaymentMode(This,modelId)
{
    var popModelId='#'+modelId;
    var myVal = $(This).val();
    $(popModelId+' #paymentTypeDetails span.required-msg').remove();
    $(popModelId+' #paymentTypeDetails input').removeClass("required");
    $(popModelId+' #paymentTypeDetails input').val('');
    $(popModelId+' #paymentTypeDetails label').removeClass("active");

    if(myVal=='Cheque' || myVal=='PDC')
    {
        $(popModelId+' .dd-details,.cc-details,.onlinepayment-details').slideUp();
        $(popModelId+' .feeCollectionCreat-hiddenFrm').find('.cheque-details').slideDown();
        $(popModelId+' #txtChequeBankName').val('0');
        $(popModelId+' #txtChequeBankName').material_select();
    }
    else if(myVal=='DD')
    {
        $(popModelId+' .cheque-details,.cc-details,.onlinepayment-details').slideUp();
        $(popModelId+' .feeCollectionCreat-hiddenFrm').find('.dd-details').slideDown();
        $(popModelId+' #txtDDBankName').val('0');
        $(popModelId+' #txtDDBankName').material_select();
    }
    else if(myVal=='Credit Card')
    {
        $(popModelId+' .cheque-details,.dd-details,.onlinepayment-details').slideUp();
        $(popModelId+' .feeCollectionCreat-hiddenFrm').find('.cc-details').slideDown();
    }
    else if(myVal=='Cash')
    {
        $(popModelId+' .cheque-details,.dd-details,.cc-details,.onlinepayment-details').slideUp();
    }
    else if(myVal=='Online Payment' || myVal=='NEFT' || myVal=='RTGS')
    {
        $(popModelId+' .cheque-details,.dd-details,.cc-details').slideUp();
        $(popModelId+' .feeCollectionCreat-hiddenFrm').find('.onlinepayment-details').slideDown();
        $(popModelId+' #txtOnlineBankName').val('0');
        $(popModelId+' #txtOnlineBankName').material_select();
    }
}

function getBanksList()
{
    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
    var params = {name: 'Bank'};
    var action = 'add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: banksListResponse});
}

function banksListResponse(response)
{
    $('select[name=DDBankName],select[name=ChequeBankName],select[name=OnlineBankName]').empty();
    $('select[name=DDBankName],select[name=ChequeBankName],select[name=OnlineBankName]').append($('<option disabled selected></option>').val('0').html('--Choose Bank--'));
    if (response.status == true)
    {
        if (response.data.length > 0) {
            $.each(response.data, function (key, value) {
                $('select[name=DDBankName],select[name=ChequeBankName],select[name=OnlineBankName]').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
            });
        }
    }
    $('select[name=DDBankName],select[name=ChequeBankName],select[name=OnlineBankName]').material_select();
}

function resetBankWrapper(This,modelId,FieldId)
{
    var popModelId='#'+modelId;
    var fieldId='#'+FieldId;
    $(popModelId+' #BankWrapper span.required-msg').remove();
    $(popModelId+' #BankWrapper label').removeClass("active");
    $(popModelId+' #BankWrapper input').removeClass("required");
    //$(popModelId+' #txtDDBankNameWrapper').val('');
    $(popModelId+' '+fieldId).val('');
    $(popModelId+' #BankWrapper').slideDown();
}

function addNewBankWrapper(This,modelId,FieldId)
{
    var popModelId='#'+modelId;
    var fieldId='#'+FieldId;
    var BankName=$(popModelId+' '+fieldId).val();
    var regx_txtTagWrapper = /^[A-Za-z ]+$/;
    var flag = 0;
    if(BankName == '')
    {
        $(popModelId+' '+fieldId).addClass("required");
        $(popModelId+' '+fieldId).after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
    }
    else if ((regx_txtTagWrapper.test(BankName)) === false)
    {
        $(popModelId+' '+fieldId).addClass("required");
        $(popModelId+' '+fieldId).after('<span class="required-msg">Accepts only alphabets</span>');
        flag = 1;
    }

    if (flag == 0)
    {
        var action = 'add';
        var description = '----';
        var referenceType = 'Bank';
        var ajaxurl = API_URL + "index.php/Referencevalues/addReferenceValueWrapper";

        var params =
        {
            referenceName: BankName,
            referenceDescription: description,
            referenceType: referenceType
        };
            var userId = $('#hdnUserId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
            if (response == -1 || response['status'] == false)
            {
                alertify.dismissAll();
                notify(response['message'], 'error', 10);
            }
            else
            {
                alertify.dismissAll();
                notify('Saved Successfully', 'success', 10);
                getBanksList();
                RemoveNewBankWrapper(This,modelId)
            }
            }});
    }
    else
    {
        return false;
    }
}
function RemoveNewBankWrapper(This,modelId)
{
    var popModelId='#'+modelId;
    $(popModelId+' #BankWrapper').slideUp();
}
//Fee collection ends

/*LeadFollowUp*/

function manageLeadCalls(This,Id){
    clearDatePicker("#dpNextFollowUp");
    clearDatePicker("#dpvisitedDate");
    $('#ManageCallStatus')[0].reset();
    $('#ManageCallStatus label').removeClass("active");
    $('#ManageCallStatus span.required-msg').remove();
    $('#ManageCallStatus input').removeClass("required");
    $('#ManageCallStatus select').removeClass("required");
    $('#ManageCallStatus textarea').find('label').addClass("active");
    $('#ManageCallStatus textarea').removeClass("required");
    $("#CallStatusInformation").hide();
    $("#callStatusComments").hide();
    $("#VisitedDatesPicker").hide();
    $("#FutureDatesPicker").hide();
    $('#callingPhoneNoWrapper').html('');
    if (Id == 0)
    {
        $("#myModalLabel").html("Call Status");
    } else
    {
        $("#myModalLabel").html("Edit Call Status");

        var ajaxurl = API_URL + 'index.php/LeadFollowUp/getleadByBranchxcrfId';
        var params = {BranchxcerLeadId: Id};
        var userID = $('#hdnUserId').val();
        var headerParams = {action: 'list', context: 'Lead Follow Up', serviceurl: 'leadfollowup', pageurl: 'leadfollowup', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: ManageCallsresponse});
    }

    $(This).attr("data-target", "#callStatus");
    $(This).attr("data-toggle", "modal");
}
function ManageCallsresponse(response){
    if (response == -1 || response['status'] == false)
    {
        notify('Something went wrong', 'error', 10);
    } else
    {
        var LeadData = response['data'];
        $("#BxrefLeadName").text(LeadData[0]['lead_name']);
        $("#BxrefLeadEmail").text(LeadData[0]['email']);
        $("#BxrefLeadPhone").text(LeadData[0]['phone']);
        $('#callingPhoneNoWrapper').html(LeadData[0]['phone']);
        $("#BxrefLeadCity").text(LeadData[0]['city']);
        $("#BxrefLeadCountry").text(LeadData[0]['country']);
        $("#BxrefLeadCreatedDate").text(LeadData[0]['created_on']);
        $("#hdnBxrefLeadId").val(LeadData[0]['branch_xref_lead_id']);
        $("#hdnLeadStage").val(LeadData[0]['status']);
        $("#hdnLeadStageText").val(LeadData[0]['lead_stage']);
        $("#hdnLead_id").val(LeadData[0]['lead_id']);
        var leadStage = LeadData[0]['lead_stage'];
        var currentStage=$('#hdnLeadStageText').val().trim();
        leadStatuses(this,currentStage);
        if(leadStage == 'L0'){
            $("#intrestedLevelLabel").html('(M3)');
        } else if(leadStage == 'M2'){
            $("#intrestedLevelLabel").html('(M3)');
        } else if(leadStage == 'M3'){
            $("#intrestedLevelLabel").html('(M3+)');
        } else{
            $("#intrestedLevelLabel").html('('+leadStage+')');
        }
    }

}
function CallStatusType(){
    $("#FutureDatesPicker").hide();
    $("#CallStatusInformation").hide();
    $('#ManageCallStatus span.required-msg').remove();
    $('#ManageCallStatus input').removeClass("required");
    $('#ManageCallStatus select').removeClass("required");
    $('#ManageCallStatus textarea').removeClass("required");
    var callStatus = $('#callsTypes').val();
    var leadStageText=$("#hdnLeadStageText").val();
    if(callStatus == "switchoff" || callStatus == "notlifted"){
        $("#FutureDatesPicker").show();
        $("#callStatusComments").show();

    }
    if(callStatus == "answered"){
        $('input[type=radio][name=callStatusInfo_radio]').change(function() {
            var leadStageText=$("#hdnLeadStageText").val();
            var selectedOption  = this.value;
            if(selectedOption == 'callStatusInfoIntrested'){
                if(leadStageText == "M3+") {
                    clearDatePicker("#dpvisitedDate");
                    $("#VisitedDatesPicker").show();
                }
                $("input[id='callStatusInfoIntrested']:checked").click();
            } else if(selectedOption == 'callStatusInfoNotintrested'){
                $("#VisitedDatesPicker").hide();
                $("input[id='callStatusInfoNotintrested']:checked").click();
            } else if(selectedOption == 'callStatusInfoDND'){
                $("#VisitedDatesPicker").hide();
                $("input[id='callStatusInfoDND']:checked").click();
            }
        });
        $("#CallStatusInformation").show();
        $("#callStatusComments").show();
    }
    if(callStatus == "notexist"){

        $("#callStatusComments").show();
    }
}
function SaveCallStatus(){
    var Id = $('#hdnBxrefLeadId').val();
    var userID = $('#hdnUserId').val();
    var LeadID = $('#hdnLead_id').val();
    var callStatus = $('#callsTypes').val();
    var date=$("#dpNextFollowUp").val();
    var nextvisited=$("#dpvisitedDate").val();
    var comments=$("#txaCallStatusDescription").val();
    var leadStage=$("#hdnLeadStage").val();
    var leadStageText=$("#hdnLeadStageText").val();
    var statusInfo=$('input[name=callStatusInfo_radio]:checked').val();

    $('#ManageCallStatus span.required-msg').remove();
    $('#ManageCallStatus input').removeClass("required");
    $('#ManageCallStatus select').removeClass("required");
    $('#ManageCallStatus textarea').removeClass("required");
    if(statusInfo=="callStatusInfoIntrested")
    {
        var lead_stage=leadStage;
    }
    else if(statusInfo=="callStatusInfoNotintrested")
    {
            lead_stage="M2";
    }
    else if(statusInfo=="callStatusInfoDND")
    {
            lead_stage="L0";
    }
    var flag=0;
    if (callStatus == "")
    {
        $("#callsTypes").parent().find('.select-dropdown').addClass("required");
        $("#callsTypes").parent().after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }
    if(callStatus == "switchoff" || callStatus == "notlifted")
    {
        if (date == "")
        {
            $("#dpNextFollowUp").addClass("required");
            $("#dpNextFollowUp").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
    }
    if(callStatus=="answered" && statusInfo=="callStatusInfoIntrested"){
        if(leadStageText=="M3+")
        {
            if (nextvisited == "")
            {
                $("#dpvisitedDate").addClass("required");
                $("#dpvisitedDate").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag = 1;
            }
        }
    }
    if(comments == '')
    {
        $("#txaCallStatusDescription").addClass("required");
        $("#txaCallStatusDescription").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
        flag=1;
    }
    if (callStatus == 'switchoff' || callStatus == 'notlifted')
    {
        var data = {
            BranchxcerLeadId: Id,
            LeadID:LeadID,
            NextFollowUpdate:date,
            lead_stage:leadStage,
            statusInfo:statusInfo,
            comments:comments,
            UserID:userID
        };

    }
    else if (callStatus == 'answered')
    {
        if(leadStageText!="M3+"){
            var data = {
                BranchxcerLeadId:Id,
                LeadID:LeadID,
                comments:comments,
                statusInfo:statusInfo,
                lead_stage:lead_stage,
                UserID:userID
            };
        }else{
            var data = {
                BranchxcerLeadId: Id,
                LeadID:LeadID,
                comments:comments,
                statusInfo:statusInfo,
                nextvisited:nextvisited,
                lead_stage:lead_stage,
                UserID:userID
            };


        }

    }
    else if (callStatus == 'notexist')
    {
        var data = {
            BranchxcerLeadId: Id,
            LeadID:LeadID,
            comments:comments,
            statusInfo:statusInfo,
            lead_stage:"L0",
            UserID:userID
        };
    }

    if(flag!=1){
		alertify.dismissAll();
		notify('Processing', 'warning', 40);
        var ajaxurl = API_URL + 'index.php/LeadFollowUp/SaveCallStatusInfo';
        var params = data;
        var headerParams = {action: 'add', context: 'Lead Follow Up', serviceurl: 'leadfollowup', pageurl: 'leadfollowup', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this,method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveCallsresponse});
    }


}
function SaveCallsresponse(response){
    alertify.dismissAll();
    if (response.status === false) {
        notify(response.message, 'error', 10);
    } else {
        notify(response.message, 'success', 10);
        $('#callStatus').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        if (typeof buildLeadsDataTable == 'function') {
            buildLeadsDataTable();
            ShowLeadsCount();
        }
        if (typeof leadInfoPageLoad == 'function') {
            leadInfoPageLoad();
        }
        closeLeadCallStatus();
    }
}
function closeLeadCallStatus(){
    $('#callsTypes').val(0);
    $('#callsTypes').material_select();
    clearDatePicker("#dpvisitedDate");
    $('input[type=radio][name=callStatusInfo_radio]').attr('checked', false);
    $('#callStatus').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

/*LeadFollowUp*/

function LeadTimelineTypes(This){
    $("#timelineDp").show();
    $('#manageTimeline li').removeClass('active-icon');
    //TypeOfTimeline
    if(This.id=="Note"){
        $("#timelineDp").hide();
    }
    $("#"+This.id+"").attr("class","active-icon");
    $("#TypeOfTimeline").val(This.id);
    clearDatePicker("#dpTimeline");
    $('#manageTimeline')[0].reset();
    $('#manageTimeline label').removeClass("active");
    $('#manageTimeline span.required-msg').remove();
    $('#manageTimeline input,textarea').removeClass("required");
    $('#manageTimeline select').removeClass("required");

}
function ManageTimeLine(This)
{
    LeadTimelineTypes("");
    $('#manageTimeline')[0].reset();
    $('#manageTimeline label').removeClass("active");
    $('#manageTimeline span.required-msg').remove();
    $('#manageTimeline input,textarea').removeClass("required");
    $('#manageTimeline select').removeClass("required");
    clearDatePicker("#dpTimeline");
}
function AddLeadTimeLine(This) {

    var dateOfTimeline = $("#dpTimeline").val();
    var information = $("#txainforamtion").val();
    var userID = $("#hdnUserId").val();
    var typeOfTimeline = $("#TypeOfTimeline").val();
    var BranchXrefId = $("#hdnBranchXrefId").val();
    var LeadId = $("#hdnLeadId").val();

    $('#manageTimeline span.required-msg').remove();
    $('#manageTimeline input').removeClass("required");
    $('#manageTimeline textarea').removeClass("required");
    var flag = 0;
    if(typeOfTimeline != "Note"){
        if (dateOfTimeline == '') {
            $("#dpTimeline").addClass("required");
            $("#dpTimeline").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
    }
    if (information == '') {
        $("#txainforamtion").addClass("required");
        $("#txainforamtion").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }
    if (typeOfTimeline == '') {
        alertify.dismissAll();
        notify('Select the type of Notification ', 'error', 10);
        //$("#txainforamtion").addClass("required");
        //$("#txainforamtion").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }


    var max_fileUpload_size = 100.048;
    /*For max file size for uploaded file(In MB)*/
    var files = $("#attachemntUpload").get(0).files;
    var selectedFile = $("#attachemntUpload").val();
    var extArray = ['gif','jpg','png','jpeg'];
    var extension = selectedFile.split('.');
    if (files.length > 0)
    {
        if(extension[extension.length - 1] == 'exe')
        {
            $(".file-path").addClass("required");
            $(".file-path").after('<span class="required-msg">Upload valid file</span>');
            flag = 1;
        }
    }
    /*if (files.length > 0 && $.inArray(extension[extension.length - 1], extArray) <= -1) {
        $(".file-path").focus();
        $(".file-path").addClass("required");
        $(".file-path").after('<span class="required-msg">only ' + extArray.toString() + '</span>');
        flag = 1;
    }
    if (files.length > 0 && ((files[0].size / 1024) / 1024) > max_fileUpload_size) {
        $(".file-path").focus();
        $(".file-path").addClass("required");
        $(".file-path").after('<span class="required-msg">File can\'t be more than 2Mb</span>');
        flag = 1;
    }*/

    if (flag == 0) {
        var action = "add";
        var headerParams = {
            action: action,
            contentType: false,
            processData: false,
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: userID
        };
        var ajaxurl = API_URL + 'index.php/LeadsInfo/SaveTimeline';
        var fileUpload = document.getElementById("attachemntUpload");

        var uploadFile = new FormData();
        if (files.length > 0) {
            uploadFile.append("file", files[0]);
            uploadFile.append("image", 'timeline_' + new Date().getTime() +'.'+extension[1]);

        }
        uploadFile.append('date', dateOfTimeline);
        uploadFile.append('information', information);
        uploadFile.append('BranchXrefId', BranchXrefId);
        uploadFile.append('typeOfTimeline', typeOfTimeline);
        uploadFile.append('userID', userID);
        uploadFile.append('LeadId', LeadId);


        //}
        $.ajax({
            /*type: 'POST',*/
            type: "POST",
            url: ajaxurl,
            dataType: "json",
            data: uploadFile,
            contentType: false,
            processData: false,
            headers: headerParams,
            beforeSend: function () {
                $(This).attr("disabled", "disabled");
                alertify.dismissAll();
                notify('Processing..', 'warning', 10);
            }, success: function (response) {
                alertify.dismissAll();
                $(This).removeAttr("disabled");
                leadInfoPageLoad();
                $('#addNotification').modal('toggle');
            }, error: function (response) {
                alertify.dismissAll();
                notify('Something went wrong. Please try again', 'error', 10);
            }
        });

    }
}
/* Created by V Parameshwar, purpose: Out station contact tagging starts here (contacts.php) */
$("#checkUncheck").change(function () {
    $("#contacts-list input:checkbox[name=txtCheckBoxContact]").prop('checked', $(this).prop("checked"));
});
function getBranchesForOutstattions() {
    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Branch/getBranchList';
    var params = {};
    var action = 'list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    var response =
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: getBranchesForOutstattionsResponse});
}
function getBranchesForOutstattionsResponse(response) {
    $('#txtContactBranch').empty();
    $('#txtContactBranch').append($('<option selected></option>').val('').html('--Select--'));
    if (response.status == true) {

        if (response.data.length > 0) {

            $.each(response.data, function (key, value) {
                $('#txtContactBranch').append($('<option></option>').val(value.id).html(value.name));
            });
        }
    }
    $('#txtContactBranch').material_select();
}
function resetTagOutStationContacts(){
    $('#formTagOutStationContacts')[0].reset();
    $('#txtContactBranch').empty();
    $('#txtContactBranch').material_select();
    $('#checkUncheck').trigger('change');
    $('#formTagOutStationContacts label').removeClass("active");
    $('#formTagOutStationContacts span.required-msg').remove();
    $('#formTagOutStationContacts input').removeClass("required");
    $('#formTagOutStationContacts select').removeClass("required");
    $('#formTagOutStationContacts textarea').removeClass("required");
    $('#OutStationsTagWrapper').slideUp();
    $('#checkUncheckWrapper').slideUp();
}
function ManageTagOutStationContacts(This,type){

    $('#formTagOutStationContacts')[0].reset();
    $('#txtContactBranch').empty();
    $('#txtContactBranch').material_select();
    $('#checkUncheck').trigger('change');
    $('#formTagOutStationContacts label').removeClass("active");
    $('#formTagOutStationContacts span.required-msg').remove();
    $('#formTagOutStationContacts input').removeClass("required");
    $('#formTagOutStationContacts select').removeClass("required");
    $('#formTagOutStationContacts textarea').removeClass("required");
    $('#hdnTaggingType').val(type);
    $('#OutStationsTagWrapper').slideDown();
    $('#checkUncheckWrapper').slideUp();
    if(type==1){
        $('#checkUncheckWrapper').slideDown();
    }
    getBranchesForOutstattions();



}
function tagOutStationContacts(This){
    notify('Processing..', 'warning', 10);
    $('#formTagOutStationContacts label').removeClass("active");
    $('#formTagOutStationContacts span.required-msg').remove();
    $('#formTagOutStationContacts input').removeClass("required");
    $('#formTagOutStationContacts select').removeClass("required");
    $('#formTagOutStationContacts textarea').removeClass("required");
    var outStationContact='';
    var branch='';
    var sel=[];
    branch=$('#txtContactBranch').val();
    var flag=0;
    if(branch.trim()==''){
        $("#txtContactBranch").parent().find('.select-dropdown').addClass("required");
        $("#txtContactBranch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
        return false;
    }
    if($('#hdnTaggingType').val()==1) {
        $('input[name="txtCheckBoxContact"]:checked').each(function () {
            sel.push(this.value);
        });
        outStationContact = sel.join(',');
        if (outStationContact.trim() == '') {
            var ModelTitle='Contact Tagging';
            var Description='No Contacts Selected!';
            customAlert(ModelTitle,Description);
            flag = 1;
            return false;
        }
    }

    if(flag==1){
        return false;
    }
    else{

        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Lead/saveTagOutStationContact';
        var params = {branchId:branch,contactId:outStationContact,type:$('#hdnTaggingType').val()};
        var action = 'list';
        var type='POST';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: tagOutStationContactsResponse});

    }


}
function tagOutStationContactsResponse(response){
    alertify.dismissAll();
    if(response.status==true){
        notify(response.message,'success');
        resetTagOutStationContacts();
        refreshContactsView();
    }
    else{
        notify(response.message,'error');
    }
}
/* Out station contact tagging ends here */

/* Lead source report */
function leadReportBySourcePageLoad()
{
    var userId = $('#userId').val();
    var action = 'report';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

    var ajaxurl = API_URL + 'index.php/BranchReport/getAllConfigurationsByUserId';
    commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
        var dashboard = '';
        if (response.data.length == 0 || response == -1 || response['status'] == false) {
        } else {
            var RawData = response.data;
            var html = '';
            if (RawData.branch.length > 0) {
                for (var a in RawData.branch) {
                    html += '<a class="active" data-id="' + RawData.branch[a].id + '" href="javascript:;">' + RawData.branch[a].name + '</a>'
                }
                $('#branchFilter').html(html);
            }
            html = '';
            if (RawData.course.length > 0) {
                for (var a in RawData.course) {
                    html += '<a class="active" data-id="' + RawData.course[a].courseId + '" href="javascript:;">' + RawData.course[a].name + '</a>'
                }
                $('#courseFilter').html(html);
            }
            html = '';
            if (RawData.leadStage.length > 0) {
                for (var a in RawData.leadStage) {
                    html += '<a class="active" data-id="' + RawData.leadStage[a].lead_stage_id + '" href="javascript:;">' + RawData.leadStage[a].lead_stage + '</a>'
                }
                $('#stageFilter').html(html);
            }
        }
    }});
}

function filterLeadReportBySource(This) {
    var branchId = [];
    var courseId = [];
    var branchIdText = [];
    var courseIdText = [];
    var error = true;
    $("#branchFilter > .active").each(function () {
        error = false;
        branchId.push($(this).attr('data-id'));
        branchIdText.push($(this).text());
    });
    $("#courseFilter > .active").each(function () {
        error = false;
        courseId.push($(this).attr('data-id'));
        courseIdText.push($(this).text());
    });
    var branchId=branchId.join(', ');
    var courseId=courseId.join(', ');
    if(branchId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one branch','error');
        return false;
    }
    else if(courseId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one course','error');
        return false;
    }
    else{
        error=false;
    }

    if (!error) {
        var action = 'report';
        var ajaxurl=API_URL+'index.php/SourceReport/getSourceReport';
        var params = {'branch': branchId, 'course': courseId, 'stage': '','branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '), 'stageText': '' };
        var userID=$('#userId').val();
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#report-list").show();
        $("#report-list").dataTable().fnDestroy();
        $tableobj = $('#report-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                verifyAccess();
                buildpopover();
            },
            /*"footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var colnumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
                var colNames = ['M7','M6','M5', 'M4','M3+','M3','M2','L2','L3','L3+','L4','L5','L6','DnD'];
                $.each(colnumbers, function (keys, values) {
                    var eachcolumn=values;
                    var eachColumnname=colNames[keys];

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    // Total over all pages
                    total = api
                        .column(eachcolumn)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b[eachColumnname]);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(eachcolumn, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b[eachColumnname]);
                        }, 0);

                    // Update footer
                    $(api.column(eachcolumn).footer()).html(
                        pageTotal
                    );
                });
            },*/
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": false,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No Records Found",
                "sZeroRecords": "No Records Found",
                "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
            },
            "bProcessing": true,
            ajax: {
                url:ajaxurl,
                type:'GET',
                data:params,
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.Source;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M7;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M6;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M5;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M4;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data['M3+'];
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M3;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M2;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L2;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L3;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data['L3+'];
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L4;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L5;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L6;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.DnD;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data['Conversion Rate'];
                }
                },
                {
                    data: null, render: function ( data, type, row )
                    {

                        return data.Total;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();

    } else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}
function filterSourceWiseReportExport(This){
    var branchId = [];
    var branchIdText = [];
    var courseIdText = [];
    var courseId = [];

    var error = true;
    $("#branchFilter > .active").each(function() {
        error = false;
        branchId.push($(this).attr('data-id'));
        branchIdText.push($(this).text());
    });
    $("#courseFilter > .active").each(function() {
        error = false;
        courseId.push($(this).attr('data-id'));
        courseIdText.push($(this).text());
    });
    var branchId=branchId.join(', ');
    var courseId=courseId.join(', ');

    if(branchId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one branch','error');
        return false;
    }
    else if(courseId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one course','error');
        return false;
    }
    else{
        error=false;
    }

    if(!error){
        var total=0;
        var pageTotal=0;
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var action = 'report';
        var ajaxurl=API_URL+'index.php/SourceReport/getSourceReportExport';
        var params = {'branch': branchId, 'course': courseId, 'stage': '','branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '), 'stageText': ''};
        var userID=$('#userId').val();
        var type='GET';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response =
            commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterSourceWiseReportExportResponse});


    } else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}

function filterSourceWiseReportExportResponse(response){
    alertify.dismissAll();
    if(response.status===true){
        var download=API_URL+'Download/downloadReport/'+response.file_name;
        window.location.href=(download);

    }
    else{
        notify(response.message,'error');
    }
}
/* lead source report ends here */

/* lead branchwise report */
function branchReportPageLoad()
{
    var userId = $('#userId').val();
    var action = 'report';
    var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

    var ajaxurl=API_URL+'index.php/BranchReport/getAllConfigurationsByUserId';
    var params = {};
    commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
        var dashboard = '';
        if (response.data.length == 0 || response == -1 || response['status'] == false) {
        } else {
            var RawData = response.data;
            var html = '';
            if(RawData.branch.length > 0){
                for(var a in RawData.branch){
                    html += '<a class="active" data-id="'+RawData.branch[a].id+'" href="javascript:;">'+RawData.branch[a].name+'</a>'
                }
                $('#branchFilter').html(html);
            }
            html = '';
            if(RawData.course.length > 0){
                for(var a in RawData.course){
                    html += '<a class="active" data-id="'+RawData.course[a].courseId+'" href="javascript:;">'+RawData.course[a].name+'</a>'
                }
                $('#courseFilter').html(html);
            }
            html = '';
            if(RawData.leadStage.length > 0){
                for(var a in RawData.leadStage){
				if(RawData.leadStage[a].lead_stage != 'M7'){
                        html += '<a class="active tooltipped"  data-position="bottom" data-tooltip="'+RawData.leadStage[a].description+'" data-id="'+RawData.leadStage[a].lead_stage_id+'" href="javascript:;">'+RawData.leadStage[a].lead_stage+'</a>'
                    }
                }
                $('#stageFilter').html(html);
            }
            buildpopover();
        }
    }});
}

function filterBranchWiseReport(This){
    var branchId = [];
    var courseId = [];
    var stageId = [];
    var error = true;
    $("#branchFilter > .active").each(function() {
        error = false;
        branchId.push($(this).attr('data-id'));
    });
    $("#courseFilter > .active").each(function() {
        error = false;
        courseId.push($(this).attr('data-id'));
    });
    $("#stageFilter > .active").each(function() {
        error = false;
        stageId.push($(this).attr('data-id'));
    });

    var branchId=branchId.join(', ');
    var courseId=courseId.join(', ');
    var stageId=stageId.join(', ');

    if(branchId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one branch','error');
        return false;
    }
    else if(courseId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one course','error');
        return false;
    }
    else if(stageId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one lead stage','error');
        return false;
    }
    else{
        error=false;
    }

    if(!error){
        var total=0;
        var pageTotal=0;

        var action = 'report';
        var ajaxurl=API_URL+'index.php/BranchReport/getBranchReport';
        var params = {'branch': branchId, 'course': courseId, 'stage': stageId};
        var userID=$('#userId').val();
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        $("#report-list").show();
        $("#report-list").dataTable().fnDestroy();
        $tableobj = $('#report-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                verifyAccess();
                buildpopover();
            },
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var colnumbers = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
                var colNames = ['M2','M3', 'M3+','M4','M5','M6','L2','L3','L3+','L4','L5','L6'];
                $.each(colnumbers, function (keys, values) {
                    var eachcolumn=values;
                    var eachColumnname=colNames[keys];

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    // Total over all pages
                    total = api
                        .column(eachcolumn)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b[eachColumnname]);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(eachcolumn, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b[eachColumnname]);
                        }, 0);

                    // Update footer
                    $(api.column(eachcolumn).footer()).html(
                        pageTotal
                    );
                });
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": false,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No Records Found",
                "sZeroRecords": "No Records Found",
                "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
            },
            "bProcessing": true,
            ajax: {
                url:ajaxurl,
                type:'GET',
                data:params,
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.Branch;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.Course;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M2;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M3;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data['M3+'];
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M4;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M5;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.M6;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L2;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L3;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data['L3+'];
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L4;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L5;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.L6;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.Total;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();

    } else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}

function filterBranchWiseReportExport(This){
    var branchId = [];
    var branchIdText = [];
    var courseIdText = [];
    var stageIdText = [];
    var courseId = [];
    var stageId = [];
    var error = true;
    $("#branchFilter > .active").each(function() {
        error = false;
        branchId.push($(this).attr('data-id'));
        branchIdText.push($(this).text());
    });
    $("#courseFilter > .active").each(function() {
        error = false;
        courseId.push($(this).attr('data-id'));
        courseIdText.push($(this).text());
    });
    $("#stageFilter > .active").each(function() {
        error = false;
        stageId.push($(this).attr('data-id'));
        stageIdText.push($(this).text());
    });
    var branchId=branchId.join(', ');
    var courseId=courseId.join(', ');
    var stageId=stageId.join(', ');

    if(branchId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one branch','error');
        return false;
    }
    else if(courseId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one course','error');
        return false;
    }
    else if(stageId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one stage','error');
        return false;
    }
    else{
        error=false;
    }

    if(!error){
        var total=0;
        var pageTotal=0;
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var action = 'report';
        var ajaxurl=API_URL+'index.php/BranchReport/getBranchReportExport';
        var params = {'branch': branchId, 'course': courseId, 'stage': stageId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '), 'stageText': stageIdText.join(', ') };
        var userID=$('#userId').val();
        var type='GET';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response =
            commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterBranchWiseReportExportResponse});


    } else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}

function filterBranchWiseReportExportResponse(response){
    alertify.dismissAll();
    if(response.status===true){
        var download=API_URL+'Download/downloadReport/'+response.file_name;
        window.location.href=(download);

    }
    else{
        notify(response.message,'error');
    }
}
/*lead branchwise report ends here */

//Fee concession function starts
function ManageFeeConcession(This)
{
    $("#LeadData").hide();
    $('#ManageFeeAssignForm')[0].reset();
    $('#ManageFeeAssignForm label').removeClass("active");
    $('#ManageFeeAssignForm span.required-msg').remove();
    $('#ManageFeeAssignForm input').removeClass("required");
    $('#ManageFeeAssignForm select').removeClass("required");
    $('#leadInfoSnapShot').html("");
    $("select[name=ddlFeetypes]").val(0);
    $('#ddlFeetypes').material_select();
    $(This).attr("data-target", "#FeeAssign");
    $(This).attr("data-toggle", "modal");
}

function showConcessionLeadInfo(This)
{
    var userID = $('#hdnUserId').val();
    var userRole = $("#hdnUserRole").val();
    var leadNumber=$("#txtLeadNumber").val();
    var RegexNumbers = /^[0-9]+$/;
    feeassignReset();

    $("#ddlFeetypes").attr("disabled",false);
    $('#ManageFeeAssignForm span.required-msg').remove();
    $('#ManageFeeAssignForm input').removeClass("required");
    $('#ManageFeeAssignForm textarea').removeClass("required");
    $("#online").attr("disabled",false);
    $("#clasRoom").attr("disabled",false);

    $("#ShowfeeDetails").hide();
    var flag=0;
    if(leadNumber == '')
    {
        $("#txtLeadNumber").addClass("required");
        $("#txtLeadNumber").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
        flag=1;
    }
    else if (RegexNumbers.test(leadNumber) === false)
    {
        $("#txtLeadNumber").addClass("required");
        $("#txtLeadNumber").after('<span class="required-msg">Accepts Numbers only</span>');
        flag = 1;
    }

    if(flag==0){
        var ajaxurl = API_URL + 'index.php/FeeAssign/getFeeAssignInfoByLeadNumber';
        var action = 'concession request';
        var params = {UserID: userID,LeadNumber:leadNumber};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID,userRole:userRole};
        commonAjaxCall({This: this,  requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: GetConcessionLeadInfoResponse});
    }

}

function GetConcessionLeadInfoResponse(response)
{
    if(response.status===false){
        notify(response.message,'error',10);
    }
    else
    {
        rawData=response.data[0];
        if(rawData.qualification==null || rawData.qualification==''){
            rawData.qualification='----';
        }
        if(rawData.companyName==null || rawData.companyName==''){
            rawData.companyName='----';
        }
        if(rawData.institution==null || rawData.institution==''){
            rawData.institution='----';
        }
        if(rawData.qualification==null || rawData.qualification==''){
            rawData.qualification='----';
        }

        var content = '<div class="col-sm-12 ">';
        content += '<div class="light-blue3-bg clearfix pt10 pb10">';
        content += '<div class="col-sm-6 mb5">';
        content += '<div class="input-field data-head-name">';
        content += '<span class="display-block ash">Name</span>';
        content += rawData.name;
        content += '<input type="hidden" id="hdnCandidateFeeId" value="' + rawData.candidate_fee_id + '" />';
        content += '</div>';
        content += '</div>';
        content += '<div class="col-sm-6 mb5">';
        content += '<div class="input-field data-head-name">';
        content += '<span class="display-block ash">Email</span>';
        content += '<p>' + rawData.email + '</p>';
        content += '</div>';
        content += '</div>';
        content += '<div class="col-sm-6 mb5">';
        content += '<div class="input-field data-head-name">';
        content += '<span class="display-block ash">Phone</span>';
        content += '<p>' + rawData.phone + '</p>';
        content += '</div>';
        content += ' </div>';
        content += '<div class="col-sm-6 mb5">';
        content += '<div class="input-field data-head-name">';
        content += '<span class="display-block ash">Branch</span>';
        content += '<p>' + rawData.branchName + '</p>';
        content += '</div>';
        content += '</div>';
        content += '<div class="col-sm-6 mb5">';
        content += '<div class="input-field data-head-name">';
        content += '<span class="display-block ash">Course</span>';
        content += '<p>' + rawData.courseName + '</p>';
        content += '</div>';
        content += '</div>';
        content += '<div class="col-sm-6 mb5">';
        content += '<div class="input-field data-head-name">';
        content += '<span class="display-block ash">Qualification</span>';
        content += '<p>' + rawData.qualification + '</p>';
        content += '</div>';
        content += '</div>';
        content += '<div class="col-sm-6 mb5">';
        content += '<div class="input-field data-head-name">';
        content += '<span class="display-block ash">Company</span>';
        content += '<p>' + rawData.companyName + '</p>';
        content += '</div>';
        content += '</div>';
        content += '<div class="col-sm-6 mb5">';
        content += '<div class="input-field data-head-name">';
        content += '<span class="display-block ash">Institution</span>';
        content += '<p>' + rawData.institution + '</p>';
        content += '</div>';
        content += '</div>';
        content += '</div>';
        content += '</div>';
        var disableAmnt='';
        if(rawData.is_concession_allowed == 0)
        {
            disableAmnt=' readonly disabled ';
        }
        else if(rawData.is_modification_request==1){
            disableAmnt=' readonly disabled ';
        }
        else
        {
            disableAmnt='';
        }
        var Amounts = "";
        Amounts += '<div class="col-sm-12 mt15">'
            + '<table class="table table-responsive table-striped table-custom" id="feeassign-tb-list">'
            + '<thead><tr>'
            + '<th>Fee Head</th><th>Due days</th><th class="text-right">Amount</th><th class="text-right">Amount Paid</th><th>Concession</th></tr>'
            + '</thead><tbody>';
        var concession_amount=0;
        for(var i=0;i<response.data.length;i++)
        {
            if(response.data[i].approved_status == 'Rejected' && response.data[i].approval_type == 'Concession')
            {
                concession_amount=0;
            }
            else
            {
                concession_amount=response.data[i].modified_concession;
            }
            Amounts += '<tr>';
            Amounts += '<td>' + response.data[i].feeHeadName + '</td>';
            Amounts += '<td>'+  response.data[i].due_days + '</td>';
            Amounts += '<td class="text-right"><span class="icon-rupee"></span>'+ moneyFormat(response.data[i].amount_payable) + '</td>';
            Amounts += '<td class="text-right"><span class="icon-rupee"></span>'+ moneyFormat(response.data[i].amount_paid) + '</td>';
            Amounts += '<td><div class="col-sm-8 pl0">';
            Amounts += '<input type="hidden" name="hdnCandidateFeeId[]" value="' + response.data[i].candidate_fee_id + '" />';
            Amounts += '<input type="hidden" name="hdnCandidateFeeItemId[]" value="' + response.data[i].candidate_fee_item_id + '" />';
            Amounts += '<input type="hidden" name="hdnAmountPayable[]" value="' + (response.data[i].amount_payable-response.data[i].amount_paid) + '" />';
            Amounts += '<input class="modal-table-textfiled m0" type="text" name="txtConcessionAmount[]" value="' + concession_amount + '" onkeypress="return isNumberKey(event)" '+disableAmnt+' />';
            Amounts += '</div></td>';
            Amounts += '</tr>';
        }

        Amounts += '</tbody></table></div><div id="DataMessagePayment" class="col-sm-12 mt15 text-center"></div>';
        $('#leadInfoSnapShot').html(content+Amounts);
        $("#DataMessagePayment").hide();
        $("#DataMessagePayment").html('');
        if(rawData.is_modification_request==1)
        {
            $("#DataMessagePayment").show();
            $("#DataMessagePayment").html("<b style='color:red;'>Modification request is in pending, Concession can't be given to this lead</b>");
            $('#saveModifyFeeButton').attr('disabled',true);

        }
        else if(rawData.is_concession_allowed==0)
        {
            $("#DataMessagePayment").show();
            $("#DataMessagePayment").html("<b style='color:red;'>Concession can't be given to this lead</b>");
            $('#saveModifyFeeButton').attr('disabled',true);
        }
        else
        {
            $('#saveModifyFeeButton').attr('disabled',false);
        }
    }
}
/*  Starts Product Management By Manasa T */
function manageProducts(This,Id)
{
    notify('Processing..', 'warning', 10);
    $('#product_button').html('');
    if(Id==0)
    {
        $('#product_button').html('<i class="icon-right mr8"></i>Add');
    }
    else
    {
        $('#product_button').html('<i class="icon-right mr8"></i>Update');
    }
    $('#ManageProductForm')[0].reset();
    Materialize.updateTextFields();
    $('#ManageProductForm span.required-msg').remove();
    $('#ManageProductForm input').removeClass("required");
    $("#ManageProductForm").removeAttr("disabled");
    $('#PublicationWrapper').slideUp();
    var userID = $("#hdnUserId").val();
    resetPublicationWrapper();
    if (Id == 0)
    {
        $("#myModalLabel").html("Create Product");
        getProductType();
        getPublication();
    }
    else
    {
        var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        $.when(
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    params: {name: 'Product Type'},
                    requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                    action: 'edit'
                }
            ),
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    params: {name: 'Publication'},
                    requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                    action: 'edit'
                }
            ),
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    params: {productId: Id},
                    requestUrl: API_URL + 'index.php/Product/getProductDetails',
                    action: 'edit'
                }
            )
        ).then(function (response1, response2,response3)
        {
            getProductTypeResponse(response1[0]);
            getPublicationResponse(response2[0]);
            ManageProductResponse(response3[0]);
            $("#myModalLabel").html("Edit Product");
            //$("#product_button").val("Add");
            $("#product_button").html("<i class=\"icon-right mr8\"></i>Update");
        });
    }
    $("#hdnProductId").val(Id);
    $(This).attr("data-target", "#myProduct");
    $(This).attr("data-toggle", "modal");
}
function getProductType()
{
    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
    var params = {name: 'Product Type'};
    var action = 'list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    var response =
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getProductTypeResponse});
}
function getProductTypeResponse(response)
{
    $('#product_type').empty();
    $('#product_type').append($('<option disabled selected></option>').val('').html('--Select--'));
    if (response.status == true) {

        if (response.data.length > 0) {

            $.each(response.data, function (key, value) {

                $('#product_type').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
            });
        }
    }
    $('#product_type').material_select();
}
function getPublication() {
    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
    var params = {name: 'Publication'};
    var action = 'list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    var response =
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getPublicationResponse});
}
function getPublicationResponse(response) {
    $('#publications').empty();
    $('#publications').append($('<option disabled selected></option>').val('').html('--Select--'));
    if (response.status == true) {

        if (response.data.length > 0) {

            $.each(response.data, function (key, value) {

                $('#publications').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
            });
        }
    }
    $('#publications').material_select();
}
function saveProduct(This)
{
    var productName = $.trim($("#product_name").val());
    var productType = $("#product_type").val();
    var productCode = $("#product_code").val();
    var publication = $('#publications').val();
    var version = $('#version').val();
    var description = $.trim($('#description').val());
    var userID = $("#hdnUserId").val();
    var productId = $('#hdnProductId').val();
    $('#ManageProductForm span.required-msg').remove();
    $('#ManageProductForm input').removeClass("required");
    $('#ManageProductForm select').removeClass("required");
    //$('#ManageProductForm textarea').removeClass("required");
    //var regx_productName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
    var regx_productName = /^(\w+\s?)*\s*$/;
    var flag = 0;
    if (productName == '')
    {
        $("#product_name").addClass("required");
        $("#product_name").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } /*else if (regx_productName.test($('#product_name').val()) === false) {
        $("#product_name").addClass("required");
        $("#product_name").after('<span class="required-msg">Invalid Name</span>');
        flag = 1;
    }*/
    if(productType =='' || productType==null)
    {
        $("#product_type").parent().find('.select-dropdown').addClass("required");
        $("#product_type").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if (productCode == '')
    {
        $("#product_code").addClass("required");
        $("#product_code").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } /*else if (regx_productName.test($('#product_code').val()) === false) {
        $("#product_code").addClass("required");
        $("#product_code").after('<span class="required-msg">Invalid Product Code</span>');
        flag = 1;
    }*/
    if(publication == '' || publication==null)
    {
        $("#publications").parent().find('.select-dropdown').addClass("required");
        $("#publications").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if (flag == 1) {
        return false;
    } else {
        notify('Processing..', 'warning', 10);
        if(productId==0)
        {
            var ajaxurl = API_URL + 'index.php/Product/addProduct';
            var params = {userID: userID, productName: productName, productType: productType, productCode: productCode, version:version, publication: publication,description:description};
            var type = "POST";
            var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveProductResponse});
        }
        else {
            var ajaxurl = API_URL + 'index.php/Product/updateProduct';
            var params = {userID: userID,productId: productId, productName: productName, productType: productType, productCode: productCode, version:version, publication: publication,description:description};
            var type = "POST";
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: SaveProductResponse});
        }
    }
}
function SaveProductResponse(response)
{
    alertify.dismissAll();
    var response = response;
    if (response == -1 || response.status == false)
    {
        $.each(response.data, function (i, item) {
            alertify.dismissAll();
            notify(item, 'error', 10);
            if (response.data['productName'])
            {
                alertify.dismissAll();
                $("#product_name").addClass("required");
                $("#product_name").after('<span class="required-msg">' + response.data['productName'] + '</span>');
            }
            if (response.data['productType'])
            {
                alertify.dismissAll();
                $("#product_type").addClass("required");
                $("#product_type").after('<span class="required-msg">' + response.data['productType'] + '</span>');
            }
            if (response.data['productCode'])
            {
                alertify.dismissAll();
                $("#product_code").addClass("required");
                $("#product_code").after('<span class="required-msg">' + response.data['productCode'] + '</span>');
            }
            if (response.data['code'])
            {
                alertify.dismissAll();
                $("#product_code").addClass("required");
                $("#product_code").after('<span class="required-msg">' + response.data['code'] + '</span>');
            }
            if (response.data['publication'])
            {
                alertify.dismissAll();
                $("#publications").addClass("required");
                $("#publications").after('<span class="required-msg">' + response.data['publication'] + '</span>');
            }
            return false;
        });
    } else
    {
        alertify.dismissAll();
        notify(response.message, 'success', 10);
        $(this).attr("data-dismiss", "modal");
        buildProductsDataTable();
        $('#myProduct').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

    }
}

function addPublicationWrapper(This)
{
    $('#PublicationWrapper span.required-msg').remove();
    var url = '';
    var action = 'add';
    var name = $('#txtPublication').val();
    var description = '----';
    var data = '';
    var referenceType = 'Publication';
    data = {
        referenceName: name,
        referenceDescription: description,
        referenceType: referenceType
    };
    url = API_URL + "index.php/Referencevalues/addReferenceValueWrapper";
    var regx_txtTagWrapper = /^[a-zA-Z][a-zA-Z0-9\s]*$/;

    var flag = 0;
    if (name == '') {
        $("#txtPublication").addClass("required");
        $("#txtPublication").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    } else if ((regx_txtTagWrapper.test(name)) === false)
    {
        $("#txtPublication").addClass("required");
        $("#txtPublication").after('<span class="required-msg">Invalid Publication.</span>');
        flag = 1;
    }

    if (flag == 0) {
        var ajaxurl = url;
        var params = data;

        var userId = $('#hdnUserId').val();

        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
            if (response == -1 || response['status'] == false)
            {

                var id = '';
                alertify.dismissAll();
                notify(response['message'], 'error', 10);
                for (var a in response.data) {
                    if (a == referenceType) {
                        id = '#txtPublication';
                        $(id).addClass("required");
                        $(id).next('label').addClass("active");
                        $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                    } else {
                        id = '#' + a;
                    }

                }

            } else
            {
                alertify.dismissAll();
                notify('Saved Successfully', 'success', 10);
                $('#txtPublication').val('');
                $('#PublicationWrapper').slideUp();
                $('#PublicationWrapper label').removeClass("active");
                $('#PublicationWrapper input').removeClass("required");
                $('#PublicationWrapper span.required-msg').remove();
                getPublication(this);

            }
        }});

    } else {
        return false;
    }
}
function resetPublicationWrapper(This) {
    $('#PublicationWrapper span.required-msg').remove();
    $('#PublicationWrapper label').removeClass("active");
    $('#PublicationWrapper input').removeClass("required");
    $('#txtPublication').val('');
}
function ManageProductResponse(response)
{
    alertify.dismissAll();
    var productDetails = response;
    if (productDetails == -1 || productDetails['status'] == false)
    {
        notify('Something went wrong', 'error', 10);
    } else
    {
        var productData = productDetails.data[0];
        $("#product_name").val(productData.name);
        $("#product_name").next('label').addClass("active");
        $('#product_type').val(productData.fk_product_type_id);
        $('#product_type').material_select();
        $("#product_type").next('label').addClass("active");
        $("#product_code").val(productData.code);
        $("#product_code").next('label').addClass("active");

        $("#version").val(productData.version);
        $("#version").next('label').addClass("active");

        $('#publications').val(productData.fk_publication_id);
        $('#publications').material_select();
        $("#publications").next('label').addClass("active");
        $("#description").val(productData.description);
        $("#description").next('label').addClass("active");
    }
}
/*  Ends Product Management By Manasa T */

function leadStatuses(This,currentStage)
{
    currentStage = currentStage || '';
    var userID=$('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/LeadStatus/getLeadStages';
    var params = {currentStage:currentStage};
    var type = "GET";
    var action='list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    commonAjaxCall({This: This, method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: leadStatusesResponse});
}
function leadStatusesResponse(response){
    var callStatusDropdown=[];
    $('#callsTypes').empty();
    $('#callsTypes').append($('<option></option>').val('').html('--Select--'));
    $('#CallAnswerTypes').empty();
    $('#CallAnswerTypes').append($('<option></option>').val('').html('--Select--'));
    if(response.status===true){

        $.each(response.data, function (key, value)
        {
            callStatusDropdown.push(key);
            if(key.length>0 && (typeof value['value'] != "undefined") &&  value['value'].length>0) {
                $('#callsTypes').append($('<option></option>').val(value['value']).html(key));
                if(value['value']=='Answered' && (typeof value['options'] != "undefined") && value['options'].length>0){
                    $.each(value['options'], function (keyOption, valueOption)
                    {
                        var attrDate='';
                        if(valueOption['followup date label']){
                            var attrDate=valueOption['followup date label'];
                        }
                        else{
                            var attrDate='Next Followup Date';
                        }
                        if((typeof valueOption['key'] != "undefined") && valueOption['key'].length>0 && (typeof valueOption['value'] != "undefined") && valueOption['value'].length>0) {
                            $('#CallAnswerTypes').append($('<option></option>').val(valueOption['key']).html(valueOption['value']).attr('data-date', attrDate));
                        }
                    });
                }
            }

        });

    }
    else{

    }

    $('#callsTypes').material_select();
    $('#callsTypes').trigger('change');
    $('#CallAnswerTypes').material_select();
}
function CallStatusChange(){
    $("#FutureDatesPicker").hide();
    $("#CallStatusInformation").hide();
    $('#ManageCallStatus span.required-msg').remove();
    $('#ManageCallStatus input').removeClass("required");
    $('#ManageCallStatus select').removeClass("required");
    $('#ManageCallStatus textarea').removeClass("required");
    var callStatus = $('#callsTypes').val();
    var leadStageText=$("#hdnLeadStageText").val();
    $("#CommonCallStatusInformation").hide();
    if(callStatus == "Answered"){
        $("#CommonCallStatusInformation").show();
        $("#callStatusComments").show();
    }
    else{
        $('#CallAnswerTypes').prop('selectedIndex',0);
        $('#CallAnswerTypes').material_select();
        $('#CallAnswerTypes').trigger('change');
        $("#callStatusComments").show();
    }
}
function getCallAnswerTypes(This){
    $('#CommonCallStatusDatePicker').hide();
    clearDatePicker('#txtCommonCallStatusDatePicker');
    if($(This).val()!=null && $(This).val()!='')
    {
        var dateLabel = $('option:selected', This).attr('data-date') + ' <em>*</em>';
        $('#txtLabelCommonCallStatusDatePicker').html(dateLabel);
        $('#CommonCallStatusDatePicker').show();
    }
}
function changeCallStatus(This){
var flag=0;
$('#ManageCallStatus span.required-msg').remove();
$('#ManageCallStatus input').removeClass("required");
$('#ManageCallStatus select').removeClass("required");
$('#ManageCallStatus textarea').removeClass("required");
var Id = $('#hdnBxrefLeadId').val();
var userID = $('#hdnUserId').val();
var LeadID = $('#hdnLead_id').val();
var callStatus = $('#callsTypes').val().trim();
var callAnswer = $('#CallAnswerTypes').val().trim();
var currentStage=$('#hdnLeadStageText').val().trim();
var date='';
var lead_stage='';
var dateType='';
var callType = $("#ddlCalltype").val();
var comments=$("#txaCallStatusDescription").val();
var leadStage=$("#hdnLeadStage").val();
var leadStageText=$("#hdnLeadStageText").val();
if (callStatus == "") {
    $("#callsTypes").parent().find('.select-dropdown').addClass("required");
    $("#callsTypes").parent().after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
    flag = 1;
}
else{
    if(callStatus=="Answered")
    {
        dateType=$('option:selected', $('#CallAnswerTypes')).attr('data-date');
        if(callAnswer==""){
            $("#CallAnswerTypes").parent().find('.select-dropdown').addClass("required");
            $("#CallAnswerTypes").parent().after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else {
            lead_stage = callAnswer;
            date = $("#txtCommonCallStatusDatePicker").val();
            if(date==''){
                $("#txtCommonCallStatusDatePicker").addClass("required");
                $("#txtCommonCallStatusDatePicker").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag = 1;
            }


        }
    }
    else
    {
        lead_stage=callStatus;
    }
}
if(comments == '')
{
    $("#txaCallStatusDescription").addClass("required");
    $("#txaCallStatusDescription").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
    flag=1;
}
var data = {
        BranchxcerLeadId: Id,
        LeadID:LeadID,
        date:date,
        callType:callType,
        dateType:dateType,
        lead_stage:lead_stage,
        comments:comments,
        UserID:userID,
        currentStage:currentStage
    };
    if(flag!=1){
        alertify.dismissAll();
        notify('Processing', 'warning', 40);
        var ajaxurl = API_URL + 'index.php/LeadFollowUp/ChangeCallStatusInfo';
        var params = data;
        var headerParams = {action: 'add', context: 'Lead Follow Up', serviceurl: 'leadfollowup', pageurl: 'leadfollowup', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This,method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveCallsresponse});
    }
    else{
        return false;
    }


}
function SaveCallsresponse(response){
    alertify.dismissAll();
    if (response.status === false) {
        notify(response.message, 'error', 10);
    } else {
        notify(response.message, 'success', 10);
        $('#callStatus').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        if (typeof buildLeadsDataTable == 'function') {
            buildLeadsDataTable();
            ShowLeadsCount();
        }
        if (typeof leadInfoPageLoad == 'function') {
            leadInfoPageLoad();
        }
        closeLeadCallStatus();
    }
}
function manageLeadCallStatus(This,Id){
    notify('Processing..', 'warning', 10);
    $('#ManageCallStatus')[0].reset();
    $('#ManageCallStatus label').removeClass("active");
    $('#ManageCallStatus span.required-msg').remove();
    $('#ManageCallStatus input').removeClass("required");
    $('#ManageCallStatus select').removeClass("required");
    $('#ManageCallStatus textarea').find('label').addClass("active");
    $('#ManageCallStatus textarea').removeClass("required");
    $("#CallStatusInformation").hide();
    $("#callStatusComments").hide();
    $('#callingPhoneNoWrapper').html('');
    if (Id == 0)
    {
        $("#myModalLabel").html("Call Status");
    } else
    {
        $("#myModalLabel").html("Edit Call Status");

        var ajaxurl = API_URL + 'index.php/LeadFollowUp/getleadByBranchxcrfId';
        var params = {BranchxcerLeadId: Id};
        var userID = $('#hdnUserId').val();
        var headerParams = {action: 'list', context: 'Lead Follow Up', serviceurl: 'leadfollowup', pageurl: 'leadfollowup', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: manageLeadCallStatusResponse});
    }

    $(This).attr("data-target", "#callStatus");
    $(This).attr("data-toggle", "modal");
}
function manageLeadCallStatusResponse(response){
    if (response == -1 || response['status'] == false)
    {
        notify('Something went wrong', 'error', 10);
    } else
    {
        var LeadData = response['data'];
        var leadEmail = '';
        if(LeadData[0]['email'] == '' || LeadData[0]['email'] == null)
        {
            leadEmail='----';
        }
        else
        {
            leadEmail=LeadData[0]['email'];
        }
        $("#BxrefLeadName").text(LeadData[0]['lead_name']);
        $("#BxrefLeadEmail").text(leadEmail);
        $("#BxrefLeadPhone").text(LeadData[0]['phone']);
        $('#callingPhoneNoWrapper').html(LeadData[0]['phone']);
        $("#BxrefLeadCity").text(LeadData[0]['city']);
        $("#BxrefLeadCountry").text(LeadData[0]['country']);
        $("#BxrefLeadCreatedDate").text(LeadData[0]['created_on']);
        $("#hdnBxrefLeadId").val(LeadData[0]['branch_xref_lead_id']);
        $("#hdnLeadStage").val(LeadData[0]['status']);
        $("#hdnLeadStageText").val(LeadData[0]['lead_stage']);
        $("#hdnLead_id").val(LeadData[0]['lead_id']);
        var leadStage = LeadData[0]['lead_stage'];
        var currentStage=$('#hdnLeadStageText').val().trim();
        leadStatuses(this,currentStage);
        alertify.dismissAll();
    }

}

/* UnclearedCheque list report starts here */
function UnclearedChequeListReportPageLoad()
{
    var userId = $('#userId').val();
    var action = 'report';
    var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

    var ajaxurl=API_URL+'index.php/UnclearedCheque/getAllConfigurations';
    var params = {};
    commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
        var dashboard = '';
        //return false;
        if (response.data.length == 0 || response == -1 || response['status'] == false) {
        } else {
            var RawData = response.data;
            var html = '';
            if(RawData.branch.length > 0){
                for(var a in RawData.branch){
                    html += '<a class="active" data-id="'+RawData.branch[a].id+'" href="javascript:;">'+RawData.branch[a].name+'</a>'
                }
                $('#branchFilter').html(html);
            }
            buildpopover();
        }
    }});
}
function filterUnclearedChequeReport(This){
    var branchId = [];
    var error = true;
    $("#branchFilter > .active").each(function() {
        error = false;
        branchId.push($(this).attr('data-id'));
    });
    var branchId=branchId.join(', ');
    if(branchId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one branch','error');
        return false;
    }
    else{
        error=false;
    }

    if(!error){
        var action = 'report';
        var ajaxurl=API_URL+'index.php/UnclearedCheque/getUnclearedChequeListReport';
        var params = {'branch': branchId};
        var userID=$('#userId').val();
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#UnclearedCheque-report-list-wrapper").show();
        $("#UnclearedCheque-report-list").dataTable().fnDestroy();
        $tableobj = $('#UnclearedCheque-report-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                verifyAccess();
                buildpopover();
            },
            "footerCallback": function ( row, data, start, end, display ) {
                $('#UnclearedCheque-report-list_wrapper div.DTHeader').html('Uncleared Cheque / DD Report');
            },
            dom: "<'DTHeader'>Bfrtip",
            bInfo: false,
            "serverSide": false,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No Records Found",
                "sZeroRecords": "No Records Found",
                "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
            },
            "bProcessing": true,
            ajax: {
                url:ajaxurl,
                type:'GET',
                data:params,
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.enrollNo;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.leadName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.receiptNo;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.remarks;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.chequeBankname;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.receipt_date;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.postedBy;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.posting_date;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.postedBankname;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {

                    return data.bank_voucher_number;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();

    } else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}
function filterUnclearedChequeListReportExport(This){
    var branchId = [];
    var branchIdText = [];
    var error = true;
    $("#branchFilter > .active").each(function() {
        error = false;
        branchId.push($(this).attr('data-id'));
        branchIdText.push($(this).text());
    });
    var branchId=branchId.join(', ');
    if(branchId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one branch','error');
        return false;
    }
    else{
        error=false;
    }

    if(!error){
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var action = 'report';
        var ajaxurl=API_URL+'index.php/UnclearedCheque/getUnclearedChequeListReportExport';
        var params = {'branch': branchId, 'branchText': branchIdText.join(', ')};
        var userID=$('#userId').val();
        var type='GET';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response =
            commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterUnclearedChequeListReportExportResponse});
    } else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}
function filterUnclearedChequeListReportExportResponse(response){
    alertify.dismissAll();
    if(response.status===true){
        var download=API_URL+'Download/downloadReport/'+response.file_name;
        window.location.href=(download);
    }
    else{
        notify(response.message,'error');
    }
}
/* Uncleared Cheque list report ends here */

function dateConvertion(dt){
    var orgDate=new Date(dt);
    var dd=orgDate.getDate();
    var mm=orgDate.getMonth()+1;
    if(dd<10){
        dd='0'+dd;
    }
    if(mm<10){
        mm='0'+mm;
    }

    var yy=orgDate.getFullYear();
    return (yy+'-'+mm+'-'+dd);

}
/* Cash Counter list report starts here */
function cashCounterListReportPageLoad()
{

    var userId = $('#userId').val();
    var action = 'report';
    var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

    var ajaxurl=API_URL+'index.php/CashCounter/getAllConfigurations';
    var params = {};
    commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
        var dashboard = '';
        //return false;
        if (response.data.length == 0 || response == -1 || response['status'] == false) {
        } else {
            var RawData = response.data;
            var html = '';
            if(RawData.branch.length > 0){
                for(var a in RawData.branch){
                    html += '<a class="active" data-id="'+RawData.branch[a].id+'" href="javascript:;">'+RawData.branch[a].name+'</a>'
                }
                $('#branchFilter').html(html);
            }
            html = '';
            if(RawData.course.length > 0){
                for(var a in RawData.course){
                    html += '<a class="active" data-id="'+RawData.course[a].courseId+'" href="javascript:;">'+RawData.course[a].name+'</a>'
                }
                $('#courseFilter').html(html);
            }
            html = '';
            if(RawData.company.length > 0){
                for(var a in RawData.company){
                    html += '<a class="active" data-id="'+RawData.company[a].company_id+'" href="javascript:;">'+RawData.company[a].name+'</a>'
                }
                $('#companyFilter').html(html);
            }
            buildpopover();
        }
    }});
}
function filterCashCounterReport(This){
    $("#txtFromDate").removeClass("required");
    $("#txtToDate").removeClass("required");
    $('#txtFromDate').parent().find('label').removeClass("active");
    $('#txtFromDate').parent().find('span.required-msg').remove();
    $('#txtToDate').parent().find('label').removeClass("active");
    $('#txtToDate').parent().find('span.required-msg').remove();
    var branchId = [];
    var courseId = [];
    var receiptmodeId = [];
    var companyId = [];
    var modeofpaymentId=[];
    var fromDate=$('#txtFromDate').val();
    var toDate=$('#txtToDate').val();
    if(fromDate.trim()!='') {
        var txtfromDate = dateConvertion(fromDate);
    }
    if(toDate.trim()!='') {
        var txttoDate = dateConvertion(toDate);
    }
    var error = true;
    $("#branchFilter > .active").each(function() {
        error = false;
        branchId.push($(this).attr('data-id'));
    });
    $("#companyFilter > .active").each(function() {
        error = false;
        companyId.push($(this).attr('data-id'));
    });
    $("#receiptmodeFilter > .active").each(function() {
        error = false;
        receiptmodeId.push("'"+$(this).attr('data-id')+"'");
    });
    $("#courseFilter > .active").each(function() {
        error = false;
        courseId.push($(this).attr('data-id'));
    });
    $("#modeofpaymentFilter > .active").each(function() {
        error = false;
        modeofpaymentId.push("'"+$(this).attr('data-id')+"'");
    });


    var branchId=branchId.join(',');
    var companyId=companyId.join(',');
    var receiptmodeId=receiptmodeId.join(',');
    var courseId=courseId.join(',');
    var modeofpaymentId=modeofpaymentId.join(',');
    if(branchId.trim()=='') {
        error = true;
        alertify.dismissAll();
        notify('Select any one branch', 'error');
        return false;
    }
    else if(modeofpaymentId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one mode of payment','error');
        return false;
    }
    else if(companyId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one company','error');
        return false;
    }
    else if(receiptmodeId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one receipt mode','error');
        return false;
    }
    else if(courseId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one course','error');
        return false;
    }
    else if($('#txtFromDate').val().trim()==''){
        error = true;
        alertify.dismissAll();
        notify('Select from date','error');
        return false;

    }
    else if($('#txtToDate').val().trim()==''){
        error = true;
        alertify.dismissAll();
        notify('Select to date','error');
        return false;

    }
    else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
        $("#txtFromDate").addClass("required");
        $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
        $("#txtToDate").addClass("required");
        $("#txtToDate").after('<span class="required-msg">Invalid</span>');
        alertify.dismissAll();
        notify('Invalid dates','error');
        error=true;
        return false;
    }
    else{
        error=false;
        var params = {'branch': branchId, 'course': courseId, 'receiptmode': receiptmodeId,company:companyId,fromDate:fromDate,toDate:toDate,paymentMode:modeofpaymentId};
        var ajaxurl=API_URL+'index.php/CashCounter/getCashCounterListReport';
        getCashCounterList(ajaxurl,params);

        var ajaxurl=API_URL+'index.php/CashCounter/getReceiptDeletionListReport';
        getReceiptDeletionList(ajaxurl,params);
    }
    if(!error){

    } else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}
function getReceiptDeletionList(ajaxurl,params){
    var action = 'report';
    var userID=$('#userId').val();
    var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
    $("#receiptdeletion-report-list-wrapper").show();
    $("#receiptdeletion-report-list").dataTable().fnDestroy();
    var currentObject = $('#receiptdeletion-report-list').DataTable( {
        "fnDrawCallback": function() {
            $("#receiptdeletion-report-list thead th").removeClass("icon-rupee");
            var $api = this.api();
            var pages = $api.page.info().pages;
            if(pages > 1)
            {
                $('#receiptdeletion-report-list-wrapper .dataTables_paginate').css("display", "block");
                $('#receiptdeletion-report-list-wrapper .dataTables_length').css("display", "block");
                //$('#receiptdeletion-report-list-wrapper .dataTables_filter').css("display", "block");
            } else {
                $('#receiptdeletion-report-list-wrapper .dataTables_paginate').css("display", "none");
                $('#receiptdeletion-report-list-wrapper .dataTables_length').css("display", "none");
                //$('#receiptdeletion-report-list-wrapper .dataTables_filter').css("display", "none");
            }
            verifyAccess();
            buildpopover();
        },
        "footerCallback": function ( row, data, start, end, display ) {
            $('#receiptdeletion-report-list_wrapper div.DTHeader').html('Deleted / Cheque Reversals');
            var api = this.api(), data;
            var colnumbers = [4];
            var colNames = ['amount'];
            deletedWrapper=0;
            calcTotals(0,deletedWrapper);
            $.each(colnumbers, function (keys, values) {
                var eachcolumn=values;
                var eachColumnname=colNames[keys];

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                // Total over all pages
                total = api
                    .column(eachcolumn)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b[eachColumnname]);
                    }, 0);

                // Total over this page
                pageTotal = api
                    .column(eachcolumn, {page: 'current'})
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b[eachColumnname]);
                    }, 0);
                calcTotals(0,total);
            });
        },
        dom: "<'DTHeader'>Bfrtip",
        bInfo: false,
        "serverSide": false,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No Records Found",
            "sZeroRecords": "No Records Found",
            "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
        },
        "bProcessing": true,
        ajax: {
            url:ajaxurl,
            type:'GET',
            data:params,
            headers:headerParams,
            error:function(response) {
                DTResponseerror(response);
            }
        },
        columns: [
            {
                data: null, render: function ( data, type, row )
            {
                return data.enrollNo;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {
                return data.leadName;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {

                return data.receiptNo;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {

                return data.remarks;
            }
            },
            {
                data: null,className:"text-right icon-rupee", render: function ( data, type, row )
            {
                return moneyFormat(data.amount);
            }
            },
            {
                data: null, render: function ( data, type, row )
            {

                return data.userName;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {

                return data.deletedBy;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {

                return data.deletedOn;
            }
            }
        ]
    });
    MultipleDTSearchEnter(currentObject, "#receiptdeletion-report-list");
}
function getCashCounterList(ajaxurl,params){
    var action = 'report';
    var userID=$('#userId').val();
    var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
    $("#cashcounter-report-list-wrapper").show();
    $("#cashcounter-report-list").dataTable().fnDestroy();
    var currentObject = $('#cashcounter-report-list').DataTable( {
        "fnDrawCallback": function() {
            $("#cashcounter-report-list thead th").removeClass("icon-rupee");
            var $api = this.api();
            var pages = $api.page.info().pages;
            if(pages > 1)
            {
                $('#cashcounter-report-list-wrapper .dataTables_paginate').css("display", "block");
                $('#cashcounter-report-list-wrapper .dataTables_length').css("display", "block");
                //$('#cashcounter-report-list-wrapper .dataTables_filter').css("display", "block");
            } else {
                $('#cashcounter-report-list-wrapper .dataTables_paginate').css("display", "none");
                $('#cashcounter-report-list-wrapper .dataTables_length').css("display", "none");
                //$('#cashcounter-report-list-wrapper .dataTables_filter').css("display", "none");
            }
            verifyAccess();
            buildpopover();
        },
        "footerCallback": function ( row, data, start, end, display ) {
            $('#cashcounter-report-list_wrapper div.DTHeader').html('Cash Counter');
            var api = this.api(), data;
            var colnumbers = [5];
            var colNames = ['amount'];
            paidWrapper=0;
            calcTotals(paidWrapper,0);
            $.each(colnumbers, function (keys, values) {
                var eachcolumn=values;
                var eachColumnname=colNames[keys];

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                // Total over all pages
                total = api
                    .column(eachcolumn)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b[eachColumnname]);
                    }, 0);

                // Total over this page
                pageTotal = api
                    .column(eachcolumn, {page: 'current'})
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b[eachColumnname]);
                    }, 0);
                calcTotals(total,0);
            });
        },
        dom: "<'DTHeader'>Bfrtip",
        bInfo: false,
        "serverSide": false,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No Records Found",
            "sZeroRecords": "No Records Found",
            "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
        },
        "bProcessing": true,
        ajax: {
            url:ajaxurl,
            type:'GET',
            data:params,
            headers:headerParams,
            error:function(response) {
                DTResponseerror(response);
            }
        },
        columns: [
            {
                data: null, render: function ( data, type, row )
            {
                return data.enrollNo;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {
                return data.leadName;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {

                return data.receiptNo;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {

                return data.payment_mode;
            }
            },
            {
                data: null, render: function ( data, type, row )
            {

                return data.remarks;
            }
            },
            {
                data: null,className:"text-right icon-rupee", render: function ( data, type, row )
            {
                return moneyFormat(data.amount);
            }
            },
            {
                data: null, render: function ( data, type, row )
            {

                return data.userName;
            }
            }
        ]
    } );
    MultipleDTSearchEnter(currentObject, "#cashcounter-report-list");
}
var paidWrapper=0;
var deletedWrapper=0;
function calcTotals(paid,deleted){
    paidWrapper=paidWrapper+paid;
    deletedWrapper=deletedWrapper+deleted;
    $('#spanPaidAmnt').html(moneyFormat(paidWrapper));
    $('#spanDeletedAmnt').html(moneyFormat(deletedWrapper));
    $('#spanTotalAmnt').html(moneyFormat(paidWrapper+deletedWrapper));

}
function filterCashCounterReportExport(This){
    $("#txtFromDate").removeClass("required");
    $("#txtToDate").removeClass("required");
    $('#txtFromDate').parent().find('label').removeClass("active");
    $('#txtFromDate').parent().find('span.required-msg').remove();
    $('#txtToDate').parent().find('label').removeClass("active");
    $('#txtToDate').parent().find('span.required-msg').remove();
    var branchId = [];
    var branchIdText = [];
    var courseId = [];
    var courseIdText = [];
    var receiptmodeId = [];
    var receiptmodeIdText = [];
    var companyId = [];
    var companyIdText = [];
    var modeofpaymentId = [];
    var modeofpaymentIdText = [];
    var fromDate=$('#txtFromDate').val();
    var toDate=$('#txtToDate').val();
    if(fromDate.trim()!='') {
        var txtfromDate = dateConvertion(fromDate);
    }
    if(toDate.trim()!='') {
        var txttoDate = dateConvertion(toDate);
    }
    var error = true;
    $("#branchFilter > .active").each(function() {
        error = false;
        branchId.push($(this).attr('data-id'));
        branchIdText.push($(this).text());
    });
    $("#modeofpaymentFilter > .active").each(function() {
        error = false;
        modeofpaymentId.push("'"+$(this).attr('data-id')+"'");
        modeofpaymentIdText.push($(this).text());
    });
    $("#companyFilter > .active").each(function() {
        error = false;
        companyId.push($(this).attr('data-id'));
        companyIdText.push($(this).text());
    });
    $("#receiptmodeFilter > .active").each(function() {
        error = false;
        receiptmodeId.push("'"+$(this).attr('data-id')+"'");
        receiptmodeIdText.push($(this).text());
    });
    $("#courseFilter > .active").each(function() {
        error = false;
        courseId.push($(this).attr('data-id'));
        courseIdText.push($(this).text());
    });

    var branchId=branchId.join(', ');
    var companyId=companyId.join(', ');
    var receiptmodeId=receiptmodeId.join(', ');
    var courseId=courseId.join(', ');
    var modeofpaymentId=modeofpaymentId.join(', ');
    if(branchId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one branch','error');
        return false;
    }
    else if(companyId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one company','error');
        return false;
    }
    else if(receiptmodeId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one receipt mode','error');
        return false;
    }
    else if(courseId.trim()==''){
        error=true;
        alertify.dismissAll();
        notify('Select any one course','error');
        return false;
    }
    else if($('#txtFromDate').val().trim()==''){
        error = true;
        alertify.dismissAll();
        notify('Select from date','error');
        return false;

    }
    else if($('#txtToDate').val().trim()==''){
        error = true;
        alertify.dismissAll();
        notify('Select to date','error');
        return false;

    }
    else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
        $("#txtFromDate").addClass("required");
        $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
        $("#txtToDate").addClass("required");
        $("#txtToDate").after('<span class="required-msg">Invalid</span>');
        alertify.dismissAll();
        notify('Invalid dates','error');
        error=true;
        return false;
    }
    else{
        error=false;
    }

    if(!error){
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var action = 'report';
        var ajaxurl=API_URL+'index.php/CashCounter/getCashCounterListReportExport';
        var params = {'branch': branchId, 'course': courseId, 'receiptmode': receiptmodeId,'company':companyId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '), 'receiptmodeText': receiptmodeIdText.join(', '),'companyText': companyIdText.join(', '),fromDate:fromDate,toDate:toDate,paymentMode:modeofpaymentId,'paymentModeText': modeofpaymentIdText.join(', ')};
        var userID=$('#userId').val();
        var type='GET';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response =
            commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterCashCounterReportExportResponse});
    } else {
        alertify.dismissAll();
        notify('Select any filter', 'error', 10);
    }
}
function filterCashCounterReportExportResponse(response){
    alertify.dismissAll();
    if(response.status===true){
        var download=API_URL+'Download/downloadReport/'+response.file_name;
        window.location.href=(download);
    }
    else{
        notify(response.message,'error');
    }
}
/* Cash Counter list report ends here */


/*For autocomplete for */
function loadBranchUsersAutocompleteNew(){
    $("#autocomplete").val('');
    $('#autocomplete').next('ul').remove();
    $("#userBranchId").val(0);
    
    var headerParams = {
        action: 'manage users',
        context: $context,
        serviceurl: $pageUrl,
        pageurl: $pageUrl,
        Authorizationtoken: $accessToken,
        user: $('#hdnUserId').val()
    };
    $("#autocomplete").jsonSuggest({
            onSelect:function(item){
                    $("#txtOldStudents").val(item.text);
                    var split=item.id;
                    $('#userBranchId').val(split);

            },
            headers:headerParams,
            url:API_URL + 'index.php/Branch/getManageUsers' ,
            minCharacters: 2});
}
function multiSelectChosenDropdown(This) {
    var SelectBox = '#' + $(This).attr('id');
    $(SelectBox + ' > :selected').each(function () {
        if ($(this).text() == '--Select All--') {

            $(SelectBox + ' option').prop('selected', true);
            $(this).text('--De Select All--');
            $(this).prop('selected', false);
            //$(SelectBox).material_select();
            $(SelectBox).trigger("chosen:updated");
        } else if ($(this).text() == '--De Select All--') {
            $(SelectBox + ' option').prop('selected', false);
            $(this).text('--Select All--');
            $(this).prop('selected', false);
            //$(SelectBox).material_select();
            $(SelectBox).trigger("chosen:updated");
        }
    });
    var TotalCnt = $(SelectBox + ' option').size() - 1;
    var SelCnt = $(SelectBox + ' > :selected').length;
    $(SelectBox + ' > option').each(function () {

        if (SelCnt < TotalCnt && SelCnt > 0) {
            if ($(this).text() == '--De Select All--')
            {
                $(this).text('--Select All--');
                //$(SelectBox).material_select();
                $(SelectBox).trigger("chosen:updated");
            }

        }
        if (SelCnt == TotalCnt) {

            if ($(this).text() == '--Select All--')
            {
                $(this).text('--De Select All--');
                $(SelectBox).material_select();
            }
        }
        if (SelCnt == 0) {
            if ($(this).text() == '--De Select All--')
            {
                $(this).text('--Select All--');
                //$(SelectBox).material_select();
                $(SelectBox).trigger("chosen:updated");
            }

        }

    });
}
function downloadInvoice(filename){
    alertify.dismissAll();
    var download=API_URL+'Download/downloadFile/'+filename;
    window.location.href=(download);
}
function getBatchesByBranchCourse()
{
    var branchId = [];
    var courseId = [];
    var error = true;
    $("#branchFilter > .active").each(function() {
        error = false;
        branchId.push($(this).attr('data-id'));
    });
    $("#courseFilter > .active").each(function() {
        error = false;
        courseId.push($(this).attr('data-id'));
    });
    if(branchId.length==0 || courseId.length==0)
    {
        $('#ddlBatchesList').empty();
        $('#ddlBatchesList').trigger("chosen:updated");
    }
    else
    {
        var branchId=branchId.join(', ');
        var courseId=courseId.join(', ');
        var userId = $('#userId').val();
        var action = 'report';
        var batcheslist = '';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        var params = {branchId: branchId, courseId: courseId};
        var ajaxurl=API_URL+'index.php/FeeDetails/getBatchesByBranchCourse';
        commonAjaxCall({This:this, headerParams:headerParams,params:params, requestUrl:ajaxurl,action:action,onSuccess:function(response)
        {
            if(response.status == true)
            {
                var batcheslist = response.data;
                //batcheslist = $.merge(RawData.previousbatches,RawData.currentbatches);
                $('#ddlBatchesList').empty();
                $('#ddlBatchesList').append($('<option></option>').val('').html('--Select All--'));
                if (batcheslist.length > 0)
                {
                    $.each(batcheslist, function (key, value)
                    {
                        $('#ddlBatchesList').append($('<option></option>').val(value.batch_id).html(value.code));
                    });
                }
                $("#ddlBatchesList").chosen({
                    no_results_text: "Oops, nothing found!",
                    width: "95%"
                });
                $('#ddlBatchesList').trigger("chosen:updated");
                $("#ddlBatchesList").next('label').addClass("active");
            }
            else
            {
                $('#ddlBatchesList').empty();
                $('#ddlBatchesList').trigger("chosen:updated");
                notify('Something went wrong. Please try again','error');
            }
        }
        });
    }
}

function getLeadContactTypes(This) {
    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
    var params = {name: 'Contact Type'};
    var action = 'add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    var response =
        commonAjaxCall({This: This, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadContactTypesResponse});
}
function getLeadContactTypesResponse(response) {

    var rowHtml = '';
    if (response.status === true) {
        var rdo_checked='';
        if (response.data.length > 0) {
            rowHtml += "<div class=\"multiple-radio\" id=\"contacatTypes\">";
            rowHtml += "<label>Lead Type</label>";
            $.each(response.data, function (key, value)
            {
                if(value.value == 'Retail')
                {
                    rdo_checked="checked";
                }
                else
                {
                    rdo_checked="";
                }
                rowHtml += "<span class=\"inline-radio\">";
                rowHtml += "<input class=\"with-gap\" name=\"coldCallcontacttype\" type=\"radio\" id=\"contacttype" + value.reference_type_value_id + "\" value=\"" + value.value + "\""+rdo_checked+" />";
                rowHtml += "<label for=\"contacttype" + value.reference_type_value_id + "\">" + value.value + "</label>";
                rowHtml += "</span>";
            });
            rowHtml += "</div>";
        }
   }
    $('#LeadCreateContactTypeWrapper').html(rowHtml);
    $("input[name='coldCallcontacttype']").click(function () {
        $('#ManageCreateLeadForm input').removeClass("required");
        $('#ManageCreateLeadForm select').removeClass("required");
        $('#ManageCreateLeadForm textarea').removeClass("required");
        $('#ManageCreateLeadForm span.required-msg').remove();
        var contacttypeVal = $(this).val();
        $("#txtLeadContactCompany").val('').trigger("change");
        $('#txtLeadContactInstitution').val('').trigger("change");
        LeadCreateresetCompanyWrapper(this);
        $('#ComapnyWrapper').slideUp();
        LeadCreateresetInstitutionWrapper(this);
        $('#InstitutionWrapper').slideUp();
        if (contacttypeVal == 'Corporate') {
            $("#contactType-Institution-wrap").hide();
            $("#contactType-corporate-wrap").slideDown();
        }
        if (contacttypeVal == 'University') {
            $("#contactType-corporate-wrap").hide();
            $("#contactType-Institution-wrap").slideDown();
        }
        if (contacttypeVal == 'Retail') {
            $("#contactType-corporate-wrap").hide();
            $("#contactType-Institution-wrap").hide();
        }
    });
    $("input[name='coldCallcontacttype']:checked").click();
}

function LeadCreateresetCompanyWrapper(This) {
    $('#ComapnyWrapper span.required-msg').remove();
    $('#ComapnyWrapper label').removeClass("active");
    $('#ComapnyWrapper input').removeClass("required");
    $('#txtComapnyNameWrapper').val('');
}
function LeadCreateresetInstitutionWrapper(This) {
    $('#InstitutionWrapper span.required-msg').remove();
    $('#InstitutionWrapper label').removeClass("active");
    $('#InstitutionWrapper input').removeClass("required");
    $('#txtInstitutionNameWrapper').val('');
}
function getLeadCreatePostQualifications() {
    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
    var params = {name: 'Post Qualification'};
    var action = 'add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    var response =
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadCreatePostQualificationsResponse});
}
function getLeadCreatePostQualificationsResponse(response) {
    $('#txtLeadContactPostQualification').empty();
    $('#txtLeadContactPostQualification').append($('<option selected></option>').val('').html('--Select--'));
    if (response.status == true) {

        if (response.data.length > 0) {

            $.each(response.data, function (key, value) {

                $('#txtLeadContactPostQualification').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
            });
        }
    }
    $('#txtLeadContactPostQualification').material_select();
    $('#txtLeadContactPostQualification').trigger('change');
}
function getLeadCreateQualifications() {
    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
    var parent_id='';
    if($('#txtLeadContactPostQualification').length>0){
        parent_id=$('#txtLeadContactPostQualification').val();
        if(parent_id==''){
            parent_id='N/A';
        }
    }
    var params = {name: 'Qualification',parent_id:parent_id};
    var action = 'add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    var response =
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadCreateQualificationsResponse});
}
function getLeadCreateQualificationsResponse(response) {
    $('#txtLeadContactQualification').empty();
    $('#txtLeadContactQualification').append($('<option selected></option>').val('').html('--Select--'));
    if (response.status == true) {

        if (response.data.length > 0) {

            $.each(response.data, function (key, value) {

                $('#txtLeadContactQualification').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
            });
        }
    }
    $('#txtLeadContactQualification').material_select();
}
function getLeadCreateCompanies() {

    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
    var params = {name: 'Company'};
    var action = 'add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    var response =
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadCreateCompaniesResponse});

}
function getLeadCreateCompaniesResponse(response) {
    $('#txtLeadContactCompany').empty();
    $('#txtLeadContactCompany').append($('<option selected></option>').val('').html('--Select--'));
    if (response.status == true) {

        if (response.data.length > 0) {

            $.each(response.data, function (key, value) {

                $('#txtLeadContactCompany').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
            });
            $('#txtLeadContactCompany').trigger("chosen:updated");
        }
    }
    //$("#txtLeadContactCompany").addClass('chosen-select');
    $("#txtLeadContactCompany").chosen({
        no_results_text: "Oops, nothing found!",
        width: "95%"
    });
    //$('#txtLeadContactCompany').material_select();
}
function getLeadCreateInstitutions() {

    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
    var params = {name: 'Institution'};
    var action = 'add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    var response =
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadCreateInstitutionsResponse});

}
function getLeadCreateInstitutionsResponse(response) {

    $('#ManageCreateLeadForm span.required-msg').remove();
    $('#ManageCreateLeadForm input').removeClass("required");
    $('#ManageCreateLeadForm select').removeClass("required");

    $('#txtLeadContactInstitution').empty();
    $('#txtLeadContactInstitution').append($('<option selected></option>').val('').html('--Select--'));
    if (response.status == true) {

        if (response.data.length > 0) {

            $.each(response.data, function (key, value) {

                $('#txtLeadContactInstitution').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
            });
            $('#txtLeadContactInstitution').trigger("chosen:updated");
        }
    }
    $("#txtLeadContactInstitution").chosen({
        no_results_text: "Oops, nothing found!",
        width: "95%"
    });
    //$('#txtLeadContactInstitution').material_select();
}
function branchVisitleadStatuses(This,currentStage='')
{
    var userID=$('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/LeadStatus/getBranchVisitLeadStages';
    var params = {currentStage:currentStage};
    var type = "GET";
    var action='list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    commonAjaxCall({This: This, method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: branchVisitleadStatusesResponse});
}
function branchVisitleadStatusesResponse(response){
    $('#CallAnswerTypes').empty();
    $('#CallAnswerTypes').append($('<option></option>').val('').html('--Select--'));
    if(response.status===true && (typeof response.data.options!='undefined')){
        $.each(response.data.options, function (key, value)
        {
            var attrDate='';
            if(value['followup date label']){
                var attrDate=value['followup date label'];
            }
            else{
                var attrDate='Next Followup Date';
            }
            if((typeof value['key'] != "undefined") && value['key'].length>0 && (typeof value['value'] != "undefined") && value['value'].length>0) {
                $('#CallAnswerTypes').append($('<option></option>').val(value['key']).html(value['value']).attr('data-date', attrDate));
            }
        });
    }
    else
    {

    }
    $('#CallAnswerTypes').material_select();
    $('#CallAnswerTypes').trigger('change');
}
function manageWarehouseGrn(This,Id)
{

    $('#warehouseGrn_btn').html('');
    if(Id==0)
    {
        $('#warehouseGrn_btn').html('<i class="icon-right mr8"></i>Add');
    }
    else
    {
        $('#warehouseGrn_btn').html('<i class="icon-right mr8"></i>Update');
    }
    //$('#warehouse_grn')[0].reset();
    $('#warehouse_grn span.required-msg').remove();
    $('#warehouse_grn input').removeClass("required");
    $("#warehouse_grn").removeAttr("disabled");
    $('#warehouseGrnForm')[0].reset();
    $('#txtinvoice').val('');
    $('span.required-msg').remove();
    $('#vendor').parent().find('.select-dropdown').removeClass('required');
    $("#vendor").parent().find(".required-msg").remove();
    $('#txtinvoice').removeClass("required");
    $("#txtinvoice").removeAttr("disabled");
    $('#txtRemarks').val('');
    $('#txtfileinvoice').val('');
    $('#txtfileinvoice').trigger('change');
    Materialize.updateTextFields();
    $('#warehouseGrnForm span.required-msg').remove();
    $('#warehouseGrnForm input').removeClass("required");
    $("#warehouseGrnForm").removeAttr("disabled");
    $('#warehouse-grn-products tbody').html('');

    $('#warehouseGtnForm span.required-msg').remove();
    $('#warehouseGtnForm input').removeClass("required");
    $("#warehouseGtnForm").removeAttr("disabled");
    $('#warehouse-gtn tbody').html('');

    $('#hdnSentBy').val('');
    $('#hdnMode').val('');
    $('#hdnProductId').val('');
    $('#stock_flow_id').val('');
    $('input[type=radio][name=receivedFrom][value="branch"]').click();

    rowNum=0;
    gtnProducts = new Object();
    $('#warehouse-gtn tbody').html('');
    var userID = $("#hdnUserId").val();
    if (Id == 0)
    {
        $("#myModalLabel").html("Create GRN");
        getBranchGtnNumbers();
        getProducts();
    }
    $('#myGrnModal').modal('show');
}