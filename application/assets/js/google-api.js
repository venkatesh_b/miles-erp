/**
 * Created by VENKATESH.B on 9/2/16.
 */

function login()
{
    var myParams =
    {
        'clientid' : GOOGLE_CLIENT_ID,
        'cookiepolicy' : 'single_host_origin',
        'callback' : 'loginCallback',
        'approvalprompt':'force',
        'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
    };
    gapi.auth.signIn(myParams);
}

function loginCallback(result)
{
    if(result['status']['signed_in'])
    {
        var request = gapi.client.plus.people.get(
            {
                'userId': 'me'
            });
        request.execute(function (resp)
        {
            var email,name,gender,profileImage,accountId;
            if(resp['emails'])
            {
                for(i = 0; i < resp['emails'].length; i++)
                {
                    if(resp['emails'][i]['type'] == 'account')
                    {
                        email = resp['emails'][i]['value'];
                    }
                }
            }
            name=resp['name']['givenName'];
            if(resp['gender']=="male")
            {
                gender=1;
            }
            else
            {
                gender=0;
            }

            profileImage=resp['image']['url'];
            accountId=resp['id'];

            //var ajaxurl=BASE_URL+'index.php/login/authorizeUser';
            var ajaxurl=BASE_URL+'index.php/authorize';
            var headerParams = {context:'default'};
            var params = {emailId: email, userName: name, gender: gender, profileImage:profileImage, accountId:accountId};
            $.ajax({
                async:false,
                type:"POST",
                url:ajaxurl,
                dataType:"json",
                data:params,
                headers: headerParams,
                beforeSend:function()
                {
                    $(".has-spinner").addClass('active');
                    $('.has-spinner .google-btn-text').hide();
                },
                success:function(response)
                {
                    if(response == -1)
                    {
                        $(".has-spinner").removeClass('active');
                        $('.has-spinner .google-btn-text').show();
                        notify('You are not authorised user','error' ,10);
                        gapi.auth.signOut();
                    }
                    else
                    {
                        $context=response.data['context'];
                        headerParams = {context:$context,Authorizationtoken:response.data['accessToken'],user:response.data['user']};
                        ajaxurl=API_URL+'index.php/ApplicationMenu/getApplicationMenu/Userid/'+response.data['user'];
                        commonAjaxCall({This:this,requestUrl:ajaxurl,headerParams:headerParams,onSuccess:userAccessDetails}).then(function()
                            {
                                window.location=APP_REDIRECT+response.message;
                            }
                        );
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown)
                {
                    console.log(XMLHttpRequest,textStatus,errorThrown);
                    notify('Something went wrong. Please try again','error' ,10);
                }
            });
        });
    }
}

function userAccessDetails(userResponse)
{
    localStorage.setItem("AppMenu", JSON.stringify(userResponse.data.AppMenu));
    localStorage.setItem("Branches", JSON.stringify(userResponse.data.Branches));
}

function onLoadCallback()
{
    gapi.client.setApiKey(GOOGLE_API_KEY);
    gapi.client.load('plus', 'v1',function(){});
}

(function()
{
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();