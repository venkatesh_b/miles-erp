$(window).load(function () {
    $body = $('body');
    $('body[data-theme="change"] .logo-change').css('margin-left', -($('body[data-theme="change"] .logo-change')).width() / 2)
    // Page Preloader
    $('#body-status').fadeOut();
    $('#preloader').fadeOut();
    $leftPan = $('.leftpanel'), $mainPan = $('.mainpanel'), $mainWA = $('.main-work-area');
   
    $('.menutoggle').click(function () {
        var cur = $(this);
        if ($body.hasClass('leftpanel-collapsed')) {
            $body.removeClass('leftpanel-collapsed');
            $('.nav-active-sm').addClass('nav-active').removeClass('nav-active-sm')/*.find('ul.children').show()*/;
        } else {
            $body.addClass('leftpanel-collapsed');
            $('.nav-active').addClass('nav-active-sm').removeClass('nav-active').find('ul.children').hide();
        }
    });
    $(window).resize(function () {
        var mainPH = $(window).height() - $('.headerbar').height();
        $mainPan.css('height', mainPH);
        $leftPan.css('height', mainPH-42);
        $mainWA.css('min-height', mainPH - $('.footer-logo').height() - 30);

        //UI DIV FOR REMAINING WIDTH - START
        var remParent = $('.remaining-width').parent();
        var remPrevElem = $('.remaining-width').prev();
        var parWid = parseInt(remParent.width()), prevWid = parseInt(remPrevElem.width());
        var remWidth = parseInt(parWid - prevWid);
        remParent.css('padding', 0);
        remPrevElem.css('float', 'left');
        $('.remaining-width').width(remWidth);
        //UI DIV FOR REMAINING WIDTH - END

    });
    $(window).resize();
    $mainPan.mCustomScrollbar({
        theme: "dark", scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true,
        callbacks: {
            onOverflowY: function () {
                $('.mCSB_inside > .mCSB_container').css('margin-right', 15);
            },
            onOverflowYNone: function () { $('.mCSB_inside > .mCSB_container').css('margin-right', 15); },
            onScroll: function () { $('.tooltip').removeClass('in'); }
        }
    });
    $leftPan.mCustomScrollbar({ theme: "dark", scrollbarPosition: 'outside', autoHideScrollbar: true, scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true });

});
