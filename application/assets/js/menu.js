function logout()
{
    localStorage.removeItem("AppMenu");
    localStorage.removeItem("Branches");
    window.location=APP_REDIRECT+'logout';
}
$(function(){
  buildUserMenu();
});

function buildUserMenu()
{
    /*
     Author:Venkatesh
     Description: Builds menu items and Branches dropdown present in header
     */
    var menuRawData = $.parseJSON(localStorage.getItem("AppMenu"));
    var branchRawData = $.parseJSON(localStorage.getItem("Branches"));
    //Menu building starts
    var menuData='',menuActions='',menuIcon='',links=[],list=[];
    if(menuRawData != null)
    {
        if(menuRawData.length > 0)
        {
            for(var i=0;i<menuRawData.length;i++)
            {
                menuIcon='<i style="width:18px;overflow:hidden"';
                if(menuRawData[i]['fa_icon']==null)
                {
                    menuIcon+=' class="fa">'+menuRawData[i]['name'][0]+'</i>';
                }
                else
                {
                    menuIcon+=' class="fa '+menuRawData[i]['fa_icon']+'"></i>';
                }
                if(menuRawData[i]['childs'] ==undefined || menuRawData[i]['childs'].length == 0)
                {
                    menuActions='';
                    if(menuRawData[i]['action'].length>0)
                    {
                        menuActions=menuRawData[i]['action'].join('__@__');
                    }
                    if(menuRawData[i]['name']=='SR') {
                        menuData += '<li style="display:block" tss-actions="' + menuActions + '" tss-url="' + menuRawData[i]['url'] + '" tss-context="' + menuRawData[i]['context'] + '" ><a href="' + APP_REDIRECT + menuRawData[i]['url'] + '">' + menuIcon + '<span>' + menuRawData[i]['name'] + '</span></a></li>';
                    }
                    else{
                        menuData += '<li tss-actions="' + menuActions + '" tss-url="' + menuRawData[i]['url'] + '" tss-context="' + menuRawData[i]['context'] + '" ><a href="' + APP_REDIRECT + menuRawData[i]['url'] + '">' + menuIcon + '<span>' + menuRawData[i]['name'] + '</span></a></li>';
                        var obj = {};
                        obj.Name = menuRawData[i]['name'];
                        obj.Link = APP_REDIRECT + menuRawData[i]['url'];
                        list.push(obj);
                        links.push(obj.Link);
                    }
                }
                else
                {
                    menuData+='<li class="nav-parent"><a href="javascript:;">'+menuIcon+'<span>'+menuRawData[i]['name']+'</span></a>';
                    menuData+='<ul class="children">';
                    for(var j=0;j<menuRawData[i]['childs'].length;j++)
                    {
                        menuActions='';
                        if(menuRawData[i]['childs'][j]['action'])
                        {
                            menuActions=menuRawData[i]['childs'][j]['action'].join('__@__');
                        }
                        menuData+='<li tss-actions="'+menuActions+'" tss-url="'+menuRawData[i]['childs'][j]['url']+'" tss-context="'+menuRawData[i]['childs'][j]['context']+'" ><a href="'+APP_REDIRECT+menuRawData[i]['childs'][j]['url']+'"><i class="fa fa-caret-right"></i><span>'+menuRawData[i]['childs'][j]['name']+'</span></a></li>';
                        var obj = {};
                        obj.Name = menuRawData[i]['childs'][j]['name'];
                        obj.Link = APP_REDIRECT+menuRawData[i]['childs'][j]['url'];
                        list.push(obj);
                        links.push(obj.Link);
                    }
                    menuData+='</ul></li>';
                }
            }
            $('#left-panel-main-menus-ul li:eq(0)').after(menuData);
            /*$("#left-panel-main-menus-ul").find('.nav-parent').on('click', function () {
                if (!$(this).hasClass('active'))
                {
                    $("#left-panel-main-menus-ul").find('.nav-parent').removeClass('active');
                    //$("#left-panel-main-menus-ul").find('.nav-parent').removeClass('nav-active');
                    $(this).addClass('active');
                    $("#left-panel-main-menus-ul").find('.children').slideUp();
                    $(this).find('.children').slideDown();
                }
            });*/
            //debugger;
            preSelectMenu(list);

        }
    }

    //Menu building Ends

    //Branches dropdown starts
    var branchData='',isDefault='',pageUrl=$pageUrl.toLowerCase();
    var arr = [ "coldcallscheduler","coldcalling","feecollection","feecollection_create","database","leadtransfer","branchvisit","secondlevelcounsel","retail_leadfollowup","corporate_leadfollowup","institutional_leadfollowup","mwbscheduler","dailyrevenueposting","dailyrevenueauthorization","feedefaulters","dashboard","branches","feeassign","students","branchgrn","branchgtn","branchstock","branch_dashboard","lead_conversion","feemodificationrequest","feeconcession","feeconcessionrequest","netenquiry","retail_leads","corporate_leads","institutional_leads","mwb_corporatescheduler","mwb_institutionalscheduler","students_alumini","student_schedular","student_followup"];
    if(branchRawData != null)
    {
        if(branchRawData.length > 0 && jQuery.inArray( pageUrl, arr ) >=0)
        {
            for (var b = 0; b < branchRawData.length; b++) {
                var o = $('<option/>', { value: branchRawData[b]['branch_id'] })
                    .text(branchRawData[b]['name'])
                    .prop('selected', branchRawData[b]['is_default_branch'] == 1);
                o.appendTo('#userBranchList');
            }
            $('#userBranchList').material_select();
        }
        else
        {
            $('#userBranchList').remove();
        }
    }

    //Branches dropdown ends
}


function preSelectMenu(list) {

    var url = decodeURIComponent(window.location.href);
    url=url.split('/')[url.split('/').length-1];
    $("a[href$='" + url + "']").closest('li').parent().parent().addClass('nav-active');
    $("a[href$='" + url + "']").closest('li').parent().css('display','block');
    $("a[href$='" + url + "']").closest('li').addClass('active');
    searchInMenu(list);
    _menuLeft();
}
function searchInMenu(list) {
    if($("#menuSearchDemo").length >0)
    {
        $("#menuSearchDemo").autocomplete({
            minLength: 0,
            appendTo: "#left-panel-main-menus-ul",
            source: list,
            focus: function (event, ui) {
                $("#menuSearchDemo").val(ui.item.Name);
                return false;
            }
        })
            .data('ui-autocomplete')._renderItem = function (ul, item)
        {
            return $('<li>')
                .append('<a  href="' + item.Link + '">' + item.Name + '</a>')
                .appendTo(ul);
        };

        $.ui.autocomplete.filter = function (array, term) {
            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
            return $.grep(list, function (value) {
                return matcher.test(value.Name);
            });
        };

        $('.leftpanel-collapsed .showSearch').on('hover', function () {
            $(this).closest('li.search-menu').addClass('searchCollapsedHover');
        });

        $('.leftpanel-collapsed .ui-autocomplete').mouseenter(function () {
            $('.search-menu').addClass('nav-hover');
        });
    }
}

function _menuLeft() {
    $('.leftpanel .nav-bracket > li.nav-hover').unbind('mouseleave');
    $('.leftpanel .nav-bracket > li.nav-hover').unbind('mouseenter');
    $('.leftpanel .nav-parent > a').unbind('click');
    $('.leftpanel .nav-bracket > li').mouseenter(function (event) {
        var cur = $(this);
        var halfLPHt = parseInt(($(window).height() - 80) / 2);
        if ($body.hasClass('leftpanel-collapsed')) {
            cur.addClass('nav-hover');
            if (cur.offset().top < halfLPHt) {
                cur.find('ul.children').show().css({ 'top': cur.offset().top + (cur.height() + 40) });
            } else {
                cur.find('ul.children').show().css({ 'top': cur.offset().top - cur.find('ul.children').height() + 40 });
            }
            $("li.nav-hover ul.children").mCustomScrollbar({ theme: "light", scrollbarPosition: 'outside', autoHideScrollbar: true, scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true });
        }
    }).mouseleave(function (event) {
        var cur = $(this);
        cur.removeClass('nav-hover');
        cur.find('ul.children').css({ 'top': '' });
        if ($body.hasClass('leftpanel-collapsed'))
            cur.find('ul.children').hide();
    });
}