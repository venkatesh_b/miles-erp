/**
 * Created by JAGADEESH.T on 19/2/16.
 */
function verifyAccess(){
    try{if(!$context)$context=null;}catch(e){$context=null;}
    try{if(!$actions)$actions=null;}catch(e){$actions=null;}
    if($context==undefined || isBlank($context))
    {
        if($pageUrl == 'dummy')
        {
            var path=window.location.pathname;
            $pageUrl=path.substr(path.lastIndexOf('/') + 1);
            $context=$('li[tss-url="'+$pageUrl+'"]').attr('tss-context');//'Dummy page';
            if($context == undefined)
            {
                $( location ).attr("href", BASE_URL);
            }
        }
        else
        {
            $context=$('li[tss-url="'+$pageUrl+'"]').attr('tss-context');//'Role Management';
        }
    }
    var ele=$('li[tss-context="'+$context+'"]')
    if(!ele.hasClass('active')){
        ele.addClass('active');
        if(ele.parent().parent().hasClass('nav-parent') && !ele.parent().parent().hasClass('nav-active')){
            ele.parent().parent().addClass('nav-active');
            ele.parent().parent().find('>ul').show();
        }
    }
    if($context!='default')
    {
        $actions=$('li[tss-context="'+$context+'"]').attr('tss-actions').toLowerCase().split('__@__');
    }
    if($actions==undefined || $actions==null || $actions.length==0){
        //Redirect to no menu page
    }else{
        //Remove no access elements
        //console.log(actions);
        $.each($('body *[access-element]'),function(){
            if($actions.indexOf($(this).attr('access-element').toLowerCase())==-1)
                $(this).remove()
        });
    }
}
function sl(obj){
    if(true)
        console.log(obj);
}
function isBlank(ele) {
    if (typeof (ele) != 'object' && (ele === undefined || ele === null || $.trim(ele + '') === ''))
        return true;
    else {
        if (ele == null || ($.isArray(ele) && ele.length == 0))
            return true;
        return false;
    }
}
function docReady(call, ajaxCall) {
    $(function () {
        verifyAccess();
        if (isBlank(ajaxCall))
            ajaxCall = [];
        $.when.apply($, ajaxCall).done(function () {
            call();
        });
    });
}