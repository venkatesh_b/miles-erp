$(window).load(function () {
    $body = $('body');
    $('body[data-theme="change"] .logo-change').css('margin-left', -($('body[data-theme="change"] .logo-change')).width() / 2)
    // Page Preloader
    $('#body-status').fadeOut();
    $('#preloader').fadeOut();
    $leftPan = $('.leftpanel'), $mainPan = $('.mainpanel'), $mainWA = $('.main-work-area');
   
    $('.menutoggle').click(function () {
        var cur = $(this);
        if ($body.hasClass('leftpanel-collapsed')) {
            $body.removeClass('leftpanel-collapsed');
            $('.nav-active-sm').addClass('nav-active').removeClass('nav-active-sm')/*.find('ul.children').show()*/;
        } else {
            $body.addClass('leftpanel-collapsed');
            $('.nav-active').addClass('nav-active-sm').removeClass('nav-active').find('ul.children').hide();
        }
    });
    $(window).resize(function () {
        var mainPH = $(window).height() - $('.headerbar').height();
        $mainPan.css('height', mainPH);
        $leftPan.css('height', mainPH-42);
        $mainWA.css('min-height', mainPH - $('.footer-logo').height() - 30);

        //UI DIV FOR REMAINING WIDTH - START
        var remParent = $('.remaining-width').parent();
        var remPrevElem = $('.remaining-width').prev();
        var parWid = parseInt(remParent.width()), prevWid = parseInt(remPrevElem.width());
        var remWidth = parseInt(parWid - prevWid);
        remParent.css('padding', 0);
        remPrevElem.css('float', 'left');
        $('.remaining-width').width(remWidth);
        //UI DIV FOR REMAINING WIDTH - END

    });
    $(window).resize();
    $mainPan.mCustomScrollbar({
        theme: "dark", scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true,
        callbacks: {
            onOverflowY: function () {
                $('.mCSB_inside > .mCSB_container').css('margin-right', 15);
            },
            onOverflowYNone: function () { $('.mCSB_inside > .mCSB_container').css('margin-right', 15); },
            onScroll: function () { $('.tooltip').removeClass('in'); }
        }
    });
    $leftPan.mCustomScrollbar({ theme: "dark", scrollbarPosition: 'outside', autoHideScrollbar: true, scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true });

});
function _menuLeft(e) {
    $('.leftpanel .nav-bracket > li.nav-hover').unbind('mouseleave');
    $('.leftpanel .nav-bracket > li.nav-hover').unbind('mouseenter');
    $('.leftpanel .nav-parent > a').unbind('click');
    $('.leftpanel .nav-bracket > li').mouseenter(function (event) {
        var cur = $(this);
        var halfLPHt = parseInt(($(window).height() - 80) / 2);
        if ($body.hasClass('leftpanel-collapsed')) {
            cur.addClass('nav-hover');
            if (cur.offset().top < halfLPHt) {
                cur.find('ul.children').show().css({ 'top': cur.offset().top + (cur.height() + 40) });
            } else {
                cur.find('ul.children').show().css({ 'top': cur.offset().top - cur.find('ul.children').height() + 40 });
            }
            $("li.nav-hover ul.children").mCustomScrollbar({ theme: "light", scrollbarPosition: 'outside', autoHideScrollbar: true, scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true });
        }
    }).mouseleave(function (event) {
        var cur = $(this);
        cur.removeClass('nav-hover');
        cur.find('ul.children').css({ 'top': '' });
        if ($body.hasClass('leftpanel-collapsed'))
            cur.find('ul.children').hide();
    });
    // Toggle Left Menu
	

    $('#leftNav').on( 'click', '.nav-parent > a', function (){
        var parent = $(this).parent();
        var sub = parent.find('> ul');
        // Dropdown works only when leftpanel is not collapsed
        if (!$('body').hasClass('leftpanel-collapsed')) {
            if (sub.is(':visible')) {
                sub.slideUp(200, function () {
                    parent.removeClass('nav-active active');
                    $('.mainpanel').css({ height: '' });
                });
            } else {
                $('.leftpanel .nav-parent').each(function () {
                    var t = $(this);
                    if (t.hasClass('nav-active')) {
                        t.find('> ul').slideUp(200, function () {
                            t.removeClass('nav-active');
                        });
                    }
                });
               $('.nav-bracket > li').removeClass('nav-active active')
                parent.addClass('nav-active active');
                sub.slideDown(200);
            }
        }
    });
}