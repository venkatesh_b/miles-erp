//GLOBAL VARIABLE (DECLARE variable in caps ex: var MY_VAR = '';)

//LOAD / RESIZE / SCROLL
$(window).on('load resize', function(){
	$('#contentBody').height($(window).height()-140);
	$('.home-page-parent').height($(window).height()-155);
	$('.masters-menu-style').height($(window).height()-125);
	//collapsed menu active 1024width
	if($(window).width()<=1024){
		$("body").addClass('leftpanel-collapsed');
		$('.nav-parent').removeClass('nav-active');
			if ($body.hasClass('leftpanel-collapsed'))
            $('.leftpanel-collapsed').find('ul.children').hide();
		
		}else{
			$("body").removeClass('leftpanel-collapsed');
			}
	
		
});

//DOCUMENT READY
$(function(){
	//print
		/*$('#printableReciept').on('click',function(){
				$(".receipt-printable-wrapper").printElement({printMode:'popup'});
			});*/
		
	//adding animation to dropdowns
		$('.table tr').addClass('animated fadeInUp');
	 	$('.fixed-wrap').addClass('animated fadeIn');
	 	$('ul.dropdown-menu').addClass('animated zoomIn');
		$('.batch-info-wrap,.col-5,.contact-wrap').addClass('animated zoomIn');
	//textarea
	 /*$('#textarea1').val('New Text');
 	 $('#textarea1').trigger('autoresize');*/
	 //scroll Tabs
	 $('#tabs2').scrollTabs();
	 $('body').on('.nav-tabs li a','click',function(){
		//alert();
		$(".nav-tabs li").removeClass("active");
		});
	$('.showAddCompanyWrap-btn').on('click',function(){
			//console.log($(this).parent().next().slideDown());
			$(this).parent().next().slideToggle();
		});
		$('.showAddInstituteWrap-btn').on('click',function(){
			$(this).parent().next().slideToggle();
		});
		$('.company-icon-cancel').on('click',function(){
			$(".coldCall-addCompanyWrap").slideUp();
		});
		
/**/
var input_selector = 'input[type=text], input[type=password], input[type=email], input[type=url], input[type=tel], input[type=number], input[type=search], textarea';


      /**************************
       * Auto complete plugin  *
       *************************/
	   var stateData = [{
		   value: "Alabama"
		 }, {
		   value: "Alaska"
		 }, {
		   value: "Arizona"
		 }, {
		   value: "Arkansas"
		 }, {
		   value: "California"
		 }, {
		   value: "Colorado"
		 }, {
		   value: "Connecticut"
		 }, {
		   value: "District of Columbia"
		 }, {
		   value: "Delaware"
		 }, {
		   value: "Florida"
		 }, {
		   value: "Georgia"
		 }, {
		   value: "Hawaii"
		 }, {
		   value: "Idaho"
		 }, {
		   value: "Illinois"
		 }, {
		   value: "Indiana"
		 }, {
		   value: "Iowa"
		 }, {
		   value: "Kansas"
		 }, {
		   value: "Kentucky"
		 }, {
		   value: "Louisiana"
		 }, {
		   value: "Maine"
		 }, ];
		
		 $('#autocompleteState').data('array', stateData);
	   


      $(input_selector).each(function() {
        var $input = $(this);

        if ($input.hasClass('autocomplete')) {

          var $array = $input.data('array'),
            $inputDiv = $input.closest('.input-field'); // Div to append on
          // Check if "data-array" isn't empty
          if ($array !== '') {
            // Create html element
            var $html = '<ul class="autocomplete-content hide">';

            for (var i = 0; i < $array.length; i++) {
              // If path and class aren't empty add image to auto complete else create normal element
              if ($array[i]['path'] !== '' && $array[i]['path'] !== undefined && $array[i]['path'] !== null && $array[i]['class'] !== undefined && $array[i]['class'] !== '') {
                $html += '<li class="autocomplete-option"><span>' + $array[i]['value'] + '</span></li>';
              } else {
                $html += '<li class="autocomplete-option"><span>' + $array[i]['value'] + '</span></li>';
              }
            }

            $html += '</ul>';
            $inputDiv.append($html); // Set ul in body
            // End create html element

            function highlight(string) {
              $('.autocomplete-content li').each(function() {
                var matchStart = $(this).text().toLowerCase().indexOf("" + string.toLowerCase() + ""),
                  matchEnd = matchStart + string.length - 1,
                  beforeMatch = $(this).text().slice(0, matchStart),
                  matchText = $(this).text().slice(matchStart, matchEnd + 1),
                  afterMatch = $(this).text().slice(matchEnd + 1);
                $(this).html("<span>" + beforeMatch + "<span class='highlight'>" + matchText + "</span>" + afterMatch + "</span>");
              });
            }

            // Perform search
            $(document).on('keyup', $input, function() {
              var $val = $input.val().trim(),
                $select = $('.autocomplete-content');
              // Check if the input isn't empty
              $select.css('width', $input.width());

              if ($val != '') {
                $select.children('li').addClass('hide');
                $select.children('li').filter(function() {
                  $select.removeClass('hide'); // Show results

                  // If text needs to highlighted
                  if ($input.hasClass('highlight-matching')) {
                    highlight($val);
                  }
                  var check = true;
                  for (var i in $val) {
                    if ($val[i].toLowerCase() !== $(this).text().toLowerCase()[i])
                      check = false;
                  };
                  return check ? $(this).text().toLowerCase().indexOf($val.toLowerCase()) !== -1 : false;
                }).removeClass('hide');
              } else {
                $select.children('li').addClass('hide');
              }
            });

            // Set input value
            $('.autocomplete-option').click(function() {
              $input.val($(this).text().trim());
              $('.autocomplete-option').addClass('hide');
            });
          } else {
            return false;
          }
        }
      });
/**/
		
	//cold call status
	$('#callStatus').on('change', function() {
		var coldCallStatusVal = $(this).val();
		var contacttypeVal = $(this).val();
		
		if ($("input[name='callStatusInfo-radio']:checked").val() == 'callStatusInfoIntrested') {
                $(".coldcall-info").slideDown();
            }
			
		if ($("input[name='contacttype']:checked").val() == 'contacttype-corp') {
			$("#contactType-corporate-wrap").slideDown();
		}
			
		if(coldCallStatusVal=='answered'){
			$('#callStatusInfo').slideDown();
			}
		if(coldCallStatusVal!='answered'){
			$('#callStatusInfo').slideUp();
			$(".coldcall-info").slideUp();
		}
	});
	//
	$("input[name='callStatusInfo-radio']").click(function(){
			$(".coldcall-info").hide();
			if ($("input[name='callStatusInfo-radio']:checked").val() == 'callStatusInfoIntrested') {
                $(".coldcall-info").slideDown();
				$(".coldcall-info").find('#coldcalling-branch').hide();
            }
			if ($("input[name='callStatusInfo-radio']:checked").val() == 'callStatusInfoIntrestedTransfer') {
                $(".coldcall-info").slideDown();
				$(".coldcall-info").find('#coldcalling-branch').slideDown();
            }
		});
		$("input[name='contacttype']").click(function(){
			var contacttypeVal = $(this).val();
			if(contacttypeVal=='contacttype-corp'){
				$("#contactType-Institution-wrap").hide();
			$("#contactType-corporate-wrap").slideDown();
			}
			if(contacttypeVal=='contacttype-insti'){
				$("#contactType-corporate-wrap").hide();
			$("#contactType-Institution-wrap").slideDown();
			}
			if(contacttypeVal=='contacttype-se'){
				$("#contactType-corporate-wrap").hide();
			$("#contactType-Institution-wrap").hide();
			}
		});
	
	
	$(".branch-head-data").click(function(){
		$(".student-custom").removeClass("addpaid");
		$(".paid-info").hide();
	});
	$(".super-admin-data").click(function(){
		$(".student-custom").addClass("addpaid");
		$(".paid-info").show();
	});	
	//super admin widgets and branch head widgets
	 $(".super-admin").click(function(){
		 $("body").addClass("batch-hideshow");
	 });	
	 $(".branch-head").on('click',function(){
		 $("body").removeClass("batch-hideshow");
 	});
	//tooltip
	 /*$('[data-toggle="tooltip"]').tooltip({
		 container: 'body'
		 })*/
		  $('.tooltipped').tooltip({delay: 50});
	//funnel chart
	$('#stageFunnel').highcharts({
        chart: {
            type: 'funnel',
            marginRight: 100
        },
        title: {
            text: 'Leads Life Cycle',
            x: -50
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b> ({point.y:,.0f})',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '28%',
                neckHeight: '25%'

                //-- Other available options
                // height: pixels or percent
                // width: pixels or percent
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Leads',
            data: [
				['M1', 25],
				['M2', 15],
                ['M3', 23],
                ['M4', 56],
                ['M5', 23],
                ['M6', 53],
                ['M7', 80]
            ]
        }]
    });
	//fee structure create
	$('#includeFeetype').on('change', function() {
		var mystructureVal = $(this).val();
 	  // alert( myVal ); // or $(this).val()
		if(mystructureVal=='group1'){
			$('.feeStructureCreate-companyname').hide();
			$('.feeStructureCreate-groupname').slideDown();
			}
			if(mystructureVal=='corporate1'){
			$('.feeStructureCreate-groupname').hide();
			$('.feeStructureCreate-companyname').slideDown();
			}
			if(mystructureVal=='retail1'){
			$('.feeStructureCreate-groupname').hide();
			$('.feeStructureCreate-companyname').hide();
			}
			
	});
	//fee collection type select
	
	$('.feeCollectionType').on('change', function() {
		var myVal = $(this).val();
 	  // alert( myVal ); // or $(this).val()
		if(myVal=='cheque'){
			$('.dd-details,.cc-details').hide();
			$('.feeCollectionCreat-hiddenFrm').find('.cheque-details').slideDown();
			}
			if(myVal=='dd'){
			$('.cheque-details,.cc-details').hide();
			$('.feeCollectionCreat-hiddenFrm').find('.dd-details').slideDown();
			}
			if(myVal=='cc'){
			$('.cheque-details,.dd-details').hide();
			$('.feeCollectionCreat-hiddenFrm').find('.cc-details').slideDown();
			}
			if(myVal=='cash'){
			$('.cheque-details,.dd-details,.cc-details').hide();
			}
	});
	$('#makePayment').click(function(){
		$('.feeCollectioncreate-tabs').slideDown();
		$('.feeCollectionCreate-paymentTable').hide();
		});
	
	
	//receipt view
	//$('#feecollectionCreate-tform').on('click','#finalizee',function(){
		$('#finalize').click(function(){
		$('#feecollectionCreate-tform').hide();
		$('.receipt-printable').show();
		});
		
	$('#proceed').on('click',function(){
		$('#proceedWrapper').hide();
	$('#feeCollectionCreate-studentDetails').slideDown();
	});
	//reports select all and un select all
	$('.reportFilter-selectall').click(function(){
		$(this).closest('.reports-filter-wrapper').find('.boxed-tags a').addClass('active');
		});
		$('.reportFilter-unselectall').click(function(){
		$(this).closest('.reports-filter-wrapper').find('.boxed-tags a').removeClass('active');
		});
		
		$('.boxed-tags a').click(function(){
			$(this).toggleClass('active');
		});
	
	
	var modalBodyHeight = $(window).height()-111;
	$('[data-modal="right"]').on('show.bs.modal', function () {
       $(this).find('.modal-dialog').css({
              width:$(this).attr('modal-width') ? $(this).attr('modal-width') : 'auto'
       });
	   	   //modal-scrool height
	   $(this).find('.modal-scroll').css({
		   /*'height':'auto',
			'max-height':modalBodyHeight*/
		   'height':modalBodyHeight,
		   'overflow-y':'auto'
		});
	});
	//filters
	$('.accordian-heading').on('click',function(){
	$(this).toggleClass("active");
	//$('.accordian-content').slideUp();
	$(this).next().slideToggle()
		});
	//Materialize select
	$('select').material_select();
	//Materialize Date picker
	$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });
  
  	$( ".select-dropdown" ).change(function() {
 		$(this).parent('.select-wrapper').find('.select-label').show();
	});
	$('.datepicker').click(function(){
	var datePickerValue = $('.datepicker').val();
		if(datePickerValue !== ""){
			$('.datepicker').prev().addClass('active');
		}
	});
	//Manage users- add users working add
	   $(".slide-down").click(function(){
		   $(".hide-data").slideToggle();
	   });
	//warehouse grn create inventory recieved from
		$(".others").click(function(){
			if($("input.others:checked")){
			$(".sent-field").slideToggle();
			}
		});
			$("#branch").click(function(){
			$(".sent-field").slideUp();
	  	});
				

		
		//popover
	   $('.popper').popover({
		container: 'body',
		html: true,
		content: function () {
			return $(this).next('.popper-content').html();
		},
		title: function(){
		return jQuery(this).data('title')+'<span class="close icon-times f12 mt2"></span>';
		}
		}).on('shown.bs.popover', function(e){
			var popover = jQuery(this);
			$('div.popover .popover-title .close').on('click', function(e){
				
				popover.popover('hide');
			});
		});
		
		// hide all other popovers
		$('body').popover({
                selector: '[rel=popover]',
                trigger: "click"
            }).on("show.bs.popover", function(e){
                // hide all other popovers
                $("[rel=popover]").not(e.target).popover("destroy");
                $(".popover").remove();                    
            });
	
	 
  	//malihu scroll
   $(window).load(function(){
      $("#contentBody").mCustomScrollbar({
  		theme:"dark",
		autoHideScrollbar: true,
		autoExpandScrollbar: true
		});
		$(".modal-scrol").mCustomScrollbar({
  		theme:"dark",
		autoHideScrollbar: true,
		autoExpandScrollbar: true,
		updateOnContentResize: true 
		});
   });   
   
});
//cliseHideData
function closeHideData(){
	$(".hide-data").slideUp();
	}

//USER DEFINED FUNCTIONS
//notifications
function notify(message, type , duration){
	var defaultMessage = 'Message Not Given', defaultType='normal';
	if(message=='' || message == undefined){
	message=defaultMessage;
	}
	if(type=='' || type == undefined){
	type=defaultType;
	}
	message = message + '<div class="timer-wrapper"><div class="pie spinner"></div><div class="pie filler"></div><div class="mask"></div></div>'
		       
		alertify.notify(message, type, duration);
		$('.filler').css({
			'animation': 'fill ' + duration + 's steps(1, end) infinite'
	});
		$('.spinner').css({
			'animation': 'rota ' + duration + 's linear infinite'
	});
		$('.mask').css({
		'animation': 'mask ' + duration + 's steps(1, end) infinite'
	});
	(function () {
    // hold onto the drop down menu                                             
    var dropdownMenu;

    // and when you show it, move it to the body                                     
    $(window).on('show.bs.dropdown', function (e) {

        // grab the menu        
        dropdownMenu = $(e.target).find('.dropdown-menu');

        // detach it and append it to the body
        $('body').append(dropdownMenu.detach());

        // grab the new offset position
        var eOffset = $(e.target).offset();

        // make sure to place it where it would normally go (this could be improved)
        dropdownMenu.css({
            'display': 'block',
                'top': eOffset.top + $(e.target).outerHeight(),
                'left': eOffset.left
        });
    });

    // and when you hide it, reattach the drop down, and hide it normally                                                   
    $(window).on('hide.bs.dropdown', function (e) {
        $(e.target).append(dropdownMenu.detach());
        dropdownMenu.hide();
    });
})();
}











