// LEFTPANEL MENU
var mainURL = 'file:///D:/pramod/oasis-html/' + 'Pages/';
var attendanceURL = mainURL + 'ATTENDANCE/', timetableURL = mainURL + 'TIMETABLE_HTMLs/', cmsURL = mainURL + 'CMS/', feeURL = mainURL + 'FEE/',
    feedbackURL = mainURL + 'FEEDBACK/', hostelURL = mainURL + 'HOSTEL/', leadershipURL = mainURL + 'LEADERSHIP_PROGRAM/',
    storeURL = mainURL + 'OAK_STORE/', transportURL = mainURL + 'TRANSPORT/', schoolrecommendationURL = mainURL + 'SCHOOL_RECOMMENDATION/'
var jsonData = [
	
	//STORE
	
{
    "name": "Account",
    "url": "",
	"icon": "fa-user",
    "submenu": [
    {
        "name": "Fee Collection",
        "url":  "#"
    },
    {
        "name": "Fee Assignment",
        "url":  "#"
    },
	{
        "name": "Concession",
        "url":  "#"
    }
	
    ]
},
	
{
    "name": "Configuration",
    "url": "",
	"icon": "fa-gears",
    "submenu": [
    {
        "name": "Call Order Confirmation",
        "url": "Call-order-confirmation.html"
    },
    {
        "name": "Call Schedule",
        "url": "Call-schedule.html"
    }
	,
    {
        "name": "Stationary",
        "url": "Stationary.html"
    }
	,
    {
        "name": "Stationary Report",
        "url": "Stationary-report.html"
    }
    ]
},
{
    "name": "Crm Reports",
    "url":"#",
	"icon": "fa-file",
    "submenu": [
	{
        "name": "Branch Wise Report",
        "url":  "Branchwise-report.html"
    },
	{
        "name": "Lead Life Cycle Report",
        "url":  "Lead-lifecycle-report.html"
    },
	{
        "name": "Lead Report by City ",
        "url":  "Lead-reportby-city.html"
    },
	{
        "name": "Lead Report by Source ",
        "url":  "Lead-reportby-source.html"
    }
	]
},
{
    "name": "Student Reports",
    "url":"#",
	"icon": "fa-file",
    "submenu": [
	{
        "name": "Enrolment reports",
        "url":  "Student-enrolment-report.html"
    },
	{
        "name": "Attendance report",
        "url":  "Student-attendance-report.html"
    },
	{
        "name": "Adhoc Attendance",
        "url":  "Student-adhoc-report.html"
    }
	]
},
{
    "name": "Fee Reports",
    "url":"#",
	"icon": "fa-file",
    "submenu": [
	{
        "name": "Fee Details",
        "url":  "Fee-details-report.html"
    },
	{
        "name": "Cash Counter",
        "url":  "Cash-counter-report.html"
    },
	{
        "name": "Cheque/DD/Reversal",
        "url":  "cheque-dd-reversal-report.html"
    },
	{
        "name": "Due List",
        "url":  "Duelist-report.html"
    }
	,
	{
        "name": "Mode of Payment",
        "url":  "Mode-of-payment.html"
    }
	,
	{
        "name": "Receipt Deletion",
        "url":  "Reciept-deletion-report.html"
    }
	,
	{
        "name": "Student Dropouts",
        "url":  "Student-dropouts.html"
    }
	,
	{
        "name": "Daily Revenue Posting",
        "url":  "Daily-revenue-posting.html"
    }
	,
	{
        "name": "Daily Revenue Collection",
        "url":  "Daily-revenue-collection.html"
    }
	,
	{
        "name": "Daily Revenue Authorization",
        "url":  "Daily-revenue-authorization.html"
    }
	,
	{
        "name": "Uncleared Cheques / DD ",
        "url":  "Uncleared-cheque-dd-report.html"
    }
	]
},
{
    "name": "Masters",
    "url":"",
	"icon": "fa-graduation-cap",
    "submenu": [
    {
        "name": "Feehead",
        "url":  "Feehead.html"
    },
    {
        "name": "Fee Company",
        "url":   "Company.html"
    },
    {
        "name": "Course",
        "url":  "Course.html"
    },
	{
        "name": "Fee Types",
        "url":  "Fee-types.html"
    },
	{
        "name": "Fee Structure",
        "url":  "Fee-structure-list.html"
    },
	{
        "name": "Fee Collection",
        "url":  "Fee-collection.html"
    },
	{
        "name": "Branches",
        "url":  "Branches-list.html"
    }
    ]
},
{
    "name": "Branches",
    "url":"Branches-list.html",
	"icon": "fa-sitemap",
    "submenu": [ ]
},
{
    "name": "Roles",
    "url":"",
	"icon": "fa-street-view",
    "submenu": [
    {
        "name": "Roles View",
        "url":  "roles-view.html"
    },
	{
        "name": "User Management",
        "url":  "user-management.html"
    }
    ]
},
{
    "name": "Campaign",
    "url":"Compaign-list.html",
	"icon": "fa-bullhorn",
    "submenu": []
},
{
    "name": "Inventory",
    "url":"",
	"icon": "fa-cubes",
    "submenu": [
	{
        "name": "Warehouse GRN",
        "url":  "Warehouse-grn.html"
    },
	{
        "name": "Branch GRN",
        "url":  "Branch-grn.html"
    },
	{
        "name": "Warehouse GTN",
        "url":  "Warehouse-gtn.html"
    },
	{
        "name": "Branch GTN",
        "url":  "Branch-gtn.html"
    },
	{
        "name": "Warehouse Stock",
        "url":  "Warehouse-stock.html"
    },
	{
        "name": "Branch Stock",
        "url":  "Branch-stock.html"
    },
	{
        "name": "Products",
        "url":  "Products-list.html"
    },
	{
        "name": "Product Course Mapping",
        "url":  "Product-Mapping.html"
    }
	
    ]
},
{
    "name": "Contacts",
    "url":"Contacts.html",
	"icon": "fa-phone",
    "submenu": []
},
{
    "name": "Cold Calling",
    "url":"Cold-calling.html",
	"icon": "fa-phone",
    "submenu": []
},
{
    "name": "Leads",
    "url":"Leads-list.html",
	"icon": "fa-users",
    "submenu": []
},
{
    "name": "Student Relation",
    "url":"Student-relation.html",
	"icon": "fa-users",
    "submenu": []
}


];
var jsonString = JSON.stringify(jsonData);
