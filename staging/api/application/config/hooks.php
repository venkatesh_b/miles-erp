<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

/* 
 * Added By V Parameshwar
 * Dt: 22-02-2016
 * purpose: To check whether the user having access to the componet or not.
 */
$hook['post_controller_constructor'] = array(
                                'class'    => 'accessLayer',
                                'function' => 'checkAccess',
                                'filename' => 'AccessLayer.php',
                                'filepath' => 'hooks');
