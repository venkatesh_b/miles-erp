<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Accesstoken extends CI_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        $this->load->database();

        $this->load->library('form_validation');
    }
    
    
    public function generateToken() {

        // $this->load->library('oauth/oauth');
        $this->load->library('oauth/oauth', '', 'oauth');
        $oauth = $this->oauth;

        $accesstoken = $oauth->generateAccessToken();

        header('Content-Type: application/json');
        echo json_encode($accesstoken);
    }
    
    public function getRole(){
        die('role');
    }

    public function register() {
        $fullname = $this->input->post('fullname');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $data['fullname'] = $fullname;
        $data['email'] = $email;
        $data['password'] = $password;
        
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('fullname', 'Name', 'required|alpha');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');

//    $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
//            json_encode(validation_errors());die;
            $error = array('status'=>FALSE, 'message' => 'error', 'error'=>validation_errors());
            $this->output->set_header("HTTP/1.0 400 ERROR");
            $this->output->set_content_type('application/json')->set_output(json_encode($error));
        } else {
            $this->load->model("Users_model");
            $isregistered = $this->Users_model->registeruser($data);
            if ($isregistered) {
//                echo "Registration successful";
                $result = array('status'=>TRUE, 'message' => 'success', 'data'=>'Registration successful');
//                echo json_encode($result);die;
                
                $this->output->set_header("HTTP/1.0 200 Success");
                $this->output->set_content_type('application/json')->set_output(json_encode($result));
            } else {
                $result = array('status'=>FALSE, 'message' => 'warning', 'data'=>'Registration Failed');
                $this->output->set_header("HTTP/1.0 400 Error");
                $this->output->set_content_type('application/json')->set_output(json_encode($result));
//                echo json_encode($result);die;
//                echo "Registration Failed";
            }
//        $data[$key] = $val;
        }
    }

    public function get_refresh_token() {


        // $this->load->library('oauth/oauth');
        //if expired 

        $client_id = $this->input->post('User');
        $Authorizationtoken = $this->input->post('Authorizationtoken');

        $this->load->model("Users_model");

        $secret_id = $this->Users_model->getsecret_id($client_id);

        $client_scopes = $this->Users_model->getscopes($client_id);
        $scopeids = '';
        foreach ($client_scopes as $scopeid) {
            $scopeids.=$scopeid->scope_id . ',';
        }

        $scopeids = rtrim($scopeids, ',');

        $postdata = http_build_query(
                array(
                    'client_id' => $client_id,
                    'client_secret' => $secret_id,
                    'grant_type' => 'client_credentials',
                    'scope' => $scopeids
                )
        );

        // Set the POST options
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n" . "User: $client_id\r\n",
                'content' => $postdata
            )
        );

        // Create the POST context
        $context = stream_context_create($opts);

        // POST the data to an api
        $url = 'http://' . $_SERVER['HTTP_HOST'] . '/index.php/api/login/sign';
        $actoken = (file_get_contents($url, false, $context));

        //$response['access_token_response']=$actoken;
        echo json_encode($actoken);
        //exit;
    }

    /**
     * Modified for validation checks and the error message
     * Modified for getting proper json response
     * Modified Date - 20 Jan 2016
     */
    public function login() {
        $data = array();
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $this->load->model("Users_model");
        $userResp = $this->Users_model->loginUser($data);
        $scopeids = '';
        
        if (!$userResp) {
            $result = array('status'=>FALSE, 'message' => 'error', 'data'=>'invalid login');
//            echo json_encode($result);die;
            $this->output->set_header("HTTP/1.0 400 Error");
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
            
        } else {
            // Set the POST data
            $client_id = $userResp[0]->user_id;
            
            $secret_id = $this->Users_model->getsecret_id($client_id);

            $client_scopes = $this->Users_model->getscopes($client_id);
//print_r($client_scopes);die;
            foreach ($client_scopes as $scopeid) {
                $scopeids.=$scopeid->scope_id . ',';
            }

            $scopeids = rtrim($scopeids, ',');
            
            $postdata = http_build_query(
                array(
                    'client_id' => $client_id,
                    'client_secret' => $secret_id,
                    'grant_type' => 'client_credentials',
                    'scope' => $scopeids
                )
            );


            // Set the POST options
            $opts = array('http' =>
                array(
                    'method' => 'POST',
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n" . "User: $client_id\r\n",
                    'content' => $postdata
                )
            );

            // Create the POST context
            $context = stream_context_create($opts);

            // POST the data to an api
            $url = 'http://'.$_SERVER['HTTP_HOST'].'/index.php/api/login/sign';
            $actoken = (file_get_contents($url, false, $context));
            $response['access_token_response'] = $actoken;
            $this->output->set_header("HTTP/1.0 200 Success");
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
//            echo json_encode($response);die;
        }
        
    }

}
