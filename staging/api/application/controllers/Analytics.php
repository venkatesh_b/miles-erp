<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 7/9/16
 * Time: 5:32 PM
 */

class Analytics extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("Analytics_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);
        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0)
            {
                return $this->form_validation->error_array();
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    function getAnalyticsFilters_get()
    {
        $final_response['response'] = $this->Analytics_model->getAnalyticsFilters();
        $this->response($final_response['response'], REST_Controller::HTTP_OK);
    }
    function getAnalyticalWidgetsData_get()
    {
        $branchIds = $this->get('branchIds');
        $fromDate = $this->get('fromDate');
        $toDate = $this->get('toDate');
        $final_response['response'] = $this->Analytics_model->widgetsRelatedData($branchIds,$fromDate,$toDate);
        $this->response($final_response['response'], REST_Controller::HTTP_OK);
    }
    function getLeadStudentYearlyReport_get()
    {

        $final_response['response'] = $this->Analytics_model->LeadStudentYearlyReport();
        $this->response($final_response['response'], REST_Controller::HTTP_OK);
    }
    function LeadStudentCourseYearlyReport_get()
    {
        $final_response['response'] = $this->Analytics_model->LeadStudentCourseBranchWiseReport();
        $this->response($final_response['response'], REST_Controller::HTTP_OK);
    }
    function AchievedLeadsCourseWiseReport_get()
    {
        $final_response['response'] = $this->Analytics_model->AchievedLeadsCourseWiseReport();
        $this->response($final_response['response'], REST_Controller::HTTP_OK);
    }
    function LeadStudentMonthlyReport_get()
    {
        $final_response['response'] = $this->Analytics_model->LeadStudentMonthlyReport();
        $this->response($final_response['response'], REST_Controller::HTTP_OK);
    }
}