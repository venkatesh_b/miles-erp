<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class ApplicationMenu extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->database();
        $this->load->model("ApplicationMenu_model");
        $this->load->library('form_validation');
        $this->load->helper('url');
        //$this->load->library('oauth/oauth');
        
    }
    function customvalidation($data){
   
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
}
    function getMenu_get(){ 
        
        $Userid=$this->get("Userid");
        $this->form_validation->set_data(array("Userid" => $Userid));
        
        $validations[]=["field"=>"Userid","label"=>"Userid","rules"=>"required|integer","errors"=>array(
        'required' => 'User ID should not be blank','integer' => 'User ID should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        
         if($validationstatus===true){
                $data['UserID']=$Userid;
                $res=$this->ApplicationMenu_model->getMenu($data);
                
                $final_response['response']=[
                   'status' => $res['status'],
                   'message' => $res['message'],
                   'data' => $res['data'],
                   ];
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                
         }
         else{
             
             $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            
         }
         $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getApplicationMenu_get(){ 
        
        $Userid=$this->get("Userid");
        $this->form_validation->set_data(array("Userid" => $Userid));
        
        $validations[]=["field"=>"Userid","label"=>"Userid","rules"=>"required|integer","errors"=>array(
        'required' => 'User ID should not be blank','integer' => 'User ID should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        
         if($validationstatus===true){
                $data['UserID']=$Userid;
                $res=$this->ApplicationMenu_model->getApplicationMenu($data);
                
                $final_response['response']=[
                   'status' => $res['status'],
                   'message' => $res['message'],
                   'data' => $res['data'],
                   ];
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                
         }
         else{
             
             $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            
         }
         $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}

?>

