<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-26
 * Time: 02:39 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class BooksIssue extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("BooksIssue_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE) {
            if (count($this->form_validation->error_array()) > 0) {
                return $this->form_validation->error_array();
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    function getStudentCourseBooksList_get()
    {
        $batchId = $this->get('batchId');
        $leadId = $this->get('studentId');
        $res = $this->BooksIssue_model->getStudentCourseBooksList($batchId,$leadId);
        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }
    function saveBooksIssued_post()
    {
        $userId = $this->post('userID');
        $booksIssued = $this->post('booksIssued');
        $res = $this->BooksIssue_model->saveBooksIssued($userId,$booksIssued);
        $final_response['response'] = [
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getStudentAdhocBookDetails_get()
    {
        $courseId = $this->get('courseId');
        $studentId = decode($this->get('studentId'));
        $adhoc_book_issue_response = $this->BooksIssue_model->getStudentAdhocCourseBooks($courseId,$studentId);
        $this->set_response($adhoc_book_issue_response);
    }
    function saveStudentAdhocBookDetails_post()
    {
        $userId = $this->post('userID');
        $courseId = $this->post('courseId');
        $batchId = $this->post('batchId');
        $adhocProductQty = $this->post('adhocProductQty');
        $studentId = decode($this->post('studentId'));
        $adhoc_book_issue_response = $this->BooksIssue_model->saveStudentAdhocCourseBooks($userId,$studentId,$courseId,$batchId,$adhocProductQty);
        $this->set_response($adhoc_book_issue_response);
    }
}