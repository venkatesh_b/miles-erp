<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Branch Visit
 * @category        Controller
 * @author          Abhilash
 */
class BranchVisit extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("BranchVisit_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    public function getBranchVisitList_get() {
        $branch = $this->get('branch_id');
        $this->response($this->BranchVisit_model->getBranchVisitList($branch),REST_Controller::HTTP_OK);
    }
    function getLeadsList_get(){
        $data['branchId']=$this->get('branchId');
        $this->response($result=$this->BranchVisit_model->getLeadsList($data));

    }

    /**
     * Function to get Lead snapshot
     *
     * @return - array
     * @created date - 31 Mar 2016
     * @author : Abhilash
     */
    public function getLeadSpanshot_get(){
        $status=true;$message="";$results='';
        $id = $this->get('lead_id');
        $branch_id = $this->get('branch_id');
        $hdnBranchVisitId = $this->get('hdnBranchVisitId');
        $contactType = $this->get('contactType');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required'
            ),
            array(
                'field' => 'branch_id',
                'label' => 'Branch Id',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data(array('id' => $id, 'branch_id' => $branch_id));

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        }
        else
        {
            $resposne = $this->BranchVisit_model->getLeadSpanshot($id, $branch_id,$hdnBranchVisitId);

            if (is_array($resposne) && $resposne['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $results=$resposne['msg'];
            }  else if ($resposne === 'nodata') {
                $message="Lead not found";
                $status=false;
                $results=[];
            }  else if ($resposne === 'nobranch') {
                $message="Branch not found";
                $status=false;
                $results=[];
            } else if($resposne) {
                $message="Lead details";
                $status=true;
                $results=$resposne;
            } else {
                $message="Something went wrong";
                $status=false;
                $results=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }

    public function getContactLeadSpanshot_get(){
        $status=true;$message="";$results='';
        $id = $this->get('lead_id');
        $branch_id = $this->get('branch_id');
        $hdnBranchVisitId = $this->get('hdnBranchVisitId');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);

        //$this->form_validation->set_data(array('id' => $id, 'branch_id' => $branch_id));
        $this->form_validation->set_data(array('id' => $id));

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $resposne = $this->BranchVisit_model->getContactLeadSpanshot($id, $branch_id,$hdnBranchVisitId);
            if (is_array($resposne) && $resposne['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $results=$resposne['msg'];
            }  else if ($resposne === 'nodata') {
                $message="Lead not found";
                $status=false;
                $results=[];
            }  else if ($resposne === 'nobranch') {
                $message="Branch not found";
                $status=false;
                $results=[];
            } else if($resposne) {
                $message="Lead details";
                $status=true;
                $results=$resposne;
            } else {
                $message="Something went wrong";
                $status=false;
                $results=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }

    public function saveBranchVisit_post() {
        $status=true;$message="";$results='';
        $config = array(
            array(
                'field' => 'user',
                'label' => 'user',
                'rules' => 'required'
            ),
            array(
                'field' => 'lead_id',
                'label' => 'lead_id',
                'rules' => 'required'
            ),
            array(
                'field' => 'visitedDate',
                'label' => 'Vistied Date',
                'rules' => 'required'
            ),
            array(
                'field' => 'branchVisitComments',
                'label' => 'Comments',
                'rules' => 'required'
            ),
            array(
                'field' => 'CommentType',
                'label' => 'CommentType',
                'rules' => 'required'
            )

        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $data_save=$this->input->post();
            $date=$this->post('common_date');
            if(isset($date) && !empty($date) && $date!='' ){
                $commonDateFormat = DateTime::createFromFormat('d M, Y',$date);
                $commonDate = $commonDateFormat->format("Y-m-d H:i:s");
            }
            else{
                $commonDate=NULL;
            }
            $data_save['common_date']=$commonDate;
            $response = $this->BranchVisit_model->saveBranchVisit($data_save);
            if (is_array($response) && $response['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $results=$response['msg'];
            }  else if ($response === 'nolead') {
                $message="Lead not found";
                $status=false;
                $results=[];
            } else if ($response === 'nouser') {
                $message="Counsellor not found";
                $status=false;
                $results=[];
            } else if ($response === 'duplicate') {
                $message="Counsellor has already commented";
                $status=false;
                $results=[];
            } else if ($response === 'already') {
                $message="Lead is already assigned to counsellor";
                $status=false;
                $results=[];
            } else if ($response === 'alreadyDate') {
                $message="Lead is already assigned to counsellor for same visited date";
                $status=false;
                $results=[];
            } else if($response){
                $message="Data Updated";
                $status=true;
                $results=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }

    public function saveContactBranchVisit_post() {
        $status=true;$message="";$results='';
        $config = array(
            array(
                'field' => 'user',
                'label' => 'user',
                'rules' => 'required'
            ),
            array(
                'field' => 'lead_id',
                'label' => 'lead_id',
                'rules' => 'required'
            ),
            array(
                'field' => 'visitedDate',
                'label' => 'Vistied Date',
                'rules' => 'required'
            ),
            array(
                'field' => 'branchVisitComments',
                'label' => 'Comments',
                'rules' => 'required'
            ),
            array(
                'field' => 'course',
                'label' => 'Course',
                'rules' => 'required'
            )
        );
        $data = $this->input->post();
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($data);

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
            $data=array("status"=>$status,"message"=>$message,"data"=>$results);
            $this->response($data);
        }
        else
        {
            $response = $this->BranchVisit_model->saveContactBranchVisit($data);
            if (is_array($response) && $response['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $results=$response['msg'];
            }  else if ($response === 'nolead') {
                $message="Lead not found";
                $status=false;
                $results=[];
            } else if ($response === 'nouser') {
                $message="Counsellor not found";
                $status=false;
                $results=[];
            } else if ($response === 'duplicate') {
                $message="Counsellor has already commented";
                $status=false;
                $results=[];
            } else if ($response === 'already') {
                $message="Lead is already assigned to counsellor";
                $status=false;
                $results=[];
            } else if ($response === 'alreadyDate') {
                $message="Lead is already assigned to counsellor for same visited date";
                $status=false;
                $results=[];
            } else if($response){
                $message="Data Updated";
                $status=true;
                $results=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);


    }

    function saveCounsellorComments_post(){

        $params['userId']=$userId=$_SERVER['HTTP_USER'];
        $params['branchId']=$branchId=$this->post('branchId');
        $params['branchXrefLeadId']=$branchXrefLeadId=$this->post('branchXrefLeadId');
        $params['commentType']=$commentType=$this->post('commentType');
        $params['comments']=$comments=$this->post('comments');
        $params['visitDate']=$visitDate=$this->post('visitDate');
        $params['branchVisitorId']=$branchVisitorId=$this->post('branchVisitorId');
        /*if(!empty($this->post('enrollDate')) && $this->post('enrollDate')!='') {
            $enrollDate = DateTime::createFromFormat('d M, Y', $this->post('enrollDate'));
            $params['enrollDate'] = $enrollDate->format("Y-m-d");
        }
        else{
            $params['enrollDate']='';
        }*/

        $resposne = $this->CounsellorVisitors_model->saveCounsellorComments($params);

        $data=array("status"=>$resposne['status'],"message"=>$resposne['message'],"data"=>$resposne['data']);
        $this->response($data);

    }
    function deleteBranchVisit_post(){
        $params=$this->post();
        $resposne = $this->BranchVisit_model->deleteBranchVisit($params);
        $data=array("status"=>$resposne['status'],"message"=>$resposne['message'],"data"=>$resposne['data']);
        $this->response($data);


    }
    function getBranchVisitDetails_get(){
        $params=$this->get();
        $resposne = $this->BranchVisit_model->getBranchVisitDetails($params);
        $data=array("status"=>$resposne['status'],"message"=>$resposne['message'],"data"=>$resposne['data']);
        $this->response($data);
    }

    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function getBranchVisitorContactInformation_get()
    {
        $branchId = $this->get('branchId');
        $searchVal=$this->get('searchVal');
        $this->response($result=$this->BranchVisit_model->getBranchVisitContactsInformation($searchVal,$branchId));
    }

    /**
     * Function to get Lead/Contact snapshot
     *
     * @return - array
     * @created date - 27 Jan 2017
     * @author : Abhilash
     */
    public function getContactSnapshot_get(){
        $status=true;$message="";$results='';
        $id = $this->get('lead_id');
        $branch_id = $this->get('branch_id');
        $hdnBranchVisitId = $this->get('hdnBranchVisitId');
        $config = array(
            array(
                'field' => 'id',
                'label' => 'id',
                'rules' => 'required'
            ),
            array(
                'field' => 'branch_id',
                'label' => 'Branch Id',
                'rules' => 'required'
            )
        );
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data(array('id' => $id, 'branch_id' => $branch_id));

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $resposne = $this->BranchVisit_model->getContactSnapshot($id, $branch_id,$hdnBranchVisitId);
            if (is_array($resposne) && $resposne['error'] === 'dberror') {
                $message="Database Error";
                $status=false;
                $results=$resposne['msg'];
            }  else if ($resposne === 'nodata') {
                $message="Lead not found";
                $status=false;
                $results=[];
            }  else if ($resposne === 'nobranch') {
                $message="Branch not found";
                $status=false;
                $results=[];
            } else if($resposne) {
                $message="Lead details";
                $status=true;
                $results=$resposne;
            } else {
                $message="Something went wrong";
                $status=false;
                $results=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->response($data);
    }
}