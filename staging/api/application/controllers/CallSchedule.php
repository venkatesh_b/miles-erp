<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 15-03-2016
 * Time: 02:46 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class CallSchedule extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("CallSchedule_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function customvalidation($data)
    {

        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE) {
            if (count($this->form_validation->error_array()) > 0) {
                return $this->form_validation->error_array();
            } else {
                return true;
            }
        } else {
            return true;
        }

    }
    function getCallCounts_get()
    {
        $status=true;$message='success';$data='';
        $param='';
        $branchId=$this->get('branchId');
        $chosenTagId=$this->get('chosenTag');
        $chosenSourceId=$this->get('chosenSource');
        $this->form_validation->set_data(array('branchId'=>$branchId));
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true) {
            $status=TRUE;$message='success';$data='';
            $param['branchId']=$branchId;
            $param['tagId']=$chosenTagId;
            $param['sourceId']=$chosenSourceId;
            $param['userId']=$_SERVER['HTTP_USER'];
            $response_users=$this->CallSchedule_model->getColdCallingCount($param);
            // echo $this->CallSchedule_model->getColdCallingCount($param);
            //exit;
            $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        }
        else{
            $status=FALSE;$message='validation failed';$data=$validationstatus;
        }

        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getLeadColdCallingCount_get(){
        $status=true;$message='success';$data='';
        $param='';
        $branchId=$this->get('branchId');
        $this->form_validation->set_data(array('branchId'=>$branchId));
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true) {
            $status=TRUE;$message='success';$data='';
            $param['branchId']=$branchId;
            $param['userId']=$_SERVER['HTTP_USER'];
            $response_users=$this->CallSchedule_model->getLeadColdCallingCount($param);
            // echo $this->CallSchedule_model->getColdCallingCount($param);
            //exit;
            $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        }
        else{
            $status=FALSE;$message='validation failed';$data=$validationstatus;
        }

        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getCallSchedules_get(){
        $status=true;$message='success';$data='';
        $param='';
        $branchId=$this->get('branchId');
        $chosenTagId=$this->get('chosenTag');
        $chosenSourceId=$this->get('chosenSource');
        $this->form_validation->set_data(array('branchId'=>$branchId));
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true)
        {
            $status=TRUE;$message='success';$data='';
            $param['branchId']=$branchId;
            $param['tagId']=$chosenTagId;
            $param['sourceId']=$chosenSourceId;
            $param['userId']=$_SERVER['HTTP_USER'];
            $response_users=$this->CallSchedule_model->getCallSchedules($param);
           // echo $this->CallSchedule_model->getColdCallingCount($param);
            //exit;
            $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        }
        else{
            $status=FALSE;$message='validation failed';$data=$validationstatus;
        }

        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);



    }
    function addCallSchedule_post(){
        $status=true;$message='success';$data='';
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $chosenTagId=$this->post('chosenTag');
        $chosenSourceId=$this->post('chosenSource');
        $param['branchId']=$this->post('branchId');
        $param['callSchedules']=$this->post('callSchedules');
        $param['tagId']=$chosenTagId;
        $param['sourceId']=$chosenSourceId;
        $param['createdBy']=$_SERVER['HTTP_USER'];
        $response_users=$this->CallSchedule_model->addCallSchedule($param);
        $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function releaseScheduledCalls_post(){

        $status=true;$message='success';$data='';
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }

        $param['branchId']=$this->post('branchId');
        $param['releaseCnt']=$this->post('releaseCnt');
        $param['releaseBy']=$_SERVER['HTTP_USER'];
        $param['assignedUserId']=$this->post('assignedUserId');
        $response_users=$this->CallSchedule_model->releaseCallSchedule($param);
        $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}