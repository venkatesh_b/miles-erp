<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Receipt Deletion
 * @category        Controller
 * @author          Parameshwar
 */
class CashCounter extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("CashCounter_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    public function getAllConfigurations_get() {
        $message='Receipt Deletion List Report';
        $status=true;
        $branch = [];
        $course = [];
        $company = [];
        $branchList = $this->CashCounter_model->getBranchListByUserId($_SERVER['HTTP_USER']);
        if(count($branchList) >0 ){
            foreach ($branchList as $key => $value) {
                $branch[$value['id']] = (object)$value;
                $temp = $this->CashCounter_model->getAllBranchCoursesList(array('branchId'=>$value['id']))['data'];
                if(count($temp) > 0){
                    foreach ($temp as $k => $v) {
                        $course[$v->courseId] = $v;

                    }
                }else {
                    $course = [];
                }

            }
        } else {
            $branch = [];
        }

        $companyList = $this->CashCounter_model->getCompaniesList()['data'];
        if(count($companyList) >0 ) {
            foreach ($companyList as $keyCompany => $valueCompany) {
                $company[] = (object)$valueCompany;
            }
        }

        $data=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>array(
                'branch' => array_values($branch),
                'course' => array_values($course),
                'company' => array_values($company)));
        $this->set_response($data);
    }
    public function getReceiptDeletionListReport_get() {
        $message='Receipt Deletion List Report';
        $status=true;
        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $course = strlen(trim($this->get('course')))>0?"'".trim($this->get('course'))."'":'NULL';
        $company = strlen(trim($this->get('company')))>0?"'".trim($this->get('company'))."'":'NULL';
        $receiptmode = strlen(trim($this->get('receiptmode')))>0?'"'.trim($this->get('receiptmode')).'"':'NULL';
        $paymentMode = strlen(trim($this->get('paymentMode')))>0?'"'.trim($this->get('paymentMode')).'"':'NULL';
        $fromDate = strlen(trim($this->get('fromDate')))>0?"".trim($this->get('fromDate'))."":'NULL';
        $toDate = strlen(trim($this->get('toDate')))>0?"".trim($this->get('toDate'))."":'NULL';

        $formaterDate = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $formatDate = $formaterDate->format("Y-m-d");
        $fromDate=$formaterDate->format("d M, Y");
        $fromDate_format=$formatDate;

        $formatDate = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $formatDate = $formatDate->format("Y-m-d");
        $fromDate_format=$formatDate;

        $formaterDate = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $formatDate = $formaterDate->format("Y-m-d");
        $toDate=$formaterDate->format("d M, Y");
        $toDate_format=$formatDate;

        $formatDate = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $formatDate = $formatDate->format("Y-m-d");
        $toDate_format=$formatDate;
        $toDate_format=date('Y-m-d',strtotime($formatDate.' +1 day'));
        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0 && strlen(trim($this->get('receiptmode')))<=0 && strlen(trim($this->get('company')))<=0){
            $status=false;
            $message = 'Select any filter';
            $res = [];
        }else{
            $res = $this->CashCounter_model->getReceiptDeletionListReport($branch, $course, $receiptmode,$company,"'".$fromDate_format."'","'".$toDate_format."'",$paymentMode);
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$res['data']);

        /*//Testing
        $data_main=array();
        for($i=0;$i<100;$i++){

            $data_main[]=array('enrollNo'=>"CPA1001$i",'leadName'=>"kiran Kumar $i",'receiptNo'=>"RECEPT1234$i",'remarks'=>"--","amount"=>(100*$i),'userName'=>"userName $i",'deletedBy'=>"deletedBy $i",'deletedOn'=>date('d M, Y'));
        }
        $data=array("status"=>true,"message"=>'success',"data"=>$data_main);*/


        $this->set_response($data);
    }
    function getCashCounterListReport_get(){
        $message='Cash Counter List Report';
        $status=true;
        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $course = strlen(trim($this->get('course')))>0?"'".trim($this->get('course'))."'":'NULL';
        $company = strlen(trim($this->get('company')))>0?"'".trim($this->get('company'))."'":'NULL';
        $receiptmode = strlen(trim($this->get('receiptmode')))>0?'"'.trim($this->get('receiptmode')).'"':'NULL';
        $paymentMode = strlen(trim($this->get('paymentMode')))>0?'"'.trim($this->get('paymentMode')).'"':'NULL';
        $fromDate = strlen(trim($this->get('fromDate')))>0?"".trim($this->get('fromDate'))."":'NULL';
        $toDate = strlen(trim($this->get('toDate')))>0?"".trim($this->get('toDate'))."":'NULL';

        $formaterDate = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $formatDate = $formaterDate->format("Y-m-d");
        $fromDate=$formaterDate->format("d M, Y");
        $fromDate_format=$formatDate;

        $formatDate = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $formatDate = $formatDate->format("Y-m-d");
        $fromDate_format=$formatDate;

        $formaterDate = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $formatDate = $formaterDate->format("Y-m-d");
        $toDate=$formaterDate->format("d M, Y");
        $toDate_format=$formatDate;

        $formatDate = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $formatDate = $formatDate->format("Y-m-d");
        $toDate_format=$formatDate;
        $toDate_format=date('Y-m-d',strtotime($formatDate.' +1 day'));
        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0 && strlen(trim($this->get('receiptmode')))<=0 && strlen(trim($this->get('company')))<=0){
            $status=false;
            $message = 'Select any filter';
            $res = [];
        }else{
            $res = $this->CashCounter_model->getCashCounterListReport($branch, $course, $receiptmode,$company,$paymentMode,"'".$fromDate_format."'","'".$toDate_format."'");
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$res['data']);

        /*//Testing
        $data_main=array();
        for($i=0;$i<100;$i++){

            $data_main[]=array('enrollNo'=>"CPA1001$i",'leadName'=>"kiran Kumar $i",'receiptNo'=>"RECEPT1234$i",'remarks'=>"--","payment_mode"=>"payment mode $i","amount"=>(100*$i),'userName'=>"userName $i");
        }
        $data=array("status"=>true,"message"=>'success',"data"=>$data_main);*/


        $this->set_response($data);
    }
    function getkey($pos){

        return chr(65+$pos);

    }
    public function getCashCounterListReportExport_get()
    {

        $message = 'Cash Counter Report';
        $status = true;
        $this->load->library('excel');
        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $course = strlen(trim($this->get('course')))>0?"'".trim($this->get('course'))."'":'NULL';
        $company = strlen(trim($this->get('company')))>0?"'".trim($this->get('company'))."'":'NULL';
        $receiptmode = strlen(trim($this->get('receiptmode')))>0?'"'.trim($this->get('receiptmode')).'"':'NULL';
        $paymentMode = strlen(trim($this->get('paymentMode')))>0?'"'.trim($this->get('paymentMode')).'"':'NULL';
        $fromDate = strlen(trim($this->get('fromDate')))>0?"".trim($this->get('fromDate'))."":'NULL';
        $toDate = strlen(trim($this->get('toDate')))>0?"".trim($this->get('toDate'))."":'NULL';

        $formaterDate = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $formatDate = $formaterDate->format("Y-m-d");
        $fromDate=$formaterDate->format("d M, Y");
        $fromDate_format=$formatDate;

        $formatDate = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $formatDate = $formatDate->format("Y-m-d");
        $fromDate_format=$formatDate;

        $formaterDate = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $formatDate = $formaterDate->format("Y-m-d");
        $toDate=$formaterDate->format("d M, Y");
        $toDate_format=$formatDate;

        $formatDate = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $formatDate = $formatDate->format("Y-m-d");
        $toDate_format=$formatDate;
        $toDate_format=date('Y-m-d',strtotime($formatDate.' +1 day'));
        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0 && strlen(trim($this->get('receiptmode')))<=0 && strlen(trim($this->get('company')))<=0){
            $status=false;
            $message = 'Select any filter';
            $data = array("status" => false, "message" => $message, "data" => array());

        }else{

            $res = $this->CashCounter_model->getCashCounterListReport($branch, $course, $receiptmode,$company,$paymentMode,"'".$fromDate_format."'","'".$toDate_format."'");
            $res_deletion_list = $this->CashCounter_model->getReceiptDeletionListReport($branch, $course, $receiptmode,$company,"'".$fromDate_format."'","'".$toDate_format."'",$paymentMode);
            //$res['status']=true;
            $headers_cashcounter=array('Enroll No.','Name','Receipt No.','Mode of Payment','Remark','Amount','Username / Cashier');
            $paid=0;
            $deleted=0;
            if($res['status']===false){
                $data = array("status" => false, "message" => $res['message'], "data" => array());
                $data_main_cashcounter=array();
            }
            else{


                $data_main_cashcounter=array();
                foreach($res['data'] as $kd=>$vd){
                    $paid=$paid+$vd->amount;
                    $data_main_cashcounter[]=implode('||',array('enrollNo'=>$vd->enrollNo,'leadName'=>$vd->leadName,'receiptNo'=>$vd->receiptNo,'payment_mode'=>$vd->payment_mode,'remarks'=>$vd->remarks,'amount'=>$vd->amount,'userName'=>$vd->userName));
                }
                /*for($i=0;$i<1000;$i++){
                    $data_main_cashcounter[]=implode('||',array('enrollNo'=>"CPA1001$i",'leadName'=>"kiran Kumar $i",'receiptNo'=>"RECEPT1234$i",'payment_mode'=>"pm $i",'remarks'=>"--","amount"=>(100*$i),'userName'=>"userName $i"));
                }*/
            }
            $headers_deleted=array('Enroll No.','Name','Receipt No.','Remark','Amount','Username / Cashier','Deleted / Reversal By','Deleted / Reversal Date');
            if($res_deletion_list['status']===false){
                $data = array("status" => false, "message" => $res['message'], "data" => array());
                $data_main_deleted=array();
            }
            else{

                $data_main_deleted=array();
                foreach($res_deletion_list['data'] as $kd=>$vd){
                    $deleted=$deleted+$vd->amount;
                    $data_main_deleted[]=implode('||',array('enrollNo'=>$vd->enrollNo,'leadName'=>$vd->leadName,'receiptNo'=>$vd->receiptNo,'remarks'=>$vd->remarks,'amount'=>$vd->amount,'userName'=>$vd->userName,'deletedBy'=>$vd->deletedBy,'deletedOn'=>$vd->deletedOn));
                }
            }
            if($res_deletion_list['status']===false && $res['status']===false){
                //no recrods
                $data = array("status" => false, "message" => 'No records found', "data" => array());
                $data_main_cashcounter=array();
            }
            else{




                $excelRowstartsfrom=3;
                $excelColumnstartsFrom=2;
                $excelstartsfrom=$excelRowstartsfrom;

                if(count($headers_cashcounter)>=count($headers_deleted)){
                    $lastColumnNumber=count($headers_cashcounter);
                }
                else{
                    $lastColumnNumber=count($headers_deleted);
                }

                $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'CASH COUNTER REPORT');

                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        ),
                        'font'  => array(
                            'bold'  => true,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    )
                );

                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

                $excelstartsfrom=$excelstartsfrom+1;
                $excelstartsfrom=$excelstartsfrom+1;
                $excelstartsfrom=$excelstartsfrom+1;

                $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Cash Counter / Branch");
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('branchText'));
                $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
                $excelstartsfrom=$excelstartsfrom+1;

                $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel3);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Mode of Payment");
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('paymentModeText'));
                $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
                $excelstartsfrom=$excelstartsfrom+1;

                $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel3);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Companies");
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('companyText'));
                $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
                $excelstartsfrom=$excelstartsfrom+1;

                $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel3);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Receipt Modes");
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('receiptmodeText'));
                $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
                $excelstartsfrom=$excelstartsfrom+1;

                $mrgesel2 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel2);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Courses");
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('courseText'));
                $mrgesel2_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->getStartColor()->setRGB('C2F7D8');
                $excelstartsfrom=$excelstartsfrom+1;

                $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel3);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "From Date");
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $fromDate);
                $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
                $excelstartsfrom=$excelstartsfrom+1;

                $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel3);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "To Date");
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $toDate);
                $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
                $excelstartsfrom=$excelstartsfrom+1;


                $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel3);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Report on");
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
                $excelstartsfrom=$excelstartsfrom+1;

                //---------------//
                $header_main = implode('||', $headers_cashcounter);
                $header = $header_main;
                $headervals = explode('||', $header);
                $excelstartsfrom=$excelstartsfrom+1;
                $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'CASH COUNTER');

                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        ),
                        'font'  => array(
                            'bold'  => true,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    )
                );

                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

                $head_starts_from = $excelstartsfrom+1;
                for ($k = 0; $k < count($headervals); $k++) {
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from, $headervals[$k]);
                    $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k))->setAutoSize(true);
                    $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from)->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
                        )
                    );

                }

                $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
                $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
                $rowsStratsFrom = $head_starts_from + 1;
                $c = $rowsStratsFrom;
                $data_main=$data_main_cashcounter;
                for ($k = 0; $k < count($data_main); $k++) {
                    $datavals = explode('||', $data_main[$k]);
                    for ($j = 0; $j <=count($datavals)-1; $j++) {
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, $datavals[$j]);
                    }
                    $c++;
                }
                //---------------//
                //---------------//
                $header_main = implode('||', $headers_deleted);
                $header = $header_main;
                $headervals = explode('||', $header);
                $head_starts_from=$c+1;

                $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
                $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $head_starts_from, 'DELETED / CHEQUE REVERSALS');

                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        ),
                        'font'  => array(
                            'bold'  => true,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    )
                );

                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

                $head_starts_from=$head_starts_from+1;
                for ($k = 0; $k < count($headervals); $k++) {
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from, $headervals[$k]);
                    $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k))->setAutoSize(true);
                    $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from)->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
                        )
                    );

                }

                $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
                $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
                $rowsStratsFrom = $head_starts_from + 1;
                $c = $rowsStratsFrom;
                $data_main=$data_main_deleted;
                for ($k = 0; $k < count($data_main); $k++) {
                    $datavals = explode('||', $data_main[$k]);
                    for ($j = 0; $j <=count($datavals)-1; $j++) {
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, $datavals[$j]);
                    }
                    $c++;
                }
                //--------------//


                $footer_starts_from=$c+2;

                $totals=(int)$lastColumnNumber/2;
                $totalsStartfrom=$totals-3;

                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom) . $footer_starts_from, 'Paid');
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom+1) . $footer_starts_from, '+');
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom+2) . $footer_starts_from, 'Delete');
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom+3) . $footer_starts_from, '=');
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom+4) . $footer_starts_from, 'Total');
                $footer_starts_from=$footer_starts_from+1;
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom) . $footer_starts_from, $paid);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom+1) . $footer_starts_from, '');
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom+2) . $footer_starts_from, $deleted);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom+3) . $footer_starts_from, '');
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$totalsStartfrom+4) . $footer_starts_from, $paid+$deleted);

                $footer_starts_from=$footer_starts_from+2;
                $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $footer_starts_from;
                $this->excel->getActiveSheet()->mergeCells($mrgesel);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from,'SIGNATURES');

                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        ),
                        'font'  => array(
                            'bold'  => true,
                            'color' => array('rgb' => '04578C')
                        )
                    )
                );

                $footer_starts_from=$footer_starts_from+1;


                $test=(int)((count($headervals)-1)/2);

                $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$test) . ($footer_starts_from+1);
                $this->excel->getActiveSheet()->mergeCells($mrgesel);

                $mrgesel = $this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . ($footer_starts_from+1);
                $this->excel->getActiveSheet()->mergeCells($mrgesel);

                $footer_starts_from=$footer_starts_from+2;

                $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$test) . $footer_starts_from;
                $this->excel->getActiveSheet()->mergeCells($mrgesel);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from,'Accountant / Cashier');

                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        ),
                        'font'  => array(
                            'bold'  => true,
                            'color' => array('rgb' => 'a9aaae')
                        )
                    )
                );




                $mrgesel = $this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$lastColumnNumber - 1) . $footer_starts_from;
                $this->excel->getActiveSheet()->mergeCells($mrgesel);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from,'Branch Head');

                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        ),
                        'font'  => array(
                            'bold'  => true,
                            'color' => array('rgb' => 'a9aaae')
                        )
                    )
                );

                //activate worksheet number 1
                $this->excel->setActiveSheetIndex(0);

                //name the worksheet
                $this->excel->getActiveSheet()->setTitle('CASH COUNTER REPORT');
                $filename = "CashCounterReport_".date('Ymd_His') . '.xls'; //save our workbook as this file name
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format

                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

                //force user to download the Excel file without writing it to server's HD
                $file_path = './uploads/' . $filename;
                $objWriter->save($file_path);
                $file_path = base_url().'uploads/' . $filename;
                $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
            }
        }

        $this->set_response($data);
    }
    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}