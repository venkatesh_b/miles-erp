<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Source wise report
 * @category        Controller
 * @author          Parameshwar
 */
class CityReport extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("CityReport_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

   public function getCityReport_get() {
        $message='City Report Details';
        $status=true;

        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $course = strlen(trim($this->get('course')))>0?"'".trim($this->get('course'))."'":'NULL';
//        $stage = strlen(trim($this->get('stage')))>0?"'".trim($this->get('stage'))."'":'NULL';
        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0){
            $status=false;
            $message = 'Select any filter';
            $res = [];
        }else{
            $res = $this->CityReport_model->getCityReport($branch, $course, $this->get('branchText'), $this->get('courseText'));
        }
        if($res===false){
            $data = array("status" => false, "message" => 'No Records Found', "data" => array());
        }
        else {
            $data=array("status"=>$status,"message"=>$message,"data"=>$res);
        }
        $this->set_response($data);
    }
    function getkey($pos){

        return chr(65+$pos);

    }
    public function getCityReportExport_get()
    {

        $message = 'Branch Details';
        $status = true;
        $this->load->library('excel');
        $branch = strlen(trim($this->get('branch'))) > 0 ? "'" . trim($this->get('branch')) . "'" : 'NULL';
        $course = strlen(trim($this->get('course'))) > 0 ? "'" . trim($this->get('course')) . "'" : 'NULL';
//        $stage = strlen(trim($this->get('stage'))) > 0 ? "'" . trim($this->get('stage')) . "'" : 'NULL';
        if (strlen(trim($this->get('branch'))) <= 0 && strlen(trim($this->get('course'))) <= 0) {
            $status = false;
            $message = 'Select any filter';
            $res = [];
        } else {
            $res = $this->CityReport_model->getCityReport($branch, $course, $this->get('branchText'), $this->get('courseText'));
        }
        if($res===false){
            $data = array("status" => false, "message" => 'No Records Found', "data" => array());
        }
        else {
//            print_r($res);die;
            $header_main = implode('||', $res['course']);
            $header_main .= '||Total';
            $sub_columns = 'Lead||Count||Conversion Rate||Probability';
            $sub_header_main = '';
            for($i =0 ; $i < count($res['course'])+1; $i++){
                $sub_header_main .= $sub_columns.'||';
            }
            $sub_header_main = rtrim($sub_header_main, '||');
//            $sub_header_main .= '||Admission||Probability';
            $datarows = array();
            
            $repeat = count($res['course']);    //for course
            $lastColumn = $repeat<3?6:4;    //for last column
            
            foreach ($res['data'] as $k => $v) {
                $count = 0;
                foreach ($v as $k1 => $v1) {
                    if($count == 0){
                        $datarows[] = $v1['branchName'];
                        $lead = $v1['lead_stage'];
                        $leadCount = 0;
                        $conversion_rate_Count = 0;
                        $probCount = 0;
                    }
                    foreach ($v1 as $k2 => $v2) {
                        if(!in_array($k2, array('branchName', 'courseName'))){
                            $datarows[] = $v2;
                        }
                        if($k2 == 'leadCount'){
                            $leadCount += (float)$v2;
                        }
                        
                        if($k2 == 'prob'){
                            $probCount += (float)$v2;
                        }
                        
                        if($k2 == 'conversion_rate'){
                            $conversion_rate_Count = $v2;
                        }
                    }
                    $count++;
                    if($count == $repeat){
                        $datarows[] = $lead;
                        $datarows[] = $leadCount;
                        $datarows[] = $conversion_rate_Count;
                        $datarows[] = $probCount;
                        $data_main[] = implode('||', $datarows);
                        $datarows = array();
                        $count = 0;
                    }
                }
            }
            $header = $header_main;

            $headervals = explode('||', $header);
            $subheadervals = explode('||', $sub_header_main);
            
            $excelRowstartsfrom=3;
            $excelColumnstartsFrom=2;
            $excelstartsfrom=$excelRowstartsfrom;
            $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+((count($headervals)*$repeat)+$lastColumn)) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'LEAD REPORT BY CITY');

            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );

            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+((count($headervals)*$repeat)+$lastColumn)) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Branches");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('branchText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+((count($headervals)*$repeat)+$lastColumn)) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel2 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+((count($headervals)*$repeat)+$lastColumn)) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel2);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Courses");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('courseText'));
            $mrgesel2_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+((count($headervals)*$repeat)+$lastColumn)) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;
            
            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+((count($headervals)*$repeat)+$lastColumn)) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Report on");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
            $excelstartsfrom=$excelstartsfrom+1;
            $head_starts_from = $excelstartsfrom+1;
            $end_pos = 0;
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom) . $head_starts_from,'Branch');
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );
            for ($k = 0; $k < count($headervals); $k++) {
                $mrgesel3 = $this->getkey($excelColumnstartsFrom+1+(($k)*4)) . $head_starts_from.':' . $this->getkey($excelColumnstartsFrom+(($k+1)*4)) . $head_starts_from;
                $end_pos = $this->getkey($excelColumnstartsFrom+1+(($k)*4));
                $this->excel->getActiveSheet()->mergeCells($mrgesel3);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+1+(($k)*4)) . $head_starts_from, $headervals[$k]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+1+(($k)*4)))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+1+(($k)*4)) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );

            }

            $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+((count($headervals)*$repeat)+$lastColumn)) . $head_starts_from;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
            
            $excelstartsfrom=$excelstartsfrom+1;
            $head_starts_from = $excelstartsfrom+1;
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom) . $head_starts_from,'');
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );
            for ($k = 0; $k < count($subheadervals); $k++) {
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$k+1) . $head_starts_from, $subheadervals[$k]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k+1))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k+1) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );

            }
            $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+((count($headervals)*$repeat)+$lastColumn)) . $head_starts_from;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
            
            $rowsStratsFrom = $head_starts_from + 1;
            $c = $rowsStratsFrom;
            for ($k = 0; $k < count($data_main); $k++) {
                $datavals = explode('||', $data_main[$k]);
                for ($j = 0; $j < count($datavals); $j++) {
//                    if($j<15){
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, $datavals[$j]);

//                    }
                }
                $c++;
            }
            
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('LEAD REPORT BY CITY');
//            $this->excel->getActiveSheet()->getStyle($this->getkey($k) . $head_starts_from)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $filename = "Citywisereport_".date('Ymd_His') . '.xls'; //save our workbook as this file name
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $file_path = './uploads/' . $filename;
            $objWriter->save($file_path);
            $file_path = base_url().'uploads/' . $filename;
            $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
        }
        $this->set_response($data);
    }

    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 April 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}