<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 18-03-2016
 * Time: 10:00 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class ColdCalling extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("ColdCalling_model");
        $this->load->model("References_model");
        $this->load->model('Branch_model');
        $this->load->model('Tag_model');
        $this->load->model('Company_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function customvalidation($data)
    {

        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE) {
            if (count($this->form_validation->error_array()) > 0) {
                return $this->form_validation->error_array();
            } else {
                return true;
            }
        } else {
            return true;
        }

    }
    function getColdCallsList_get()
    {

        $param['branchId']=$this->get('branchId');
        $param['userId']=$_SERVER['HTTP_USER'];
        $param['appliedFiltersString']=$this->get('appliedFiltersString');
        $this->response($this->ColdCalling_model->coldCallListDataTables($param),REST_Controller::HTTP_OK);
        exit;
    }
    function saveColdCall_post(){
        $param['userId']=$_SERVER['HTTP_USER'];
        $param['leadId']=decode($this->post('leadId'));
        $param['ColdCallContactName']=$this->post('ColdCallContactName');
        $param['ColdCallContactEmail']=$this->post('ColdCallContactEmail');
        $param['ColdCallContactPhone']=$this->post('ColdCallContactPhone');
        $param['ColdCallContactAltPhone']=$this->post('ColdCallContactAltPhone');
        $param['ColdCallStatus']=$this->post('ColdCallStatus');
        $param['ColdcallStatusInfo']=$this->post('ColdcallStatusInfo');
        $param['ColdCallContactCourse']=$this->post('ColdCallContactCourse');
        $param['ColdCallContactBranch']=$this->post('ColdCallContactBranch');
        $param['coldCallcontacttype']=$this->post('coldCallcontacttype');
        $param['ColdCallContactQualification']=$this->post('ColdCallContactQualification');
        $param['ColdCallContactCompany']=$this->post('ColdCallContactCompany');
        $param['ColdCallContactInstitution']=$this->post('ColdCallContactInstitution');
        $param['ColdCallDescription']=$this->post('ColdCallDescription');
        $nextFollowUpdate = $this->post('ColdcallNextFollowupData');
        $callType = $this->post('callType');

        if(isset($nextFollowUpdate) && !empty($nextFollowUpdate))
        {
            $nextFollowUpdate = DateTime::createFromFormat('d M, Y',$this->post('ColdcallNextFollowupData'));
            $nextFollowUpdate= $nextFollowUpdate->format("Y-m-d H:i:s");
        }
        else
        {
            $nextFollowUpdate=date("Y-m-d H:i:s");
        }
        $param['ColdcallNextFollowupDate'] = $nextFollowUpdate;
        $res=$this->ColdCalling_model->saveColdCall($param);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function coldCallCounts_get(){
        $param['branchId']=$this->get('branchId');
        $param['userId']=$_SERVER['HTTP_USER'];
        $res=$this->ColdCalling_model->coldCallCounts($param);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getContactConfigurations_get(){

        $final_data=array();
        $param['branchId']=$this->get('branchId');
        $final_data['branches']=array();
        $final_data['cities']=array();
        $final_data['tags']=array();
        $final_data['companies']=array();

        $res_reftypeid=$this->ColdCalling_model->getBranchPrimaryCities($param);

        if($res_reftypeid['status']==true){

                $final_data['cities']=$res_reftypeid['data'];
            foreach($res_reftypeid['data'] as $k=>$v){

                $final_data['countries'][0]=array("countryId"=>$v->country_id,"countryName"=>$v->Country);
            }

            $final_data['branches']=array();

            $tags=$this->Tag_model->getAllTags();

            if($tags['status']==true)
                $final_data['tags']=$tags['data'];


                $final_data['companies']=array();

        }
        $final_response['response']=[
            'status' => true,
            'message' => 'success',
            'data' => $final_data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;

        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function addContact_post(){
        $dataPost = json_decode(file_get_contents("php://input"), true);
        if($dataPost){ $_POST = $dataPost; }
        $this->form_validation->set_data($this->post());
        $data['contactName']=$contactName=$this->post('contactName');
        $data['contactEmail']=$contactEmail=$this->post('contactEmail');
        $data['contactPhone']=$contactPhone=$this->post('contactPhone');
        $data['contactCity']=$contactCity=$this->post('contactCity');
        $data['contactCountry']=$contactCountry=$this->post('contactCountry');
        $data['contactTags']=$contactTags=$this->post('contactTags');
        $data['createdBy']=$_SERVER['HTTP_USER'];
        $data['branchId']=$this->post('branchId');
        $data['contactSource']=$contactSource=$this->post('contactSource');


        $validations[]=["field"=>"contactName","label"=>'contactName',"rules"=>"required|regex_match[/^[A-Za-z0-9- ]+$/]","errors"=>array('required' => 'Contact name should not be blank','regex_match' => 'Contact name should be alphabetic only')];
        $validations[]=["field"=>"contactEmail","label"=>'contactEmail',"rules"=>"valid_email","errors"=>array('required' => 'Email should not be blank','valid_email' => 'Email is invalid','is_unique'=>'Email already exists.')];
        $validations[]=["field"=>"contactPhone","label"=>'contactPhone',"rules"=>"regex_match[/^[0-9() +-]{8,15}$/]","errors"=>array('required' => 'Phone number should not be blank','regex_match' => 'Invalid phone number','is_unique'=>'Phone number already exists.')];
        $validations[]=["field"=>"contactCountry","label"=>'contactCountry',"rules"=>"required","errors"=>array('required' => 'Country should not be blank','integer' => 'City id should be numeric')];
        $validations[]=["field"=>"contactCity","label"=>'contactCity',"rules"=>"required","errors"=>array('required' => 'City should not be blank','integer' => 'City id should be numeric')];
        //$validations[]=["field"=>"contactTags","label"=>'contactTags',"rules"=>"required","errors"=>array('required' => 'Tags should not be blank','regex_match' => 'Tag ids should be numeric with comma(,) seperated')];
        $validations[]=["field"=>"contactSource","label"=>'contactSource',"rules"=>"required","errors"=>array('required' => 'Source should not be blank','integer' => 'Source id should be numeric')];
        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true){
            $addContactres=$this->ColdCalling_model->addContact($data);
            $final_response['response']=[
                'status' => $addContactres['status'],
                'message' => $addContactres['message'],
                'data' => $addContactres['data'],
            ];


            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

}