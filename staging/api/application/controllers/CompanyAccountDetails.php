<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: V Parameshwar. Date: 04-02-2016
 * Purpose: Services for Companies.
 * 
 */

class CompanyAccountDetails extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('CompanyAccountDetails_model');
        $this->load->model('References_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){

    $this->form_validation->set_rules($data);

    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }

    }
    function AddCompanyAccountDetails_post(){

            $postdata = json_decode(file_get_contents("php://input"), true);
            if($postdata){ $_POST = $postdata; }
            $this->form_validation->set_data($this->post());
            $companyName=trim($this->post('companyName'));
            $accountNumber=trim($this->post('accountNumber'));
            $bankName=$this->post('bankName');
            $Ifsc=trim($this->post('Ifsc'));
            $status=intval($this->post('status'));
            $branchName=trim($this->post('branchName'));
            $city=$this->post('City');
            $UserID=$this->post('UserID');

        $validations[]=["field"=>"companyName","label"=>'companyName',"rules"=>"required|regex_match[/^[0-9 ]+$/]","errors"=>array('required' => 'Name should not be blank','regex_match' => 'Id should be integer  only')];
        $validations[]=["field"=>"accountNumber","label"=>'accountNumber',"rules"=>"required|regex_match[/^[0-9]+$/]","errors"=>array('required' => 'Code should not be blank','regex_match' => 'Code should be numeric only')];
        $validations[]=["field"=>"bankName","label"=>'bankName',"rules"=>"required","errors"=>array('required' => 'Bank Name should not be blank','regex_match' => 'Bank Name should be alphabets only')];
        $validations[]=["field"=>"Ifsc","label"=>'Ifsc',"rules"=>"","errors"=>array('required' => 'Ifsc  should not be blank')];
        $validations[]=["field"=>"branchName","label"=>'branchName',"rules"=>"required","errors"=>array('required' => 'Branch Name should not be blank','regex_match' => 'Name should be alphabets only')];
        $validations[]=["field"=>"City","label"=>'City',"rules"=>"required","errors"=>array('required' => 'City should not be blank','regex_match' => 'city should be alphabets only')];
        $validations[]=["field"=>"status","label"=> 'status',"rules"=>"required|integer|regex_match[/^[0-1]$/]",
            "errors"=>array('required' => 'Status should not be blank','integer' => 'Status should be integer value only','regex_match' => 'Status should be either 0 or 1')];
        //$validations[]=["field"=>"roleDescription","label"=> 'Role Description ',"rules"=>"required","errors"=>array('required' => 'Role Description should not be blank')];

            $validationstatus=$this->customvalidation($validations);

            if($validationstatus===true){
                $data=array("fk_company_id"=>$companyName,'acc_no'=>$accountNumber,"bank_name"=>$bankName,
                    "bank_ifsc"=>$Ifsc,'bank_branch'=>$branchName,"bank_city"=>$city,
                    'fk_created_by'=>$UserID,"created_date"=>date('Y-m-d h:i:s'),'status'=>(int)$status

                );


                $getType=$this->CompanyAccountDetails_model->saveCompanyDetails($data);

                $final_response['response']=[
                    'status' => $getType['status'],
                    'message' => $getType['message'],
                    'data' => $getType['data'],
                ];

            }
            else{

                $final_response['response']=[
                    'status' => FALSE,
                    'message' => 'validation failed',
                    'data' => $validationstatus,
                ];
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function getSingleCompanyDetails_get(){
        $company_id=$this->get('companyID');
        // $company_id=decode($company_id);
        $this->form_validation->set_data(array("companyID" => $company_id));

        $validations[]=["field"=>"companyID","label"=>'companyID',"rules"=>"required|integer","errors"=>array(
            'required' => 'company Id should not be blank','integer' => 'company Id should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){

            $company_data=$this->CompanyAccountDetails_model->getSingleCompanydetail($company_id);
            $final_response['response']=[
                'status' => $company_data['status'],
                'message' => $company_data['message'],
                'data' => $company_data['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function updateCompanyAccountDetails_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $companyName=trim($this->post('companyName'));
        $accountNumber=trim($this->post('accountNumber'));
        $bankName=$this->post('bankName');
        $Ifsc=trim($this->post('Ifsc'));
        $branchName=trim($this->post('branchName'));
        $status=intval($this->post('status'));
        $city=$this->post('City');
        $UserID=$this->post('UserID');
    $companyID=$this->post('CompanyID');

        $validations[]=["field"=>"companyName","label"=>'companyName',"rules"=>"required|regex_match[/^[0-9 ]+$/]","errors"=>array('required' => 'Name should not be blank','regex_match' => 'Company id should be integer  only')];
        $validations[]=["field"=>"accountNumber","label"=>'accountNumber',"rules"=>"required|regex_match[/^[0-9 ]+$/]","errors"=>array('required' => ' Code should not be blank','regex_match' => 'Code should be numeric only')];
        $validations[]=["field"=>"bankName","label"=>'bankName',"rules"=>"required","errors"=>array('required' => ' bank name should not be blank')];
        $validations[]=["field"=>"Ifsc","label"=>'Ifsc',"rules"=>"","errors"=>array('required' => 'Ifsc should not be blank')];
        $validations[]=["field"=>"branchName","label"=>'branchName',"rules"=>"required","errors"=>array('required' => 'branch Name should not be blank')];
        $validations[]=["field"=>"City","label"=>'City',"rules"=>"required","errors"=>array('required' => 'City Name should not be blank')];
        $validations[]=["field"=>"status","label"=> 'status',"rules"=>"required|integer|regex_match[/^[0-1]$/]",
            "errors"=>array('required' => 'Status should not be blank','integer' => 'Status should be integer value only','regex_match' => 'Status should be either 0 or 1')];

        //$validations[]=["field"=>"roleDescription","label"=> 'Role Description ',"rules"=>"required","errors"=>array('required' => 'Role Description should not be blank')];


        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array("fk_company_id"=>$companyName,'acc_no'=>$accountNumber,"bank_name"=>$bankName,
                "bank_ifsc"=>$Ifsc,'bank_branch'=>$branchName,"bank_city"=>$city,
                'fk_created_by'=>$UserID,"created_date"=>date('Y-m-d h:i:s'),'status'=>$status

            );


            $getType=$this->CompanyAccountDetails_model->updateCompanyDetails($data,$companyID);
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function getAllCompanies_get(){
        $result=$this->CompanyAccountDetails_model->getAllCompanies();
       if(isset($result) && !empty($result)){
           $data=array("status"=>true,"message"=>"","data"=>$result);

       }else{
           $data=array("status"=>false,"message"=>"There are no companies","data"=>"");

       }

           $this->set_response($data);
    }
    function getCompanyAccounts_get(){

         $this->response($this->CompanyAccountDetails_model->getCompanyAccountDetails());
    }
    function deleteCompanyAccountDetails_get(){

        $companyId=$this->get('companyId');

        $this->form_validation->set_data(array("companyId" => $this->get('companyId')));

        $validations[]=["field"=>"companyId","label"=>'companyId',"rules"=>"required|integer","errors"=>array(
            'required' => 'Company ID should not be blank','integer' => 'Company ID should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
           // $resp_checkTag=$this->Company_model->checkCompany($companyId);
                $res_delete=$this->CompanyAccountDetails_model->deleteCompany($companyId);
                $final_response['response']=[
                    'status' => $res_delete['status'],
                    'message' => $res_delete['message'],
                    'data' => $res_delete['data'],
                ];
                if($res_delete['status']===true){
                    //sucess
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    //fail
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }

        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

}

