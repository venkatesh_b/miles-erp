<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Configuration
 * @category        Controller
 * @author          Abhilash
 */
class Configuration extends REST_Controller {

    function __construct() {
        parent::__construct();
//        $this->load->database();
        
        $this->load->model("App_configuration_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('Put_method_extenstion');   /* method PUT */
    }

    /**
     * Function for geting configuration settings list and specific configuration settings)
     * 
     * @args 
     *  "id"(INT) -> id for configuration [NOT Required]
     * 
     * @return - array
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    function getAllConfiguration_get() {

        //for scope verification
        /*$is_having_scope = $this->oauth->checkscope('configurationlist');
        if(!$is_having_scope){
             $this->result_set('401', 'Access Denied', '');
        }*/

        $status=true;$message="";$results='';
        
        //no id
        $result = $this->App_configuration_model->getConfiguration();
        if (isset($result['error']) && $result['error'] == 'dberror') 
        {
            $message='Database error';
            $status=false;
            $results = $result['msg'];
        }
        else if ($result)
        {
            if(count($result)>0){
                $message='Configuration List';
                $status=true;
                $results = $result;
            }else{
                $message='Configuration List';
                $status=true;
                $results = array();
            }
        } 
        else
        {
            $message='No Data found';
            $status=true;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }
    
    /**
     * Function for geting configuration settings for specific configuration settings
     * 
     * @args 
     *  "config_id"(INT) -> id for configuration
     * 
     * @return - array
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    function getSingleConfiguration_get() {

        //for scope verification
        /*$is_having_scope = $this->oauth->checkscope('configurationlist');
        if(!$is_having_scope){
             $this->result_set('401', 'Access Denied', '');
        }*/
        
        $status=true;$message="";$results='';
        
        $id = $this->get('config_id');
        
        //when id is present
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run($this) == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } 
        else
        {
            $result = $this->App_configuration_model->getConfiguration($id);
            if (isset($result['error']) && $result['error'] == 'dberror') 
            {
                $message='Database error';
                $status=false;
                $results = $result['msg'];
            } 
            else if($result == 'nodata')
            {
                $message="No data found";
                $status=false;  
            }
            else if ($result) 
            {
                $message="Data rendered successfully";
                $status=true;
                $results = $result;
            }
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    /**
     * Function for geting configuration settings for specific configuration settings
     * 
     * @args 
     *  "config_name"(INT) -> id for configuration
     * 
     * @return - array
     * @date modified - 28 April 2016
     * @author : Abhilash
     */
    function getSingleConfigurationByName_get() {
        $status=true;$message="";$results='';
        
        $id = $this->get('config_name');
        
        //when id is present
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required|callback_name_check');

        if ($this->form_validation->run($this) == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } 
        else
        {
            $result = $this->App_configuration_model->getConfigurationByName($id);
            if (isset($result['error']) && $result['error'] == 'dberror') 
            {
                $message='Database error';
                $status=false;
                $results = $result['msg'];
            } 
            else if($result == 'nodata')
            {
                $message="No data found";
                $status=false;  
            }
            else if ($result) 
            {
                $message="Data rendered successfully";
                $status=true;
                $results = $result;
            }
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }
    
    /**
     * Function adding new site configuration
     * 
     * @args 
     *  "key"(String) -> key[POST]
     *  "value"(String) -> value[POST]
     * 
     * @return - (Bool)
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function addConfiguration_post() {
        
        /*$is_having_scope = $this->oauth->checkscope('configurationPost');
        if(!$is_having_scope){
             $this->result_set('401', 'Access Denied', '');
        }*/
        $status=true;$message="";$results='';
        
        $config = array(
            array(
                'field' => 'key',
                'label' => 'key',
                'rules' => 'required|is_unique[`app_configuration_setting`.`key`]|callback_name_check'
            ),
            array(
                'field' => 'value',
                'label' => 'value',
                'rules' => 'required|callback_name_check'
            )
        );
        
        /**/
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
//        $data = $this->input->post();
        /**/

        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) 
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } else {
            $result = $this->App_configuration_model->addConfiguration($this->input->post('key'), $this->input->post('value'));
            if (isset($result['error']) && $result['error'] == 'dberror') 
            {
                $message='Database error';
                $status=false;
                $results = $result['msg'];
            } 
            else if ($result === 'duplicate')
            {
                $message="This Key is already in use, please use another one";
                $status=false;
            } 
            else if ($result) 
            {
                $message="Data Saved";
                $status=true;
            } 
            else 
            {
                $message="Data can't be added";
                $status=false;
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    /**
     * Function Updateing existing site configuration
     * 
     * @args 
     *  "key"(String) -> key[POST]
     *  "value"(String) -> value[POST]
     *  "id"(INT) -> congiguration ID
     * 
     * @return - (Bool)
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function editConfiguration_put() {
        
        /*$is_having_scope = $this->oauth->checkscope('configurationPut');
        if(!$is_having_scope){
             $this->result_set('401', 'Access Denied', '');
        }*/
        $status=true;$message="";$results='';
        
        $id = $this->get('config_id');

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'callback_num_check');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        } 
        else
        {
            $this->put = file_get_contents('php://input');
            //parse and clean request
            $this->put_method_extenstion->_parse_request($this->put);

            $config = array(
                array(
                    'field' => 'key',
                    'label' => 'key',
                    'rules' => 'required|is_unique[`app_configuration_setting`.`key`]|callback_name_check'
                ),
                array(
                    'field' => 'value',
                    'label' => 'value',
                    'rules' => 'required|callback_name_check'
                )
            );
            $this->form_validation->set_rules($config);
            $this->form_validation->set_data($this->put());
            if ($this->form_validation->run() == FALSE) {
                $message="Validation Error";
                $status=false;
                $results=$this->form_validation->error_array();
            } else {
                $result = $this->App_configuration_model->editConfiguration($id, $this->put("key"), $this->put("value"));
                
                if (isset($branchList['error']) && $branchList['error'] == 'dberror')
                {
                    $message='Database error';
                    $status=false;
                    $results = $result['msg'];
                }
                else if ($result === 'duplicate')
                {
                    $message='This Key is already in use, please use another one';
                    $status=false;
                } 
                else if ($result === 'badrequest')
                {
                    $message='No Id Found';
                    $status=false;
                } 
                else if ($result) 
                {
                    $message='Data Updated successfully';
                    $status=true;
                } 
                else 
                {
                    $message='Data Can\'t be updated';
                    $status=false;
                }
            }
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    /**
     * Function deleting existing site configuration
     * 
     * @args 
     *  "key"(String) -> key[POST]
     *  "value"(String) -> value[POST]
     *  "config_id"(INT) -> congiguration ID
     * 
     * @return - (Bool)
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function deleteConfiguration_delete() {

        $status=true;$message="";$results='';
        
        $id = $this->get('config_id');

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "id", 'callback_num_check');

        if ($this->form_validation->run() == FALSE) 
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        }
        else 
        {

            $result = $this->App_configuration_model->deleteConfiguration($id);

            if (isset($result['error']) && $result['error'] == 'dberror')
            {
                $message='Database error';
                $status=false;
                $results = $result['msg'];
            } 
            else if ($result === 'active') 
            {
                $message="Setting Activated";
                $status=true;
            } 
            else if ($result === 'deactivated')
            {
                $message="Setting De-Activated";
                $status=true;
            } 
            else if ($result === 'nodata') 
            {
                $message='Id : '.$id.' not found';
                $status=false;
            } 
            else {
                $message='Data Can\'t be delete';
                $status=false;
            }
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }
    
    /**
     * Function for checking number(REGEX)
     * 
     * @args 
     *  "string"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
//        die('asd');
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Function for checking valid string
     * 
     * @args 
     *  "str"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function name_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str)) {
            $this->form_validation->set_message('name_check', 'The %s field is not valid!, use only Alphabets');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Function Name - result_set
     * @param type String, $status EX - 200, 400,.. etc
     * @param type String, $msg Ex - DB error, validation error message, Date rendered sucessfully, etc.
     * @param type - String/Array
     * 
     * #auther - Abhilash
     * 
     * #modified - 1 Feb 2016
     */
    public function result_set($status, $msg, $data) 
    {
        $resp_array = ['200', '201'];
        if (in_array($status, $resp_array)) 
        {
//            $data = $data;
            $statusMsg = 'TRUE';
        } 
        else
        {
//            $data = $data;
            $statusMsg = 'FALSE';
        }

        $this->response([
            'status' => $statusMsg,
            'message' => $msg,
            'data' => $data
                ], $status);
    }
}