<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 25/4/16
 * Time: 5:26 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class DailyRevenue extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('Company_model');
        $this->load->model('DailyRevenue_model');
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('Put_method_extenstion');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function getRevenuePostingDetails_get()
    {
        $branch_id=$this->input->get('branchId');
        $company_id=$this->input->get('companyId');
        $payment_mode=$this->input->get('PaymentMode');
        $chosen_date = DateTime::createFromFormat('d M, Y', $this->get('selectedDate'));

        $chosen_date = $chosen_date->format("Y-m-d");
        $revenuePostingData=$this->DailyRevenue_model->getDailyRevenuePosting($branch_id,$company_id,$payment_mode,$chosen_date);
        $this->response($revenuePostingData);
    }
    function updateCashRevenuePosting_post()
    {
        //rawRevenueCashPosting
        $rawRevenueCashPosting=$this->input->post('rawRevenueCashPosting');
        if($this->input->post('paymentType')=='cash')
            $is_cash=0;
        else
            $is_cash=1;
        $selected_date = DateTime::createFromFormat('d M, Y', $this->post('selected_date'));
        $selected_date = $selected_date->format("Y-m-d H:i:s");
        $drs_data = array(
            'opening_balance'       =>  $this->input->post('opening_balance'),
            'amount_collected'      =>  $this->input->post('amount_collected'),
            'amount_deposited'      =>  $this->input->post('amount_deposited'),
            'signoff_amount'        =>  $this->input->post('signoff_amount'),
            'closing_balance'       =>  $this->input->post('closing_balance'),
            'selected_date'         =>  $selected_date,
            'company_id'            =>  $this->input->post('company_id'),
            'branch_id'             =>  $this->input->post('branch_id'),
            'payment_type'          =>  $is_cash,
            'signoff_amount'        =>  $this->input->post('signoffAmount'),
            'signoff_comments'        =>  $this->input->post('signoffComments')
        );
        $revenuePostingData=$this->DailyRevenue_model->updateDailyRevenueCashPosting($rawRevenueCashPosting,$drs_data);
        $this->response($revenuePostingData);
    }

    function updateOtherRevenuePosting_post()
    {
        $rawRevenueOtherPosting=$this->input->post('rawRevenueOtherPosting');
        if($this->input->post('paymentType')=='cash')
            $is_cash=0;
        else
            $is_cash=1;
        $selected_date = DateTime::createFromFormat('d M, Y', $this->post('selected_date'));
        $selected_date = $selected_date->format("Y-m-d H:i:s");
        $drs_data = array(
            'opening_balance'       => $this->input->post('opening_balance'),
            'amount_collected'      => $this->input->post('amount_collected'),
            'amount_deposited'      => $this->input->post('amount_deposited'),
            'signoff_amount'        => $this->input->post('signoff_amount'),
            'closing_balance'       => $this->input->post('closing_balance'),
            'selected_date'         =>  $selected_date,
            'company_id'            => $this->input->post('company_id'),
            'branch_id'             => $this->input->post('branch_id'),
            'payment_type'          => $is_cash,
        );
        $revenuePostingData=$this->DailyRevenue_model->updateDailyRevenueOtherPosting($rawRevenueOtherPosting,$drs_data);
        $this->response($revenuePostingData);
    }

    function getRevenueAuthorizationDetails_get()
    {
        $branch_id=$this->input->get('branchId');
        $company_id=$this->input->get('companyId');
        $payment_mode=$this->input->get('PaymentMode');
        $chosen_date = DateTime::createFromFormat('d M, Y', $this->get('selectedDate'));
        $chosen_date = $chosen_date->format("Y-m-d");
        $revenuePostingData=$this->DailyRevenue_model->getDailyRevenueAuthorization($branch_id,$company_id,$payment_mode,$chosen_date);
        $this->response($revenuePostingData);
    }

    function updateDailyRevenueAuthorization_post()
    {
        $rawRevenueAuthorizationItems=$this->input->post('rawRevenueAuthorization');
        $revenuAuthorization=$this->DailyRevenue_model->updateDailyRevenueAuthorization($rawRevenueAuthorizationItems);
        $this->response($revenuAuthorization);
    }

    function getRevenueCollectionDetails_get()
    {
        $branch_id=$this->input->get('branchId');
        $companyIds=$this->input->get('companyId');
        $payment_mode=$this->input->get('PaymentMode');
        $chosen_date = DateTime::createFromFormat('d M, Y', $this->get('selectedDate'));
        $chosen_date = $chosen_date->format("Y-m-d");
        $revenuePostingData=$this->DailyRevenue_model->getDailyRevenueCollection($branch_id,$companyIds,$payment_mode,$chosen_date);
        $this->response($revenuePostingData);
    }

    function getkey($pos){

        return chr(65+$pos);

    }

    public function getFeeDetailsReportExport_get()
    {
        $branch_id=$this->input->get('branchId');
        $companyIds=$this->input->get('companyId');
        $payment_mode=$this->input->get('PaymentMode');
        $chosen_date = DateTime::createFromFormat('d M, Y', $this->get('selectedDate'));
        $chosen_date = $chosen_date->format("Y-m-d");
        $revenuePostingData=$this->DailyRevenue_model->getDailyRevenueCollection($branch_id,$companyIds,$payment_mode,$chosen_date);
        $message='DAILY REVENUE COLLECTION';
        $status = true;
        $this->load->library('excel');
        if($revenuePostingData['status']===false){
            $data = array("status" => false, "message" => $revenuePostingData['message'], "data" => array());
        }
        else
        {
            $excelRowstartsfrom=3;
            $excelColumnstartsFrom=2;
            $excelstartsfrom=$excelRowstartsfrom;

            $last_column=count($revenuePostingData['data'])*2+(count($revenuePostingData['data'])-1);
            $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'DAILY REVENUE COLLECTION');

            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );

            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Companies");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('selectedFeeCompanyText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Mode of Payment");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('PaymentModeText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Date");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('selectedDate'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Report on");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $sub_excelstartsfrom=0;
            $sub_excelstartsfrom=$excelstartsfrom;
            $sub_excelColumnstartsFrom=$excelColumnstartsFrom;
            $max_rownumber=0;
            foreach($revenuePostingData['data'] as $kd=>$vd)
            {
                $mrgesel1 = $this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom.':' . $this->getkey($sub_excelColumnstartsFrom+2 - 1) . $sub_excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, $kd);

                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($sub_excelColumnstartsFrom+0))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        ),
                        'font'  => array(
                            'bold'  => true,
                            'color' => array('rgb' => 'FFFFFF')
                        )
                    )
                );

                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');


                $sub_excelstartsfrom=$sub_excelstartsfrom+1;

                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'Particulars');

                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($sub_excelColumnstartsFrom+1+0) . $sub_excelstartsfrom, 'Amount');

                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($sub_excelColumnstartsFrom+0))->setAutoSize(false);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($sub_excelColumnstartsFrom+0))->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($sub_excelColumnstartsFrom+1+0))->setAutoSize(false);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($sub_excelColumnstartsFrom+1+0))->setWidth(35);

                $mrgesel1 = $this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom.':' . $this->getkey($sub_excelColumnstartsFrom+2 - 1) . $sub_excelstartsfrom;
                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('C2F7D8');

                $sub_excelstartsfrom=$sub_excelstartsfrom+1;

                $mrgesel1 = $this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom.':' . $this->getkey($sub_excelColumnstartsFrom+2 - 1) . $sub_excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'Today Fee Head wise Collection');
                $this->excel->getActiveSheet()->getStyle($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom)->applyFromArray(
                    array(
                        'font'  => array(
                            'bold'  => true
                        )
                    )
                );

                if($vd['collection']==0)
                {
                    $sub_excelstartsfrom=$sub_excelstartsfrom+1;
                    $mrgesel1 = $this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom.':' . $this->getkey($sub_excelColumnstartsFrom+2 - 1) . $sub_excelstartsfrom;
                    $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'No amount collected for the day');
                }
                else
                {
                    //loop through each fee head
                    foreach($vd['collection'] as $k_collection=>$v_collection)
                    {
                        $sub_excelstartsfrom=$sub_excelstartsfrom+1;
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, $v_collection['name']);
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($sub_excelColumnstartsFrom+1+0) . $sub_excelstartsfrom, $v_collection['amount_paid']);
                    }
                }
                if($vd['total_collection']>0)
                {
                    $sub_excelstartsfrom=$sub_excelstartsfrom+1;
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'Total');
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($sub_excelColumnstartsFrom+1+0) . $sub_excelstartsfrom, $vd['total_collection']);
                }

                $sub_excelstartsfrom=$sub_excelstartsfrom+1;

                $mrgesel1 = $this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom.':' . $this->getkey($sub_excelColumnstartsFrom+2 - 1) . $sub_excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'Deposits');
                $this->excel->getActiveSheet()->getStyle($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom)->applyFromArray(
                    array(
                        'font'  => array(
                            'bold'  => true
                        )
                    )
                );

                if($vd['bank_deposits']==0)
                {
                    $sub_excelstartsfrom=$sub_excelstartsfrom+1;
                    $mrgesel1 = $this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom.':' . $this->getkey($sub_excelColumnstartsFrom+2 - 1) . $sub_excelstartsfrom;
                    $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'No Deposits done for the day');
                }
                else
                {
                    //loop through each deposit
                    foreach($vd['bank_deposits'] as $k_deposits=>$v_deposits)
                    {
                        $sub_excelstartsfrom=$sub_excelstartsfrom+1;
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, $v_deposits['bank_name']."\n". $v_deposits['acc_no']);
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($sub_excelColumnstartsFrom+1+0) . $sub_excelstartsfrom, $v_deposits['deposited_amount']);
                    }
                }

                $sub_excelstartsfrom=$sub_excelstartsfrom+1;

                $mrgesel1 = $this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom.':' . $this->getkey($sub_excelColumnstartsFrom+2 - 1) . $sub_excelstartsfrom;
                $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'Cash in Hand');
                $this->excel->getActiveSheet()->getStyle($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom)->applyFromArray(
                    array(
                        'font'  => array(
                            'bold'  => true
                        )
                    )
                );
                if($vd['in_hand']==0)
                {
                    $sub_excelstartsfrom=$sub_excelstartsfrom+1;
                    $mrgesel1 = $this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom.':' . $this->getkey($sub_excelColumnstartsFrom+2 - 1) . $sub_excelstartsfrom;
                    $this->excel->getActiveSheet()->mergeCells($mrgesel1);
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'No posting done for the day');
                }
                else
                {
                    //loop through each deposit
                    $sub_excelstartsfrom=$sub_excelstartsfrom+1;
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'Cash in Hand');
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($sub_excelColumnstartsFrom+1+0) . $sub_excelstartsfrom, $vd['in_hand'][0]['opening_balance']);

                    $sub_excelstartsfrom=$sub_excelstartsfrom+1;
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($sub_excelColumnstartsFrom+0) . $sub_excelstartsfrom, 'Other In Hand');
                    $this->excel->setActiveSheetIndex(0)
                        ->setCellValue($this->getkey($sub_excelColumnstartsFrom+1+0) . $sub_excelstartsfrom, $vd['in_hand'][0]['closing_balance']);
                }

                if($max_rownumber<$sub_excelstartsfrom)
                {
                    $max_rownumber=$sub_excelstartsfrom;
                }
                $sub_excelstartsfrom=$excelstartsfrom;
                $sub_excelColumnstartsFrom=$sub_excelColumnstartsFrom+3;
            }
            $excelstartsfrom=$max_rownumber+1;
            $footer_starts_from=$max_rownumber+4;

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from,'SIGNATURES');

            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '04578C')
                    )
                )
            );

            $footer_starts_from=$footer_starts_from+1;


            $test=(int)(($last_column-1)/2);

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$test) . ($footer_starts_from+1);
            $this->excel->getActiveSheet()->mergeCells($mrgesel);

            $mrgesel = $this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . ($footer_starts_from+1);
            $this->excel->getActiveSheet()->mergeCells($mrgesel);

            $footer_starts_from=$footer_starts_from+2;

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$test) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from,'Accountant / Cashier');

            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'a9aaae')
                    )
                )
            );

            $mrgesel = $this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+0+$last_column-1) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from,'Branch Head');

            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'a9aaae')
                    )
                )
            );

            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);

            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('DAILY REVENUE COLLECTION');
            $filename = "DailyRevenue_".date('Ymd_His') . '.xls'; //save our workbook as this file name
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

            //force user to download the Excel file without writing it to server's HD
            $file_path = './uploads/' . $filename;
            $objWriter->save($file_path);
            $file_path = base_url().'uploads/' . $filename;
            $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
        }
        $this->set_response($data);
    }

    public function uploadBankReceipt_post()
    {
        $config['upload_path'] = './uploads/bank_receipt/';
        $upload_file=$_FILES['file']['name'];
        $file_split=explode('.',$upload_file);
        $file_ext = $file_split[count($file_split)-1];
        $fileName = 'dr_'.date('U');
        $config['file_name']= $fileName;
        $config['allowed_types'] = '*';
        $config['max_size']	= '0';
        $config['remove_spaces']= TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file'))
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => $this->upload->display_errors('',''),
                'data' => array(),
            ];
        }
        else
        {
            $data=$this->upload->data();
            $file_path = base_url().'uploads/bank_receipt/' . $fileName.'.'.$file_ext;
            $final_response['response']=[
                'status' => TRUE,
                'message' => 'success',
                'data' => array('template_path'=>$file_path,'fileName'=>'uploads/bank_receipt/'.$data['file_name']),
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}