<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/*
 * Created By: Abhilash 
 * Purpose: Services for Faculty.
 * 
 * Updated on - 20 May 2016
 */

class Faculty extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("Faculty_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->helper('encryption');
        $this->load->library('SSP');
    }
    
    function getFacultyDatatables_get(){
        $this->response($this->Faculty_model->getFacultyDatatables(), REST_Controller::HTTP_OK);
    }
    /**
     * Get All Faculty list
     */
    function getAllFaculty_get() {
        $id = $this->get('subject_id');

        $status = true;
        $message = "";
        $result = [];

        $list = $this->Faculty_model->getAllFaculty();
        if ($list == 'badrequest') {
            $message = 'No data found';
            $status = false;
        } else if ($list == 'nosubject') {
            $message = 'No subject found';
            $status = false;
        } else if (isset($list['error']) && $list['error'] == 'dberror') {
            $message = 'Database error';
            $status = false;
            $result = $list['msg'];
        } else if ($list) {
            $message = "Faculty data";
            $status = true;
            $result = $list;
        } else {
            /* No data found */
            $message = 'No data found';
            $status = false;
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function getFacultyById_get() {
        $id = $this->get('faculty_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $list = $this->Faculty_model->getFacultyById($id);
            if ($list == 'badrequest') {
                $message = 'No data found';
                $status = false;
            } else if (isset($list['error']) && $list['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $result = $list['msg'];
            } else if ($list) {
                $message = "Faculty data";
                $status = true;
                $result = $list;
            } else {
                /* No data found */
                $message = 'No data found';
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function saveFaculty_post() {
        $status = true;
        $message = "";
        $result = "";
        $config = array(
            array(
                'field' => 'facultyName',
                'label' => 'Faculty Name',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'subject',
                'label' => 'Subject',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Required',
                ),
            ),
            array(
                'field' => 'comment',
                'label' => 'Comment',
                'rules' => 'regex_match[/^[a-zA-Z0-9 ,.]+$/]',
                'errors' => array(
                    'regex_match' => 'Invalid %s',
                ),
            )
        );

        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $data = $this->input->post();
            $isAdded = $this->Faculty_model->saveFaculty($data);
            if (isset($isAdded['error']) && $isAdded['error'] == 'dberror')
            {
                $message = 'Database error';
                $status = false;
                $results = $isAdded['msg'];
            }
            else if ($isAdded == 1)
            {
                $result = [];
                $message = "Saved successfully";
                $status = true;
            }
            else if ($isAdded === 'nosubject')
            {
                $result = array('subject' => 'Subject is invalid');
                $message = "Subject is invalid";
                $status = false;
            }
            else if ($isAdded === 'duplicate')
            {
                $result = array('facultyName' => 'Name already exists');
                $message = "Name already exists";
                $status = false;
            }
            else
            {
                $result = [];
                $message = "Something went wrong";
                $status = false;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

    function editFaculty_post() {
        $id = $this->get('faculty_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            
            $config = array(
                array(
                    'field' => 'facultyName',
                    'label' => 'Faculty Name',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'subject',
                    'label' => 'Subject',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Required',
                    ),
                ),
                array(
                    'field' => 'comment',
                    'label' => 'Comment',
                    'rules' => 'regex_match[/^[a-zA-Z0-9 ,.]+$/]',
                    'errors' => array(
                        'regex_match' => 'Invalid %s',
                    ),
                )
            );

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message = "Validation Error";
                $status = false;
                $result = $this->form_validation->error_array();
            } else {
                $data = $this->input->post();
                $isEdited = $this->Faculty_model->editFaculty($id, $data);
                if (isset($isEdited['error']) && $isEdited['error'] == 'dberror')
                {
                    $message = 'Database error';
                    $status = false;
                    $results = $isEdited['msg'];
                }
                else if ($isEdited == 1)
                {
                    $result = [];
                    $message = "Saved successfully";
                    $status = true;
                }
                else if ($isEdited === 'nosubject')
                {
                    $result = array('subject' => 'Subject is invalid');
                    $message = "Subject is invalid";
                    $status = false;
                }
                else if ($isEdited === 'duplicate')
                {
                    $result = array('facultyName' => 'Name already exists');
                    $message = "Name already exists";
                    $status = false;
                }
                else
                {
                    $result = [];
                    $message = "Something went wrong";
                    $status = false;
                }
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function deleteFaculty_post() {
        $id = $this->get('faculty_id');

        $status = true;
        $message = "";
        $result = "";

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $isDeleted = $this->Faculty_model->deleteFaculty($id);
            if (isset($isDeleted['error']) && $isDeleted['error'] == 'dberror') {
                $message = 'Database error';
                $status = false;
                $results = $isDeleted['msg'];
            } else if ($isDeleted === 'nodata') {
                $message = "No subject found";
                $status = false;
            } else if ($isDeleted === 'active') {
                $message = "Faculty Active";
                $status = true;
            } else if ($isDeleted === 'deactivated') {
                $message = "Faculty Inactive";
                $status = true;
            }
        }
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }

}
