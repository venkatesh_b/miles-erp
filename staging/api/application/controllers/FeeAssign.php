<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 21/3/16
 * Time: 9:44 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class FeeAssign extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('FeeAssign_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){

        $this->form_validation->set_rules($data);
        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0){
                return $this->form_validation->error_array();
            }
            else{
                return true;
            }
        }
        else{
            return true;
        }

    }
    function getFeeAssignList_get()
    {
        $branchId=$this->get('branchID');
        $this->response($this->FeeAssign_model->getFeeAssignList($branchId));
    }
    function getFeeAssignModificationsList_get(){
        $branchId=$this->get('branchID');
        $this->response($this->FeeAssign_model->getFeeAssignModificationsList($branchId));

    }

    function getLeadInfoByLeadNumber_post(){
        $lead_number=$this->post('LeadNumber');
        $courseId=$this->post('courseId');
        $this->form_validation->set_data(array("LeadNumber" => $lead_number));
        $validations[]=["field"=>"LeadNumber","label"=>'LeadNumber',"rules"=>"required|integer","errors"=>array(
            'required' => 'should not be blank','integer' => 'should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        $branch='';
        if(!empty($this->post('branch'))){
            $branch=$this->post('branch');
        }
        if($validationstatus===true)
        {
           $lead_data=$this->FeeAssign_model->getFeeDetailstoLeadNumber($lead_number,$branch,$courseId);
           if(isset($lead_data['data']))
           {
               if(isset($lead_data['data'][0]->candidate_fee_id))
               {
                   $structure_data = $this->FeeAssign_model->getFeeDetailsFromCandidateItem($lead_data['data'][0]->candidate_fee_id);
                   $lead_data['data'][0]->ammounts = $structure_data['data'];
               }
            }
            $final_response['response']=[
                'status' => $lead_data['status'],
                'message' => $lead_data['message'],
                'data' => $lead_data['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function AddCandidate_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $feeStructureId = $this->post('FeeStructureId');
        $branchXrefLead = $this->post('brachxcerflead');
        $courseId = null;
        $pageUrl = null;
        if($this->post('courseId'))
        {
            $courseId = $this->post('courseId');
        }
        if($this->post('pageURL'))
        {
            $pageUrl = $this->post('pageURL');
        }
        $UserID=$this->post('userID');
        $validations[]=["field"=>"brachxcerflead","label"=>'brachxcerflead',"rules"=>"required","errors"=>array('required' => 'should not be blank ')];
        $validations[]=["field"=>"FeeStructureId","label"=>'FeeStructureId',"rules"=>"required","errors"=>array('required' => 'should not be blank ')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $data=array(
                "fk_fee_structure_id"=>$feeStructureId,
                "fk_branch_xref_lead_id"=>$branchXrefLead,
                "pageUrl"=>$pageUrl,
                "courseId"=>$courseId,
                "assigned_on"=>date("Y-m-d H:i:s"),
                "assigned_by"=>$UserID
            );
            $getType=$this->FeeAssign_model->saveCandidate($data);
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function getFeeStructureByLead_post(){
        $branch=$this->post('branch');
        $course=$this->post("course");
        $userId=$this->post("UserID");
        $feetype = $this->post("feeType");
        $corporate=$this->post("corporate");
        $groupvalue=$this->post("groupvalue");
        $fk_training_type=$this->post("trainingType");
        if(isset($feetype) && $feetype!=null && !empty($feetype)){
            $feetype=$this->post("feeType");
        }else{
            $feetype=null;
        }
        if(isset($corporate) && $corporate != null && !empty($corporate)){

            $corporate=$this->post("corporate");
        }else{
            $corporate=null;
        }
        if(isset($groupvalue) && $groupvalue!=null && !empty($groupvalue)){
            $groupvalue=$this->post("groupvalue");
        }else{

            $groupvalue=null;
        }

        $this->form_validation->set_data(array("branch" => $branch));
        $validations[]=["field"=>"branch","label"=>'branch',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $data=array("fk_branch_id"=>$branch,"fk_type_id"=>$feetype,"fk_institution_id"=>$groupvalue,"fk_corporate_company_id"=>$corporate,"fk_course_id"=>$course,
                "fk_training_type"=>$fk_training_type);
            $fee_structure=$this->FeeAssign_model->getFeeStructureInfo($data);

            if(isset($fee_structure['data']))
            {
                if(isset($fee_structure['data'][0]->fee_structure_id))
                {
                    $structure_data = $this->FeeAssign_model->getFeeItems($fee_structure['data'][0]->fee_structure_id);
                    if(!empty($structure_data['data']))
                    {
                        $fee_structure['data'][0]->ammounts = $structure_data['data'];
                    }
                    else
                    {
                        $fee_structure['data'][0]->ammounts = array();
                    }
                }
            }
            $final_response['response']=[
                'status' => $fee_structure['status'],
                'message' => $fee_structure['message'],
                'data' => $fee_structure['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getFeeAssignByCandidateItem_get(){

        $this->form_validation->set_data(array("candidate_fee_item_id" => $this->get('candidate_fee_item_id')));
        $validations[]=["field"=>"candidate_fee_item_id","label"=>'candidate_fee_item_id',"rules"=>"required","errors"=>array(
            'required' => 'Candidate fee item should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $params['candidate_fee_item_id'] = $this->get('candidate_fee_item_id');

            $res = $this->FeeAssign_model->getFeeAssignInfo($params);
            $final_response['response'] = [
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function modifyFeeAssign_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $validations[]=["field"=>"feeModifications","label"=>'feeModifications',"rules"=>"required","errors"=>array('required' => 'Candidate fee item should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $params['feeModifications'] = $this->post('feeModifications');
            $params['candidate_fee_id']=$this->post('candidate_fee_id');
            $params['type']=$this->post('type');
            $res = $this->FeeAssign_model->saveModifyFee($params);
            $final_response['response'] = [
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }

    function getFeeAssignInfoByLeadNumber_get()
    {
        $this->form_validation->set_data(array("LeadNumber" => $this->get('LeadNumber')));
        $validations[]=["field"=>"LeadNumber","label"=>'LeadNumber',"rules"=>"required","errors"=>array(
            'required' => 'Lead number is required')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $params['LeadNumber'] = $this->get('LeadNumber');
            $res = $this->FeeAssign_model->getFeeAssignInfoByLeadNumber($params);
            $final_response['response'] = [
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function getFeeAssignInfoByCandidateFeeId_get()
    {
        $candidateFeeId = $this->get('candidateFeeId');
        $res = $this->FeeAssign_model->getFeeAssignInfoByCandidateFeeId($candidateFeeId);
        $final_response['response'] = [
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function getFeeConcessionList_get()
    {
        $branchId=$this->get('branchID');
        $this->response($result=$this->FeeAssign_model->getFeeConcessionList($branchId));
    }

    function getFeeConcessionRequestList_get()
    {
        $branchId=$this->get('branchID');
        $this->response($result=$this->FeeAssign_model->getFeeConcessionRequestList($branchId));
    }

    function getFeeRequestList_get()
    {
        $this->response($result=$this->FeeAssign_model->getLeadFeeRequestList());
    }
    function UpdateCandidateFeeConcession_post()
    {
        $feeConcessionData = $this->post('feeConcessionData');
        $candidateFeeId = $this->post('candidateFeeId');
        $candidateFeeIds = $this->post('CandidateFeeIds');
        /*$validateFeeConcession=$this->FeeAssign_model->checkForCandidateFeeConcession($candidateFeeIds);
        if($validateFeeConcession > 0)
        {
            $final_response=[
                'status' => FALSE,
                'message' => 'Concession cannot be applied for this lead',
                'data' => NULL,
            ];
        }
        else*/
        {
            $final_response=$this->FeeAssign_model->updateCandidateFeeConcession($candidateFeeId,$feeConcessionData);
        }
        $this->response($final_response);
    }
    function ApproveCandidateFeeConcession_post()
    {
        $feeConcessionData = $this->post('feeConcessionData');
        $candidateFeeId = $this->post('candidateFeeId');
        $feeConcessionStatus=$this->FeeAssign_model->checkForCandidateFeeConcessionStatus($candidateFeeId);
        if($feeConcessionStatus[0]->approved_status == 'Success')
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Concession already applied for this lead',
                'data' => NULL,
            ];
        }
        elseif($feeConcessionStatus[0]->approved_status == 'Rejected')
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Concession already rejected for this lead',
                'data' => NULL,
            ];
        }
        elseif($feeConcessionStatus[0]->approved_status == 'Pending')
        {
            $final_response['response']=$this->FeeAssign_model->approveCandidateFeeConcession($candidateFeeId,$feeConcessionData);
        }
        $this->response($final_response['response']);
    }
    function RejectCandidateFeeConcessionRequest_get()
    {
        $candidateFeeId = $this->get('candidateFeeId');
        $requestType = $this->get('requestType');
        $feeConcessionStatus=$this->FeeAssign_model->checkForCandidateFeeConcessionStatus($candidateFeeId);
        if($feeConcessionStatus[0]->approved_status == 'Success')
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Concession already applied for this lead',
                'data' => NULL,
            ];
        }
        elseif($feeConcessionStatus[0]->approved_status == 'Rejected')
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Concession already rejected for this lead',
                'data' => NULL,
            ];
        }
        elseif($feeConcessionStatus[0]->approved_status == 'Pending')
        {
            $final_response['response']=$this->FeeAssign_model->rejectCandidateFeeConcessionRequest($candidateFeeId,$requestType);
        }
        $this->response($final_response['response']);
    }
    function RejectCandidateFeeModificationRequest_get()
    {
        $candidateFeeId = $this->get('candidateFeeId');
        $requestType = $this->get('requestType');
        $feeModificationStatus=$this->FeeAssign_model->checkForCandidateFeeModificationStatus($candidateFeeId);
        if($feeModificationStatus[0]->approved_status == 'Success')
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Fee Modification already applied for this lead',
                'data' => NULL,
            ];
        }
        elseif($feeModificationStatus[0]->approved_status == 'Rejected')
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Fee Modification already rejected for this lead',
                'data' => NULL,
            ];
        }
        elseif($feeModificationStatus[0]->approved_status == 'Pending')
        {
            $final_response['response']=$this->FeeAssign_model->rejectCandidateFeeModificationRequest($candidateFeeId,$requestType);
        }
        $this->response($final_response['response']);
    }


    function getLeadsInformation_get()
    {
        $branchId = $this->get('branchId');
        $searchVal=$this->get('searchVal');
        $this->response($result=$this->FeeAssign_model->getLeadsInformation($searchVal,$branchId));
    }

    function getBranchVisitLeadsInformation_get()
    {
        $branchId = $this->get('branchId');
        $searchVal=$this->get('searchVal');
        $type=$this->get('type');
        $this->response($result=$this->FeeAssign_model->getBranchVisitLeadsInformation($searchVal,$branchId,$type));
    }
}