<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 28/3/16
 * Time: 2:02 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class FeeCollection extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("FeeCollection_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
        $this->load->library('SendEmail');
    }

    function feeCollectionList_get()
    {
        $branchId=$this->get('branchID');
        $this->response($this->FeeCollection_model->feeCollectionList($branchId));
    }
    function getBranchLeadInfoByLeadNumber_get()
    {
        $branchId=$this->get('branchID');
        $leadNumber=$this->get('leadNumber');
        $leadCourseId=$this->get('leadCourseId');
        $branchLeadInfoCheck=$this->FeeCollection_model->checkBranchLeadInfo($branchId,$leadNumber);
        $feeCollectionCheck=$this->FeeCollection_model->checkLeadFeeCollection($branchId,$leadNumber);
        if($branchLeadInfoCheck == 0)
        {
            $final_response=[
                'status' => false,
                'message' => 'Lead Number not exists',
                'data' => null,
            ];
        }
        elseif($feeCollectionCheck == 0)
        {
            $final_response=[
                'status' => false,
                'message' => 'Fee not assigned to this Lead',
                'data' => null,
            ];
        }
        else
        {
            $final_response=$this->FeeCollection_model->getLeadFeeInfoByBranchIdLeadNumber($branchId,$leadNumber,$leadCourseId);
        }
        $this->response($final_response);
    }
    function addLeadFeeCollection_post()
    {
        $feeCollect_data = [];
        foreach ($this->input->post() as $key => $value)
        {
            $feeCollect_data[$key]=$value;
        }
        if($feeCollect_data['feeCollectionType']==1) {
            $addLeadFeeCollectionInfo = $this->FeeCollection_model->addLeadFeeCollectionItem($feeCollect_data);
        }
        if($feeCollect_data['feeCollectionType']==2) {
            $addLeadFeeCollectionInfo = $this->FeeCollection_model->addLeadFeeCollectionUnassignedFeeItem($feeCollect_data);
        }
        if($addLeadFeeCollectionInfo['status'] == true)
        {
            /*Mail function starts*/
            $email = 'bh.hyd.miles@gmail.com';
            $key = 'FEERECEIPT';
            $feeDetails = '<table cellspacing="0" cellpadding="2" border="1" class="feeDetails mt20" data-genre="null" style="width:100% !important;margin-top:20px;border-collapse: collapse;">
                        <thead>
                            <tr style="font-size: 14px;">
                            <td class="font-bold" data-genre="nullType" style="font-weight:600;padding:5px;">Feehead</td>
                            <td class="font-bold" data-genre="nullInstallment" data-content="installment-header" style="font-weight:600;padding:5px;">Installment</td>
                            <td class="font-bold" width="150" align="right" data-genre="nullTotal" style="font-weight:600;padding:5px;">Total Amount(Rs.)</td>
                          </tr>
                          </thead>
                        <tbody>
                            <tr style="font-size: 14px;">
                            <td data-genre="nullType" style="padding:5px;">'.$addLeadFeeCollectionInfo['data'][0]->feeHead.'</td>
                            <td data-genre="nullInstallment" data-content="installment" style="padding:5px;">1</td>
                            <td align="right" data-genre="nullTotal" style="padding:5px;">'.$addLeadFeeCollectionInfo['data'][0]->amount_paid.'</td>
                          </tr>
                          </tbody>
                      </table>
                        <table cellspacing="0" cellpadding="4" border="0" class="total" style="width:100% !important;">
                        <tbody>
                            <tr style="font-size: 14px;">
                            <td colspan="6">Rupees '.ucfirst($this->convert_number_to_words($addLeadFeeCollectionInfo['data'][0]->amount_paid)).'</td>
                            <td class="font-bold" width="150" align="right" style="text-align: right;font-weight:bold;">Rs. '.$addLeadFeeCollectionInfo['data'][0]->amount_paid.'</td>
                          </tr>
                            <tr></tr>
                          </tbody>
                      </table>';
            $studentData = array(
                'leadName' => $addLeadFeeCollectionInfo['data'][0]->studentName,
                'leadNumber' => $addLeadFeeCollectionInfo['data'][0]->lead_number,
                'courseName' => $addLeadFeeCollectionInfo['data'][0]->courseName,
                'branchName' => $addLeadFeeCollectionInfo['data'][0]->branchName,
                'batchInfo' => $addLeadFeeCollectionInfo['data'][0]->batchInfo,
                'receiptNumber' => $addLeadFeeCollectionInfo['data'][0]->receipt_number,
                'paymentType' => $addLeadFeeCollectionInfo['data'][0]->payment_type,
                'paymentMode' => $addLeadFeeCollectionInfo['data'][0]->payment_mode,
                'feeHead' => $addLeadFeeCollectionInfo['data'][0]->feeHead,
                'companyName' => $addLeadFeeCollectionInfo['data'][0]->companyName,
                'cityName' => $addLeadFeeCollectionInfo['data'][0]->cityName,
                'amountPaid' => $addLeadFeeCollectionInfo['data'][0]->amount_paid,
                'paymentStatus' => $addLeadFeeCollectionInfo['data'][0]->payment_status,
                'receiptDate' => $addLeadFeeCollectionInfo['data'][0]->receipt_date,
                'receiptDateTime' => $addLeadFeeCollectionInfo['data'][0]->receipt_date_time,
                'feeDetails' => $feeDetails,
                'BaseURL' => str_replace('/api', '/application', BASE_URL).'assets/images' /*BASE_URL.'uploads'*/
            );
            $this->load->model("EmailTemplates_model");
            $mailTemplate = $this->EmailTemplates_model->getTemplate($key,$studentData);
            if(is_array($mailTemplate) && isset($mailTemplate['error']) && $mailTemplate['error'] == 'dberror'){
                //db error
            } else if(!is_array($mailTemplate) && strlen($mailTemplate) == 'nodata'){
                //no template found
            }else{
                $sendemail = new sendemail;
                $res = $sendemail->sendMail($email, $mailTemplate['title'], $mailTemplate['content'], $mailTemplate['attachment']);
            }
            /*Mail function ends*/
        }

        $this->response($addLeadFeeCollectionInfo);
    }
    function FeeCollectionDetails_get()
    {
        $feeCollectionId=$this->get('feeCollectionID');
        $FeeCollectionInfo=$this->FeeCollection_model->feeCollectionDetails($feeCollectionId);
        $this->response($FeeCollectionInfo);
    }

    function cancelFeeCollection_get()
    {
        $feeCollectionId=$this->get('feeCollectionID');
        $userId=$this->get('userID');
        //check fee collection status is active or inactive
        $FeeCollectionInfo=$this->FeeCollection_model->feeCollectionDetails($feeCollectionId);
        //'Pending','Success','Cancelled','Reversed'
        if($FeeCollectionInfo['data'][0]->payment_status=='Success')
        {
            $FeeCollectionInfo=$this->FeeCollection_model->cancelFeeCollectionReceipt($feeCollectionId,$userId);
            $this->response($FeeCollectionInfo);
        }
        else if($FeeCollectionInfo['data'][0]->payment_status=='Pending')
        {
            $db_response=array("status"=>false,"message"=>'This receipt cannot be cancelled. It is in pending',"data"=>array());
            $this->response($db_response);
        }
        else if($FeeCollectionInfo['data'][0]->payment_status=='Reversed')
        {
            $db_response=array("status"=>false,"message"=>'This receipt cannot be cancelled. It is already reversed',"data"=>array());
            $this->response($db_response);
        }
        else if($FeeCollectionInfo['data'][0]->payment_status=='Cancelled')
        {
            $db_response=array("status"=>false,"message"=>'This receipt already cancelled',"data"=>array());
            $this->response($db_response);
        }
        else
        {
            $db_response=array("status"=>false,"message"=>'This receipt cannot be cancelled',"data"=>array());
            $this->response($db_response);
        }
	}
    function convert_number_to_words($number) {

        $hyphen      = ' ';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $this->convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }
    function getLeadFeeInfoDetailsUnassignedFee_get()
    {
        $branchId=$this->get('branchID');
        $leadNumber=$this->get('leadNumber');
        $leadCourseId=$this->get('leadCourseId');
        $branchLeadInfoCheck=$this->FeeCollection_model->checkBranchLeadInfo($branchId,$leadNumber);
        if($branchLeadInfoCheck == 0)
        {
            $final_response=[
                'status' => false,
                'message' => 'Lead Number not exists',
                'data' => null,
            ];
        }
        else
        {
            $final_response=$this->FeeCollection_model->getLeadFeeInfoDetailsUnassignedFee($branchId,$leadNumber,$leadCourseId);
        }
        $this->response($final_response);
    }
    function getBranchLeadInfoByLeadNumberUnassignedFee_get()
    {
        $branchId=$this->get('branchID');
        $leadNumber=$this->get('leadNumber');
        $feeStructureType=$this->get('feeStructureType');

        $branchLeadInfoCheck=$this->FeeCollection_model->checkBranchLeadInfo($branchId,$leadNumber);
        if($branchLeadInfoCheck == 0)
        {
            $final_response=[
                'status' => false,
                'message' => 'Lead Number not exists',
                'data' => null,
            ];
        }
        else
        {
            $final_response=$this->FeeCollection_model->getLeadFeeInfoByBranchIdLeadNumberUnassignedFee($branchId,$leadNumber,$feeStructureType);
        }
        $this->response($final_response);
    }

}