<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: V Parameshwar. Date: 04-02-2016
 * Purpose: Services for Companies.
 * 
 */

class FeeHead extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('FeeHead_model');
        $this->load->model('References_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){

    $this->form_validation->set_rules($data);

    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }

    }
    function AddFeeHead_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
            if($postdata){ $_POST = $postdata; }
            $this->form_validation->set_data($this->post());

        $companyName=trim($this->post('companyName'));
        $description=trim($this->post('description'));
        $name=$this->post('name');
        $status=intval($this->post('status'));
        $UserID=$this->post('UserID');

        $validations[]=["field"=>"companyName","label"=>'companyName',"rules"=>"required|regex_match[/^[0-9 ]+$/]","errors"=>array('required' => ' Company Name should not be blank','regex_match' => 'company Id should be integer  only')];
        $validations[]=["field"=>"name","label"=>'name',"rules"=>"required","errors"=>array('required' => ' Name should not be blank')];
        //$validations[]=["field"=>"description","label"=>'description',"rules"=>"required","errors"=>array('required' => ' Code should not be blank')];
        $validations[]=["field"=>"status","label"=> 'status',"rules"=>"required|integer|regex_match[/^[0-1]$/]",
            "errors"=>array('required' => 'Status should not be blank','integer' => ' Status should be integer value only','regex_match' => 'Status should be either 0 or 1')];
               //$validations[]=["field"=>"roleDescription","label"=> 'Role Description ',"rules"=>"required","errors"=>array('required' => 'Role Description should not be blank')];

            $validationstatus=$this->customvalidation($validations);
              if($validationstatus===true){
                $data=array("fk_company_id"=>$companyName,'description'=>$description,"name"=>$name,
                    'fk_created_by'=>$UserID,"created_date"=>date('Y-m-d h:i:s'),'status'=>(int)$status
                );
                  $getType=$this->FeeHead_model->saveFeeHead($data);

                $final_response['response']=[
                    'status' => $getType['status'],
                    'message' => $getType['message'],
                    'data' => $getType['data'],
                ];

            }
            else{

                $final_response['response']=[
                    'status' => FALSE,
                    'message' => 'validation failed',
                    'data' => $validationstatus,
                ];
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function getSingleFeeHead_get(){
        $FeeHead=$this->get('FeeHead');
        // $company_id=decode($company_id);
        $this->form_validation->set_data(array("FeeHead" => $FeeHead));

        $validations[]=["field"=>"FeeHead","label"=>'FeeHead',"rules"=>"required|integer","errors"=>array(
            'required' => 'FeeHead Id should not be blank','integer' => 'FeeHead Id should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){

            $FeeHead_data=$this->FeeHead_model->getSingleFeeHeadDetails($FeeHead);

            $final_response['response']=[
                'status' => $FeeHead_data['status'],
                'message' => $FeeHead_data['message'],
                'data' => $FeeHead_data['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function updateFeeHead_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $companyName=trim($this->post('companyName'));
        $description=trim($this->post('description'));
        $name=$this->post('name');
        $status=intval($this->post('status'));
        $UserID=$this->post('UserID');
        $companyID=$this->post('CompanyID');

        $validations[]=["field"=>"companyName","label"=>'Name',"rules"=>"required|regex_match[/^[0-9 ]+$/]","errors"=>array('required' => ' Company Name should not be blank','regex_match' => 'company id should be integer  only')];
        $validations[]=["field"=>"name","label"=>'name',"rules"=>"required|regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => ' Name should not be blank','regex_match' => 'Name should be alphabets only')];
        //$validations[]=["field"=>"code","label"=>'code',"rules"=>"required|regex_match[/^[0-9 ]+$/]","errors"=>array('required' => ' Code should not be blank','regex_match' => 'Code should be numeric only')];
        $validations[]=["field"=>"CompanyID","label"=>'CompanyID',"rules"=>"required|regex_match[/^[0-9 ]+$/]","errors"=>array('required' => 'Bank details id required','regex_match' => 'id should be integer')];
        $validations[]=["field"=>"status","label"=> 'status',"rules"=>"required|integer|regex_match[/^[0-1]$/]",
            "errors"=>array('required' => 'FeeHead Status should not be blank','integer' => 'FeeHead Status should be integer value only','regex_match' => 'FeeHead Status should be either 0 or 1')];

        //$validations[]=["field"=>"roleDescription","label"=> 'Role Description ',"rules"=>"required","errors"=>array('required' => 'Role Description should not be blank')];


        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array("fk_company_id"=>$companyName,'description'=>$description,"name"=>$name,
                'fk_created_by'=>$UserID,"created_date"=>date('Y-m-d h:i:s'),'status'=>(int)$status

            );
            $getType=$this->FeeHead_model->updateFeeHead($data,$companyID);
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function getAllCompanies_get(){
        $result=$this->CompanyAccountDetails_model->getAllCompanies();
       if(isset($result) && !empty($result)){
           $data=array("status"=>true,"message"=>"","data"=>$result);

       }else{
           $data=array("status"=>false,"message"=>"There are no companies","data"=>"");

       }

           $this->set_response($data);
    }
    function getListOfFeeHead_get(){

         $this->response($this->FeeHead_model->getFeeHeadDetails());
    }
    function deleteFeeHead_get(){

        $FeeheadId=$this->get('feeHeadId');

        $this->form_validation->set_data(array("feeHeadId" => $this->get('feeHeadId')));

        $validations[]=["field"=>"feeHeadId","label"=>'feeHeadId',"rules"=>"required|integer","errors"=>array(
            'required' => 'feeHead ID should not be blank','integer' => 'feeHead ID should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
           // $resp_checkTag=$this->Company_model->checkCompany($companyId);
                $res_delete=$this->FeeHead_model->deleteFeedHead($FeeheadId);
                $final_response['response']=[
                    'status' => $res_delete['status'],
                    'message' => $res_delete['message'],
                    'data' => $res_delete['data'],
                ];
                if($res_delete['status']===true){
                    //sucess
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    //fail
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }

        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    /**
     * Created by : Abhilash
     */
    function getSingleFeeHeadDetails_get(){
        $FeeHead=$this->get('FeeHead');
        // $company_id=decode($company_id);
        $this->form_validation->set_data(array("FeeHead" => $FeeHead));

        $validations[]=["field"=>"FeeHead","label"=>'FeeHead',"rules"=>"required","errors"=>array(
            'required' => 'FeeHead Id should not be blank'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){

            $FeeHead_data=$this->FeeHead_model->getSingleFeeHeadAllDetails($FeeHead);

            $final_response['response']=[
                'status' => $FeeHead_data['status'],
                'message' => $FeeHead_data['message'],
                'data' => $FeeHead_data['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    /**
     * Created by : Abhilash
     */
    function getFeeHeadList_get(){
        $FeeHead_data=$this->FeeHead_model->getFeeHeadList();

        $final_response['response']=[
            'status' => $FeeHead_data['status'],
            'message' => $FeeHead_data['message'],
            'data' => $FeeHead_data['data'],
        ];
        
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}