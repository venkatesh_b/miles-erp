<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Fee Structure
 * @category        Controller
 * @author          Abhilash
 */

class FeeStructure extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('FeeStructure_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    
    function getFeeStructure_get() {
        $list = $this->FeeStructure_model->getAllFeeStructure();
        $status=true;$message="";$result="";
        
        if ($list == 'badrequest')
        {
            $message='There are no Fee Structure';
            $status=false;
        }
        else if (isset($list['error']) && $list['error'] == 'dberror') 
        {
            $message='Database error';
            $status=false;
            $result = $list['msg'];
        }
        else if ($list) 
        {
            $message="Fee Structure list";
            $status=true;
            $result=array_values($list);
        } 
        else 
        {   
            //No data found
            $message='There are no Fee Structure';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->response($data);
    }
    
    function getFeeStructureList_get() {
        $name = $this->get('time');
        $this->form_validation->set_data(array('name' => $name));
        $this->form_validation->set_rules('name', "time", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
            $this->response(array("status"=>$status,"message"=>$message,"data"=>$result));
        } else {
            $this->response($this->FeeStructure_model->feeStructuresListDataTables($name),REST_Controller::HTTP_OK);
            exit;
        }
    }
    
    function getFeeStructureHistory_get(){
        $id = $this->get('fee_id');
        $this->form_validation->set_data(array('fee_id' => $id));
        $this->form_validation->set_rules('fee_id', "Id", 'required');

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } else {
            $list = $this->FeeStructure_model->getHistory($id);
            $status=true;$message="";$result="";

            if ($list == 'badrequest')
            {
                $message='There are no Fee Structure';
                $status=false;
            }
            elseif (isset($list['error']) && $list['error'] == 'dberror') 
            {
                $message='Database error';
                $status=false;
                $result = $list['msg'];
            }
            elseif ($list) 
            {
                $message="Fee Structure history list";
                $status=true;
                $result=array_values($list);
            } 
            else 
            {   
                /*No data found*/
                $message='There are no Fee Structure';
                $status=false;
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
       
    function getSingleFeeStructure_get(){
        $status=true;$message="";$result="";

        $id = $this->get('fee_id');
        
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required|numeric');

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        }
        else
        {
            $list = $this->FeeStructure_model->getSingleFeeStructure($id);
            if ($list == 'badrequest')
            {
                $message='There are no Fee Structure';
                $status=false;
            }
            else if (isset($list['error']) && $list['error'] == 'dberror')
            {
                $message='Database error';
                $status=false;
                $result = $list['msg'];
            }
            else if ($list['status'] == true)
            {
                $message=$list['msg'];
                $status=true;
                $result = $list['data'];
            }
            else 
            {   
                /*No data found*/
                $message='There are no Fee Structure';
                $status=false;
            }
            $data=array("status"=>$status,"message"=>$message,"data"=>$result);
            $this->response($data);
        }
    }

    function addFeeStructure_post() {
        $status = true;
        $message = "";
        $result = "";

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|callback_name_check'
            ),
            array(
                'field' => 'effective',
                'label' => 'Effective',
                'rules' => 'required'
            ),
            array(
                'field' => 'decription',
                'label' => 'Description',
                'rules' => 'regex_match[/[a-zA-Z0-9. ]+$/]',
                'errors' => array(
                    'regex_match' => 'Invalid %s, may contain alphabets, numbers , dots ' . ', spaces',
                ),
            ),
            array(
                'field' => 'feetype',
                'label' => 'feetype',
                'rules' => 'required|regex_match[/^(?i)(group|corporate|retail)$/]',
                'errors' => array(
                    'regex_match' => 'Invalid %s',
                    'required' => '%s required'
                ),
            )
        );

        /**/
        $data = json_decode(file_get_contents("php://input"), true);
        if ($data) {
            $_POST = $data;
        }

        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message = "Validation Error";
            $status = false;
            $result = $this->form_validation->error_array();
        } else {
            $error = false;
            $data = $this->input->post();
            if($data['feetype']=='group'){
                if(strlen($data['groupname']) <= 0){
                    $error = true;
                    $message = 'Institute can\'t be blank';
                    $status = false;
                    $result = [];
                    $data = array("status" => $status, "message" => $message, "data" => $result);
                    $this->response($data);
                }
            }else if($data['feetype']=='corporate'){
                if(strlen($data['companyName']) <= 0){
                    $error = true;
                    $message = 'Company can\'t be blank';
                    $status = false;
                    $result = [];
                    $data = array("status" => $status, "message" => $message, "data" => $result);
                    $this->response($data);
                }
            }

            /* For Fee structure items */
            $fee = [];
            $days = [];
            if(isset($data['item'])){
                foreach ($data['item']['noOfDays'] as $key => $value) {
                    if(strlen(trim($value['value'])) > 0){
                        /*if (array_key_exists($value['value'], $days)) {
                            $error = true;
                            $message = 'Due Day can\'t be duplicate';
                            $status = false;
                            $data = array("status" => $status, "message" => $message, "data" => $result);
                            $this->response($data);
                        } else*/
                        {
                            foreach ($data['item']['totAmt'] as $k => $v) {
                                if(strlen(trim($v['value'])) > 0){
                                    if($key == $k)
                                    {
                                        $days[$value['value']] = $value['value'];
                                        $fee[$key] = ['id' => $value['id'],  'days' => $value['value'], 'amount' => $v['value']];
                                    }
                                }else{
                                    $error = true;
                                    $message = 'Due Amount can\'t be empty';
                                    $status = false;
                                    $data = array("status" => $status, "message" => $message, "data" => $result);
                                    $this->response($data);
                                }
                            }
                        }
                    }else{
                        $error = true;
                        $message = 'Due Day can\'t be empty';
                        $status = false;
                        $data = array("status" => $status, "message" => $message, "data" => $result);
                        $this->response($data);
                    }
                }
                $data['item'] = $fee;
            }else {
                $error = true;
                $message = 'Select Fee Head';
                $status = false;
                $result = [];
                $data = array("status" => $status, "message" => $message, "data" => $result);
                $this->response($data);
            }
            if (!$error) {
                $isAdded = $this->FeeStructure_model->addFeeStructure($data);
                if ($isAdded === 'nobranch') {
                    $result = array('branch' => 'Branch is invalid');
                    $message = "Validation Error - Branch is invalid";
                    $status = false;
                } elseif (strpos($isAdded, 'alreadyPresent')!==false) {
                    $result = [];
                    $ms=explode('-',$isAdded);
                    if($ms[1]=='')
                        $message = "Fee Structure Already Exist";
                    else
                        $message = "Fee Structure Already Exist In ".$ms[1]." Branch";
                    $status = false;
                } elseif ($isAdded === 'nofeetype') {
                    $result = [];
                    $message = "Fee Type not found";
                    $status = false;
                } elseif ($isAdded === 'current') {
                    $result = [];
                    $message = "Can't update, for the same Fee Structure.";
                    $status = false;
                } elseif ($isAdded === 'nocourse') {
                    $result = array('course' => 'Course is invalid');
                    $message = "Validation Error - Course is invalid";
                    $status = false;
                } elseif ($isAdded === 'nofeetype') {
                    $result = array('feetype' => 'Fee Type is invalid');
                    $message = "Validation Error - Fee Type is invalid";
                    $status = false;
                }elseif ($isAdded === 'nogroupname') {
                    $result = array('feetype' => 'Institute is invalid');
                    $message = "Validation Error - Fee Type is invalid";
                    $status = false;
                } elseif ($isAdded === 'nocompanyname') {
                    $result = [];
                    $message = "Validation Error - Company is invalid";
                    $status = false;
                } elseif ($isAdded) {
                    $result = [];
                    $message = "Fee Structure Added successfully";
                    $status = true;
                } else {
                    $result = [];
                    $message = "Something went wrong please try after sometime";
                    $status = false;
                }
            }
        }

        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->response($data);
    }
    
    function updateFeeStructure_post($feeId){
        $status=true;$message="";$result="";

        $id = $feeId;
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } else {
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required|callback_name_check'
                ),
                array(
                    'field' => 'effective',
                    'label' => 'Effective',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'decription',
                    'label' => 'Description',
                    'rules' => 'regex_match[/[a-zA-Z0-9. ]+$/]',
                    'errors' => array(
                        'regex_match' => 'Invalid %s, may contain alphabets, numbers , dots ' . ', spaces',
                    ),
                ),
                array(
                    'field' => 'feetype',
                    'label' => 'feetype',
                    'rules' => 'required|regex_match[/^(?i)(group|corporate|retail)$/]',
                    'errors' => array(
                        'regex_match' => 'Invalid %s',
                        'required' => '%s required'
                    ),
                )
            );

            $data = json_decode(file_get_contents("php://input"), true);
            if ($data) {
                $_POST = $data;
            }

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message = "Validation Error";
                $status = false;
                $result = $this->form_validation->error_array();
            } else {
                $error = false;
                $data = $this->input->post();
                if($data['feetype']=='group'){
                    if(strlen($data['groupname']) <= 0){
                        $error = true;
                        $message = 'Institute can\'t be blank';
                        $status = false;
                        $result = [];
                        $data = array("status" => $status, "message" => $message, "data" => $result);
                        $this->response($data);
                    }
                }else if($data['feetype']=='corporate'){
                    if(strlen($data['companyName']) <= 0){
                        $error = true;
                        $message = 'Company can\'t be blank';
                        $status = false;
                        $result = [];
                        $data = array("status" => $status, "message" => $message, "data" => $result);
                        $this->response($data);
                    }
                }

                /* For Fee structure items */
                $fee = [];
                $days = [];
                if(isset($data['item'])){
                    foreach ($data['item']['noOfDays'] as $key => $value) {
                        if(strlen(trim($value['value'])) > 0){
                            /*if (array_key_exists($value['value'], $days)) {
                                $error = true;
                                $message = 'Due Day can\'t be duplicate';
                                $status = false;
                                $data = array("status" => $status, "message" => $message, "data" => $result);
                                $this->response($data);
                            } else*/
                            {
                                foreach ($data['item']['totAmt'] as $k => $v) {
                                    if(strlen(trim($v['value'])) > 0){
                                        if($key == $k){
                                            $days[$value['value']] = $value['value'];
                                            $fee[$key] = ['id' => $value['id'],  'days' => $value['value'], 'amount' => $v['value']];
                                        }
                                    }else{
                                        $error = true;
                                        $message = 'Due Amount can\'t be empty';
                                        $status = false;
                                        $data = array("status" => $status, "message" => $message, "data" => $result);
                                        $this->response($data);
                                    }
                                }
                            }
                        }else{
                            $error = true;
                            $message = 'Due Day can\'t be empty';
                            $status = false;
                            $data = array("status" => $status, "message" => $message, "data" => $result);
                            $this->response($data);
                        }
                    }
                    $data['item'] = $fee;
                }else {
                    $error = true;
                    $message = 'Select Fee Head';
                    $status = false;
                    $result = [];
                    $data = array("status" => $status, "message" => $message, "data" => $result);
                    $this->response($data);
                }

                if (!$error) {
                    $isEdited = $this->FeeStructure_model->updateFeeStructure($data, $id);
                    if ($isEdited === 'nobranch') {
                        $result = array('branch' => 'Branch is invalid');
                        $message = "Validation Error - Branch is invalid";
                        $status = false;
                    } elseif ($isEdited === 'current') {
                        $result = [];
                        $message = "Can't update, for the same configuration.";
                        $status = false;
                    } elseif ($isEdited === 'alreadyPresent') {
                        $result = [];
                        $message = "Fee Structure exists";
                        $status = false;
                    } elseif ($isEdited === 'nodata') {
                        $result = [];
                        $message = "No Data found";
                        $status = false;
                    }elseif ($isEdited === 'nocourse') {
                        $result = array('course' => 'Course is invalid');
                        $message = "Validation Error - Course is invalid";
                        $status = false;
                    } elseif ($isEdited === 'nofeetype') {
                        $result = array('feetype' => 'Fee Type is invalid');
                        $message = "Validation Error - Fee Type is invalid";
                        $status = false;
                    }elseif ($isEdited === 'nogroupname') {
                        $result = array('feetype' => 'Institute is invalid');
                        $message = "Validation Error - Fee Type is invalid";
                        $status = false;
                    } elseif ($isEdited === 'nocompanyname') {
                        $result = [];
                        $message = "Validation Error - Company is invalid";
                        $status = false;
                    } elseif ($isEdited) {
                        $result = [];
                        $message = "Data updated successfully";
                        $status = true;
                    } else{
                        $result = [];
                        $message = "Some error occuried";
                        $status = false;
                    }
                }
            }
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    
    function copyFeeStructure_post(){
        $status=true;$message="";$result="";

        $id = $this->get('fee_id');
        
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } else {
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required|callback_name_check'
                ),
                array(
                    'field' => 'effective',
                    'label' => 'Effective',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'decription',
                    'label' => 'Description',
                    'rules' => 'regex_match[/[a-zA-Z0-9. ]+$/]',
                    'errors' => array(
                        'regex_match' => 'Invalid %s, may contain alphabets, numbers , dots ' . ', spaces',
                    ),
                ),
                array(
                    'field' => 'feetype',
                    'label' => 'feetype',
                    'rules' => 'required|regex_match[/^(?i)(group|corporate|retail)$/]',
                    'errors' => array(
                        'regex_match' => 'Invalid %s',
                        'required' => '%s required'
                    ),
                )
            );

            $data = json_decode(file_get_contents("php://input"), true);
            if ($data) {
                $_POST = $data;
            }

            $this->form_validation->set_rules($config);

            $this->form_validation->set_data($this->input->post());

            if ($this->form_validation->run() == FALSE) {
                $message = "Validation Error";
                $status = false;
                $result = $this->form_validation->error_array();
            } else {
                $error = false;
                $data = $this->input->post();


                if($data['feetype']=='group'){
                    if(strlen($data['groupname']) <= 0){
                        $error = true;
                        $message = 'Institute can\'t be blank';
                        $status = false;
                        $result = [];
                        $data = array("status" => $status, "message" => $message, "data" => $result);
                        $this->response($data);
                    }
                }else if($data['feetype']=='corporate'){
                    if(strlen($data['companyName']) <= 0){
                        $error = true;
                        $message = 'Company can\'t be blank';
                        $status = false;
                        $result = [];
                        $data = array("status" => $status, "message" => $message, "data" => $result);
                        $this->response($data);
                    }
                }

                /* For Fee structure items */
                $fee = [];
                $days = [];
                if(isset($data['item'])){
                    foreach ($data['item']['noOfDays'] as $key => $value) {
                        if(strlen(trim($value['value'])) > 0){
                            /*if (array_key_exists($value['value'], $days)) {
                                $error = true;
                                $message = 'Due Day can\'t be duplicate';
                                $status = false;
                                $data = array("status" => $status, "message" => $message, "data" => $result);
                                $this->response($data);
                            } else*/
                            {
                                foreach ($data['item']['totAmt'] as $k => $v) {
                                    if(strlen(trim($v['value'])) > 0){
                                        if($key == $k){
                                            $days[$value['value']] = $value['value'];
                                            $fee[$key] = ['id' => $value['id'],  'days' => $value['value'], 'amount' => $v['value']];
                                        }
                                    }else{
                                        $error = true;
                                        $message = 'Due Amount can\'t be empty';
                                        $status = false;
                                        $data = array("status" => $status, "message" => $message, "data" => $result);
                                        $this->response($data);
                                    }
                                }
                            }
                        }else{
                            $error = true;
                            $message = 'Due Day can\'t be empty';
                            $status = false;
                            $data = array("status" => $status, "message" => $message, "data" => $result);
                            $this->response($data);
                        }
                    }
                    $data['item'] = $fee;
                }else {
                    $error = true;
                    $message = 'Select Fee Head';
                    $status = false;
                    $result = [];
                    $data = array("status" => $status, "message" => $message, "data" => $result);
                    $this->response($data);
                }

                if (!$error) {
                    $isEdited = $this->FeeStructure_model->copyFeeStructure($data, $id);
                    if ($isEdited === 'nobranch') {
                        $result = array('branch' => 'Branch is invalid');
                        $message = "Validation Error - Branch is invalid";
                        $status = false;
                    } elseif (strpos($isEdited, 'alreadyPresent')!==false) {
                        $result = [];
                        $ms=explode('-',$isEdited);
                        if($ms[1]=='')
                            $message = "Fee Structure Already Exist";
                        else
                            $message = "Fee Structure Already Exist In ".$ms[1]." Branch";
                        $status = false;
                    }elseif ($isEdited === 'nodata') {
                        $result = [];
                        $message = "No Data found";
                        $status = false;
                    }elseif ($isEdited === 'nocourse') {
                        $result = array('course' => 'Course is invalid');
                        $message = "Validation Error - Course is invalid";
                        $status = false;
                    } elseif ($isEdited === 'nofeetype') {
                        $result = array('feetype' => 'Fee Type is invalid');
                        $message = "Validation Error - Fee Type is invalid";
                        $status = false;
                    }elseif ($isEdited === 'nogroupname') {
                        $result = array('feetype' => 'Institute is invalid');
                        $message = "Validation Error - Fee Type is invalid";
                        $status = false;
                    } elseif ($isEdited === 'nocompanyname') {
                        $result = [];
                        $message = "Validation Error - Company is invalid";
                        $status = false;
                    } elseif ($isEdited) {
                        $result = [];
                        $message = "Data updated successfully";
                        $status = true;
                    } else{
                        $result = [];
                        $message = "Some error occuried";
                        $status = false;
                    }
                }
            }
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    
    public function changeStatus_delete(){
        $status=true;$message="";$results='';
        
        $id = $this->get('fee_id');

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "id", 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        }
        else
        {
            $result = $this->FeeStructure_model->changeStatus($id);
            if (is_array($result) && $result['error'] === 'dberror')
            {
                $message="Database Error";
                $status=false;
                $results=$result['msg'];
            } 
            else if ($result === 'active')
            {
                $message="Fee Structure Activated successfully";
                $status=true;
            } 
            else if ($result === 'deactivated')
            {
                $message="Fee Structure De-Activated successfully";
                $status=true;
            }
            else if ($result === 'nodata') 
            {
                $message= 'Branch not found';
                $status=false;
            } 
            else
            {
                $message= 'Branch can\'t be deleted';
                $status=false;
            }
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }
    
    /**
     * Function for checking number(REGEX)
     * 
     * @args 
     *  "string"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Function for checking valid string
     * 
     * @args 
     *  "str"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function name_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z0-9 ])/", $str)) {
            $this->form_validation->set_message('name_check', 'Use only Alphabets and Numbers');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
