<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 21/3/16
 * Time: 9:44 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class LeadFollowUp extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('LeadFollowUp_model');
        $this->load->model('References_model');
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
        $this->load->library('SendEmail');
    }
    function customvalidation($data){

        $this->form_validation->set_rules($data);
        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0){
                return $this->form_validation->error_array();
            }
            else{
                return true;
            }
        }
        else{
            return true;
        }

    }
    function getLeadsList_get(){
        $data['branchId']=$this->get('branchId');
        $data['userID']=$this->get('userID');
        $data['type']=$this->get('type');
        $this->response($result=$this->LeadFollowUp_model->getLeadsList($data));

    }
    function getleadByBranchxcrfId_get(){
        $branchxcerLeadId=$this->get('BranchxcerLeadId');
        $this->form_validation->set_data(array("BranchxcerLeadId" => $branchxcerLeadId));
        $validations[]=["field"=>"BranchxcerLeadId","label"=>'BranchxcerLeadId',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_data=$this->LeadFollowUp_model->getSingleLeadBxrefId($branchxcerLeadId);

            if(isset($lead_data['data'][0]->city) && !empty($lead_data['data'][0]->city))
            {
                $country=$this->References_model ->getReferenceValuesList(3);

                foreach($country['data'] as $cnames){

                    if($cnames->Id1 == $lead_data['data'][0]->city_id){
                        $lead_data['data'][0]->country=$cnames->Country;
                    }
                }

            }

                if(isset($lead_data['data'][0]->created_on) && !empty($lead_data['data'][0]->created_on)){
                    $lead_data['data'][0]->created_on=date("d M,Y",strtotime($lead_data['data'][0]->created_on));

                }

            $final_response['response']=[
                'status' => $lead_data['status'],
                'message' => $lead_data['message'],
                'data' => $lead_data['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function UpdatePersonalInfo_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $infoName=$this->post('pInfoName');
        $infoCourse=$this->post('pInfoCourse');
        $infoAltmobile=$this->post('pInfoAltMobile');
        $leadGender=$this->post('LeadGender');
        $infoCompany=$this->post('pInfoCompany');
        $infoEmail=$this->post('pInfoEmail');
        $infoDesignation=$this->post('pInfoDesignation');
        $infoEducation=$this->post('pInfoEducation');
        $UserID=$this->post('UserID');
        $leadID=decode($this->post('LeadID'));

        $validations[]=["field"=>"LeadID","label"=>"LeadID","rules"=>"required","errors"=>array('required' => 'ID should not be blank')];
        $validations[]=["field"=>"pInfoName","label"=>'pInfoName',"rules"=>"required|regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => ' Name should not be blank','regex_match' => 'Name should be alphabets  only')];
        $validations[]=["field"=>"pInfoCourse","label"=>'pInfoCourse',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"pInfoAltMobile","label"=>'pInfoAltMobile',"rules"=>"required|regex_match[/^[0-9]+$/]","errors"=>array('required' => ' mobile should not be blank','regex_match' => 'mobile should be in numbers  only')];
        $validations[]=["field"=>"pInfoCompany","label"=>'pInfoCompany',"rules"=>"required","errors"=>array('required' => ' company should not be blank')];
        $validations[]=["field"=>"pInfoEmail","label"=>'pInfoEmail',"rules"=>"required","errors"=>array('required' => 'email should not be blank')];
        $validations[]=["field"=>"pInfoDesignation","label"=>'pInfoDesignation',"rules"=>"required","errors"=>array('required' => ' Designation should not be blank')];
        $validations[]=["field"=>"pInfoEducation","label"=>'pInfoEducation',"rules"=>"required","errors"=>array('required' => ' Education should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array("name"=>$infoName,'gender'=>(int)$leadGender,'fk_course_id'=>$infoCourse,'fk_company_id'=>$infoCompany,'fk_designation_id'=>$infoDesignation,'fk_qualification_id'=>$infoEducation,
                'alternate_mobile'=>$infoAltmobile,'email'=>$infoEmail,
                'status'=>'1'
            );
            $getType=$this->LeadFollowUp_model->updateinfo($data,$leadID);
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }

    function getleadById_get(){

        $lead_id=decode($this->get('lead_id'));
        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_data=$this->LeadFollowUp_model->getSingleLeadById($lead_id);
            if(isset($lead_data['data']) && !empty($lead_data['data']))
            {
                if($lead_data['data'][0]->next_followup_date != "0000-00-00 00:00:00")
                {
                    $lead_data['data'][0]->next_followup_date = date('d M,Y', strtotime($lead_data['data'][0]->next_followup_date));
                }
                else
                {
                    $lead_data['data'][0]->next_followup_date="----";
                }
                if($lead_data['data'][0]->created_on != "0000-00-00 00:00:00")
                {
                    $lead_data['data'][0]->created_on = date('d M,Y', strtotime($lead_data['data'][0]->created_on));
                }
                else
                {
                    $lead_data['data'][0]->created_on="----";
                }
                $lead_data['data'][0]->lead_id = encode($lead_data['data'][0]->lead_id);
            }
            $final_response['response']=[
                'status' => $lead_data['status'],
                'message' => $lead_data['message'],
                'data' => $lead_data['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getLeadHistoryById_get(){

        $lead_id=decode($this->get('lead_id'));
        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_history=$this->LeadFollowUp_model->getLeadHistory($lead_id);
            $final_response['response']=[
                'status' => $lead_history['status'],
                'message' => $lead_history['message'],
                'data' => $lead_history['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getTimelinesById_get(){

        $lead_id=decode($this->get('lead_id'));
        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_timelines=$this->LeadFollowUp_model->getTimelinesById($lead_id);

            if(!empty($lead_timelines['data'])) {
                for($i = 0; $i < count($lead_timelines['data']); $i++) {
                    if ($lead_timelines['data'][$i]->created_on != "0000-00-00 00:00:00") {
                        $lead_timelines['data'][$i]->created_on = date('d M, Y H:i A', strtotime($lead_timelines['data'][$i]->created_on));
                    }
                    else
                    {
                        $lead_timelines['data'][$i]->created_on = "----";
                    }
                }
            }
            $final_response['response']=[
                'status' => $lead_timelines['status'],
                'message' => $lead_timelines['message'],
                'data' => $lead_timelines['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addEducationalInfoDetails_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $Name=$this->post('Name');
        $university=$this->post('University');
        $percentage=$this->post('Percentage');
        $yearOfCompletion = DateTime::createFromFormat('d M, Y',$this->post('YearOfCompletion'));
        $yearOfCompletion=$yearOfCompletion->format("Y-m-d");
        $comments=$this->post('Comments');
        $lead_id=decode($this->post('LeadID'));
        $UserID=$this->post('UserID');


        $validations[]=["field"=>"Name","label"=>"Name","rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"University","label"=>'University',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"Percentage","label"=>'Percentage',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"YearOfCompletion","label"=>'YearOfCompletion',"errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"LeadID","label"=>'LeadID',"rules"=>"required","errors"=>array('required' => 'should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array("course"=>$Name,'university'=>$university,'percentage'=>$percentage,
                'year_of_completion'=>$yearOfCompletion,
                'comments'=>$comments,
                'lead_id'=>$lead_id,
                'created_on'=>date("Y-m-d H:i:s"),
                'created_by'=>$UserID
            );
            $getType=$this->LeadFollowUp_model->AddEducationalInfoDetails($data);
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function UpdateEducationalInfo_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        //echo date('Y-m-d H:i:s',strtotime($this->post('YearOfCompletion')));exit;
        $Name=$this->post('Name');
        $university=$this->post('University');
        $percentage=$this->post('Percentage');
        $yearOfCompletion = DateTime::createFromFormat('d M, Y',$this->post('YearOfCompletion'));
        $yearOfCompletion=$yearOfCompletion->format("Y-m-d");
        $comments=$this->post('Comments');
        $educationInfoId=$this->post('EducationId');
        $lead_id=decode($this->post('LeadID'));
        $UserID=$this->post('UserID');
        $validations[]=["field"=>"Name","label"=>"Name","rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"University","label"=>'University',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"Percentage","label"=>'Percentage',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"YearOfCompletion","label"=>'YearOfCompletion',"errors"=>array('required' => '%s should not be blank')];
        $validations[]=["field"=>"LeadID","label"=>'LeadID',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validations[]=["field"=>"EducationId","label"=>'EducationId',"rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            //date('Y-m-d H:i:s',strtotime($data['dob']))
            $data=array("course"=>$Name,'university'=>$university,'percentage'=>$percentage,
                'year_of_completion'=>$yearOfCompletion,
                'comments'=>$comments,
                'lead_id'=>$lead_id,
                'updated_on'=>date("Y-m-d h:i:s"),
                'updated_by'=>$UserID
            );
            $getType=$this->LeadFollowUp_model->UpdateEducationalInfoDetails($data,$educationInfoId);

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getEducationInfoById_get(){

        $edu_id=$this->get('EducationInfoId');
        $this->form_validation->set_data(array("EducationInfoId" =>$edu_id));
        $validations[]=["field"=>"EducationInfoId","label"=>'EducationInfoId',"rules"=>"required","errors"=>array(
            'required' => 'Id should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_history=$this->LeadFollowUp_model->getEducationDetailsById($edu_id);
            $lead_history['data'][0]->year_of_completion=date("d M,Y",strtotime($lead_history['data'][0]->year_of_completion));
            $final_response['response']=[
                'status' => $lead_history['status'],
                'message' => $lead_history['message'],
                'data' => $lead_history['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getLeadEducationalInfo_get(){

        $lead_id=decode($this->get('lead_id'));

        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_educational_info=$this->LeadFollowUp_model->getLeadEducationalInfo($lead_id);
            for($i=0;$i<count($lead_educational_info['data']);$i++){
                if(isset($lead_educational_info['data'][$i]->year_of_completion))
                {
                    $lead_educational_info['data'][$i]->year_of_completion = date("d M,Y", strtotime($lead_educational_info['data'][$i]->year_of_completion));
                }
            }
            $final_response['response']=[
                'status' => $lead_educational_info['status'],
                'message' => $lead_educational_info['message'],
                'data' => $lead_educational_info['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function AddDesignation_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $DesignationName=$this->post('DesignationName');
        $UserID=$this->post('UserID');
        $type=$this->post('ReferenceType');
        $validations[]=["field"=>"DesignationName","label"=>"DesignationName","rules"=>"required","errors"=>array('required' => 'should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array("value"=>$DesignationName,"fk_created_by"=>$UserID,"created_date"=>date("Y-m-d H:i:s"));
            $getType=$this->LeadFollowUp_model->AddDesignation($data,$type);
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else{

            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function deleteEducationalInfoData_get(){
        $infoId=$this->get('EduInfoId');

        $this->form_validation->set_data(array("EduInfoId" => $this->get('EduInfoId')));

        $validations[]=["field"=>"EduInfoId","label"=>'EduInfoId',"rules"=>"required|integer","errors"=>array(
            'required' => 'should not be blank','integer' => ' should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $res_delete=$this->LeadFollowUp_model->deleteEducationInfoById($infoId);
            $final_response['response']=[
                'status' => $res_delete['status'],
                'message' => $res_delete['message'],
                'data' => $res_delete['data'],
            ];
            if($res_delete['status']===true){
                //sucess
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            else{
                //fail
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }

        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addOtherInfo_post(){
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $cpaCourse=$this->post('CpaCourse');
        $cpaDesignation=$this->post('CpaDesignation');
        $cpaReview=$this->post('CpaReview');
        $presentation=$this->post('Presentation');
        $cmaNumber=$this->post('CmaNumber');

        $UserID=$this->post('UserID');
        $leadID=decode($this->post('leadId'));
        $this->form_validation->set_data(array("leadId" =>$leadID));

        $validations[]=["field"=>"leadId","label"=>'leadId',"rules"=>"required|integer","errors"=>array(
            'required' => 'should not be blank','integer' => ' should be integer value only')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            $data = array("Significance_of_CPA"=>(int)$cpaCourse,'Willingness_to_pursue_the_CPA'=>(int)$cpaDesignation,'Response_on_Miles_CPA' =>(int)$cpaReview,
                'Value_addition' => (int)$presentation,
                'CMA_No' => (int)$cmaNumber,
                'updated_on'=>date("Y-m-d H:i:s"),
                'updated_by'=>$UserID,

            );
            $getType = $this->LeadFollowUp_model->AddOtherInfoDetails($data,$leadID);
            $final_response['response'] = [
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
      else
      {
          $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);



    }
    function getOtherInfoDetailsByLeadId_get(){
        $lead_id=decode($this->get('lead_id'));

        $this->form_validation->set_data(array("lead_id" => $lead_id));
        $validations[]=["field"=>"lead_id","label"=>'lead_id',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_other_info=$this->LeadFollowUp_model->getOtherInfoDetailsById($lead_id);
            $final_response['response']=[
                'status' => $lead_other_info['status'],
                'message' => $lead_other_info['message'],
                'data' => $lead_other_info['data']
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function SaveCallStatusInfo_post(){
        $dndConfigDays = null;
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $branchxcerLeadId=$this->post('BranchxcerLeadId');
        $comments=$this->post('comments');
        $lead_stage=$this->post('lead_stage');
        $statusInfo=$this->post('statusInfo');
        $lead_id=$this->post('LeadID');
        $nextVisitedDate=$this->post('nextvisited');
        $userID=$this->post('UserID');
        $nextLeadStage = '';
        if($statusInfo=="callStatusInfoIntrested"){
            if(isset($lead_stage) && !empty($lead_stage)){
                $stage=$this->LeadFollowUp_model->getLeadStagesName($lead_stage);
                if($stage[0]->lead_stage == "L0")
                {
                    $lead_stage="M3";
                    $nextLeadStage = "M3";
                }
                if($stage[0]->lead_stage == "M2")
                {
                    $lead_stage="M3";
                    $nextLeadStage = "M3";
                }
                if($stage[0]->lead_stage == "M3")
                {
                    $lead_stage="M3+";
                }
                if($stage[0]->lead_stage == "M3+")
                {

                    $lead_stage="M3+";
                }

            }
        }
        if($statusInfo=="callStatusInfoDND"){
            $stage=$this->LeadFollowUp_model->getLeadStagesId($lead_stage);
            $this->load->model('App_configuration_model');
            $leadDetails = $this->App_configuration_model->getConfigurationByName('DND CONFIG');
            $dndConfigDays = $leadDetails[0]->value;
        }
        /*For Lead details*/
        $this->load->model('LeadsInfo_model');
        $leadDetails = $this->LeadsInfo_model->getSingleLeadById($lead_id);

        $nextFollowUpdate=$this->post('NextFollowUpdate');
        $nextFollowUp = null;
        if(isset($nextFollowUpdate) && !empty($nextFollowUpdate) ){
            $nextFollowUpdate = DateTime::createFromFormat('d M, Y',$this->post('NextFollowUpdate'));
            $nextFollowUpdate= $nextFollowUpdate->format("Y-m-d H:i:s");
            }
        else{
            $nextFollowUpdate=date("Y-m-d H:i:s");
        }
        if(isset($nextVisitedDate) && !empty($nextVisitedDate)){
            $nextVisitedDate = DateTime::createFromFormat('d M, Y',$this->post('nextvisited'));
            $nextVisitedDate= $nextVisitedDate->format("Y-m-d H:i:s");
        }else{
            $nextVisitedDate=null;
            $this->load->model('CallOrderConfirmation_model');
            $callOrderData = $this->CallOrderConfirmation_model->getAllLeadStages()['data'];
            foreach ($callOrderData as $key => $value) {
                if($lead_stage == $value->left_side){
                    $addDate = "+" . $value->left_days . " days";
                    $nextFollowUp = date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s') . $addDate));
                }
            }
        }
        $this->form_validation->set_data(array("BranchxcerLeadId" =>$branchxcerLeadId,"comments"=>$comments,"LeadID"=>$lead_id));

        $validations[]=["field"=>"BranchxcerLeadId","label"=>'BranchxcerLeadId',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];
        $validations[]=["field"=>"LeadID","label"=>'LeadID',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];
        $validations[]=["field"=>"comments","label"=>'comments',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true)
        {
            if(isset($stage[0]->lead_stage) && $stage[0]->lead_stage == 'M3+'){
                $nextFollowUp = $nextVisitedDate;
            }
            if(isset($lead_stage) && !empty($lead_stage) && is_numeric($lead_stage)){
                $lead_stage = $lead_stage;
            }
            else
            {
                $lead_stage=$this->LeadFollowUp_model->getLeadStagesId($lead_stage);
            }
            
            $data = array(
                "branch_xref_lead_id"=>$branchxcerLeadId,
                "description"=>$comments,
                "status"=>$lead_stage,
                "expected_visit_date"=>$nextVisitedDate,
                "fk_lead_id"=>$lead_id,
                "fk_user_id"=>$userID,
                "dndConfigDays"=>$dndConfigDays
            );
            if($nextFollowUp!=null){
                $data['next_followup_date']=$nextFollowUp;
            }
                /*"status"=>$lead_stage,"next_followup_date"=>$nextVisitedDate,"fk_lead_id"=>$lead_id,"fk_user_id"=>$userID);*/
            $getType = $this->LeadFollowUp_model->SaveCallStatusInfo($data);
            /*Mail functionality started*/
            if($getType['status'] == true && $nextLeadStage=='M3'){
                $key = 'LEADSTAGE';
                /*For Lead details*/
                $this->load->model('LeadsInfo_model');
                $leadDetails = $this->LeadsInfo_model->getSingleLeadById($lead_id);
                if(isset($leadDetails['data'])){
                    $leadDetail = $leadDetails['data'][0];
                    $userData = array(
                        'leadName' => $leadDetail->name,
                        'last_modified' => $leadDetail->last_modified,
                        'leadEmail' => $leadDetail->email,
                        'courseName' => $leadDetail->course,
                        'branchName' => $leadDetail->city,
                        'batchCode' => ' -- ',
                        'enrollNo' => ' -- ',
                        'feeheadName' => ' -- ',
                        'amountPayable' => ' -- ',
                        'amountPaid' => ' -- ',
                        'due_days_amt' => ' -- ',
                        'due_days_html' => ' -- ',
                        'companyName' => ' -- ',
                        'institutionName' => ' -- ',
                        'currentDate' => (new DateTime())->format('d M,Y'),
                    );
                    $this->load->model("EmailTemplates_model");
                    $mailTemplate = $this->EmailTemplates_model->getTemplate($key,$userData);
//                    $res = $this->sendMail($leadDetail->email, $mailTemplate['title'], $mailTemplate['content'], $mailTemplate['attachment']);
//                    $res = $this->sendMail('bh.hyd.miles@gmail.com', $mailTemplate['title'], $mailTemplate['content'], $mailTemplate['attachment']);
					if(is_array($mailTemplate) && isset($mailTemplate['error']) && $mailTemplate['error'] == 'dberror'){
                        //db error
                    } else if(!is_array($mailTemplate) && strlen($mailTemplate) == 'nodata'){
                        //no template found
                    }else{
                        $sendemail = new sendemail;
                        $res = $sendemail->sendMail('bh.hyd.miles@gmail.com', $mailTemplate['title'], $mailTemplate['content'], $mailTemplate['attachment']);
                    }
                }
            }
            $final_response['response'] = [
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getBranchVisitorById_get(){
        $branchXrefId=$this->get('branchXrefId');

        $this->form_validation->set_data(array("branchXrefId" => $branchXrefId));
        $validations[]=["field"=>"branchXrefId","label"=>'branchXrefId',"rules"=>"required|integer","errors"=>array(
            'required' => 'Id should not be blank','integer' => 'Id should be integer value only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $lead_other_info=$this->LeadsInfo_model->getBranchVisitorById($branchXrefId);
            if(!empty($lead_other_info['data'][0])){
                $lead_other_info['data'][0]->created_date=date("d M,Y",strtotime($lead_other_info['data'][0]->created_date));
            }
            $final_response['response']=[
                'status' => $lead_other_info['status'],
                'message' => $lead_other_info['message'],
                'data' => $lead_other_info['data']
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function SaveTimeline_post(){
//    print_r($_FILES);die;
        $max_fileUpload_size = 1024;    //For Fmax filr upload (in KB)
        $status=true;$message="";$result="";
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        if($this->post('date')!=""){
            $timelineDate = DateTime::createFromFormat('d M, Y',$this->post('date'));
            $timelineDate=$timelineDate->format("Y-m-d H:i:s");
        }else{
            $timelineDate=null;
        }

        $information=$this->post('information');
        $leadId=decode($this->post('LeadId'));
        $typeOfTimeline=$this->post('typeOfTimeline');
        $branchXrefId=$this->post('BranchXrefId');
        $file=$this->post('image');
        $UserID=$this->post('userID');
//        print_r($_POST);exit;
        if(!empty($file)) {
            if ((($_FILES['file']['size'] / 1024) / 1024) > $max_fileUpload_size) {
                $message = 'Upload Error';
                $status = false;
                $result = array('attachemntUpload' => 'File can\'t be more than 2Mb');
                $data = array("status" => $status, "message" => $message, "data" => $result);
                $this->response($data);
            }

            preg_match_all('/\.[0-9a-z]+$/', $_FILES['file']['name'], $res); /*For uploaded Image extension*/
        }
        $this->form_validation->set_data(array("BranchXrefId" =>$branchXrefId));

        $validations[]=["field"=>"BranchXrefId","label"=>'BranchXrefId',"rules"=>"required|integer","errors"=>array(
            'required' => 'should not be blank','integer' => ' should be integer value only')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true) {
            if(count($_FILES) > 0) {
                $this->load->helper(array('form', 'url'));

                $config['upload_path'] = './../application/uploads';
                $config['file_name'] = $file;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = $max_fileUpload_size;
                $this->load->library('upload', $config);
                $data = array("lead_id"=>$leadId,"interaction_file"=>$file,"type"=>$typeOfTimeline,"interaction_date"=>$timelineDate,'description'=>$information,'created_on'=>date("Y-m-d H:i:s"),'created_by' =>$UserID,"branch_xref_lead_id"=>$branchXrefId
                );
                if (!$this->upload->do_upload('file'))
                {
                    $message='Upload Error';
                    $status=false;
                    $result = array('imageUpload' => $this->upload->display_errors());
                }
            }else{
                $data = array("lead_id"=>$leadId,"type"=>$typeOfTimeline,"interaction_date"=>$timelineDate,'description'=>$information,'created_on'=>date("Y-m-d H:i:s"),'created_by' =>$UserID,"branch_xref_lead_id"=>$branchXrefId
                );
            }
            $getType = $this->LeadsInfo_model->saveTimelineDetails($data);
//                $config['max_width']  = '1024';
//                $config['max_height']  = '768';



            $final_response['response'] = [
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);



    }
    function ShowLeadsCount_get(){
        $leads_count=array();
        $newLeadsCount=$this->LeadsInfo_model->getNewLeadsCount("M3");
        $branchXcefLeads=$this->LeadsInfo_model->getNewLeadsCount();
        $transferdLeadsCount=$this->LeadsInfo_model->TransferdLeadsCount();
        $students=$this->LeadsInfo_model->getNewLeadsCount("M7");



        $leads_count['new_leads']=$newLeadsCount['data'][0]->NewLeads;
        $leads_count['total_leads']=$branchXcefLeads['data'][0]->NewLeads - $transferdLeadsCount['data'][0]->Transfered;
        $leads_count['transferd_leds']=$transferdLeadsCount['data'][0]->Transfered;
        $leads_count['completed_leds']=$students['data'][0]->NewLeads;

        $this->response($result=$leads_count);


    }
    function ChangeCallStatusInfo_post(){
        $dndConfigDays = null;
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $data['branchxcerLeadId']=$branchxcerLeadId=$this->post('BranchxcerLeadId');
        $data['comments']=$comments=$this->post('comments');
        $data['lead_stage']=$lead_stage=$this->post('lead_stage');
        $data['lead_id']=$lead_id=$this->post('LeadID');
        $date=$this->post('date');
        $data['dateType']=$dateType=$this->post('dateType');
        $data['callType']=$dateType=$this->post('callType');
        $data['UserID']=$userID=$this->post('UserID');
        $data['currentStage']=$this->post('currentStage');
        $nextLeadStage='';
        $leadStage= $this->post('lead_stage');
        $leadStagePrefix = substr($leadStage, 0, 2);
        if($leadStagePrefix!='' && $leadStagePrefix=='L_'){
            $statusExplode=explode('_',$leadStage);
            $nextLeadStage=$statusExplode[2];
        }
        else{
            $nextLeadStage=$leadStage;
        }


        /*For Lead details*/
        $this->load->model('LeadsInfo_model');
        $leadDetails = $this->LeadsInfo_model->getSingleLeadById($lead_id);

        $nextFollowUpdate=$this->post('date');
        $nextFollowUp = null;
        if(isset($date) && !empty($date) && $date!='' ){
            $commonDateFormat = DateTime::createFromFormat('d M, Y',$this->post('date'));
            $commonDate = $commonDateFormat->format("Y-m-d H:i:s");
        }
        else{
            $commonDate=NULL;
        }
        $data['commonDate']=$commonDate;

        $this->form_validation->set_data(array("BranchxcerLeadId" =>$branchxcerLeadId,"comments"=>$comments,"LeadID"=>$lead_id));

        $validations[]=["field"=>"BranchxcerLeadId","label"=>'BranchxcerLeadId',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];
        $validations[]=["field"=>"LeadID","label"=>'LeadID',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];
        $validations[]=["field"=>"comments","label"=>'comments',"rules"=>"required","errors"=>array(
            'required' => 'should not be blank')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true)
        {

            /*"status"=>$lead_stage,"next_followup_date"=>$nextVisitedDate,"fk_lead_id"=>$lead_id,"fk_user_id"=>$userID);*/
            $getType = $this->LeadFollowUp_model->ChangeCallStatusInfo($data);
            /*Mail functionality started*/
            if($getType['status'] == true && $getType['data']['mail_send']==1){
                $key = 'LEADSTAGE';
                /*For Lead details*/
                if(isset($leadDetails['data'])){
                    $leadDetail = $leadDetails['data'][0];
                    $userData = array(
                        'leadName' => $leadDetail->name,
                        'last_modified' => $leadDetail->last_modified,
                        'leadEmail' => $leadDetail->email,
                        'courseName' => $leadDetail->course,
                        'branchName' => $leadDetail->city,
                        'batchCode' => ' -- ',
                        'enrollNo' => ' -- ',
                        'feeheadName' => ' -- ',
                        'amountPayable' => ' -- ',
                        'amountPaid' => ' -- ',
                        'due_days_amt' => ' -- ',
                        'due_days_html' => ' -- ',
                        'companyName' => ' -- ',
                        'institutionName' => ' -- ',
                        'currentDate' => (new DateTime())->format('d M,Y'),
                    );
                    $this->load->model("EmailTemplates_model");
                    $mailTemplate = $this->EmailTemplates_model->getTemplate($key,$userData);
//                    $res = $this->sendMail($leadDetail->email, $mailTemplate['title'], $mailTemplate['content'], $mailTemplate['attachment']);
//                    $res = $this->sendMail('bh.hyd.miles@gmail.com', $mailTemplate['title'], $mailTemplate['content'], $mailTemplate['attachment']);
                    if(is_array($mailTemplate) && isset($mailTemplate['error']) && $mailTemplate['error'] == 'dberror'){
                        //db error
                    } else if(!is_array($mailTemplate) && strlen($mailTemplate) == 'nodata'){
                        //no template found
                    }else{
                        $sendemail = new sendemail;
                        $res = $sendemail->sendMail('bh.hyd.miles@gmail.com', $mailTemplate['title'], $mailTemplate['content'], $mailTemplate['attachment']);
                    }
                }
            }
            $final_response['response'] = [
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
}