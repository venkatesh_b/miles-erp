<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 1/8/16
 * Time: 12:06 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class LeadStageChange extends REST_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
        $this->load->model("LeadStageChange_model");
    }
    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);
        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0)
            {
                return $this->form_validation->error_array();
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    function ChangeLeadStage_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata)
        {
            $_POST = $postdata;
        }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"courseId","label"=>'courseBranches',"rules"=>"regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Choose course','regex_match' => 'Choose course')];
        $validations[]=["field"=>"comments","label"=>'courseDescription',"rules"=>"required","errors"=>array('required' => 'Comments cannot be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus == true)
        {
            $data['userID']=$this->post('userID');
            $data['branchId']=$this->post('branchId');
            $data['courseId']=$this->post('courseId');
            $data['comments']=$this->post('comments');
            $final_response['response']=$this->LeadStageChange_model->changeLeadStages($data);
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function getConvertedLeads_get()
    {
        $getdata = json_decode(file_get_contents("php://input"), true);
        if($getdata)
        {
            $_GET = $getdata;
        }
        $this->form_validation->set_data($this->get());
        $data['branchId']=$this->get('branchId');
        if($data['branchId'] == NULL || $data['branchId'] == '')
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Invalid branch',
                'data' => 'Invalid branch',
            ];
        }
        else
        {
            $final_response['response'] = $this->LeadStageChange_model->getAllConvertedToOldLeads($data['branchId']);
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function LeadsCountByStage_post()
    {
        $data['branchId']=$this->post('branchId');
        $data['courseId']=$this->post('courseId');
        if($data['branchId'] == NULL || $data['branchId'] == '')
        {
            $final_response=[
                'status' => FALSE,
                'message' => 'Invalid branch',
                'data' => 'Invalid branch',
            ];
        }
        elseif($data['courseId'] == NULL || $data['courseId'] == '')
        {
            $final_response=[
                'status' => FALSE,
                'message' => 'Invalid course',
                'data' => 'Invalid course',
            ];
        }
        else
        {
            $data['branchId']=$this->post('branchId');
            $data['courseId']=$this->post('courseId');
            $final_response=$this->LeadStageChange_model->getLeadsCountByStage($data);
        }
        $this->response($final_response, REST_Controller::HTTP_OK);
    }

    function LeadsCountByAllStages_post()
    {
        $data['leadstageId']=$this->post('leadstageId');
        if($data['leadstageId'] == NULL || $data['leadstageId'] == '')
        {
            $final_response=[
                'status' => FALSE,
                'message' => 'Invalid record',
                'data' => 'Invalid record',
            ];
        }
        else
        {
            $final_response=$this->LeadStageChange_model->getLeadsCountOfAllStages($data);
        }
        $this->response($final_response, REST_Controller::HTTP_OK);
    }
}