<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/*
 * Created By: V Parameshwar. Date: 04-02-2016
 * Purpose: Services for Leads.
 *
 */

class LeadStatus extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){

        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0){
                return $this->form_validation->error_array();
            }
            else{
                return true;
            }
        }
        else{
            return true;
        }

    }
    function getLeadStages_get(){
        $contactId='';

        $currentStage=$this->get('currentStage');
        $file='./uploads/leadStatus.json';
        if(file_exists($file)){
            $data = json_decode(file_get_contents($file));
            if(empty($data->$currentStage)){
                $stageInfo=[];
            }
            else{
                $stageInfo=$data->$currentStage;
            }
        }
        else{
            $stageInfo=[];
        }

        $final_response['response']=[
            'status' => true,
            'message' => 'success',
            'data' => $stageInfo,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    function getBranchVisitLeadStages_get()
    {
        $currentStage=$this->get('currentStage');
        $file='./uploads/branchvisitLeadStatus.json';
        if(file_exists($file))
        {
            $data = json_decode(file_get_contents($file));
            if(empty($data->$currentStage))
            {
                $stageInfo=[];
            }
            else
            {
                $stageInfo=$data->$currentStage;
            }
        }
        else
        {
            $stageInfo=[];
        }
        $final_response['response']=[
            'status' => true,
            'message' => 'success',
            'data' => $stageInfo,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }


}

