<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-09
 * Time: 05:09 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Product extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("Product_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE)
        {
            if (count($this->form_validation->error_array()) > 0)
            {
                return $this->form_validation->error_array();
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    function productList_get()
    {
        $allProducts = $this->Product_model->productList();
        $final_response['response']=[
            'status' => $allProducts['status'],
            'message' => $allProducts['message'],
            'data' => $allProducts['data']
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;

        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function products_get()
    {
        $this->response($this->Product_model->productDataTables(),REST_Controller::HTTP_OK);
    }
    function addProduct_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"productName","label"=>'productName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]+$/]","errors"=>array('required' => 'Product Name should not be blank','regex_match' => 'Invalid Product Name')];

        $validations[]=["field"=>"productType","label"=>'productType',"rules"=>"required","errors"=>array('required' => 'Product Type should not be blank')];

        $validations[]=["field"=>"productCode","label"=> 'productCode',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]+$/]","errors"=>array('required' => 'Product Code should not be blank','regex_match' => 'Invalid Product Code')];

        $validations[]=["field"=>"publication","label"=>'publication',"rules"=>"required","errors"=>array('required' => 'Publication should not be blank')];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $data = array(
                'name' =>trim($this->post('productName')),
                'fk_product_type_id' => $this->post('productType'),
                'fk_publication_id' => $this->post('publication'),
                'code' => $this->post('productCode'),
                'version' => $this->post('version'),
                'description' => $this->post('description'),
                'created_by' => $_SERVER['HTTP_USER']
            );
            $res=$this->Product_model->addProduct($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getProductDetails_get()
    {
        $productId=decode($this->get('productId'));
        $this->form_validation->set_data(array("productId"=>$productId));

        $validations[]=["field"=>"productId","label"=>'productId',"rules"=>"required","errors"=>array('required' => 'Product id should not be blank','regex_match' => 'Product id should be numeric only')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $data['productId']=$productId;
            $res=$this->Product_model->checkProductExist($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
            if($res['status']===true)
            {
                $res=$this->Product_model->getProductDetailsById($data);
                $final_response['response']=[
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function updateProduct_post()
    {
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $productId=decode($this->post('productId'));
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"productId","label"=>'productId',"rules"=>"required","errors"=>array('required' => 'Product ID should not be blank','regex_match' => 'Product ID should be numeric only')];

        $validations[]=["field"=>"productName","label"=>'productName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]+$/]","errors"=>array('required' => 'Product Name should not be blank','regex_match' => 'Invalid Product Name')];

        $validations[]=["field"=>"productType","label"=>'productType',"rules"=>"required","errors"=>array('required' => 'Product Type should not be blank')];

        $validations[]=["field"=>"productCode","label"=> 'productCode',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]+$/]","errors"=>array('required' => 'Product Code should not be blank','regex_match' => 'Invalid Product Code')];

        $validations[]=["field"=>"publication","label"=>'publication',"rules"=>"required","errors"=>array('required' => 'Publication should not be blank')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $data = array(
                'name' =>$this->post('productName'),
                'fk_product_type_id' => $this->post('productType'),
                'fk_publication_id' => $this->post('publication'),
                'code' => $this->post('productCode'),
                'version' => $this->post('version'),
                'description' => $this->post('description'),
                'created_by' => $_SERVER['HTTP_USER'],
                'product_id' => $productId
            );
            $res=$this->Product_model->updateProduct($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function deleteProduct_post()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $param['productId']=decode($this->post('productId'));
        $res=$this->Product_model->deleteProduct($param);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}