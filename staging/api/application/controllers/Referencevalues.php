<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Referencevalues extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("References_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
    }
    function customvalidation($data){
   
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
}

    public function getSingleReferenceValues_get(){
        
        $status=true;$message="";$result="";
        
        $reference_type_value_id = $this->get('reference_type_value_id');
        $status=true;$message="";$result="";
        
        $this->form_validation->set_data(array('id' => $reference_type_value_id));
        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE) 
        {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } 
        else 
        {
            $branchList = $this->References_model->getSingleReferenceValue($reference_type_value_id);
            if ($branchList == 'badrequest')
            {
                $message="No data Found";
                $status=false;
                $result=array();
            }
            else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
            {
                $message='Database error';
                $status=false;
                $result = $branchList['msg'];
            }
            else if ($branchList) 
            {
                $message="Reference Detail";
                $status=true;
                $result=$branchList;
            }
            else 
            {
                $message="No data found";
                $status=false;
                $result=array();
            }
        }
        
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
  
    public function getAllReferenceValues_get(){
        
        //$is_having_scope=$this->oauth->checkscope('Employeelist');
        $is_having_scope=true;//static 
        if($is_having_scope===true){
            
            $reference_type_id=$this->get("reference_type_id");
            $this->form_validation->set_data(array("reference_type_id" => $reference_type_id));
        
        $validations[]=["field"=>"reference_type_id","label"=>"reference_type_id","rules"=>"required|integer","errors"=>array(
        'required' => 'Reference ID should not be blank','integer' => 'Reference ID should be integer value only'
        )];
        
        $validationstatus=$this->customvalidation($validations);
        
         if($validationstatus===true){
            $res=$this->References_model->getReferenceValuesList($reference_type_id);
            if($res['status']===true){
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            else{
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            $result = array('status'=>$res['status'], 'message' => $res['message'], 'data'=>$res['data']);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
            
            
            
         }
         else{
             
             $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            
         }
        
        }
        else{
            $final_response['response']=[
            'status'=>FALSE,
            'message' => 'Access Denied',
            'data'=>array(),
            'links' =>array()
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    
    /**
     * function name : getReferenceValuesByName controller
     * Params 
     *      - name (Refrence type name)
     * return 
     *      Success - list
     *      Error/no records - blank array
     * 
     * author : Abhilash
     */
    public function getReferenceValuesByName_get(){
        $name = urldecode($this->get('name'));
        $parent=empty(trim($this->get('parent_id'))) ? '' : trim($this->get('parent_id')) ;
        //name_check
        $status=true;$message="";$result="";
        
        $this->form_validation->set_data(array('name' => $name));
        $this->form_validation->set_rules('name', "Name", 'required|callback_name_check');

        if ($this->form_validation->run() == FALSE) 
        {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        }
        else 
        {
//            echo $name;die;
            $getType=$this->References_model->getReferenceType($name);
//            print_r($getType);die;
            if(count($getType['data']) > 0)
            {
//                print_r($getType[0]->reference_type_id);die;
                $list = $this->References_model->getReferenceValuesByName($name,$parent);
//                print_r($list);die;
                if(count($list) > 0)
                {
                    $message=$name." list";
                    $status=true;
                    $result=$list;
                }
                else
                {
                    $message=$name." list found";
                    $status=false;
                    $result=[];
                }
                
            }
            else
            {
                $message=$getType['message'];
                $status=false;
                $result=[];
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->response($data);
    }
    
    public function getReferenceValuesUsingName_get(){

        $reference_type_value=$this->get("name");
        $parent_id=$this->get("parent_id");

        
       $this->form_validation->set_data(array("parent_id" => $this->get('parent_id'),"reference_type_id" => $this->get('name')));

       $validations[]=[
           "field"=>"reference_type_id",
           "label"=>"reference_type_id",
           "rules"=>"required|callback_name_check"
        ];
       
       $validations[]=["field"=>"parent_id","label"=>"parent_id","rules"=>"integer","errors"=>array(
        'integer' => 'Reference ID should be integer value only'
        )];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
        $res=$this->References_model->getReferenceValuesUsingName($reference_type_value,$parent_id);
        if($res['status']===true){
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        else{
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
        
       }else{
           $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
       }
        
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    
    public function getReferenceValues_get(){

        $reference_type_id=$this->get("reference_type_id");
        $parent_id=$this->get("parent_id");

        
       $this->form_validation->set_data(array("parent_id" => $this->get('parent_id'),"reference_type_id" => $this->get('reference_type_id')));

       $validations[]=["field"=>"reference_type_id","label"=>"reference_type_id","rules"=>"required|integer","errors"=>array(
        'required' => 'Reference ID should not be blank','integer' => 'Reference ID should be integer value only'
        )];
       
       $validations[]=["field"=>"parent_id","label"=>"parent_id","rules"=>"integer","errors"=>array(
        'integer' => 'Reference ID should be integer value only'
        )];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
        $res=$this->References_model->getReferenceValues($reference_type_id,$parent_id);
        if($res['status']===true){
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        else{
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
        
       }else{
           $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
       }
        
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    public function getFieldsList_get(){

        $reference_type_id=$this->get("reference_type_id");
        $this->form_validation->set_data(array("reference_type_id" => $this->get('reference_type_id')));
        $this->form_validation->set_rules('reference_type_id', 'reference_type_id', 'required|integer',array(
        'required' => 'Reference ID should not be blank','integer' => 'Reference ID should be integer value only'
        ));
        if ($this->form_validation->run() == FALSE)
        {
            foreach($this->form_validation->error_array() as $formerrors_key => $formerrors_value){
            $this->response([
            'status' => FALSE,
            'message' => $formerrors_value,
            'data' => '',
            ], REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code

            }
        }
        $res=$this->References_model->getAddReferenceValueFileds($reference_type_id);
        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$res);
        $this->response($result, REST_Controller::HTTP_OK);

    }
    public function addReferenceValue_post(){
        $this->form_validation->set_data($this->input->post());
        $reference_type_id=$this->input->post('reference_type_id');
        $reference_type_val=$this->input->post('reference_type_val');
        $reference_type_val_description=$this->input->post('reference_type_val_description');
        $reference_type_is_active=$this->input->post('reference_type_is_active');
        $reference_type_val_parent=empty(trim($this->input->post('reference_type_val_parent'))) ? '' : trim($this->input->post('reference_type_val_parent')) ;
        $reference_type=empty(trim($this->input->post('reference_type'))) ? '' : trim($this->input->post('reference_type')) ;
        if($reference_type!=''){
            $getType=$this->References_model->getReferenceType($reference_type);

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

            if($getType['status']===true) {

                foreach ($getType['data'] as $k => $v) {
                    $reference_type_id = $v->reference_type_id;
                }
            }
        }
        $reftype=$this->References_model->GetReferences($reference_type_id);
        $error_label="Reference";
        foreach($reftype as $rowrefurenece){
            $error_label=$rowrefurenece->name;
        }
        

        $validations[]=["field"=>"reference_type_id","label"=>"reference_type_id","rules"=>"required|integer","errors"=>array(
        'required' => $error_label.' ID should not be blank','integer' => $error_label.' ID should be integer value only'
        )];
        
       /*$validations[]=["field"=>"reference_type_val","label"=> $error_label.' Value',"rules"=>"required|regex_match[/^[A-Za-z, \']+$/]","errors"=>array(
        'required' => $error_label.' Value should not be blank','regex_match' => 'Accepts only Alphabetic'
        )];*/

        $validations[]=["field"=>"reference_type_val","label"=> $error_label.' Value',"rules"=>"required","errors"=>array(
            'required' => $error_label.' Value should not be blank','regex_match' => 'Accepts only Alphabetic'
        )];
       
       $validations[]=["field"=>"reference_type_val_description","label"=> $error_label.' Description ',"rules"=>"","errors"=>array(
        'required' => $error_label.' Description should not be blank')];

        $validations[]=["field"=>"reference_type_val_parent","label"=> 'reference_type_val_parent',"rules"=>"integer","errors"=>array('integer' => 'Parent ID should be integer value only')];

        $validations[]=["field"=>"reference_type_is_active","label"=> 'reference_type_is_active',"rules"=>"required|integer|regex_match[/^[0-1]$/]","errors"=>array(
'required' => $error_label.' Status should not be blank','integer' => $error_label.' Status should be integer value only','regex_match' => $error_label.' Status should be either 0 or 1'
)];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();
            $data['reference_type_id']=$reference_type_id;
            $data['reference_type_val']=$reference_type_val;
            $data['reference_type_val_description']=$reference_type_val_description;
            $data['reference_type_val_parent']=$reference_type_val_parent;
            $data['reference_type_is_active']=$reference_type_is_active;


            $res_add=$this->References_model->AddReferenceValue($data);
            $final_response['response']=[
            'status'=>$res_add['status'],
            'message' => $res_add['message'],
            'data'=>$res_add['data']];
            
             if($res_add['status']===true){
                //sucess
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            else{
                //fail
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    public function editReferenceValue_post(){

$this->form_validation->set_data($this->input->post());
        $reference_type_val_id=$this->input->post('reference_type_val_id');
        $reference_type_id=$this->input->post('reference_type_id');
        $reference_type_val=$this->input->post('reference_type_val');
        $reference_type_val_description=$this->input->post('reference_type_val_description');
        $reference_type_is_active=$this->input->post('reference_type_is_active');

        $reference_type_val_parent=empty(trim($this->input->post('reference_type_val_parent'))) ? '' : trim($this->input->post('reference_type_val_parent'));
        $reference_type=empty(trim($this->input->post('reference_type'))) ? '' : trim($this->input->post('reference_type')) ;
        if($reference_type!=''){
            $getType=$this->References_model->getReferenceType($reference_type);

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

            if($getType['status']===true) {

                foreach ($getType['data'] as $k => $v) {
                    $reference_type_id = $v->reference_type_id;
                }
            }
        }
        $error_label="Reference";
        $reftype=$this->References_model->GetReferences($reference_type_id);
        foreach($reftype as $rowrefurenece){

            $error_label=$rowrefurenece->name;

        } 

        $validations[]=["field"=>"reference_type_val_id","label"=>"reference_type_val_id","rules"=>"required|integer","errors"=>array(
        'required' => $error_label.' ID should not be blank','integer' => $error_label.' ID should be integer value only'
        )];
        $validations[]=["field"=>"reference_type_id","label"=>"reference_type_id","rules"=>"required|integer","errors"=>array(
        'required' => $error_label.' ID should not be blank','integer' => $error_label.' ID should be integer value only'
        )];
        /*$validations[]=["field"=>"reference_type_val","label"=>$error_label.' Value',"rules"=>"required|regex_match[/^[A-Za-z, \']+$/]","errors"=>array(
        'required' => $error_label.' Value should not be blank','regex_match' => $error_label.' Value should be alphabetic only'
        )];*/

        $validations[]=["field"=>"reference_type_val","label"=>$error_label.' Value',"rules"=>"required","errors"=>array(
            'required' => $error_label.' Value should not be blank','regex_match' => $error_label.' Value should be alphabetic only'
        )];
        
        $validations[]=["field"=>"reference_type_val_description","label"=> $error_label.' Description ',"rules"=>"","errors"=>array(
        'required' => $error_label.' Description should not be blank')];
        
        
        $validations[]=["field"=>"reference_type_val_parent","label"=>'reference_type_val_parent',"rules"=>"integer","errors"=>array('integer' => 'Parent ID should be integer value only')];
        $validations[]=["field"=>"reference_type_is_active","label"=>'reference_type_is_active',"rules"=>"required|integer|regex_match[/^[0-1]$/]","errors"=>array(
'required' => $error_label.' Status should not be blank','integer' => $error_label.' Status should be integer value only','regex_match' => $error_label.' Status should be either 0 or 1'
)];

        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();
            $data['reference_type_id']=$reference_type_id;
            $data['reference_type_val']=$reference_type_val;
            $data['reference_type_val_description']=$reference_type_val_description;
            $data['reference_type_val_parent']=$reference_type_val_parent;
            $data['reference_type_is_active']=$reference_type_is_active;
            $data['reference_type_val_id']=$reference_type_val_id;
            $res_update=$this->References_model->UpdateReferenceValue($data);
            $final_response['response']=[
                'status' => $res_update['status'],
                'message' => $res_update['message'],
                'data' => $res_update['data'],
                ];
            
            
            if($res_update['status']===true){
                //sucess
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;

            }
            else{
                //fail
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    
    public function deleteReferenceValue_delete(){

        $error_label="Reference";
        $reference_type_val_id=$this->get('reference_type_val_id');
        $this->form_validation->set_data(array("reference_type_val_id" => $this->get('reference_type_val_id')));

        $validations[]=["field"=>"reference_type_val_id","label"=>'reference_type_val_id',"rules"=>"required|integer","errors"=>array(
        'required' => $error_label.' ID should not be blank','integer' => $error_label.' ID should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            
            $data['reference_type_val_id']=$reference_type_val_id;
            $res_delete=$this->References_model->RemoveReferenceValue($data);
            $final_response['response']=[
                'status' => $res_delete['status'],
                'message' => $res_delete['message'],
                'data' => $res_delete['data'],
                ];
            if($res_delete['status']===true){
                //sucess
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            else{
                //fail
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
             }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    
    /**
     * Function for checking number(REGEX)
     * 
     * @args 
     *  "string"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    /**
     * Function for checking valid string
     * 
     * @args 
     *  "str"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function name_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str)) {
            $this->form_validation->set_message('name_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function getReferenceValuesList_get()
    {
        $reference_type=trim($this->get("reference_type"));
        $this->response($this->References_model->referencesDataTables($reference_type),REST_Controller::HTTP_OK);
        exit;
    }
    public function addReferenceValueWrapper_post(){


        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $referenceName=$this->input->post('referenceName');
        $referenceDescription=$this->input->post('referenceDescription');
        $referenceType=$this->input->post('referenceType');


        $validations[]=["field"=>"referenceName","label"=>'referenceName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9 ]*$/]","errors"=>array('required' => 'Name should not be blank','regex_match' => 'Invalid '.$referenceType)];

        $validations[]=["field"=>"referenceDescription","label"=> 'referenceDescription',"rules"=>"required","errors"=>array('required' => 'Description should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();

            $getType=$this->References_model->getReferenceType($referenceType);

            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
            ];

            if($getType['status']===true){

                foreach($getType['data'] as $k=>$v){
                    $reftype=$v->reference_type_id;
                }
                $data = array(
                    'reference_type_id' => $reftype ,
                    'reference_type_val' => $referenceName ,
                    'reference_type_val_parent' => NULL,
                    'reference_type_is_active' => 1,
                    'reference_type_val_description' => $referenceDescription
                );

                $createRoleRes=$this->References_model->AddReferenceValue($data);
                $final_response['response']=[
                    'status' => $createRoleRes['status'],
                    'message' => $createRoleRes['message'],
                    'data' => array($referenceType=>$createRoleRes['message']),
                ];
                if($createRoleRes['status']===true){
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
            ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }

        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
}