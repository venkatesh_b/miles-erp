<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 27/1/17
 * Time: 11:36 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class SRCallSchedule extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("SRCallSchedule_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function customvalidation($data)
    {

        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE) {
            if (count($this->form_validation->error_array()) > 0) {
                return $this->form_validation->error_array();
            } else {
                return true;
            }
        } else {
            return true;
        }

    }

    function getStudentColdCallingCount_get()
    {
        $status=TRUE;$message='success';$data='';
        $param='';
        $branchId=$this->get('branchId');
        $toDate=$this->get('toDate');

        $this->form_validation->set_data(array('branchId'=>$branchId));
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $param['branchId']=$branchId;
            $param['userId']=$_SERVER['HTTP_USER'];
            $param['toDate']=$toDate;
            $response_users=$this->SRCallSchedule_model->getStudentColdCallingCount($param);
            $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        }
        else
        {
            $status=FALSE;$message='validation failed';$data=$validationstatus;
        }

        $final_response=
            [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            ];
        $this->response($final_response, REST_Controller::HTTP_OK);
    }

    function getStudentCallSchedules_get()
    {
        $status=true;$message='success';$data='';
        $param='';
        $branchId=$this->get('branchId');
        $toDate=$this->get('toDate');
        $this->form_validation->set_data(array('branchId'=>$branchId));
        $validations[]=["field"=>"branchId","label"=>'branchId',"rules"=>"required","errors"=>array('required' => 'Branch should not be blank')];

        $validationstatus=$this->customvalidation($validations);

        if($validationstatus===true)
        {
            $status=TRUE;$message='success';$data='';
            $param['branchId']=$branchId;
            $param['userId']=$_SERVER['HTTP_USER'];
            $param['toDate']=$toDate;
            $response_users=$this->SRCallSchedule_model->getStudentCallSchedules($param);
            $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        }
        else
        {
            $status=FALSE;$message='validation failed';$data=$validationstatus;
        }
        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    function addSRCallSchedule_post()
    {
        $status=true;$message='success';$data='';
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        $param['branchId']=$this->post('branchId');
        $param['callSchedules']=$this->post('callSchedules');
        $param['createdBy']=$_SERVER['HTTP_USER'];
        $response_users=$this->SRCallSchedule_model->addCallSchedule($param);
        $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        $final_response=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $this->response($final_response, REST_Controller::HTTP_OK);
    }
    function releaseScheduledCalls_post(){

        $status=true;$message='success';$data='';
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }

        $param['branchId']=$this->post('branchId');
        $param['releaseCnt']=$this->post('releaseCnt');
        $param['releaseBy']=$_SERVER['HTTP_USER'];
        $param['assignedUserId']=$this->post('assignedUserId');
        $toDate=$this->post('toDate');
        $param['toDate']=$toDate;
        $param['type']=$this->post('type');
        $response_users=$this->SRCallSchedule_model->releaseCallSchedule($param);
        $status=$response_users['status'];$message=$response_users['message'];$data=$response_users['data'];
        $final_response['response']=[
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getFollowUpStudentsList_get()
    {
        $message='Student Details';
        $status=true;

        $branch = strlen(trim($this->get('branch')))>0?"".trim($this->get('branch'))."":'NULL';
        $data['branchId']=$branch;
        $data['userID']=$_SERVER['HTTP_USER'];
        $res = $this->SRCallSchedule_model->getFollowUpStudentsList($data);

        $this->response($res);

    }
}