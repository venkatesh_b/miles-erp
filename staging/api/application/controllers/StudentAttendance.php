<?php
/**
 * Created by PhpStorm.
 * User: NARESH.G
 * Date: 25-05-2016
 * Time: 06:43 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Source wise report
 * @category        Controller
 * @author          Parameshwar
 */
class StudentAttendance extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("StudentAttendance_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->helper('encryption');
    }
    function getStudents_get(){
        $classId=$this->get('classId');
        $params['classId']=$classId;
        $res=$this->StudentAttendance_model->getStudents($params);
        $this->set_response($res);
    }
    function getOldStudents_get(){
        $classId=$_SERVER['HTTP_CLASSID'];
        $searchVal=$this->get('searchVal');
        $params['classId']=$classId;
        $params['searchVal']=$searchVal;
        $res=$this->StudentAttendance_model->getOldStudents($params);
        $this->set_response($res);
    }
    function saveAttendance_post(){
        $params['classId']=trim($this->post('classId'));
        $params['attendedLeads']=trim($this->post('attended'));
        $params['absentedLeads']=trim($this->post('absented'));
        $params['specialattendedLeads']=trim($this->post('specialattended'));
        $params['guestattendedLeads']=trim($this->post('guestattended'));
        //print_r($params);exit;
        $res=$this->StudentAttendance_model->saveAttendance($params);
        $this->set_response($res);
    }
}