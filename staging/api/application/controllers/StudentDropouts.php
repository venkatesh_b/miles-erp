<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Student Dropouts report
 * @category        Controller
 * @author          Parameshwar
 */
class StudentDropouts extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("StudentDropouts_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    public function getAllConfigurations_get() {
        $message='Student Dropouts';
        $status=true;
        $branch = [];
        $course = [];
        $company = [];
        $feehead = [];
        $currentbatches=[];
        $previousbatches=[];
        $branchList = $this->StudentDropouts_model->getBranchListByUserId($_SERVER['HTTP_USER']);
        if(count($branchList) >0 ){
            foreach ($branchList as $key => $value) {
                $branch[$value['id']] = (object)$value;
                $temp = $this->StudentDropouts_model->getAllBranchCoursesList(array('branchId'=>$value['id']))['data'];
                if(count($temp) > 0){
                    foreach ($temp as $k => $v) {
                        $course[$v->courseId] = $v;

                    }
                }else {
                    $course = [];
                }

                $currentBatchList = $this->StudentDropouts_model->getCurrentBatchesList(array('branchId'=>$value['id']))['data'];
                if(count($currentBatchList) >0 ) {
                    foreach ($currentBatchList as $keycurrentBatch => $valuecurrentBatch) {
                        $currentbatches[] = (object)$valuecurrentBatch;
                    }
                }

                $previousBatchList = $this->StudentDropouts_model->getPreviousBatchesList(array('branchId'=>$value['id']))['data'];
                if(count($previousBatchList) >0 ) {
                    foreach ($previousBatchList as $keypreviousBatch => $valuepreviousBatch) {
                        $previousbatches[] = (object)$valuepreviousBatch;
                    }
                }


            }
        } else {
            $branch = [];
        }

        $data=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>array(
                'branch' => array_values($branch),
                'course' => array_values($course),
                'currentbatches' => array_values($currentbatches),
                'previousbatches' => array_values($previousbatches),
            )
        );
        $this->set_response($data);
    }
    public function getAllBatchesConfigurations_get() {
        $branchId=$this->get('branchId');
        $courseId=$this->get('courseId');
        $is_current=$this->get('is_current');
        $is_previous=$this->get('is_previous');
        $currentBatchList = $this->StudentDropouts_model->getCurrentBatchesList(array('branchId'=>$branchId,'courseId'=>$courseId))['data'];
        if($is_current==1) {
            if (count($currentBatchList) > 0) {
                foreach ($currentBatchList as $keycurrentBatch => $valuecurrentBatch) {
                    $currentbatches[] = (object)$valuecurrentBatch;
                }
            }
        }
        else{
            $currentbatches=[];
        }
        if($is_previous==1) {
            $previousBatchList = $this->StudentDropouts_model->getPreviousBatchesList(array('branchId' => $branchId, 'courseId' => $courseId))['data'];
            if (count($previousBatchList) > 0) {
                foreach ($previousBatchList as $keypreviousBatch => $valuepreviousBatch) {
                    $previousbatches[] = (object)$valuepreviousBatch;
                }
            }
        }
        else{
            $previousbatches=[];
        }
        $data=array(
            "status"=>true,
            "message"=>'success',
            "data"=>array(
                'currentbatches' => array_values($currentbatches),
                'previousbatches' => array_values($previousbatches),
            )
        );
        $this->set_response($data);

    }

    public function getStudentDropoutsReport_get() {
        $message='Student Dropouts';
        $status=true;
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";
        $branch = strlen(trim($this->get('branch')))>0?"'".trim($this->get('branch'))."'":'NULL';
        $course = strlen(trim($this->get('course')))>0?"'".trim($this->get('course'))."'":'NULL';
        //$batch = strlen(trim($this->get('batch')))>0?"'".trim($this->get('batch'))."'":'NULL';

        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0 && strlen(trim($this->get('batch')))<=0){
            $status=false;
            $message = 'Select any filter';
            $res = [];
        }else{
            //$res = $this->StudentDropouts_model->getStudentDropoutsReport($branch, $course, $batch);
            $res = $this->StudentDropouts_model->getStudentDropoutsReport($branch, $course, $from_date, $to_date);
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$res['data']);
        $this->set_response($data);
    }
    function getkey($pos){

        return chr(65+$pos);

    }
    public function getStudentDropoutsReportExport_get()
    {
        $message='Student Dropouts';
        $status = true;
        $this->load->library('excel');
        $from_date = DateTime::createFromFormat('d M, Y', $this->get('fromDate'));
        $from_date = "'".$from_date->format("Y-m-d")."'";
        $to_date = DateTime::createFromFormat('d M, Y', $this->get('toDate'));
        $to_date = "'".$to_date->format("Y-m-d")."'";
        $branch = strlen(trim($this->get('branch'))) > 0 ? "'" . trim($this->get('branch')) . "'" : 'NULL';
        $course = strlen(trim($this->get('course'))) > 0 ? "'" . trim($this->get('course')) . "'" : 'NULL';
        //$batch = strlen(trim($this->get('batch')))>0?"'".trim($this->get('batch'))."'":'NULL';
        if(strlen(trim($this->get('branch')))<=0 && strlen(trim($this->get('course')))<=0){
            $status = false;
            $message = 'Select any filter';
            $res = [];
        } else {
            $res = $this->StudentDropouts_model->getStudentDropoutsReport($branch, $course, $from_date, $to_date);
        }
        if($res['status']===false){
            $data = array("status" => false, "message" => $res['message'], "data" => array());
        }
        else {
            $header_main = '';
            $datarows = array();
            $headers=array('Branch','Enroll No.','Name','Course','Batch','Payable','Paid','Due From');
            $data_main=array();
            foreach($res['data'] as $kd=>$vd){
                $data_main[]=implode('||',array('branchName'=>$vd->branchName,'enrollNo'=>$vd->enrollNo,'leadName'=>$vd->leadName,'courseName'=>$vd->courseName,'batchCode'=>$vd->batchCode,'amountPayable'=>$vd->amountPayable,'amountPaid'=>$vd->amountPaid,'dueDays'=>$vd->dueDays));
            }
            $header_main = implode('||', $headers);
            $header = $header_main;
            $headervals = explode('||', $header);

            $excelRowstartsfrom=3;
            $excelColumnstartsFrom=2;
            $excelstartsfrom=$excelRowstartsfrom;
            $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'STUDENT DROPOUTS');

            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );

            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Branches");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('branchText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel2 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel2);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Courses");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('courseText'));
            $mrgesel2_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Batch Dates");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, ('From '.$this->get('fromDate').' To '.$this->get('toDate')));
            $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Report on");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
            $excelstartsfrom=$excelstartsfrom+1;
            $head_starts_from = $excelstartsfrom+1;
            for ($k = 0; $k < count($headervals); $k++) {
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from, $headervals[$k]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );

            }
            $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
            $rowsStratsFrom = $head_starts_from + 1;
            $c = $rowsStratsFrom;
            for ($k = 0; $k < count($data_main); $k++) {
                $datavals = explode('||', $data_main[$k]);

                for ($j = 0; $j <=count($datavals)-1; $j++) {
                       $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, $datavals[$j]);
                    if($j!=7){
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, $datavals[$j]);
                    }
                    if($j==7){
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom+$j) . $c, abs($datavals[$j]).' Days');
                        $txtColor='000000';
                        if($datavals[$j]<0){
                            $txtColor='FF0000';
                        }
                        else if($datavals[$j]==0){
                            $txtColor='FFA500';
                        }
                        else if($datavals[$j]>0){
                            $txtColor='00FF00';
                        }

                        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$j) . $c)->applyFromArray(
                            array(
                                'alignment' => array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                ),
                                'font'  => array(
                                    'bold'  => true,
                                    'color' => array('rgb' => $txtColor)
                                )
                            )
                        );


                    }
                }
                $c++;
            }
            //exit;

            $footer_starts_from=$c+2;

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from,'SIGNATURES');

            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => '04578C')
                    )
                )
            );

            $footer_starts_from=$footer_starts_from+1;


            $test=(int)((count($headervals)-1)/2);

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$test) . ($footer_starts_from+1);
            $this->excel->getActiveSheet()->mergeCells($mrgesel);

            $mrgesel = $this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . ($footer_starts_from+1);
            $this->excel->getActiveSheet()->mergeCells($mrgesel);

            $footer_starts_from=$footer_starts_from+2;

            $mrgesel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+$test) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from,'Accountant / Cashier');

            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $footer_starts_from)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'a9aaae')
                    )
                )
            );




            $mrgesel = $this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $footer_starts_from;
            $this->excel->getActiveSheet()->mergeCells($mrgesel);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from,'Branch Head');

            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$test+1) . $footer_starts_from)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'a9aaae')
                    )
                )
            );

            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);

            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('STUDENT DROPOUTS');
            $filename = "StudentDropouts_".date('Ymd_His') . '.xls'; //save our workbook as this file name
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

            //force user to download the Excel file without writing it to server's HD
            $file_path = './uploads/' . $filename;
            $objWriter->save($file_path);
            $file_path = base_url().'uploads/' . $filename;
            $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
        }
        $this->set_response($data);
    }

    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}