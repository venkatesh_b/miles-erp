<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * @controller Name Branch Visit
 * @category        Controller
 * @author          Abhilash
 */
class Students extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Students_model");
        $this->load->model("Survey_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    public function getAllConfigurations_get() {
        $message='Student Details';
        $status=true;
        $branch = [];
        $course = [];
        $company = [];
        $feehead = [];
        $currentbatches=[];
        $previousbatches=[];
        $branchList = $this->Students_model->getBranchListByUserId($_SERVER['HTTP_USER']);
        if(count($branchList) >0 ){
            foreach ($branchList as $key => $value) {
                $branch[$value['id']] = (object)$value;
                $temp = $this->Students_model->getAllBranchCoursesList(array('branchId'=>$value['id']))['data'];
                if(count($temp) > 0){
                    foreach ($temp as $k => $v) {
                        $course[$v->courseId] = $v;

                    }
                }else {
                    $course = [];
                }

                $currentBatchList = $this->Students_model->getCurrentBatchesList(array('branchId'=>$value['id']))['data'];
                if(count($currentBatchList) >0 ) {
                    foreach ($currentBatchList as $keycurrentBatch => $valuecurrentBatch) {
                        $currentbatches[] = (object)$valuecurrentBatch;
                    }
                }

                $previousBatchList = $this->Students_model->getPreviousBatchesList(array('branchId'=>$value['id']))['data'];
                if(count($previousBatchList) >0 ) {
                    foreach ($previousBatchList as $keypreviousBatch => $valuepreviousBatch) {
                        $previousbatches[] = (object)$valuepreviousBatch;
                    }
                }


            }
        } else {
            $branch = [];
        }

        $data=array(
            "status"=>$status,
            "message"=>$message,
            "data"=>array(
                'branch' => array_values($branch),
                'course' => array_values($course),
                'currentbatches' => array_values($currentbatches),
                'previousbatches' => array_values($previousbatches),
            )
        );
        $this->set_response($data);
    }
    public function getAllBatchesConfigurations_get() {
        $branchId=$this->get('branchId');
        $courseId=$this->get('courseId');
        $is_current=$this->get('is_current');
        $is_previous=$this->get('is_previous');
        $currentBatchList = $this->Students_model->getCurrentBatchesList(array('branchId'=>$branchId,'courseId'=>$courseId))['data'];
        if($is_current==1) {
            if (count($currentBatchList) > 0) {
                foreach ($currentBatchList as $keycurrentBatch => $valuecurrentBatch) {
                    $currentbatches[] = (object)$valuecurrentBatch;
                }
            }
        }
        else{
            $currentbatches=[];
        }
        if($is_previous==1) {
            $previousBatchList = $this->Students_model->getPreviousBatchesList(array('branchId' => $branchId, 'courseId' => $courseId))['data'];
            if (count($previousBatchList) > 0) {
                foreach ($previousBatchList as $keypreviousBatch => $valuepreviousBatch) {
                    $previousbatches[] = (object)$valuepreviousBatch;
                }
            }
        }
        else{
            $previousbatches=[];
        }
        $data=array(
            "status"=>true,
            "message"=>'success',
            "data"=>array(
                'currentbatches' => array_values($currentbatches),
                'previousbatches' => array_values($previousbatches),
            )
        );
        $this->set_response($data);

    }

    /**
     * Function to get student list
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getStudentsList_get() {
        $message='Student Details';
        $status=true;

        $branch = strlen(trim($this->get('branch')))>0?"".trim($this->get('branch'))."":'NULL';
        
        $res = $this->Students_model->getStudentsList($branch);

        $this->response($res);

    }
    /**
     * Function get key number for excel
     * 
     * @return - array
     */
    function getkey($pos){

        return chr(65+$pos);

    }
    /**
     * Function create and get excel
     * 
     * @return - string
     */
    public function getStudentsListExcel_get() {
        $message = 'Student Details';
        $status = true;
        $this->load->library('excel');
        $branch = strlen(trim($this->get('branch')))>0?"".trim($this->get('branch'))."":'NULL';
        
        $res = $this->Students_model->getStudentsListExcel($branch);
        if($res===false){
            $data = array("status" => false, "message" => "No Records Found", "data" => array());
        }
        else {
            $header_main = '';
            $datarows = array();
            $headers=array('Name','Enroll No.','Branch','Course','Batch','Level','Info');
            $data_main=array();
            
            foreach($res as $kd=>$vd){
                $data_main[]=implode('||',array('branchName'=>$vd->lead_name,'enrollNo'=>$vd->lead_number,'leadName'=>$vd->branch_name,'courseName'=>$vd->course_name,'batchCode'=>$vd->batch_code,'level'=>$vd->level,'info'=>$vd->info));
            }
            $header_main = implode('||', $headers);
            $header = $header_main;
            $headervals = explode('||', $header);

            $excelRowstartsfrom=3;
            $excelColumnstartsFrom=2;
            $excelstartsfrom=$excelRowstartsfrom;
            $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'STUDENTS');

            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );

            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Branches");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('branchText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            /*$mrgesel2 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel2);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Courses");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('courseText'));
            $mrgesel2_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;*/

            /*$mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Batches");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('batchText'));
            $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;*/



            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Report on");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
            $excelstartsfrom=$excelstartsfrom+1;
            $head_starts_from = $excelstartsfrom+1;
            for ($k = 0; $k < count($headervals); $k++) {
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from, $headervals[$k]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );

            }
            $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
            $rowsStratsFrom = $head_starts_from + 1;
            $c = $rowsStratsFrom;
            for ($k = 0; $k < count($data_main); $k++) {
                $datavals = explode('||', $data_main[$k]);

                for ($j = 0; $j <=count($datavals)-1; $j++) {
                        if($j!=6) {
                            $this->excel->setActiveSheetIndex(0)
                                ->setCellValue($this->getkey($excelColumnstartsFrom + $j) . $c, $datavals[$j]);
                        }
                    if($j==6){
                        $info='';
                        $iExplode=explode(',',$datavals[$j]);
                        if(!empty($iExplode[0]) && trim($iExplode[0])!='' && ($iExplode[0]!=0 || $iExplode[0]!='')){
                            $info.="Email : ".$iExplode[0]."\n";
                        }
                        if(!empty($iExplode[1]) && trim($iExplode[1])!='' && ($iExplode[1]!=0 || $iExplode[1]!='')){
                            $info.="Phone : ".$iExplode[1]."\n";
                        }
                        if(!empty($iExplode[2]) && trim($iExplode[2])!='' && ($iExplode[2]!=0 || $iExplode[2]!='')){
                            $info.="Qualification : ".$iExplode[2]."\n";
                        }
                        if(!empty($iExplode[3]) && trim($iExplode[3])!='' && ($iExplode[3]!=0 || $iExplode[3]!='')){
                            $info.="Company : ".$iExplode[3]."\n";
                        }
                        if(!empty($iExplode[4]) && trim($iExplode[4])!='' && ($iExplode[4]!=0 || $iExplode[4]!='')){
                            $info.="Institution : ".$iExplode[4]."";
                        }

                       //exit;
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom + $j) . $c, $info);
                        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom + $j) . $c)
                            ->getAlignment()->setWrapText(true);
                    }
                 }
                $c++;
            }
            //exit;







            //$footersel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $footer_starts_from;
            //$this->excel->getActiveSheet()->getStyle($footersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            //$this->excel->getActiveSheet()->getStyle($footersel)->getFill()->getStartColor()->setRGB('CCCCCC');



            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);

            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('STUDENTS');
            $filename = "Students_".date('Ymd_His') . '.xls'; //save our workbook as this file name
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

            //force user to download the Excel file without writing it to server's HD
            $file_path = './uploads/' . $filename;
            $objWriter->save($file_path);
            $file_path = base_url().'uploads/' . $filename;
            $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
        }
        $this->set_response($data);
    }
    
    /**
     * Function get Student Fee Details
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getStudentFeeDetails_get() {
        $leadId=$this->get('lead-id');

        $message = "Student Fee Details";
        $status = true;
        $result = [];
        $result['OverallDetails']=array();
        $result['ReceiptDetails']=array();
        if(is_numeric($leadId)){ $leadId = encode($leadId); }
        $leadId = decode($leadId);
        $branch_xref_lead_id='';
        if(!empty($this->get('branch_xref_lead_id'))){
            $branch_xref_lead_id=decode($this->get('branch_xref_lead_id'));
        }
        $details = $this->Students_model->getStudentFeeDetails($leadId,$branch_xref_lead_id);
        $detailsReceipt = $this->Students_model->getStudentFeeReceiptDetails($leadId,$branch_xref_lead_id);
//        print_r($details);die;
        if ($details == 'nodata') {
        } else if (isset($details['error']) && $details['error'] == 'dberror') {

        } else if ($details) {

            $result['OverallDetails'] = $details;
        } else {
            /* No data found */

        }

        if ($detailsReceipt == 'nodata') {
        } else if (isset($detailsReceipt['error']) && $detailsReceipt['error'] == 'dberror') {

        } else if ($detailsReceipt) {

            $result['ReceiptDetails'] = $detailsReceipt;
        } else {
            /* No data found */

        }
        
        $data = array("status" => $status, "message" => $message, "data" => $result);
        $this->set_response($data);
    }

    /**
     * Function to get student list(Exam status = Cleared All Alumni)
     *
     * @created date - 25 Jan 2017
     * @return - array
     * @author : Abhilash
     */
    public function getClearedAlumniLeadList_get() {
        $message='Student Details';
        $status=true;
        $data = [];

        $data['branch'] = strlen(trim($this->get('branch')))>0?"".trim($this->get('branch'))."":'NULL';

        $res = $this->Students_model->getClearedAlumniLeadList($data);

        $this->response($res);
    }

    /**
     * Function to get student excel(Exam status = Cleared All Alumni)
     *
     * @created date - 25 Jan 2017
     * @return - string
     */
    public function getClearedAlumniLeadListExcel_get() {
        $message = 'Student Details';
        $status = true;
        $this->load->library('excel');
        $branch = strlen(trim($this->get('branch')))>0?"".trim($this->get('branch'))."":'NULL';

        $res = $this->Students_model->getClearedAlumniLeadListExcel($branch);
        if($res===false){
            $data = array("status" => false, "message" => "No Records Found", "data" => array());
        }
        else {
            $header_main = '';
            $datarows = array();
            $headers=array('Name','Enroll No.','Branch','Course','Batch','Level','Info');
            $data_main=array();

            foreach($res as $kd=>$vd){
                $data_main[]=implode('||',array('branchName'=>$vd->lead_name,'enrollNo'=>$vd->lead_number,'leadName'=>$vd->branch_name,'courseName'=>$vd->course_name,'batchCode'=>$vd->batch_code,'level'=>$vd->level,'info'=>$vd->info));
            }
            $header_main = implode('||', $headers);
            $header = $header_main;
            $headervals = explode('||', $header);

            $excelRowstartsfrom=3;
            $excelColumnstartsFrom=2;
            $excelstartsfrom=$excelRowstartsfrom;
            $mrgesel1 = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, 'STUDENTS');

            $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+0))->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom)->applyFromArray(
                array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ),
                    'font'  => array(
                        'bold'  => true,
                        'color' => array('rgb' => 'FFFFFF')
                    )
                )
            );

            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1)->getFill()->getStartColor()->setRGB('01588e');

            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;
            $excelstartsfrom=$excelstartsfrom+1;

            $mrgesel1 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel1);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Branches");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('branchText'));
            $mrgesel1_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel1_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;

            /*$mrgesel2 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel2);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Courses");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('courseText'));
            $mrgesel2_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel2_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;*/

            /*$mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Batches");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, $this->get('batchText'));
            $mrgesel3_total = $this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($mrgesel3_total)->getFill()->getStartColor()->setRGB('C2F7D8');
            $excelstartsfrom=$excelstartsfrom+1;*/



            $mrgesel3 = $this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom.':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $excelstartsfrom;
            $this->excel->getActiveSheet()->mergeCells($mrgesel3);
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+0) . $excelstartsfrom, "Report on");
            $this->excel->setActiveSheetIndex(0)
                ->setCellValue($this->getkey($excelColumnstartsFrom+1) . $excelstartsfrom, date('d M, Y'));
            $excelstartsfrom=$excelstartsfrom+1;
            $head_starts_from = $excelstartsfrom+1;
            for ($k = 0; $k < count($headervals); $k++) {
                $this->excel->setActiveSheetIndex(0)
                    ->setCellValue($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from, $headervals[$k]);
                $this->excel->getActiveSheet()->getColumnDimension($this->getkey($excelColumnstartsFrom+$k))->setAutoSize(true);
                $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom+$k) . $head_starts_from)->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        )
                    )
                );

            }
            $headersel = $this->getkey($excelColumnstartsFrom+0) . $head_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $head_starts_from;
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $this->excel->getActiveSheet()->getStyle($headersel)->getFill()->getStartColor()->setARGB('D4E4F3FF');
            $rowsStratsFrom = $head_starts_from + 1;
            $c = $rowsStratsFrom;
            for ($k = 0; $k < count($data_main); $k++) {
                $datavals = explode('||', $data_main[$k]);

                for ($j = 0; $j <=count($datavals)-1; $j++) {
                    if($j!=6) {
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom + $j) . $c, $datavals[$j]);
                    }
                    if($j==6){
                        $info='';
                        $iExplode=explode(',',$datavals[$j]);
                        if(!empty($iExplode[0]) && trim($iExplode[0])!='' && ($iExplode[0]!=0 || $iExplode[0]!='')){
                            $info.="Email : ".$iExplode[0]."\n";
                        }
                        if(!empty($iExplode[1]) && trim($iExplode[1])!='' && ($iExplode[1]!=0 || $iExplode[1]!='')){
                            $info.="Phone : ".$iExplode[1]."\n";
                        }
                        if(!empty($iExplode[2]) && trim($iExplode[2])!='' && ($iExplode[2]!=0 || $iExplode[2]!='')){
                            $info.="Qualification : ".$iExplode[2]."\n";
                        }
                        if(!empty($iExplode[3]) && trim($iExplode[3])!='' && ($iExplode[3]!=0 || $iExplode[3]!='')){
                            $info.="Company : ".$iExplode[3]."\n";
                        }
                        if(!empty($iExplode[4]) && trim($iExplode[4])!='' && ($iExplode[4]!=0 || $iExplode[4]!='')){
                            $info.="Institution : ".$iExplode[4]."";
                        }

                        //exit;
                        $this->excel->setActiveSheetIndex(0)
                            ->setCellValue($this->getkey($excelColumnstartsFrom + $j) . $c, $info);
                        $this->excel->getActiveSheet()->getStyle($this->getkey($excelColumnstartsFrom + $j) . $c)
                            ->getAlignment()->setWrapText(true);
                    }
                }
                $c++;
            }
            //exit;

            //$footersel = $this->getkey($excelColumnstartsFrom+0) . $footer_starts_from . ':' . $this->getkey($excelColumnstartsFrom+count($headervals) - 1) . $footer_starts_from;
            //$this->excel->getActiveSheet()->getStyle($footersel)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            //$this->excel->getActiveSheet()->getStyle($footersel)->getFill()->getStartColor()->setRGB('CCCCCC');



            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);

            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('STUDENTS');
            $filename = "Students_".date('Ymd_His') . '.xls'; //save our workbook as this file name
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format

            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

            //force user to download the Excel file without writing it to server's HD
            $file_path = './uploads/' . $filename;
            $objWriter->save($file_path);
            $file_path = base_url().'uploads/' . $filename;
            $data = array("status" => true, "message" => 'success', "data" => $file_path,"file_name"=>$filename);
        }
        $this->set_response($data);
    }

    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}