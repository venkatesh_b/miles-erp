<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * @controller Name User
 * @category        Controller
 * @author          Abhilash
 * @Description     For user/employee CRUD Service
 */
class User extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("User_model");
        $this->load->library('form_validation');
        $this->load->library('MY_Form_Validation');
        $this->load->library('oauth/oauth','','oauth');
        $this->load->library('Put_method_extenstion');
        $this->load->library('SSP');
        $this->load->helper('encryption');

    }
    public function getUsersList_get()
    {
        $this->response($this->User_model->usersListDataTables(),REST_Controller::HTTP_OK);
        exit;
    }
    
    public function getSecondaryCounselorList_get() {
        $status=true;$message="";$results='';
        
        $id = $this->get('branch_id');
        if(!is_numeric($id))
        {
            $id = decode($id);
        }
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        }
        else
        {
            $result = $this->User_model->getSecondaryCounselorList($id);
            if (isset($result['error']) && $result['error'] == 'dberror')
            {
                $message='Database error';
                $status=false;
                $results = $result['msg'];
            }
            else if ($result)
            {
                if(count($result)>0)
                {
                    $message='User List';
                    $status=true;
                    $results = $result;
                }
                else
                {
                    $message='No Counselor found';
                    $status=false;
                    $results = [];
                }
            }
            else
            {
                $message='No Counselor found';
                $status=true;
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }
    
    public function getUserList_get()
    {
        $status=true;$message="";$results='';
        $result = $this->User_model->getAllUser();
        if (isset($result['error']) && $result['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $results = $result['msg'];
        }
        else if ($result)
        {
            if(count($result)>0)
            {
                $message='User List';
                $status=true;
                $results = $result;
            }
            else
            {
                $message='User List';
                $status=false;
                $results = [];
            }
        }
        else
        {
            $message='No Data found';
            $status=true;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    public function getSingleUser_get()
    {
        $status=true;$message="";$results='';
        $id = $this->get('user_id');

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "ID", 'required|callback_num_check');

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        }
        else
        {
            $result = $this->User_model->getSingleUser($id);
            if (isset($result['error']) && $result['error'] == 'dberror')
            {
                $message='Database error';
                $status=false;
                $results = $result['msg'];
            }
            else if($result == "badrequest")
            {
                $message='No User found';
                $status=false;
            }
            else if($result == "badrequest")
            {
                $message='No User found';
                $status=false;
            }            
            else if ($result)
            {
                if(count($result)>0)
                {
                    $message='User Details';
                    $status=true;
                    $results = $result;
                }
                else
                {
                    $message='User Details';
                    $status=true;
                    $results = array();
                }
            }
            else
            {
                $message='No Data found';
                $status=true;
            }
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    public function addUser_post()
    {
        $status=true;$message="";$results='';
        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|callback_name_check'
            ),
            array(
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'required|regex_match[/^[0-9() +-]{8,15}$/]'
            ),
            array(
                'field' => 'sCounselor',
                'label' => 'Secondary Counselor',
                'rules' => 'regex_match[/^[01]+$/]',
                "errors"=>array('regex_match' => '%s is invalid')
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'employee_code',
                'label' => 'Employee code',
                'rules' => 'required|regex_match[/^[A-Za-z0-9]+$/]|is_unique[`employee`.`employee_code`]',
                "errors"=>array('required' => 'Employee code should not be blank','regex_match' => 'Employee code should be alphanumeric only','is_unique'=>'Employee code already exists.')
            ),
            array(
                'field' => 'image',
                'label' => 'Image',
                'rules' => ''
            ),
            array(
                'field' => 'dob',
                'label' => 'Date Of Birth',
                'rules' => ''
            ),
            array(
                'field' => 'join',
                'label' => 'Joining Date',
                'rules' => 'required'
            ),
            array(
                'field' => 'sex',
                'label' => 'Gender',
                'rules' => 'required'
            ),
            array(
                'field' => 'role',
                'label' => 'role',
                'rules' => 'required'
            ),
            array(
                'field' => 'branch',
                'label' => 'Branch',
                'rules' => 'required'
            )
        );

        /**/
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
//        $data = $this->input->post();
        /**/

        $this->form_validation->set_rules($config);
        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        }
        else
        {
            if(trim($this->input->post('dob'))!='')
            {
                $dob = DateTime::createFromFormat('d M, Y', $this->post('dob'));
                $dob = $dob->format("Y-m-d");
            }
            else
            {
                $dob='';
            }
            $userData = array(
                'email' => $this->input->post('email'),
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'sex' => $this->input->post('sex'),
                'dob' => $dob,
                'join' => $this->input->post('join'),
                'image' => $this->input->post('image'),
                'role' => decode($this->input->post('role')),
                'sCounselor' => $this->input->post('sCounselor'),
                'branch' => $this->input->post('branch'),
                'employee_code' => $this->input->post('employee_code')
            );
            $isUserCreated = $this->User_model->createUser($userData);

            if($isUserCreated === 'nobranch')
            {
                $message="Branch is incorrect";
                $status=false;
                $results=array("branch"=>"Incorrect Branch");
            }
            else if($isUserCreated === 'norolefound')
            {
                $message="Role is in-correct";
                $status=false;
                $results=array("txtUserRole"=>"Incorrect Role");
//                $results=array();
            }
            else if (isset($isUserCreated['error']) && $isUserCreated['error'] == 'dberror')
            {
                $message="Database Error";
                $status=false;
                $results=$isUserCreated['msg'];
                
            }
            else if($isUserCreated === 'duplicate')
            {
                $message="This email is already registered";
                $status=false;
                $results=array("email"=>"This email is already registered");
            }
            else if ($isUserCreated)
            {
                $message="User Created";
                $status=true;
            }
            else
            {
                $message="Database Error";
                $status=false;
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    public function deleteUser_delete()
    {
        $status=true;$message="";$results="";
        $id = $this->get('user_id');
        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "Id", 'required|callback_num_check');
        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        }
        else
        {
            $result = $this->User_model->changeStatusUser($id);
            if (isset($result['error']) && $result['error'] == 'dberror')
            {
                $message="Database Error";
                $status=false;
                $results=$result['msg'];
            }
            else if ($result === 'active')
            {
                $message="User Activated";
                $status=true;
            }
            else if ($result === 'deactivated')
            {
                $message="User De-Activated";
                $status=true;
            }
            else if ($result === 'nodata')
            {
                $message="Id not found";
                $status=false;
            }
            else
            {
                $message="User can't be deleted";
                $status=false;
            }
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    public function editUser_put()
    {
        $status=true;$message="";$results="";
        $id = $this->get('user_id');
        $this->form_validation->set_data(array('id' => $this->get('id')));
        $this->form_validation->set_rules('id', "ID", 'numeric');

        if ($this->form_validation->run() == FALSE)
        {
            $message="Validation Error";
            $status=false;
            $results=$this->form_validation->error_array();
        }
        else
        {
            $this->put = file_get_contents('php://input');
            //parse and clean request
            $this->put_method_extenstion->_parse_request($this->put);
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required|callback_name_check'
                ),
                array(
                    'field' => 'phone',
                    'label' => 'Phone',
                    'rules' => 'required|regex_match[/^[0-9]*$/]'
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required|valid_email'
//                    'rules' => 'required|valid_email|is_unique[`user`.`login_id`]'
                ),
                array(
                    'field' => 'employee_code',
                    'label' => 'Employee code',
//                    'rules' => 'required|regex_match[/^[A-Za-z0-9]+$/]|is_unique[`employee`.`employee_code`]',
                    'rules' => 'required|regex_match[/^[A-Za-z0-9]+$/]',
                    "errors"=>array('required' => 'Employee code should not be blank','regex_match' => 'Employee code should be alphanumeric only','is_unique'=>'Employee code already exists.')
                ),
                array(
                    'field' => 'image',
                    'label' => 'Image',
                    'rules' => ''
                ),
                array(
                    'field' => 'dob',
                    'label' => 'Date Of Birth',
                    'rules' => ''
                ),
                array(
                    'field' => 'join',
                    'label' => 'Joining Date',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'sex',
                    'label' => 'Gender',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'role',
                    'label' => 'role',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'branch',
                    'label' => 'Branch',
                    'rules' => 'required'
                )
            );
//            print_r($this->put());die;
            $data = [];
            foreach ($this->put() as $key => $value) {
                if($key == 'role')
                {
                    $data[$key] = decode($value);
                }
                else
                {
                    $data[$key] = $value;
                }
            }
            
            $this->form_validation->set_rules($config);
            $this->form_validation->set_data($data);

            if ($this->form_validation->run() == FALSE)
            {
                $message="Validation Error";
                $status=false;
                $results=$this->form_validation->error_array();
            }
            else
            {
                if(!empty($this->put('dob')) && trim($this->put('dob'))!='') {
                    $dob = DateTime::createFromFormat('d M, Y', $this->put('dob'));
                    $dob = $dob->format("Y-m-d");
                }
                else{
                    $dob='';
                }
                $data['dob']=$dob;
                $userData = array(
                    'email' => $data['email'],
                    'name' => $data['name'],
                    'phone' => $data['phone'],
                    'sex' => $data['sex'],
                    'dob' => $dob,
                    'role' => $data['role'],
                    'branch' => $data['branch'],
                    'employee_code' => $data['employee_code']
                );
                //print_r($userData);
                //exit;
                
                $isUpdated = $this->User_model->updateUser($id, $data);

                if($isUpdated === 'nobranch')
                {
                    $message="Branch is incorrect";
                    $status=false;
                    $results=array("db_error"=>"Incorrect Branch");
                }
                else if($isUpdated === 'norolefound')
                {
                    $message="Role is in-correct";
                    $status=false;
                    $results=array("db_error"=>"Incorrect Role");
                }
                else if($isUpdated === 'badrequest')
                {
                    $message="User not found";
                    $status=false;
                    $results=array("db_error"=>"User not found");
                }
                else if (isset($isUpdated['error']) && $isUpdated['error'] == 'dberror')
                {
                    $message="Database Error";
                    $status=false;
                    $results=array("db_error"=>"Profile Updation failed");
                }
                else if($isUpdated === 'duplicate')
                {
                    $message="This email is already registered";
                    $status=false;
                    $results=array("email"=>"This email is already registered");
                }
                else if($isUpdated === 'duplicate employee code')
                {
                    $message="Employee code already exists.";
                    $status=false;
                    $results=array("employee_code"=>"Employee code already exists.");
                }
                
                else if ($isUpdated)
                {
                    $message="Profile Updated";
                    $status=true;
                    $releaseCalls=array('assignedUserId'=>$id,'releaseBy'=>$_SERVER['HTTP_USER']);
                    $this->load->model("CallSchedule_model");
                    $this->CallSchedule_model->releaseCallScheduleByUser($releaseCalls);
                    $this->load->model("MWBCallSchedule_model");
                    $this->MWBCallSchedule_model->releaseCallScheduleByUser($releaseCalls);
                }
                else
                {
                    $message="Data can't be updated";
                    $status=false;
                    $results=array("employee_code"=>"Profile Updation failed");
                }
            }
        }

        $data=array("status"=>$status,"message"=>$message,"data"=>$results);
        $this->set_response($data);
    }

    function updateUserStatus_post(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }

        $param['userID']=$this->post('userID');
        $param['toStatus']=$this->post('toStatus');

        $res=$this->User_model->updateUserStatus($param);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        if($res['status']===true){
            if($param['toStatus']==0){
                $releaseCalls=array('assignedUserId'=>$param['userID'],'releaseBy'=>$_SERVER['HTTP_USER']);
                $this->load->model("CallSchedule_model");
                $this->CallSchedule_model->releaseCallScheduleByUser($releaseCalls);
                $this->load->model("MWBCallSchedule_model");
                $this->MWBCallSchedule_model->releaseCallScheduleByUser($releaseCalls);
            }
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    function updateUserDefaultBranch_post(){
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }

        $param['branchId']=$this->post('branchId');
        $param['userId']=$_SERVER['HTTP_USER'];
        $res=$this->User_model->updateUserDefaultBranch($param);
        $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
        ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

    /**
     * Function Name - result_set
     * @param type String, $status EX - 200, 400,.. etc
     * @param type String, $msg Ex - DB error, validation error message, Date rendered sucessfully, etc.
     * @param type - String/Array
     *
     * #auther - Abhilash
     *
     * #modified - 1 Feb 2016
     */
    public function result_set($status, $msg, $data)
    {
        $resp_array = ['200', '201'];
        if (in_array($status, $resp_array))
        {
            $statusMsg = 'TRUE';
        }
        else
        {
            $statusMsg = 'FALSE';
        }

        $this->response([
            'status' => $statusMsg,
            'message' => $msg,
            'data' => $data
        ], $status);
    }

    /**
     * Function for checking number(REGEX)
     *
     * @args
     *  "string"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str)
    {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str))
        {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /**
     * Function for checking valid string
     *
     * @args
     *  "str"(String) -> any string
     *
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function name_check($str)
    {
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str))
        {
            $this->form_validation->set_message('name_check', 'Accepts only alphabets and spaces');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
    /**
     * Validates that a Date is accessible. Also takes ports into consideration.
     *
     * @access  public
     * @param   string
     * @return  Boolean
     */
    function checkDateFormat($date)
    {
        if (preg_match("/[0-31]{2}\/[0-12]{2}\/[0-9]{4}/", $date))
        {
            if (checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))
                return true;
            else
                $this->form_validation->set_message('checkDateFormat', '(DD/MM/YYYY) The date you entered is not correctly formatted.');
            return FALSE;
        }
        else
        {
            $this->form_validation->set_message('checkDateFormat', '(DD/MM/YYYY) The date you entered is not correctly formatted.');
            return FALSE;
        }
    }

    /**
     * Validates phone is valid
     *
     * @access  public
     * @param   string
     * @return  Boolean
     */
    function checkPhone($phone)
    {
        /* Valid
         *  (+12) 99988 88123
         *  (+12) 999 8812
         *  (91)1234567890
         *  (91) 1234567890
         *  (91) 12345 67890
         *  (91) 0123-67812
         *  +91 01231 67812
         *  +91 01231-67812
         *  +91 0123167812
         */
        if (preg_match("/^\(?[+]?([0-9]{2,3})\)?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/", $phone))
        {
            return true;
        }
        else
        {
            $this->form_validation->set_message('checkPhone', '(+XX) XXXXX XXXXX OR (+XX) XXX XXXX The Phone Number you entered is not correctly formatted.');
            return FALSE;
        }
    }
}