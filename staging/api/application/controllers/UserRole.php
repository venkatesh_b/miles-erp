<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: V Parameshwar. Date: 03-02-2016
 * Purpose: Services for User role.
 * 
 */

class UserRole extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("UserRole_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data){
   
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
}
    function rolesList_get(){
        $allroles=$this->UserRole_model->rolelist();
        $final_response['response']=[
                'status' => $allroles['status'],
                'message' => $allroles['message'],
                'data' => $allroles['data'],
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        
       $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function roles_get(){
        /*
        $allroles=$this->UserRole_model->roles();
        $final_response['response']=[
                'status' => $allroles['status'],
                'message' => $allroles['message'],
                'data' => $allroles['data'],
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        */
        $this->response($this->UserRole_model->rolesDataTables());
    }
    function getRoleById_get(){
        
        $role_id=$this->get('roleId');
        $role_id=decode($role_id);
        $this->form_validation->set_data(array("roleId" => $role_id));

        $validations[]=["field"=>"roleId","label"=>'roleId',"rules"=>"required|integer","errors"=>array(
        'required' => 'Role ID should not be blank','integer' => 'Role should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            
            $resp_checkrole=$this->UserRole_model->checkrole($role_id);
            $final_response['response']=[
                'status' => $resp_checkrole['status'],
                'message' => $resp_checkrole['message'],
                'data' => $resp_checkrole['data'],
                ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addRole_post(){
        
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
       
        $role_name=trim($this->post('roleName'));
        $role_description=trim($this->post('roleDescription'));
        
        $validations[]=["field"=>"roleName","label"=>'roleName',"rules"=>"required|regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => 'Role Name should not be blank','regex_match' => 'Role Name should be alphanumeric only')];
        
        //$validations[]=["field"=>"roleDescription","label"=> 'Role Description ',"rules"=>"required","errors"=>array('required' => 'Role Description should not be blank')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();
            
            $this->load->model('References_model');
            $getType=$this->References_model->getReferenceType('User Role');
            
            $final_response['response']=[
                'status' => $getType['status'],
                'message' => $getType['message'],
                'data' => $getType['data'],
                ];
            
            if($getType['status']===true){
                
                foreach($getType['data'] as $k=>$v){
                    $reftype=$v->reference_type_id;
                }
                $data = array(
                    'reference_type_id' => $reftype ,
                    'reference_type_val' => $role_name ,
                    'reference_type_val_parent' => NULL,
                    'reference_type_is_active' => 1,
                    'reference_type_val_description' => $role_description  
                    );
                
                
                $createRoleRes=$this->References_model->AddReferenceValue($data);
                if($createRoleRes['status']===true){
                    $getMenuType=$this->References_model->getReferenceType('Menu'); 
                    $final_response['response']=[
                    'status' => $getMenuType['status'],
                    'message' => $getMenuType['message'],
                    'data' => $getMenuType['data'],
                    ];

                    if($getMenuType['status']===true){
                        foreach($getMenuType['data'] as $k=>$v){
                            $reftypeMenu=$v->reference_type_id;
                        }
                        
                        $getMenuValue=$this->References_model->getReferenceValues($reftypeMenu);
                        foreach($getMenuValue['data'] as $k_menu=>$v_menu){
                            $reftypeMenuId=$v_menu->reference_type_value_id;
                        }
                        
                        $data_assign_menu['roleId']=$createRoleRes['data']['ReferenceId'];
                        $data_assign_menu['menuId']=$reftypeMenuId;
                        $resp_assignmenu=$this->UserRole_model->assignMenu($data_assign_menu);
                        $final_response['response']=[
                        'status' => $resp_assignmenu['status'],
                        'message' => $resp_assignmenu['message'],
                        'data' => $resp_assignmenu['data'],
                        ];
                    }
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                    
                }
                else{
                    if($createRoleRes['status']===false && $createRoleRes['message']=='Already Exist'){
                        $createRoleRes['data']=array("roleName"=>"Already Exist");
                    }
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                $final_response['response']=[
                'status' => $createRoleRes['status'],
                'message' => $createRoleRes['message'],
                'data' => $createRoleRes['data'],
                ];
            }
        }
        else{
            
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    public function updateRole_post(){
        
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        
        $role_id=trim($this->post('roleId'));
        $role_id=decode($role_id);
        $role_name=trim($this->post('roleName'));
        $role_description=trim($this->post('roleDescription'));
        
        $validations[]=["field"=>"roleId","label"=>"roleId","rules"=>"required","errors"=>array(
        'required' => 'Role ID should not be blank','integer' => 'Role should be integer value only'
        )];
        
        $validations[]=["field"=>"roleName","label"=>'roleName',"rules"=>"required|regex_match[/^[A-Za-z0-9 ]+$/]","errors"=>array('required' => 'Role Name should not be blank','regex_match' => 'Role Name should be alphanumeric only')];
        
        //$validations[]=["field"=>"roleDescription","label"=> 'Role Description ',"rules"=>"required","errors"=>array('required' => 'Role Description should not be blank')];
        
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            
            $resp_checkrole=$this->UserRole_model->checkrole($role_id);
            if($resp_checkrole['status']===true){
                $data=array();
                $this->load->model('References_model');
                $getType=$this->References_model->getReferenceType('User Role');

                $final_response['response']=[
                    'status' => $getType['status'],
                    'message' => $getType['message'],
                    'data' => $getType['data'],
                    ];

                if($getType['status']===true){

                    foreach($getType['data'] as $k=>$v){
                        $reftype=$v->reference_type_id;
                    }

                    $data = array(
                        'reference_type_id' => $reftype ,
                        'reference_type_val' => $role_name ,
                        'reference_type_val_parent' => NULL,
                        'reference_type_is_active' => 1,
                        'reference_type_val_description' => $role_description,
                        'reference_type_val_id' => $role_id
                        );


                    $createRoleRes=$this->References_model->UpdateReferenceValue($data);
                    $final_response['response']=[
                    'status' => $createRoleRes['status'],
                    'message' => $createRoleRes['message'],
                    'data' => $createRoleRes['data'],
                    ];
                    if($createRoleRes['status']===true){
                        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                    }
                    else{
                        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                    }
                }
            }
            else{
                $final_response['response']=[
                    'status' => $resp_checkrole['status'],
                    'message' => $resp_checkrole['message'],
                    'data' => $resp_checkrole['data'],
                    ];
            }
            
        }
        else{
            
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    public function deleteRole_delete(){
        
        $role_id=$this->get('roleId');
        $role_id=decode($role_id);
        $this->form_validation->set_data(array("roleId" => $role_id));

        $validations[]=["field"=>"roleId","label"=>'roleId',"rules"=>"required|integer","errors"=>array(
        'required' => 'Role ID should not be blank','integer' => 'Role should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            
            $resp_checkrole=$this->UserRole_model->checkrole($role_id);
            if($resp_checkrole['status']===true){
                $this->load->model('References_model');
                $data['reference_type_val_id']=$role_id;
                $res_delete=$this->References_model->RemoveReferenceValue($data);
                $final_response['response']=[
                'status' => $res_delete['status'],
                'message' => $res_delete['message'],
                'data' => $res_delete['data'],
                ];
                if($res_delete['status']===true){
                    //sucess
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
                else{
                    //fail
                    $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
                }
            }
            else{
                $final_response['response']=[
                'status' => $resp_checkrole['status'],
                'message' => $resp_checkrole['message'],
                'data' => $resp_checkrole['data'],
                ];
            }
            
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    public function roleComponentPermissions_get(){
        
        $role_id=$this->get('roleId');
        $role_id=decode($role_id);
        $this->form_validation->set_data(array("roleId" => $role_id));
        $validations[]=["field"=>"roleId","label"=>'roleId',"rules"=>"required|integer","errors"=>array(
        'required' => 'Role ID should not be blank','integer' => 'Role should be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            
            $resp_checkrole=$this->UserRole_model->checkrole($role_id);
            
            if($resp_checkrole['status']===true){
                $data['RoleId']=$RoleId=$role_id;
                $res=$this->UserRole_model->getRolePermissions($data);
                $res_main=array();
                $res_main['RoleDetails']=$resp_checkrole['data'];
                $res_main['RolePermissions']=$res['data'];
                $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res_main,
                ];
            }
            else{
                $final_response['response']=[
                'status' => $resp_checkrole['status'],
                'message' => $resp_checkrole['message'],
                'data' => $resp_checkrole['data'],
                ];
            }
            
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    public function saveRoleComponentPermissions_post() {
        
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        
        $data['roleId']=$role_id=decode($this->post('roleId'));
        $data['accessId']=$access_ids=$this->post('accessIds');
        $data['menuId']=$menu_ids=$this->post('menuIds');
        $orders=$this->post('menuOrders');
        //$data['roleDescription']=$roleDescription=$this->post('roleDescription');
        
        $validations[]=["field"=>"roleId","label"=>'roleId',"rules"=>"required","errors"=>array(
        'required' => 'Role ID should not be blank','integer' => 'Role should be integer value only'
        )];
        
        //$validations[]=["field"=>"roleDescription","label"=> 'Role Description ',"rules"=>"required","errors"=>array('required' => 'Role Description should not be blank')];
        $validations[]=["field"=>"menuIds","label"=>'menuIds',"rules"=>"required|regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Menu ids should not be blank','regex_match' => 'Menu ids should be numeric with comma(,) seperated')];
        
        $validations[]=["field"=>"accessIds","label"=>'accessIds',"rules"=>"required|regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Access ids should not be blank','regex_match' => 'Access ids should be numeric with comma(,) seperated')];
        
       $orders_explode=explode('|',$orders);
        $menu_order=array();
        for($order1=0;$order1<count($orders_explode);$order1++){
            
            $order2_explode=explode('-',$orders_explode[$order1]);
             $menu_order[]=array("id"=>$order2_explode[0],"order"=>$order2_explode[1]);
            
        }
        $data['menu_order']=$menu_order;
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $resp_checkrole=$this->UserRole_model->checkrole($role_id);
            if($resp_checkrole['status']===true){
                    $expl_access_ids=explode(',',$access_ids);
                    $is_validaccessids=1;//0 invalid,1-valid
                    $is_validMenuids=1;//0 invalid,1-valid
                    $expl_menu_ids=explode(',',$menu_ids);
                    for($i=0;$i<count($expl_menu_ids);$i++){
                        $checkMenuresp=$this->UserRole_model->checkMenu($expl_menu_ids[$i]);
                        if($expl_menu_ids[$i]!='' && $checkMenuresp['status']===true){
                           //$is_validMenuids=1;
                        }
                        else{
                            $is_validMenuids=0;
                            break;
                        }
                    }
                    if($is_validMenuids==1){
                            for($i=0;$i<count($expl_access_ids);$i++){
                                $checkAccessresp=$this->UserRole_model->checkAccess($expl_access_ids[$i]);
                                if($expl_access_ids[$i]!='' && $checkAccessresp['status']===true){
                                   //$is_validaccessids=1;
                                }
                                else{
                                    $is_validaccessids=0;
                                    break;
                                }
                            }
                            if($is_validaccessids==1){
                                $saveAccessres=$this->UserRole_model->saveAccessPermissions($data);
                                $final_response['response']=[
                                'status' => $saveAccessres['status'],
                                'message' => $saveAccessres['message'],
                                'data' => $saveAccessres['data'],
                                ];
                            }
                            else{
                                $final_response['response']=[
                                'status' => false,
                                'message' => 'Invalid Access Ids',
                                'data' => array(),
                                ];
                            }
                    }
                    else{
                        $final_response['response']=[
                        'status' => false,
                        'message' => 'Invalid Menu Ids',
                        'data' => array("menuIds"=>'Invalid Menu Ids')
                        ];
                    }
            }
            else{
                $final_response['response']=[
                'status' => $resp_checkrole['status'],
                'message' => $resp_checkrole['message'],
                'data' => $resp_checkrole['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
}