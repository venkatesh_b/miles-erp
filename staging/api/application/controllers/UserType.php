<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: Abhilash. Date: 24-02-2016
 * Purpose: Services for User Types.
 * 
 */

class UserType extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('UserType_model');
        $this->load->model('References_model');
        $this->load->library('form_validation');
        $this->load->library('Put_method_extenstion');   /* method PUT */
        $this->load->library('oauth/oauth', '', 'oauth');
    }
    function customvalidation($data){
   
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
    }
    function getUserType_get(){
        $res=$this->UserType_model->getAllUserType();
        $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    
    function getUserTypeById_get(){
        $id = $this->get('user_type_id');
        
        if(!preg_match('/^([0-9]+)$/', $id))
        {
            $final_response['response']=[
                'status' => false,
                'message' => "Validation Error",
                'data' => 'Invalid user type id',
            ];
        } else {
            $res=$this->UserType_model->getUserTypeById($id);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
            
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    
    function addUserType_post(){
        
        $status=true;$message="";$result="";
        
        $config = array(
            array(
                'field' => 'UserTypeName',
                'label' => 'User Type',
                'rules' => 'required|callback_name_check'
            ),
            array(
                'field' => 'UserTypeDescription',
                'label' => 'User Type Description',
                'rules' => 'regex_match[/^[0-9a-zA-Z ]+/]',
                'errors' => array(
                                'regex_match' => 'Invalid %s',
                            ),
            )
        );
        
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        
        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } else {
            $isAdded = $this->UserType_model->addUserType($this->input->post(), $_SERVER['HTTP_USER']);
            
            if($isAdded === 'duplicate')
            {
                $message='User Type is already present';
                $status=false;
                $result = array('UserTypeName' => 'Already Present');
            } 
            else if ($isAdded['error'] === 'dberror')
            {
                $message='Database error';
                $status=false;
                $result = $branchList['msg'];
            } 
            else if($isAdded)
            {
                $message='Added successfully';
                $status=true;
                $result = array($isAdded);
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    
    function editUserType_post(){
        $id = $this->get('user_type_id');
        
        $status=true;$message="";$result="";
        
        $config = array(
            array(
                'field' => 'UserTypeName',
                'label' => 'User Type',
                'rules' => 'required|callback_name_check'
            ),
            array(
                'field' => 'UserTypeDescription',
                'label' => 'User Type Description',
                'rules' => 'regex_match[/^[0-9a-zA-Z ]+/]',
                'errors' => array(
                                'regex_match' => 'Invalid %s',
                            ),
            ),
            array(
                'field' => 'id',
                'label' => 'User Type Id',
                'rules' => 'required|regex_match[/^([0-9]+)$/]',
                'errors' => array(
                                'regex_match' => 'Invalid %s',
                            ),
            )
        );
        
        $data = json_decode(file_get_contents("php://input"), true);
        if($data){ $_POST = $data; }
        
        $this->form_validation->set_rules($config);
        
        $data = array(
            'UserTypeName' => $_POST['UserTypeName'],
            'UserTypeDescription' => $_POST['UserTypeDescription'],
            'id' => $id
        );
        $this->form_validation->set_data($data);

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } else {
            $isUpdated = $this->UserType_model->updateUserType($data, $id, $_SERVER['HTTP_USER']);
//            echo 'Result';
//            print_r($isUpdated);die;
            if($isUpdated === 'duplicate')
            {
                $message='User Type Already present';
                $status=false;
                $result = array('UserTypeName' => 'Already present');
            } 
            else if($isUpdated === 'notpresent')
            {
                $message='User type not found';
                $status=false;
                $result = array();
            } 
            else if ($isUpdated['error'] === 'dberror')
            {
                $message='Database error';
                $status=false;
                $result = $branchList['msg'];
            } 
            else if($isUpdated)
            {
                $message='Updated successfully';
                $status=true;
                $result = array();
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    
    function deleteUserType_post(){
//        print_r($this->get());die;
        $id = $this->get('user_type_id');
        $userStatus = $this->get('status');
        
        $status=true;$message="";$result="";
        
        $config = array(
            array(
                'field' => 'status',
                'label' => 'status',
                'rules' => 'required|regex_match[/^([01])$/]',
                'errors' => array(
                                'regex_match' => 'Invalid %s',
                            ),
            ),
            array(
                'field' => 'id',
                'label' => 'User Type Id',
                'rules' => 'required|regex_match[/^([0-9]+)$/]',
                'errors' => array(
                                'regex_match' => 'Invalid %s',
                            ),
            )
        );
        
        $this->form_validation->set_rules($config);
        
        $data = array(
            'status' => $userStatus,
            'id' => $id
        );
        $this->form_validation->set_data($data);

        if ($this->form_validation->run() == FALSE) {
            $message="Validation Error";
            $status=false;
            $result=$this->form_validation->error_array();
        } else {
            $isdeleted = $this->UserType_model->deleteUserType($data, $id, $_SERVER['HTTP_USER']);
            if($isdeleted === 'duplicate')
            {
                $message='User Type is Invalid';
                $status=false;
                $result = array();
            } 
            else if($isdeleted === 'notpresent')
            {
                $message='User type not found';
                $status=false;
                $result = array();
            } 
            else if (isset($isdeleted['error']) && $isdeleted['error'] === 'dberror')
            {
                $message='Database error';
                $status=false;
                $result = $branchList['msg'];
            } 
            else if(isset($isdeleted['status']) && $isdeleted['status'])
            {
                $message=$isdeleted['msg'].' successfully';
                $status=true;
                $result = array();
            }
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    
    /**
     * Function for checking valid string
     * 
     * @args 
     *  "str"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function name_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str)) {
            $this->form_validation->set_message('name_check', 'The %s field is not valid!, use only Alphabets');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}

