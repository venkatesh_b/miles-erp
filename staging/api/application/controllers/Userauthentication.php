<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userauthentication extends CI_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->helper('form');
        $this->load->model("Userauthentication_model");
        $this->load->library('form_validation');
    }
    function Login() {
        $input_data = json_decode(file_get_contents("php://input"), true);
        if($input_data){
            $_POST = $input_data; 
            
        }
        //$data = $this->input->post();
        //echo "VENKI";
        //print_r($data);
       // $this->result_set('200', "sample message",array("k"=>'venki'));
        //exit;
        $this->load->library('user_agent');
        
        $user_browser=$this->agent->browser();
        $user_platform=$this->agent->platform();
        $user_ipaddress= $this->input->ip_address();
        
        $this->form_validation->set_rules('emailId', 'emailId', 'required|regex_match[/\S+@\S+\.\S+/]', array('required' => 'User mail id should not be blank', 'regex_match' => 'Invalid email id'));
        
        $this->form_validation->set_rules('accountId', 'accountId', 'required|regex_match[/^[A-z0-9]+$/]', array('required' => 'Account ID should not be blank', 'regex_match' => 'Invalid Account ID'));
        
        $this->form_validation->set_rules('userName', 'userName', 'regex_match[/^[A-z]+$/]', array('regex_match' => 'Invalid User Name'));
        
         $this->form_validation->set_rules('gender', 'gender', 'integer|regex_match[/^[0-1]$/]', array('integer' => 'Gender should be integer value only','regex_match' => 'Gender should be 0 for Female,1 for Male'));
         
         $this->form_validation->set_rules('phoneNumber', 'phoneNumber', 'regex_match[/^[0-9() +-]{8,15}$/]', array('regex_match' => 'Invalid Phone Number'));
        
        $this->form_validation->set_rules('userDob', 'userDob', "regex_match[/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/]", array('regex_match' => 'Invalid Date of Birth. Date should be in YYYY-MM-DD Format.'));
        
        
        
        

        if ($this->form_validation->run() == FALSE) {
            $this->result_set('400', 'Bad Request', $this->form_validation->error_array());
        }
        else{

        $user_email_id = $this->input->post("emailId");
        $user_name = $this->input->post("userName");
        $phone_number = $this->input->post("phoneNumber");
        $profile_image = $this->input->post("profileImage");
        $user_dob = $this->input->post("userDob");
        
        $account_id = $this->input->post("accountId");
        $gender = $this->input->post("gender");

        $data['user_email_id'] = $user_email_id;
        $data['user_name'] = $user_name;
        $data['phone_number'] = $phone_number;
        $data['profile_image'] = $profile_image;
        $data['user_dob'] = $user_dob;
        $data['account_id'] = $account_id;
        $data['gender'] = $gender;
        $data['user_browser']=$user_browser;
        $data['user_platform']=$user_platform;
        $data['user_ipaddress']=$user_ipaddress;
        
        $res = $this->Userauthentication_model->userLogin($data);

       if($res['status']===true){

           $resp['data'] = $this->createToken($res['data']['userID'], $res['data']['email']);

           foreach($res['data'] as $k=>$v){
               $resp['data'][$k]=$v;
           }

           $this->result_set('200', $res['message'], $resp['data']);
         //  print_r($res);die("asd");

       }
       else{
           $this->result_set('400', $res['message'], $res['data']);
       }
       
        }
    }

    function CustomLogin()
    {
        $input_data = json_decode(file_get_contents("php://input"), true);
        if($input_data){
            $_POST = $input_data;
        }
        $this->load->library('user_agent');
        $user_browser=$this->agent->browser();
        $user_platform=$this->agent->platform();
        $user_ipaddress= $this->input->ip_address();

        $this->form_validation->set_rules('emailId', 'emailId', 'required|regex_match[/\S+@\S+\.\S+/]', array('required' => 'Email cannot be blank', 'regex_match' => 'Invalid email id'));

        $this->form_validation->set_rules('loginPassword', 'loginPassword', 'required', array('required' => 'Password cannot not be blank'));

        if ($this->form_validation->run() == FALSE)
        {
            $this->result_set('400', 'Bad Request', $this->form_validation->error_array());
        }
        else
        {
            $user_email_id = $this->input->post("emailId");
            $user_password = $this->input->post("loginPassword");

            if($user_password != LOGIN_PWD)
            {
                $this->result_set('400', "Incorrect password!", "");
            }
            else
            {
                $data['user_email_id'] = $user_email_id;
                $data['user_browser']=$user_browser;
                $data['user_platform']=$user_platform;
                $data['user_ipaddress']=$user_ipaddress;

                $res = $this->Userauthentication_model->userCustomLogin($data);
                if($res['status']===true)
                {
                    $resp['data'] = $this->createToken($res['data']['userID'], $res['data']['email']);

                    foreach($res['data'] as $k=>$v)
                    {
                        $resp['data'][$k]=$v;
                    }
                    //print_r($resp['data']);
                    $this->result_set('200', $res['message'], $resp['data']);
                }
                else
                {
                    $this->result_set('400', $res['message'], $res['data']);
                }
            }

        }
    }



    private function createToken($id, $email) {
        // Set the POST data
        $client_id = $id;
        $secret_id = $this->Userauthentication_model->getsecret_id($client_id);

        if ($secret_id == 'dberror') {
            $this->result_set('400', 'Error - 400', '[DB]Something went wrong, Plase try after sometime.');
        } else {
            $client_scopes = $this->Userauthentication_model->getscopes($client_id);

            if ($client_scopes == 'dberror') {
                $this->result_set('400', 'Error - 400', '[DB]Something went wrong, Plase try after sometime.');
            } else {
                
                $scopeids = '';
                if(isset($client_scopes) && !empty($client_scopes) && count($client_scopes)>0)
                {
                    foreach ($client_scopes as $scopeid) {
                        $scopeids.=$scopeid->scope_id . ',';
                    }
                }


                $scopeids = rtrim($scopeids, ',');
                
                $postdata = http_build_query(
                        array(
                            'client_id' => $client_id,
                            'client_secret' => $secret_id,
                            'grant_type' => 'client_credentials',
                            'scope' => $scopeids
                        )
                );

                // Set the POST options
                $opts = array('http' =>
                    array(
                        'method' => 'POST',
                        'header' => "Content-type: application/x-www-form-urlencoded\r\n" . "User: $client_id\r\n". "context: default\r\n",
                        'content' => $postdata
                    )
                );

                // Create the POST context
                $context = stream_context_create($opts);

                // POST the data to an api
                $url = BASE_URL . 'index.php/accesstoken/generateToken';
                $actoken = (file_get_contents($url, false, $context));
                $actoken = json_decode($actoken);
                
                if (isset($actoken)) {
                    $response['accessToken'] = $actoken->access_token;
                    $response['tokenType'] = $actoken->token_type;
                    $response['expiresIn'] = $actoken->expires_in;
                    //$response['userID'] = $id;
                    //$response['email'] = $email;
                    //$response['role'] = $role;

                    return $response;
                } else {
                    $this->result_set('401', 'Error - 401', 'Access Denied!!');
                }
            }
        }
    }

    public function CreateRefreshToken() {


        // $this->load->library('oauth/oauth');
        //if expired 
        
        $input_data = json_decode(file_get_contents("php://input"), true);
        if($input_data){
            $_POST = $input_data; 
            
        }
        
        

        $client_id = $this->input->post('User');
        $Authorizationtoken = $this->input->post('Authorizationtoken');

        //$this->load->model("Users_model");

        $secret_id = $this->Userauthentication_model->getsecret_id($client_id);

        $client_scopes = $this->Userauthentication_model->getscopes($client_id);
        
        
        $scopeids = '';
                if(isset($client_scopes) && !empty($client_scopes) && count($client_scopes)>0)
                {
                    foreach ($client_scopes as $scopeid) {
                        $scopeids.=$scopeid->scope_id . ',';
                    }
                }

        $scopeids = rtrim($scopeids, ',');

        $postdata = http_build_query(
                array(
                    'client_id' => $client_id,
                    'client_secret' => $secret_id,
                    'grant_type' => 'client_credentials',
                    'scope' => $scopeids
                )
        );

        // Set the POST options
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n" . "User: $client_id\r\n",
                'content' => $postdata
            )
        );

        // Create the POST context
        $context = stream_context_create($opts);

        // POST the data to an api
        $url = BASE_URL . 'index.php/accesstoken/generateToken';
        $actoken = (file_get_contents($url, false, $context));
         $actoken = json_decode($actoken);
         if (isset($actoken)) {
                    $response['accessToken'] = $actoken->access_token;
                    $response['tokenType'] = $actoken->token_type;
                    $response['expiresIn'] = $actoken->expires_in;
                    //$response['userID'] = $id;
                    //$response['email'] = $email;
                    //$response['role'] = $role;
                    $this->result_set('200', 'success',$response);
                    //return $response;
                } else {
                    $this->result_set('401', 'Error - 401', 'Access Denied!!');
                }
        //$response['access_token_response']=$actoken;
        //echo json_encode($actoken);
        //exit;
    }
    
    function Logout(){
        
        $authorizationtoken=(isset($_SERVER['HTTP_AUTHORIZATIONTOKEN']) && !empty($_SERVER['HTTP_AUTHORIZATIONTOKEN'])) ? $_SERVER['HTTP_AUTHORIZATIONTOKEN'] : '' ;
        $userid=(isset($_SERVER['HTTP_USER']) && !empty($_SERVER['HTTP_USER'])) ? $_SERVER['HTTP_USER'] : '' ;
        //$authorizationtoken=$_SERVER['HTTP_AUTHORIZATIONTOKEN'];
        //$userid=$_SERVER['HTTP_USER'];
        
        $this->form_validation->set_data(array("Authorizationtoken"=> $authorizationtoken,"User"=> $userid));
        
        $this->form_validation->set_rules('Authorizationtoken', 'Authorizationtoken', 'required', array('required' => 'Authorization Token is Required or Should not be blank in header'));
        
        $this->form_validation->set_rules('User', 'User', 'required|integer', array('required' => 'User ID is Required or Should not be blank in header','integer' => 'Invalid User ID, ID should be integer only'));
        if ($this->form_validation->run() == FALSE) {
            
            $this->result_set('400', 'Bad Request', $this->form_validation->error_array());
            
        }
        else{
            $data['authorizationtoken']=$authorizationtoken;
            $data['userid']=$userid;
            $res=$this->Userauthentication_model->userLogout($data);
            if($res['status']===true){
                $this->result_set('200', $res['message'],array());
            }
            else{
                $this->result_set('400', $res['message'],array());
            }
        }
     }

    /**
     * Function Name - result_set
     * @param type String, $status EX - 200, 400,.. etc
     * @param type String, $msg Ex - Error - 400, Success - 200, .. etc
     * @param type - String/Array, $data 
     * 
     * #auther - Abhilash
     */
    public function result_set($status, $msg, $data) {
        $resp_array = ['200', '201'];
        $statusVal = FALSE;
        //$status=200;
        if (in_array($status, $resp_array)) {
            $statusVal = TRUE;
            $status = "HTTP/1.0 " . $status . " SUCCESS";
        } else {
            $status = "HTTP/1.1 " . $status . " ERROR";
        }

        $result = ['status' => $statusVal, 'message' => $msg, 'data' => $data];
        
        $this->output->set_header("HTTP/1.0 200 SUCCESS");
        $this->output->set_output(json_encode($result));

      // print_r($result);
         // exit;
        /*$this->response([
            'status' => $status,
            'message' => $msg,
            'data' => $data,
                ], $status);*/
    }

}

?>
