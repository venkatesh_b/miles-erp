<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/* 
 * Created By: Venkatesh
 * Purpose: Services for Vouchers.
 * 
 * Updated on - 6 Feb 2016
 * 
 * updated by: Parameshwar
 * updated on: 17 Feb 2016:
 * Purpose: Services for creation of Vouchers, edit and delete of a Voucher.
 */

class Voucher extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("Voucher_model");
        $this->load->library('form_validation');
        $this->load->model('User_model');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->helper('encryption');
        $this->load->library('SSP');
    }
    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);
        if ($this->form_validation->run() == FALSE)
        {
            if(count($this->form_validation->error_array())>0)
            {
               return $this->form_validation->error_array();
            }
            else
            {
               return true;
            }
        }
        else
        {
            return true;
        }
    }

    function getAllVouchers_get()
    {
        $this->response($this->Voucher_model->voucherDataTables(),REST_Controller::HTTP_OK);
    }

    function getVoucherDetails_get()
    {
        $voucherId=decode($this->get('voucherId'));
        $this->form_validation->set_data(array("voucherId"=>$voucherId));
        $validations[]=["field"=>"voucherId","label"=>'hdnVoucherId',"rules"=>"required","errors"=>array('required' => 'Invalid voucher','regex_match' => 'Invalid voucher')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
                $data['voucherId']=$voucherId;
                $res=$this->Voucher_model->getVoucherDetailsByID($data);
                $final_response['response']=
                    [
                        'status' => $res['status'],
                        'message' => $res['message'],
                        'data' => $res['data'],
                    ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addVoucher_post(){
        
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $validations[]=["field"=>"voucherName","label"=>'txtVoucherName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9-]+$/]","errors"=>array('required' => 'Required','regex_match' => 'Invalid Name')];
        $validations[]=["field"=>"companyId","label"=>'ddlCompanies',"rules"=>"regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Required','regex_match' => 'Invalid Company')];
        $validations[]=["field"=>"branchId","label"=>'txtCourseBranches',"rules"=>"regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Required','regex_match' => 'Invalid Branch')];
        $validations[]=["field"=>"voucherAmount","label"=>'txtVoucherAmount',"rules"=>"regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Required','regex_match' => 'Invalid Amount')];
        $validations[]=["field"=>"voucherDescription","label"=>'txtVoucherDescription',"rules"=>"required","errors"=>array('required' => 'Description should not be blank')];
        $validations[]=["field"=>"voucherDate","label"=>'txtVoucherDate',"rules"=>"required","errors"=>array('required' => 'Choose date')];
        $validations[]=["field"=>"voucherStatus","label"=> 'txtVoucherStatusValue',"rules"=>"required|integer|regex_match[/^[0-1]$/]","errors"=>array('required' => 'Required','integer' => 'Required','regex_match' => 'Invalid Status')];
         $validations[]=["field"=>"userID","label"=>'userID',"rules"=>"required|regex_match[/^[0-9]+$/]","errors"=>array('required' => 'User ID should not be blank','regex_match' => 'User ID should be numeric only')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $data['userID']=$this->post('userID');
            $data['voucherName']=$this->post('voucherName');
            $data['companyId']=$this->post('companyId');
            $data['branchId']=$this->post('branchId');
            $data['voucherAmount']=$this->post('voucherAmount');
            $chosen_date = DateTime::createFromFormat('d M, Y', $this->post('voucherDate'));
            $chosen_date = $chosen_date->format("Y-m-d");
            $data['voucherDate']=$chosen_date;
            $data['voucherDescription']=$this->post('voucherDescription');
            $data['voucherStatus']=$this->post('voucherStatus');
            $res=$this->Voucher_model->addVoucher($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function updateVoucher_post(){
        
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        //var params = {userID: userID,voucherId: voucherId, voucherName: voucherName, branchId: branchId,voucherAmount:voucherAmount, voucherDescription: voucherDescription, voucherStatus: voucherStatus };
        $voucherId=decode($this->post('voucherId'));
        $validations[]=["field"=>"voucherId","label"=>'hdnVoucherId',"rules"=>"required","errors"=>array('required' => 'Invalid voucher','regex_match' => 'Invalid voucher')];
        $validations[]=["field"=>"voucherName","label"=>'txtVoucherName',"rules"=>"required|regex_match[/^[a-zA-Z][a-zA-Z0-9-]+$/]","errors"=>array('required' => 'Required','regex_match' => 'Invalid Name')];
        $validations[]=["field"=>"branchId","label"=>'txtCourseBranches',"rules"=>"regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Required','regex_match' => 'Invalid Branch')];
        $validations[]=["field"=>"voucherAmount","label"=>'txtVoucherAmount',"rules"=>"regex_match[/^[0-9,]+$/]","errors"=>array('required' => 'Required','regex_match' => 'Invalid Amount')];
        $validations[]=["field"=>"voucherDate","label"=>'txtVoucherDate',"rules"=>"required","errors"=>array('required' => 'Choose date')];
        $validations[]=["field"=>"voucherDescription","label"=>'txtVoucherDescription',"rules"=>"required","errors"=>array('required' => 'Description should not be blank')];
        $validations[]=["field"=>"voucherStatus","label"=> 'txtVoucherStatusValue',"rules"=>"required|integer|regex_match[/^[0-1]$/]","errors"=>array('required' => 'Required','integer' => 'Required','regex_match' => 'Invalid Status')];
        $validations[]=["field"=>"userID","label"=>'userID',"rules"=>"required|regex_match[/^[0-9]+$/]","errors"=>array('required' => 'User ID should not be blank','regex_match' => 'User ID should be numeric only')];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $data['userID']=$this->post('userID');
            $data['voucherId']=$voucherId;
            $data['voucherName']=$this->post('voucherName');
            $data['branchId']=$this->post('branchId');
            $data['voucherAmount']=$this->post('voucherAmount');
            $chosen_date = DateTime::createFromFormat('d M, Y', $this->post('voucherDate'));
            $chosen_date = $chosen_date->format("Y-m-d");
            $data['voucherDate']=$chosen_date;
            $data['voucherDescription']=$this->post('voucherDescription');
            $data['voucherStatus']=$this->post('voucherStatus');
            $res=$this->Voucher_model->updateVoucher($data);
            $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
            ];
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
        }
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    function deleteVoucher_delete(){
        $voucherId=decode($this->get('voucherId'));
        $status = $this->get('status');
        $this->form_validation->set_data(array("voucherId"=>$voucherId,"userID"=>$this->get('userID')));
        
        $validations[]=["field"=>"voucherId","label"=>'hdnVoucherId',"rules"=>"required","errors"=>array('required' => 'Invalid Voucher','regex_match' => 'Invalid Voucher')];
        
        $validations[]=["field"=>"userID","label"=>'userID',"rules"=>"required|regex_match[/^[0-9]+$/]","errors"=>array('required' => 'You don\'t have access','regex_match' => 'You don\'t have access')];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true)
        {
            $data['status'] = $status;
            $data['voucherId']=$voucherId;
            $res=$this->Voucher_model->deleteVoucher($data);
            $final_response['response']=[
            'status' => $res['status'],
            'message' => $res['message'],
            'data' => $res['data'],
            ];
        }
        else
        {
            $final_response['response']=
                [
                    'status' => FALSE,
                    'message' => 'validation failed',
                    'data' => $validationstatus,
                ];
        }
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            $this->response($final_response['response'], $final_response['responsehttpcode']);
    }

}

