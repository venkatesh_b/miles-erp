<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-19
 * Time: 06:53 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class WarehouseGrn extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("WarehouseGrn_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }

    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE) {
            if (count($this->form_validation->error_array()) > 0) {
                return $this->form_validation->error_array();
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    function warehouseGrnList_get()
    {
        $this->response($this->WarehouseGrn_model->warehouseGrnDataTables(),REST_Controller::HTTP_OK);
    }
    function getGtnProductDetails_get()
    {
        $gtnNo=$this->get('gtnId');
        $this->form_validation->set_data(array("gtnId"=>$gtnNo));

        $validations[]=["field"=>"gtnId","label"=>'gtnId',"rules"=>"required","errors"=>array('required' => 'GRN No should not be blank','regex_match' => 'GRN No should be numeric only')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $data['gtnNo']=$gtnNo;
            $res=$this->WarehouseGrn_model->checkGtnNoExist($data);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
            if($res['status']===true)
            {
                $res=$this->WarehouseGrn_model->getGtnProductDetails($data);
                $final_response['response']=[
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getGtnProductsByVendor_get()
    {
        $vendorId=$this->get('vendorId');
        $this->form_validation->set_data(array("vendorId"=>$vendorId));

        $validations[]=["field"=>"vendorId","label"=>'vendorId',"rules"=>"required","errors"=>array('required' => 'Name should not be blank','regex_match' => 'Name should be numeric only')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true)
        {
            $data['vendorId']= $vendorId;
            $res=$this->WarehouseGrn_model->checkVendorGtnNoExist($vendorId);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
            if($res['status']===true)
            {
                $res=$this->WarehouseGrn_model->getVendorGtnProductDetails($data);
                $final_response['response']=[
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
        }
        else
        {
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getBranchGtnNumbers_get()
    {
        $searchVal=$this->get('searchVal');
        $warehouseGtnList = $this->WarehouseGrn_model->getGtnNumbers($searchVal);
        $status=true;$message="";$result="";

        if ($warehouseGtnList == 'badrequest')
        {
            $message='There are no products';
            $status=false;
        }
        else if (isset($warehouseGtnList['error']) && $warehouseGtnList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $warehouseGtnList['msg'];
        }
        else if ($warehouseGtnList)
        {
            $message="list";
            $status=true;
            $result=$warehouseGtnList['data'];
        }
        else
        {
            /*No data found*/
            $message='No details found';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function getProducts_get()
    {
        $searchVal=$this->get('searchVal');
        $branchList = $this->WarehouseGrn_model->getProductList($searchVal);
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no products';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList['data'];
        }
        else
        {
            /*No data found*/
            $message='There are no products';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function getVendors_get()
    {
        $searchVal=$this->get('searchVal');
        $branchList = $this->WarehouseGrn_model->getVendorList($searchVal);
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no vendors';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList;
        }
        else
        {
            /*No data found*/
            $message='There are no vendors';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function addWarehouseGrn_post()
    {
        $inputData = $this->post();
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"grnNo","label"=>'grnNo',"rules"=>"required","errors"=>array('required' => 'GTN No should not be blank','regex_match' => 'GRN No should be numeric only')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true) {

           if($inputData['requestMode'] != 'branch' )
           {
               $param['invoiceNumber']=$inputData['grnInvoice'];
               $invoiceRes = $this->WarehouseGrn_model->checkInvoiceNoExist($param);
               if($invoiceRes['status']===true){
                   $config['upload_path'] = './uploads/inventory_invoice/';
                   $fileName = $inputData['image'];
                   $config['file_name']= $fileName;
                   $config['allowed_types'] = '*';
                   $config['max_size']	= '0';
                   $config['remove_spaces']= TRUE;
                   $this->load->library('upload', $config);
                   if (!$this->upload->do_upload('file'))
                   {
                       $final_response['response']=[
                           'status' => FALSE,
                           'message' => $this->upload->display_errors('',''),
                           'data' => array(),
                       ];
                   }
                   else
                       $uploadedFileName = 'uploads/inventory_invoice/' . $this->upload->data('file_name');

               }
               else{
                   $final_response['response']=[
                       'status' => $invoiceRes['status'],
                       'message' => $invoiceRes['message'],
                       'data' => $invoiceRes['data'],
                   ];
               }

               $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
               $this->response($final_response['response'], $final_response['responsehttpcode']);
           }
           else
           {
               $uploadedFileName = '';
               $inputData['grnInvoice'] = '';
           }

            $data = array(
                'userID' => $inputData['userID'],
                'receiverMode' => $inputData['receiverMode'],
                'receiverObjectId' => $inputData['receiverObjectId'],
                'requestMode' => $inputData['requestMode'],
                'requestObjectId' => $inputData['requestObjectId'],
                'products' => explode(',',$inputData['products']),
                'grnNo' => $inputData['grnNo'],
                'stock_flow_id' => $inputData['stock_flow_id']
            );
            $data['invoice_file']=$uploadedFileName;
            $data['invoice_number']=$inputData['grnInvoice'];
            $data['remarks']=$inputData['grnRemarks'];

            $res = $this->WarehouseGrn_model->addWarehouseGrn($data);
            $final_response['response'] = [
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function addWarehouseGrnFromVendor_post()
    {
        $inputData = $this->post();
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $param['invoiceNumber']=$inputData['grnInvoice'];
        $invoiceRes = $this->WarehouseGrn_model->checkInvoiceNoExist($param);
        if($invoiceRes['status']===true) { //invoice existence
            $config['upload_path'] = './uploads/inventory_invoice/';
            $fileName = $inputData['image'];
            $config['file_name'] = $fileName;
            $config['allowed_types'] = '*';
            $config['max_size'] = '0';
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $final_response['response'] = [
                    'status' => FALSE,
                    'message' => $this->upload->display_errors('', ''),
                    'data' => array(),
                ];
            } else {
                $uploadedFileName = 'uploads/inventory_invoice/' . $this->upload->data('file_name');
                $data = array(
                    'userID' => $inputData['userID'],
                    'receiverMode' => $inputData['receiverMode'],
                    'receiverObjectId' => $inputData['receiverObjectId'],
                    'requestMode' => $inputData['requestMode'],
                    'requestObjectId' => $inputData['requestObjectId'],
                    'products' => $inputData['products']
                );
                $data['invoice_file']=$uploadedFileName;
                $data['invoice_number']=$inputData['grnInvoice'];
                $data['remarks']=$inputData['grnRemarks'];
                $res = $this->WarehouseGrn_model->addWarehouseGrnFromVendor($data);
                $final_response['response'] = [
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => $invoiceRes['status'],
                'message' => $invoiceRes['message'],
                'data' => $invoiceRes['data'],
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getWareHouseStock_get(){
        $status=true;
        $message='success';
        $res = $this->WarehouseGrn_model->getWareHouseStock();

        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }

    function getWareHouseStockByProduct_get(){
        $status=true;
        $message='success';
        $productId=$this->get('productId');
        $res = $this->WarehouseGrn_model->getWareHouseStockByProductId($productId);

        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }
    function liquidateWarehouseStockByProduct_get() {
        $status=true;
        $message='success';
        $productId = $this->get('productId');
        $availableStock = $this->get('availableStock');
        $liquidateStock = $this->get('liquidateStock');
        if($liquidateStock > $availableStock)
        {
            $data=array("status"=>false,"message"=>'Liquidate cannot be greater than Available Stock',"data"=>'');
        }
        else
        {
            $res = $this->WarehouseGrn_model->liquidateWarehouseStockByProduct($productId,$liquidateStock);

            $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        }
        $this->set_response($data);
    }
    function getWareHouseStockProductWise_get(){
        $status=true;
        $message='success';
        $type=$this->get('type');
        $productId=$this->get('productId');
        $res = $this->WarehouseGrn_model->getWareHouseStockProductWise($type,$productId);
        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }
    function getWareHouseGrnProducts_get()
    {
        $sequenceNumber=$this->get('sequenceNumber');
        $res = $this->WarehouseGrn_model->getWareHouseGrnProducts($sequenceNumber);
        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }
    function getGrnDetails_get(){
        $stockFlowId=$this->get('stockFlowId');
        $res = $this->WarehouseGrn_model->getGrnDetails($stockFlowId);
        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }
    function editWarehouseGrnFromVendor_post()
    {
        $inputData = $this->post();
        $postdata = json_decode(file_get_contents("php://input"), true);


        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());
        $param['invoiceNumber']=$inputData['grnInvoice'];
        $param['not_stockFlowId']=$inputData['stockFlowId'];
        $invoiceRes = $this->WarehouseGrn_model->checkInvoiceNoExist($param);
        if($invoiceRes['status']===true) { //invoice existence
            $checkFileUploaded=false;
            $uploadedFileName='';
            if(isset($inputData['image'])){
                $config['upload_path'] = './uploads/inventory_invoice/';
                $fileName = $inputData['image'];
                $config['file_name'] = $fileName;
                $config['allowed_types'] = '*';
                $config['max_size'] = '0';
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('file')) {
                    $final_response['response'] = [
                        'status' => FALSE,
                        'message' => $this->upload->display_errors('', ''),
                        'data' => array(),
                    ];
                    $checkFileUploaded=false;
                }
                else{
                    $uploadedFileName = 'uploads/inventory_invoice/' . $this->upload->data('file_name');
                    $checkFileUploaded=true;
                }
            }
            else{
                $uploadedFileName='NA';
                $checkFileUploaded=true;
            }



            if (!$checkFileUploaded) {

            } else {

                $data = array(
                    'userID' => $inputData['userID'],
                    'receiverMode' => $inputData['receiverMode'],
                    'receiverObjectId' => $inputData['receiverObjectId'],
                    'requestMode' => $inputData['requestMode'],
                    'requestObjectId' => $inputData['requestObjectId'],
                    'products' => $inputData['products'],
                    'stockFlowId' => $inputData['stockFlowId']
                );
                $data['invoice_file']=$uploadedFileName;
                $data['invoice_number']=$inputData['grnInvoice'];
                $data['remarks']=$inputData['grnRemarks'];
                $res = $this->WarehouseGrn_model->editWarehouseGrnFromVendor($data);
                $final_response['response'] = [
                    'status' => $res['status'],
                    'message' => $res['message'],
                    'data' => $res['data'],
                ];
            }
        }
        else{
            $final_response['response']=[
                'status' => $invoiceRes['status'],
                'message' => $invoiceRes['message'],
                'data' => $invoiceRes['data'],
            ];
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
}
