<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-17
 * Time: 02:40 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class WarehouseGtn extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("WarehouseGtn_model");
        $this->load->library('form_validation');
        $this->load->library('oauth/oauth', '', 'oauth');
        $this->load->library('SSP');
        $this->load->helper('encryption');
    }
    function customvalidation($data)
    {
        $this->form_validation->set_rules($data);

        if ($this->form_validation->run() == FALSE)
        {
            if (count($this->form_validation->error_array()) > 0)
            {
                return $this->form_validation->error_array();
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    function warehouseGtnList_get()
    {
        $this->response($this->WarehouseGtn_model->warehouseGtnDataTables(),REST_Controller::HTTP_OK);
    }
    function getBranches_get()
    {
        //$searchVal=$this->get('searchVal');
        $branchList = $this->WarehouseGtn_model->getBranchList();
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no branches';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList;
        }
        else
        {
            /*No data found*/
            $message='There are no branches';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function getVendors_get()
    {
        $searchVal=$this->get('searchVal');
        $branchList = $this->WarehouseGtn_model->getVendorList();
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no vendors';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList;
        }
        else
        {
            /*No data found*/
            $message='There are no vendors';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function getProducts_get()
    {
        $searchVal=$this->get('searchVal');
        $branchList = $this->WarehouseGtn_model->getProductList($searchVal);
        $status=true;$message="";$result="";

        if ($branchList == 'badrequest')
        {
            $message='There are no products';
            $status=false;
        }
        else if (isset($branchList['error']) && $branchList['error'] == 'dberror')
        {
            $message='Database error';
            $status=false;
            $result = $branchList['msg'];
        }
        else if ($branchList)
        {
            $message="Branches list";
            $status=true;
            $result=$branchList['data'];
        }
        else
        {
            /*No data found*/
            $message='There are no products';
            $status=false;
        }
        $data=array("status"=>$status,"message"=>$message,"data"=>$result);
        $this->set_response($data);
    }
    function addWarehouseGtn_post()
    {
        $inputData = $this->post();
        $postdata = json_decode(file_get_contents("php://input"), true);
        if($postdata){ $_POST = $postdata; }
        $this->form_validation->set_data($this->post());

        $validations[]=["field"=>"requestObjectId","label"=>'requestObjectId',"rules"=>"required","errors"=>array('required' => 'Received from should not be blank')];

        $validationStatus=$this->customvalidation($validations);
        if($validationStatus===true) {

            $data = array(
                'userID' => $inputData['userID'],
                'receiverMode' => $inputData['receiverMode'],
                'receiverObjectId' => $inputData['receiverObjectId'],
                'requestMode' => $inputData['requestMode'],
                'requestObjectId' => $inputData['requestObjectId'],
                'remarks' => $inputData['gtnRemarks'],
                'products' => $inputData['products'],
                'invoice_number' => $inputData['gtnInvoice'],
                'weight' => $inputData['gtnWeight'],
                'invoice_file' => ''
            );
            $res = $this->WarehouseGtn_model->addWarehouseGtn($data);
            $final_response['response'] = [
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
            ];


            /*$param['invoiceNumber']=$inputData['gtnInvoice'];
            $invoiceRes = $this->WarehouseGtn_model->checkInvoiceNoExist($param);
            if($invoiceRes['status']===true){
                $config['upload_path'] = './uploads/inventory_invoice/';
                $fileName = $inputData['image'];
                $config['file_name']= $fileName;
                $config['allowed_types'] = '*';
                $config['max_size']	= '0';
                $config['remove_spaces']= TRUE;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('file'))
                {
                    $final_response['response']=[
                        'status' => FALSE,
                        'message' => $this->upload->display_errors('',''),
                        'data' => array(),
                    ];
                }else {
                    $uploadedFileName = 'uploads/inventory_invoice/' . $this->upload->data('file_name');
                    $data = array(
                        'userID' => $inputData['userID'],
                        'receiverMode' => $inputData['receiverMode'],
                        'receiverObjectId' => $inputData['receiverObjectId'],
                        'requestMode' => $inputData['requestMode'],
                        'requestObjectId' => $inputData['requestObjectId'],
                        'remarks' => $inputData['gtnRemarks'],
                        'products' => $inputData['products']
                    );
                    $data['invoice_file']=$uploadedFileName;
                    $data['invoice_number']=$inputData['gtnInvoice'];
                    $res = $this->WarehouseGtn_model->addWarehouseGtn($data);
                    $final_response['response'] = [
                        'status' => $res['status'],
                        'message' => $res['message'],
                        'data' => $res['data'],
                    ];
                }
            }
            else{
                $final_response['response']=[
                    'status' => $invoiceRes['status'],
                    'message' => $invoiceRes['message'],
                    'data' => $invoiceRes['data'],
                ];
            }*/
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationStatus,
            ];

            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    function getWareHouseGtnProducts_get()
    {
        $sequenceNumber=$this->get('sequenceNumber');
        $res = $this->WarehouseGtn_model->getWareHouseGtnProducts($sequenceNumber);
        $data=array("status"=>$res['status'],"message"=>$res['message'],"data"=>$res['data']);
        $this->set_response($data);
    }
}