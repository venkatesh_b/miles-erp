<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

class SendEmail {
    
    function sendMail($email,$subject,$message, $files){
        $config=array(
            'protocol' => 'smtp',
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASS,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'newline' => "\r\n" //use double quotes to comply with RFC 822 standard
        );
        
        $CI =& get_instance();

        $CI->load->library('email',$config);
        $CI->email->from(SMTP_FROM, SMTP_FROM_NAME);
        $CI->email->to($email);

        $CI->email->subject($subject);
        $CI->email->message($message);
        
        foreach($files as $k => $v){
            $CI->email->attach($v);
        }
        
        if($CI->email->send()){
            $CI->email->clear(TRUE);
            $CI->load->model("EmailTemplates_model");
            $CI->EmailTemplates_model->deleteOrginalFile($files);
            return true;
        }else{
            return $CI->email->print_debugger();
        }
    }
}