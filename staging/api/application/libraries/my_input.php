<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * For extending the put method and cleaning the data
 * 
 * @author          nambok
 * @link            https://gist.github.com/nambok/3834056
 * Prerquisite
 *  $this->load->library('my_input');   //include
 *  
 *  $this->put = file_get_contents('php://input');
 *  //parse and clean request
 *  $this->my_input->_parse_request($this->put);
 *  $this->put("key") returns data
 */
class My_input {

    /**
     * Parse json request
     *
     * @access	private
     * @return	void
     */
    public function _parse_request(&$array) {
        //for now only json :(, will add more as they come up
        if ($_SERVER['CONTENT_TYPE'] == "application/json" ||
                $_SERVER['CONTENT_TYPE'] == "application/json; charset=UTF-8") {
            $array = json_decode($array);
        } else {
            parse_str($array, $array);
        }

        $this->_sanitize_request($array);
    }

    /**
     * Extend Sanitize Data
     *
     * @access	private
     * @return	void
     */
    public function _sanitize_request(&$data) {
        //convert object to array
        $object = $data;
        $data = array();

        //clean put variables
        foreach ($object as $key => $val) {
            if (!is_object($val))
                $data[$this->_clean_input_keys($key)] = $this->_clean_input_data($val);
        }

//            print_r("Global PUT and DELETE data sanitized");
    }

}
