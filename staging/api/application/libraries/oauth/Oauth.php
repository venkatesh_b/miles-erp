<?php
/**
 * Created by PhpStorm.
 * User: BHASHA
 * Date: 28/12/15
 * Time: 5:50 PM
 */
include APPPATH . 'libraries/vendor/autoload.php';  /*Modified due to not found error*/ /*Modified by Sam*/
include 'storage/SessionStorage.php';
include 'storage/AccessTokenStorage.php';
include 'storage/AuthCodeStorage.php';
include 'storage/ClientStorage.php';
include 'storage/RefreshTokenStorage.php';
include 'storage/ScopeStorage.php';

use League\OAuth2\Server\ResourceServer;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use Symfony\Component\HttpFoundation\Request;


class Oauth
{

    public $CI;
    public $server;
    
    function __construct()
    {

        $this->CI =& get_instance();
        $this->CI->load->helper('url');

    }

    public function generateAccessToken()
    {
        
        $sessionStorage = new SessionStorage();
        $accessTokenStorage = new AccessTokenStorage();
        $clientStorage = new ClientStorage();
        $scopeStorage = new ScopeStorage();

        $this->server = new AuthorizationServer();

        // Authorization server
        $this->server->setSessionStorage(new SessionStorage);
        $this->server->setAccessTokenStorage(new AccessTokenStorage);
        $this->server->setRefreshTokenStorage(new RefreshTokenStorage);
        $this->server->setClientStorage(new ClientStorage);
        $this->server->setScopeStorage(new ScopeStorage);
        $this->server->setAuthCodeStorage(new AuthCodeStorage);

        $clientCredentials = new ClientCredentialsGrant();
        $this->server->addGrantType($clientCredentials);

        $request = $this->setRequest();
        $this->server->setRequest($request);
        $response = $this->server->issueAccessToken();
        
        return $response;
    }

    private function setRequest()
    {
        $attributes = [];
        foreach($_REQUEST as $key=>$val)
        {
            $attributes[$key] = $val;
        }

        $headers = '';
        foreach ($_SERVER as $name => $value)
        {
            $headers[$name] = $value;
        }

        $request = new Request([],[],$attributes,[],[],$headers);
       
        return $request;
    }

    public function verifyRefreshToken()
    {
        $sessionStorage = new SessionStorage();
        $accessTokenStorage = new AccessTokenStorage();
        $clientStorage = new ClientStorage();
        $scopeStorage = new ScopeStorage();

        $this->server = new ResourceServer(
            $sessionStorage,
            $accessTokenStorage,
            $clientStorage,
            $scopeStorage
        );

        $request = $this->setRequest();
        $this->server->setRequest($request);

        $user_id = '';
        if($_SERVER['HTTP_USER'])
            $user_id = $_SERVER['HTTP_USER'];

        try {
            if($this->checkAccessPermissions($request))
            {
                return $this->server->isValidRequest($user_id);
            }
            else
            {
                return false;
            }
        }catch (OAuthException $e){
            return false;
        }
    }

    private function checkAccessPermissions($request)
    {
        $currentAccessToken = $request->headers->get('Authorizationtoken');
        $oauthRefreshTokens = new RefreshTokenStorage();

        $refreshToken = $oauthRefreshTokens->query('select * from oauth_refresh_tokens where access_token_id="'.$currentAccessToken.'"');
        return count($refreshToken);
    }

    public function verifyAccessToken()
    {
        $sessionStorage = new SessionStorage();
        $accessTokenStorage = new AccessTokenStorage();
        $clientStorage = new ClientStorage();
        $scopeStorage = new ScopeStorage();

        $this->server = new ResourceServer(
            $sessionStorage,
            $accessTokenStorage,
            $clientStorage,
            $scopeStorage
        );

        $request = $this->setRequest();
        $user_id = '';
        
/*Modified by Sam*/
/*        if($_SERVER['HTTP_USER'])*/
/*Commented and modified to resolve HTTP_USER index not found error */
        if(isset($_SERVER['HTTP_USER']) && $_SERVER['HTTP_USER'])   /**/
            $user_id = $_SERVER['HTTP_USER'];
            $this->server->setRequest($request);

           //echo $server->getRequest()->headers->get('Authorization1');
           //exit;
        try {

                
            return $this->server->isValidRequest($user_id);
         //return $server->isValidRequest($user_id,'',$server->getRequest()->headers->get('Authorization1'));
        }catch (Exception $e){

            return false;
        }
    }
	public function checkscope($scopid=''){
		
            if($this->CI->config->item('rest_auth')=='oauth'){

            if ($this->server->getAccessToken()->hasScope($scopid)) {

                return true;
            }
            else{

                return false;
            }
            }
            else{
             return true;
            }

		
		
	}
}
