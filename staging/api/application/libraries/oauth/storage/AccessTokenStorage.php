<?php

use League\OAuth2\Server\Entity\AccessTokenEntity;
use League\OAuth2\Server\Entity\ScopeEntity;
use League\OAuth2\Server\Storage\AbstractStorage;
use League\OAuth2\Server\Storage\AccessTokenInterface;

class AccessTokenStorage extends AbstractStorage implements AccessTokenInterface
{
    private $db;

    function __construct()
    {
        $CI =& get_instance();
        $this->db = $CI->db;
    }

    /**
     * {@inheritdoc}
     */
    public function get($token,$user)
    {
        $query = $this->db->query('select * from oauth_access_tokens oct
                                            left join oauth_sessions os on oct.session_id=os.id
                                            left join oauth_clients oc on oc.id=os.client_id
                                            join user u on oc.user_id=u.user_id
                                            where oct.access_token="'.$token.'" and oc.user_id="'.$user.'" and u.is_active=1');
        // and oct.is_valid=1
        
        $result = $query->result_array();

        if (count($result) === 1) {

            $expire_time=$result[0]['expire_time'];
            $timeGap=SITE_ACCESS_TOKEN_EXPIRY;
            if(((time() - $expire_time) >=-$timeGap) && ((time() - $expire_time) <=0)){
                $expire_time=$expire_time+SITE_ACCESS_TOKEN_EXPIRY;
                $data_update_exptime=array("expire_time"=>$expire_time);
                $this->db->where('access_token',$result[0]['access_token']);
                $this->db->update('oauth_access_tokens',$data_update_exptime);
            }

            $token = (new AccessTokenEntity($this->server))
                        ->setId($result[0]['access_token'])
                        ->setExpireTime($expire_time);

            return $token;
        }

        return;
    }

    /**
     * {@inheritdoc}
     */
    public function getScopes(AccessTokenEntity $token)
    {
        

        /*$this->db->select('oauth_scopes.id,oauth_scopes.description');
        $this->db->from('oauth_access_token_scopes');
        $this->db->join('oauth_scopes', 'oauth_access_token_scopes.scope_id', '=', 'oauth_scopes.id');
        $this->db->where('access_token_id', $token->getId());
        $query = $this->db->get(); */

        $query=$this->db->query("select o.id,o.description from oauth_access_token_scopes ao,oauth_scopes o where ao.scope_id=o.id and o.id=ao.scope_id and ao.access_token_id='".$token->getId()."'");

        

        $result = $query->result_array();
		
        

        //select oauth_scopes.id,oauth_scopes.description from oauth_access_token_scopes,oauth_scopes  join oauth_access_token_scopes on  oauth_access_token_scopes.scope_id=oauth_scopes.id and access_token_id='X6j7bqdAXJlqrYYhC64VuYUGnDKScV7iZjqtLK4Q'


            /*$result = Capsule::table('oauth_access_token_scopes')
                                        ->select(['oauth_scopes.id', 'oauth_scopes.description'])
                                        ->join('oauth_scopes', 'oauth_access_token_scopes.scope', '=', 'oauth_scopes.id')
                                        ->where('access_token', $token->getId())
                                        ->get();*/

        $response = [];

        if (count($result) > 0) {
			
            foreach ($result as $row) {
				
                $scope = (new ScopeEntity($this->server))->hydrate([
                    'id'            =>  $row['description'],
                    'description'   =>  $row['description'],
                ]);
                $response[] = $scope;
				
            }
        }

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function create($token, $expireTime, $sessionId)
    {

       
        $this->db->insert('oauth_access_tokens',array('access_token' =>  $token,'session_id'    =>  $sessionId,'expire_time'   =>  $expireTime));

        
        /*Capsule::table('oauth_access_tokens')
                    ->insert([
                        'access_token'     =>  $token,
                        'session_id'    =>  $sessionId,
                        'expire_time'   =>  $expireTime,
                    ]);*/
    }

    /**
     * {@inheritdoc}
     */
    public function associateScope(AccessTokenEntity $token, ScopeEntity $scope)
    {
       
	  $ins_array=array('access_token_id'  =>  $token->getId(),'scope_id' =>  $scope->getId(),'created_at' => date('Y-m-d H:i:s'));
	  
	  
	  $this->db->insert('oauth_access_token_scopes',$ins_array);
	  

        /*Capsule::table('oauth_access_token_scopes')
                    ->insert([
                        'access_token'  =>  $token->getId(),
                        'scope' =>  $scope->getId(),
                    ]);*/
    }

    /**
     * {@inheritdoc}
     */
    public function delete(AccessTokenEntity $token)
    {
        $this->db->where('access_token', $token->getId());
        $this->db->delete('oauth_access_tokens');

        /*Capsule::table('oauth_access_tokens')
                    ->where('access_token', $token->getId())
                    ->delete();*/
    }
}
