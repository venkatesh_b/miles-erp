<?php
/**
 * OAuth 2.0 Resource Server
 *
 * @package     league/oauth2-server
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace League\OAuth2\Server;

use League\OAuth2\Server\Entity\AccessTokenEntity;
use League\OAuth2\Server\Exception\AccessDeniedException;
use League\OAuth2\Server\Exception\InvalidRequestException;
use League\OAuth2\Server\Storage\AccessTokenInterface;
use League\OAuth2\Server\Storage\ClientInterface;
use League\OAuth2\Server\Storage\ScopeInterface;
use League\OAuth2\Server\Storage\SessionInterface;
use League\OAuth2\Server\TokenType\Bearer;
use League\OAuth2\Server\TokenType\MAC;

/**
 * OAuth 2.0 Resource Server
 */
class ResourceServer extends AbstractServer
{
    /**
     * The access token
     *
     * @var \League\OAuth2\Server\Entity\AccessTokenEntity
     */
    protected $accessToken;

    /**
     * The query string key which is used by clients to present the access token (default: access_token)
     *
     * @var string
     */
    protected $tokenKey = 'access_token';

    /**
     * Initialise the resource server
     *
     * @param \League\OAuth2\Server\Storage\SessionInterface     $sessionStorage
     * @param \League\OAuth2\Server\Storage\AccessTokenInterface $accessTokenStorage
     * @param \League\OAuth2\Server\Storage\ClientInterface      $clientStorage
     * @param \League\OAuth2\Server\Storage\ScopeInterface       $scopeStorage
     *
     * @return self
     */
    public function __construct(
        SessionInterface $sessionStorage,
        AccessTokenInterface $accessTokenStorage,
        ClientInterface $clientStorage,
        ScopeInterface $scopeStorage
    ) {
        $this->setSessionStorage($sessionStorage);
        $this->setAccessTokenStorage($accessTokenStorage);
        $this->setClientStorage($clientStorage);
        $this->setScopeStorage($scopeStorage);

        // Set Bearer as the default token type
        $this->setTokenType(new Bearer());

        parent::__construct();

        return $this;
    }

    /**
     * Sets the query string key for the access token.
     *
     * @param string $key The new query string key
     *
     * @return self
     */
    public function setIdKey($key)
    {
        $this->tokenKey = $key;

        return $this;
    }

    /**
     * Gets the access token
     *
     * @return \League\OAuth2\Server\Entity\AccessTokenEntity
     */
    public function getAccessToken()
    {
       
		
		return $this->accessToken;
    }

    /**
     * Checks if the access token is valid or not
     *
     * @param bool   $headerOnly Limit Access Token to Authorization header
     * @param \League\OAuth2\Server\Entity\AccessTokenEntity|null $accessToken Access Token
     *
     * @throws \League\OAuth2\Server\Exception\AccessDeniedException
     * @throws \League\OAuth2\Server\Exception\InvalidRequestException
     *
     * @return bool
     */
    public function isValidRequest($user,$headerOnly = true, $accessToken = null)
    {



        $accessTokenString = ($accessToken !== null)
                                ? $accessToken
                                : $this->determineAccessToken($headerOnly);
                               
        // Set the access token
       //echo "<pre>"; print_r($accessTokenString); exit;
        $this->accessToken = $this->getAccessTokenStorage()->get($accessTokenString,$user);
		
		
		

        // Ensure the access token exists
        if (!$this->accessToken instanceof AccessTokenEntity) {
            throw new AccessDeniedException();
        }

        // Check the access token hasn't expired
        // Ensure the auth code hasn't expired
        if ($this->accessToken->isExpired() === true) {

            //if expired 
            /*
            $client_id= $this->getRequest()->headers->get('User');
           
            $CI =& get_instance();
           
            $CI->load->model("Users_model");
            
            $secret_id=$CI->Users_model->getsecret_id($client_id);

            
           
                $postdata = http_build_query(
                array(
                'client_id' => $client_id,
                'client_secret' => $secret_id,
                'grant_type' => 'client_credentials'

                )
                );


                // Set the POST options
                $opts = array('http' => 
                array (
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n"."User: $client_id\r\n",
                'content' => $postdata
                )
                );


                // Create the POST context
                $context  = stream_context_create($opts);


                // POST the data to an api
                //$url = 'http://localhost/paramesh/ci_auth2/index.php/api/login/sign';
                $url = 'http://localhost/paramesh/ci_auth2/index.php/api/login/sign';
                $actoken = (file_get_contents($url, false, $context));
                
                $response['access_token_response']=$actoken;
                
                $this->accessToken=$actoken;
                echo json_encode($response);
                exit; */
                $CI =& get_instance();
                
//                $CI->response([
//                $CI->config->item('rest_status_field_name') => FALSE,
//                $CI->config->item('rest_message_field_name') => $CI->lang->line('text_rest_accesstoken_expired')
//            ], $CI::HTTP_UNAUTHORIZED);
                
                                $CI->response([
                $CI->config->item('rest_status_field_name') => FALSE,
                'message' => $CI->lang->line('text_rest_accesstoken_expired'),
                'data' => array('message' => $CI->lang->line('text_rest_accesstoken_expired')),
            ], $CI::HTTP_UNAUTHORIZED);
                

                return false;
                
                
        }
		

        

        return true;
    }

    /**
     * Reads in the access token from the headers
     *
     * @param bool $headerOnly Limit Access Token to Authorization header
     *
     * @throws \League\OAuth2\Server\Exception\InvalidRequestException Thrown if there is no access token presented
     *
     * @return string
     */
    public function determineAccessToken($headerOnly = false)
    {

        
        if ($this->getRequest()->headers->get('Authorizationtoken') !== null) {
            $accessToken = $this->getTokenType()->determineAccessTokenInHeader($this->getRequest());
         
        } elseif ($headerOnly === false && (!$this->getTokenType() instanceof MAC)) {
            
            $accessToken = ($this->getRequest()->server->get('REQUEST_METHOD') === 'GET')
                                ? $this->getRequest()->query->get($this->tokenKey)
                                : $this->getRequest()->request->get($this->tokenKey);
            
        }
        

        if (empty($accessToken)) {

            throw new InvalidRequestException('access token');
        }

        return $accessToken;
    }
}
