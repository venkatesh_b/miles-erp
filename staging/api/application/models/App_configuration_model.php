<?php

/**
 * @Model Name App_configuration_model
 * @category        Model
 * @author          Abhilash
 * @Description     Application related configuration
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class App_configuration_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
    }
    
    public function getConfiguration($id=NULL) {
        if($id){
//            die('Modal With Id');
            $this->db->select('key name, value');
            $this->db->from('app_configuration_setting');
            $this->db->where('app_configuration_setting_id', $id);
            $res_all = $this->db->get();
            
            if($res_all){
                $res_all = $res_all->result();
            
                if($res_all){
                    return $res_all;
                }else{
                    return 'nodata';
                }
            }else{
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }else{
//            die('Modal Without Id');
            $this->db->select('app_configuration_setting_id id, key name');
            $this->db->from('app_configuration_setting');
            $res_all = $this->db->get();
            
            if($res_all){
                $res_all = $res_all->result();
            
                if($res_all){
                    return $res_all;
                }else{
                    return 'false';
                }
            }else{
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            
            
        }
    }
    
    public function getConfigurationByName($name) {
        $this->db->select('key name, value');
        $this->db->from('app_configuration_setting');
        $this->db->like('key', urldecode($name));
        $res_all = $this->db->get();
        if($res_all){
            $res_all = $res_all->result();

            if($res_all){
                return $res_all;
            }else{
                return 'nodata';
            }
        }else{
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function addConfiguration($key, $value) {
        if((isset($key) && strlen($key)>0) && (isset($value) && strlen($value)>0)){
            $this->db->select('*');
            $this->db->from('app_configuration_setting');
            $this->db->where('key', $key);
            
            $res_all = $this->db->get();
            
            if($res_all){
                $res_all = $res_all->result();
            
                if($res_all){
                    return 'duplicate';
                }else{
                    $data_insert = array(
                        'key' => $key,
                        'value' => $value
                    );
                    
                    $res_insert = $this->db->insert('app_configuration_setting', $data_insert);
                    if($this->db->error()['code'] == '0'){
                        if ($res_insert) {
                            return true;
                        } else {
                            return false;
                        }
                    }else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }
            }else{
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }else{
            return false;
        }
    }
    
    public function editConfiguration($id, $key, $value) {
        
        if((isset($id) && strlen($id)>0)
                && ((isset($key) && strlen($key)>0)
                        || (isset($value) && strlen($value)>0))){
            
            $this->db->select('*');
            $this->db->from('app_configuration_setting');
            $this->db->where('app_configuration_setting_id', $id);
            $res_all=$this->db->get();

            if($res_all){
                
                $res_all = $res_all->result();
            
                if($res_all && count($res_all) >= 1){

                    $this->db->select('*');
                    $this->db->from('app_configuration_setting');
                    $this->db->where('key', $key);
                    
                    $res_all=$this->db->get();
            
                    if($res_all){
                        $res_all=$res_all->result();
                        if($res_all){

                            return 'duplicate';
                        }else{

                            $data_update = array(
                                'key' => $key,
                                'value' => $value
                            );

                            $this->db->where('app_configuration_setting_id', $id);
                            $res_update=$this->db->update('app_configuration_setting', $data_update);
                            if($this->db->error()['code'] == '0'){
                                if ($res_update) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }else {
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                        }
                    }else{
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }

                }else{
                    return 'badrequest';
                }
            }else{
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }else{
            return false;
        }
    }
    
    public function deleteConfiguration($id) {
        
        $this->db->select('*');
        $this->db->from('app_configuration_setting');
        $this->db->where('app_configuration_setting_id', $id);
        
        $res_all=$this->db->get();

        if($res_all){
            $res_all = $res_all->result();
        
            if($res_all){

                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );

                $this->db->where('app_configuration_setting_id', $id);
                $res_update=$this->db->update('app_configuration_setting', $data_update);

                if($this->db->error()['code'] == '0'){
                    if ($res_update) {
                        if($is_active){
                            return 'active';
                        }else{
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                }else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
                

            }else{
                return 'nodata';
            }
        }else{
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
}