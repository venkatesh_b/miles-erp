<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @Model Name      Batch
 * @category        Model
 * @author          Abhilash
 * @Description     For Batch CRUD Services
 */
class Batch_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    /**
     * Function get All batches
     * 
     * @return - array
     * @author : Abhilash
     */
    public function allBatches() {
        $this->db->select('*');
        $this->db->from('batch');

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
//                return $res_all;
                $upcomingBatch = [];
                $currentBatch = [];
                $completedBatch = [];
                //$value->fk_course_id
                foreach ($res_all as $key => $value) {
//                    print_r($value);die;
                    $this->db->select('*');
                    $this->db->from('course');
                    $this->db->where('course_id', $value->fk_course_id);
                    $this->db->where('is_active', 1);
                    $course = $this->db->get();
                    if ($course) {
                        $course = $course->result();
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    $start = strtotime($value->acedemic_startdate);
                    $end = strtotime($value->acedemic_enddate);
                    $current = strtotime(date('Y-m-d'));
                    if ($start > $current && $end > $current) {
                        //upcoming Batch
                        $upcomingBatch[] = [
                            'batch_id' => $value->batch_id,
                            'fk_branch_id' => $value->fk_branch_id,
                            'fk_course_id' => $value->fk_course_id,
                            'courseName' => $course[0]->name,
                            'courseDescription' => $course[0]->description,
                            'code' => $value->code,
//                            'marketing_startdate' => (new DateTime($value->marketing_startdate))->format('d M,Y'),
                            'marketing_startdate' => (new DateTime($value->marketing_startdate))->format('d M,Y'),
                            'marketing_enddate' => (new DateTime($value->marketing_enddate))->format('d M,Y'),
                            'target' => $value->target,
                            'acedemic_startdate' => (new DateTime($value->acedemic_startdate))->format('d M,Y'),
                            'acedemic_enddate' => (new DateTime($value->acedemic_enddate))->format('d M,Y'),
                            'fk_created_by' => $value->fk_created_by,
                            'created_date' => $value->created_date,
                        ];
                    } else if ($start < $current && $end > $current) {
                        //Current
                        $currentBatch[] = [
                            'batch_id' => $value->batch_id,
                            'fk_branch_id' => $value->fk_branch_id,
                            'fk_course_id' => $value->fk_course_id,
                            'courseName' => $course[0]->name,
                            'courseDescription' => $course[0]->description,
                            'code' => $value->code,
                            'marketing_startdate' => (new DateTime($value->marketing_startdate))->format('d M,Y'),
                            'marketing_enddate' => (new DateTime($value->marketing_enddate))->format('d M,Y'),
                            'target' => $value->target,
                            'acedemic_startdate' => (new DateTime($value->acedemic_startdate))->format('d M,Y'),
                            'acedemic_enddate' => (new DateTime($value->acedemic_enddate))->format('d M,Y'),
                            'fk_created_by' => $value->fk_created_by,
                            'created_date' => $value->created_date,
                        ];
                    } else if ($start < $current && $end < $current) {
                        //Completed
                        $completedBatch[] = [
                            'batch_id' => $value->batch_id,
                            'fk_branch_id' => $value->fk_branch_id,
                            'fk_course_id' => $value->fk_course_id,
                            'courseName' => $course[0]->name,
                            'courseDescription' => $course[0]->description,
                            'code' => $value->code,
                            'marketing_startdate' => (new DateTime($value->marketing_startdate))->format('d M,Y'),
                            'marketing_enddate' => (new DateTime($value->marketing_enddate))->format('d M,Y'),
                            'target' => $value->target,
                            'acedemic_startdate' => (new DateTime($value->acedemic_startdate))->format('d M,Y'),
                            'acedemic_enddate' => (new DateTime($value->acedemic_enddate))->format('d M,Y'),
                            'fk_created_by' => $value->fk_created_by,
                            'created_date' => $value->created_date,
                        ];
                    }
                }
                return [
                    'upcoming' => array_values($upcomingBatch),
                    'current' => array_values($currentBatch),
                    'completed' => array_values($completedBatch),
                ];
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /**
     * Function get Batch datatables
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getBatchDataTableDetails($id, $name) {
        $id = decode($id);
        if ($name == 'upcoming') {
            $table = 'batch';
            // Table's primary key
            $primaryKey = 'b`.`batch_id';
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
            $columns = array(
                array('db' => 'CONCAT_WS("-",br.code,c.name,"M7",`b`.`code`) as code', 'dt' => 'code', 'field' => 'code'),
                array('db' => '`c`.`name` as `course`', 'dt' => 'course', 'field' => 'course'),
                array('db' => '`b`.`acedemic_startdate`', 'dt' => 'acedemic_startdate', 'field' => 'acedemic_startdate', 'formatter' => function( $d, $row ) {
                        return date('d M,Y', strtotime($d));
                    }),
                array('db' => '`b`.`marketing_startdate`', 'dt' => 'marketing_startdate', 'field' => 'marketing_startdate', 'formatter' => function( $d, $row ) {
                        return date('d M,Y', strtotime($d));
                    }),
                array('db' => '`b`.`marketing_enddate`', 'dt' => 'marketing_enddate', 'field' => 'marketing_enddate', 'formatter' => function( $d, $row ) {
                        return date('d M,Y', strtotime($d));
                    }),
                array('db' => "(select count(*)from branch_xref_lead bxl1
                                    JOIN lead l1 ON bxl1.lead_id=l1.lead_id
                                    JOIN batch b1 ON bxl1.fk_batch_id=b1.batch_id
                                    WHERE
                                    bxl1.is_active=1 
                                    AND bxl1.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                                    AND bxl1.branch_id=b.fk_branch_id 
                                    AND bxl1.fk_course_id=b.fk_course_id 
                                    AND bxl1.fk_batch_id=b.batch_id) as dropout", 'dt' => 'dropout', 'field' => 'dropout'),
                array('db' => '(select IFNULL(sum(cfi1.amount_payable-cfi1.amount_paid+cfi1.refunded_other_amount),0) AS dueAmount
                                    from branch_xref_lead bxl1
                                    left join lead l1 ON l1.lead_id=bxl1.lead_id
                                    LEFT JOIN candidate_fee cf1 ON bxl1.branch_xref_lead_id=cf1.fk_branch_xref_lead_id
                                    LEFT JOIN candidate_fee_item cfi1 ON cf1.candidate_fee_id=cfi1.fk_candidate_fee_id
                                   WHERE bxl1.branch_id=b.fk_branch_id
                                   AND bxl1.fk_course_id=b.fk_course_id
                                   AND bxl1.fk_batch_id=b.batch_id) as due', 'dt' => 'due', 'field' => 'due'),
                array('db' => '`b`.`target`', 'dt' => 'target', 'field' => 'target'),
                array('db' => '(select count(*) from branch_xref_lead bxl1
                                JOIN lead l1 ON bxl1.lead_id=l1.lead_id
                                WHERE
                                bxl1.is_active=1 
                                AND bxl1.branch_id=b.fk_branch_id 
                                AND bxl1.fk_course_id=b.fk_course_id
                                AND bxl1.fk_batch_id=b.batch_id) as enrolled', 'dt' => 'enrolled', 'field' => 'enrolled'),
                array('db' => '`b`.`batch_id`', 'dt' => 'batch_id', 'field' => 'batch_id'),
                array('db' => 'if(`b`.`is_active`=1,1,0) as is_active', 'dt' => 'is_active', 'field' => 'is_active'),
                array('db' => 'if(`br`.`is_active`=1,1,0) as `branch_active`', 'dt' => 'branch_active', 'field' => 'branch_active'),
                array('db' => 'if(`c`.`is_active`=1,1,0) as `course_active`', 'dt' => 'course_active', 'field' => 'course_active'),
                array('db' => 'if(`bxc`.`is_active`=1,1,0) as `b_course_active`', 'dt' => 'b_course_active', 'field' => 'b_course_active')
            );

            $globalFilterColumns = array(
                array('db' => 'CONCAT_WS("-",br.code,c.name,"M7",`b`.`code`)', 'dt' => 'courseName', 'field' => 'courseName'),
                array('db' => '`c`.`name`', 'dt' => 'course', 'field' => 'course'),
                array('db' => '`b`.`acedemic_startdate`', 'dt' => 'inst_name', 'field' => 'inst_name'),
                array('db' => '`b`.`marketing_startdate`', 'dt' => 'type_name', 'field' => 'type_name'),
                array('db' => '`b`.`marketing_enddate`', 'dt' => 'inst_name', 'field' => 'inst_name'),
                array('db' => '`b`.`target`', 'dt' => 'target', 'field' => 'target'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => SITE_HOST_USERNAME,
                'pass' => SITE_HOST_PASSWORD,
                'db' => SITE_DATABASE,
                'host' => SITE_HOST_NAME
            );
            /*             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = " FROM batch b left join course c on b.fk_course_id=c.course_id left join branch br on b.fk_branch_id=br.branch_id LEFT JOIN branch_xref_course bxc ON (b.fk_branch_id=bxc.fk_branch_id AND b.fk_course_id=bxc.fk_course_id) JOIN branch_xref_user bxu ON b.fk_branch_id=bxu.branch_id";
            $extraWhere = "b.fk_branch_id=" . $id . " AND b.marketing_startdate>DATE(NOW()) AND b.marketing_enddate>DATE(NOW()) AND bxu.user_id=" . $_SERVER['HTTP_USER'];
            $groupBy = "";

            $responseData = (SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere, $groupBy));

            for ($i = 0; $i < count($responseData['data']); $i++) {
                $responseData['data'][$i]['counter'] = count($responseData['data']);
            }
            return $responseData;
        } else if ($name == 'current') {
            $table = 'batch';
            // Table's primary key
            $primaryKey = 'b`.`batch_id';
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
            $columns = array(
                array('db' => 'CONCAT_WS("-",br.code,c.name,"M7",`b`.`code`) as code', 'dt' => 'code', 'field' => 'code'),
                array('db' => '`c`.`name` as `course`', 'dt' => 'course', 'field' => 'course'),
                array('db' => '`b`.`acedemic_startdate`', 'dt' => 'acedemic_startdate', 'field' => 'acedemic_startdate', 'formatter' => function( $d, $row ) {
                        return date('d M,Y', strtotime($d));
                    }),
                array('db' => '`b`.`marketing_startdate`', 'dt' => 'marketing_startdate', 'field' => 'marketing_startdate', 'formatter' => function( $d, $row ) {
                        return date('d M,Y', strtotime($d));
                    }),
                array('db' => '`b`.`marketing_enddate`', 'dt' => 'marketing_enddate', 'field' => 'marketing_enddate', 'formatter' => function( $d, $row ) {
                        return date('d M,Y', strtotime($d));
                    }),
                array('db' => "(select count(*)from branch_xref_lead bxl1
                                    JOIN lead l1 ON bxl1.lead_id=l1.lead_id
                                    JOIN batch b1 ON bxl1.fk_batch_id=b1.batch_id
                                    WHERE
                                    bxl1.is_active=1 
                                    AND bxl1.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                                    AND bxl1.branch_id=b.fk_branch_id 
                                    AND bxl1.fk_course_id=b.fk_course_id 
                                    AND bxl1.fk_batch_id=b.batch_id) as dropout", 'dt' => 'dropout', 'field' => 'dropout'),
                array('db' => '(select IFNULL(sum(cfi1.amount_payable-cfi1.amount_paid+cfi1.refunded_other_amount),0) AS dueAmount
                                    from branch_xref_lead bxl1
                                    left join lead l1 ON l1.lead_id=bxl1.lead_id
                                    LEFT JOIN candidate_fee cf1 ON bxl1.branch_xref_lead_id=cf1.fk_branch_xref_lead_id
                                    LEFT JOIN candidate_fee_item cfi1 ON cf1.candidate_fee_id=cfi1.fk_candidate_fee_id
                                   WHERE bxl1.branch_id=b.fk_branch_id
                                   AND bxl1.fk_course_id=b.fk_course_id
                                   AND bxl1.fk_batch_id=b.batch_id) as due', 'dt' => 'due', 'field' => 'due'),
                array('db' => '`b`.`target`', 'dt' => 'target', 'field' => 'target'),
                array('db' => '(select count(*) from branch_xref_lead bxl1
                                JOIN lead l1 ON bxl1.lead_id=l1.lead_id
                                WHERE
                                bxl1.is_active=1 
                                AND bxl1.branch_id=b.fk_branch_id 
                                AND bxl1.fk_course_id=b.fk_course_id
                                AND bxl1.fk_batch_id=b.batch_id) as enrolled', 'dt' => 'enrolled', 'field' => 'enrolled'),
                array('db' => '`b`.`batch_id`', 'dt' => 'batch_id', 'field' => 'batch_id'),
                array('db' => 'if(`b`.`is_active`=1,1,0) as is_active', 'dt' => 'is_active', 'field' => 'is_active'),
                array('db' => 'if(`br`.`is_active`=1,1,0) as `branch_active`', 'dt' => 'branch_active', 'field' => 'branch_active'),
                array('db' => 'if(`c`.`is_active`=1,1,0) as `course_active`', 'dt' => 'course_active', 'field' => 'course_active'),
                array('db' => 'if(`bxc`.`is_active`=1,1,0) as `b_course_active`', 'dt' => 'b_course_active', 'field' => 'b_course_active')
            );

            $globalFilterColumns = array(
                array('db' => 'CONCAT_WS("-",c.name,"M7",`b`.`code`)', 'dt' => 'courseName', 'field' => 'courseName'),
                array('db' => '`c`.`name`', 'dt' => 'course', 'field' => 'course'),
                array('db' => '`b`.`acedemic_startdate`', 'dt' => 'inst_name', 'field' => 'inst_name'),
                array('db' => '`b`.`marketing_startdate`', 'dt' => 'type_name', 'field' => 'type_name'),
                array('db' => '`b`.`marketing_enddate`', 'dt' => 'inst_name', 'field' => 'inst_name'),
                array('db' => '`b`.`target`', 'dt' => 'target', 'field' => 'target'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => SITE_HOST_USERNAME,
                'pass' => SITE_HOST_PASSWORD,
                'db' => SITE_DATABASE,
                'host' => SITE_HOST_NAME
            );
            /*             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = " FROM batch b left join course c on b.fk_course_id=c.course_id left join branch br on b.fk_branch_id=br.branch_id LEFT JOIN branch_xref_course bxc ON (b.fk_branch_id=bxc.fk_branch_id AND b.fk_course_id=bxc.fk_course_id) JOIN branch_xref_user bxu ON b.fk_branch_id=bxu.branch_id";
            $extraWhere = "b.fk_branch_id=" . $id . " AND b.marketing_startdate<=DATE(NOW()) AND b.marketing_enddate>=DATE(NOW()) AND bxu.user_id=" . $_SERVER['HTTP_USER'];
            $groupBy = "";

            $responseData = (SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere, $groupBy));

            for ($i = 0; $i < count($responseData['data']); $i++) {
                $responseData['data'][$i]['counter'] = count($responseData['data']);
            }
            return $responseData;
        } else if ($name == 'completed') {
            $table = 'batch';
            // Table's primary key
            $primaryKey = 'b`.`batch_id';
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
            $columns = array(
                array('db' => 'CONCAT_WS("-",br.code,c.name,"M7",`b`.`code`) as code', 'dt' => 'code', 'field' => 'code'),
                array('db' => '`c`.`name` as `course`', 'dt' => 'course', 'field' => 'course'),
                array('db' => '`b`.`acedemic_startdate`', 'dt' => 'acedemic_startdate', 'field' => 'acedemic_startdate', 'formatter' => function( $d, $row ) {
                        return date('d M,Y', strtotime($d));
                    }),
                array('db' => '`b`.`marketing_startdate`', 'dt' => 'marketing_startdate', 'field' => 'marketing_startdate', 'formatter' => function( $d, $row ) {
                        return date('d M,Y', strtotime($d));
                    }),
                array('db' => '`b`.`marketing_enddate`', 'dt' => 'marketing_enddate', 'field' => 'marketing_enddate', 'formatter' => function( $d, $row ) {
                        return date('d M,Y', strtotime($d));
                    }),
                array('db' => "(select count(*)from branch_xref_lead bxl1
                                    JOIN lead l1 ON bxl1.lead_id=l1.lead_id
                                    JOIN batch b1 ON bxl1.fk_batch_id=b1.batch_id
                                    WHERE
                                    bxl1.is_active=1 
                                    AND bxl1.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                                    AND bxl1.branch_id=b.fk_branch_id 
                                    AND bxl1.fk_course_id=b.fk_course_id 
                                    AND bxl1.fk_batch_id=b.batch_id) as dropout", 'dt' => 'dropout', 'field' => 'dropout'),
                array('db' => '(select IFNULL(sum(cfi1.amount_payable-cfi1.amount_paid+cfi1.refunded_other_amount),0) AS dueAmount
                                    from branch_xref_lead bxl1
                                    left join lead l1 ON l1.lead_id=bxl1.lead_id
                                    LEFT JOIN candidate_fee cf1 ON bxl1.branch_xref_lead_id=cf1.fk_branch_xref_lead_id
                                    LEFT JOIN candidate_fee_item cfi1 ON cf1.candidate_fee_id=cfi1.fk_candidate_fee_id
                                   WHERE bxl1.branch_id=b.fk_branch_id
                                   AND bxl1.fk_course_id=b.fk_course_id
                                   AND bxl1.fk_batch_id=b.batch_id) as due', 'dt' => 'due', 'field' => 'due'),
                array('db' => '`b`.`target`', 'dt' => 'target', 'field' => 'target'),
                array('db' => '(select count(*) from branch_xref_lead bxl1
                                JOIN lead l1 ON bxl1.lead_id=l1.lead_id
                                WHERE
                                bxl1.is_active=1 
                                AND bxl1.branch_id=b.fk_branch_id 
                                AND bxl1.fk_course_id=b.fk_course_id
                                AND bxl1.fk_batch_id=b.batch_id) as enrolled', 'dt' => 'enrolled', 'field' => 'enrolled'),
                array('db' => '`b`.`batch_id`', 'dt' => 'batch_id', 'field' => 'batch_id'),
                array('db' => 'if(`b`.`is_active`=1,1,0) as is_active', 'dt' => 'is_active', 'field' => 'is_active'),
                array('db' => 'if(`br`.`is_active`=1,1,0) as `branch_active`', 'dt' => 'branch_active', 'field' => 'branch_active'),
                array('db' => 'if(`c`.`is_active`=1,1,0) as `course_active`', 'dt' => 'course_active', 'field' => 'course_active'),
                array('db' => 'if(`bxc`.`is_active`=1,1,0) as `b_course_active`', 'dt' => 'b_course_active', 'field' => 'b_course_active')
            );

            $globalFilterColumns = array(
                array('db' => 'CONCAT_WS("-",c.name,"M7",`b`.`code`)', 'dt' => 'courseName', 'field' => 'courseName'),
                array('db' => '`c`.`name`', 'dt' => 'course', 'field' => 'course'),
                array('db' => '`b`.`acedemic_startdate`', 'dt' => 'inst_name', 'field' => 'inst_name'),
                array('db' => '`b`.`marketing_startdate`', 'dt' => 'type_name', 'field' => 'type_name'),
                array('db' => '`b`.`marketing_enddate`', 'dt' => 'inst_name', 'field' => 'inst_name'),
                array('db' => '`b`.`target`', 'dt' => 'target', 'field' => 'target'),
            );

            // SQL server connection information
            $sql_details = array(
                'user' => SITE_HOST_USERNAME,
                'pass' => SITE_HOST_PASSWORD,
                'db' => SITE_DATABASE,
                'host' => SITE_HOST_NAME
            );
            /*             * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = " FROM batch b left join course c on b.fk_course_id=c.course_id left join branch br on b.fk_branch_id=br.branch_id LEFT JOIN branch_xref_course bxc ON (b.fk_branch_id=bxc.fk_branch_id AND b.fk_course_id=bxc.fk_course_id) JOIN branch_xref_user bxu ON b.fk_branch_id=bxu.branch_id";
            $extraWhere = "b.fk_branch_id=" . $id . " AND b.marketing_startdate<DATE(NOW()) AND b.marketing_enddate<DATE(NOW()) AND bxu.user_id=" . $_SERVER['HTTP_USER'];
            $groupBy = "";

            $responseData = (SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere, $groupBy));

            for ($i = 0; $i < count($responseData['data']); $i++) {
                $responseData['data'][$i]['counter'] = count($responseData['data']);
            }
            return $responseData;
        }
    }

    /**
     * Function get Batch dashboard by batch id
     * 
     * @return - array
     * @author : Abhilash
     */
    public function batchesDashboardByID($id) {
        $this->db->select('*');
        $this->db->from('batch');
        $this->db->where('fk_branch_id', decode($id));

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $upcomingBatch = [];
                $currentBatch = [];
                $completedBatch = [];
                foreach ($res_all as $key => $value) {
                    $this->db->select('*');
                    $this->db->from('course');
                    $this->db->where('course_id', $value->fk_course_id);
                    $this->db->where('is_active', 1);
                    $course = $this->db->get();
                    if ($course) {
                        $course = $course->result();
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    $start = strtotime($value->acedemic_startdate);
                    $end = strtotime($value->acedemic_enddate);
                    $current = strtotime(date('Y-m-d'));
                    if ($start > $current && $end > $current) {
                        //upcoming Batch
                        $upcomingBatch[] = [
                            'batch_id' => $value->batch_id,
                            'fk_branch_id' => $value->fk_branch_id,
                            'fk_course_id' => $value->fk_course_id,
                            'courseName' => $course[0]->name,
                            'courseDescription' => $course[0]->description,
                            'code' => $value->code,
                            'marketing_startdate' => (new DateTime($value->marketing_startdate))->format('d M,Y'),
                            'marketing_enddate' => (new DateTime($value->marketing_enddate))->format('d M,Y'),
                            'target' => $value->target,
                            'acedemic_startdate' => (new DateTime($value->acedemic_startdate))->format('d M,Y'),
                            'acedemic_enddate' => (new DateTime($value->acedemic_enddate))->format('d M,Y'),
                            'fk_created_by' => $value->fk_created_by,
                            'created_date' => $value->created_date,
                            'is_active' => $value->is_active,
                        ];
                    } else if ($start < $current && $end > $current) {
                        //Current
                        $currentBatch[] = [
                            'batch_id' => $value->batch_id,
                            'fk_branch_id' => $value->fk_branch_id,
                            'fk_course_id' => $value->fk_course_id,
                            'courseName' => $course[0]->name,
                            'courseDescription' => $course[0]->description,
                            'code' => $value->code,
                            'marketing_startdate' => (new DateTime($value->marketing_startdate))->format('d M,Y'),
                            'marketing_enddate' => (new DateTime($value->marketing_enddate))->format('d M,Y'),
                            'target' => $value->target,
                            'acedemic_startdate' => (new DateTime($value->acedemic_startdate))->format('d M,Y'),
                            'acedemic_enddate' => (new DateTime($value->acedemic_enddate))->format('d M,Y'),
                            'fk_created_by' => $value->fk_created_by,
                            'created_date' => $value->created_date,
                            'is_active' => $value->is_active,
                        ];
                    } else if ($end < $current) {
                        //Completed
                        $completedBatch[] = [
                            'batch_id' => $value->batch_id,
                            'fk_branch_id' => $value->fk_branch_id,
                            'fk_course_id' => $value->fk_course_id,
                            'courseName' => $course[0]->name,
                            'courseDescription' => $course[0]->description,
                            'code' => $value->code,
                            'marketing_startdate' => (new DateTime($value->marketing_startdate))->format('d M,Y'),
                            'marketing_enddate' => (new DateTime($value->marketing_enddate))->format('d M,Y'),
                            'target' => $value->target,
                            'acedemic_startdate' => (new DateTime($value->acedemic_startdate))->format('d M,Y'),
                            'acedemic_enddate' => (new DateTime($value->acedemic_enddate))->format('d M,Y'),
                            'fk_created_by' => $value->fk_created_by,
                            'created_date' => $value->created_date,
                            'is_active' => $value->is_active,
                        ];
                    }
                }
                return [
                    'upcoming' => array_values($upcomingBatch),
                    'current' => array_values($currentBatch),
                    'completed' => array_values($completedBatch),
                ];
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /**
     * Function get Batch by id
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getBatchById($id, $branchId) {
        $this->db->select('*');
        $this->db->from('batch');
        $this->db->where('batch_id', $id);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                foreach ($res_all as $key => $value) {
                    $this->db->select('*');
                    $this->db->from('course');
                    $this->db->where('course_id', $value->fk_course_id);
                    $this->db->where('is_active', 1);
                    $course = $this->db->get();
                    if ($course) {
                        $course = $course->result();
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }

                    $this->db->select('code as branchCode');
                    $this->db->from('branch');
                    $this->db->where('branch_id', $value->fk_branch_id);
                    $this->db->where('is_active', 1);
                    $branchcoderes = $this->db->get();
                    if ($branchcoderes) {
                        if($branchcoderes->num_rows()>0) {
                            $branchCode = $branchcoderes->result();
                            $branchCode=$branchCode[0]->branchCode;
                        }
                        else{
                            $branchCode='';
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }

                    $start = strtotime($value->marketing_startdate);
                    $end = strtotime($value->marketing_enddate);
                    $current = strtotime(date('Y-m-d'));
                    if ($start > $current && $end > $current) {
                        $status = 'Upcoming';
                    } else if ($start <= $current && $end >= $current) {
                        $status = 'In-Progress';
                    } else if ($end < $current) {
                        $status = 'Completed';
                    }
                    /*Due/overdue*/
                    $amountPayable = 0;
                    $amountPaid = 0;
                    $query = $this->db->query("call Sp_Get_Due_List_Report('{$branchId}',null,{$id},null,null,null)");
                    if ($query) {
                        if ($query->num_rows() > 0) {
                            $duelist = $query->result();
                            foreach ($duelist as $key => $v) {
                                $amountPayable += $v->amountPayable;
                                $amountPaid += $v->amountPaid;
                            }
                        }
                    }
                    $amountDue = $amountPayable - $amountPaid;
                    $this->db->close();
                    $this->load->database();
                    $amountDuePercetage = $amountPaid != 0 || $amountPayable != 0 ? number_format((float) (($amountDue / $amountPayable) * 100), 2, '.', '') . ' %' : '0 %';
                    
                    /*For Students*/
                    $qry = "SELECT (select count(*) from branch_xref_lead bxl
                                left join lead_stage l ON bxl.`status`=l.lead_stage_id
                                left join lead le ON bxl.lead_id=le.lead_id
                              where bxl.is_active=1
                                AND bxl.branch_id={$branchId} AND bxl.fk_batch_id={$id}) AS enrolled,
                            (select count(*) from branch_xref_lead bxl
                                left join lead_stage l ON bxl.`status`=l.lead_stage_id
                                left join lead le ON bxl.lead_id=le.lead_id
                              where l.lead_stage='M7'
                                AND bxl.is_active=1
                                AND bxl.branch_id={$branchId} AND bxl.fk_batch_id={$id}) AS dropouts";
                    $res_all = $this->db->query($qry);
                    if ($res_all) {
                        $res_all = $res_all->result();
                        if (count($res_all) > 0) {
                            $enrolled = $res_all[0]->enrolled;
                            $dropouts = $res_all[0]->dropouts;
                        } else {
                            //nodata
                            $enrolled = 0;
                            $dropouts = 0;
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    /*For Classes*/
                    $current = 0;
                    $old = 0;
                    $facultyName = '';
                    $venueName = '';
                    /*current class*/
                    $qry = "select bxc.class_date current, v.name as venue_name,bxc.fk_venue_id, f.name faculty_name,bxc.fk_faculty_id from batch_xref_class bxc
                                LEFT JOIN faculty f ON bxc.fk_faculty_id=f.faculty_id
                                LEFT JOIN venue v ON bxc.fk_venue_id=v.venue_id
                            where (bxc.class_date=DATE(NOW()) OR bxc.class_date>DATE(NOW())) AND bxc.fk_batch_id={$id} AND bxc.is_active=1 limit 0,1";
                    $res_all = $this->db->query($qry);
                    if ($res_all) {
                        $res_all = $res_all->result();
                        if (count($res_all) > 0) {
                            $current = $res_all[0]->current;
                            $facultyName = $res_all[0]->faculty_name;
                            $venueName = $res_all[0]->venue_name;
                        } 
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    /*Old class*/
                    $qry = "select bxc.class_date old, v.name as venue_name,bxc.fk_venue_id, f.name faculty_name,bxc.fk_faculty_id from batch_xref_class bxc
                                LEFT JOIN faculty f ON bxc.fk_faculty_id=f.faculty_id
                                LEFT JOIN venue v ON bxc.fk_venue_id=v.venue_id
                            where bxc.class_date<DATE(NOW()) AND bxc.fk_batch_id={$id} AND bxc.is_active=1 order by bxc.class_date DESC limit 0,1";
                    $res_all = $this->db->query($qry);
                    if ($res_all) {
                        $res_all = $res_all->result();
                        if (count($res_all) > 0) {
                            $old = $res_all[0]->old;
                        } 
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    $upcoming = 0;
                    $qry = "select bxc.class_date upcoming from batch_xref_class bxc
                        where bxc.class_date>DATE(NOW()) AND bxc.fk_batch_id={$id} AND bxc.is_active=1 limit 0,1";
                    $res_all = $this->db->query($qry);
                    if ($res_all) {
                        $res_all = $res_all->result();
                        if (count($res_all) > 0) {
                            $upcoming = $res_all[0]->upcoming;
                        } else {
                            //nodata
                            $upcoming = 0;
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    /*For inventory*/
                    $stock = 0;
                    $issued = 0;
                    $pending = 0;
                    
                    $qry = "select (select if(sum(si.quantity-si.returned_quantity)>0,sum(si.quantity-si.returned_quantity),0) from product p
                                LEFT JOIN product_xref_course pxc ON p.product_id=pxc.fk_product_id
                                LEFT JOIN stockflow_item si ON p.product_id=si.fk_product_id
                                LEFT JOIN stockflow s ON si.fk_stockflow_id=s.stockflow_id
                                LEFT JOIN batch b ON b.fk_course_id=pxc.fk_course_id
                                WHERE s.type='GRN' 
                                and s.receiver_mode='branch' 
                                AND b.batch_id={$id}
                                AND s.fk_receiver_id=b.fk_branch_id) -
                            (select if(sum(si.quantity-si.returned_quantity)>0,sum(si.quantity-si.returned_quantity),0) from product p
                                LEFT JOIN product_xref_course pxc ON p.product_id=pxc.fk_product_id
                                LEFT JOIN stockflow_item si ON p.product_id=si.fk_product_id
                                LEFT JOIN stockflow s ON si.fk_stockflow_id=s.stockflow_id
                                LEFT JOIN batch b ON b.fk_course_id=pxc.fk_course_id
                                WHERE s.type='GTN' 
                                and s.requested_mode='branch' 
                                AND b.batch_id={$id}
                                AND s.fk_requested_id=b.fk_branch_id) AS total";
                    $res_all = $this->db->query($qry);
                    if ($res_all) {
                        $res_all = $res_all->result();
                        if (count($res_all) > 0) {
                            $stock = $res_all[0]->total;
                        } else {
                            //nodata
                            $stock = 0;
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    
                    $qry = "SELECT (select count(DISTINCT(cfi1.candidate_fee_item_id)) from fee_collection fc1
                                JOIN fee_collection_item fci1 ON fc1.fee_collection_id=fci1.fk_fee_collection_id
                                JOIN company co1 ON fc1.fk_fee_company_id=co1.company_id
                                JOIN branch_xref_lead bxl2 ON fc1.fk_branch_xref_lead_id=bxl2.branch_xref_lead_id
                                JOIN candidate_fee cf1 ON bxl2.branch_xref_lead_id=cf1.fk_branch_xref_lead_id
                                JOIN candidate_fee_item cfi1 ON cf1.candidate_fee_id=cfi1.fk_candidate_fee_id
                                where bxl2.fk_batch_id={$id}
                                AND co1.name='publication'
                                AND cfi1.amount_payable=cfi1.amount_paid) as student,
                            (SELECT  sum(`pc`.`quantity`)
                                FROM `batch` `b`
                                JOIN `product_xref_course` `pc` ON `pc`.`fk_course_id`=`b`.`fk_course_id`
                                JOIN `product` `p` ON `p`.`product_id`=`pc`.`fk_product_id`
                                WHERE `b`.`batch_id` = {$id}
                                AND `pc`.`is_active` =0) as quantity,
                            (select sum(li.quantity) from lead_item_issue li 
                                where li.fk_batch_id={$id}) as issued";
                    $res_all = $this->db->query($qry);
                    if ($res_all) {
                        $res_all = $res_all->result();
                        if (count($res_all) > 0) {
                            $student = $res_all[0]->student;
                            $quantity = $res_all[0]->quantity;
                            $issued = $res_all[0]->issued;
                            $pending = ($student * $quantity) - $issued;
                        } else {
                            //nodata
                            $student = 0;
                            $quantity = 0;
                            $issued = 0;
                            $pending = 0;
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    
                    return [
                        'batch_id' => $value->batch_id,
                        'alias_name' => $value->alias_name,
                        'fk_branch_id' => $value->fk_branch_id,
                        'fk_course_id' => $value->fk_course_id,
                        'courseName' => $course[0]->name,
                        'status' => $status,
                        'courseDescription' => $course[0]->description,
                        'code' => $value->code,
                        'marketing_startdate' => ($this->checkDateValidity($value->marketing_startdate) == 'progress' ||
                        $this->checkDateValidity($value->marketing_startdate) == 'completed') ? (new DateTime($value->marketing_startdate))->format('d F,Y') : (new DateTime($value->marketing_startdate))->format('Y-m-d'),
                        'marketing_start' => (new DateTime($value->marketing_startdate))->format('d M,Y'),
                        'marketing_enddate' => ($this->checkDateValidity($value->marketing_enddate) == 'progress' ||
                        $this->checkDateValidity($value->marketing_enddate) == 'completed') ? (new DateTime($value->marketing_enddate))->format('d M,Y') : (new DateTime($value->marketing_enddate))->format('Y-m-d'),
                        'marketing_end' => (new DateTime($value->marketing_enddate))->format('d M,Y'),
                        'target' => $value->target,
                        'visitor_target' => $value->visitor_target,
                        'acedemic_startdate' => ($this->checkDateValidity($value->acedemic_startdate) == 'progress' ||
                        $this->checkDateValidity($value->acedemic_startdate) == 'completed') ? (new DateTime($value->acedemic_startdate))->format('d M,Y') : (new DateTime($value->acedemic_startdate))->format('Y-m-d'),
                        'acedemic_start' => (new DateTime($value->acedemic_startdate))->format('d M,Y'),
                        'acedemic_enddate' => ($this->checkDateValidity($value->acedemic_enddate) == 'progress' ||
                        $this->checkDateValidity($value->acedemic_enddate) == 'completed') ? (new DateTime($value->acedemic_enddate))->format('d M,Y') : (new DateTime($value->acedemic_enddate))->format('Y-m-d'),
                        'acedemic_end' => (new DateTime($value->acedemic_enddate))->format('d M,Y'),
                        'marketing_startdate_status' => $this->checkDateValidity($value->marketing_startdate),
                        'marketing_enddate_status' => $this->checkDateValidity($value->marketing_enddate),
                        'acedemic_startdate_status' => $this->checkDateValidity($value->acedemic_startdate),
                        'acedemic_enddate_status' => $this->checkDateValidity($value->acedemic_enddate),
                        'fk_created_by' => $value->fk_created_by,
                        'created_date' => (new DateTime($value->created_date))->format('d M,Y'),
                        'amountDue' => $amountDue,
                        'duePercentage' => $amountDuePercetage,
                        'enrolled' => $enrolled,
                        'dropouts' => $dropouts,
                        'facultyName' => $facultyName!=''?$facultyName:'',
                        'currentClass' => $current!=0?(new DateTime($current))->format('d M,Y'):'',
                        'oldClass' => $old!=0?(new DateTime($old))->format('d M,Y'):'',
                        'upcomingClass' => $upcoming!=0?(new DateTime($upcoming))->format('d M,Y'):'',
                        'venueName' => $venueName!=''?$venueName:'',
                        'stock' => $stock,
                        'pending' => $pending,
                        'issued' => $issued,
                        'branchCode' => $branchCode
                    ];
                }
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /**
     * To get listi of students
     */
    function getStudentListByBatch($batchId) {
        $this->db->select('bxc.batch_xref_class_id,bxc.class_date,v.name location,f.name as faculty');
        $this->db->from('batch_xref_class bxc');
        $this->db->join('venue v','v.venue_id=bxc.fk_venue_id','left');
        $this->db->join('faculty f','bxc.fk_faculty_id=f.faculty_id','left');
        $this->db->where('bxc.fk_batch_id',$batchId);
        $this->db->where('date(bxc.class_date)<=date(now())');
        $this->db->where('bxc.is_active',1);
        $this->db->order_by('bxc.batch_xref_class_id','asc');
        $currentClassRes=$this->db->get();
        $currentClasses=0;
        if($currentClassRes){
            $currentClasses=$currentClassRes->num_rows();
        }
        else{
            $currentClasses=0;
        }
        /*echo $qry = "select l.name AS student_name,
                    c.name course_name,
                    bxl.branch_xref_lead_id,
                    CONCAT_WS(\"-\",b.code,c.name,ba.code,bxl.lead_number) as enroll,
                    (select sum((cfi.amount_payable-cfi.concession)-cfi.amount_paid)
                    FROM candidate_fee cf
                    LEFT JOIN branch_xref_lead bxl1 ON cf.fk_branch_xref_lead_id=bxl1.branch_xref_lead_id
                    LEFT JOIN candidate_fee_item cfi ON cf.candidate_fee_id=cfi.fk_candidate_fee_id
                    where bxl1.branch_xref_lead_id=bxl.branch_xref_lead_id) as due,
                    bxl.lead_id, bxl.is_active,
                    (select if(count(*)>0,1,0) from fee_collection fc1
                        LEFT JOIN fee_collection_item fci1 ON fc1.fee_collection_id=fci1.fk_fee_collection_id
                        LEFT JOIN company co1 ON fc1.fk_fee_company_id=co1.company_id
                        LEFT JOIN branch_xref_lead bxl2 ON fc1.fk_branch_xref_lead_id=bxl2.branch_xref_lead_id
                        LEFT JOIN candidate_fee cf1 ON bxl2.branch_xref_lead_id=cf1.fk_branch_xref_lead_id
                        LEFT JOIN candidate_fee_item cfi1 ON cf1.candidate_fee_id=cfi1.fk_candidate_fee_id
                        where bxl2.fk_batch_id=".$batchId."
                        AND cf1.fk_branch_xref_lead_id=bxl.branch_xref_lead_id
                        AND co1.name='publication'
                        AND cfi1.amount_payable=cfi1.amount_paid) as is_publication_fee_paid,
                    (SELECT  sum(`pc`.`quantity`)
                        FROM `batch` `b`
                        JOIN `product_xref_course` `pc` ON `pc`.`fk_course_id`=`b`.`fk_course_id`
                        JOIN `product` `p` ON `p`.`product_id`=`pc`.`fk_product_id`
                        WHERE `b`.`batch_id` = {$batchId}
                        AND `pc`.`is_active` =0) as quantity,
                        (select sum(li.quantity) from lead_item_issue li 
                        where li.fk_batch_id= {$batchId}
                        AND li.fk_lead_id = bxl.branch_xref_lead_id) as issued,
                    c.course_id, 
                    l.lead_id, 
                    b.branch_id, b.code branch_code,
                    ba.batch_id, ba.code branch_code
                from branch_xref_lead bxl
                    LEFT JOIN lead l ON bxl.lead_id=l.lead_id
                    LEFT JOIN course c ON bxl.fk_course_id=c.course_id
                    LEFT JOIN branch b ON bxl.branch_id=b.branch_id
                    LEFT JOIN batch ba ON bxl.fk_batch_id=ba.batch_id
                where bxl.fk_batch_id=".$batchId." and bxl.is_active=1";
        exit;*/
            /*$qry="SELECT bxl.lead_number,bxl.branch_xref_lead_id,b.batch_id,bxl.lead_number,l.`name` as student_name,c.course_id,c.`name` as coursename,b.`code` as batchcode,
            CONCAT_WS('-',br.`code`,c.`name`,b.`code`,bxl.lead_number) as enrollment_number,
            SUM(cfi.amount_paid) as amountpaid,SUM(cfi.amount_payable) as amountpayable,SUM(cfi.concession) as concessionamount,
            (SUM(cfi.amount_payable)-SUM(cfi.amount_paid)) as balanceamount,
            (SELECT SUM(pxc.quantity)
            FROM product_xref_course pxc
            JOIN product p ON p.product_id=pxc.fk_product_id
            WHERE pxc.fk_course_id=b.fk_course_id AND p.is_active = 0 AND pxc.is_active=0) as quantity,
            (SELECT IFNULL(SUM(li.quantity),0)
            FROM product_xref_course pxc
            JOIN product p ON p.product_id=pxc.fk_product_id
            LEFT JOIN lead_item_issue li ON pxc.fk_product_id=li.fk_product_id AND li.`status`= 1
            WHERE pxc.fk_course_id=b.fk_course_id AND p.is_active = 0 AND pxc.is_active=0 AND li.fk_lead_id=bxl.branch_xref_lead_id) as adhoc_quantity,
            (select IFNULL(sum(li.quantity),0) FROM
            lead_item_issue li
            WHERE li.fk_batch_id = b.batch_id AND li.fk_lead_id = bxl.branch_xref_lead_id AND li.`status`=0) AS issued
            FROM branch_xref_lead bxl
            JOIN lead l ON bxl.lead_id=l.lead_id
            JOIN course c ON bxl.fk_course_id=c.course_id
            JOIN batch b ON bxl.fk_batch_id=b.batch_id AND bxl.fk_course_id=b.fk_course_id
            JOIN branch br ON b.fk_branch_id=br.branch_id
            LEFT JOIN candidate_fee cf ON bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id
            LEFT JOIN candidate_fee_item cfi ON cf.candidate_fee_id=cfi.fk_candidate_fee_id
            WHERE bxl.fk_batch_id=".$batchId." AND bxl.is_active=1
            GROUP BY cf.candidate_fee_id";*/
        $qry="SELECT bxl.lead_number,bxl.branch_xref_lead_id,b.batch_id,bxl.lead_number,l.`name` as student_name,c.course_id,c.`name` as coursename,b.`code` as batchcode,
            CONCAT_WS('-',br.`code`,c.`name`,'M7',IF(rf.value='Retail','R',IF(rf.value='Corporate','C',IF(rf.value='Group','I',''))) , IF(rf1.value='Online Training','W',IF(rf1.value='Class Room Training','C',IF(rf1.value='Class Room & Online Training','P',IF(rf1.value='Assistance','A',IF(rf1.value='Books','B',''))))),b.`code`,bxl.lead_number) as enrollment_number,
            SUM(cfi.amount_paid) as amountpaid,SUM(cfi.amount_payable) as amountpayable,SUM(cfi.concession) as concessionamount,
            (SUM(cfi.amount_payable)-SUM(cfi.amount_paid)) as balanceamount,
            (SELECT SUM(pxc.quantity)
            FROM product_xref_course pxc
            JOIN product p ON p.product_id=pxc.fk_product_id
            WHERE pxc.fk_course_id=b.fk_course_id AND p.is_active = 0 AND pxc.is_active=0) as quantity,
            (SELECT IFNULL(SUM(li.quantity),0)
            FROM product_xref_course pxc
            JOIN product p ON p.product_id=pxc.fk_product_id
            LEFT JOIN lead_item_issue li ON pxc.fk_product_id=li.fk_product_id AND li.`status`= 1
            WHERE pxc.fk_course_id=b.fk_course_id AND p.is_active = 0 AND pxc.is_active=0 AND li.fk_lead_id=bxl.branch_xref_lead_id) as adhoc_quantity,
            (select IFNULL(sum(li.quantity),0) FROM
            lead_item_issue li
            WHERE li.fk_batch_id = b.batch_id AND li.fk_lead_id = bxl.branch_xref_lead_id AND li.`status`=0) AS issued
            FROM branch_xref_lead bxl
            JOIN lead l ON bxl.lead_id=l.lead_id
            JOIN course c ON bxl.fk_course_id=c.course_id
            JOIN batch b ON bxl.fk_batch_id=b.batch_id AND bxl.fk_course_id=b.fk_course_id
            JOIN branch br ON b.fk_branch_id=br.branch_id
            LEFT JOIN candidate_fee cf ON bxl.branch_xref_lead_id=cf.fk_branch_xref_lead_id
            LEFT JOIN candidate_fee_item cfi ON cf.candidate_fee_id=cfi.fk_candidate_fee_id
            LEFT JOIN fee_structure fs ON cf.fk_fee_structure_id=fs.fee_structure_id
				LEFT JOIN reference_type_value rf ON fs.fk_type_id=rf.reference_type_value_id
				LEFT JOIN reference_type_value rf1 ON fs.fk_training_type=rf1.reference_type_value_id
            WHERE bxl.fk_batch_id=".$batchId." AND bxl.is_active=1
            GROUP BY cf.candidate_fee_id";

        $studentList = $this->db->query($qry);
        if ($studentList) {
            $studentList = $studentList->result();
            if (count($studentList) > 0) {
                foreach($studentList as $key=>$val){
                    $val->attendedClassPercentage=0;
                    if($currentClasses>0)
                    {
                        $query = "select ifnull(count(la.lead_attendence_id),0) as attendedClassCount from lead_attendence la, batch_xref_class bxc where la.is_attended=1 and la.fk_batch_xref_class_id=bxc.batch_xref_class_id and la.fk_lead_id=" . $val->branch_xref_lead_id . " and bxc.fk_batch_id=" . $batchId;
                        $res = $this->db->query($query);
                        if ($res)
                        {
                            $rows = $res->result();
                            $val->attendedClassPercentage = ($rows[0]->attendedClassCount/$currentClasses)*100;
                        }
                    }

                }
                return $studentList;
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    /**
     * Function get Batch dashboard data by id
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getStudentDetailsByBatch($batch_id, $branchId, $courseId) {
        if ($batch_id != 'all') {
            $this->db->select('*');
            $this->db->from('batch');
            $this->db->where('code', $batch_id);
            $this->db->where('fk_branch_id', $branchId);
            $this->db->where('fk_course_id', $courseId);
            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    $branchId = $res_all[0]->fk_branch_id;
                    $courseId = $res_all[0]->fk_course_id;
                    $batchId = '';
                    $batchId = $res_all[0]->batch_id == 'all' ? '' : " AND bxl.fk_batch_id=" . $res_all[0]->batch_id . " ";
                    $amountPayable = 0;
                    $amountPaid = 0;
                    $query = $this->db->query("call Sp_Get_Due_List_Report('{$branchId}','{$courseId}','{$res_all[0]->batch_id}',null,null,null)");
                    if ($query) {
                        if ($query->num_rows() > 0) {
                            $duelist = $query->result();
                            foreach ($duelist as $key => $value) {
                                $amountPayable += $value->amountPayable;
                                $amountPaid += $value->amountPaid;
                            }
                        }
                    }
                    $this->db->close();
                    $this->load->database();
                    $totalAmount = $amountPayable;
                    $amountDue = $amountPayable - $amountPaid;
                    $amountDuePercentage = $amountPaid != 0 || $amountPayable != 0 ? number_format((float) (($amountDue / $amountPayable) * 100), 2, '.', '') . ' %' : '0 %';
                    $qry = "SELECT
                            (select count(*) from branch_xref_lead bxl
                                JOIN lead l ON bxl.lead_id=l.lead_id
                                WHERE
                                bxl.is_active=1
                                AND bxl.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                                AND bxl.branch_id=" . $branchId . "
                                AND bxl.fk_course_id=" . $courseId . "
                                " . $batchId . ") AS enrolled ,
                            (select count(*)from branch_xref_lead bxl
                                JOIN lead l ON bxl.lead_id=l.lead_id
                                JOIN batch b ON bxl.fk_batch_id=b.batch_id
                                WHERE
                                bxl.is_active=1 
                                AND bxl.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                                AND bxl.branch_id=" . $branchId . " 
                                AND bxl.fk_course_id=" . $courseId . "
                                " . $batchId . ") AS dropout";

                    $studentDetails = $this->db->query($qry);
                    if ($studentDetails) {
                        $studentDetails = $studentDetails->result();
                        if (count($studentDetails) > 0) {
                            $details = array(
                                'enrolled' => $studentDetails[0]->enrolled,
                                'dropout' => $studentDetails[0]->dropout,
                                'totalAmount' => $totalAmount,
                                'amountDue' => $amountDue,
                                'amountDuePercentage' => $amountDuePercentage
                            );
                        } else {
                            $details = array(
                                'enrolled' => 0,
                                'dropout' => 0,
                            );
                        }
                        return $details;
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                } else {
                    $qry = "SELECT 
                        (select count(*) from branch_xref_lead bxl
                            JOIN lead l ON bxl.lead_id=l.lead_id
                            WHERE
                            bxl.is_active=1 
                            AND bxl.branch_id=" . $branchId . " 
                            AND bxl.fk_course_id=" . $courseId . ") AS enrolled ,
                        (select count(*)from branch_xref_lead bxl
                            JOIN lead l ON bxl.lead_id=l.lead_id
                            JOIN batch b ON bxl.fk_batch_id=b.batch_id
                            WHERE
                            bxl.is_active=1 
                            AND bxl.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                            AND bxl.branch_id=" . $branchId . " 
                            AND bxl.fk_course_id=" . $courseId . " ) AS dropout";
                    $studentDetails = $this->db->query($qry);
                    if ($studentDetails) {
                        $studentDetails = $studentDetails->result();
                        if (count($studentDetails) > 0) {
                            $details = array(
                                'enrolled' => $studentDetails[0]->enrolled,
                                'dropout' => $studentDetails[0]->dropout,
                            );
                        } else {
                            $details = array(
                                'enrolled' => 0,
                                'dropout' => 0,
                            );
                        }
                        return $details;
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }
            }
        } else {
            if (is_numeric($branchId)) {
                $branchId = encode($branchId);
            }
            $branchId = decode($branchId);
            $courseId = $courseId;
            $amountPayable = 0;
            $amountPaid = 0;
            $query = $this->db->query("call Sp_Get_Due_List_Report('{$branchId}','{$courseId}',null,null,null,null)");
            if ($query) {
                if ($query->num_rows() > 0) {
                    $duelist = $query->result();
                    foreach ($duelist as $key => $value) {
                        $amountPayable += $value->amountPayable;
                        $amountPaid += $value->amountPaid;
                    }
                }
            }
            $this->db->close();
            $this->load->database();
            $totalAmount = $amountPayable;
            $amountDue = $amountPayable - $amountPaid;
            $amountDuePercentage = $amountPaid != 0 || $amountPayable != 0 ? number_format((float) (($amountDue / $amountPayable) * 100), 2, '.', '') . ' %' : '0 %';
            $qry = "SELECT 
                    (select count(*) from branch_xref_lead bxl
                        JOIN lead l ON bxl.lead_id=l.lead_id
                        WHERE
                        bxl.is_active=1
                        AND bxl.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                        AND bxl.branch_id=" . $branchId . " 
                        AND bxl.fk_course_id=" . $courseId . ") AS enrolled ,
                    (select count(*)from branch_xref_lead bxl
                        JOIN lead l ON bxl.lead_id=l.lead_id
                        JOIN batch b ON bxl.fk_batch_id=b.batch_id
                        WHERE
                        bxl.is_active=1 
                        AND bxl.`status`=(select lead_stage_id from lead_stage where lead_stage='M7')
                        AND bxl.branch_id=" . $branchId . " 
                        AND bxl.fk_course_id=" . $courseId . ") AS dropout";
            $studentDetails = $this->db->query($qry);
            if ($studentDetails) {
                $studentDetails = $studentDetails->result();
                if (count($studentDetails) > 0) {
                    $details = array(
                        'enrolled' => $studentDetails[0]->enrolled,
                        'dropout' => $studentDetails[0]->dropout,
                        'totalAmount' => $totalAmount,
                        'amountDue' => $amountDue,
                        'amountDuePercentage' => $amountDuePercentage
                    );
                } else {
                    $details = array(
                        'enrolled' => 0,
                        'dropout' => 0,
                    );
                }
                return $details;
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }

    /**
     * Function get Dats stage
     * 
     * @return - string
     * @author : Abhilash
     */
    public function checkDateValidity($date) {
        $start = strtotime($date);
        $current = strtotime(date('Y-m-d'));
        if ($start > $current) {
            $status = 'upcoming';
        } else if ($start <= $current) {
            $status = 'progress';
        } else if ($end < $current) {
            $status = 'completed';
        }
        return $status;
    }

    /**
     * Function Add Batch
     * 
     * @author : Abhilash
     */
    public function addBatch($data) {
        $error = true;
        /* Branch */
        if (is_numeric($data['branch_id'])) {
            $data['branch_id'] = encode($data['branch_id']);
        }
        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', decode($data['branch_id']));
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* Branch Inactive check */
        $this->db->select('branch_id');
        $this->db->from('branch');
        $this->db->where('branch_id', decode($data['branch_id']));
        $this->db->where('is_active', 1);
        $res_all = $this->db->get();
        if ($res_all)
        {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranchActive';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* User */
        $this->db->select('user_id');
        $this->db->from('user');
        $this->db->where('user_id', $_SERVER['HTTP_USER']);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nouser';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* Course */
        if (preg_match("/,/", $data['course'])) {
            $course = explode(",", $data['course']);
            if (is_array($course) && count($course) > 0) {
                $coursePresent = [];
                foreach ($course as $key => $val) {
                    $this->db->select('course_id');
                    $this->db->from('course');
                    $this->db->where('course_id', $val);
                    $query = $this->db->get();
                    if ($query) {
                        $query = $query->result_array();
                        if (count($query) > 0) {
                            $valid = true;
                        } else {
                            $valid = false;
                            return 'nocourse';
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    /* for duplicacy check for same branch and course */
                    /*$this->db->select('b.batch_id, c.name as course');
                    $this->db->from('batch b');
                    $this->db->join('course c', 'b.fk_course_id=c.course_id');
                    $this->db->where('b.fk_course_id', $val);
                    $this->db->where('b.fk_branch_id', decode($data['branch_id']));
                    $this->db->where(" '" . date('Y-m-d', strtotime($data['msd'])) . "' BETWEEN b.marketing_startdate AND b.marketing_enddate");
                    $query = $this->db->get();
                    if ($query) {
                        $query = $query->result_array();
                        if (count($query) > 0) {
                            $valid = false;
                            foreach ($query as $k => $v) {
                                $coursePresent[] = $v['course'];
                            }
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }*/
                }
                /*if (is_array($coursePresent) && count($coursePresent) > 0) {
                    $coursePresent = array_unique($coursePresent);
                    return array('error' => 'alreadypresent', 'msg' => $error = implode(', ', $coursePresent) . ' batch already present');
                }*/
            }
        } else {
            $this->db->select('course_id');
            $this->db->from('course');
            $this->db->where('course_id', $data['course']);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                } else {
                    $valid = false;
                    return 'nocourse';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            /* for duplicacy check for same branch and course */
            /*$coursePresent = [];
            $this->db->select('b.batch_id, c.name as course');
            $this->db->from('batch b');
            $this->db->join('course c', 'b.fk_course_id=c.course_id');
            $this->db->where('b.fk_course_id', $data['course']);
            $this->db->where('b.fk_branch_id', decode($data['branch_id']));
            $this->db->where(" '" . date('Y-m-d', strtotime($data['msd'])) . "' BETWEEN b.marketing_startdate AND b.marketing_enddate");
            $query = $this->db->get();
//            die($this->db->last_query());
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = false;
                    $coursePresent = [];
                    foreach ($query as $k => $v) {
                        $coursePresent[] = $v['course'];
                    }
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }*/
            /*if (is_array($coursePresent) && count($coursePresent) > 0)
            {
                $coursePresent = array_unique($coursePresent);
                return array('error' => 'alreadypresent', 'msg' => $error = implode(', ', $coursePresent) . ' batch already present');
            }*/
        }
        if (!$error) {
            $this->db->trans_start();
            if (preg_match("/,/", $data['course'])) {
                $course = explode(',', $data['course']);
                foreach ($course as $key => $value)
                {
                    /* Batch Sequence */
                    $code=0;
                    $this->db->select_max('code');
                    $this->db->from('batch');
                    $this->db->where('fk_branch_id', decode($data['branch_id']));
                    $this->db->where('fk_course_id', $value);
                    $query = $this->db->get();
                    if ($query) {
                        $query = $query->result_array();
                        if (count($query) > 0 && strlen($query[0]['code']) > 0) {
                            //use the same data with increment of 1
                            $code = $query[0]['code'];
                        } else {
                            //take sequence from the Branch table
                            $this->db->select('batch_sequence');
                            $this->db->from('branch');
                            $this->db->where('branch_id', decode($data['branch_id']));
                            $query = $this->db->get();
                            if ($query) {
                                $query = $query->result_array();
                                if (count($query) > 0) {
                                    $code = $query[0]['batch_sequence'];
                                }
                            } else {
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }

                    $insert = array(
                        'fk_branch_id' => decode($data['branch_id']),
                        'fk_course_id' => $value,
                        'alias_name' => $data['alias_name'],
                        'acedemic_startdate' => date('Y-m-d', strtotime($data['asd'])),
                        'acedemic_enddate' => date('Y-m-d', strtotime($data['aed'])),
                        'marketing_enddate' => date('Y-m-d', strtotime($data['med'])),
                        'marketing_startdate' => date('Y-m-d', strtotime($data['msd'])),
                        'target' => $data['target'],
                        'visitor_target' => $data['visitorTarget'],
                        'code' => ++$code,
                        'created_date' => date('Y-m-d H:i:s'),
                        'fk_created_by' => $_SERVER['HTTP_USER'],
                        'is_active' => 1,
                    );
                    $res_insert = $this->db->insert('batch', $insert);
                }
            }
            else
            {
                /* Batch Sequence */
                $code=0;
                $this->db->select_max('code');
                $this->db->from('batch');
                $this->db->where('fk_branch_id', decode($data['branch_id']));
                $this->db->where('fk_course_id', $data['course']);
                $query = $this->db->get();
                if ($query) {
                    $query = $query->result_array();
                    if (count($query) > 0 && strlen($query[0]['code']) > 0)
                    {
                        //use the same data with increment of 1
                        $code = $query[0]['code'];
                    }
                    else
                    {
                        //take sequence from the Branch table
                        $this->db->select('batch_sequence');
                        $this->db->from('branch');
                        $this->db->where('branch_id', decode($data['branch_id']));
                        $query = $this->db->get();
                        if ($query) {
                            $query = $query->result_array();
                            if (count($query) > 0) {
                                $code = $query[0]['batch_sequence'];
                            }
                        } else {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
                $insert = array(
                    'fk_branch_id' => decode($data['branch_id']),
                    'fk_course_id' => $data['course'],
                    'alias_name' => $data['alias_name'],
                    'acedemic_startdate' => date('Y-m-d', strtotime($data['asd'])),
                    'acedemic_enddate' => date('Y-m-d', strtotime($data['aed'])),
                    'marketing_enddate' => date('Y-m-d', strtotime($data['med'])),
                    'marketing_startdate' => date('Y-m-d', strtotime($data['msd'])),
                    'target' => $data['target'],
                    'visitor_target' => $data['visitorTarget'],
                    'code' => ++$code,
                    'created_date' => date('Y-m-d H:i:s'),
                    'fk_created_by' => $_SERVER['HTTP_USER'],
                    'is_active' => 1,
                );
                $res_insert = $this->db->insert('batch', $insert);
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }

    /**
     * Function Ediy Batch by Id
     * 
     * @author : Abhilash
     */
    public function editBatch($data, $id) {
        $error = true;
        /* Branch */
        if (is_numeric($data['branch_id'])) {
            $data['branch_id'] = encode($data['branch_id']);
        }
        $this->db->select('*');
        $this->db->from('branch');
        $this->db->where('branch_id', decode($data['branch_id']));

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* Branch Inactive check */
        $this->db->select('*');
        $this->db->from('branch');
        $this->db->where('branch_id', decode($data['branch_id']));
        $this->db->where('is_active', 1);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nobranchActive';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* User */
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $_SERVER['HTTP_USER']);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $error = false;
            } else {
                $error = true;
                return 'nouser';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* Course */
        if (preg_match("/,/", $data['course'])) {
            $course = explode(",", $data['course']);
            if (is_array($course) && count($course) > 0) {
                foreach ($course as $key => $val) {
                    $this->db->select('*');
                    $this->db->from('course');
                    $this->db->where('course_id', $val);
                    $query = $this->db->get();
                    if ($query) {
                        $query = $query->result_array();
                        if (count($query) > 0) {
                            $valid = true;
                        } else {
                            $valid = false;
                            return 'nocourse';
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }
            }
        } else {
            $this->db->select('*');
            $this->db->from('course');
            $this->db->where('course_id', $data['course']);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                } else {
                    $valid = false;
                    return 'nocourse';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        /* sequence */
        /* $this->db->select('branch_xref_lead_id');
          $this->db->from('branch_xref_lead');
          $this->db->where('lead_number', $data['sequence']);
          $query = $this->db->get();
          if ($query) {
          $query = $query->result_array();
          if (count($query) <= 0) {
          $valid = true;
          } else {
          $valid = false;
          return 'usedsequence';
          }
          } else {
          return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
          } */

        if (!$error) {
            $this->db->trans_start();

            $update = array(
                /* 'fk_branch_id' => decode($data['branch_id']), */
                'fk_course_id' => $data['course'],
                'alias_name' => $data['alias_name'],
                'acedemic_startdate' => date('Y-m-d', strtotime($data['asd'])),
                'acedemic_enddate' => date('Y-m-d', strtotime($data['aed'])),
                'marketing_enddate' => date('Y-m-d', strtotime($data['med'])),
                'marketing_startdate' => date('Y-m-d', strtotime($data['msd'])),
                'target' => $data['target'],
                'visitor_target' => $data['visitorTarget'],
//                'code' => trim($data['code']),
                'updated_date' => date('Y-m-d H:i:s'),
                'fk_updated_by' => $_SERVER['HTTP_USER'],
                'is_active' => 1,
                    /* 'sequence_number' => $data['sequence'], */
            );
            $this->db->where('batch_id', $id);
            $res_update = $this->db->update('batch', $update);

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }

    /**
     * Function active/inactive Batch
     * 
     * @return - array
     * @author : Abhilash
     */
    public function changeStatus($code) {
        $this->db->select('*');
        $this->db->from('batch');
        $this->db->where('code', $code);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if ($res_all) {

                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );

                $this->db->where('code', $code);
                $res_update = $this->db->update('batch', $data_update);

                if ($this->db->error()['code'] == '0') {
                    if ($res_update) {
                        if ($is_active) {
                            return 'active';
                        } else {
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /**
     * Function get Lead Stage and their counts
     * 
     * @return - array
     * @author : Abhilash
     */
    function getLeadStageCounts($param) {
        $result = [];
        /* Student */
        $qry = "select (select count(rtv.reference_type_value_id) from branch_xref_lead bxl
            left join lead_stage l ON bxl.`status`=l.lead_stage_id
            left join lead le ON bxl.lead_id=le.lead_id
            JOIN reference_type_value rtv ON le.fk_company_id=rtv.reference_type_value_id
            where l.lead_stage='M7'
            AND bxl.is_active=1
            AND bxl.branch_id=" . $param['branchId'] . " AND bxl.fk_batch_id=" . $param['batchId'] . ") AS corporate,
                (select count(rtv.reference_type_value_id) from branch_xref_lead bxl
            left join lead_stage l ON bxl.`status`=l.lead_stage_id
            left join lead le ON bxl.lead_id=le.lead_id
            JOIN reference_type_value rtv ON le.fk_institution_id=rtv.reference_type_value_id
            where l.lead_stage='M7'
            AND bxl.is_active=1
            AND bxl.branch_id=" . $param['branchId'] . " AND bxl.fk_batch_id=" . $param['batchId'] . ") AS institute,
                (select count(*) from branch_xref_lead bxl
            left join lead_stage l ON bxl.`status`=l.lead_stage_id
            left join lead le ON bxl.lead_id=le.lead_id
            where l.lead_stage='M7'
            AND bxl.is_active=1
            AND bxl.branch_id=" . $param['branchId'] . " AND bxl.fk_batch_id=" . $param['batchId'] . ") AS total,
                (select (total-(institute+corporate))) AS retail";
        $res_all = $this->db->query($qry);
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $result['student'] = $res_all[0];
            } else {
                //nodata
                $result['student'] = [];
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        /* Marketing */
        $qry = "SELECT (select count(bxl.branch_xref_lead_id) from branch_xref_lead bxl 
                left join lead_stage ls ON  bxl.`status`=ls.lead_stage_id
                where ls.lead_stage='M3' 
                AND bxl.is_active=1 AND bxl.branch_id=" . $param['branchId'] . " AND bxl.fk_batch_id=" . $param['batchId'] . ") AS M3,
                    (select count(bxl.branch_xref_lead_id) from branch_xref_lead bxl 
                left join lead_stage ls ON  bxl.`status`=ls.lead_stage_id
                where ls.lead_stage='M3+' 
                AND bxl.is_active=1 AND bxl.branch_id=" . $param['branchId'] . " AND bxl.fk_batch_id=" . $param['batchId'] . ") AS M3p,
                    (select count(bxl.branch_xref_lead_id) from branch_xref_lead bxl 
                left join lead_stage ls ON  bxl.`status`=ls.lead_stage_id
                where ls.lead_stage='M5' 
                AND bxl.is_active=1 AND bxl.branch_id=" . $param['branchId'] . " AND bxl.fk_batch_id=" . $param['batchId'] . ") AS M5,
                    (select count(bxl.branch_xref_lead_id) from branch_xref_lead bxl 
                left join lead_stage ls ON  bxl.`status`=ls.lead_stage_id
                where ls.lead_stage='M6' 
                AND bxl.is_active=1 AND bxl.branch_id=" . $param['branchId'] . " AND bxl.fk_batch_id=" . $param['batchId'] . ") AS M6";
        $res_all = $this->db->query($qry);
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $result['marketing'] = $res_all[0];
            } else {
                //nodata
                $result['marketing'] = [];
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        /* DUE/OVERDUE */
        $amountPayable = 0;
        $amountPaid = 0;
        $query = $this->db->query("call Sp_Get_Due_List_Report('{$param['branchId']}',null,{$param['batchId']},null,null,null)");
        if ($query) {
            if ($query->num_rows() > 0) {
                $duelist = $query->result();
                foreach ($duelist as $key => $value) {
                    $amountPayable += $value->amountPayable;
                    $amountPaid += $value->amountPaid;
                }
            }
        }
        $amountDue = $amountPayable - $amountPaid;
        $this->db->close();
        $this->load->database();
        $amountDuePercetage = $amountPaid != 0 || $amountPayable != 0 ? number_format((float) (($amountDue / $amountPayable) * 100), 2, '.', '') . ' %' : '0 %';

        /* Paid */
        $studentCount = 0;
        $qry = "select count(bxl.lead_id) AS count from branch_xref_lead bxl
                    left join lead_stage l ON bxl.`status`=l.lead_stage_id
                    left join lead le ON bxl.lead_id=le.lead_id
                where l.lead_stage='M7'
                    AND bxl.is_active=1
                    AND bxl.branch_id=" . $param['branchId'] . "
                    AND bxl.fk_batch_id=" . $param['batchId'];
        $res_all = $this->db->query($qry);
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $studentCount = $res_all[0]->count;
            } else {
                //nodata
                $studentCount = 0;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $result['dueamount'] = [
            'amountDue' => $amountDue,
            'duePercentage' => $amountDuePercetage,
            'studentCount' => $studentCount,
        ];

        $result['paid'] = [
            'amountPaid' => $amountPaid,
            'studentCount' => $studentCount,
        ];



        return $result;
    }

    /**
     * Save Subject using Batch id
     */
    function saveSubjectForBatch($data) {
        $this->db->select('b.batch_id');
        $this->db->from('batch b');
        $this->db->where('b.batch_id', $data['batchId']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nobatch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('subject_id');
        $this->db->from('subject');
        $this->db->where('subject_id', $data['subject']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nosubject';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('faculty_id');
        $this->db->from('faculty');
        $this->db->where('faculty_id', $data['faculty']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nofaculty';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('faculty_id');
        $this->db->from('faculty');
        $this->db->where('faculty_id', $data['faculty']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nofaculty';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('venue_id');
        $this->db->from('venue');
        $this->db->where('venue_id', $data['venue']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'novenue';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('*');
        $this->db->from('batch');
        $this->db->where('batch_id', $data['batchId']);
        $this->db->where("'".date('Y-m-d', strtotime($data['subjectDate']))."' BETWEEN acedemic_startdate AND acedemic_enddate");
        $res = $this->db->get();
        if($res){
            $res= $res->result();
            if(count($res) <= 0){
                return 'datanotinrandg';
                $isValidDate = false;
            }
        }else{
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('*');
        $this->db->from('batch_xref_class');
        $this->db->where('fk_batch_id', $data['batchId']);
        $this->db->where("'".date('Y-m-d', strtotime($data['subjectDate']))."' = class_date");
        $res = $this->db->get();
        if($res){
            $res= $res->result();
            if(count($res) > 0){
                return 'samedate';
                $isValidDate = false;
            }
        }else{
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        if (preg_match("/,/", $data['subject'])) {
            $subject = explode(',', $data['subject']);
            foreach ($subject as $key => $value) {
                $insert_data = array(
                    'fk_batch_id' => $data['batchId'],
                    'fk_subject_id' => $value,
                    'fk_faculty_id' => $data['faculty'],
                    'fk_venue_id' => $data['venue'],
                    'topic' => $data['topic'],
                    'class_date' => date('Y-m-d', strtotime($data['subjectDate'])),
                    'is_active' => 1,
                    'no_of_plates_used' => $data['no_of_plates_used']!=''?$data['no_of_plates_used']:0,
                    'no_of_cups_used' => $data['no_of_cups_used']!=''?$data['no_of_cups_used']:0,
                    'fk_created_by' => $_SERVER['HTTP_USER'],
                    'created_date' => date('Y-m-d H:i:s'),
                );
                $inserted = $this->db->insert('batch_xref_class', $insert_data);
            }
        } else {
            $insert_data = array(
                'fk_batch_id' => $data['batchId'],
                'fk_subject_id' => $data['subject'],
                'fk_faculty_id' => $data['faculty'],
                'fk_venue_id' => $data['venue'],
                'topic' => $data['topic'],
                'class_date' => date('Y-m-d', strtotime($data['subjectDate'])),
                'is_active' => 1,
                'no_of_plates_used' => $data['no_of_plates_used']!=''?$data['no_of_plates_used']:0,
                'no_of_cups_used' => $data['no_of_cups_used']!=''?$data['no_of_cups_used']:0,
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
            );
            $inserted = $this->db->insert('batch_xref_class', $insert_data);
        }
        return $inserted;
    }

    /**
     * Edit Subject using Batch id and batch_xref_class_id
     */
    function editSubjectForBatch($data, $id) {
        $this->db->select('batch_xref_class_id');
        $this->db->from('batch_xref_class');
        $this->db->where('batch_xref_class_id', $id);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'noid';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('b.batch_id');
        $this->db->from('batch b');
        $this->db->where('b.batch_id', $data['batchId']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nobatch';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('subject_id');
        $this->db->from('subject');
        $this->db->where('subject_id', $data['subject']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nosubject';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('faculty_id');
        $this->db->from('faculty');
        $this->db->where('faculty_id', $data['faculty']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nofaculty';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('faculty_id');
        $this->db->from('faculty');
        $this->db->where('faculty_id', $data['faculty']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nofaculty';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('venue_id');
        $this->db->from('venue');
        $this->db->where('venue_id', $data['venue']);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'novenue';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('*');
        $this->db->from('batch');
        $this->db->where('batch_id', $data['batchId']);
        $this->db->where("'".date('Y-m-d', strtotime($data['subjectDate']))."' BETWEEN acedemic_startdate AND acedemic_enddate");
        $res = $this->db->get();
        if($res){
            $res= $res->result();
            if(count($res) <= 0){
                return 'datanotinrandg';
                $isValidDate = false;
            }
        }else{
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('*');
        $this->db->from('batch_xref_class');
        $this->db->where('fk_batch_id', $data['batchId']);
        $this->db->where('batch_xref_class_id!=', $id);
        $this->db->where("'".date('Y-m-d', strtotime($data['subjectDate']))."' = class_date");
        $res = $this->db->get();
        if($res){
            $res= $res->result();
            if(count($res) > 0){
                return 'samedate';
                $isValidDate = false;
            }
        }else{
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        if (preg_match("/,/", $data['subject'])) {
            $subject = explode(',', $data['subject']);
            foreach ($subject as $key => $value) {
                $updated_data = array(
                    'fk_batch_id' => $data['batchId'],
                    'fk_subject_id' => $value,
                    'fk_faculty_id' => $data['faculty'],
                    'fk_venue_id' => $data['venue'],
                    'topic' => $data['topic'],
                    'class_date' => date('Y-m-d', strtotime($data['subjectDate'])),
                    'is_active' => 1,
                    'no_of_plates_used' => $data['no_of_plates_used'],
                    'no_of_cups_used' => $data['no_of_cups_used'],
                    'fk_created_by' => $_SERVER['HTTP_USER'],
                    'created_date' => date('Y-m-d H:i:s'),
                );
                $this->db->where('batch_xref_class_id', $id);
                $updated = $this->db->insert('batch_xref_class', $updated_data);
            }
        } else {
            $updated_data = array(
                'fk_batch_id' => $data['batchId'],
                'fk_subject_id' => $data['subject'],
                'fk_faculty_id' => $data['faculty'],
                'fk_venue_id' => $data['venue'],
                'topic' => $data['topic'],
                'class_date' => date('Y-m-d', strtotime($data['subjectDate'])),
                'is_active' => 1,
                'no_of_plates_used' => $data['no_of_plates_used'],
                'no_of_cups_used' => $data['no_of_cups_used'],
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
            );
            $this->db->where('batch_xref_class_id', $id);
            $updated = $this->db->update('batch_xref_class', $updated_data);
        }

        return $updated;
    }

    /**
     * Get branch subject by id
     * @param type $id
     * @return array
     */
    function getSubjectForBatch($id) {
        $this->db->select('batch_xref_class_id');
        $this->db->from('batch_xref_class');
        $this->db->where('batch_xref_class_id', $id);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

        $this->db->select('bxs.fk_batch_id, bxs.fk_subject_id, bxs.fk_faculty_id, bxs.fk_venue_id, bxs.topic, bxs.class_date topic_date, if(bxs.attendence_status,1,0), bxs.no_of_plates_used, bxs.no_of_cups_used,
            f.name faculty_name, f.comments, 
            b.batch_id, b.fk_branch_id, b.fk_course_id, b.code batch_code,
            s.fk_course_id, s.name subject_name, s.code subject_code,
            v.name venue_name, v.fk_city_id');
        $this->db->from('batch_xref_class bxs');
        $this->db->join('batch b', 'bxs.fk_batch_id=b.batch_id', 'left');
        $this->db->join('faculty f', 'bxs.fk_faculty_id=f.faculty_id', 'left');
        $this->db->join('subject s', 'bxs.fk_subject_id=s.subject_id', 'left');
        $this->db->join('venue v', 'bxs.fk_venue_id=v.venue_id', 'left');
        $this->db->where('batch_xref_class_id', $id);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                foreach ($result as $key => $value) {
                    $value->topic_date = (new DateTime($value->topic_date))->format('d F,Y');
                }
                return $result;
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /**
     * Check if batch is present or not
     */
    function checkBatchPresent($id) {
        $this->db->select('*');
        $this->db->from('batch');
        $this->db->where('batch_id', $id);
        $res = $this->db->get();
        if($res){
            $res = $res->result();
            if(count($res) > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    /**
     * Active/inactive a topic using id
     */
    function deleteBatchSubject($id) {
        $this->db->select('*');
        $this->db->from('batch_xref_class');
        $this->db->where('batch_xref_class_id', $id);
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );

                $this->db->where('batch_xref_class_id', $id);
                $res_update = $this->db->update('batch_xref_class', $data_update);

                if ($this->db->error()['code'] == '0') {
                    if ($res_update) {
                        if ($is_active) {
                            return 'active';
                        } else {
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    /**
     * class datatbles
     * @param type $batchId - batch Id
     * @return array
     */
    function getBatchSubjectList($batchId) {
        $table = 'batch_xref_class';
        // Table's primary key
        $primaryKey = 'bxc`.`batch_xref_class_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        /*$columns = array(
            array('db' => '@a:=@a+1 AS serial_number', 'dt' => 'serial_number', 'field' => 'serial_number'),
            array('db' => '`b`.`class_date` AS topic_date', 'dt' => 'topic_date', 'field' => 'topic_date', 'formatter' => function( $d, $row ) {
                    return date('d M,Y', strtotime($d));
                }),
            array('db' => '`s`.`code`', 'dt' => 'code', 'field' => 'code'),
            array('db' => '`b`.`topic`', 'dt' => 'topic', 'field' => 'topic'),
            array('db' => '`f`.`name` AS faculty_name', 'dt' => 'faculty_name', 'field' => 'faculty_name'),
            array('db' => '`v`.`name` AS venue_name', 'dt' => 'venue_name', 'field' => 'venue_name'),
            array('db' => '(select count(*) from branch_xref_lead bxl where bxl.fk_batch_id=b.fk_batch_id AND bxl.`status`=(select l.lead_stage_id from lead_stage l where l.lead_stage=\'M7\')) AS regular', 'dt' => 'regular', 'field' => 'regular'),
            array('db' => '`b`.`batch_xref_class_id`', 'dt' => 'batch_xref_class_id', 'field' => 'batch_xref_class_id'),
            array('db' => '`s`.`name` AS subject_name', 'dt' => 'subject_name', 'field' => 'subject_name'),
            array('db' => '`s`.`subject_id`', 'dt' => 'subject_id', 'field' => 'subject_id'),
            array('db' => '`f`.`comments`', 'dt' => 'comments', 'field' => 'comments'),
            array('db' => '`f`.`faculty_id`', 'dt' => 'faculty_id', 'field' => 'faculty_id'),
            array('db' => '`v`.`venue_id`', 'dt' => 'venue_id', 'field' => 'venue_id'),
            array('db' => '(select count(lead_attendence_id) as regularAttendance from lead_attendence la JOIN batch_xref_class bxca ON bxca.batch_xref_class_id=la.fk_batch_xref_class_id JOIN batch bta ON bxca.fk_batch_id=bta.batch_id JOIN branch_xref_lead bxla ON la.fk_lead_id=bxla.branch_xref_lead_id AND bta.fk_course_id=bxla.fk_course_id where fk_batch_xref_class_id=b.batch_xref_class_id and is_attended=1 and student_type=\'current\') as regularAttendance', 'dt' => 'regularAttendance', 'field' => 'regularAttendance'),
            array('db' => '(select count(lead_attendence_id) as repeatAttendance from lead_attendence where fk_batch_xref_class_id=b.batch_xref_class_id and is_attended=1 and student_type=\'old\') as  repeatAttendance ', 'dt' => 'repeatAttendance', 'field' => 'repeatAttendance'),
            array('db' => 'if(date(`b`.`class_date`)<date(now()),"old", if(date(`b`.`class_date`)=date(now()),"current",if(date(`b`.`class_date`)>date(now()),"new",""))) AS topic_status', 'dt' => 'topic_status', 'field' => 'topic_status'),
            array('db' => 'if(`b`.`is_active`=1,1,0) as topic_active', 'dt' => 'topic_active', 'field' => 'topic_active'),
            array('db' => 'if(`b`.`attendence_status`=1,1,0) as attendence_status', 'dt' => 'attendence_status', 'field' => 'attendence_status'),
            array('db' => 'if(`b`.`class_date`<now()=1,1,0) as topic_date_status', 'dt' => 'topic_date_status', 'field' => 'topic_date_status'),
            array('db' => 'if(`b`.`class_date`>now()=1,1,0) as topic_date_past', 'dt' => 'topic_date_past', 'field' => 'topic_date_past'),
        );*/

        $columns = array(
                    array('db' => 'bxc.class_date AS topic_date', 'dt' => 'topic_date', 'field' => 'topic_date', 'formatter' => function( $d, $row ) {
                            return date('d M,Y', strtotime($d));
                        }),
                    array('db' => 'sb.`code`', 'dt' => 'code', 'field' => 'code'),
                    array('db' => 'bxc.topic', 'dt' => 'topic', 'field' => 'topic'),
                    array('db' => 'fc.`name` as faculty_name', 'dt' => 'faculty_name', 'field' => 'faculty_name'),
                    array('db' => 'vn.`name` as venue_name', 'dt' => 'venue_name', 'field' => 'venue_name'),
                    array('db' => 'bxc.batch_xref_class_id', 'dt' => 'batch_xref_class_id', 'field' => 'batch_xref_class_id'),
                    array('db' => 'sb.`name` as `subject_name`', 'dt' => 'subject_name', 'field' => 'subject_name'),
                    array('db' => 'sb.subject_id', 'dt' => 'subject_id', 'field' => 'subject_id'),
                    array('db' => 'fc.comments', 'dt' => 'comments', 'field' => 'comments'),
                    array('db' => 'fc.faculty_id', 'dt' => 'faculty_id', 'field' => 'faculty_id'),
                    array('db' => 'vn.venue_id', 'dt' => 'venue_id', 'field' => 'venue_id'),
                    array('db' => 'bxc.no_of_plates_used', 'dt' => 'no_of_plates_used', 'field' => 'no_of_plates_used'),
                    array('db' => 'bxc.no_of_cups_used', 'dt' => 'no_of_cups_used', 'field' => 'no_of_cups_used'),
                    array('db' => '(SELECT count(branch_xref_lead_id) FROM branch_xref_lead WHERE fk_batch_id=bxc.fk_batch_id) as regular', 'dt' => 'regular', 'field' => 'regular'),
                    array('db' => '(SELECT COUNT(la.lead_attendence_id) FROM lead_attendence la WHERE la.fk_batch_xref_class_id=bxc.batch_xref_class_id and la.is_attended=1 and la.student_type="current") as regularAttendance', 'dt' => 'regularAttendance', 'field' => 'regularAttendance'),
                    array('db' => '(SELECT COUNT(la.lead_attendence_id) FROM lead_attendence la WHERE la.fk_batch_xref_class_id=bxc.batch_xref_class_id and la.is_attended=1 and la.student_type="old") as oldAttendance', 'dt' => 'oldAttendance', 'field' => 'oldAttendance'),
                    array('db' => '(SELECT COUNT(la.lead_attendence_id) FROM lead_attendence la WHERE la.fk_batch_xref_class_id=bxc.batch_xref_class_id and la.is_attended=1 and la.student_type="guest") as guestAttendance', 'dt' => 'guestAttendance', 'field' => 'guestAttendance'),
                    array('db' => 'if(date(`bxc`.`class_date`)<date(now()),"old", if(date(`bxc`.`class_date`)=date(now()),"current",if(date(`bxc`.`class_date`)>date(now()),"new",""))) AS topic_status', 'dt' => 'topic_status', 'field' => 'topic_status'),
                    array('db' => 'if(`bxc`.`is_active`=1,1,0) as topic_active', 'dt' => 'topic_active', 'field' => 'topic_active'),
                    array('db' => 'if(`bxc`.`attendence_status`=1,1,0) as attendence_status', 'dt' => 'attendence_status', 'field' => 'attendence_status'),
                    array('db' => 'if(`bxc`.`class_date`<now()=1,1,0) as topic_date_status', 'dt' => 'topic_date_status', 'field' => 'topic_date_status'),
                    array('db' => 'if(`bxc`.`class_date`>now()=1,1,0) as topic_date_past', 'dt' => 'topic_date_past', 'field' => 'topic_date_past'),
                );

        $globalFilterColumns = array(
            array('db' => '`bxc`.`class_date`', 'dt' => 'topic_date', 'field' => 'topic_date'),
            array('db' => '`sb`.`code`', 'dt' => 'code', 'field' => 'code'),
            array('db' => '`fc`.`name`', 'dt' => 'faculty_name', 'field' => 'faculty_name'),
            array('db' => '`vn`.`name`', 'dt' => 'venue_name', 'field' => 'venue_name'),
//            array( 'db' => '`b`.`marketing_startdate`'  ,'dt' => 'type_name'   ,'field' => 'type_name' ),
//            array( 'db' => '`b`.`marketing_enddate`'    ,'dt' => 'inst_name'   ,'field' => 'inst_name' ),
//            array('db' => '`b`.`target`', 'dt' => 'target', 'field' => 'target'),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db' => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /*         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        //$joinQuery = " FROM batch_xref_class b left join subject s ON b.fk_subject_id=s.subject_id LEFT JOIN faculty f ON b.fk_faculty_id=f.faculty_id LEFT JOIN venue v ON b.fk_venue_id=v.venue_id";
        $joinQuery = " FROM batch_xref_class bxc LEFT JOIN `subject` sb ON bxc.fk_subject_id=sb.subject_id LEFT JOIN faculty fc ON bxc.fk_faculty_id=fc.faculty_id LEFT JOIN venue vn ON bxc.fk_venue_id=vn.venue_id";
        $extraWhere = "bxc.fk_batch_id=".$batchId;
        $groupBy = "";

        $responseData = (SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere, $groupBy));

        /*for ($i = 0; $i < count($responseData['data']); $i++) {
            $status = 0;
            $start = strtotime($responseData['data'][$i]['topic_date']);
            $current = strtotime(date('Y-m-d'));
            if ($start > $current  || $start = $current) {
                $status = 0;
            } else if ($start < $current) {
                $status = 1;
            }
            $responseData['data'][$i]['topic_date_status'] = $status;
        }*/
        return $responseData;
    }

    /**
     * For having entry in temp table and uploaded files entry
     * @param type $param
     * @return array
     */
    public function BulkClass($param) {
        $this->db->trans_start();
        try {
            $reference_type_value_id  = 0;
            $this->db->select('*');
            $this->db->from('reference_type_value');
            $this->db->where('value','Class');
            $res = $this->db->get();
            if($res){
                $res = $res->result();
                if(count($res) > 0){
                    $reference_type_value_id = $res[0]->reference_type_value_id;
                    
                    $fileName=$param['file_name'];
                    $content=$param['Rows'];
                    $data_mt_insert=array("file_name"=>$fileName,"file_type"=>1,"created_date"=>date('Y-m-d H:i:s'),"fk_created_by"=>$_SERVER['HTTP_USER'],"fk_import_type_id"=>$reference_type_value_id);
                    $res_insert_mt=$this->db->insert('import_history', $data_mt_insert);
                    if($res_insert_mt){
                        $master_id=$this->db->insert_id();
                        $rows=array();
                        $row_number=0;
                        foreach($content as $k=>$v){
                            $row_number=$row_number+1;
                            $rows[]=array(
                                "fk_batch_class_upload_id"=>$master_id,
                                "subject"=>$v['Subject'],
                                "faculty"=>$v['Faculty'],
                                "venue"=>$v['Venue'],
                                "topic"=>$v['Topic'],
                                'no_of_plates_used' => $v['PlatesUsed']!=''?$v['PlatesUsed']:0,
                                'no_of_cups_used' => $v['CupsUsed']!=''?$v['CupsUsed']:0,
                                "class_date"=>$v['Date(dd-mm-yyyy)'],
                                "row_number"=>$row_number
                            );
                        }
                        $res_insert_rows=$this->db->insert_batch('temp_batch_class_import', $rows);
                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            return array('error' => 'loaderror', 'msg' => $error = $this->db->error()['message'], "data"=>array("uploadId"=>$master_id));
                            $this->db->trans_rollback();
                        } else {
                            $this->db->trans_commit();
                            return array("status"=>true,"data"=>array("uploadId"=>$master_id));;
                        }
                    } else{
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }else{
                    return 'norefrence';
                }
            }else{
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } catch (Exception $ex) {
            return array('error' => 'error', 'msg' => $ex->getMessage());
        }
    }

    /**
     * Check if error present for the Import functionality
     */
    function checkBulkUploadErrorExist($row,$file_upload_id,$errorMessage){
        $this->db->select('*');
        $this->db->from('temp_batch_class_import');
        $this->db->where('row_number',$row);
        $this->db->where('fk_batch_class_upload_id',$file_upload_id);
        $this->db->where('status','2');
        $data = $this->db->get();
        if($data){
            $data = $data->result();
            if(count($data)>0){
                $current_error = $data[0]->error_log;
                $update = array('error_log'=>$current_error.', '.$errorMessage);
                $this->db->where(array('fk_batch_class_upload_id' => $file_upload_id, 'row_number' => $row));
                $this->db->update('temp_batch_class_import' ,$update);
            }else{
                $update = array('status' => '2', 'error_log'=>$errorMessage);
                $this->db->where(array('fk_batch_class_upload_id' => $file_upload_id, 'row_number' => $row));
                $this->db->update('temp_batch_class_import' ,$update);
            }
        }
    }
    /**
     * Save all details to main masters table
     * @param type $file_upload_id - fk_batch_class_upload_id
     * @param type $batch_id - batch id
     * @return string
     */
    function SaveBulkClass($file_upload_id, $batch_id) {
        $success_count = 0;
        $falied_count = 0;
        $total_count = 0;
        $duplicates_count = 0;
        $error = 0;
        try {
            /*To remove future class while uploading*/
            $this->db->select('batch_xref_class_id');
            $this->db->from('batch_xref_class');
            $this->db->where('fk_batch_id', $batch_id);
            $this->db->where("'".date('Y-m-d')."' < class_date");
            $res = $this->db->get();
            if($res){
                $res= $res->result();
                if(count($res) > 0){
                    foreach ($res as $k => $v) {
                        $this->db->delete('batch_xref_class',array('batch_xref_class_id' => $v->batch_xref_class_id));
                    }
                }
            }else{
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            
            
            $this->db->trans_start();
            $this->db->select('temp_batch_class_import_id ,subject, faculty, venue, topic, class_date, status, no_of_plates_used, no_of_cups_used, row_number');
            $this->db->from('temp_batch_class_import');
            $this->db->where('fk_batch_class_upload_id',$file_upload_id);
            $query = $this->db->get();

            if($query){
                $query=$query->result();
                if (count($query) > 0) {
                    foreach ($query as $k => $v) {
                        $isError = 0;
                        
                        if(strlen($v->subject) > 0){
                            $this->db->select('subject_id');
                            $this->db->from('subject');
                            $this->db->where('name', $v->subject);
                            $result = $this->db->get();
                            if ($result) {
                                $result = $result->result();
                                if (count($result) > 0) {
                                    $subject = $result[0]->subject_id;
                                }else{
                                    $isError = 1;
                                    $update = array('status' => '2', 'error_log'=>'No subject found');
                                    $this->db->where(array('fk_batch_class_upload_id' => $file_upload_id, 'row_number' => $v->row_number));
                                    $this->db->update('temp_batch_class_import' ,$update);
                                }
                            } else {
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                        }else{
                            $isError = 1;
                            $update = array('status' => '2', 'error_log'=>'Subject is empty');
                            $this->db->where(array('fk_batch_class_upload_id' => $file_upload_id, 'row_number' => $v->row_number));
                            $this->db->update('temp_batch_class_import' ,$update);
                        }
                        
                        /*For faculty*/
                        if(strlen($v->faculty)>0){
                            $this->db->select('faculty_id');
                            $this->db->from('faculty');
                            $this->db->where('name', $v->faculty);
                            $result = $this->db->get();
                            if ($result) {
                                $result = $result->result();
                                if (count($result) > 0) {
                                    $faculty = $result[0]->faculty_id;
                                }else{
                                    $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'No faculty found');
                                    $isError = 1;
                                }
                            } else {
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                        }else{
                            $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'Faculty is emplty');
                            $isError = 1;
                        }
                        
                        /*For Topic*/
                        if(strlen($v->topic)<=0){
                            $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'Topic is empty');
                            $isError = 1;
                        }else{
                            if(!preg_match("/[a-zA-Z0-9 ,.]/", $v->topic)){
                                $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'Topic must contain alphanumeric content');
                                $isError = 1;
                            }else if(strlen($v->topic) > 200){
                                $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'Topic should be max of 200 characters');
                                $isError = 1;
                            }
                        }
                        
                        /*For venue*/
                        if(strlen($v->venue)>0){
                            $this->db->select('venue_id');
                            $this->db->from('venue');
                            $this->db->where('name', $v->venue);
                            $result = $this->db->get();
                            if ($result) {
                                $result = $result->result();
                                if (count($result) > 0) {
                                    $venue = $result[0]->venue_id;
                                }else{
                                    $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'No Venue found');
                                    $isError = 1;
                                }
                            } else {
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                        }else{
                            $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'Venue is empty');
                            $isError = 1;
                        }
                        /*For Date*/
                        $isValidDate = true;
//                        if(!preg_match("/^(0[1-9]|1[0-9]|2[0-9]|3(0|1)) (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sept|Oct|Nov|Dec|jan|feb|mar|apr|may|jun|jul|aug|sept|oct|nov|dec) \d{4}$/", $v->class_date)){
                            if(!preg_match("/^(([1-9]|0[1-9])|1[0-9]|2[0-9]|3(0|1))-(0[1-9]|1[0-2])-\d{4}$/", $v->class_date)){
                                $isValidDate = false;
                            }
//                        }
                        
                        if(!$isValidDate){
                            $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'Date is invalid');
                            $isError = 1;
                        }
                        $currentDate = strtotime(date('Y-m-d'));
                        $uploadedDate = strtotime($v->class_date);
                        if($uploadedDate<$currentDate){
                            $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'Classes can\'t be created for past dates');
                            $isError = 1;
                        }
                        
                        $this->db->select('*');
                        $this->db->from('batch_xref_class');
                        $this->db->where('fk_batch_id', $batch_id);
                        $this->db->where("'".date('Y-m-d', strtotime($v->class_date))."' = class_date");
                        $res = $this->db->get();
                        if($res){
                            $res= $res->result();
                            if(count($res) > 0){
                                $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'Class already scheduled for this date');
                                $isError = 1;
                            }
                        }else{
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                        
                        if($isError == 0){
                            $isValidDate = false;
                            $this->db->select('*');
                            $this->db->from('batch');
                            $this->db->where('batch_id', $batch_id);
                            $this->db->where("'".date('Y-m-d', strtotime($v->class_date))."' BETWEEN acedemic_startdate AND acedemic_enddate");
                            $res = $this->db->get();
                            if($res){
                                $res= $res->result();
                                if(count($res) > 0){
                                    $isValidDate = true;
                                }else{
                                    $isValidDate = false;
                                }
                            }else{
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                            if($isValidDate){
                                $where = array(
                                    'fk_batch_id' => $batch_id,
                                    'fk_subject_id' => $subject,
                                    'fk_faculty_id' => $faculty,
                                    'fk_venue_id' => $venue,
                                    'topic' => $v->topic,
                                    'class_date' => date('Y-m-d H:i:s', strtotime($v->class_date)),
                                    'is_active' => 1
                                );
                                $this->db->select('*');
                                $this->db->from('batch_xref_class');
                                $this->db->where($where);
                                $res = $this->db->get();
                                if($res){
                                    $res= $res->result();
                                    if(count($res) > 0){
                                        //dupicate
                                        $update = array('status' => '2', 'error_log'=>'Duplicate entry');
                                        $this->db->where(array('temp_batch_class_import_id' => $v->temp_batch_class_import_id));
                                        $this->db->update('temp_batch_class_import' ,$update);
                                    }else{
                                        $insert = array(
                                            'fk_batch_id' => $batch_id,
                                            'fk_subject_id' => $subject,
                                            'fk_faculty_id' => $faculty,
                                            'fk_venue_id' => $venue,
                                            'topic' => $v->topic,
                                            'no_of_cups_used' => $v->no_of_cups_used,
                                            'no_of_plates_used' => $v->no_of_plates_used,
                                            'class_date' => date('Y-m-d H:i:s', strtotime($v->class_date)),
                                            'is_active' => 1,
                                            'fk_created_by' => $_SERVER['HTTP_USER'],
                                            'created_date' => date('Y-m-d H:i:s'),
                                        );
                                        $this->db->insert('batch_xref_class', $insert);
                                        $update = array('status' => '1', 'error_log'=>'');
                                        $this->db->where(array('fk_batch_class_upload_id' => $file_upload_id, 'row_number' => $v->row_number));
                                        $this->db->update('temp_batch_class_import' ,$update);
                                    }
                                }else{
                                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                }
                            }else{
                                $this->checkBulkUploadErrorExist($v->row_number, $file_upload_id, 'Date should be between academic start and end date');
                            }
                        }
                    }
                    $this->db->trans_complete();
                }else{
                    return 'nodata';
                }
            }else{
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } catch (Exception $ex) {
            return array('error' => 'error', 'msg' => $ex->getMessage());
        }
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        } else {
            $this->db->trans_commit();
            
            $this->db->select('count(fk_batch_class_upload_id) as totalCnt,sum(if(status=2 && error_log="Duplicate entry",1,0)) as duplicateCnt, sum(if(status=1,1,0)) as successCnt,sum(if(status=2  && error_log IS NOT NULL,1,0)) as failureCnt');
            $this->db->from('temp_batch_class_import');
            $this->db->where('fk_batch_class_upload_id',$file_upload_id);
            $countsQuery=$this->db->get();
            $countsQueryRes=$countsQuery->result();
            foreach($countsQueryRes as $kCounts=>$vCounts){
                $total_count=$vCounts->totalCnt;
                $success_count=$vCounts->successCnt;
                $failed_count=$vCounts->failureCnt;
                $duplicates_count=$vCounts->duplicateCnt;
            }
            $finalresp['Counts']=array("total_count"=>$total_count,"successCount"=>$success_count,"failedCount"=>($failed_count-$duplicates_count),"duplicateCount"=>$duplicates_count);
            
            $this->db->select('row_number, topic, status, error_log');
            $this->db->from('temp_batch_class_import');
            $this->db->where('fk_batch_class_upload_id',$file_upload_id);
            $query = $this->db->get();
            
            if($query){
                $query = $query->result();
                if(count($query) > 0){
                    $finalresp['Records']=$query;
                    return $finalresp;
                }else{
                    return 'nodata';
                }
            }else{
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }

    /**
     * *****No used *******
     * Function get Branch Code
     * 
     * @return - string
     * @author : Abhilash
     */
    function getBranchCode() {
        $code = $this->generateRandomString('10');
        $this->db->select('*');
        $this->db->from('batch');
        $this->db->where('code', $code);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) > 0) {
                $this->getBranchCode();
            } else {
                return $code;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    function getBatchGraphData($batchId){
        $dataArray = '';
        $classAttendanceCount = '';
        $classPreviousAttendanceCount = '';
        $classGuestAttendanceCount = '';
        if($this->checkBatchPresent($batchId)){
        
            /*$qry = 'select date(bxc.class_date) classDate,
                    (select ifnull(count(branch_xref_lead_id),0) from branch_xref_lead bxl,lead_stage l where bxl.fk_batch_id=bxc.fk_batch_id and l.lead_stage_id=bxl.status and l.lead_stage="M7") as totalBatchCount,
                    (select ifnull(count(lead_attendence_id),0) from lead_attendence la JOIN batch_xref_class bxca ON bxca.batch_xref_class_id=la.fk_batch_xref_class_id JOIN batch bta ON bxca.fk_batch_id=bta.batch_id JOIN branch_xref_lead bxla ON la.fk_lead_id=bxla.branch_xref_lead_id AND bta.fk_course_id=bxla.fk_course_id where la.fk_batch_xref_class_id=bxc.batch_xref_class_id and la.is_attended=1 and la.student_type="current") classAttendanceCount,
                    (select ifnull(count(lead_attendence_id),0) from lead_attendence la where la.fk_batch_xref_class_id=bxc.batch_xref_class_id and la.is_attended=1 and la.student_type="old") classPreviousAttendanceCount,(select ifnull(count(lead_attendence_id),0) from lead_attendence la where la.fk_batch_xref_class_id=bxc.batch_xref_class_id and la.is_attended=1 and la.student_type="guest") classGuestAttendanceCount from batch_xref_class bxc 
                    where bxc.is_active=1  and bxc.fk_batch_id='.$batchId.' order by bxc.class_date ASC';*/
            $qry = "SELECT bxc.class_date as classDate,
                    (SELECT count(la.lead_attendence_id) FROM lead_attendence la WHERE la.fk_batch_xref_class_id=bxc.batch_xref_class_id AND la.student_type='current') as totalBatchCount,
                    (SELECT count(la.lead_attendence_id) FROM lead_attendence la WHERE la.fk_batch_xref_class_id=bxc.batch_xref_class_id AND la.student_type='current' AND la.is_attended=1) as classAttendanceCount,
                    (SELECT count(la.lead_attendence_id) FROM lead_attendence la WHERE la.fk_batch_xref_class_id=bxc.batch_xref_class_id AND la.student_type='old' AND la.is_attended=1) as classPreviousAttendanceCount,
                    (SELECT count(la.lead_attendence_id) FROM lead_attendence la WHERE la.fk_batch_xref_class_id=bxc.batch_xref_class_id AND la.student_type='guest' AND la.is_attended=1) as classGuestAttendanceCount
                    FROM batch_xref_class bxc
                    WHERE bxc.fk_batch_id=".$batchId." order by bxc.class_date ASC";

            $res_all = $this->db->query($qry);
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    foreach ($res_all as $k => $v) {
                        $v->classDate = (new DateTime($v->classDate))->format('d M,Y');
//                        $dataArray[] = (new DateTime($v->classDate))->format('d M,Y');
//                        $classAttendanceCount[] = $v->classAttendanceCount == 0?'':(float)$v->classAttendanceCount;
//                        $classPreviousAttendanceCount[] = $v->classPreviousAttendanceCount==0?'':(float)$v->classPreviousAttendanceCount;
//                        $classGuestAttendanceCount[] = $v->classGuestAttendanceCount==0?'':(float)$v->classGuestAttendanceCount;
                    }
                    /*return array(
                                'dates' => $dataArray,
                                'regular' => $classAttendanceCount,
                                'guest' => $classGuestAttendanceCount,
                                'old' => $classPreviousAttendanceCount,
                                'all' => $res_all
                            );*/
                    return $res_all;
                } else {
                    return 'nodata';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }else{
            return 'nobatch';
        }
    }
    
    /**
     * Chheck the date format
     */
    function validateDate($date)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

    /**
     * *****No used *******
     * Function get Random string
     * 
     * @return - string
     * @author : Abhilash
     */
    function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function getTotalClasses($batchId){
        $data=[];
        $this->db->select('bxc.batch_xref_class_id,bxc.class_date,v.name location,f.name as faculty');
        $this->db->from('batch_xref_class bxc');
        $this->db->join('venue v','v.venue_id=bxc.fk_venue_id','left');
        $this->db->join('faculty f','bxc.fk_faculty_id=f.faculty_id','left');
        $this->db->where('bxc.fk_batch_id',$batchId);
        $this->db->where('bxc.is_active',1);
        $this->db->order_by('bxc.batch_xref_class_id','asc');
        $currentClassRes=$this->db->get();
        $data['totalClassCount']=0;
        $data['currentClassCount']=0;
        if($currentClassRes){
            $totalClassCount=$currentClassRes->num_rows();
            $data['totalClassCount']=$totalClassCount;
        }

        $this->db->select('bxc.batch_xref_class_id,bxc.class_date,v.name location,f.name as faculty');
        $this->db->from('batch_xref_class bxc');
        $this->db->join('venue v','v.venue_id=bxc.fk_venue_id','left');
        $this->db->join('faculty f','bxc.fk_faculty_id=f.faculty_id','left');
        $this->db->where('bxc.fk_batch_id',$batchId);
        $this->db->where('date(bxc.class_date)<=date(now())');
        $this->db->where('bxc.is_active',1);
        $this->db->order_by('bxc.batch_xref_class_id','asc');
        $currentClassRes=$this->db->get();
        if($currentClassRes){
            $position=$currentClassRes->num_rows();
        }
        else{
            $position=0;
        }
        $data['currentClassCount']=$position;
        return $data;
    }

}
