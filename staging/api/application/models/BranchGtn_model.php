<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-19
 * Time: 05:10 PM
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class BranchGtn_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    public function getProductList($val,$branchId=0)
    {
        //$query = $this->db->query("SELECT product_id as id,CONCAT_WS(' | ',code,name) as text FROM product WHERE (code like '%".$val."%' or name like '%".$val."%') AND product_id in (select si.fk_product_id as available_quantity from stockflow s,stockflow_item si where s.stockflow_id=si.fk_stockflow_id AND s.type='GRN' AND s.receiver_mode='branch' AND s.fk_receiver_id=$branchId AND (si.quantity-si.returned_quantity)>0)");

        $sqlQuery = "select CONCAT_WS(' | ',id,(sum(avail)-sum(dispatched)-sum(issued))) as id,text from
        (SELECT product_id as id,CONCAT_WS(' | '
        ,code,name) as text,(sum(si.quantity)-sum(returned_quantity)) as avail,0 as dispatched,0 as issued FROM product p,stockflow s,stockflow_item si WHERE  (p.code like '%".$val."%' or p.name like '%".$val."%') and p.product_id=si.fk_product_id and  s.stockflow_id=si.fk_stockflow_id AND s.type='GRN'
        AND s.receiver_mode='branch' AND s.fk_receiver_id=$branchId AND (si.quantity-si.returned_quantity)>0 group by
        p.product_id
        UNION ALL
        SELECT product_id id,CONCAT_WS(' | '
        ,code,name) as text,0 as avail,sum(si.quantity) as dispatched,0 as issued FROM product p,stockflow s,stockflow_item si WHERE  (p.code like '%".$val."%' or p.name like '%".$val."%') and p.product_id=si.fk_product_id and  s.stockflow_id=si.fk_stockflow_id AND s.type='GTN'
        AND s.requested_mode='branch' AND s.fk_requested_id=$branchId AND (si.quantity)>0 group by
        p.product_id
		UNION ALL
        SELECT product_id id,CONCAT_WS(' | '
        ,p.code,p.name) as text,0 as avail,0 as dispatched,sum(lis.quantity) as issued FROM product p,lead_item_issue lis,batch b WHERE  (p.code like '%".$val."%' or p.name like '%".$val."%') and p.product_id=lis.fk_product_id and lis.status=0 and lis.fk_batch_id=b.batch_id and b.fk_branch_id=$branchId and (lis.quantity)>0 group by
        p.product_id

		) z group by z.id";

        //$query = $this->db->query("SELECT concat_ws(' | ',product_id,(sum(si.quantity)-sum(si.returned_quantity))) as id,CONCAT_WS(' | ',code,name) as text FROM product p,stockflow s,stockflow_item si WHERE  (p.code like '%".$val."%' or p.name like '%".$val."%') and p.product_id=si.fk_product_id and  s.stockflow_id=si.fk_stockflow_id AND s.type='GRN' AND s.receiver_mode='branch' AND s.fk_receiver_id=$branchId AND (si.quantity-si.returned_quantity)>0 group by p.product_id");
        $query = $this->db->query($sqlQuery);
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    public function getPreviousRecords()
    {
        $this->db->select('sequence_number');
        $this->db->from('stockflow');
        $this->db->where('type','GTN');
        $this->db->order_by('stockflow_id','desc');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $recordId = $query->row();
            $previousId = $recordId->sequence_number;
        }
        else{
            $previousId = 0;
        }
        return $previousId;
    }
    public function addBranchGtn($data)
    {
        $receiverMode = $data['receiverMode'];
        $receiverObjectId = $data['receiverObjectId'];
        $requestMode = $data['requestMode'];
        $requestObjectId = $data['requestObjectId'];
        $createdBy = $data['userID'];
        $invoiceNumber = $data['invoice_number'];
        $invoiceAttachment = $data['invoice_file'];
        $gtnRemarks = $data['remarks'];
        $createdDate = date('Y-m-d H:i:s');
        $products = explode(',',$data['products']);
        $valid=1;
        for($p=0;$p<count($products);$p++)
        {
            $product = explode('||',$products[$p]);
            $query="select si.fk_product_id from stockflow s,stockflow_item si where s.stockflow_id=si.fk_stockflow_id AND s.type='GRN' AND s.receiver_mode='branch' AND s.fk_receiver_id=$requestObjectId AND (si.quantity-si.returned_quantity)>=".$product[1]." AND si.fk_product_id=".$product[0]."";

            $res=$this->db->query($query);
            if($res){
                if($res->num_rows()>0){

                }
                else{
                    $valid=0;
                }
            }
            else{
                $valid=0;
            }

        }
        if($valid==0){
            return array("status"=>false,"message"=>"Invalid product or no sufficient stock at branch. ","data"=>[]);
        }

        //need check received stock is less than request stock
        $previousRecord = $this->getPreviousRecords();
        if($previousRecord == 0)
        {
            $sequenceNumber = 10001;
        }
        else{
            $sequenceNumber = $previousRecord+1;
        }
        $grnId = $this->db->insert('stockflow',array('sequence_number'=>$sequenceNumber,'type'=>'GTN','receiver_mode'=>$receiverMode,'fk_receiver_id'=>$receiverObjectId,'requested_mode'=>$requestMode,'fk_requested_id'=>$requestObjectId,'created_by'=>$createdBy,'created_date'=>$createdDate,'invoice_number'=>$invoiceNumber,'invoice_attachment'=>$invoiceAttachment,'remarks'=>$gtnRemarks));
        if($grnId)
        {
            $lastInsertedId = $this->db->insert_id();
            for($p=0;$p<count($products);$p++)
            {
                $product = explode('||',$products[$p]);
                $data = array(
                    'fk_stockflow_id'=>$lastInsertedId,
                    'fk_product_id'=>$product[0],
                    'quantity' => $product[1]
                );
                $this->db->insert('stockflow_item',$data);
            }
            $status = true;
            $message = 'Transferred successfully';
        }
        else{
            $error = $this->db->error();
            $status=FALSE;
            $message=$error['message'];
        }
        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function getBranchGtnProducts($seqNum)
    {
        $this->db->select('si.quantity,p.`name` as productName');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','s.stockflow_id=si.fk_stockflow_id','left');
        $this->db->join('product p','p.product_id=si.fk_product_id');
        $this->db->where('s.sequence_number',$seqNum);
        $query = $this->db->get();
        if($query){
            if($query->num_rows()>0){
                $rows=$query->result();
                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;
    }
    public function checkInvoiceNoExist($param)
    {
        $this->db->select("stockflow_id");
        $this->db->from("stockflow");
        $this->db->where("invoice_number", $param['invoiceNumber']);
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>false,"message"=>'Invoice number already exist',"data"=>array("invoiceNumber"=>'Invoice number already exist'));
            }
            else
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function branchGtnDataTables($branchId)
    {
        $table = 'stockflow s';
        $primaryKey = 's`.`stockflow_id';
        $columns = array(
            array( 'db' => 'CONCAT_WS("",if(s.requested_mode="branch",(select code from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse","'.INVENTORY_WAREHOUSE_PREFIX.'","NA"))))),`s`.`sequence_number`) as new_sequence_number', 'dt' => 'new_sequence_number',          'field' => 'new_sequence_number' ),
            array( 'db' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA"))))) as receiveName',              'dt' => 'receiveName',           'field' => 'receiveName'),
            array( 'db' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA"))))) as requestedName',              'dt' => 'requestedName',           'field' => 'requestedName'),
            array( 'db' => 'sum(si.quantity) as total',                 'dt' => 'total',                    'field' => 'total' ),
            array( 'db' => 's.invoice_number',                 'dt' => 'invoice_number',                    'field' => 'invoice_number' ),
            array( 'db' => 's.invoice_attachment',                 'dt' => 'invoice_attachment',                    'field' => 'invoice_attachment' ),
            array( 'db' => 'sum(ifnull(si.returned_quantity,0)) as returnTotal',  'dt' => 'returnTotal',              'field' => 'returnTotal'),
            array( 'db' => '`s`.`created_date` as sentOn',                     'dt' => 'sentOn',          'field' => 'sentOn' ),
            array( 'db' => '`s`.`created_date` as receivedOn',                     'dt' => 'receivedOn',          'field' => 'receivedOn'),
            array( 'db' => '`s`.`stockflow_id`',                     'dt' => 'stockflow_id',          'field' => 'stockflow_id' ),
            array( 'db' => '`s`.`sequence_number`',                     'dt' => 'sequence_number',          'field' => 'sequence_number' ),
        );
        $globalFilterColumns = array(
            array( 'db' => 'CONCAT_WS("",if(s.requested_mode="branch",(select code from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse","'.INVENTORY_WAREHOUSE_PREFIX.'","NA"))))),`s`.`sequence_number`)', 'dt' => 'new_sequence_number',          'field' => 'new_sequence_number' ),
            array( 'db' => '`s`.`sequence_number`',            'dt' => 'sequence_number',         'field' => 'sequence_number' ),
            array( 'db' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))',              'dt' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))',           'field' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))'),
            array( 'db' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))',              'dt' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))',           'field' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))'),
            array( 'db' => '`s`.`created_date`',                     'dt' => 'created_date',          'field' => 'created_date' ),
            array( 'db' => '`s`.`invoice_number`',                     'dt' => 'invoice_number',          'field' => 'invoice_number' )
        );
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        $joinQuery = "from stockflow s JOIN stockflow_item si ON s.stockflow_id=si.fk_stockflow_id";
        $extraWhere = "s.type='GTN' and s.requested_mode='branch' and s.fk_requested_id=".$branchId;
        $groupBy = "s.stockflow_id";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery,$extraWhere,$groupBy));
        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['receivedOn']=date('j M, Y',strtotime($responseData['data'][$i]['receivedOn']));
            $responseData['data'][$i]['sentOn']=date('j M, Y',strtotime($responseData['data'][$i]['sentOn']));
            $responseData['data'][$i]['invoice_attachment']=urlencode(basename('./'.$responseData['data'][$i]['invoice_attachment']));
        }
        return $responseData;
    }
}
