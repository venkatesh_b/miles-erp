<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @controller Name Receipt Deletion report
 * @category        Model
 * @author          Parameshwar
 */
class CashCounter_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    public function getCashCounterListReport($branch, $course, $receiptmode,$company,$paymentMode,$fromDate,$toDate){
        $payment_status='"'."'Success'".'"';
        $this->db->close();
        $this->load->database();
        $query = $this->db->query("call Sp_Get_CashCounter_List_Report ({$branch}, {$course}, {$receiptmode},{$company},{$fromDate},{$toDate},NULL,{$payment_status},{$paymentMode})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $k=>$v){
                    if(isset($v->deletedOn))
                    $v->deletedOn=date('d M, Y',strtotime($v->deletedOn));
                    if(isset($v->receipt_date))
                        $v->receipt_date=date('d M, Y',strtotime($v->receipt_date));
                    if(isset($v->created_date))
                        $v->created_date=date('d M, Y',strtotime($v->created_date));
                    if(isset($v->created_on))
                        $v->created_on=date('d M, Y',strtotime($v->created_on));

                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$res);
            }
            else{
                $db_response=array('status'=>false,'message'=>'No Records Found','data'=>array('message'=>'No Records Found'));
            }
        } else {
            $error = $this->db->error()['message'];
            $db_response=array('status'=>false,'message'=>$error,'data'=>array('message'=>$error));
        }
        return $db_response;
    }
    public function getReceiptDeletionListReport($branch, $course, $receiptmode,$company,$fromDate,$toDate,$paymentMode=NULL){
        $payment_status='"'."'Cancelled','Reversed'".'"';
        $this->db->close();
        $this->load->database();
        $query = $this->db->query("call Sp_Get_CashCounter_List_Report ({$branch}, {$course}, {$receiptmode},{$company},{$fromDate},{$toDate},NULL,{$payment_status},{$paymentMode})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $k=>$v){
                    $v->deletedOn=date('d M, Y',strtotime($v->deletedOn));
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$res);
            }
            else{
                $db_response=array('status'=>false,'message'=>'No Records Found','data'=>array('message'=>'No Records Found'));
            }
        } else {
            $error = $this->db->error()['message'];
            $db_response=array('status'=>false,'message'=>$error,'data'=>array('message'=>$error));
        }
        return $db_response;
    }

    public function getBranchListByUserId($userId) {
        $qry = "select
                b.branch_id id, b.name name
            from branch b
            left join branch_xref_user bxu ON b.branch_id=bxu.branch_id
             where b.is_active=1 AND bxu.user_id=".$userId;

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    function getAllBranchCoursesList($params){
        $branchId=$params['branchId'];
        $this->db->select("c.course_id courseId, c.name, c.description,c.is_active,c.fk_created_by");
        $this->db->from("course c");
        $this->db->join("branch_xref_course bxc" ,"bxc.fk_course_id=c.course_id");
        $this->db->where("bxc.fk_branch_id", $branchId);

        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            $db_response=array("status"=>true,"message"=>"success","data"=>$list);
        }
        else
        {
            $db_response=array("status"=>false,"message"=>"fail","data"=>array());
        }
        return $db_response;
    }
    public function getCompaniesList(){

        $this->db->select("company_id,name,code");
        $this->db->from('company');
        $this->db->order_by("name");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'No Companies Found',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
}
