<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 18-03-2016
 * Time: 10:04 AM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ColdCalling_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('encryption');
        $this->db->db_debug = FALSE;
    }
    function getDNDCountConstant(){
        $this->db->select('value');
        $this->db->from('app_configuration_setting');
        $this->db->where('key','DND CONFIG');
        $res=$this->db->get();
        if($res){
            if($res->num_rows()>0){
                $row=$res->result();
                return $row[0]->value;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }
    function saveColdCall($params){
        $lead_status='';
        //user_xref_lead_history
        $maxCount=$this->getDNDCountConstant();
        $userId=$params['userId'];
        $leadId=$params['leadId'];
        $lead_status=$params['ColdCallStatus'];
        $description=NULL;
        $lead_name=$params['ColdCallContactName'];
        $lead_email=$params['ColdCallContactEmail'];
        $lead_phone=$params['ColdCallContactPhone'];
        $lead_altphone=$params['ColdCallContactAltPhone'];
        $description=$params['ColdCallDescription'];
        $callType = $params['callType'];
        $contactNextFollowupDate=$params['ColdcallNextFollowupDate'];
        $insert_into_branchLead=0;
        $lead_number=0;
        $this->db->select("uld.call_schedule_head_id,l.name,l.email,l.fk_company_id,l.fk_qualification_id,l.fk_institution_id,l.is_dnd,l.fk_contact_type_id,l.logic_item_name,l.logic_item_count");
        $this->db->from('lead l');
        $this->db->join('user_xref_lead_due uld','uld.lead_id=l.lead_id');
        $this->db->where('uld.user_id',$userId);
        $this->db->where('l.lead_id',$leadId);
        $getLeadDetails=$this->db->get();
        $LeadDetails=$getLeadDetails->result();
        $logic_item_name=NULL;
        $logic_item_count=0;
        foreach($LeadDetails as $key=>$val){

            $headId=$val->call_schedule_head_id;
            $lead_is_dnd=0;
            $fk_course_id=NULL;
            $fk_company_id=$val->fk_company_id;
            $fk_qualification_id=$val->fk_qualification_id;
            $fk_institution_id=$val->fk_institution_id;
            $contactType=$val->fk_contact_type_id;
            $logic_item_name=$val->logic_item_name;
            $logic_item_count=$val->logic_item_count;

        }
        if($params['ColdCallStatus']=='switchoff'){
            if('switchoff'==$logic_item_name){
                $logic_item_name='switchoff';
                $logic_item_count=$logic_item_count+1;
            }
            else{
                $logic_item_name='switchoff';
                $logic_item_count=1;
            }
            if($logic_item_count==$maxCount){
                $lead_is_dnd=1;
                $logic_item_name=NULL;
                $logic_item_count=0;
            }
        }
        if($params['ColdCallStatus']=='notlifted'){
            if('notlifted'==$logic_item_name){
                $logic_item_name='notlifted';
                $logic_item_count=$logic_item_count+1;
            }
            else{
                $logic_item_name='notlifted';
                $logic_item_count=1;
            }
            if($logic_item_count==$maxCount){
                $lead_is_dnd=1;
                $logic_item_name=NULL;
                $logic_item_count=0;
            }
        }
        if($params['ColdCallStatus']=='notexist'){
            if('notexist'==$logic_item_name){
                $logic_item_name='notexist';
                $logic_item_count=$logic_item_count+1;
            }
            else{
                $logic_item_name='notexist';
                $logic_item_count=1;
            }
            if($logic_item_count==$maxCount){
                $lead_is_dnd=1;
                $logic_item_name=NULL;
                $logic_item_count=0;
            }
        }

        if($params['ColdCallStatus']=='answered'){

            if($params['ColdcallStatusInfo']=='callStatusInfoIntrestedTransfer' || $params['ColdcallStatusInfo']=='callStatusInfoIntrested'){
                $logic_item_name=NULL;
                $logic_item_count=0;
                $lead_is_dnd = 0;
                $fk_course_id=$params['ColdCallContactCourse'];
                $branch_id=$params['ColdCallContactBranch'];
                $insert_into_branchLead=1;

                $this->db->select('lead_stage_id,ifnull(call_after_days,0) as call_after_days');
                $this->db->from('lead_stage');
                $this->db->where('lead_stage','M3');
                $query_leadStage=$this->db->get();
                $leadStageDetails=$query_leadStage->result();
                //$next_followup_date=date('Y-m-d H:i:s',strtotime(' +1 day'));
                foreach($leadStageDetails as $k_leadStage=>$v_leadStage){
                    $lead_stage=$v_leadStage->lead_stage_id;
                    $next_followup_date=date('Y-m-d H:i:s',strtotime(' +'.$v_leadStage->call_after_days.' day'));
                }
                $next_followup_date=date('Y-m-d H:i:s',strtotime(' +1 day'));
                $fk_qualification_id=$params['ColdCallContactQualification'];
                $getSequenceNumber=1;
                $this->db->select('IFNULL((MAX(lead_number)+1),0) as lead_number');
                $this->db->from('branch_xref_lead');
                $this->db->where('branch_id',$branch_id);
                $get_max_leadNumber=$this->db->get();

                $final_lead_number='';
                if($get_max_leadNumber){
                    if ($get_max_leadNumber->num_rows() > 0){
                        $select_max_leadNumber=$get_max_leadNumber->result();

                        foreach($select_max_leadNumber as $k_lead_number=>$v_lead_number){

                            $final_lead_number=$v_lead_number->lead_number;
                            if($final_lead_number!=0){
                                $getSequenceNumber=0;
                            }

                        }
                    }
                    else{

                    }

                }
                else{

                }
                if($getSequenceNumber==1){
                    $final_lead_number=0;
                    $this->db->select('sequence_number');
                    $this->db->from('branch');
                    $this->db->where('branch_id',$branch_id);
                    $get_max_leadNumber=$this->db->get();
                    if($get_max_leadNumber){
                        if ($get_max_leadNumber->num_rows() > 0){
                            $select_max_leadNumber=$get_max_leadNumber->result();

                            foreach($select_max_leadNumber as $k_lead_number=>$v_lead_number){

                                $final_lead_number=$v_lead_number->sequence_number+1;


                            }
                        }
                        else{

                        }

                    }
                    else{

                    }
                }



            }
            /*if($params['ColdcallStatusInfo']=='callStatusInfoIntrested'){
                $fk_course_id=$params['ColdCallContactCourse'];
                $branch_id=NULL;
                $insert_into_branchLead=1;
                $lead_stage='M3';
                $fk_qualification_id=$params['ColdCallContactQualification'];
                $lead_number=NULL;
                $final_lead_number=0;

            }*/
            if($params['ColdcallStatusInfo']=='callStatusInfoNotintrested'){
                //$description=$params['ColdCallDescription'];
                if('callStatusInfoNotintrested'==$logic_item_name){
                    $logic_item_name='callStatusInfoNotintrested';
                    $logic_item_count=$logic_item_count+1;
                }
                else{
                    $logic_item_name='callStatusInfoNotintrested';
                    $logic_item_count=1;
                }
                if($logic_item_count==$maxCount){
                    $lead_is_dnd=1;
                    $logic_item_name=NULL;
                    $logic_item_count=0;
                }
            }
            if($params['ColdcallStatusInfo']=='callStatusInfoDND'){
                if('callStatusInfoDND'==$logic_item_name){
                    $logic_item_name='callStatusInfoDND';
                    $logic_item_count=$logic_item_count+1;
                }
                else{
                    $logic_item_name='callStatusInfoDND';
                    $logic_item_count=1;
                }
                if($logic_item_count==$maxCount){
                    $lead_is_dnd=1;
                    $logic_item_name=NULL;
                    $logic_item_count=0;
                }
            }
        }
        if($insert_into_branchLead==1){
            if($params['coldCallcontacttype']=='Corporate'){
                $fk_company_id=$params['ColdCallContactCompany'];
                $fk_institution_id=NULL;
            }
            else if($params['coldCallcontacttype']=='University'){
                $fk_institution_id=$params['ColdCallContactInstitution'];
                $fk_company_id=NULL;
            }
            else if($params['coldCallcontacttype']=='Retail'){
                $fk_company_id=NULL;
                $fk_institution_id=NULL;
            }
            else{
                $fk_company_id=NULL;
                $fk_institution_id=NULL;
            }
            $contactType=NULL;
            $this->db->select('reference_type_value_id');
            $this->db->from('reference_type_value rtv');
            $this->db->JOIN('reference_type rt','rt.reference_type_id=rtv.reference_type_id');
            $this->db->where('rt.name','Contact Type');
            $this->db->where('rtv.value',$params['coldCallcontacttype']);
            $getContactType=$this->db->get();
            $tem=$this->db->last_query();
            if($getContactType){
                if($getContactType->num_rows()>0){
                    $getContactTypeRes=$getContactType->result();
                    foreach($getContactTypeRes as $kr=>$vr){
                        $contactType=$vr->reference_type_value_id;
                    }
                }
            }
        }

        $data_update_leadDetails=array("name"=>$lead_name,"email"=>$lead_email,"phone"=>$lead_phone,"email"=>$lead_altphone,"is_dnd"=>$lead_is_dnd,"fk_company_id"=>$fk_company_id,"fk_qualification_id"=>$fk_qualification_id,"fk_institution_id"=>$fk_institution_id,"fk_contact_type_id"=>$contactType,"logic_item_name"=>$logic_item_name,"logic_item_count"=>$logic_item_count,"lead_next_followup_date"=>$contactNextFollowupDate);
        $this->db->where('lead_id',$leadId);
        $res_update_leadDetails=$this->db->update('lead',$data_update_leadDetails);



        $data_user_xref_lead_history_insert=array("user_id"=>$userId,"lead_id"=>$leadId,"call_schedule_head_id"=>$headId,"lead_status"=>$lead_status,"description"=>$description,"created_by" => $userId, "created_on" => date('Y-m-d H:i:s'), "call_type" => $callType);
        $res_user_xref_lead_history_insert=$this->db->insert('user_xref_lead_history',$data_user_xref_lead_history_insert);
        $history_id=$this->db->insert_id();
        if($res_user_xref_lead_history_insert){
            $this->db->where('lead_id',$leadId);
            $this->db->where('call_schedule_head_id',$headId);
            $res_delete_lead_due=$this->db->delete('user_xref_lead_due');
        }
        if($insert_into_branchLead==1) {
            $data_branch_xref_lead_insert = array("lead_id" => $leadId, "branch_id" => $branch_id,"fk_course_id"=>$fk_course_id, "status" => $lead_stage, "created_by" => $userId, "created_on" => date('Y-m-d H:i:s'), "is_active" => 1, "lead_number" => $final_lead_number,"fk_user_xref_lead_history_id"=>$history_id,"next_followup_date"=>$next_followup_date,"call_type" => $callType);
            $res_branch_xref_lead_insert=$this->db->insert('branch_xref_lead',$data_branch_xref_lead_insert);
            $branch_xref_lead_id=$this->db->insert_id();

            $this->db->select('value');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_value_id',$fk_qualification_id);
            $get_qualification=$this->db->get();
            if($get_qualification){
                if($get_qualification->num_rows()>0){
                    $get_qualification_row=$get_qualification->result();
                    $data_insert_education_info=array('lead_id'=>$leadId,"course"=>$get_qualification_row[0]->value,"fk_course_id"=>$fk_qualification_id,"created_on"=>date('Y-m-d H:i:s'),"created_by"=>$userId);
                    $res_branch_xref_lead_insert=$this->db->insert('lead_educational_info',$data_insert_education_info);
                }

            }



        }
        else{
            $branch_xref_lead_id=NULL;
        }
        $data_insert_timeline=array('lead_id'=>$leadId,'branch_xref_lead_id'=>$branch_xref_lead_id,"type"=>"Before Lead Conversion","description"=>$description,"created_by"=>$_SERVER['HTTP_USER'],"created_on"=>date('Y-m-d H:i:s'),"call_type" => $callType);
        $res_insert_timeline=$this->db->insert('timeline',$data_insert_timeline);
        $db_response=array("status"=>true,"message"=>"success","data"=>array());
        return $db_response;


    }
    function coldCallListDataTables($params)
    {
        $filter_condition='';
        if($params['appliedFiltersString']!=''){
            $Filters=explode(',',$params['appliedFiltersString']);
        }
        else{
            $Filters=array();
        }
        if(count($Filters)==0){
            $filter_condition=" l.lead_id IN (select lead_id from user_xref_lead_due where user_id=".$params['userId'].") OR ";
        }
        else {
            if (in_array('history', $Filters)) {
                $filter_condition.=" l.lead_id IN (select lead_id from user_xref_lead_history where user_id=".$params['userId'].") OR ";
            }
            if (in_array('DND', $Filters)) {
                $filter_condition .= " (l.lead_id IN (select lead_id from user_xref_lead_history where user_id=" . $params['userId'] . ") AND l.is_dnd=1) OR ";
            }
            if (in_array('callDue', $Filters)) {
                $filter_condition.=" l.lead_id IN (select lead_id from user_xref_lead_due where user_id=".$params['userId'].") OR ";
            }
            if (in_array('released', $Filters)) {
                $filter_condition .= " l.lead_id IN (select lead_id from user_xref_lead_release where user_id=" . $params['userId'] . ") OR ";
            }
            if (in_array('leadConverted', $Filters)) {
                $filter_condition .= " l.lead_id IN (select lead_id from user_xref_lead_history where user_id=" . $params['userId'] . ") AND l.lead_id IN (select lead_id from branch_xref_lead where created_by=" . $params['userId'] . ") OR ";
            }
        }
        $filter_condition=rtrim($filter_condition,' OR ');
        $filter_condition='('.$filter_condition.')';
        $filter_condition.=' AND ';
        $table = 'lead';
        // Table's primary key
        $primaryKey = 'l`.`lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`l`.`name`'                             ,'dt' => 'name'             ,'field' => 'name' ),
            array( 'db' => '`l`.`email`'                            ,'dt' => 'email'            ,'field' => 'email' ),
            array( 'db' => '`l`.`phone`'                            ,'dt' => 'phone'            ,'field' => 'phone' ),
            array( 'db' => 'rtv.value AS city'                      ,'dt' => 'city'             ,'field' => 'city' ),
            array( 'db' => 'GROUP_CONCAT(DISTINCT(rtvt.`value`) ORDER BY rtvt.`value` ASC) AS `tags`'   ,'dt' => 'tags'             ,'field' => 'tags' ),
            array( 'db' => '`l`.`lead_id`'                          ,'dt' => 'lead_id'          ,'field' => 'lead_id' ),
            array( 'db' => '`u`.`user_xref_lead_due_id`'                          ,'dt' => 'user_xref_lead_due_id'          ,'field' => 'user_xref_lead_due_id' ),
            array( 'db' => '`u`.`call_schedule_head_id`'                          ,'dt' => 'call_schedule_head_id'          ,'field' => 'call_schedule_head_id' ),
            array( 'db' => '(select if(count(lead_id)>1,1,0) from lead l2 where l2.email=l.email and l2.status=1) as `is_duplicate`'   ,'dt' => 'is_duplicate'             ,'field' => 'is_duplicate' ),
            array( 'db' => '`l`.`status`'                          ,'dt' => 'status'          ,'field' => 'status' ),
            array( 'db' => '(select if(count(lead_id)>=1,1,0) from user_xref_lead_due where user_id=' . $params['userId'] . ' and lead_id=l.lead_id) as `is_hist_avail`'   ,'dt' => 'is_hist_avail'             ,'field' => 'is_hist_avail' ),
        );

        $globalFilterColumns = array(
            array( 'db' => '`l`.`email`'        ,'dt' => 'email'            ,'field' => 'email' ),
            array( 'db' => '`l`.`name`'         ,'dt' => 'name'             ,'field' => 'name' ),
            array( 'db' => '`l`.`phone`'        ,'dt' => 'phone'            ,'field' => 'phone' ),
            array( 'db' => 'rtv.value'          ,'dt' => 'value'            ,'field' => 'value' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        //$joinQuery = "FROM user_xref_lead_due u,branch b,lead l LEFT JOIN reference_type_value rtv ON rtv.reference_type_value_id=l.city_id LEFT JOIN lead_xref_tag lxt ON lxt.lead_id=l.lead_id LEFT JOIN reference_type_value rtvt ON rtvt.reference_type_value_id=lxt.tag_id ";
        //$extraWhere = " `l`.`status`=1 AND l.city_id=b.fk_city_id and l.lead_id=u.lead_id and b.branch_id=".$params['branchId']." and u.user_id=".$params['userId']."";
        $joinQuery = "FROM lead l LEFT JOIN user_xref_lead_due u ON u.lead_id=l.lead_id LEFT JOIN reference_type_value rtv ON rtv.reference_type_value_id=l.city_id LEFT JOIN lead_xref_tag lxt ON lxt.lead_id=l.lead_id LEFT JOIN reference_type_value rtvt ON rtvt.reference_type_value_id=lxt.tag_id ";
        //$extraWhere = " $filter_condition `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') ";
        //$extraWhere = " $filter_condition `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') ";
        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."')) OR l.fk_branch_id='".$params['branchId']."')";
        $extraWhere = " $filter_condition `l`.`status`=1 AND $city_condition ";
        $groupBy = "`l`.`lead_id`";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
        }
        return $responseData;
    }
    function coldCallCounts($params){

        $data['overallScheduledCount']='----';
        $data['callsDueCount']='----';
        $data['convertedLeadsCount']='----';
        $data['dndCount']='----';
        $data['rejectedCount']='----';
        $dues=0;
        $hist=0;
        //$dues_query="select count(l.lead_id) as cnt from user_xref_lead_due u,branch b,lead l where `l`.`status`=1 AND l.city_id=b.fk_city_id and l.lead_id=u.lead_id and b.branch_id=".$params['branchId']." and u.user_id=".$params['userId'];
        //$dues_query="select count(l.lead_id) as cnt from user_xref_lead_due u,lead l where `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') and l.lead_id=u.lead_id and u.user_id=".$params['userId'];
        //$dues_query="select count(l.lead_id) as cnt from user_xref_lead_due u,lead l where `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') and l.lead_id=u.lead_id and u.user_id=".$params['userId'];
        $city_condition=" ((l.fk_branch_id IS NULL AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."')) OR l.fk_branch_id='".$params['branchId']."')";
        $dues_query="select count(l.lead_id) as cnt from user_xref_lead_due u,lead l where `l`.`status`=1 AND $city_condition and l.lead_id=u.lead_id and u.user_id=".$params['userId'];
        $res_dues=$this->db->query($dues_query);
        if($res_dues){
            $result_dues=$res_dues->result();
            foreach($result_dues as $k=>$v){
                $dues=$v->cnt;
                $data['callsDueCount']=$dues;
            }
        }
        //$hist_query="select count(l.lead_id) as cnt from user_xref_lead_history u,branch b,lead l where `l`.`status`=1 AND l.city_id=b.fk_city_id and l.lead_id=u.lead_id and b.branch_id=".$params['branchId']." and u.user_id=".$params['userId'];
        //$hist_query="select count(l.lead_id) as cnt from user_xref_lead_history u,lead l where `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') and l.lead_id=u.lead_id and u.user_id=".$params['userId'];
        //$hist_query="select count(l.lead_id) as cnt from user_xref_lead_history u,lead l where `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') and l.lead_id=u.lead_id and u.user_id=".$params['userId'];
        $hist_query="select count(l.lead_id) as cnt from user_xref_lead_history u,lead l where `l`.`status`=1 AND $city_condition and l.lead_id=u.lead_id and u.user_id=".$params['userId'];
        $res_hist=$this->db->query($hist_query);
        if($res_hist){
            $result_hist=$res_hist->result();
            foreach($result_hist as $k=>$v){
                $hist=$v->cnt;
            }
        }
        //$dnd_query="select count(l.lead_id) as cnt from user_xref_lead_history u,branch b,lead l where `l`.`status`=1 AND l.city_id=b.fk_city_id and l.lead_id=u.lead_id and b.branch_id=".$params['branchId']." and u.user_id=".$params['userId']." and l.is_dnd=1";
        //$dnd_query="select count(l.lead_id) as cnt from user_xref_lead_history u,lead l where `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') and l.lead_id=u.lead_id and u.user_id=".$params['userId']." and l.is_dnd=1";
        //$dnd_query="select count(l.lead_id) as cnt from user_xref_lead_history u,lead l where `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') and l.lead_id=u.lead_id and u.user_id=".$params['userId']." and l.is_dnd=1";
        $dnd_query="select count(l.lead_id) as cnt from user_xref_lead_history u,lead l where `l`.`status`=1 AND $city_condition and l.lead_id=u.lead_id and u.user_id=".$params['userId']." and l.is_dnd=1";
        $res_dnd=$this->db->query($dnd_query);
        if($res_dnd){
            $result_dnd=$res_dnd->result();
            foreach($result_dnd as $k=>$v){
                $dnd=$v->cnt;
                $data['dndCount']=$dnd;
            }
        }

        //$convertedLeadsQuery="select count(l.lead_id) as cnt from user_xref_lead_history u,lead l,branch_xref_lead b where l.lead_id=b.lead_id AND `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$params['branchId']."') and l.lead_id=u.lead_id and u.user_id=".$params['userId'];
        //$convertedLeadsQuery="select count(l.lead_id) as cnt from user_xref_lead_history u,lead l,branch_xref_lead b where l.lead_id=b.lead_id AND `l`.`status`=1 AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$params['branchId']."') and l.lead_id=u.lead_id and u.user_id=".$params['userId'];
        $convertedLeadsQuery="select count(l.lead_id) as cnt from user_xref_lead_history u,lead l,branch_xref_lead b where l.lead_id=b.lead_id AND `l`.`status`=1 AND $city_condition and l.lead_id=u.lead_id and u.user_id=".$params['userId']." and u.lead_status='answered' ";
        $res_dnd=$this->db->query($convertedLeadsQuery);
        if($res_dnd){
            $result_dnd=$res_dnd->result();
            foreach($result_dnd as $k=>$v){
                $dnd=$v->cnt;
                $data['convertedLeadsCount']=$dnd;
            }
        }
        $data['overallScheduledCount']=$dues+$hist;
        $db_response=array("status"=>true,"message"=>"success","data"=>$data);
        return $db_response;
    }
    function getBranchPrimaryCities($params){

        $city_query="SELECT fk_city_id,rtv.value as City,rtvState.reference_type_value_id as state_id,rtvState.value as State,rtvCountry.reference_type_value_id as country_id,rtvCountry.value as Country FROM branch_xref_city b,reference_type_value rtv,reference_type_value rtvState,reference_type_value rtvCountry where b.is_primary=1 and b.fk_branch_id='".$params['branchId']."' and rtv.reference_type_value_id=b.fk_city_id and rtvState.reference_type_value_id=rtv.parent_id and rtvCountry.reference_type_value_id=rtvState.parent_id";
        $res_city=$this->db->query($city_query);
        $result_city=$res_city->result();
        $db_response=array("status"=>true,"message"=>"success","data"=>$result_city);
        return $db_response;

    }
    public function addContact($param) {
        //$this->db->trans_begin();
        $data_insert = array(
            'name' => $param['contactName'] ,
            'email' => $param['contactEmail'],
            'phone' => $param['contactPhone'],
            'city_id' => $param['contactCity']);
        $this->db->query('SET @leadId=0');

        $storeproc='CALL Sp_Add_Edit_Contact("'.$param['contactName'].'","'.$param['contactPhone'].'","'.$param['contactEmail'].'","'.$param['contactCity'].'","'.$param['contactCountry'].'","'.$param['contactTags'].'",@leadId,0,0,@msg,"'.$param['contactSource'].'",'.$_SERVER['HTTP_USER'].',"","")';


        $res_insert=$this->db->query($storeproc);

        if($res_insert){
            $query_response = $this->db->query("SELECT @leadId as leadId,@msg as message");
            $query_response_result=$query_response->result();
            $message='Contact added successfully';
            $status=true;
            $leadId=0;
            foreach($query_response_result as $klog=>$vlog){
                if($vlog->message!=''){
                    $message=$vlog->message;
                    $status=false;
                }
                $leadId=$vlog->leadId;
            }
            if($leadId>0)
            {
                    //schedule call here
                    $assignedBy=$param['createdBy'];
                    $assignedTo = $param['createdBy'];
                    $totalAssignedCnt = 1;
                    if ($totalAssignedCnt > 0)
                    {
                        $cnt = 0;
                        $data_insert_head = array("total_scheduled_calls" => $totalAssignedCnt, "assigned_to" => $assignedTo, "assigned_on" => date('Y-m-d H:i:s'), "assigned_by" => $assignedBy);
                        $res_insert_head = $this->db->insert('call_schedule_head', $data_insert_head);
                        if ($res_insert_head)
                        {
                            $head_id = $this->db->insert_id();
                            //$available = "select l.lead_id from lead l,branch b where l.city_id=b.fk_city_id and b.branch_id='" . $params['branchId'] . "' and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;
                            //$available = "select l.lead_id from lead l where l.lead_id='".$leadId."' AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;
                            $available = "select l.lead_id from lead l where l.lead_id='".$leadId."' AND l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where (is_primary=1 or is_secondary=1) and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due)  and l.lead_id not in (select lead_id from branch_xref_lead)  and l.is_dnd=0 order by l.lead_id asc limit " . $totalAssignedCnt;
                            $res = $this->db->query($available);
                            if ($res) {
                                $cnt = $res->num_rows();
                                $result = $res->result();
                                $data_insert_calls = '';
                                foreach ($result as $k_r => $v_r) {
                                    $data_insert_calls[] = array("user_id" => $assignedTo, "lead_id" => $v_r->lead_id, "call_schedule_head_id" => $head_id);
                                }
                                if (count($data_insert_calls) > 0) {
                                    $res_insert_calls = $this->db->insert_batch('user_xref_lead_due', $data_insert_calls);
                                }

                            } else {

                            }

                        }
                    }
            }
            $db_response=array("status"=>$status,"message"=>$message,"data"=>array("message"=>$message));
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }

        //$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            //$this->db->trans_rollback();
        }
        else
        {
            // $this->db->trans_commit();
        }

        return $db_response;

    }

}