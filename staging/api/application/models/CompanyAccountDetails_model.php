<?php

/* 
 * Created By: V Parameshwar. Date: 03-02-2016
 * Purpose: Services for Companies.
 * 
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CompanyAccountDetails_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    public function getAllCompanies(){

        $this->db->select('company_id,name');
        $this->db->from('company');
        $this->db->where('status',1);
        $query = $this->db->get();
        return $query->result();
    }
    public function updateCompanyDetails($data,$id=null){
        $this->db->select('acc_no');
        $this->db->from('company_bank_detail');

        $this->db->where('acc_no',$data['acc_no']);
        if(isset($id)) {
            $this->db->where('company_bank_detail_id !=', $id);
        }
        $query = $this->db->get();

        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{
            $checkBranchcode=$this->checkBranchCode($data['bank_ifsc'],$id);
            if ($query->num_rows() > 0)
            {

                $db_response=array("status"=>false,"message"=>'Already Used',"data"=>array('name'=>'Already Used'));
            }
            //commented by Manasa
            /*else if($checkBranchcode==true){
                $db_response=array("status"=>false,"message"=>'Already Used',"data"=>array('ifsc'=>'Already Used'));
            }*/
            else{
                $this->db->where('company_bank_detail_id', $id);
                $res_update=$this->db->update('company_bank_detail', $data);
                if($res_update){
                    $db_response=array("status"=>true,"message"=>"updated successfully","data"=>array());
                }
                else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
        }

        return $db_response;


    }
    public function getSingleCompanydetail($company_id){
        $this->db->select("*");
        $this->db->from("company_bank_detail");
        $this->db->where("company_bank_detail_id",$company_id);

        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                //exit;
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Company ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function saveCompanyDetails($data){

        $this->db->select('acc_no');
        $this->db->from('company_bank_detail');

        $this->db->where('acc_no',$data['acc_no']);

        $query = $this->db->get();
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{
            $checkBranch=$this->checkBranchCode($data['bank_ifsc'],$id=null);
            if ($query->num_rows() > 0)
            {

                $db_response=array("status"=>false,"message"=>'Already Used',"data"=>array('name'=>'Already Used'));
            }
            // commented by Manasa
            /*else if($checkBranch==true){
                $db_response=array("status"=>false,"message"=>'Already Used',"data"=>array('ifsc'=>'Already Used'));
            }*/
            else{
                $res_insert=$this->db->insert('company_bank_detail', $data);
                if($res_insert){
                    $db_response=array("status"=>true,"message"=>"Added successfully","data"=>array());
                }
                else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
        }

        return $db_response;

    }
    public function checkBranchCode($data,$id){
        $this->db->select('company_bank_detail_id');
        $this->db->from('company_bank_detail');

        $this->db->where('bank_ifsc',$data);
        if(isset($id)) {
            $this->db->where('company_bank_detail_id!=',$id);
        }
        $query = $this->db->get();
        $db_response="";
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{
            if ($query->num_rows() > 0)
            {
                $db_response=true;
            }


        }
        return $db_response;

    }
    public function deleteCompany($id){
        $this->db->where('company_bank_detail_id', $id);
        $status=$this->db->delete('company_bank_detail');
        if($status){
            $db_response=array("status"=>true,"message"=>"Deleted successfully","data"=>array());
        }else{
            $db_response=array("status"=>false,"message"=>"Problem in deleting","data"=>array());
        }
        return $db_response;
    }
    public function getCompanyAccountDetails(){



         $table = 'company';
        // Table's primary key
        $primaryKey = 'c`.`company_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(

            array( 'db' => '`c`.`name`',                            'dt' => 'name',                     'field' => 'name' ),
            array( 'db' => '`cbd`.`acc_no`',                        'dt' => 'acc_no',                   'field' => 'acc_no' ),
            array( 'db' => '`cbd`.`bank_name` ',                    'dt' => 'bank_name',                'field' => 'bank_name' ),
            array( 'db' => '`cbd`.`bank_ifsc`',                     'dt' => 'bank_ifsc',                'field' => 'bank_ifsc' ),
            array( 'db' => '`cbd`.`bank_branch` ',                  'dt' => 'bank_branch',              'field' => 'bank_branch' ),
            array( 'db' => '`cbd`.`bank_city`',                     'dt' => 'bank_city',                'field' => 'bank_city' ),
            array( 'db' => 'if(`cbd`.`status`=1,1,0) as status',    'dt' => 'status',                   'field' => 'status' ),
            array( 'db' => '`cbd`.`company_bank_detail_id`',        'dt' => 'company_bank_detail_id',   'field' => 'company_bank_detail_id' ),
            array( 'db' => '`c`.`company_id`',                      'dt' => 'company_id',               'field' => 'company_id' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`c`.`name`',                'dt' => 'name',         'field' => 'name' ),
            array( 'db' => '`cbd`.`acc_no`',            'dt' => 'acc_no',       'field' => 'acc_no' ),
            array( 'db' => '`cbd`.`bank_name` ',        'dt' => 'bank_name',    'field' => 'bank_name' ),
            array( 'db' => '`cbd`.`bank_ifsc`',         'dt' => 'bank_ifsc',    'field' => 'bank_ifsc' ),
            array( 'db' => '`cbd`.`bank_branch` ',      'dt' => 'bank_branch',  'field' => 'bank_branch' ),
            array( 'db' => '`cbd`.`bank_city`',         'dt' => 'bank_city',    'field' => 'bank_city' ),
            array( 'db' => '`cbd`.`status`',         'dt' => 'status',    'field' => 'status' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM `company` AS `c` JOIN `company_bank_detail` AS `cbd` ON (c.company_id=cbd.fk_company_id) ";
        $extraWhere = "";
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        return $responseData;


    }

    /* Added by s.raju */
}
