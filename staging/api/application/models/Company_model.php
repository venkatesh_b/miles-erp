<?php

/* 
 * Created By: V Parameshwar. Date: 03-02-2016
 * Purpose: Services for Companies.
 * 
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Company_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    public function checkCompany($param){
        $this->db->select("company_id");
        $this->db->from('company');
        $this->db->where("company_id",$param);
        $query = $this->db->get();

        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Company ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    public function getAllCompanies()
    {
        $this->db->select('company_id,name');
        $this->db->from('company');
        $this->db->where('status',1);
        $this->db->order_by('`name`', "asc");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'No Companies Found',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;


    }
    /* Added by s.raju */
    public function saveCompany($data){

        $this->db->select('company_id');
        $this->db->from('company');
        $this->db->where('name',$data['name']);
        $query = $this->db->get();
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{
            $code_check=$this->checkCompanyCode($data['code'],$id=null);
            $voucherCode_check=$this->checkVoucherCode($data['voucher_code'],$id=null);

            if ($query->num_rows() > 0)
            {
                $db_response=array("status"=>false,"message"=>'Name Already Used',"data"=>array('name'=>'Already Used'));
            }
            else if($code_check==true)
            {
                $db_response=array("status"=>false,"message"=>'Code Already Used',"data"=>array('code'=>'Already Used'));
            }
            else if($voucherCode_check==true)
            {
                $db_response=array("status"=>false,"message"=>'Voucher Code Already Used',"data"=>array('voucher_code'=>'Already Used'));
            }
            else{
                $res_insert=$this->db->insert('company', $data);
                if($res_insert){
                    $db_response=array("status"=>true,"message"=>"Added successfully","data"=>array());
                }
                else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
        }

        return $db_response;

    }

    public function getSingleCompany($company_id){
        $this->db->select("*");
        $this->db->from("company");
        $this->db->where("company_id",$company_id);

        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                //exit;
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Company ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function updateCompany($data,$id=null)
    {
        $this->db->select('company_id');
        $this->db->from('company');
        $this->db->where('name',$data['name']);
        $this->db->where('company_id !=',$id);
        $query = $this->db->get();

        if(!$query)
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else
        {
            $code_check=$this->checkCompanyCode($data['code'],$id);
            $voucherCode_check=$this->checkVoucherCode($data['voucher_code'],$id=null);
            if ($query->num_rows() > 0)
            {
                $db_response=array("status"=>false,"message"=>'Already Used',"data"=>array('name'=>'Already Used'));
            }
            elseif($code_check==true)
            {
                $db_response=array("status"=>false,"message"=>'Already Used',"data"=>array('code'=>'Already Used'));
            }
            elseif($voucherCode_check==true)
            {
                $db_response=array("status"=>false,"message"=>'Already Used',"data"=>array('voucher_code'=>'Already Used'));
            }
            else
            {
                $this->db->where('company_id', $id);
                $res_update=$this->db->update('company', $data);
                if($res_update)
                {
                    $db_response=array("status"=>true,"message"=>"updated successfully","data"=>array());
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
        }
        return $db_response;
    }
    function checkCompanyCode($data,$id){

        $this->db->select('company_id');
        $this->db->from('company');

        $this->db->where('code',$data);
        if(isset($id)) {
            $this->db->where('company_id!=',$id);
        }
        $query = $this->db->get();
        $db_response="";
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{
            if ($query->num_rows() > 0)
            {
                $db_response=true;
            }


        }
        return $db_response;
    }

    function checkVoucherCode($data,$id){

        $this->db->select('company_id');
        $this->db->from('company');

        $this->db->where('voucher_code',$data);
        if(isset($id)) {
            $this->db->where('company_id!=',$id);
        }
        $query = $this->db->get();
        $db_response="";
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{
            if ($query->num_rows() > 0)
            {
                $db_response=true;
            }


        }
        return $db_response;
    }
    public function deleteCompany($id)
    {
        $this->db->select("c.company_id");
        $this->db->from('company c');
        $this->db->join('company_bank_detail cbd', 'c.company_id = cbd.fk_company_id');
        if(isset($id))
        {
            $this->db->where("c.company_id", $id);
        }
        $query1 = $this->db->get();

        $this->db->select("c.company_id");
        $this->db->from('company c');
        $this->db->join('fee_head fh', 'fh.fk_company_id = c.company_id');
        if(isset($id))
        {
            $this->db->where("c.company_id", $id);
        }
        $query2 = $this->db->get();
        if($query1->num_rows()>0 || $query2->num_rows()>0)
        {
            $db_response=array("status"=>false,"message"=>"Dependencies are there,can‘t be delete","data"=>array());
        }else{
            if(isset($id))
            {
                $status = $this->db->delete('company', array('company_id' => $id));
                if($status){
                    $db_response=array("status"=>true,"message"=>"Deleted successfully","data"=>array());
                }else
                {
                    $db_response=array("status"=>false,"message"=>"Dependencies are there,can‘t be delete","data"=>array());
                }
            }
        }
        return $db_response;
    }

    public function getCompanies(){

        $table = 'company';
        // Table's primary key
        $primaryKey = 'company_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(

            array( 'db' => 'name',       'dt' => 'name',  'field' => 'name' ),
            array( 'db' => 'code',        'dt' => 'code',  'field' => 'code' ),
            array( 'db' => 'voucher_code',        'dt' => 'voucher_code',  'field' => 'voucher_code' ),
            array( 'db' => 'description',                   'dt' => 'description',              'field' => 'description' ),
            array( 'db' => 'if(`status`=1,1,0) as status','dt' => 'status',              'field' => 'status' ),
            array( 'db' => 'company_id',       'dt' => 'company_id',  'field' => 'company_id' )
        );

        $globalFilterColumns = array(
            array( 'db' => 'name',          'dt' => 'name',         'field' => 'name' ),
            array( 'db' => 'code',          'dt' => 'code',         'field' => 'code' ),
            array( 'db' => 'voucher_code',        'dt' => 'voucher_code',  'field' => 'voucher_code' ),
            array( 'db' => 'description',   'dt' => 'description',  'field' => 'description' ),
            array( 'db' => '`status`',      'dt' => 'status',       'field' => 'status' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from company";
        $extraWhere = "";
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        /*for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['reference_type_value_id']=encode($responseData['data'][$i]['reference_type_value_id']);
        }*/
        return $responseData;


    }

    /* Added by s.raju */

    public function checkCompanyReference($param){
        $this->db->select("rtv.reference_type_value_id,rtv.`value`,rtv.description");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='Company'");
        $this->db->where("rtv.reference_type_value_id",$param);
        $this->db->order_by("rtv.`value`");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                //exit;
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Company ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    public function checkPostQualification($param){
        $this->db->select("rtv.reference_type_value_id,rtv.`value`,rtv.description");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='Post Qualification'");
        $this->db->where("rtv.reference_type_value_id",$param);
        $this->db->order_by("rtv.`value`");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                //exit;
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Company ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
}
