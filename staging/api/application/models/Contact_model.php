<?php
/* 
 * Created By: V Parameshwar. Date: 03-02-2016
 * Purpose: Services for Contacts.
 * 
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
    }
    public function contactConfigs(){
        
    }
    public function checkContact($param){
        
        $contact_id=$param['contactId'];
        $this->db->select('name,status,course,is_lead');
        $this->db->where('contact_id', $contact_id);
        $query = $this->db->get('contact');
        if(!$query){
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else{
            if($query->num_rows()>0){
                 $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
            }
            else{
                $db_response=array("status"=>false,"message"=>'No contact details found! ',"data"=>$query->result());
            }
            }
            return $db_response;
    }
    public function getContacts() {
        
        $final_result=array();
        $this->db->select('sum(if(status=0,1,0)) as newContactsCount,sum(if(status=1,1,0)) as activeContactsCount,sum(if(status=2,1,0)) as blockedContactsCount,sum(if(status=3,1,0)) as inactiveContactsCount,sum(if(status!=3,1,0)) as totalContactsCount,sum(if(status!=3 && is_lead=1 ,1,0)) as convertedLeadCount');
        $query = $this->db->get('contact');
        
        if(!$query){
            $final_result['counts']=array();
        }
        else{
            if($query->num_rows()>0){
                $final_result['counts']=$query->result();
            }
            else{
                $final_result['counts']=array();
            }
           }
           /*select c.contact_id,c.name,c.course,c.email,c.phone,rtv.`value` as city,'Google' company from contact c,reference_type_value rtv where rtv.reference_type_value_id=c.fk_city_id;*/
           
           $this->db->select("c.contact_id,c.name,c.course,c.email,c.phone,rtv.`value` as city,rtv_company.`Value` as company,c.status,c.fk_branch_id");
           $this->db->from('contact c');
           
           $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=c.fk_city_id');
           $this->db->join('reference_type_value rtv_company','rtv_company.reference_type_value_id=c.fk_company_id');
           $this->db->where('c.status!=3');
           $query = $this->db->get();
           
            if($query){
                $data_contacts=$query->result();
                foreach($data_contacts as $k=>$v){
                    //sending tags statically
                    
                    $this->db->select("cxt.fk_tag_id as id,rtv.`value` as tagName");
                    $this->db->from('contact_xref_tag cxt');
                    $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=cxt.fk_tag_id');
                    $this->db->where('cxt.fk_contact_id',$v->contact_id);
                    $query_tag = $this->db->get();
                    $v->tags=$query_tag->result();
                    
                    
                    
                    
                    $this->db->select('name,branch_id as id');
                    
                    $this->db->where('branch_id', $v->fk_branch_id);
                    $query_branch = $this->db->get('branch');
                    
                    $v->branch=$query_branch->result();
                    unset($v->fk_branch_id);
                    
                }
                $final_result['contacts']=$data_contacts;
            }
            else{
                $final_result['contacts']=array();
            }
           
           
            $db_response=array("status"=>true,"message"=>"success","data"=>$final_result);
            
            return $db_response;
        
    }
    public function addContact($param) {
        $this->db->trans_begin();
        $data_insert = array(
            'name' => $param['contactName'] ,
            'course' => $param['contactCourse'],
            'email' => $param['contactEmail'],
            'phone' => $param['contactPhone'],
            'fk_company_id' => $param['contactCompany'],
            'fk_branch_id' => $param['contactBranch'],
            'fk_country_id' => $param['contactCountry'],
            'fk_city_id' => $param['contactCity'],
            'created_date' => date('Y-m-d H:i:s'));
        $res_insert=$this->db->insert('contact', $data_insert);
        if($res_insert){
            $res_contact_id = $this->db->insert_id();
            $contact_tags=explode(',',$param['contactTags']);
            $data_insert_tags=array();
            for($ij=0;$ij<count($contact_tags);$ij++){
                if(trim($contact_tags[$ij])!=''){
                    $data_insert_tags[]=array(
                        'fk_contact_id' => $res_contact_id,
                        'fk_tag_id' => $contact_tags[$ij],
                        'is_active' => 1);
                }
            }
            $res_insert_tags=$this->db->insert_batch('contact_xref_tag', $data_insert_tags);
            
            if($res_insert_tags>=0){
                $db_response=array("status"=>true,"message"=>"contact added successfully","data"=>array());
            }
            else{
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                
            }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }
           
        return $db_response;
        
    }
    public function editContact($param) {
        
    }
    public function activeContact($param){
        
        $contact_id=$param['contactId'];
        $data_update = array('status' => 1);
        $this->db->where('contact_id', $contact_id);
        $res_update=$this->db->update('contact', $data_update);
        if($res_update){
            $db_response=array("status"=>true,"message"=>"success","data"=>array());
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
        
    }
    public function deleteContact($param) {
        $contact_id=$param['contactId'];
        $data_update = array('status' => 3);
        $this->db->where('contact_id', $contact_id);
        $res_update=$this->db->update('contact', $data_update);
        if($res_update){
            $db_response=array("status"=>true,"message"=>"success","data"=>array());
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function blockContact($param) {
        $contact_id=$param['contactId'];
        $data_update = array('status' => 2);
        $this->db->where('contact_id', $contact_id);
        $res_update=$this->db->update('contact', $data_update);
        if($res_update){
            $db_response=array("status"=>true,"message"=>"success","data"=>array());
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function leadConvertContact($param) {
        
        $contact_id=$param['contactId'];
        $data_update = array('is_lead' => 1);
        $this->db->where('contact_id', $contact_id);
        $res_update=$this->db->update('contact', $data_update);
        if($res_update){
            $db_response=array("status"=>true,"message"=>"success","data"=>array());
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function tagContactsCount($tagIds,$branchId)
    {
        //SELECT branch_id,fk_city_id FROM branch WHERE branch_id=1;
        $this->db->select('branch_id,fk_city_id');
        $this->db->from('branch b');
        $this->db->where('b.branch_id', $branchId);
        $branch_data = $this->db->get();
        $data = $branch_data->result_array();
        $city_id=$data[0]['fk_city_id'];

        $this->db->select('rtv.`value` as tagName,count(l.lead_id) as contacts');
        $this->db->from('lead l');
        $this->db->join('lead_xref_tag lt','l.lead_id=lt.lead_id');
        $this->db->join('reference_type_value rtv','lt.tag_id=rtv.reference_type_value_id');
        $this->db->where('l.city_id', $city_id);
        $this->db->where_in('lt.tag_id', $tagIds);
        $this->db->group_by('lt.tag_id');
        $this->db->order_by('lt.tag_id', 'asc');
        $contact_tag_data = $this->db->get();
        $contact_tag=$contact_tag_data->result();
        $db_response=array("status"=>true,"message"=>"success","data"=>$contact_tag);
        return $db_response;
    }
}

