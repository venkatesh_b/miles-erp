<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @Modal Name      Venue
 * @category        Model
 * @author          Abhilash
 */
class Faculty_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    
    public function getAllFaculty() {
        $this->db->select('f.faculty_id, f.name');
        $this->db->from('faculty f');
        $this->db->where('f.is_active', '1');
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }else{
                return 'badrequest';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function getFacultyById($id) {
        $this->db->select('f.faculty_id, f.name, f.fk_subject_id, f.is_active faculty_active, f.comments, s.fk_course_id, s.code subject_code, s.name subject_name, s.is_active subject_active');
        $this->db->from('faculty f');
        $this->db->join('subject s', 'f.fk_subject_id=s.subject_id', 'left');
        $this->db->where('f.faculty_id', $id);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }else{
                return 'badrequest';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function checkFaculty($data,$faculty_id=0)
    {
        $this->db->select('faculty_id');
        $this->db->from('faculty f');
        $this->db->where('f.name ', $data['facultyName']);
        if($faculty_id!=0)
        {
            $this->db->where('f.faculty_id !=', $faculty_id);
        }
        $venue_res = $this->db->get();
        $num = $venue_res->num_rows();
        return $num;
    }
    public function saveFaculty($data) {
        /*For subject*/
        $this->db->select('subject_id');
        $this->db->from('subject');
        $this->db->where_in('subject_id',$data['subject']);
        $this->db->where('is_active', '1');
        $query = $this->db->get();
        if ($query)
        {
            $subjects = $query->num_rows();
            if ($subjects > 0)
            {
                $validate_faculty=$this->checkFaculty($data);
                if($validate_faculty > 0)
                {
                    return 'duplicate';
                }
                else
                {
                    $data_insert = array(
                        'name' => $data['facultyName'],
                        'fk_subject_id' => $data['subject'],
                        'comments' => $data['comment'],
                        'is_active' => '1',
                        'fk_created_by' => $_SERVER['HTTP_USER'],
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    return $this->db->insert('faculty', $data_insert);
                }
            }
            else
            {
                $valid = false;
                return 'nosubject';
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function editFaculty($id, $data) {
        /*For Id*/
        $this->db->select('faculty_id');
        $this->db->from('faculty');
        $this->db->where('faculty_id', $id);
        $query = $this->db->get();
        if ($query)
        {
            $faculty = $query->num_rows();
            if ($faculty > 0)
            {
                $this->db->select('subject_id');
                $this->db->from('subject');
                $this->db->where_in('subject_id',$data['subject']);
                $this->db->where('is_active', '1');
                $query = $this->db->get();
                if ($query)
                {
                    $subjects = $query->num_rows();
                    if ($subjects > 0)
                    {
                        $validate_faculty=$this->checkFaculty($data,$id);
                        if($validate_faculty > 0)
                        {
                            return 'duplicate';
                        }
                        else
                        {
                            $data_update = array(
                                'name' => $data['facultyName'],
                                'fk_subject_id' => $data['subject'],
                                'comments' => $data['comment'],
                                'is_active' => '1',
                                'fk_updated_by' => $_SERVER['HTTP_USER'],
                                'updated_date' => date('Y-m-d H:i:s')
                            );
                            $this->db->where(array('faculty_id' => $id));
                            return $this->db->update('faculty', $data_update);
                        }
                    }
                    else
                    {
                        return 'nosubject';
                    }
                }
            }
            else
            {
                return 'nodata';
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function deleteFaculty($id) {
        $this->db->select('*');
        $this->db->from('faculty');
        $this->db->where('faculty_id', $id);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if ($res_all) {

                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );
                
                $this->db->where('faculty_id', $id);
                $res_update = $this->db->update('faculty', $data_update);

                if ($this->db->error()['code'] == '0') {
                    if ($res_update) {
                        if ($is_active) {
                            return 'active';
                        } else {
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    function getFacultyDatatables(){
        $table = 'faculty';
        // Table's primary key
        $primaryKey = 'f`.`faculty_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`f`.`name`'                                     ,'dt' => 'name'               ,'field' => 'name' ),
            array( 'db' => '`f`.`faculty_id`'                             ,'dt' => 'faculty_id'           ,'field' => 'faculty_id' ),
            array( 'db' => 'if(`f`.`is_active`=1,1,0) as `is_active`'  ,'dt' => 'is_active'        ,'field' => 'is_active' ),
            array( 'db' => '`f`.`fk_subject_id`'                             ,'dt' => 'fk_subject_id'           ,'field' => 'fk_subject_id' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`f`.`name`'             ,'dt' => 'name'          ,'field' => 'name' ),
//            array( 'db' => '`s`.`name`'             ,'dt' => 'subject'   ,'field' => 'subject' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM faculty f";
        $extraWhere = "";
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));
        
        for ($i = 0; $i < count($responseData['data']); $i++) {
            $this->db->select('GROUP_CONCAT(" ",name) subject, count(subject_id) as tot');
            $this->db->from('subject');
            $this->db->where_in('subject_id',explode(',',$responseData['data'][$i]['fk_subject_id']));
            $this->db->where('is_active','1');
            $res = $this->db->get()->result();
            $responseData['data'][$i]['subject'] = $res[0]->subject;
            $responseData['data'][$i]['total'] = $res[0]->tot;
        }

        return $responseData;
    }
    
}
