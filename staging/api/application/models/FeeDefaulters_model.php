<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 02-05-2016
 * Time: 11:22 AM
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class FeeDefaulters_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    public function getFeeDefaultersList($branch, $course, $batch,$company,$feehead){
        $query = $this->db->query("call Sp_Get_Fee_Defaulters({$branch},{$course},{$batch},{$company},{$feehead})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $this->db->close();
                $this->load->database();
                $db_response=array('status'=>true,'message'=>'success','data'=>$res);
            }
            else{
                $db_response=array('status'=>false,'message'=>'No Records Found','data'=>array('message'=>'No Records Found'));
            }
        } else {
            $error = $this->db->error()['message'];
            $db_response=array('status'=>false,'message'=>$error,'data'=>array('message'=>$error));
        }
        return $db_response;
    }
    function getFeeDefaulterInfoByLeadNumber($params){

        $this->db->select('l.name,l.phone,l.email,b.name as branchName,c.name as courseName,fh.name feeHeadName,(cfi.amount_payable-cfi.concession)  as amount_payable,cfi.modified_concession,cfi.right_of_amount,cfi.candidate_fee_item_id,rtvCompany.value as companyName,rtvQualification.value as qualification,rtvInstitution.value as institution,rtvContactType.value as contactType,fsi.due_days,cf.candidate_fee_id,cf.approved_status,cfi.amount_paid,cfi.right_of_given_comments,cf.approval_type');
        $this->db->from('lead l');
        $this->db->JOIN('branch_xref_lead bxl','bxl.lead_id=l.lead_id');
        $this->db->JOIN('branch b','b.branch_id=bxl.branch_id');
        $this->db->JOIN('course c','c.course_id=bxl.fk_course_id');
        $this->db->JOIN('candidate_fee cf','cf.fk_branch_xref_lead_id=bxl.branch_xref_lead_id');
        $this->db->JOIN('candidate_fee_item cfi','cfi.fk_candidate_fee_id=cf.candidate_fee_id');
        $this->db->JOIN('fee_structure_item fsi','fsi.fee_structure_item_id=cfi.fk_fee_structure_item_id');
        $this->db->JOIN('fee_head fh','fh.fee_head_id=fk_fee_head_id');
        $this->db->JOIN('reference_type_value rtvCompany','rtvCompany.reference_type_value_id=l.fk_company_id','LEFT');
        $this->db->JOIN('reference_type_value rtvQualification','rtvQualification.reference_type_value_id=l.fk_qualification_id','LEFT');
        $this->db->JOIN('reference_type_value rtvInstitution','rtvInstitution.reference_type_value_id=l.fk_institution_id','LEFT');
        $this->db->JOIN('reference_type_value rtvContactType','rtvContactType.reference_type_value_id=l.fk_contact_type_id','LEFT');
        $this->db->where('bxl.lead_number',$params['LeadNumber']);
        $this->db->where('cf.is_regular_fee',1);
        $res_query=$this->db->get();
        if($res_query){
            if($res_query->num_rows()>0){
                $result=$res_query->result();
                $result[0]->is_concession_request_pedning=0;
                $result[0]->is_modification_request_pedning=0;
                foreach($result as $k_r=>$v_r)
                {
                    $candidate_fee_id=$v_r->candidate_fee_id;
                    $approved_status=$v_r->approved_status;
                    if($approved_status == '' || $approved_status == null || $approved_status == 'Success')
                        $v_r->approve_status=0;
                    else
                        $v_r->approve_status=1;

                    if($approved_status=='Pending'){
                        if($v_r->approval_type=='Concession'){
                            $result[0]->is_concession_request_pedning=1;
                        }
                        if($v_r->approval_type=='Fee Modification'){
                            $result[0]->is_modification_request_pedning=1;
                        }

                    }
                    $v_r->is_concession_allowed=0;
                    $where="amount_paid > 0  AND fk_candidate_fee_id = ".$candidate_fee_id;
                    $this->db->select('candidate_fee_item_id');
                    $this->db->from('candidate_fee_item');
                    $this->db->where($where);
                    $res_query_sub=$this->db->get();
                    if($res_query_sub->num_rows()>0 || $v_r->approve_status==1)
                    {
                        $v_r->is_concession_allowed=0;
                    }
                    else
                    {
                        $v_r->is_concession_allowed=1;
                    }
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$result);
            }
            else
            {
                $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array('status'=>false,'message'=>$error['message'],'data'=>array());
        }
        return $db_response;
    }
    function checkForCandidateFeeDefaulter($candidateFeeId,$feeConcessionData)
    {
        $ex=0;
        for($f=0;$f<count($feeConcessionData);$f++) {
            $feeConcessionRaw=explode("@@@@@@",$feeConcessionData[$f]);
            $candidateFeeItemId=$feeConcessionRaw[0];
            $concessionAmount=$feeConcessionRaw[2];
            $where = "cf.candidate_fee_id = " . $candidateFeeId." AND cfi.candidate_fee_item_id=".$candidateFeeItemId;
            $this->db->select('cf.candidate_fee_id,cfi.amount_paid,(cfi.amount_payable-cfi.concession) as amountPayable');
            $this->db->from('candidate_fee cf');
            $this->db->JOIN('candidate_fee_item cfi', 'cf.candidate_fee_id=cfi.fk_candidate_fee_id');
            $this->db->where($where);
            $res_query_sub = $this->db->get();
            if($res_query_sub->num_rows()>0){
                $res=$res_query_sub->result();
                $amountPayable=$res[0]->amountPayable;
                $amountPaid=$res[0]->amount_paid;
                if(($amountPayable-$amountPaid)<$concessionAmount){
                    $ex=1;
                    break;
                }
            }
            else{
                $ex=1;
                break;
            }

        }
        return $ex;
    }
    function updateCandidateFeeDefaulter($candidateFeeId,$feeConcessionData)
    {
        $current_datetime=date('Y-m-d H:i:s');
        $userRole=$_SERVER['HTTP_USERROLE'];

        for($f=0;$f<count($feeConcessionData);$f++)
        {
            $feeConcessionRaw=explode("@@@@@@",$feeConcessionData[$f]);
            $candidateFeeItemId=$feeConcessionRaw[0];
            $amountPayable=$feeConcessionRaw[1];
            $concessionAmount=$feeConcessionRaw[2];
            $description=$feeConcessionRaw[3];
            if($userRole == 'Superadmin')
                $updateConcessionQuery="UPDATE candidate_fee_item cfi JOIN candidate_fee cf ON cfi.fk_candidate_fee_id=cf.candidate_fee_id SET cfi.right_of_amount=".$concessionAmount.",cfi.right_of_given_by=".$_SERVER['HTTP_USER'].",cfi.right_of_given_date='".$current_datetime."',right_of_given_comments='".$description."' WHERE cfi.candidate_fee_item_id=".$candidateFeeItemId;
            else
                $updateConcessionQuery="UPDATE candidate_fee_item cfi JOIN candidate_fee cf ON cfi.fk_candidate_fee_id=cf.candidate_fee_id SET cfi.right_of_amount=".$concessionAmount.", cfi.right_of_given_by=".$_SERVER['HTTP_USER'].",cfi.right_of_given_date='".$current_datetime."',right_of_given_comments='".$description."' WHERE cfi.candidate_fee_item_id=".$candidateFeeItemId;
            $query_update=$this->db->query($updateConcessionQuery);
            $data['type']='Right of Amount';
            $data['fk_candidate_fee_id']=$candidateFeeId;
            $data['fk_candidate_fee_item_id']=$candidateFeeItemId;
            $data['amount']=$concessionAmount;
            $data['comments']='Right of Amount Approved';
            $data['fk_created_by']=$_SERVER['HTTP_USER'];
            $data['created_on']=$current_datetime;
            $this->db->insert('fee_audit',$data);
        }
        $db_response=array('status'=>true,'message'=>'Right of amount request is success','data'=>array());
        return $db_response;
    }
}