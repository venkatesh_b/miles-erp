<?php

/* 
 * Created By: V Parameshwar. Date: 03-02-2016
 * Purpose: Services for Companies.
 * 
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FeeHead_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    public function getAllCompanies(){

        $this->db->select('company_id,name');
        $this->db->from('company');
        $this->db->where('status',1);
        $query = $this->db->get();
        return $query->result();
    }
    public function updateFeeHead($data,$id=null){
        $this->db->select('name');
        $this->db->from('fee_head');

        $this->db->where('name',$data['name']);
        $this->db->where('fee_head_id !=',$id);
        $query = $this->db->get();

        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{

            if ($query->num_rows() > 0)
            {
                $db_response=array("status"=>false,"message"=>'Already used',"data"=>array('name'=>'Already used'));
            }
            else{
                $this->db->where('fee_head_id', $id);
                $res_update=$this->db->update('fee_head', $data);
                if($res_update){
                    $db_response=array("status"=>true,"message"=>"updated successfully","data"=>array());
                }
                else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
        }

        return $db_response;


    }
    public function getSingleFeeHeadDetails($company_id){
        $this->db->select("fee_head_id,name,description,fk_company_id,status");
        $this->db->from("fee_head");
        $this->db->where("fee_head_id",$company_id);
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid ID',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function saveFeeHead($data){

        $this->db->select('name');
        $this->db->from('fee_head');

        $this->db->where('name',$data['name']);

        $query = $this->db->get();
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{

            if ($query->num_rows() > 0)
            {

                $db_response=array("status"=>false,"message"=>'Already used',"data"=>array('name'=>'Already used'));
            }
            else{
                $res_insert=$this->db->insert('fee_head', $data);
                if($res_insert){
                    $db_response=array("status"=>true,"message"=>"Added successfully","data"=>array());
                }
                else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
        }

        return $db_response;

    }
    public function deleteFeedHead($id){
        $this->db->db_debug = FALSE;
        $this->db->where('fee_head_id', $id);
        $status=$this->db->delete('fee_head');
        if($status){
            $db_response=array("status"=>true,"message"=>"Deleted successfully","data"=>array());
        }else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>"Dependencies are there,can‘t be delete","data"=>array());
        }
        return $db_response;
    }
    public function getFeeHeadDetails(){



         $table = 'company';
        // Table's primary key
        $primaryKey = 'c`.`company_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`fh`.`name`',     'dt' => 'name',            'field' => 'name' ),
            array( 'db' => '`c`.`name`  as company_name','dt' => 'company_name','field' => 'company_name' ),
            array( 'db' => '`fh`.`description`', 'dt' => 'description',            'field' => 'description' ),
            array( 'db' => 'if(`fh`.`status`=1,1,0) as status', 'dt' => 'status',            'field' => 'status' ),
            array( 'db' => '`fh`.`fee_head_id`',   'dt' => 'fee_head_id','field' => 'fee_head_id' ),
            array( 'db' => '`c`.`company_id`',       'dt' => 'company_id',  'field' => 'company_id' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`fh`.`name`',           'dt' => 'name',     'field' => 'name' ),
            array( 'db' => '`c`.`name`',            'dt' => 'name',     'field' => 'name' ),
            array( 'db' => '`fh`.`description`',    'dt' => 'description',  'field' => 'description' ),
            array( 'db' => '`fh`.`status`',         'dt' => 'status',            'field' => 'status' ),

        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM `company` AS `c` JOIN `fee_head` AS `fh` ON (c.company_id=fh.fk_company_id) ";
        $extraWhere = "";
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        return $responseData;


	}
    
    /**
     * Created by : Abhilash
     */
    public function getSingleFeeHeadAllDetails($feeHeadId){
        $this->db->select("f.fee_head_id, f.name as fee_head_name, f.description as fee_head_description, fc.company_id as fee_company_id, fc.name as fee_company_name");
        $this->db->from("fee_head f");
        $this->db->join("company fc", "f.fk_company_id=fc.company_id");
//        $this->db->where("f.fee_head_id = ".$feeHeadId." AND f.`status`=1");
        $this->db->where("f.name = '".$feeHeadId."' AND f.`status`=1");
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid ID',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    /**
     * Created by - Abhilash
     * @return array
     */
    public function getFeeHeadList(){
        $this->db->select("fee_head_id,name,description,fk_company_id,status");
        $this->db->from("fee_head");
        $this->db->where("status",1);
        $query = $this->db->get();
        if($query)
        {
            if ($query->num_rows() > 0)
            {
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid ID',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }

    /* Added by s.raju */
}
