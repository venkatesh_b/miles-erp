<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @modal Name Fee Structure
 * @category        Modal
 * @author          Abhilash
 */
class FeeStructure_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    /*Old fee Structure History*/
    public function getHistory_old($id){
        $final = [];
        $this->db->select('*');
        $this->db->from('fee_structure f');
        $this->db->where('f.fee_structure_id', $id);
        
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $query = $query[0];
                $whereCondition = array(
                    'fk_course_id' => $query['fk_course_id'],
                    'fk_type_id' => $query['fk_type_id'],
                    'fk_branch_id' => $query['fk_branch_id'],
                    'fk_type_id' => $query['fk_type_id'],
//                    'is_allowed_for_class_room_training' => $query['is_allowed_for_class_room_training']?1:0,
//                    'is_allowed_for_online_training' => $query['is_allowed_for_online_training']?1:0,
                    'effective_from' => date('Y-m-d', strtotime($query['effective_from'])),
                );
                $res = $this->db->order_by('fee_structure_id', 'DESC')->get_where('fee_structure', $whereCondition)->result();
                foreach ($res as $key => $value) {
                    $final[$value->fee_structure_id] = $this->getDetails($value->fee_structure_id);
                }
                return array_values($final);
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    /*New fee Structure History - 6April 2016 12-49PM*/
    var $final = [];
    public function getHistory($id) {
        $this->final[$id] = $id;
        $this->getFeeStrutureHistory($id,1);
        foreach ($this->final as $key => $value) {
            $this->final[$key] = $this->getDetails($value);
        }
        $final = $this->final;
        unset($this->final);
        return array_values($final);
    }
    
    public function getFeeStrutureHistory($id, $pos) {
        $temp = $this->checkParent($id);
        if(!$temp && $pos == 0){
            $this->final[$temp] = $temp;
            $this->getFeeStrutureHistory($temp, 0);
        } else {
            $temp = $this->getParentId($id);
            if($temp){
                if($temp === null){
                    return;
                }else {
                    $this->final[$temp] = $temp;
                    $this->getFeeStrutureHistory($temp, 1);
                }
            } else {
                return;
            }
        }
    }
    
    public function checkParent($id) {
        $this->db->select('f.fee_structure_id');
        $this->db->from('fee_structure f');
        $this->db->where('f.parent_id', $id);
        
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query[0]['fee_structure_id'];
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function getParentId($id){
        $this->db->select('f.parent_id');
        $this->db->from('fee_structure f');
        $this->db->where('f.fee_structure_id', $id);
        
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query[0]['parent_id'];
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function getDetails($id) {
        try{
            $query = $this->db->query("call Sp_Get_FeeHistory($id)");
            if(!$query){
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            else{
                if ($query->num_rows() > 0){
                    $result = $query->result();
                    foreach ($result as $key => $value) {
                        $value->effective_from = (new DateTime($value->effective_from))->format('d M,Y');
                        $value->created_date = (new DateTime($value->created_date))->format('d M,Y');
                        $value->end_date = strlen($value->end_date)<=0?' -- ':(new DateTime($value->end_date))->format('d M,Y');
                    }
                    $finalVar = $result[0];
                    $this->db->close();
                    $this->load->database();
                } else {
                    return 'nodata';
                }
            }
        } catch (Exception $ex) {
            return array('error' => 'dberror', 'msg' => 'Exception',"data"=>array());
        }
        return $finalVar;
    }
    
    public function getSingleFeeStructure($id)
    {
        $feestructure_details='';
        $this->db->select('f.fee_structure_id, f.name, f.fk_course_id, f.fk_corporate_company_id, f.fk_institution_id, f.effective_from, f.fk_created_by, f.is_allowed_for_class_room_training, f.is_allowed_for_online_training, f.description, f.created_date, f.parent_id, f.fk_branch_id,
                c.name courseName, 
                rt.value type_name, rt.reference_type_value_id type_id,
                e.name created_by, e.user_id,
                rtv.reference_type_value_id inst_id, rtv.value inst_name,
                sum(fs.amount) tot,f.fk_training_type,if(f.is_partial_payment_allowed=1,1,0) as is_partial_payment_allowed,if(f.is_regular_fee=1,1,0) as is_regular_fee');
        $this->db->from('fee_structure f');
        $this->db->join("fee_structure_item fs", "f.fee_structure_id=fs.fk_fee_structure_id");
        $this->db->join("course c" , "f.fk_course_id=c.course_id");
        $this->db->join("employee e" , "f.fk_created_by=e.user_id");
        $this->db->join("reference_type_value rt" , "f.fk_type_id=rt.reference_type_value_id");
        $this->db->join("reference_type_value rtv" , "`f`.`fk_corporate_company_id`=`rtv`.`reference_type_value_id` || `f`.`fk_institution_id`=`rtv`.`reference_type_value_id`", "left");
        $this->db->where('f.fee_structure_id', $id);
        $this->db->group_by("f.fee_structure_id");
        $this->db->order_by('fs.due_days', 'asc');
        $feestructure_query = $this->db->get();
        if ($feestructure_query)
        {
            $feestructure_data = $feestructure_query->result_array();
            $feestructure_details['feestructure']=$feestructure_data[0];
            if (count($feestructure_data) > 0)
            {
                foreach($feestructure_data as $k=>$v)
                {
                    $feestructure_data[$k]['effective_from_beforeparse'] = (new DateTime($v['effective_from']))->format('Y-m-d');
                    $feestructure_data[$k]['effective_from'] = (new DateTime($v['effective_from']))->format('d M,Y');
                    $feestructure_data[$k]['created_date'] = (new DateTime($v['created_date']))->format('d M,Y');
                }
                $this->db->select('f.fee_structure_item_id, f.fk_fee_head_id, f.due_days, f.amount, fh.name');
                $this->db->from('fee_structure_item f');
                $this->db->join('fee_head fh','f.fk_fee_head_id=fh.fee_head_id');
                $this->db->where('f.fk_fee_structure_id', $id);
                $this->db->order_by('f.due_days', 'asc');
                $feestructure_item = $this->db->get();
                //echo $this->db->last_query();exit;
                if ($feestructure_item)
                {
                    $feestructure_item_data = $feestructure_item->result_array();
                    if (count($feestructure_item_data) > 0)
                    {
                        $feestructure_details['feeitems']=$feestructure_item_data;
                    }
                    else
                    {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    //return $feestructure_details;
                    return array("status"=>true,"msg"=>'Fee structure details',"data"=>$feestructure_details);
                }
                else
                {
                    $valid = false;
                }
            }
            else
            {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function getAllFeeStructure() {
        $this->db->select('f.fee_structure_id, f.name, f.fk_course_id, f.effective_from, f.fk_created_by,  f.fk_corporate_company_id, f.fk_institution_id, f.is_allowed_for_class_room_training, f.is_allowed_for_online_training, f.fk_corporate_company_id, f.fk_institution_id, f.description, f.created_date, f.parent_id, f.is_active,
                        c.name courseName, 
                        rt.value type_name, rt.reference_type_value_id type_id,
                        e.name created_by, e.user_id,
                        rtv.reference_type_value_id inst_id, rtv.value inst_name,
                        sum(fs.amount) tot');
        $this->db->from('fee_structure f');
        $this->db->join("fee_structure_item fs", "f.fee_structure_id=fs.fk_fee_structure_id");
        $this->db->join("course c" , "f.fk_course_id=c.course_id");
        $this->db->join("employee e" , "f.fk_created_by=e.user_id");
        $this->db->join("reference_type_value rt" , "f.fk_type_id=rt.reference_type_value_id");
        $this->db->join("reference_type_value rtv" , "`f`.`fk_corporate_company_id`=`rtv`.`reference_type_value_id` || `f`.`fk_institution_id`=`rtv`.`reference_type_value_id`", "left");
//        $this->db->where('f.parent_id IS NULL', null, false);
        $this->db->where('is_active', 1);
        $this->db->group_by("f.fee_structure_id");
        
        $query = $this->db->get();
//        die($this->db->last_query());
        if ($query) {
            $query = $query->result_array();
//            print_r($query);die;
            if (count($query) > 0) {
                $res = $query;
                $parent = [];
                foreach($query as $k=>$v){
                    if(strlen($v['parent_id'] ) > 0){
                        $parent[$v['parent_id']] = $v['parent_id'];
                        unset($query[$k]);
                    }
                }
                foreach($query as $k=>$v){
                    $query[$k]['effective_from'] = (new DateTime($v['effective_from']))->format('d M,Y');
                    $query[$k]['created_date'] = (new DateTime($v['created_date']))->format('d M,Y');
                    foreach($parent as $k1=>$v1){
                        if($v['fee_structure_id'] ==$v1 ){
                            unset($query[$k]);
                        }
                    }
                }
                return $query;
            } else {
                $valid = false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function removeExpiredFeeStructure() {
        $this->db->select('*');
        $this->db->from('fee_structure f');
        $this->db->where('f.end_date IS NOT NULL AND f.is_active=1');
        
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                foreach ($query as $key => $value) {
                    $timestamp = strtotime($value['end_date']);
                    $currenttime = strtotime(date('Y-m-d H:i:s'));
                    if ($timestamp < $currenttime) {
                        //build data
                        $updatedData = array(
                            'is_active' => 0,
                        );
                        $this->db->where(array('fee_structure_id' => $value['fee_structure_id']));
                        $this->db->update('fee_structure' ,$updatedData);
                    }
                }
            }
        }
    }
    
    public function feeStructuresListDataTables($name)
    {
        if($name == 'future'){
            $table = 'fee_structure';
            // Table's primary key
            $primaryKey = 'f`.`fee_structure_id';
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
            $columns = array(
                            array( 'db' => '`c`.`name` as `courseName`',                                'dt' => 'courseName',                       'field' => 'courseName' ),
                            array( 'db' => '`f`.`name`',                                                'dt' => 'name',                             'field' => 'name' ),
                            array( 'db' => '`f`.`effective_from`',                                      'dt' => 'effective_from',                   'field' => 'effective_from', 'formatter' => function( $d, $row ) { return date( 'd M,Y', strtotime($d)); }),
                            array( 'db' => 'rtvTrainingType.value as trainingType',                     'dt' => 'trainingType',                     'field' => 'trainingType'),
                            array( 'db' => '`rt`.`value` as `type_name`',                               'dt' => 'type_name',                        'field' => 'type_name' ),
                            array( 'db' => '`rtv`.`value` as `inst_name`',                              'dt' => 'inst_name',                        'field' => 'inst_name' ),
                            array( 'db' => 'if(`f`.`is_regular_fee`=1,"Yes","No") as `is_regular_fee`', 'dt' => 'is_regular_fee',                   'field' => 'is_regular_fee'),
                            array( 'db' => 'sum(fs.amount) as tot',                                     'dt' => 'tot',                              'field' => 'tot' ),
                            array( 'db' => '`b`.`name` as `branch`',                                    'dt' => 'branch',                           'field' => 'branch' ),
                            array( 'db' => '`f`.`fee_structure_id`',                                    'dt' => 'fee_structure_id',                 'field' => 'fee_structure_id' ),
                            array( 'db' => '`f`.`fk_course_id`',                                        'dt' => 'fk_course_id',                     'field' => 'fk_course_id' ),
                            array( 'db' => '`f`.`fk_corporate_company_id`',                             'dt' => 'fk_corporate_company_id',          'field' => 'fk_corporate_company_id' ),
                            array( 'db' => '`f`.`fk_institution_id`',                                   'dt' => 'fk_institution_id',                'field' => 'fk_institution_id' ),
                            array( 'db' => 'if((rtvTrainingType.value="Class Room Training" OR rtvTrainingType.value="Class Room & Online Training"),1,0) as `is_allowed_for_class_room_training`', 'dt' => 'is_allowed_for_class_room_training',       'field' => 'is_allowed_for_class_room_training' ),
                            array( 'db' => 'if((rtvTrainingType.value="Online Training" OR rtvTrainingType.value="Class Room & Online Training"),1,0) as `is_allowed_for_online_training`',         'dt' => 'is_allowed_for_online_training',           'field' => 'is_allowed_for_online_training' ),
                            array( 'db' => '`f`.`fk_corporate_company_id`',                             'dt' => 'fk_corporate_company_id',          'field' => 'fk_corporate_company_id' ),
                            array( 'db' => '`f`.`fk_institution_id`',                                   'dt' => 'fk_institution_id',                'field' => 'fk_institution_id' ),
                            array( 'db' => '`f`.`parent_id`',                                           'dt' => 'fk_institution_id',                'field' => 'fk_institution_id' ),
                            array( 'db' => '`b`.`branch_id`',                                           'dt' => 'branch_id',                        'field' => 'branch_id' ),
                            array( 'db' => 'if(`f`.`is_active`=1,1,0) as `is_active`',                  'dt' => 'is_active',                        'field' => 'is_active'),
                );
            $globalFilterColumns = array(
                array( 'db' => '`c`.`name`',                            'dt' => '`c`.`name`',                               'field' => '`c`.`name`' ),
                array( 'db' => '`f`.`name`',                            'dt' => '`f`.`name`',                               'field' => '`f`.`name`' ),
                array( 'db' => '`f`.`effective_from`',                  'dt' => '`f`.`effective_from`',                     'field' => '`f`.`effective_from`' ),
                array( 'db' => 'rtvTrainingType.value',                 'dt' => 'rtvTrainingType.value',                    'field' => 'rtvTrainingType.value'),
                array( 'db' => '`rt`.`value`',                          'dt' => 'type_name',                                'field' => 'type_name' ),
                array( 'db' => '`rtv`.`value`',                         'dt' => 'inst_name',                                'field' => 'inst_name' ),
                array( 'db' => 'if(`f`.`is_regular_fee`=1,"Yes","No")', 'dt' => 'if(`f`.`is_regular_fee`=1,"Yes","No")',    'field' => 'is_regular_fee'),
                array( 'db' => '`b`.`name`',                            'dt' => '`b`.`name`',                                   'field' => '`b`.`name`' )
            );

            // SQL server connection information
            $sql_details = array(
                'user' => SITE_HOST_USERNAME,
                'pass' => SITE_HOST_PASSWORD,
                'db'   => SITE_DATABASE,
                'host' => SITE_HOST_NAME
            );
            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = " FROM fee_structure f JOIN fee_structure_item fs ON f.fee_structure_id=fs.fk_fee_structure_id JOIN course c ON f.fk_course_id=c.course_id JOIN employee e ON f.fk_created_by=e.user_id JOIN reference_type_value rt ON f.fk_type_id=rt.reference_type_value_id left join branch b on f.fk_branch_id=b.branch_id LEFT JOIN reference_type_value rtv ON (f.fk_corporate_company_id=rtv.reference_type_value_id OR f.fk_institution_id=rtv.reference_type_value_id) LEFT JOIN reference_type_value rtvTrainingType ON f.fk_training_type=rtvTrainingType.reference_type_value_id";
            $extraWhere = "f.effective_from>DATE(NOW()) AND f.is_active=1";
            $groupBy = "f.fee_structure_id";

            $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

            /*for($i=0;$i<count($responseData['data']);$i++)
            {
                $responseData['data'][$i]['user_id']=encode($responseData['data'][$i]['user_id']);
            }*/
            return $responseData;
        }
        else if($name == 'current')
        {
            $this->removeExpiredFeeStructure();
            $table = 'fee_structure';
            // Table's primary key
            $primaryKey = 'f`.`fee_structure_id';
            // Array of database columns which should be read and sent back to DataTables.
            // The `db` parameter represents the column name in the database, while the `dt`
            // parameter represents the DataTables column identifier. In this case simple
            // indexes
            $columns = array(
                array( 'db' => '`c`.`name` as `courseName`',                                'dt' => 'courseName',                       'field' => 'courseName' ),
                array( 'db' => '`f`.`name`',                                                'dt' => 'name',                             'field' => 'name' ),
                array( 'db' => '`f`.`effective_from`',                                      'dt' => 'effective_from',                   'field' => 'effective_from', 'formatter' => function( $d, $row ) { return date( 'd M,Y', strtotime($d)); }),
                array( 'db' => 'rtvTrainingType.value as trainingType',                     'dt' => 'trainingType',                     'field' => 'trainingType'),
                array( 'db' => '`rt`.`value` as `type_name`',                               'dt' => 'type_name',                        'field' => 'type_name' ),
                array( 'db' => '`rtv`.`value` as `inst_name`',                              'dt' => 'inst_name',                        'field' => 'inst_name' ),
                array( 'db' => 'if(`f`.`is_regular_fee`=1,"Yes","No") as `is_regular_fee`', 'dt' => 'is_regular_fee',                   'field' => 'is_regular_fee'),
                array( 'db' => 'sum(fs.amount) as tot',                                     'dt' => 'tot',                              'field' => 'tot' ),
                array( 'db' => '`b`.`name` as `branch`',                                    'dt' => 'branch',                           'field' => 'branch' ),
                array( 'db' => '`f`.`fee_structure_id`',                                    'dt' => 'fee_structure_id',                 'field' => 'fee_structure_id' ),
                array( 'db' => '`f`.`fk_course_id`',                                        'dt' => 'fk_course_id',                     'field' => 'fk_course_id' ),
                array( 'db' => '`f`.`fk_corporate_company_id`',                             'dt' => 'fk_corporate_company_id',          'field' => 'fk_corporate_company_id' ),
                array( 'db' => '`f`.`fk_institution_id`',                                   'dt' => 'fk_institution_id',                'field' => 'fk_institution_id' ),
                array( 'db' => 'if((rtvTrainingType.value="Class Room Training" OR rtvTrainingType.value="Class Room & Online Training"),1,0) as `is_allowed_for_class_room_training`', 'dt' => 'is_allowed_for_class_room_training',       'field' => 'is_allowed_for_class_room_training' ),
                array( 'db' => 'if((rtvTrainingType.value="Online Training" OR rtvTrainingType.value="Class Room & Online Training"),1,0) as `is_allowed_for_online_training`',         'dt' => 'is_allowed_for_online_training',           'field' => 'is_allowed_for_online_training' ),
                array( 'db' => '`f`.`fk_corporate_company_id`',                             'dt' => 'fk_corporate_company_id',          'field' => 'fk_corporate_company_id' ),
                array( 'db' => '`f`.`fk_institution_id`',                                   'dt' => 'fk_institution_id',                'field' => 'fk_institution_id' ),
                array( 'db' => '`f`.`parent_id`',                                           'dt' => 'fk_institution_id',                'field' => 'fk_institution_id' ),
                array( 'db' => '`b`.`branch_id`',                                           'dt' => 'branch_id',                        'field' => 'branch_id' ),
                array( 'db' => 'if(`f`.`is_active`=1,1,0) as `is_active`',                  'dt' => 'is_active',                        'field' => 'is_active'),
            );

            $globalFilterColumns = array(
                array( 'db' => '`c`.`name`',                            'dt' => '`c`.`name`',                               'field' => '`c`.`name`' ),
                array( 'db' => '`f`.`name`',                            'dt' => '`f`.`name`',                               'field' => '`f`.`name`' ),
                array( 'db' => '`f`.`effective_from`',                  'dt' => '`f`.`effective_from`',                     'field' => '`f`.`effective_from`' ),
                array( 'db' => 'rtvTrainingType.value',                 'dt' => 'rtvTrainingType.value',                    'field' => 'rtvTrainingType.value'),
                array( 'db' => '`rt`.`value`',                          'dt' => 'type_name',                                'field' => 'type_name' ),
                array( 'db' => '`rtv`.`value`',                         'dt' => 'inst_name',                                'field' => 'inst_name' ),
                array( 'db' => 'if(`f`.`is_regular_fee`=1,"Yes","No")', 'dt' => 'if(`f`.`is_regular_fee`=1,"Yes","No")',    'field' => 'is_regular_fee'),
                array( 'db' => '`b`.`name`',                            'dt' => '`b`.`name`',                                   'field' => '`b`.`name`' )
            );

            // SQL server connection information
            $sql_details = array(
                'user' => SITE_HOST_USERNAME,
                'pass' => SITE_HOST_PASSWORD,
                'db'   => SITE_DATABASE,
                'host' => SITE_HOST_NAME
            );
            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = " FROM fee_structure f JOIN fee_structure_item fs ON f.fee_structure_id=fs.fk_fee_structure_id JOIN course c ON f.fk_course_id=c.course_id JOIN employee e ON f.fk_created_by=e.user_id JOIN reference_type_value rt ON f.fk_type_id=rt.reference_type_value_id left join branch b on f.fk_branch_id=b.branch_id LEFT JOIN reference_type_value rtv ON (f.fk_corporate_company_id=rtv.reference_type_value_id OR f.fk_institution_id=rtv.reference_type_value_id) LEFT JOIN reference_type_value rtvTrainingType ON f.fk_training_type=rtvTrainingType.reference_type_value_id";
            $extraWhere = "f.effective_from<=DATE(NOW()) AND f.is_active=1";
            $groupBy = "f.fee_structure_id";

            $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

            /*for($i=0;$i<count($responseData['data']);$i++)
            {
                $responseData['data'][$i]['user_id']=encode($responseData['data'][$i]['user_id']);
            }*/
            return $responseData;
        }
    }
    
    public function addFeeStructure($data) {
        $valid = false;
        $data['partialPayment']=(int)$data['partialPayment'];
        $data['regularFee']=(int)$data['regularFee'];
        $final_branches=array();
        if($data['branch'] != 0){
            if (preg_match("/,/", $data['branch']))
            {
                $branch = explode(",", $data['branch']);
                $final_branches=$branch;
                if (is_array($branch) && count($branch) > 0) {
                    foreach ($branch as $key => $val) {
                        $this->db->select('branch_id');
                        $this->db->from('branch');
                        $this->db->where('branch_id', $val);
                        $query = $this->db->get();
                        if ($query) {
                            $query = $query->result_array();
                            if (count($query) > 0) {
                                $valid = true;
                            } else {
                                $valid = false;
                                return 'nobranch';
                            }
                        } else {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                    }
                }
            }
            else
            {
                $this->db->select('branch_id');
                $this->db->from('branch');
                $this->db->where('branch_id', $data['branch']);
                $query = $this->db->get();
                if ($query) {
                    $query = $query->result_array();
                    if (count($query) > 0) {
                        $valid = true;
                    } else {
                        $valid = false;
                        return 'nobranch';
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
                $final_branches[0]=$data['branch'];
            }

        }
        else{
            $final_branches[0]=0;
        }

        /*Course*/
        $this->db->select('course_id');
        $this->db->from('course');
        $this->db->where('course_id', $data['course']);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nocourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /*For Fee Type*/
        $fk_id = '';
        $fk_typeid = '';
        
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value');
        $this->db->where('value', $data['feetype']);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $fk_typeid = $query[0]['reference_type_value_id'];
            } else {
                $valid = false;
                return 'nofeetypeDrop';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        if($data['feetype'] == 'group'){
            $feetype = 'Institution';
            $this->db->select('reference_type_id');
            $this->db->from('reference_type');
            $this->db->where('name', $feetype);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $fk_id = $query[0]['reference_type_id'];
                } else {
                    $valid = false;
                    return 'nofeetypeDrop';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } else if($data['feetype'] == 'corporate'){
            $feetype = 'Company';
            $this->db->select('reference_type_id');
            $this->db->from('reference_type');
            $this->db->where('name', $feetype);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $fk_id = $query[0]['reference_type_id'];
                } else {
                    $valid = false;
                    return 'nofeetypeDrop';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } else if($data['feetype'] == 'retail'){
            $feetype = 'Company';
            $this->db->select('reference_type_id');
            $this->db->from('reference_type');
            $this->db->where('name', $feetype);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $fk_id = $query[0]['reference_type_id'];
                } else {
                    $valid = false;
                    return 'nofeetypeDrop';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } 
        
        
        if($data['feetype']=='group'){
            $this->db->select('reference_type_value_id');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_value_id', $data['groupname']);
            $this->db->where('reference_type_id', $fk_id);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                    $fk_id = $query[0]['reference_type_value_id'];
                } else {
                    $valid = false;
                    return 'nogroupname';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            $data_insert = array(
                'name' => $data['name'],
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_institution_id' => $fk_id,
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
                'description' => $data['description'],
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
        }elseif($data['feetype']=='corporate'){
            $this->db->select('reference_type_value_id');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_value_id', $data['companyName']);
            $this->db->where('reference_type_id', $fk_id);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                    $fk_id = $query[0]['reference_type_value_id'];
                } else {
                    $valid = false;
                    return 'nocompanyname';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            $data_insert = array(
                'name' => $data['name'],
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_corporate_company_id' => $fk_id,
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
                'description' => $data['description'],
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
        }elseif($data['feetype']=='retail'){
            $data_insert = array(
                'name' => $data['name'],
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
                'description' => $data['description'],
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
        }
        
        foreach ($data['item'] as $key => $value) {
            $this->db->select('fee_head_id');
            $this->db->from('fee_head');
            $this->db->where('fee_head_id', $value['id']);
            $this->db->where('status', '1');
            $query = $this->db->get();
//            die($this->db->last_query());
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                } else {
                    $valid = false;
                    return 'nofeehead';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        if($valid)
        {
            $this->db->trans_start();
            
            $whereCondition = array(
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'fs.is_active' => 1,
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
            
            if($data['feetype']=='group'){
                $whereCondition['fk_institution_id'] = $fk_id;
            } else if($data['feetype']=='corporate'){
                $whereCondition['fk_corporate_company_id'] = $fk_id;
            } else if($data['feetype']=='retail'){
//                $whereCondition[''] = '';
                $whereCondition['fk_institution_id'] = NULL;
                $whereCondition['fk_corporate_company_id'] = NULL;
            }

            if($data['branch'] != 0){
                $whereCond = ' fs.`fk_branch_id` IN ('. $data['branch'].')';
            }else{
                $whereCond = ' fs.`fk_branch_id` IS NULL ';
                //$whereCond = '';
            }
            $this->db->select('fs.*,GROUP_CONCAT(distinct `b`.`name`) as branches');
            $this->db->from('fee_structure fs');
            $this->db->join('branch b','b.branch_id=fs.fk_branch_id' ,'left');
            if($whereCond!='') {
                $this->db->where($whereCond);
            }
            $this->db->where($whereCondition);
            //$this->db->group_by('fs.fk_branch_id');
            $this->db->order_by('fee_structure_id', 'DESC');
            $ress = $this->db->get();
            $ress=$ress->result();
            if(count($ress) > 0 && $ress[0]->fee_structure_id!=null){
                if($ress[0]->branches==null){
                    $ress[0]->branches='';
                }
                return 'alreadyPresent-'.$ress[0]->branches;
            } else {
                for($branchCnt=0;$branchCnt<count($final_branches);$branchCnt++){
                    if($final_branches[$branchCnt]==0){

                        $data_insert["fk_branch_id"] = NULL;
                    }else{
                        $data_insert["fk_branch_id"] = $final_branches[$branchCnt];
                    }
                    $this->db->insert('fee_structure', $data_insert);
                    $last_update_id = $this->db->insert_id();
                    foreach ($data['item'] as $key => $value) {
                        $data_insert1 = array(
                            'fk_fee_structure_id' => $last_update_id,
                            'fk_fee_head_id' => $value['id'],
                            'due_days' => $value['days'],
                            'amount' => $value['amount']
                        );
                        $this->db->insert('fee_structure_item', $data_insert1);
                    }
                }

            }
            
            $this->db->trans_complete();
                    
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    
    public function updateFeeStructure($data, $id) {
        $valid = false;
        $data['partialPayment']=(int)$data['partialPayment'];
        $data['regularFee']=(int)$data['regularFee'];
        $this->db->select('fee_structure_id');
        $this->db->from('fee_structure');
        $this->db->where('fee_structure_id', $id);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        if($data['branch'] != 0){
            if (preg_match("/,/", $data['branch']))
            {
                $branch = explode(",", $data['branch']);
                if (is_array($branch) && count($branch) > 0) {
                    foreach ($branch as $key => $val) {
                        $this->db->select('branch_id');
                        $this->db->from('branch');
                        $this->db->where('branch_id', $val);
                        $query = $this->db->get();
                        if ($query) {
                            $query = $query->result_array();
                            if (count($query) > 0) {
                                $valid = true;
                            } else {
                                $valid = false;
                                return 'nobranch';
                            }
                        } else {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                    }
                }
            }
            else
            {
                $this->db->select('branch_id');
                $this->db->from('branch');
                $this->db->where('branch_id', $data['branch']);
                $query = $this->db->get();
                if ($query) {
                    $query = $query->result_array();
                    if (count($query) > 0) {
                        $valid = true;
                    } else {
                        $valid = false;
                        return 'nobranch';
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }
        }
        
        /*Course*/
        $this->db->select('course_id');
        $this->db->from('course');
        $this->db->where('course_id', $data['course']);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nocourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /*For Fee Type*/
        $fk_id = '';
        $fk_typeid = '';
        
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value');
        $this->db->where('value', $data['feetype']);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $fk_typeid = $query[0]['reference_type_value_id'];
            } else {
                $valid = false;
                return 'nofeetypeDrop';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        if($data['feetype'] == 'group'){
            $feetype = 'Institution';
            $this->db->select('reference_type_id');
            $this->db->from('reference_type');
            $this->db->where('name', $feetype);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $fk_id = $query[0]['reference_type_id'];
                } else {
                    $valid = false;
                    return 'nofeetypeDrop';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } else if($data['feetype'] == 'corporate'){
            $feetype = 'Company';
            $this->db->select('reference_type_id');
            $this->db->from('reference_type');
            $this->db->where('name', $feetype);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $fk_id = $query[0]['reference_type_id'];
                } else {
                    $valid = false;
                    return 'nofeetypeDrop';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } else if($data['feetype'] == 'retail'){
            $feetype = 'Company';
            $this->db->select('reference_type_id');
            $this->db->from('reference_type');
            $this->db->where('name', $feetype);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $fk_id = $query[0]['reference_type_id'];
                } else {
                    $valid = false;
                    return 'nofeetypeDrop';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } 
        
        if($data['feetype']=='group'){
            $this->db->select('reference_type_value_id');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_value_id', $data['groupname']);
            $this->db->where('reference_type_id', $fk_id);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                    $fk_id = $query[0]['reference_type_value_id'];
                } else {
                    $valid = false;
                    return 'nogroupname';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            $data_insert = array(
                'name' => $data['name'],
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_institution_id' => $fk_id,
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
                'description' => $data['description'],
                'parent_id' => $id,
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
        }elseif($data['feetype']=='corporate'){
            $this->db->select('reference_type_value_id');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_value_id', $data['companyName']);
            $this->db->where('reference_type_id', $fk_id);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                    $fk_id = $query[0]['reference_type_value_id'];
                } else {
                    $valid = false;
                    return 'nocompanyname';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            $data_insert = array(
                'name' => $data['name'],
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_corporate_company_id' => $fk_id,
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
                'description' => $data['description'],
                'parent_id' => $id,
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
        }elseif($data['feetype']=='retail'){
            $data_insert = array(
                'name' => $data['name'],
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
                'description' => $data['description'],
                'parent_id' => $id,
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
        }
        
        foreach ($data['item'] as $key => $value) {
            $this->db->select('fee_head_id');
            $this->db->from('fee_head');
            $this->db->where('fee_head_id', $value['id']);
            $this->db->where('status', '1');
            $query = $this->db->get();
//            die($this->db->last_query());
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                } else {
                    $valid = false;
                    return 'nofeehead';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        /*Same day update*/
        if($valid)
        {
            $this->db->trans_start();
            
            if($data['branch'] != 0){
                $data_insert["fk_branch_id"] = $data['branch'];
            }
            
            $this->db->insert('fee_structure', $data_insert);
            $last_update_id = $this->db->insert_id();

            $this->db->where('fee_structure_id',$id);
            $this->db->update('fee_structure', array('is_active'=>0));

            foreach ($data['item'] as $key => $value) {
                $data_insert1 = array(
                    'fk_fee_structure_id' => $last_update_id,
                    'fk_fee_head_id' => $value['id'],
                    'due_days' => $value['days'],
                    'amount' => $value['amount']
                );
                $this->db->insert('fee_structure_item', $data_insert1);
            }
            $effective = strtotime($data['effective']);
            $currenttime = strtotime(date('Y-m-d H:i:s'));
            $endDate = Date('Y-m-d H:i:s', $effective);
            $datetime1 = new DateTime(date('Y-m-d H:i:s'));
            $datetime2 = new DateTime($endDate);
            $interval = $datetime1->diff($datetime2);
            $diff = trim(str_replace('days', '', str_replace('-', '', str_replace('+', '', $interval->format('%R%a days')))));
            if ($diff > 0){
                $subDate = "-1 days";
                $effective = strtotime($subDate . $data['effective']);
                $this->db->where('fee_structure_id', $id);
                $this->db->update('fee_structure', array(
                    'end_date' => date('Y-m-d', $effective),
                    'updated_date' => date('Y-m-d H:i:s'),
                    'fk_updated_by' => $_SERVER['HTTP_USER'],
                    'is_active' => 1,
                ));
            } else{
                /*For update to pervious create data*/
                $this->db->where('fee_structure_id', $id);
                $this->db->update('fee_structure', array(
                    'end_date' => date('Y-m-d H:i:s'),
                    'updated_date' => date('Y-m-d H:i:s'),
                    'fk_updated_by' => $_SERVER['HTTP_USER'],
                    'is_active' => 0,
                    'end_date' => date('Y-m-d H:i:s')
                ));
            }
            
            $this->db->trans_complete();
                    
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    
    public function copyFeeStructure($data, $id) {
        $valid = false;
        $final_branches=array();
        $data['partialPayment']=(int)$data['partialPayment'];
        $data['regularFee']=(int)$data['regularFee'];
        $this->db->select('fee_structure_id');
        $this->db->from('fee_structure');
        $this->db->where('fee_structure_id', $id);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        if($data['branch'] != 0){
            if (preg_match("/,/", $data['branch']))
            {
                $branch = explode(",", $data['branch']);
                $final_branches=$branch;
                if (is_array($branch) && count($branch) > 0) {
                    foreach ($branch as $key => $val) {
                        $this->db->select('branch_id');
                        $this->db->from('branch');
                        $this->db->where('branch_id', $val);
                        $query = $this->db->get();
                        if ($query) {
                            $query = $query->result_array();
                            if (count($query) > 0) {
                                $valid = true;
                            } else {
                                $valid = false;
                                return 'nobranch';
                            }
                        } else {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                    }
                }
            }
            else
            {
                $this->db->select('branch_id');
                $this->db->from('branch');
                $this->db->where('branch_id', $data['branch']);
                $query = $this->db->get();
                if ($query) {
                    $query = $query->result_array();
                    if (count($query) > 0) {
                        $valid = true;
                    } else {
                        $valid = false;
                        return 'nobranch';
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
                $final_branches[0]=$data['branch'];
            }
        }
        else{
            $final_branches[0]=0;
        }
        
        /*Course*/
        $this->db->select('course_id');
        $this->db->from('course');
        $this->db->where('course_id', $data['course']);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nocourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /*For Fee Type*/
        $fk_id = '';
        $fk_typeid = '';
        
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value');
        $this->db->where('value', $data['feetype']);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $fk_typeid = $query[0]['reference_type_value_id'];
            } else {
                $valid = false;
                return 'nofeetypeDrop';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        if($data['feetype'] == 'group'){
            $feetype = 'Institution';
            $this->db->select('reference_type_id');
            $this->db->from('reference_type');
            $this->db->where('name', $feetype);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $fk_id = $query[0]['reference_type_id'];
                } else {
                    $valid = false;
                    return 'nofeetypeDrop';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } else if($data['feetype'] == 'corporate'){
            $feetype = 'Company';
            $this->db->select('reference_type_id');
            $this->db->from('reference_type');
            $this->db->where('name', $feetype);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $fk_id = $query[0]['reference_type_id'];
                } else {
                    $valid = false;
                    return 'nofeetypeDrop';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } else if($data['feetype'] == 'retail'){
            $feetype = 'Company';
            $this->db->select('reference_type_id');
            $this->db->from('reference_type');
            $this->db->where('name', $feetype);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $fk_id = $query[0]['reference_type_id'];
                } else {
                    $valid = false;
                    return 'nofeetypeDrop';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        } 
        
        
        if($data['feetype']=='group'){
            $this->db->select('reference_type_value_id');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_value_id', $data['groupname']);
            $this->db->where('reference_type_id', $fk_id);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                    $fk_id = $query[0]['reference_type_value_id'];
                } else {
                    $valid = false;
                    return 'nogroupname';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            $data_insert = array(
                'name' => $data['name'],
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_institution_id' => $fk_id,
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
                'description' => $data['description'],
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
        }elseif($data['feetype']=='corporate'){
            $this->db->select('reference_type_value_id');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_value_id', $data['companyName']);
            $this->db->where('reference_type_id', $fk_id);
            $query = $this->db->get();
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                    $fk_id = $query[0]['reference_type_value_id'];
                } else {
                    $valid = false;
                    return 'nocompanyname';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            $data_insert = array(
                'name' => $data['name'],
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_corporate_company_id' => $fk_id,
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
                'description' => $data['description'],
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
        }elseif($data['feetype']=='retail'){
            $data_insert = array(
                'name' => $data['name'],
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_created_by' => $_SERVER['HTTP_USER'],
                'created_date' => date('Y-m-d H:i:s'),
                'description' => $data['description'],
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
        }
        
        foreach ($data['item'] as $key => $value) {
            $this->db->select('fee_head_id');
            $this->db->from('fee_head');
            $this->db->where('fee_head_id', $value['id']);
            $this->db->where('status', '1');
            $query = $this->db->get();
//            die($this->db->last_query());
            if ($query) {
                $query = $query->result_array();
                if (count($query) > 0) {
                    $valid = true;
                } else {
                    $valid = false;
                    return 'nofeehead';
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        
        if($valid)
        {
            $this->db->trans_start();
            
            $whereCondition = array(
                'fk_course_id' => $data['course'],
                'fk_type_id' => $fk_typeid,
                'fs.is_active' => 1,
                'fk_type_id' => $fk_typeid,
                'effective_from' => date('Y-m-d', strtotime($data['effective'])),
                'fk_training_type' => $data['trainingType'],
                'is_partial_payment_allowed' => $data['partialPayment'],
                'is_regular_fee' => $data['regularFee']
            );
            
            if($data['feetype']=='group'){
                $whereCondition['fk_institution_id'] = $fk_id;
            } else if($data['feetype']=='corporate'){
                $whereCondition['fk_corporate_company_id'] = $fk_id;
            } else if($data['feetype']=='retail'){
//                $whereCondition[''] = '';
                $whereCondition['fk_institution_id'] = NULL;
                $whereCondition['fk_corporate_company_id'] = NULL;
            }
            
            if($data['branch'] != 0){
                $whereCond = ' fs.`fk_branch_id` IN ('. $data['branch'].')';
            }else{
                $whereCond = ' `fk_branch_id` IS NULL ';
            }

            $this->db->select('fs.*,GROUP_CONCAT(distinct `b`.`name`) as branches');
            $this->db->from('fee_structure fs');
            $this->db->join('branch b','b.branch_id=fs.fk_branch_id' ,'left');
            if($whereCond!='') {
                $this->db->where($whereCond);
            }
            $this->db->where($whereCondition);
            //$this->db->group_by('fs.fk_branch_id');
            $this->db->order_by('fee_structure_id', 'DESC');
            $ress = $this->db->get();
            $ress=$ress->result();
            if(count($ress) > 0 && $ress[0]->fee_structure_id!=null){
                if($ress[0]->branches==null){
                    $ress[0]->branches='';
                }
                return 'alreadyPresent-'.$ress[0]->branches;
            } else {
                for ($branchCnt = 0; $branchCnt < count($final_branches); $branchCnt++) {
                    if($final_branches[$branchCnt]==0){

                        $data_insert["fk_branch_id"] = NULL;
                    }else{
                        $data_insert["fk_branch_id"] = $final_branches[$branchCnt];
                    }
                    $this->db->insert('fee_structure', $data_insert);
                    $last_update_id = $this->db->insert_id();
                    foreach ($data['item'] as $key => $value) {
                        $data_insert1 = array(
                            'fk_fee_structure_id' => $last_update_id,
                            'fk_fee_head_id' => $value['id'],
                            'due_days' => $value['days'],
                            'amount' => $value['amount']
                        );
                        $this->db->insert('fee_structure_item', $data_insert1);
                    }
                }
            }
            $this->db->trans_complete();
                    
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    
    public function changeStatus($id) {
        $this->db->select('*');
        $this->db->from('fee_structure');
        $this->db->where('fee_structure_id', $id);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if ($res_all) {

                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );

                $this->db->where('fee_structure_id', $id);
                $res_update = $this->db->update('fee_structure', $data_update);

                if ($this->db->error()['code'] == '0') {
                    if ($res_update) {
                        if ($is_active) {
                            return 'active';
                        } else {
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
}