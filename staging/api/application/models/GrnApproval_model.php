<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-19
 * Time: 06:59 PM
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class GrnApproval_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    public function checkGtnNoExist($param)
    {
        $this->db->select("stockflow_id");
        $this->db->from("stockflow");
        $this->db->where("sequence_number", $param['gtnNo']);
        $this->db->where('type','GTN');
        $this->db->where('requested_mode','branch');
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid Number',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function checkVendorGtnNoExist($vendorId)
    {
        $this->db->select("stockflow_id");
        $this->db->from("stockflow");
        $this->db->where("fk_receiver_id", $vendorId);
        $this->db->where('type','GTN');
        $this->db->where('requested_mode','warehouse');
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid Name',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getVendorGtnProductDetails($vendor)
    {
        $this->db->select('s.sequence_number,s.fk_requested_id,s.requested_mode,p.product_id,p.name,p.code,si.quantity');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','si.fk_stockflow_id= s.stockflow_id','left');
        $this->db->join('product p','si.fk_product_id=p.product_id','left');
        $this->db->where('s.fk_receiver_id',$vendor['vendorId']);
        $this->db->where('s.receiver_mode','vendor');
        $this->db->where('s.requested_mode','warehouse');
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            if(count($list) > 0)
            {
                foreach($list as $val)
                {
                    $list_final ['name'] =  $val->name;
                    $list_final ['request_mode'] = $val->requested_mode;
                    $list_final ['request_id'] = $val->fk_requested_id;
                    $list_final ['sequence_number'] = $val->sequence_number;
                }
                $list_final ['products'] = $list;
                $db_response=array("status"=>true,"message"=>"success","data"=>$list_final);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getGtnProductDetails($gtnNo)
    {
        $this->db->select('s.fk_requested_id,s.requested_mode,p.product_id,p.name,p.code,si.quantity,s.stockflow_id');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','si.fk_stockflow_id= s.stockflow_id','left');
        $this->db->join('product p','si.fk_product_id=p.product_id','left');
        $this->db->where('s.sequence_number',$gtnNo['gtnNo']);
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            if(count($list) > 0)
            {
                foreach($list as $val)
                {
                    $list_final ['name'] =  $val->name;
                    $list_final ['request_mode'] = $val->requested_mode;
                    $list_final ['request_id'] = $val->fk_requested_id;
                    $list_final ['stockflow_id'] = $val->stockflow_id;
                }
                $list_final ['products'] = $list;
                $db_response=array("status"=>true,"message"=>"success","data"=>$list_final);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getGtnNumbers($val)
    {
        $query = $this->db->query("SELECT CONCAT_WS('|',fk_requested_id,requested_mode) as id,sequence_number as text FROM stockflow WHERE sequence_number like '%".$val."%' and type='GTN' and requested_mode='branch' and `status`=0 ");
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    public function getProductList($val)
    {
        $query = $this->db->query("SELECT product_id as id,CONCAT_WS(' | ',code,name) as text FROM product WHERE code like '%".$val."%' or name like '%".$val."%'");
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    public function getVendorList($val)
    {
        $qry = "select inventory_vendor_id id, name as text from inventory_vendor where is_active=0 and (name like '%".$val."%' or `email` like '%".$val."%')";

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function checkGtnExist($gtnNo,$receiverMode)
    {
        $this->db->select('sequence_number');
        $this->db->from('stockflow');
        $this->db->where('type','GTN');
        $this->db->where('sequence_number',$gtnNo);
        if(isset($receiverMode) && $receiverMode=='vendor')
            $this->db->where('receiver_mode',$receiverMode);
        else
            $this->db->where('requested_mode','branch');
        $query = $this->db->get();

        if($query->num_rows()>0)
        {
            $existStatus = true;
        }
        else
        {
            $existStatus = false;
        }
        return $existStatus;
    }
    public function getPreviousRecords()
    {
        $this->db->select('sequence_number');
        $this->db->from('stockflow');
        $this->db->where('type','GRN');
        $this->db->order_by('stockflow_id','desc');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $recordId = $query->row();
            $previousId = $recordId->sequence_number;
        }
        else{
            $previousId = 0;
        }
        return $previousId;
    }
    public function addWarehouseGrn($data)
    {
        $receiverMode = $data['receiverMode'];
        $receiverObjectId = $data['receiverObjectId'];
        $requestMode = $data['requestMode'];
        $requestObjectId = $data['requestObjectId'];
        $createdBy = $data['userID'];
        $invoiceNumber = $data['invoice_number'];
        $invoiceAttachment = $data['invoice_file'];
        $grnRemarks = $data['remarks'];
        $createdDate = date('Y-m-d H:i:s');
        $products = $data['products'];
        $gtnNo =$data['grnNo'];
        $stock_flow_id =$data['stock_flow_id'];
        if(trim($stock_flow_id)==''){
            $stock_flow_id=NULL;
        }
        $previousRecord = $this->getPreviousRecords();
        if($previousRecord == 0)
        {
            $sequenceNumber = 20001;
        }
        else{
            $sequenceNumber = $previousRecord+1;
        }
        $gtnExist = $this->checkGtnExist($gtnNo,$requestMode);
        if($gtnExist === true)
        {
            $this->db->where('sequence_number',$gtnNo);
            $updateGtn = $this->db->update('stockflow',array('type'=>'GTN','status'=>1));
            $grnId = $this->db->insert('stockflow',array('type'=>'GRN','sequence_number'=>$sequenceNumber,'receiver_mode'=>$receiverMode,'fk_receiver_id'=>$receiverObjectId,'requested_mode'=>$requestMode,'fk_requested_id'=>$requestObjectId,'created_by'=>$createdBy,'created_date'=>$createdDate,'invoice_number'=>$invoiceNumber,'invoice_attachment'=>$invoiceAttachment,'remarks'=>$grnRemarks,'fk_gtn_stockflow_id'=>$stock_flow_id));
            if($grnId)
            {
                $lastInsertedId = $this->db->insert_id();
                for($p=0;$p<count($products);$p++)
                {
                    $product = explode('|',$products[$p]);
                    $data = array(
                        'fk_stockflow_id'=>$lastInsertedId,
                        'fk_product_id'=>$product[0],
                        'quantity' => $product[1],
                    );
                    $this->db->insert('stockflow_item',$data);
                }
                $status = true;
                $message = 'Received successfully';
            }
            else{
                $error = $this->db->error();
                $status=FALSE;
                $message=$error['message'];
            }
        }
        else{
            $status =FALSE;
            $message = 'Invalid GTN No';
        }

        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function addWarehouseGrnFromVendor($data)
    {

        $receiverMode = $data['receiverMode'];
        $receiverObjectId = $data['receiverObjectId'];
        $requestMode = $data['requestMode'];
        $requestObjectId = $data['requestObjectId'];
        $invoiceNumber = $data['invoice_number'];
        $invoiceAttachment = $data['invoice_file'];
        $grnRemarks = $data['remarks'];
        $createdBy = $data['userID'];
        $createdDate = date('Y-m-d H:i:s');
        $products = explode(',',$data['products']);
        //need check received stock is less than request stock
        $previousRecord = $this->getPreviousRecords();
        if($previousRecord == 0)
        {
            $sequenceNumber = 20001;
        }
        else{
            $sequenceNumber = $previousRecord+1;
        }
        $grnId = $this->db->insert('stockflow',array('sequence_number'=>$sequenceNumber,'type'=>'GRN','receiver_mode'=>$receiverMode,'fk_receiver_id'=>$receiverObjectId,'requested_mode'=>$requestMode,'fk_requested_id'=>$requestObjectId,'created_by'=>$createdBy,'created_date'=>$createdDate,'invoice_number'=>$invoiceNumber,'invoice_attachment'=>$invoiceAttachment,'remarks'=>$grnRemarks));
        if($grnId)
        {
            $lastInsertedId = $this->db->insert_id();
            for($p=0;$p<count($products);$p++)
            {
                $product = explode('||',$products[$p]);
                $data = array(
                    'fk_stockflow_id'=>$lastInsertedId,
                    'fk_product_id'=>$product[0],
                    'quantity' => $product[1],
                );
                $this->db->insert('stockflow_item',$data);
                //check products already exist for branch or not
                //$productId = $this->getProductExistForBranch($product[0],$requestObjectId);
            }
            $status = true;
            $message = 'Stock received successfully';
        }
        else{
            $error = $this->db->error();
            $status=FALSE;
            $message=$error['message'];
        }
        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function warehouseGrnApprovalDataTables()
    {
        $table = 'stockflow s';
        $primaryKey = 's`.`stockflow_id';
        $columns = array(
            array( 'db' => 'CONCAT_WS("","'.INVENTORY_WAREHOUSE_PREFIX.'",`s`.`sequence_number`) as new_sequence_number', 'dt' => 'new_sequence_number',          'field' => 'new_sequence_number' ),
            array( 'db' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA"))))) as receiveName',              'dt' => 'receiveName',           'field' => 'receiveName'),
            array( 'db' => '`s`.`created_date` as receivedOn',                     'dt' => 'receivedOn',          'field' => 'receivedOn'),
            array( 'db' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA"))))) as requestedName',              'dt' => 'requestedName',           'field' => 'requestedName'),
            array( 'db' => '`s`.`created_date` as sentOn',                     'dt' => 'sentOn',          'field' => 'sentOn' ),
            array( 'db' => 'sum(si.quantity) as total',                 'dt' => 'total',                    'field' => 'total' ),
            array( 'db' => 's.invoice_number',                 'dt' => 'invoice_number',                    'field' => 'invoice_number' ),
            array( 'db' => '`s`.`approval_status`',                     'dt' => 'approval_status',          'field' => 'approval_status' ),
            array( 'db' => 's.invoice_attachment',                 'dt' => 'invoice_attachment',                    'field' => 'invoice_attachment' ),
            array( 'db' => 'sum(ifnull(si.returned_quantity,0)) as returnTotal',  'dt' => 'returnTotal',              'field' => 'returnTotal'),
            array( 'db' => '`s`.`stockflow_id`',                     'dt' => 'stockflow_id',          'field' => 'stockflow_id' ),
            array( 'db' => '`s`.`sequence_number`',                     'dt' => 'sequence_number',          'field' => 'sequence_number' )

        );
        $globalFilterColumns = array(
            array( 'db' => 'CONCAT_WS("","'.INVENTORY_WAREHOUSE_PREFIX.'",`s`.`sequence_number`)', 'dt' => 'new_sequence_number',          'field' => 'new_sequence_number' ),
            array( 'db' => '`s`.`sequence_number`',            'dt' => 'sequence_number',         'field' => 'sequence_number' ),
            array( 'db' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))',              'dt' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))',           'field' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))'),
            array( 'db' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))',              'dt' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))',           'field' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))'),
            array( 'db' => '`s`.`created_date`',                     'dt' => 'created_date',          'field' => 'created_date' ),
            array( 'db' => 's.invoice_number',                 'dt' => 'invoice_number',                    'field' => 'invoice_number' ),
            array( 'db' => 's.approval_status',                 'dt' => 'approval_status',                    'field' => 'approval_status' )
        );
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        $joinQuery = "from stockflow s JOIN stockflow_item si ON s.stockflow_id=si.fk_stockflow_id";
        $extraWhere = "s.type='GRN' and s.receiver_mode='warehouse' and s.requested_mode='vendor' and s.approval_status = 'pending'";
        $groupBy = "s.stockflow_id";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery,$extraWhere,$groupBy));
        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['receivedOn']=date('j M, Y',strtotime($responseData['data'][$i]['receivedOn']));
            $responseData['data'][$i]['sentOn']=date('j M, Y',strtotime($responseData['data'][$i]['sentOn']));
            $responseData['data'][$i]['invoice_attachment']=urlencode(basename('./'.$responseData['data'][$i]['invoice_attachment']));
            $responseData['data'][$i]['approval_status']=ucfirst($responseData['data'][$i]['approval_status']);
        }
        return $responseData;
    }
    function getWareHouseStockProductWise($type,$productId=0){
        if($type=='outward'){
            $wareHouse_query="SELECT s.created_date,si.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(quantity) as quantity,IFNULL(s.remarks,'') as remarks,if(s.receiver_mode='branch',(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_receiver_id),'NA'))))) as receiveName,
                          if(s.requested_mode='branch',(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_requested_id),'NA'))))) as requestedName,if(s.status=0,'In Transit','Delivered') as deliverStatus FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=$productId group by s.stockflow_id";
        }
        if($type=='inward'){
            $wareHouse_query="SELECT s.created_date,si.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(quantity-returned_quantity) as quantity,IFNULL(s.remarks,'') as remarks,if(s.receiver_mode='branch',(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_receiver_id),'NA'))))) as receiveName,
if(s.requested_mode='branch',(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_requested_id),'NA'))))) as requestedName FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=$productId group by s.stockflow_id";
        }
        $res=$this->db->query($wareHouse_query);

        if($res){
            if($res->num_rows()>0){
                $rows=$res->result();
                foreach($rows as $k=>$v){
                    $v->created_date=date('d M, Y',strtotime($v->created_date));
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;
    }

    public function getWareHouseGrnProducts($stockFlowId)
    {
        $rows=array();
        $rows['products']=array();
        $rows['history']=array();
        $rows['details']=array();



        $this->db->select('CONCAT_WS("","WRN",`s`.`sequence_number`) as sequence,if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA"))))) as receiveName,if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA"))))) as requestedName,s.invoice_number,s.invoice_attachment,`s`.`created_date` as sentOn,`s`.`created_date` as receivedOn,`s`.`approval_status`,sum(si.quantity) as total,sum(ifnull(si.returned_quantity,0)) as returnTotal');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','s.stockflow_id=si.fk_stockflow_id');
        $this->db->where('s.stockflow_id',$stockFlowId);
        $query = $this->db->get();
        if($query){
            if($query->num_rows()>0){
                $rows['details']=$query->result();
                foreach($rows['details'] as $key=>$val){
                    $val->receivedOn=date('d M,Y h:i:s A',strtotime($val->receivedOn));
                    $val->sentOn=date('d M,Y h:i:s A',strtotime($val->sentOn));
                    $val->invoice_attachment=urlencode(basename('./'.$val->invoice_attachment));
                    $val->approval_status=ucfirst($val->approval_status);

                }
                $rows['details']=$rows['details'][0];
            }
            else{

            }
        }
        else{

        }

        $this->db->select('si.quantity,p.`name` as productName');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','s.stockflow_id=si.fk_stockflow_id','left');
        $this->db->join('product p','p.product_id=si.fk_product_id');
        $this->db->where('s.stockflow_id',$stockFlowId);
        $query = $this->db->get();

        if($query){
            if($query->num_rows()>0){
                $rows['products']=$query->result();

            }
            else{

            }
        }
        else{

        }

        $this->db->select('g.created_on,g.approval_status,g.remarks,e.name,rtv.value');
        $this->db->from('grn_approval_history g');
        $this->db->join('employee e','e.user_id=g.created_by');
        $this->db->join('user_xref_role uxr','e.user_id=uxr.fk_user_id');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=uxr.fk_role_id');
        $this->db->where('g.fk_stock_flow_id',$stockFlowId);
        $this->db->order_by('g.grn_approval_history_id','desc');
        $query = $this->db->get();
        if($query){
            if($query->num_rows()>0){
                $rows['history']=$query->result();
                foreach($rows['history'] as $key=>$val){
                    $val->created_on=date('d M,Y h:i:s A',strtotime($val->created_on));
                    $val->approval_status=ucfirst($val->approval_status);
                }

            }
            else{

            }
        }
        else{

        }

        $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
        return $db_response;
    }
    public function warehouseGrnApprovalListHistory($param)
    {
        $this->db->select('g.created_on,g.approval_status,g.remarks,e.name,rtv.value');
        $this->db->from('grn_approval_history g');
        $this->db->join('employee e','e.user_id=g.created_by');
        $this->db->join('user_xref_role uxr','e.user_id=uxr.fk_user_id');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=uxr.fk_role_id');
        $this->db->where('g.fk_stock_flow_id',$param['stockflowId']);
        $this->db->order_by('g.grn_approval_history_id','desc');
        $query = $this->db->get();
        if($query){
            if($query->num_rows()>0){
                $rows=$query->result();
                foreach($rows as $key=>$val){
                    $val->created_on=date('d M,Y h:i:s A',strtotime($val->created_on));
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;
    }
    function saveChangeStatus($params){
        $data_update=array();
        $current_date=date('Y-m-d H:i:s');
        $user=$_SERVER['HTTP_USER'];
        $data_update['approval_status']=$params['status'];
        $data_update['approval_by']=$user;
        $data_update['approval_date']=$current_date;
        $data_update['approval_remarks']=$params['comments'];

        $this->db->where('stockflow_id',$params['stockFlowId']);
        $res_update=$this->db->update('stockflow',$data_update);
        if($res_update){
            $data_ins_hist=array();
            $data_ins_hist['fk_stock_flow_id']=$params['stockFlowId'];
            $data_ins_hist['approval_status']=$params['status'];
            $data_ins_hist['remarks']=$params['comments'];
            $data_ins_hist['created_by']=$user;
            $data_ins_hist['created_on']=$current_date;
            $res_ins=$this->db->insert('grn_approval_history',$data_ins_hist);
            if($res_ins){
                $db_response=array('status'=>true,'message'=>'success','data'=>'');
            }
            else{
                $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>'');
            }

        }
        else{
            $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>'');
        }
        return $db_response;
    }
}

