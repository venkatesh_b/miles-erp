<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 21/3/16
 * Time: 9:49 PM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LeadFollowUp_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    function updateinfo($data,$lead_id){
        $branch_lead['fk_course_id']=$data['fk_course_id'];

        $this->db->select('alternate_mobile');
        $this->db->from('lead');
        $this->db->where('phone',$data['alternate_mobile']);
        $this->db->where('lead_id!=',$lead_id);
        $query = $this->db->get();
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else
        {
            if ($query->num_rows() > 0)
            {
                $db_response=array("status"=>false,"message"=>'Contact number already exist',"data"=>array('name'=>'Contact number already exist'));
            }else{

                $this->db->where('lead_id', $lead_id);
                unset($data['fk_course_id']);
                $update= $this->db->update('lead',$data);
                $res_update=array();
                if($update){

                    $this->db->where('lead_id', $lead_id);
                    $res_update = $this->db->update('branch_xref_lead',$branch_lead);
                }else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }


                if($res_update){
                    $db_response=array("status"=>true,"message"=>"updated successfully","data"=>array());
                }
                else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }

        }

        return $db_response;

    }
    function getSingleLeadBxrefId($branchxcerLeadId){

        $this->db->select("bl.*,l.lead_id,l.city_id, l.name as lead_name,l.email,l.phone,l.alternate_mobile, l.phone,rtvqual.value as qualification,
         c.name as course,ls.lead_stage, rtvcity.value as city,bl.created_on,rtvDesignation.value as designation, rtvcmp.value as company,
         rtvinit.value as institution");
        $this->db->from("branch_xref_lead bl");
        $this->db->join('lead l', 'l.lead_id = bl.lead_id', 'left');
        $this->db->join('lead_stage ls','ls.lead_stage_id=bl.status','left');
        $this->db->join('course c','c.course_id=bl.fk_course_id','left');
        $this->db->join('reference_type_value rtvcity', 'rtvcity.reference_type_value_id=l.city_id', 'left');
        $this->db->join('reference_type_value rtvcmp', 'rtvcmp.reference_type_value_id=l.fk_company_id', 'left');
        $this->db->join('reference_type_value rtvqual', 'rtvqual.reference_type_value_id=l.fk_qualification_id', 'left');
        $this->db->join('reference_type_value rtvDesignation', 'rtvDesignation.reference_type_value_id=l.fk_designation_id', 'left');
        $this->db->join('reference_type_value rtvinit', 'rtvinit.reference_type_value_id=l.fk_institution_id', 'left');
        $this->db->where("bl.branch_xref_lead_id",$branchxcerLeadId);
        $query = $this->db->get();
         //echo $this->db->last_query();exit;
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                //exit;
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    function getSingleLeadById($lead_id){

        $this->db->select("bl.branch_xref_lead_id,l.gender,l.lead_id,l.name,e.name as last_modified,l.alternate_mobile,l.phone,l.status,bl.next_followup_date,bl.lead_number,bl.created_on,
         ls.lead_stage as leadStage,l.email,c.course_id,rtvqual.reference_type_value_id as qual_id,rtvqual.value as qualification,c.name as course,rtvcity.value as city,rtvDesignation.reference_type_value_id as desi_id,rtvDesignation.value as designation,rtvcmp.reference_type_value_id as companyID,rtvcmp.value as company,rtvinit.value as institution");
        $this->db->from("lead l");
        $this->db->join('branch_xref_lead bl', 'bl.lead_id=l.lead_id', 'left');
        $this->db->join('user u','u.user_id=bl.updated_by','left');
        $this->db->join('employee e','e.user_id=u.user_id','left');
        $this->db->join("lead_stage ls","ls.lead_stage_id=bl.status");
        $this->db->join('course c', 'c.course_id=bl.fk_course_id', 'left');
        $this->db->join('reference_type_value rtvcity', 'rtvcity.reference_type_value_id=l.city_id', 'left');
        $this->db->join('reference_type_value rtvcmp', 'rtvcmp.reference_type_value_id=l.fk_company_id', 'left');
        $this->db->join('reference_type_value rtvqual', 'rtvqual.reference_type_value_id=l.fk_qualification_id', 'left');
        $this->db->join('reference_type_value rtvDesignation', 'rtvDesignation.reference_type_value_id=l.fk_designation_id', 'left');
        $this->db->join('reference_type_value rtvinit', 'rtvinit.reference_type_value_id=l.fk_institution_id', 'left');
        $this->db->where("l.lead_id",$lead_id);
        $query = $this->db->get();

        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                //exit;
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;


    }
    function getLeadEducationalInfo($lead_id){
        $this->db->select("lead_educational_info_id,course,university,year_of_completion,percentage,comments");
        $this->db->from("lead_educational_info");
        $this->db->where("lead_id",$lead_id);
        $this->db->order_by("year_of_completion","desc");
        $query = $this->db->get();
        $db_response=array();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        /*for($i=0;$i<count($db_response['data']);$i++)
        {
            $db_response['data'][$i]->lead_educational_info_id=encode($db_response['data'][$i]->lead_educational_info_id);
        }*/

        return $db_response;

    }
    function getLeadHistory($lead_id){

        $sql="SELECT DISTINCT(z.user_id),z.email, z.name, z.phone, z.image,z.description, z.Designation FROM ((SELECT e.email, e.name, e.phone, e.image,ulh.description, rtv.value as Designation,ulh.created_on,ulh.user_id
                FROM user_xref_lead_history ulh
                LEFT JOIN user u ON u.user_id = ulh.user_id
                LEFT JOIN employee e ON e.user_id=u.user_id
                LEFT JOIN user_xref_role ur ON ur.fk_user_id=u.user_id
                LEFT JOIN reference_type_value rtv ON rtv.reference_type_value_id = ur.fk_role_id
                WHERE ulh.lead_id = '".$lead_id."' )
                union all
                (SELECT e.email, e.name, e.phone, e.image,lulh.description, rtv.value as Designation,lulh.created_on,lulh.fk_user_id as user_id
                FROM lead_user_xref_lead_history lulh
                LEFT JOIN user u ON u.user_id = lulh.fk_user_id
                LEFT JOIN employee e ON e.user_id=u.user_id
                LEFT JOIN user_xref_role ur ON ur.fk_user_id=u.user_id
                LEFT JOIN reference_type_value rtv ON rtv.reference_type_value_id = ur.fk_role_id
                WHERE lulh.fk_lead_id = '".$lead_id."')) z group by z.user_id order by z.created_on DESC";

        $query = $this->db->query($sql);
        $query->result_array();

        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function AddDesignation($data,$type){
        $this->db->select('reference_type_id');
        $this->db->from('reference_type');

        $this->db->where('name',trim($type));

        $query = $this->db->get();
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }else{
            if ($query->num_rows() > 0)
            {
                $results=$query->result();
                $data['reference_type_id']= $results[0]->reference_type_id;
                $res_insert=$this->db->insert('reference_type_value', $data);
                if($res_insert){
                    $db_response=array("status"=>true,"message"=>"Added successfully","data"=>array());
                }
                else{
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }

            }

        }

        return $db_response;

    }
    function AddEducationalInfoDetails($data){
        $db_response="";
        if(isset($data) && !empty($data)) {
            $res_insert = $this->db->insert('lead_educational_info', $data);
            if ($res_insert) {
                $db_response = array("status" => true, "message" => "Added successfully", "data" => array());
            } else {
                $error = $this->db->error();
                $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
            }
        }


        return $db_response;

    }
    function UpdateEducationalInfoDetails($data,$id){
        $db_response="";
        if(isset($data) && !empty($data)) {
            $this->db->where('lead_educational_info_id', $id);
            $res_insert = $this->db->update('lead_educational_info', $data);
            if ($res_insert) {
                $db_response = array("status" => true, "message" => "Updated successfully", "data" => array());
            } else {
                $error = $this->db->error();
                $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
            }
        }


        return $db_response;

    }
    function getEducationDetailsById($edu_id){
        $this->db->select("course,university,year_of_completion,percentage,comments");
        $this->db->from("lead_educational_info");
        $this->db->where("lead_educational_info_id",$edu_id);
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    function deleteEducationInfoById($infoId){

        $this->db->where('lead_educational_info_id', $infoId);
        $status=$this->db->delete('lead_educational_info');
        if($status){
            $db_response=array("status"=>true,"message"=>"Deleted successfully","data"=>array());
        }else{
            $db_response=array("status"=>false,"message"=>"Problem in deleting","data"=>array());
        }
        return $db_response;
    }
    function AddOtherInfoDetails($data,$lead_id){
        $db_response="";
        if(isset($data) && !empty($data)) {
            if(isset($lead_id)) {
                $this->db->where("lead_id", $lead_id);
                $res_insert = $this->db->update('branch_xref_lead', $data);
            }

            if ($res_insert) {
                $db_response = array("status" => true, "message" => "Updated successfully", "data" => array());
            } else {
                $error = $this->db->error();
                $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
            }
        }


        return $db_response;

    }
    function getOtherInfoDetailsById($lead_id){
        $this->db->select("Significance_of_CPA,Willingness_to_pursue_the_CPA,Response_on_Miles_CPA,Value_addition,CMA_No");
        $this->db->from("branch_xref_lead");
        $this->db->where("lead_id",$lead_id);
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    public function getTimelinesById($lead_id){
        $this->db->select("e.name,t.description,t.interaction_file,t.interaction_date,t.type,t.created_on,rtv.value as Designation");
        $this->db->from("timeline t");
        $this->db->join('lead l', 'l.lead_id = t.lead_id', 'left');
        $this->db->join('branch_xref_lead bl', 'bl.branch_xref_lead_id=t.branch_xref_lead_id', 'left');
        $this->db->join('user_xref_lead_history ulh','ulh.user_xref_lead_history_id=bl.fk_user_xref_lead_history_id','left');
        $this->db->join('employee e','e.user_id=t.created_by','left');
        $this->db->join('user_xref_role ur','ur.fk_user_id=t.created_by','left');
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id = ur.fk_role_id','left');
        $this->db->where("t.lead_id",$lead_id);
        $this->db->order_by("t.created_on","DESC");

        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Lead ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;


    }
    function getBranchVisitorById($branchXrefId){
        $this->db->select("bv.second_level_counsellor_comments,e.name,bv.created_date");
        $this->db->from("branch_visitor bv");
        $this->db->join("employee e","e.user_id=bv.created_by","left");
        if(isset($branchXrefId) && !empty($branchXrefId)) {
            $this->db->where("bv.fk_branch_xref_lead_id",$branchXrefId);
        }
        $this->db->where("is_counsellor_visited",1);
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    /**
     * for setting counter according to L) conditions
     * Created by - Abhilash
     * @param type $leadId
     * @param type $noOfTimes
     * @return string
     */
    function getDNDStatus($leadId,$noOfTimes){
        $this->db->select('*');
        $this->db->from('branch_xref_lead');
        $this->db->where("branch_xref_lead_id", $leadId);
        
        $result = $this->db->get();
        if($result){
            $result = $result->result();
            if(count($result) > 0){
                $updatedTime = strtotime((new DateTime($result[0]->updated_on))->format('Y-m-d'));
                $currenttime = strtotime(date('Y-m-d'));
                if($updatedTime == $currenttime){
                    if($result[0]->dnd_count == ($noOfTimes-1)){
                        //make it as L0
                        return '1';
                    }else{
                        $data = array('dnd_count' => (($result[0]->dnd_count)+1));
                        $this->db->where('branch_xref_lead_id', $leadId);
                        $this->db->update('branch_xref_lead', $data);
                        return $result[0]->status;
                    }
                }else{
                    //make the dnd status to 1 for the next day
                    $data = array('dnd_count' => 1);
                    $this->db->where('branch_xref_lead_id', $leadId);
                    $this->db->update('branch_xref_lead', $data);
                    return $result[0]->status;
                }
            }else{
                return 'nodata';
            }
        }else{
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    function resetDNDCounter($leadId){
        $this->db->select('*');
        $this->db->from('branch_xref_lead');
        $this->db->where("branch_xref_lead_id", $leadId);
        
        $result = $this->db->get();
        if($result){
            $result = $result->result();
            if(count($result) > 0){
                $updatedTime = strtotime((new DateTime($result[0]->updated_on))->format('Y-m-d'));
                $currenttime = strtotime(date('Y-m-d'));
                if($updatedTime == $currenttime){
                    //
                }else{
                    //make the dnd status to 1 for the next day
                    $data = array('dnd_count' => 0);
                    $this->db->where('branch_xref_lead_id', $leadId);
                    $this->db->update('branch_xref_lead', $data);
                    return $result[0]->status;
                }
            }else{
                return 'nodata';
            }
        }else{
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    function SaveCallStatusInfo($data){
        $db_response=$delete_histroy="";
        $insert_history=$data;
        if(isset($data) && !empty($data))
        {
            if(isset($data['branch_xref_lead_id'])) {
                
                $leadId = $data['branch_xref_lead_id'];
                unset($data['description']);
                unset($data['fk_lead_id']);
                unset($data['branch_xref_lead_id']);
                $data["updated_on"]=date("Y-m-d H:i:s");
                $data["updated_by"]=$data['fk_user_id'];
                unset($data['fk_user_id']);
                /*check the DND*/
                if(!is_null($data['dndConfigDays'])){
                    $newStage = $this->getDNDStatus($leadId, $data['dndConfigDays']);
                    $postStatus = $data['status'];
                    unset($data['dndConfigDays']);
                    unset($insert_history['dndConfigDays']);
                    unset($data['status']);
                    if(is_array($newStage) && $newStage['error'] == 'dberror'){
                        return $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
                    } else if($newStage == 'nodata'){
                        $newStage = $postStatus;
                    } else if($newStage == 1){
                        $data['status'] = $newStage;
                        $data['dnd_count'] = 0;
                    } else{
                        $newStage = $postStatus;
                    }
                }else{
                    $this->resetDNDCounter($leadId);
                    unset($data['dndConfigDays']);
                    unset($insert_history['dndConfigDays']);
                }
                $this->db->where("branch_xref_lead_id", $leadId);
                $res_insert = $this->db->update('branch_xref_lead', $data);
                if($res_insert){
                    $call_schedule_head_id=$this->getHeadId($insert_history['branch_xref_lead_id']);
                    $insert_history['fk_lead_call_schedule_head_id']=$call_schedule_head_id;
                    if(isset($insert_history['fk_lead_call_schedule_head_id']) && !empty($insert_history['fk_lead_call_schedule_head_id'])){

                        $delete_histroy=$this->db->delete("lead_user_xref_lead_due",array("fk_branch_xref_lead_id"=>$insert_history['branch_xref_lead_id']));
                        if(!$delete_histroy){
                            $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
                        }
                    }
                    $insert_history['fk_branch_xref_lead_id']=$insert_history['branch_xref_lead_id'];
                    if(isset($newStage)){
                        $insert_history['lead_status']=$newStage;
                    }else{
                        $insert_history['lead_status']=$insert_history['status'];
                    }
                    
                    unset($insert_history['status']);
                    unset($insert_history['branch_xref_lead_id']);
                    unset($insert_history['next_followup_date']);
                    unset($insert_history['expected_visit_date']);
                    $insert_history['created_on']=date("Y-m-d H:i:s");

                    if(isset($insert_history['fk_lead_call_schedule_head_id']) && !empty($insert_history['fk_lead_call_schedule_head_id'])) {
                        $this->db->insert('lead_user_xref_lead_history', $insert_history);
                    }
                    $insert_history['lead_id']= $insert_history['fk_lead_id'];
                    $insert_history['created_by']= $insert_history['fk_user_id'];
                    $insert_history['branch_xref_lead_id']= $insert_history['fk_branch_xref_lead_id'];
                    $insert_history['type']="Lead status";
                    unset($insert_history['fk_lead_call_schedule_head_id']);
                    unset($insert_history['lead_status']); unset($insert_history['fk_branch_xref_lead_id']);
                    unset($insert_history['fk_lead_id']);
                    unset($insert_history['fk_user_id']);
                    unset($insert_history['expected_visit_date']);
                    $this->db->insert('timeline', $insert_history);
                }else{
                    $error = $this->db->error();
                    $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
                }
            }
            if ($res_insert) {
                $db_response = array("status" => true, "message" => "Updated successfully", "data" => array());
            } else {
                $error = $this->db->error();
                $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
            }
        }
        return $db_response;

    }
    function getHeadId($id){
            $this->db->select("fk_lead_call_schedule_head_id");
            $this->db->from("lead_user_xref_lead_due");
            $this->db->where("fk_lead_stage_id !=20");
            $this->db->where("fk_branch_xref_lead_id",$id);
            $query = $this->db->get();
            if($query) {
                foreach ($query->result() as $k) {
                    return $k->fk_lead_call_schedule_head_id;
                }
                return false;
            }
            else{
                return false;
            }


    }
    function getLeadStagesId($lead_stage){

        $this->db->select("lead_stage_id");
        $this->db->from("lead_stage");
        $this->db->where("lead_stage",$lead_stage);
        $query = $this->db->get();
        foreach($query->result()  as $k){
            return   $k->lead_stage_id;
        }
    }
    function getLeadStagesName($lead_stage){
        $this->db->select("lead_stage_id,lead_stage");
        $this->db->from("lead_stage");
        $this->db->where("lead_stage_id",$lead_stage);
        $query = $this->db->get();
        return $query->result();
    }
    function saveTimelineDetails($data){

        $db_response="";
        if(isset($data) && !empty($data)) {
            unset($data['attachment']);
            $res_insert = $this->db->insert('timeline', $data);
            if ($res_insert) {
                $db_response = array("status" => true, "message" => "Added successfully", "data" => array());
            } else {
                $error = $this->db->error();
                $db_response = array("status" => false, "message" => $error['message'], "data" => array("message" => $error['message']));
            }
        }


        return $db_response;

    }
    public function getNewLeadsCount($status=""){
        $this->db->select("count(branch_xref_lead_id) as NewLeads");
        $this->db->from("branch_xref_lead bl");
        $this->db->join("lead_stage ls","ls.lead_stage_id=bl.status","left");
        if(isset($status) && !empty($status)) {
            $this->db->where("ls.lead_stage",$status);
        }
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'no data',"data"=>0);
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    function TransferdLeadsCount(){
        $this->db->select("count(branch_xref_lead_id) as Transfered");
        $this->db->from("branch_xref_lead");
        $this->db->where("transferred_to is not null and transferred_from is not null");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
            }
            else{
                $db_response=array("status"=>false,"message"=>'no data',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;


    }
    public function getLeadsList($data){

        $table = 'lead_user_xref_lead_due';
        // Table's primary key
        $primaryKey = 'lead_user_xref_lead_due_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'bl.lead_number',                  'dt' => 'lead_number',          'field' => 'lead_number' ),
            array( 'db' => 'c.name as course_name',           'dt' => 'course_name',          'field' => 'course_name' ),
            array( 'db' => 'ls.lead_stage as level',          'dt' => 'level',                'field' => 'level' ),
            array( 'db' => 'l.name as lead_name',             'dt' => 'lead_name',            'field' => 'lead_name' ),
            array( 'db' => '`bl`.`created_on`',               'dt' => 'created_on',           'field' => 'created_on', 'formatter' => function( $d, $row ) {
                    return date( 'd M,Y', strtotime($d));}),
            array( 'db' => 'l.email',                         'dt' => 'email',                'field' => 'email' ),
            array( 'db' => 'l.phone',                         'dt' => 'phone',                'field' => 'phone' ),
            array( 'db' => 'rtvcity.value as city',           'dt' => 'city',                 'field' => 'city' ),
            array( 'db' => 'rtvcmp.value as company',         'dt' => 'company',              'field' => 'company' ),
            array( 'db' => 'rtvqual.value as qualification',  'dt' => 'qualification',        'field' => 'qualification' ),
            array( 'db' => 'rtvinit.value as institution',    'dt' => 'institution',          'field' => 'institution' ),
            array( 'db' => 'bl.lead_id',                      'dt' => 'lead_id',              'field' => 'lead_id' ),
            array( 'db' => 'bl.branch_xref_lead_id',          'dt' => 'branch_xref_lead_id',  'field' => 'branch_xref_lead_id' ),
            array( 'db' => 'c.course_id',                     'dt' => 'course_id',            'field' => 'course_id' ),
            array( 'db' => 'if(l.gender=1,1,0) as gender',          'dt' => 'gender',  'field' => 'gender' )
            );
        $globalFilterColumns = array(
            array( 'db' => 'l.name ',       'dt' => 'name',       'field' => 'name' ),
            array( 'db' => 'c.name',        'dt' => 'name',       'field' => 'name' ),
            array( 'db' => 'bl.lead_number',                  'dt' => 'lead_number',          'field' => 'lead_number' ),
            array( 'db' => 'ls.lead_stage', 'dt' => 'lead_stage', 'field' => 'lead_stage'),
            array( 'db' => 'l.email',       'dt' => 'email',      'field' => 'email'),
            array( 'db' => 'l.phone',       'dt' => 'phone',      'field' => 'phone'),
            array( 'db' => 'rtvqual.value',  'dt' => 'rtvqual.value',        'field' => 'qualification' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "from lead_user_xref_lead_due lul
                       left join branch_xref_lead bl on bl.branch_xref_lead_id = lul.fk_branch_xref_lead_id
                       left join lead l on l.lead_id = bl.lead_id
                       left join lead_stage ls on  ls.lead_stage_id=bl.status
                       left join course c on c.course_id=bl.fk_course_id
                       left join reference_type_value rtvcity on rtvcity.reference_type_value_id=l.city_id
                       left join reference_type_value rtvcmp on rtvcmp.reference_type_value_id=l.fk_company_id
                       left join reference_type_value rtvqual on rtvqual.reference_type_value_id=l.fk_qualification_id
                       left join reference_type_value rtvinit on rtvinit.reference_type_value_id=l.fk_institution_id
                       left join reference_type_value rtvtype on rtvtype.reference_type_value_id=l.fk_contact_type_id";
        $extraWhere="";
        if(isset($data['branchId']) && !empty($data['branchId']))
        {
            if(isset($data['type']) && !empty($data['type']))
            {
                $extraWhere.= "bl.is_active=1 AND ls.lead_stage!='M7' and bl.branch_id=" . $data['branchId'] . " and rtvtype.value='" . $data['type'] . "'";
            }
            else
            {
                $extraWhere.= "bl.is_active=1 AND ls.lead_stage!='M7' and bl.branch_id=" . $data['branchId'] . "";
            }
        }
        else
        {
            $data['branchId']=0;
            $extraWhere.= "bl.is_active=1 AND ls.lead_stage!='M7' and bl.branch_id=".$data['branchId']."";
        }
        if(isset($data['userID']) && !empty($data['userID']))
        {

            $extraWhere.=" and lul.fk_user_id=".$data['userID']."";
            //$extraWhere.=" and lul.fk_user_id=9";
        }
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
            $responseData['data'][$i]['branch_xref_lead_id_encode']=encode($responseData['data'][$i]['branch_xref_lead_id']);
            //$responseData['data'][$i]['second_level_counsellor_updated_date']=(new DateTime($responseData['data'][$i]['second_level_counsellor_updated_date']))->format('d M,Y');
            //$responseData['data'][$i]['branch_visited_date']=(new DateTime($responseData['data'][$i]['branch_visited_date']))->format('d M,Y');
            $responseData['data'][$i]['comments']=array();
            $getComments="select bv.branch_visited_date,bv.second_level_counsellor_updated_date as counsellor_visited_date,bv.branch_visit_comments,bv.second_level_counsellor_comments,e1.name as counseollor_name,e.name as second_counsellor_name from branch_visitor bv,employee e1,employee e where bv.fk_branch_xref_lead_id =".$responseData['data'][$i]['branch_xref_lead_id']." AND is_counsellor_visited=1 AND status=1 AND bv.created_by=e1.user_id AND bv.second_level_counsellor_id=e.user_id order by bv.branch_visitor_id DESC";
            $res_comment=$this->db->query($getComments);
            if($res_comment){
                if($res_comment->num_rows()>0){
                    $row_comment=$res_comment->result();
                    foreach($row_comment as $k_comment=>$v_comment){
                        $responseData['data'][$i]['comments'][]=array('counsellor_visited_date'=>(new DateTime($v_comment->counsellor_visited_date))->format('d M,Y'),'branch_visited_date'=>(new DateTime($v_comment->branch_visited_date))->format('d M,Y'),'second_level_comments'=>$v_comment->second_level_counsellor_comments,'branch_visit_comments'=>$v_comment->branch_visit_comments,'counseollor_name'=>$v_comment->counseollor_name,'second_counseollor_name'=>$v_comment->second_counsellor_name);
                    }
                }
            }
        }
        return $responseData;


    }

    /* Added by s.raju */
    function getDNDCountConstant(){
        $this->db->select('value');
        $this->db->from('app_configuration_setting');
        $this->db->where('key','DND CONFIG');
        $res=$this->db->get();
        if($res){
            if($res->num_rows()>0){
                $row=$res->result();
                return $row[0]->value;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }
    function ChangeCallStatusInfo($data){
       //print_r($data);

        /*Array
        (
        [branchxcerLeadId] => 34
        [comments] => asdasd
        [lead_stage] => M3+
        [lead_id] => 44
        [dateType] => Expected Visited Date
        [UserID] => 1
        [currentStage] => M3
        [commonDate] => 2016-05-22 10:04:03
        )*/
        $status=true;$message='success';$data_response=[];
        $maxCount=$this->getDNDCountConstant();
        if($maxCount===false){
            $status=false;$message='Fail! Please try again';$data_response=[];
        }
        else {
            $currentLeadStage = $data['currentStage'];
            $this->db->select('logic_item_name,logic_item_count');
            $this->db->from('branch_xref_lead');
            $this->db->where('branch_xref_lead_id', $data['branchxcerLeadId']);
            $res_leadDetails = $this->db->get();
            if ($res_leadDetails) {

                if ($res_leadDetails->num_rows() > 0) {
                    $row_leadDetails = $res_leadDetails->result();
                    $leadDetails = $row_leadDetails[0];
                    $logic_item_name = $leadDetails->logic_item_name;
                    $logic_item_count = $leadDetails->logic_item_count;
                    $leadStage = $data['lead_stage'];
                    $leadStagePrefix = substr($leadStage, 0, 2);
                    if ($leadStagePrefix != '' && $leadStagePrefix == 'L_') {
                        $statusExplode = explode('_', $leadStage);
                        if ($leadStage == $logic_item_name) {
                            $logic_item_name = $logic_item_name;
                            $logic_item_count = $logic_item_count + 1;
                            $nextLeadStage = $currentLeadStage;
                        } else {
                            $logic_item_name = $leadStage;
                            $logic_item_count = 1;
                            $nextLeadStage = $currentLeadStage;
                        }

                        if ($maxCount == $logic_item_count) {
                            $logic_item_name = NULL;
                            $logic_item_count = 0;
                            $nextLeadStage = $statusExplode[2];
                        }

                    } else {
                        $nextLeadStage = $leadStage;
                        $logic_item_name = NULL;
                        $logic_item_count = 0;
                    }
                    if ($data['commonDate'] != NULL) {
                        if ($data['dateType'] != '' && $data['dateType'] == 'Expected Visited Date'){
                            $data_branch_xref_lead['expected_visit_date'] = $data['commonDate'];
                        } else if($data['dateType'] != '' && $data['dateType'] == 'Expected Enrollment Date'){
                            $data_branch_xref_lead['expected_enroll_date'] = $data['commonDate'];
                        } else if($data['dateType'] != '' && $data['dateType'] == 'Expected Admission Date'){
                            $data_branch_xref_lead['expected_admission_date'] = $data['commonDate'];
                        } else if ($data['dateType'] != '') {
                            $data_branch_xref_lead['next_followup_date'] = $data['commonDate'];
                        } else {

                        }
                    }
                    $data_branch_xref_lead['call_type'] = $data['callType'];
                    $data_branch_xref_lead['logic_item_name'] = $logic_item_name;
                    $data_branch_xref_lead['logic_item_count'] = $logic_item_count;
                    $data_branch_xref_lead['logic_item_name'] = $logic_item_name;
                    $data_branch_xref_lead['updated_by'] = $_SERVER['HTTP_USER'];
                    $data_branch_xref_lead['updated_on'] = date('Y-m-d H:i:s');

                    $nextLeadRes = $this->getLeadStageId($nextLeadStage);
                    if ($nextLeadRes === false) {
                        $status = false;
                        $message = 'Invalid lead stage';
                        $data_response = [];
                    } else {
                        $data_branch_xref_lead['status'] = $nextLeadRes['stage_id'];
                        $call_schedule_head_id = $this->getHeadId($data['branchxcerLeadId']);
                        if ($call_schedule_head_id === false)
                        {

                        }
                        else
                        {
                            $this->db->where('fk_lead_stage_id != 20');
                            $this->db->delete("lead_user_xref_lead_due", array("fk_branch_xref_lead_id" => $data['branchxcerLeadId']));

                            $data_lead_user_xref_lead_history['fk_user_id'] = $_SERVER['HTTP_USER'];
                            $data_lead_user_xref_lead_history['fk_branch_xref_lead_id'] = $data['branchxcerLeadId'];
                            $data_lead_user_xref_lead_history['fk_lead_id'] = $data['lead_id'];
                            $data_lead_user_xref_lead_history['fk_lead_call_schedule_head_id'] = $call_schedule_head_id;
                            $data_lead_user_xref_lead_history['lead_status'] = $nextLeadStage;
                            $data_lead_user_xref_lead_history['description'] = $data['comments'];
                            $data_lead_user_xref_lead_history['call_type'] = $data['callType'];

                            $data_lead_user_xref_lead_history['created_on'] = date('Y-m-d H:i:s');
                            //print_r($data_lead_user_xref_lead_history);
                            $this->db->insert('lead_user_xref_lead_history', $data_lead_user_xref_lead_history);

                        }

                        $this->db->where('branch_xref_lead_id', $data['branchxcerLeadId']);
                        $res_update_leadstatus = $this->db->update('branch_xref_lead', $data_branch_xref_lead);
                        //print_r($data_branch_xref_lead);
                        $data_timeline = '';
                        $data_timeline['lead_id'] = $data['lead_id'];
                        $data_timeline['branch_xref_lead_id'] = $data['branchxcerLeadId'];
                        $data_timeline['type'] = 'Lead Status';
                        $data_timeline['call_type'] = $data['callType'];
                        $data_timeline['description'] = $data['comments'];
                        $data_timeline['created_by'] = $_SERVER['HTTP_USER'];
                        $data_timeline['created_on'] = date('Y-m-d H:i:s');
                        $this->db->insert('timeline', $data_timeline);
                        //$res_update_leadstatus='';
                        //print_r($data_timeline);
                        if ($res_update_leadstatus) {
                            $status = true;
                            $message = 'success';
                            $mail_send=0;
                            if($currentLeadStage!=$nextLeadStage && $nextLeadStage=='M3'){
                                $mail_send=1;
                            }
                            $data_response = ['mail_send'=>$mail_send];
                        } else {
                            $status = false;
                            $message = 'Failed! Please try again.';
                            $data_response = [];
                        }


                    }

                } else {
                    $status = false;
                    $message = 'No lead details found';
                    $data_response = [];
                }
            } else {
                $status = false;
                $message = 'Failed! Please try again';
                $data_response = [];
            }
        }
        //exit;
        return $db_response=array('status'=>$status,'message'=>$message,'data'=>$data_response);

    }
    function getLeadStageId($stage=''){
        $data='';
        $this->db->select('lead_stage_id');
        $this->db->from('lead_stage');
        $this->db->where('lead_stage',$stage);
        $res=$this->db->get();
        if($res){
            if($res->num_rows()>0){
                $row=$res->result()[0];
                $data['stage_id']=$row->lead_stage_id;
                return $data;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }
}