<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 1/8/16
 * Time: 12:07 PM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LeadStageChange_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }

    function changeLeadStages($stage_data)
    {
        $this->db->select('`bxl`.`branch_xref_lead_id` as fk_branch_xref_lead_id,bxl.branch_id as fk_branch_id,bxl.fk_course_id,ls.lead_stage_id as fk_lead_stage_id,ls.linked_to as fk_converted_lead_stage_id,'.$_SERVER['HTTP_USER'].' as fk_created_by,"'.date("Y-m-d H:i:s").'" as created_date');
        $this->db->from('branch_xref_lead bxl');
        $this->db->join('lead_stage ls', 'bxl.status=ls.lead_stage_id');
        $this->db->where('bxl.branch_id',$stage_data['branchId']);
        $this->db->where('bxl.fk_course_id',$stage_data['courseId']);
        $this->db->where('ls.linked_to !=','');
        $leads_query = $this->db->get();
        if($leads_query)
        {
            if ($leads_query->num_rows() > 0)
            {
                $data_insert = array(
                    'fk_branch_id' => $stage_data['branchId'],
                    'fk_course_id' => $stage_data['courseId'],
                    'fk_created_by' => $_SERVER['HTTP_USER'] ,
                    'created_date' => date("Y-m-d H:i:s"),
                    'comments' => $stage_data['comments']
                );
                $res_insert=$this->db->insert('lead_stage_management', $data_insert);
                if($res_insert)
                {
                    $lead_mgmt_id = $this->db->insert_id();
                    $leads_convert_data = $leads_query->result_array();
                    foreach($leads_convert_data as $key => $csm)
                    {
                        $leads_convert_data[$key]['fk_lead_stage_management_id'] = $lead_mgmt_id;
                    }
                    $this->db->insert_batch('lead_stage_audit', $leads_convert_data);
                    $lead_stage_query="UPDATE branch_xref_lead bxl JOIN lead_stage st ON bxl.`status`=st.lead_stage_id SET bxl.`status`=st.linked_to, bxl.updated_by=".$_SERVER['HTTP_USER'].",bxl.updated_on='".date('Y-m-d H:i:s')."' WHERE st.linked_to!='' AND bxl.branch_id=".$stage_data['branchId']." AND bxl.fk_course_id=".$stage_data['courseId'];
                    $update_query = $this->db->query($lead_stage_query);
                    if (! $update_query)
                    {
                        $this->db->where('lead_stage_management_id', $lead_mgmt_id);
                        $this->db->delete('lead_stage_management');
                        $error = $this->db->error();
                        $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                    }
                    else
                    {
                        $db_response=array("status"=>true,"message"=>'success',"data"=>[]);
                    }
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
                }
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'No leads present to convert',"data"=>'');
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
        }
        return $db_response;
   }


    function getLeadsCountByStage($stage_data)
    {
        $leadstage_query = $this->db->query('SELECT DISTINCT ls.lead_stage_id,ls.lead_stage,(SELECT COUNT(*) FROM branch_xref_lead WHERE `status` = ls.lead_stage_id AND branch_id = '.$stage_data['branchId'].' AND fk_course_id = '.$stage_data['courseId'].') AS leads_count FROM lead_stage ls WHERE ls.linked_to!="" ORDER BY ls.`order`');
        if($leadstage_query)
        {
            if($leadstage_query->num_rows()>0)
            {
                $leadstage_data = $leadstage_query->result();
                $db_response=array("status"=>true,"message"=>'success',"data"=>$leadstage_data);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Lead stages not present',"data"=>[]);
            }
        }
        else
        {
            $db_response=array("status"=>false,"message"=>'Please try again after sometime',"data"=>[]);
        }
        return $db_response;
    }

    function getLeadsCountOfAllStages($stage_data)
    {
        $this->db->select('lsm.lead_stage_management_id,lsm.fk_branch_id,lsm.fk_course_id,c.`name` as courseName,lsm.comments');
        $this->db->from('lead_stage_management lsm');
        $this->db->join('course c','c.course_id=lsm.fk_course_id');
        $this->db->where('lead_stage_management_id',$stage_data['leadstageId']);
        $leadstagemgmt_query = $this->db->get();
        if($leadstagemgmt_query)
        {
            if($leadstagemgmt_query->num_rows()>0)
            {
                $leadstagemgmt_data = $leadstagemgmt_query->result();
                $leadmgmt_data['conversion_data']=$leadstagemgmt_data;
                //$leadstage_query = $this->db->query("SELECT DISTINCT ls.lead_stage_id,ls.lead_stage,ls.linked_to,(SELECT COUNT(*) FROM lead_stage_audit WHERE fk_branch_id = ".$leadstagemgmt_data[0]->fk_branch_id." AND fk_course_id = ".$leadstagemgmt_data[0]->fk_course_id." AND (fk_lead_stage_id=ls.lead_stage_id OR fk_converted_lead_stage_id=ls.lead_stage_id) AND fk_lead_stage_management_id=".$stage_data['leadstageId'].") AS leadsCount FROM lead_stage ls WHERE ls.is_hidden=0 AND lead_stage!='M7' ORDER BY ls.lead_stage_id,ls.`order`");
                $leadstage_query = $this->db->query("SELECT DISTINCT ls.lead_stage_id,ls.lead_stage as from_lead_stage,ls2.lead_stage as to_lead_stage, (SELECT COUNT(*) FROM lead_stage_audit WHERE fk_branch_id = ".$leadstagemgmt_data[0]->fk_branch_id." AND fk_course_id = ".$leadstagemgmt_data[0]->fk_course_id." AND (fk_lead_stage_id=ls.lead_stage_id OR fk_converted_lead_stage_id=ls.lead_stage_id) AND fk_lead_stage_management_id=".$stage_data['leadstageId'].") AS leads_count FROM lead_stage ls LEFT JOIN lead_stage ls2 ON ls.linked_to=ls2.lead_stage_id WHERE ls.is_hidden=0 AND ls.lead_stage!='M7' AND ls.linked_to!='' ORDER BY ls.lead_stage_id,ls.`order`");
                if($leadstage_query)
                {
                    if($leadstage_query->num_rows()>0)
                    {
                        $leadstage_data = $leadstage_query->result();
                        $leadmgmt_data['leadstage_count']=$leadstage_data;
                        $db_response=array("status"=>true,"message"=>'success',"data"=>$leadmgmt_data);
                    }
                    else
                    {
                        $db_response=array("status"=>false,"message"=>'Lead stages not present',"data"=>[]);
                    }
                }
                else
                {
                    $db_response=array("status"=>false,"message"=>'Please try again after sometime',"data"=>[]);
                }
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Lead stages not present',"data"=>[]);
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
        }
        return $db_response;
    }


    function getAllConvertedToOldLeads($branchId)
    {
        $table = 'lead_stage_management';
        // Table's primary key
        $primaryKey = 'lsm`.`lead_stage_management_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
                        array( 'db' => 'c.`name` as course_name',           'dt' => 'course_name',                  'field' => 'course_name' ),
                        array( 'db' => 'e.`name` as created_by',            'dt' => 'created_by',                   'field' => 'created_by' ),
                        array( 'db' => 'lsm.created_date',                  'dt' => 'created_date',                 'field' => 'created_date','formatter' => function( $d, $row ) {
                                return date( 'd M,Y', strtotime($d));
                            } ),
                        array( 'db' => 'lsm.comments',                      'dt' => 'comments',                     'field' => 'comments' ),
                        array( 'db' => 'lsm.lead_stage_management_id',      'dt' => 'lead_stage_management_id',     'field' => 'lead_stage_management_id' )
        );

        $globalFilterColumns = array(
                        array( 'db' => 'c.`name`',                          'dt' => '`name`',           'field' => '`name`' ),
                        array( 'db' => 'e.`name`',                          'dt' => '`name`',           'field' => '`name`' ),
                        array( 'db' => 'lsm.comments',                      'dt' => 'comments',         'field' => 'comments' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM lead_stage_management lsm JOIN branch b ON lsm.fk_branch_id=b.branch_id JOIN course c ON lsm.fk_course_id=c.course_id JOIN employee e ON lsm.fk_created_by=e.user_id ";
        $extraWhere = " lsm.fk_branch_id = ".$branchId;
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));
        return $responseData;
    }
}