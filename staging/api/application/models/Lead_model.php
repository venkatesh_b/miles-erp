<?php
/* 
 * Created By: V Parameshwar. Date: 03-02-2016
 * Purpose: Services for Contacts.
 * 
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lead_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('encryption');
        $this->db->db_debug = FALSE;
    }
    public function contactConfigs(){

    }
    public function checkContact($param){

        $contact_id=$param['contactId'];
        $this->db->select('name,status,course,is_lead');
        $this->db->where('contact_id', $contact_id);
        $query = $this->db->get('contact');
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else{
            if($query->num_rows()>0){
                $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
            }
            else{
                $db_response=array("status"=>false,"message"=>'No contact details found! ',"data"=>$query->result());
            }
        }
        return $db_response;
    }
    public function checkEmailExist($param){

        $email_id=$param;
        $this->db->select('name,lead_id');
        $this->db->where('email', $email_id);
        $query = $this->db->get('lead');
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else{
            if($query->num_rows()>0){
                $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
            }
            else{
                $db_response=array("status"=>false,"message"=>'No contact details found! ',"data"=>$query->result());
            }
        }
        return $db_response;
    }
    public function getContactCities($countryId=''){

        $this->db->select('rtv2.reference_type_value_id id, rtv2.value, rtv2.is_active, rtv2.parent_id, rtv2.description');
        $this->db->from('reference_type_value rtv');
        $this->db->join('reference_type_value rtv1','rtv1.parent_id=rtv.reference_type_value_id');
        $this->db->join('reference_type_value rtv2','rtv2.parent_id=rtv1.reference_type_value_id');
        if($countryId!=0)
            $this->db->where('rtv.reference_type_value_id', $countryId);
        $this->db->where('rtv.is_active', '1');
        $this->db->where('rtv1.is_active', '1');
        $this->db->where('rtv2.is_active', '1');
        $this->db->order_by('rtv2.value', "asc");
        $query = $this->db->get();

        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else{
            if($query->num_rows()>0){
                $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
            }
            else{
                $db_response=array("status"=>false,"message"=>'No cities found! ',"data"=>$query->result());
            }
        }
        return $db_response;
    }
    public function getDuplicateContacts($contactId=''){
        $finalresponse=array();
        $finalresponse['emailId']='';
        $finalresponse['duplicates']='';
        $name=$phone=$email=$city=$tags=$source=array();
        $this->db->select("l.lead_id,l.name,l.phone,l.email,`rtv`.`value` as city,`rtv2`.`value` as country,rtv.reference_type_value_id as cityId,rtv2.reference_type_value_id as countryId");
        $this->db->from('lead l');
        if($contactId!=''){
            $this->db->where('l.lead_id',$contactId);
        }
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=l.city_id','LEFT');
        $this->db->join('reference_type_value rtv1','rtv1.reference_type_value_id=rtv.parent_id','LEFT');
        $this->db->join('reference_type_value rtv2','rtv2.reference_type_value_id=rtv1.parent_id','LEFT');
        $query = $this->db->get();
        if($query){
            if($query->num_rows()>0){
                $data_contacts=$query->result();
                foreach($data_contacts as $k=>$v) {
                    $emailId = $v->email;
                    $finalresponse['emailId']=$emailId;
                    $finalresponse['duplicates']=array();
                    //get the names
                    $this->db->select("l.lead_id,l.name,l.phone,l.email,`rtv`.`value` as city,`rtv2`.`value` as country,rtv.reference_type_value_id as cityId,rtv2.reference_type_value_id as countryId,rtvSource.value as source");
                    $this->db->from('lead l');
                    $this->db->where('l.email',$emailId);
                    $this->db->where('l.email!=',"");
                    $this->db->where('l.email is not null');
                    //$this->db->where('l.status',1);
                    $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=l.city_id','LEFT');
                    $this->db->join('reference_type_value rtv1','rtv1.reference_type_value_id=rtv.parent_id','LEFT');
                    $this->db->join('reference_type_value rtv2','rtv2.reference_type_value_id=rtv1.parent_id','LEFT');
                    $this->db->join('reference_type_value rtvSource','rtvSource.reference_type_value_id=l.fk_source_id','LEFT');
                    $query = $this->db->get();
                    if($query->num_rows()>0){
                        $data_duplicates=$query->result();
                        foreach($data_duplicates as $k_duplicates=>$v_duplicates) {
                            $name[]=$v_duplicates->name;
                            $phone[]=$v_duplicates->phone;
                            $email[]=$v_duplicates->email;
                            $city[]=$v_duplicates->city.'-'.$v_duplicates->country;
                            $source[]=$v_duplicates->source;

                            $this->db->select("`rtv`.`value` as tagName");
                            $this->db->from('lead_xref_tag l');
                            $this->db->where('l.lead_id',$v_duplicates->lead_id);
                            $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=l.tag_id');
                            $query = $this->db->get();
                            if($query->num_rows()>0){
                                $data_duplicates_tags=$query->result();
                                foreach($data_duplicates_tags as $k_duplicates_tags=>$v_duplicates_tags) {
                                    $tags[]=$v_duplicates_tags->tagName;
                                }
                            }


                        }
                    }
                }
            }
        }
        else{

        }
        $finalresponse['duplicates']['name']='';
        $finalresponse['duplicates']['phone']='';
        $finalresponse['duplicates']['email']='';
        $finalresponse['duplicates']['city']='';
        $finalresponse['duplicates']['tags']='';
        $finalresponse['duplicates']['source']='';
        $name=array_values(array_unique(array_filter($name)));
        $phone=array_values(array_unique(array_filter($phone)));
        $email=array_values(array_unique(array_filter($email)));
        $city=array_values(array_unique(array_filter($city)));
        $tags=array_values(array_unique(array_filter($tags)));
        $source=array_values(array_unique(array_filter($source)));


        if(count($name)>0)
            $finalresponse['duplicates']['name']=$name;
        if(count($phone)>0)
            $finalresponse['duplicates']['phone']=$phone;
        if(count($email)>0)
            $finalresponse['duplicates']['email']=$email;
        if(count($city)>0)
            $finalresponse['duplicates']['city']=$city;
        if(count($tags)>0)
            $finalresponse['duplicates']['tags']=$tags;
        if(count($source)>0)
            $finalresponse['duplicates']['source']=$source;
        $db_response=array("status"=>true,"message"=>"success","data"=>$finalresponse);
        return $db_response;


    }
    public function getContacts($contactId='') {

        $final_result=array();
        /*$this->db->select('sum(if(status=0,1,0)) as newContactsCount,sum(if(status=1,1,0)) as activeContactsCount,sum(if(status=2,1,0)) as blockedContactsCount,sum(if(status=3,1,0)) as inactiveContactsCount,sum(if(status!=3,1,0)) as totalContactsCount,sum(if(status!=3 && is_lead=1 ,1,0)) as convertedLeadCount');
        $query = $this->db->get('contact');

        if(!$query){
            $final_result['counts']=array();
        }
        else{
            if($query->num_rows()>0){
                $final_result['counts']=$query->result();
            }
            else{
                $final_result['counts']=array();
            }
           }*/
        $final_result['counts']=array();

        $this->db->select("l.lead_id,l.name,l.phone,l.alternate_mobile,l.email,`rtv`.`value` as city,`rtv2`.`value` as country,rtv.reference_type_value_id as cityId,rtv2.reference_type_value_id as countryId,l.fk_source_id,rtvSource.value as sourceName");
        $this->db->from('lead l');
        if($contactId!=''){
            $this->db->where('l.lead_id',$contactId);
        }
        $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=l.city_id','LEFT');
        $this->db->join('reference_type_value rtv1','rtv1.reference_type_value_id=rtv.parent_id','LEFT');
        $this->db->join('reference_type_value rtv2','rtv2.reference_type_value_id=rtv1.parent_id','LEFT');
        $this->db->join('reference_type_value rtvSource','rtvSource.reference_type_value_id=l.fk_source_id','LEFT');
        $query = $this->db->get();

        if($query){
            $data_contacts=$query->result();
            foreach($data_contacts as $k=>$v){
                $lead_id=$v->lead_id;
                $v->lead_id=encode($v->lead_id);
                $this->db->select("cxt.tag_id as id,rtv.`value` as tagName");
                $this->db->from('lead_xref_tag cxt');
                $this->db->join('reference_type_value rtv','rtv.reference_type_value_id=cxt.tag_id');
                $this->db->where('cxt.lead_id',$lead_id);
                $query_tag = $this->db->get();
                $v->tags=$query_tag->result();

            }
            $final_result['contacts']=$data_contacts;
        }
        else{
            $final_result['contacts']=array();
        }


        $db_response=array("status"=>true,"message"=>"success","data"=>$final_result);

        return $db_response;

    }

    public function addContact($param) {
        //$this->db->trans_begin();

        $data_insert = array(
            'name' => $param['contactName'] ,
            'email' => $param['contactEmail'],
            'phone' => $param['contactPhone'],
            'city_id' => $param['contactCity']);
        $this->db->query('SET @leadId=0');

        $storeproc='CALL Sp_Add_Edit_Contact("'.$param['contactName'].'","'.$param['contactPhone'].'","'.$param['contactEmail'].'","'.$param['contactCity'].'","'.$param['contactCountry'].'","'.$param['contactTags'].'",@leadId,0,0,@msg,"'.$param['contactSource'].'",'.$_SERVER['HTTP_USER'].',"","")';


        $res_insert=$this->db->query($storeproc);

        if($res_insert){
            $query_response = $this->db->query("SELECT @msg as message");
            $query_response_result=$query_response->result();
            $message='Contact added successfully';
            $status=true;
            foreach($query_response_result as $klog=>$vlog){
                if($vlog->message!=''){
                    $message=$vlog->message;
                    $status=false;
                }
            }

            $db_response=array("status"=>$status,"message"=>$message,"data"=>array("message"=>$message));
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }

        //$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            //$this->db->trans_rollback();
        }
        else
        {
            // $this->db->trans_commit();
        }

        return $db_response;

    }
    public function editContact($param) {
        //$this->db->trans_begin();

        $data_insert = array(
            'name' => $param['contactName'] ,
            'email' => $param['contactEmail'],
            'phone' => $param['contactPhone'],
            'city_id' => $param['contactCity']);
        $this->db->query('SET @leadId='.$param['contactId']);

        $storeproc='CALL Sp_Add_Edit_Contact("'.$param['contactName'].'","'.$param['contactPhone'].'","'.$param['contactEmail'].'","'.$param['contactCity'].'","'.$param['contactCountry'].'","'.$param['contactTags'].'",@leadId,0,0,@msg,"'.$param['contactSource'].'",'.$_SERVER['HTTP_USER'].',"","")';


        $res_insert=$this->db->query($storeproc);

        if($res_insert){
            $query_response = $this->db->query("SELECT @msg as message");
            $query_response_result=$query_response->result();
            $message='Contact updated successfully';
            $status=true;
            foreach($query_response_result as $klog=>$vlog){
                if($vlog->message!=''){
                    $message=$vlog->message;
                    $status=false;
                }
            }
            if($message=='Contact updated successfully'){
                $this->releaseDueIfExists($param['contactId']);
            }
            $db_response=array("status"=>$status,"message"=>$message,"data"=>array("message"=>$message));
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }

        //$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            //$this->db->trans_rollback();
        }
        else
        {
            // $this->db->trans_commit();
        }

        return $db_response;
    }
    public function activeContact($param){

        $contact_id=$param['contactId'];
        $data_update = array('status' => 1);
        $this->db->where('contact_id', $contact_id);
        $res_update=$this->db->update('contact', $data_update);
        if($res_update){
            $db_response=array("status"=>true,"message"=>"success","data"=>array());
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    public function deleteContact($param,$mainLeadId) {
        $lead_id=$param;
        //$this->db->where('lead_id',$lead_id);
        //$res_delete_leadTags=$this->db->delete('lead_xref_tag');
        $data_update = array('status' => 0,'merge_lead_id' => $mainLeadId);
        $this->db->where('lead_id', $lead_id);
        $res_delete_lead=$this->db->update('lead',$data_update);
        //$res_delete_lead=$this->db->delete('lead');

    }
    public function blockContact($param) {
        $contact_id=$param['contactId'];
        $data_update = array('status' => 2);
        $this->db->where('contact_id', $contact_id);
        $res_update=$this->db->update('contact', $data_update);
        if($res_update){
            $db_response=array("status"=>true,"message"=>"success","data"=>array());
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function leadConvertContact($param) {

        $contact_id=$param['contactId'];
        $data_update = array('is_lead' => 1);
        $this->db->where('contact_id', $contact_id);
        $res_update=$this->db->update('contact', $data_update);
        if($res_update){
            $db_response=array("status"=>true,"message"=>"success","data"=>array());
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function BulkContacts($param){
        $this->load->helper('email');
        $this->db->trans_start();
        $delimiters=array(",","|","/",";");
        try{
            $fileName=$param['file_name'];
            $content=$param['Rows'];
            $data_mt_insert=array("file_name"=>$fileName,"file_type"=>1,"created_date"=>date('Y-m-d H:i:s'),"fk_created_by"=>$_SERVER['HTTP_USER'],"fk_import_type_id"=>59);
            $res_insert_mt=$this->db->insert('import_history', $data_mt_insert);
            if($res_insert_mt){

                $master_id=$this->db->insert_id();
                $rows=array();
                $row_number=0;
                foreach($content as $k=>$v){
                    $data_insert=array();
                    $row_number=$row_number+1;
                    $primary_email = '';
                    $secondary_email='';
                    $primary_phone = '';
                    $secondary_phone='';
                    if($v['Email']!='') {
                        $email_multi_explode = explodeX($delimiters, $v['Email']);
                        if(isset($email_multi_explode[0])) {
                            $primary_email = $email_multi_explode[0];
                            unset($email_multi_explode[0]);
                            $secondary_email=implode(',',$email_multi_explode);
                        }
                        else {
                            $primary_email = '';
                            $secondary_email='';
                        }
                    }
                    if($v['Phone']!='') {
                        $phone_multi_explode = explodeX($delimiters, $v['Phone']);
                        if(isset($phone_multi_explode[0])) {
                            $primary_phone = $phone_multi_explode[0];
                            unset($phone_multi_explode[0]);
                            $secondary_phone=implode(',',$phone_multi_explode);
                        }
                        else {
                            $primary_phone = '';
                            $secondary_phone='';
                        }

                    }
                    $data_insert['fk_import_history_id']=$master_id;
                    $data_insert['name']=$v['Name'];
                    $data_insert['email']=$primary_email;
                    $data_insert['phone']=$primary_phone;
                    $data_insert['country']=$v['Country'];
                    $data_insert['city']=$v['City'];
                    $data_insert['tags']=$v['Tags'];
                    $data_insert['contact_source']=$v['Source'];
                    $data_insert['row_number']=$row_number;
                    if($secondary_email!='') {
                        $data_insert['secondary_email'] = $secondary_email;
                    }
                    else{
                        $data_insert['secondary_email']=NULL;
                    }
                    if($secondary_phone!='') {
                        $data_insert['secondary_phone'] = $secondary_phone;
                    }
                    else{
                        $data_insert['secondary_phone']=NULL;
                    }
                    $rows[]=$data_insert;


                }
                $res_insert_rows=$this->db->insert_batch('contact_import_item', $rows);
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else{
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                $this->db->trans_rollback();
            }
            else
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array("uploadId"=>$master_id));
                $this->db->trans_commit();
            }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>$ex->getMessage(),"data"=>array());
        }

        return $db_response;


    }
    function checkPhoneAlreadyExist($param){
        $phone=$param;
        $this->db->select('name,phone');
        $this->db->where('phone', $param);
        $query = $this->db->get('lead');
        if(!$query){
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else{
            if($query->num_rows()>0){
                $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
            }
            else{
                $db_response=array("status"=>false,"message"=>'No contact details found! ',"data"=>$query->result());
            }
        }
        return $db_response;
    }
    function checkCityExist($param){
        $this->db->select("rtv.reference_type_value_id,rtv.`value`,rtv.description");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='City'");
        $this->db->where("rtv.value",$param);
        $this->db->order_by("rtv.`value`");
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $city_k=>$city_v){
                    $cityId=$city_v->reference_type_value_id;
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>array('cityId'=>$cityId));
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid City',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>"Somethig went wrong while fetching City informtion","data"=>array());
        }
        return $db_response;
    }
    public function checkTagExist($param){
        $this->db->select("rtv.reference_type_value_id,rtv.`value`,rtv.description");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='Tag'");
        $this->db->where("rtv.value",$param);
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $k=>$v){
                    $tagId=$v->reference_type_value_id;
                }
                //exit;
                $db_response=array("status"=>true,"message"=>'success',"data"=>array('tagId'=>$tagId));
            }
            else{
                $db_response=array("status"=>false,"message"=>'Invalid Tag ID',"data"=>array());
            }
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;

    }
    function createTag($param){

        $this->db->select('reference_type_id,name');
        $this->db->where('name', 'Tag');
        $query = $this->db->get('reference_type');
        $tagTypeId=$query->result();

        foreach($tagTypeId as $tag_k=>$tag_v){
            $tagtype=$tag_v->reference_type_id;
        }

        $data_insert = array(
            'reference_type_id' => $tagtype,
            'value' => $param['value'],
            'fk_created_by' => $param['userId'] ,
            'created_date' => date("Y-m-d H:i:s"),
            'parent_id' => NULL,
            'is_active' => 1,
            'description' => ''
        );

        $res_insert=$this->db->insert('reference_type_value', $data_insert);
        $ReferenceId = $this->db->insert_id();
        if($res_insert){
            $db_response=array("status"=>true,"message"=>'success',"data"=>array("tagId"=>$ReferenceId));
        }
        else{
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("tagId"=>NULL));
        }
        return$db_response;

    }
    function SaveBulkContacts($file_upload_id){

        //$this->db->trans_start();
        $success_count=0;
        $falied_count=0;
        $total_count=0;
        $duplicates_count=0;
        try{
            $this->db->select('contact_import_item_id,name,email,phone,country,city,tags,status,contact_source,secondary_email,secondary_phone');
            $this->db->from('contact_import_item');
            $this->db->where('fk_import_history_id',$file_upload_id);
            $query = $this->db->get();

            if($query){
                $total_count=$query->num_rows();
                if ($total_count > 0)
                {

                    $result=$query->result();
                    $this->db->query("CALL Sp_Import_Contact($file_upload_id,".$_SERVER['HTTP_USER'].")");
                    $this->db->close();
                    $this->load->database();
                }
                else{
                    $db_response=array("status"=>false,"message"=>'No Records Found',"data"=>array());
                }
            }
            else{
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>$ex->getMessage(),"data"=>array());
        }

        if ($this->db->trans_status() === FALSE)
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            //$this->db->trans_rollback();
        }
        else
        {

            $this->db->select('count(contact_import_item_id) as totalCnt,sum(if(status=1,1,0)) as successCnt,sum(if(status=2 && error_log="Contact already exists.",1,0)) as duplicateCnt,sum(if(status=2,1,0)) as failureCnt');
            $this->db->from('contact_import_item');
            $this->db->where('fk_import_history_id',$file_upload_id);
            $countsQuery=$this->db->get();
            $countsQueryRes=$countsQuery->result();
            foreach($countsQueryRes as $kCounts=>$vCounts){
                $total_count=$vCounts->totalCnt;
                $success_count=$vCounts->successCnt;
                $failed_count=$vCounts->failureCnt;
                $duplicates_count=$vCounts->duplicateCnt;

            }

            $finalresp['Counts']=array("total_count"=>$total_count,"successCount"=>$success_count,"failedCount"=>($failed_count-$duplicates_count),"duplicateCount"=>$duplicates_count);
            $this->db->select('contact_import_item_id,name,status,error_log,row_number');
            $this->db->from('contact_import_item');
            $this->db->where('fk_import_history_id',$file_upload_id);
            $query = $this->db->get();
            $finalresp['Records']=$query->result();
            $db_response=array("status"=>true,"message"=>'success',"data"=>$finalresp);
            //$this->db->trans_commit();
        }
        return $db_response;
    }

    function leadsListDataTables($params)
    {
        $filter_condition='';$city_filter_condition='';
        if($params['appliedFiltersString']!=''){
            $Filters=explode(',',$params['appliedFiltersString']);
        }
        else{
            $Filters=array();
        }
        if($params['appliedCityFilters']!=''){
            $CityFilters=explode(',',$params['appliedCityFilters']);
        }
        else{
            $CityFilters=array();
        }
        if(count($CityFilters) == 0)
        {
            $city_filter_condition="";
        }
        else
        {
                $city_filter_condition = ' l.city_id IN ('.$params['appliedCityFilters'].')';
                $city_filter_condition .= ' AND ';
        }
        if(count($Filters)==0){
            $filter_condition=" ";
        }
        else {
            if (in_array('All', $Filters)) {
                $filter_condition.=" (l.is_dnd=1 OR l.is_dnd=0) OR ";
            }
            if (in_array('DND', $Filters)) {
                $filter_condition .= " l.is_dnd=1 OR ";
            }
            if (in_array('OutStation', $Filters)) {
                $filter_condition .= " ( l.city_id not in (select DISTINCT(bc.fk_city_id) from branch b, branch_xref_city bc where b.branch_id=bc.fk_branch_id and bc.is_primary=1) AND l.fk_branch_id is null ) OR ";
            }
            if(in_array('ContactStudent', $Filters)){
                $filter_condition .= " ls.lead_stage='M7' OR ";
            }
            if(in_array('ContactLead', $Filters)){
                $filter_condition .= " ls.lead_stage!='M7' OR ";
            }
            if(in_array('Contacts', $Filters)){
                $filter_condition .= " (select if(count(l4.lead_id)>=1,1,0) from branch_xref_lead l4 where l4.lead_id=l.lead_id and l4.is_active=1) = 0 ";
            }
            if(in_array('MergeContact', $Filters)){
                $filter_condition .= " (select if(count(lead_id)>1,1,0) from lead l2 where l2.email=l.email and l2.status=1 and l.email!='' and l.email is not NULL) = 1 AND
                                       (select if(count(l4.lead_id)>=1,1,0) from branch_xref_lead l4 where l4.lead_id=l.lead_id and l4.is_active=1) = 0 ";
            }

            if($filter_condition!='') {
                $filter_condition = rtrim($filter_condition, ' OR ');
                $filter_condition = '(' . $filter_condition . ')';
                $filter_condition .= ' AND ';
            }
        }
        $table = 'lead';
        // Table's primary key
        $primaryKey = 'l`.`lead_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`l`.`lead_id`'                          ,'dt' => 'lead_id'          ,'field' => 'lead_id' ),
            array( 'db' => '`l`.`name`'                             ,'dt' => 'name'             ,'field' => 'name' ),
            array( 'db' => '`l`.`email`'                            ,'dt' => 'email'            ,'field' => 'email' ),
            array( 'db' => '`l`.`phone`'                            ,'dt' => 'phone'            ,'field' => 'phone' ),
            array( 'db' => 'rtv.value AS city'                      ,'dt' => 'city'             ,'field' => 'city' ),
            array( 'db' => 'GROUP_CONCAT(DISTINCT(rtvt.`value`) ORDER BY rtvt.`value` ASC) AS `tags`'   ,'dt' => 'tags'             ,'field' => 'tags' ),
            array( 'db' => '`l`.`lead_id`'                          ,'dt' => 'lead_id'          ,'field' => 'lead_id' ),
            array( 'db' => '(select if(count(lead_id)>1,1,0) from lead l2 where l2.email=l.email and l2.status=1 and l.email!="" and l.email is not NULL) as `is_duplicate`'   ,'dt' => 'is_duplicate'             ,'field' => 'is_duplicate' ),
            array( 'db' => '`l`.`status`'                          ,'dt' => 'status'          ,'field' => 'status' ),
            array( 'db' => '`l`.`is_dnd`'                          ,'dt' => 'is_dnd'          ,'field' => 'is_dnd' ),
            array( 'db' => '(select if(count(l2.lead_id)>=1,1,0) from user_xref_lead_due l2 where l2.lead_id=l.lead_id) as `is_scheduled`'   ,'dt' => 'is_scheduled'             ,'field' => 'is_scheduled' ),
            array( 'db' => '(select if(count(l3.lead_id)>=1,1,0) from branch_xref_lead l3 where l3.lead_id=l.lead_id and l3.is_active=1) as `is_lead`'   ,'dt' => 'is_lead'             ,'field' => 'is_lead' ),
            array( 'db' => 'b.name as `branch_name`'   ,'dt' => 'branch_name'             ,'field' => 'branch_name' ),
            array( 'db' => 'c.name as `course_name`'   ,'dt' => 'course_name'             ,'field' => 'course_name' ),
            array( 'db' => 'ls.lead_stage as `lead_stage`'   ,'dt' => 'lead_stage'             ,'field' => 'lead_stage' ),
            array( 'db' => 'b2.code as `branch_code`'   ,'dt' => 'branch_code'             ,'field' => 'branch_code' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`l`.`email`'        ,'dt' => 'email'            ,'field' => 'email' ),
            array( 'db' => '`l`.`name`'         ,'dt' => 'name'             ,'field' => 'name' ),
            array( 'db' => '`l`.`phone`'        ,'dt' => 'phone'            ,'field' => 'phone' ),
            array( 'db' => 'rtv.value'          ,'dt' => 'value'            ,'field' => 'value' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM lead l LEFT JOIN reference_type_value rtv ON rtv.reference_type_value_id=l.city_id LEFT JOIN lead_xref_tag lxt ON lxt.lead_id=l.lead_id LEFT JOIN reference_type_value rtvt ON rtvt.reference_type_value_id=lxt.tag_id LEFT JOIN branch_xref_lead bxl ON bxl.lead_id=l.lead_id LEFT JOIN branch b ON b.branch_id=bxl.branch_id LEFT JOIN course c ON c.course_id=bxl.fk_course_id LEFT JOIN lead_stage ls ON ls.lead_stage_id=bxl.status LEFT JOIN branch b2 ON b2.branch_id=l.fk_branch_id ";
        $extraWhere = $filter_condition.$city_filter_condition." `l`.`status`=1 ";
        $groupBy = "`l`.`lead_id`";
        $extraOrder = "`l`.`email`";

        if(isset($_GET['order'][0]['column']))
        {
            $_GET['order'][0]['column'] = 1;
        }

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy,$extraOrder));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['lead_id']=encode($responseData['data'][$i]['lead_id']);
        }
        return $responseData;
    }
    public function getContactCounts(){
        $this->db->db_debug = FALSE;
        $final_result=array();
        $data['OverallCount']='----';
        $data['UnscheduledCount']='----';
        $data['DBLeadCount']='----';
        $data['DBDNDCount']='----';
        $data['EnrolledCount']='----';


        $this->db->select('count(lead_id) as OverallCount');
        $this->db->where('status',1);
        $query = $this->db->get('lead');
        if($query){
            if($query->num_rows()>0){
                $res=$query->result();
                foreach($res as $key=>$val){
                    $data['OverallCount']=$val->OverallCount;
                }
            }
        }
        $this->db->select('count(lead_id) as DBDNDCount');
        $this->db->where('is_dnd',1);
        $query = $this->db->get('lead');
        if($query){
            if($query->num_rows()>0){
                $res=$query->result();
                foreach($res as $key=>$val){
                    $data['DBDNDCount']=$val->DBDNDCount;
                }
            }
        }

        $unscheduleQuery="select count(l.lead_id) as UnscheduledCount  from lead l where l.status=1 and l.lead_id not in (select lead_id from user_xref_lead_due) and l.lead_id not in (select lead_id from branch_xref_lead) and l.is_dnd=0";
        $query = $this->db->query($unscheduleQuery);
        if($query){
            if($query->num_rows()>0){
                $res=$query->result();
                foreach($res as $key=>$val){
                    $data['UnscheduledCount']=$val->UnscheduledCount;
                }
            }
        }

        $dbleadQuery="select count(l.lead_id) as DBLeadCount  from lead l where l.status=1 and l.lead_id in (select lead_id from branch_xref_lead)";
        $query = $this->db->query($dbleadQuery);
        if($query){
            if($query->num_rows()>0){
                $res=$query->result();
                foreach($res as $key=>$val){
                    $data['DBLeadCount']=$val->DBLeadCount;
                }
            }
        }

        $db_response = array("status" => TRUE, "message" => 'success', "data" => $data);
        return $db_response;
    }
    public function mergeContact($param) {
        //$this->db->trans_begin();

        $this->db->select('b.branch_xref_lead_id,l.phone');
        $this->db->from('branch_xref_lead b');
        $this->db->join('lead l','l.lead_id=b.lead_id');
        $this->db->where('l.email',$param['contactEmail']);
        $this->db->where('l.status',1);
        $this->db->where('b.is_active',1);
        $query=$this->db->get();
        if($query->num_rows()>0) {
            $res_leadexist=$query->result();
            foreach($res_leadexist as $k_leadexist=>$v_leadexist) {
                $row_leadexist[]=$v_leadexist->phone;
            }
            $db_response = array("status" => false, "message" => "Merge not possible! Contacts (".implode(',',$row_leadexist).") already converted to lead", "data" => array("message" => "Merge not possible! Due to contacts (".implode(',',$row_leadexist).") already converted to lead"));
        }
        else{

            $this->db->select('b.user_xref_lead_due_id,b.lead_id');
            $this->db->from('user_xref_lead_due b');
            $this->db->join('lead l','l.lead_id=b.lead_id');
            $this->db->where('l.email',$param['contactEmail']);
            $this->db->where('l.status',1);
            $query=$this->db->get();
            if($query->num_rows()>0) {
                $res_leadDueexist = $query->result();
                foreach ($res_leadDueexist as $k_leadDueexist => $v_leadDueexist) {
                    $resp[]=$this->releaseDueIfExists($v_leadDueexist->lead_id);
                }
            }


            $this->db->select("GROUP_CONCAT(lead_id) as leadIds,MAX(lead_id) as recentLead_id");
            $this->db->from('lead');
            $this->db->where('email',$param['contactEmail']);
            $this->db->where('status',1);
            $query=$this->db->get();
            if($query->num_rows()>0){
                $res=$query->result();
                foreach($res as $k=>$v) {
                    $lead_id=$v->recentLead_id;
                    $lead_id_hist=$v->leadIds;
                    $explode_leadIds=explode(',',$lead_id_hist);
                    for($exp=0;$exp<count($explode_leadIds);$exp++){
                        if($explode_leadIds[$exp]!=$lead_id){
                            $delete_leadId=$explode_leadIds[$exp];
                            $this->deleteContact($delete_leadId,$lead_id);
                        }
                    }
                    $this->db->close();
                    $this->load->database();


                    $this->db->query('SET @msg=NULL');
                    $this->db->query('SET @leadId='.$lead_id);
                    $storeproc = 'CALL Sp_Add_Edit_Contact("' . $param['contactName'] . '",' . $param['contactPhone'] . ',"' . $param['contactEmail'] . '","' . $param['contactCity'] . '","' . $param['contactCountry'] . '","' . $param['contactTags'] . '",@leadId,0,0,@msg,"' . $param['contactSource'] . '",'.$_SERVER['HTTP_USER'].',"","")';
                    $res_insert = $this->db->query($storeproc);
                    if ($res_insert) {
                        $query_response = $this->db->query("SELECT @msg as message");
                        $query_response_result = $query_response->result();
                        $message = 'Contact added successfully';
                        $status = true;
                        foreach ($query_response_result as $klog => $vlog) {
                            if ($vlog->message != '') {
                                $message = $vlog->message;
                                $status = false;
                            }
                        }

                        $db_response = array("status" => $status, "message" => $message, "data" => array("message" => $message));
                    } else {
                        $error = $this->db->error();
                        $db_response = array("status" => false, "message" => $error['message'], "data" => array());
                    }

                    $storeproc_mergeContact = "CALL Sp_Merge_Contact('$lead_id_hist')";
                    $res_insert = $this->db->query($storeproc_mergeContact);
                }
            }
        }



        return $db_response;

    }
    function addQuickLead($params){
        $this->db->select("b.name as branchName,b.fk_city_id,rtvCity.value cityName,rtvState.value stateName,rtvCountry.value countryName");
        $this->db->from("branch b");
        $this->db->JOIN('reference_type_value rtvCity','rtvCity.reference_type_value_id=b.fk_city_id','LEFT');
        $this->db->JOIN('reference_type_value rtvState','rtvState.reference_type_value_id=rtvCity.parent_id','LEFT');
        $this->db->JOIN('reference_type_value rtvCountry','rtvCountry.reference_type_value_id=rtvState.parent_id','LEFT');
        $this->db->where("b.is_active",1);
        $this->db->where("b.branch_id",$params['branchId']);
        $query=$this->db->get();
        if($query) {
            if ($query->num_rows() > 0) {
                $resp = $query->result();
                $cityName=NULL;
                $stateName=NULL;
                $countryName=NULL;
                foreach($resp as $k_row=>$v_row){
                    $cityName=$v_row->cityName;
                    $stateName=$v_row->stateName;
                    $countryName=$v_row->countryName;
                }
                $this->db->query('SET @msg=NULL');
                $this->db->query('SET @contact_id=NULL');
                $storeproc = 'CALL `Sp_Add_Edit_Lead`("' . $params['contactName'] . '", "' . $params['contactPhone'] . '", "' . $params['contactEmail'] . '", "'.$cityName.'", "'.$countryName.'", "'.$params['leadTags'].'", @contact_id, NULL, NULL, @msg,"'.$params['LeadSource'].'", "'.$params['contactCourseText'].'","'.$params['contactDescription'].'",NULL,NULL,NULL,'.$params['branchId'].','.$params['userId'].',"'.$params['leadType'].'",NULL,"","",NULL)';
                $res_insert = $this->db->query($storeproc);
                if ($res_insert) {
                    $query_response = $this->db->query("SELECT @msg as message");
                    $query_response_result = $query_response->result();
                    $message = 'Lead added successfully';
                    $status = true;
                    foreach ($query_response_result as $klog => $vlog) {
                        if ($vlog->message != '') {
                            $message = $vlog->message;
                            $status = false;
                        }
                    }

                    $db_response = array("status" => $status, "message" => $message, "data" => array("message" => $message));
                } else {
                    $error = $this->db->error();
                    $db_response = array("status" => false, "message" => $error['message'], "data" => array());
                }
            }
            else{
                $db_response = array("status" => false, "message" => "Invalid details", "data" => array("message" => "Invalid details"));
            }
        }
        else{
            $error = $this->db->error();
            $db_response = array("status" => false, "message" => $error['message'], "data" => array());
        }
        return $db_response;
    }
    function addLead($params){

        $this->db->select("b.name as branchName,b.fk_city_id,rtvCity.value cityName,rtvState.value stateName,rtvCountry.value countryName");
        $this->db->from("branch b");
        $this->db->JOIN('reference_type_value rtvCity','rtvCity.reference_type_value_id=b.fk_city_id','LEFT');
        $this->db->JOIN('reference_type_value rtvState','rtvState.reference_type_value_id=rtvCity.parent_id','LEFT');
        $this->db->JOIN('reference_type_value rtvCountry','rtvCountry.reference_type_value_id=rtvState.parent_id','LEFT');
        $this->db->where("b.is_active",1);
        $this->db->where("b.branch_id",$params['branchId']);
        $query=$this->db->get();
        if($query) {
            if ($query->num_rows() > 0) {
                $resp = $query->result();
                $cityName=NULL;
                $stateName=NULL;
                $countryName=NULL;
                foreach($resp as $k_row=>$v_row){
                    $cityName=$v_row->cityName;
                    $stateName=$v_row->stateName;
                    $countryName=$v_row->countryName;
                }


                if($params['Leadcontacttype']=='Corporate'){
                    $params['LeadContactInstitution']=NULL;
                }
                elseif($params['Leadcontacttype']=='University'){
                    $params['LeadContactCompany']=NULL;
                }
                else{
                    $params['LeadContactInstitution']=NULL;
                    $params['LeadContactCompany']=NULL;
                }
                $this->db->query('SET @msg=NULL');
                $this->db->query('SET @contact_id=NULL');
                $storeproc = 'CALL `Sp_Add_Edit_Lead`("' . $params['contactName'] . '", "' . $params['contactPhone'] . '", "' . $params['contactEmail'] . '", "'.$cityName.'", "'.$countryName.'", "'.$params['leadTags'].'", @contact_id, NULL, NULL, @msg,"'.$params['LeadSource'].'", "'.$params['contactCourseText'].'","'.$params['contactDescription'].'","'.$params['LeadContactQualification'].'","'.$params['LeadContactCompany'].'","'.$params['LeadContactInstitution'].'",'.$params['branchId'].','.$params['userId'].',"'.$params['Leadcontacttype'].'",NULL,"","","'.$params['LeadContactPostQualification'].'")';
                $res_insert = $this->db->query($storeproc);
                if ($res_insert) {
                    $query_response = $this->db->query("SELECT @msg as message");
                    $query_response_result = $query_response->result();
                    $message = 'Lead added successfully';
                    $status = true;
                    foreach ($query_response_result as $klog => $vlog) {
                        if ($vlog->message != '') {
                            $message = $vlog->message;
                            $status = false;
                        }
                    }

                    $db_response = array("status" => $status, "message" => $message, "data" => array("message" => $message));
                } else {
                    $error = $this->db->error();
                    $db_response = array("status" => false, "message" => $error['message'], "data" => array());
                }
            }
            else{
                $db_response = array("status" => false, "message" => "Invalid details", "data" => array("message" => "Invalid details"));
            }
        }
        else{
            $error = $this->db->error();
            $db_response = array("status" => false, "message" => $error['message'], "data" => array());
        }
        return $db_response;

    }
    public function BulkLeads($param){

        $this->load->helper('email');
        $this->db->trans_start();
        try{
            $fileName=$param['file_name'];
            $content=$param['Rows'];
            $data_mt_insert=array("file_name"=>$fileName,"file_type"=>1,"created_date"=>date('Y-m-d H:i:s'),"fk_created_by"=>$_SERVER['HTTP_USER'],"fk_import_type_id"=>59);
            $res_insert_mt=$this->db->insert('import_history', $data_mt_insert);
            if($res_insert_mt){

                $master_id=$this->db->insert_id();
                $rows=array();
                $row_number=0;
                foreach($content as $k=>$v){
                    $row_number=$row_number+1;    
                    if(isset($v['Name']) && trim($v['Name'])!=''){

                    }
                    else{
                        $v['Name']=NULL;
                    }
                    if(isset($v['Email']) && trim($v['Email'])!=''){

                    }
                    else{
                        $v['Email']=NULL;
                    }
                    if(isset($v['Phone']) && trim($v['Phone'])!=''){

                    }
                    else{
                        $v['Phone']=NULL;
                    }
                    if(isset($v['Country']) && trim($v['Country'])!=''){

                    }
                    else{
                        $v['Country']=NULL;
                    }
                    if(isset($v['City']) && trim($v['City'])!=''){

                    }
                    else{
                        $v['City']=NULL;
                    }
                    if(isset($v['Tags']) && trim($v['Tags'])!=''){

                    }
                    else{
                        $v['Tags']=NULL;
                    }
                    if(isset($v['Source']) && trim($v['Source'])!=''){

                    }
                    else{
                        $v['Source']=NULL;
                    }
                    if(isset($v['Course']) && trim($v['Course'])!=''){

                    }
                    else{
                        $v['Course']=NULL;
                    }
                    if(isset($v['Description']) && trim($v['Description'])!=''){

                    }
                    else{
                        $v['Description']=NULL;
                    }
                    if(isset($v['Qualification']) && trim($v['Qualification'])!=''){

                    }
                    else{
                        $v['Qualification']=NULL;
                    }
                    if(isset($v['PostQualification']) && trim($v['PostQualification'])!=''){

                    }
                    else{
                        $v['PostQualification']=NULL;
                    }
                    if(isset($v['Company']) && trim($v['Company'])!=''){

                    }
                    else{
                        $v['Company']=NULL;
                    }
                    if(isset($v['Institution']) && trim($v['Institution'])!=''){

                    }
                    else{
                        $v['Institution']=NULL;
                    }
                    if(isset($v['ContactType']) && trim($v['ContactType'])!=''){

                    }
                    else{
                        $v['ContactType']=NULL;
                    }
                    if(isset($v['AlternatePhone']) && trim($v['AlternatePhone'])!=''){

                    }
                    else{
                        $v['AlternatePhone']=NULL;
                    }
                    $delimiters=array(",","|","/",";");
                    $primary_email = '';
                    $secondary_email='';
                    $primary_phone = '';
                    $secondary_phone='';
                    $data_insert=array();
                    if($v['Email']!='') {
                        $email_multi_explode = explodeX($delimiters, $v['Email']);
                        if(isset($email_multi_explode[0])) {
                            $primary_email = $email_multi_explode[0];
                            unset($email_multi_explode[0]);
                            $secondary_email=implode(',',$email_multi_explode);
                        }
                        else {
                            $primary_email = '';
                            $secondary_email='';
                        }
                    }
                    if($v['Phone']!='') {
                        $phone_multi_explode = explodeX($delimiters, $v['Phone']);
                        if(isset($phone_multi_explode[0])) {
                            $primary_phone = $phone_multi_explode[0];
                            unset($phone_multi_explode[0]);
                            $secondary_phone=implode(',',$phone_multi_explode);
                        }
                        else {
                            $primary_phone = '';
                            $secondary_phone='';
                        }

                    }
                    if($secondary_email!='') {
                        $data_insert['secondary_email'] = $secondary_email;
                    }
                    else{
                        $data_insert['secondary_email']=NULL;
                    }
                    if($secondary_phone!='') {
                        $data_insert['secondary_phone'] = $secondary_phone;
                    }
                    else{
                        $data_insert['secondary_phone']=NULL;
                    }
                    $data_insert['fk_import_history_id']=$master_id;
                    $data_insert['name']=$v['Name'];
                    $data_insert['email']=$primary_email;
                    $data_insert['phone']=$primary_phone;
                    $data_insert['country']=$v['Country'];
                    $data_insert['city']=$v['City'];
                    $data_insert['tags']=$v['Tags'];
                    $data_insert['contact_source']=$v['Source'];
                    $data_insert['course']=$v['Course'];
                    $data_insert['description']=$v['Description'];
                    $data_insert['qualification']=$v['Qualification'];
                    $data_insert['post_qualification']=$v['PostQualification'];
                    $data_insert['company']=$v['Company'];
                    $data_insert['institution']=$v['Institution'];
                    $data_insert['fk_branch_id']=$param['branchId'];
                    $data_insert['fk_user_id']=$param['userId'];
                    $data_insert['contact_type']=$v['ContactType'];
                    $data_insert['alternate_mobile']=$v['AlternatePhone'];
                    $data_insert['row_number']=$row_number;
                    $rows[]=$data_insert;


                }
                $res_insert_rows=$this->db->insert_batch('lead_import_item', $rows);
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else{
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                $this->db->trans_rollback();
            }
            else
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array("uploadId"=>$master_id));
                $this->db->trans_commit();
            }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>$ex->getMessage(),"data"=>array());
        }

        return $db_response;


    }
    function SaveBulkLeads($file_upload_id){

        //$this->db->trans_start();
        $success_count=0;
        $falied_count=0;
        $total_count=0;
        $duplicates_count=0;
        try{
            $this->db->select('lead_import_item_id,name,email,phone,country,city,tags,status,contact_source');
            $this->db->from('lead_import_item');
            $this->db->where('fk_import_history_id',$file_upload_id);
            $query = $this->db->get();

            if($query){
                $total_count=$query->num_rows();
                if ($total_count > 0)
                {

                    $result=$query->result();
                    $this->db->query("CALL Sp_Import_Lead($file_upload_id)");
                    $this->db->close();
                    $this->load->database();
                }
                else{
                    $db_response=array("status"=>false,"message"=>'No Records Found',"data"=>array());
                }
            }
            else{
                $error = $this->db->error();
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>$ex->getMessage(),"data"=>array());
        }

        if ($this->db->trans_status() === FALSE)
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            //$this->db->trans_rollback();
        }
        else
        {

            $this->db->select('count(lead_import_item_id) as totalCnt,sum(if(status=1,1,0)) as successCnt,sum(if(status=2 && error_log like "%Lead already exists%",1,0)) as duplicateCnt,sum(if(status=2,1,0)) as failureCnt');
            $this->db->from('lead_import_item');
            $this->db->where('fk_import_history_id',$file_upload_id);
            $countsQuery=$this->db->get();
            $countsQueryRes=$countsQuery->result();
            foreach($countsQueryRes as $kCounts=>$vCounts){
                $total_count=$vCounts->totalCnt;
                $success_count=$vCounts->successCnt;
                $failed_count=$vCounts->failureCnt;
                $duplicates_count=$vCounts->duplicateCnt;

            }

            $finalresp['Counts']=array("total_count"=>$total_count,"successCount"=>$success_count,"failedCount"=>($failed_count-$duplicates_count),"duplicateCount"=>$duplicates_count);
            $this->db->select('lead_import_item_id,name,status,error_log,row_number');
            $this->db->from('lead_import_item');
            $this->db->where('fk_import_history_id',$file_upload_id);
            $query = $this->db->get();
            $finalresp['Records']=$query->result();
            $db_response=array("status"=>true,"message"=>'success',"data"=>$finalresp);
            //$this->db->trans_commit();
        }
        return $db_response;
    }
    function releaseDueIfExists($leadId=''){
        //{"status":true,"message":"success","data":{"branchId":"1","releaseCnt":"10","releaseBy":"1","assignedUserId":"9"}}
        $status=TRUE;$message='success';$data=array();
        $cnt=0;
        $tobededucted=array();

        //$availableTotal="select  count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='".$param['branchId']."') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." ";
        $availableTotal="select  count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where  l.status=1 and l.lead_id=uxl.lead_id and uxl.lead_id=$leadId";
        $resTotal=$this->db->query($availableTotal);
        if($resTotal)
        {
            $release=0;
            $resultTotal = $resTotal->result();
            foreach ($resultTotal as $kTotal => $vTotal)
            {
                $release = $vTotal->availCnt;
            }
            if($release>0)
            {
                //$available = "select  uxl.call_schedule_head_id,count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='" . $param['branchId'] . "') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " group by uxl.call_schedule_head_id";
                $available = "select  uxl.call_schedule_head_id,count(l.lead_id) as availCnt from lead l,user_xref_lead_due uxl where  l.status=1 and l.lead_id=uxl.lead_id and uxl.lead_id=" . $leadId . " group by uxl.call_schedule_head_id";
                $res = $this->db->query($available);
                if ($res)
                {
                    $result = $res->result();
                    foreach ($result as $k => $v)
                    {
                        $headId = $v->call_schedule_head_id;
                        $availCnt = $v->availCnt;
                        if ($availCnt < $release)
                        {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $availCnt);
                            $release = $release - $availCnt;
                        }
                        elseif ($availCnt > $release)
                        {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                            $release = $release - $release;
                        }
                        elseif ($availCnt == $release)
                        {
                            $tobededucted[] = array("headId" => $headId, "releaseCount" => $release);
                            $release = $release - $release;
                        }
                        if ($release == 0)
                        {
                            break;
                        }
                    }
                    if (count($tobededucted) > 0)
                    {
                        for ($i = 0; $i < count($tobededucted); $i++)
                        {
                            $callHeadId = $tobededucted[$i]['headId'];
                            $releaseCount = $tobededucted[$i]['releaseCount'];
                            $data_release_head['release_count'] = $releaseCount;
                            $data_release_head['released_by'] = $_SERVER['HTTP_USER'];
                            $data_release_head['released_on'] = date('Y-m-d H:i:s');
                            $data_release_head['call_schedule_head_id'] = $callHeadId;
                            $insert_release_head = $this->db->insert('user_xref_lead_release_head', $data_release_head);
                            if ($insert_release_head)
                            {
                                //release head inserted successfully.
                                $release_head_id = $this->db->insert_id();
                                //$available="select  l.lead_id from lead l,branch b,user_xref_lead_due uxl where l.city_id=b.fk_city_id and b.branch_id=".$param['branchId']." and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=".$param['assignedUserId']." and uxl.call_schedule_head_id=".$callHeadId." order by l.lead_id asc limit ".$releaseCount;
                                //$available = "select  l.lead_id from lead l,user_xref_lead_due uxl where l.city_id IN (SELECT fk_city_id FROM branch_xref_city b where is_primary=1 and fk_branch_id='" . $param['branchId'] . "') and l.status=1 and l.lead_id=uxl.lead_id and uxl.user_id=" . $param['assignedUserId'] . " and uxl.call_schedule_head_id=" . $callHeadId . " order by l.lead_id asc limit " . $releaseCount;
                                $available = "select  l.lead_id,uxl.user_id from lead l,user_xref_lead_due uxl where l.status=1 and l.lead_id=uxl.lead_id and uxl.lead_id=" . $leadId . " and uxl.call_schedule_head_id=" . $callHeadId . " order by l.lead_id asc limit " . $releaseCount;
                                $select_lead_ids = $this->db->query($available);
                                if ($select_lead_ids)
                                {
                                    //leads fetched successfully
                                    $result_sel_leadIds = $select_lead_ids->result();
                                    $data_insert_releaseCalls = '';
                                    $data_release_leads = '';
                                    foreach ($result_sel_leadIds as $result_sel_leadIds_k => $result_sel_leadIds_v) {
                                        $data_insert_releaseCalls[] = array("user_id" => $result_sel_leadIds_v->user_id, "lead_id" => $result_sel_leadIds_v->lead_id, "head_id" => $callHeadId, "release_head_id" => $release_head_id);
                                        $data_release_leads[] = $result_sel_leadIds_v->lead_id;
                                    }
                                    if ($data_insert_releaseCalls != '') {
                                        //release calls found
                                        $insert_release_calls = $this->db->insert_batch('user_xref_lead_release', $data_insert_releaseCalls);
                                        if ($insert_release_calls) {
                                            //release calls successfully inserted
                                            $data_release_leads = array_map('intval', $data_release_leads);
                                            $data_release_leads = implode(",", $data_release_leads);
                                            /*$this->db->where_in('lead_id', $data_release_leads);
                                            $this->db->where('call_schedule_head_id', $callHeadId);
                                            $this->db->where('user_id', $param['assignedUserId']);
                                            $delete_due_calls=$this->db->delete('user_xref_lead_due');*/
                                            $delete_due_calls_query = "DELETE FROM user_xref_lead_due WHERE call_schedule_head_id=$callHeadId  AND lead_id in ($data_release_leads)";
                                            $delete_due_calls = $this->db->query($delete_due_calls_query);
                                            if ($delete_due_calls) {
                                                //calls deleted successfully
                                                $this->db->where('call_schedule_head_id', $callHeadId);
                                                $this->db->set('total_scheduled_calls', 'total_scheduled_calls-' . $releaseCount, FALSE);
                                                $update_call_head = $this->db->update('call_schedule_head');
                                                if ($update_call_head) {
                                                    //successfully updated call head counts
                                                    $status = TRUE;
                                                    $message = 'Released successfully';
                                                    $data = array();

                                                } else {
                                                    //problem in updating count of call head
                                                    $error = $this->db->error();
                                                    $status = FALSE;
                                                    $message = 'problem in updating count of call head';
                                                    $data = array("message" => $error['message']);
                                                }
                                            } else {
                                                //problem in deleting calls from dues
                                                $error = $this->db->error();
                                                $status = FALSE;
                                                $message = 'problem in deleting calls from dues';
                                                $data = array("message" => $error['message']);
                                            }

                                        } else {
                                            //problem in batch insert of release calls
                                            $error = $this->db->error();
                                            $status = FALSE;
                                            $message = 'problem in batch insert of release calls';
                                            $data = array("message" => $error['message']);
                                        }
                                    } else {
                                        //no release calls found
                                        $error = $this->db->error();
                                        $status = FALSE;
                                        $message = 'no release calls found';
                                        $data = array("message" => $error['message']);
                                    }
                                } else {
                                    //problem in fetching leads for given head
                                    $error = $this->db->error();
                                    $status = FALSE;
                                    $message = 'problem in fetching leads for given head';
                                    $data = array("message" => $error['message']);
                                }

                            } else {
                                //problem in insertion of relese head id
                                $error = $this->db->error();
                                $status = FALSE;
                                $message = 'problem in insertion of release head id';
                                $data = array("message" => $error['message']);
                            }
                        }
                        $multi_db_response[] = array("status" => $status, "message" => $message, "data" => $data);
                    } else {
                        //no availabled heads found
                        $error = $this->db->error();
                        $status = FALSE;
                        $message = 'no available heads found';
                        $data = array("message" => $error['message']);
                    }
                } else
                {
                    //problem in fetching the available lead heads
                    $error = $this->db->error();
                    $status = FALSE;
                    $message = 'problem in fetching the available lead heads';
                    $data = array("message" => $error['message']);
                }
            }
            else{
                $status = FALSE;
                $message = 'No releases found';
                $data = array("message" => 'No releases found');
            }
        }
        else
        {
            $error = $this->db->error();
            $status = FALSE;
            $message = 'problem in fetching the total available leads';
            $data = array("message" => $error['message']);
        }
        $message="Released successfully";
        $multi_db_response=array("message"=>$message);
        $db_response=array("status"=>TRUE,"message"=>$message,"data"=>$multi_db_response);
        return $db_response;
    }
    function saveTagOutStationContact($params){
        $this->db->db_debug = FALSE;
        $branchId=$params['branchId'];
        $contactId=array();
        if($params['type']==1){
            $contactId=$params['contactId'];
        }
        if($params['type']==0){

            $getOutStations_query="select l.lead_id from lead l where ( l.city_id not in (select DISTINCT(bc.fk_city_id) from branch b, branch_xref_city bc where b.branch_id=bc.fk_branch_id and bc.is_primary=1) AND l.fk_branch_id is NULL)";
            $res_query=$this->db->query($getOutStations_query);

            if($res_query){
                if($res_query->num_rows()>0){
                    $row_query=$res_query->result();
                    foreach($row_query as $k=>$v){
                        $contactId[]=$v->lead_id;
                    }
                }
            }
        }
        if(count($contactId)==0){
            $status=false;$message='No Out Station Contacts Found!';$data=array();
        }
        else {

            for ($i = 0; $i < count($contactId); $i++) {
                $data_update = array();
                $ind_contactId = $contactId[$i];
                $this->releaseDueIfExists($ind_contactId);
                $data_update['fk_branch_id'] = $branchId;
                $this->db->where('lead_id', $ind_contactId);
                $this->db->update('lead', $data_update);

            }
            $status=true;$message='Contacts Tagged Successfully';$data=array();
        }
        return array("status"=>$status,"message"=>$message,"data"=>$data);
    }
    
    public function GetLeadStages() {
        $this->db->select('lead_stage_id, lead_stage, description');
        $this->db->from('lead_stage');
        $this->db->where("`is_current` = 1");
        $this->db->order_by("order","ASC");
        
        $query = $this->db->get();
        if($query){
            $result = $query->result();
            if(count($result) > 0){
                return $result;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }
    function getDNDCountConstant(){
        $this->db->select('value');
        $this->db->from('app_configuration_setting');
        $this->db->where('key','DND CONFIG');
        $res=$this->db->get();
        if($res){
            if($res->num_rows()>0){
                $row=$res->result();
                return $row[0]->value;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }
    function saveColdCall($params){
        $maxCount=$this->getDNDCountConstant();
        $userId=$params['userId'];
        $leadId=$params['leadId'];
        $this->releaseDueIfExists($leadId);
        $lead_status=$params['ColdCallStatus'];
        $description=NULL;
        $lead_name=$params['ColdCallContactName'];
        $lead_email=$params['ColdCallContactEmail'];
        $description=$params['ColdCallDescription'];
        $insert_into_branchLead=0;
        $lead_number=0;
        $this->db->select("l.name,l.email,l.fk_company_id,l.fk_qualification_id,l.fk_institution_id,l.is_dnd,l.fk_contact_type_id,l.logic_item_name,l.logic_item_count");
        $this->db->from('lead l');
        $this->db->where('l.lead_id',$leadId);
        $getLeadDetails=$this->db->get();
        $LeadDetails=$getLeadDetails->result();
        $logic_item_name=NULL;
        $logic_item_count=0;
        foreach($LeadDetails as $key=>$val){

            $headId=NULL;
            $lead_is_dnd=0;
            $fk_course_id=NULL;
            $fk_company_id=$val->fk_company_id;
            $fk_qualification_id=$val->fk_qualification_id;
            $fk_institution_id=$val->fk_institution_id;
            $contactType=$val->fk_contact_type_id;
            $logic_item_name=$val->logic_item_name;
            $logic_item_count=$val->logic_item_count;

        }
        if($params['ColdCallStatus']=='switchoff'){
            if('switchoff'==$logic_item_name){
                $logic_item_name='switchoff';
                $logic_item_count=$logic_item_count+1;
            }
            else{
                $logic_item_name='switchoff';
                $logic_item_count=1;
            }
            if($logic_item_count==$maxCount){
                $lead_is_dnd=1;
                $logic_item_name=NULL;
                $logic_item_count=0;
            }
        }
        if($params['ColdCallStatus']=='notlifted'){
            if('notlifted'==$logic_item_name){
                $logic_item_name='notlifted';
                $logic_item_count=$logic_item_count+1;
            }
            else{
                $logic_item_name='notlifted';
                $logic_item_count=1;
            }
            if($logic_item_count==$maxCount){
                $lead_is_dnd=1;
                $logic_item_name=NULL;
                $logic_item_count=0;
            }
        }
        if($params['ColdCallStatus']=='notexist'){
            if('notexist'==$logic_item_name){
                $logic_item_name='notexist';
                $logic_item_count=$logic_item_count+1;
            }
            else{
                $logic_item_name='notexist';
                $logic_item_count=1;
            }
            if($logic_item_count==$maxCount){
                $lead_is_dnd=1;
                $logic_item_name=NULL;
                $logic_item_count=0;
            }
        }

        if($params['ColdCallStatus']=='answered'){

            if($params['ColdcallStatusInfo']=='callStatusInfoIntrestedTransfer' || $params['ColdcallStatusInfo']=='callStatusInfoIntrested'){
                $logic_item_name=NULL;
                $logic_item_count=0;
                $fk_course_id=$params['ColdCallContactCourse'];
                $branch_id=$params['ColdCallContactBranch'];
                $insert_into_branchLead=1;

                $this->db->select('lead_stage_id,ifnull(call_after_days,0) as call_after_days');
                $this->db->from('lead_stage');
                $this->db->where('lead_stage','M3');
                $query_leadStage=$this->db->get();
                $leadStageDetails=$query_leadStage->result();
                $next_followup_date=date('Y-m-d H:i:s',strtotime(' +1 day'));
                foreach($leadStageDetails as $k_leadStage=>$v_leadStage){
                    $lead_stage=$v_leadStage->lead_stage_id;
                    $next_followup_date=date('Y-m-d H:i:s',strtotime(' +'.$v_leadStage->call_after_days.' day'));
                }

                $fk_qualification_id=$params['ColdCallContactQualification'];
                $getSequenceNumber=1;
                $this->db->select('IFNULL((MAX(lead_number)+1),0) as lead_number');
                $this->db->from('branch_xref_lead');
                $this->db->where('branch_id',$branch_id);
                $get_max_leadNumber=$this->db->get();

                $final_lead_number='';
                if($get_max_leadNumber){
                    if ($get_max_leadNumber->num_rows() > 0){
                        $select_max_leadNumber=$get_max_leadNumber->result();

                        foreach($select_max_leadNumber as $k_lead_number=>$v_lead_number){

                            $final_lead_number=$v_lead_number->lead_number;
                            if($final_lead_number!=0){
                                $getSequenceNumber=0;
                            }

                        }
                    }
                    else{

                    }

                }
                else{

                }
                if($getSequenceNumber==1){
                    $final_lead_number=0;
                    $this->db->select('sequence_number');
                    $this->db->from('branch');
                    $this->db->where('branch_id',$branch_id);
                    $get_max_leadNumber=$this->db->get();
                    if($get_max_leadNumber){
                        if ($get_max_leadNumber->num_rows() > 0){
                            $select_max_leadNumber=$get_max_leadNumber->result();

                            foreach($select_max_leadNumber as $k_lead_number=>$v_lead_number){

                                $final_lead_number=$v_lead_number->sequence_number+1;


                            }
                        }
                        else{

                        }

                    }
                    else{

                    }
                }



            }
            if($params['ColdcallStatusInfo']=='callStatusInfoNotintrested'){
                //$description=$params['ColdCallDescription'];

                if('callStatusInfoNotintrested'==$logic_item_name){
                    $logic_item_name='callStatusInfoNotintrested';
                    $logic_item_count=$logic_item_count+1;
                }
                else{
                    $logic_item_name='callStatusInfoNotintrested';
                    $logic_item_count=1;
                }
                if($logic_item_count==$maxCount){
                    $lead_is_dnd=1;
                    $logic_item_name=NULL;
                    $logic_item_count=0;
                }


            }
            if($params['ColdcallStatusInfo']=='callStatusInfoDND'){

                if('callStatusInfoDND'==$logic_item_name){
                    $logic_item_name='callStatusInfoDND';
                    $logic_item_count=$logic_item_count+1;
                }
                else{
                    $logic_item_name='callStatusInfoDND';
                    $logic_item_count=1;
                }
                if($logic_item_count==$maxCount){
                    $lead_is_dnd=1;
                    $logic_item_name=NULL;
                    $logic_item_count=0;
                }

            }
        }
        if($insert_into_branchLead==1){
            if($params['coldCallcontacttype']=='Corporate'){
                $fk_company_id=$params['ColdCallContactCompany'];
            }
            if($params['coldCallcontacttype']=='University'){
                $fk_institution_id=$params['ColdCallContactInstitution'];
            }
            if($params['coldCallcontacttype']=='Retail'){

            }
            $contactType=NULL;
            $this->db->select('reference_type_value_id');
            $this->db->from('reference_type_value rtv');
            $this->db->JOIN('reference_type rt','rt.reference_type_id=rtv.reference_type_id');
            $this->db->where('rt.name','Contact Type');
            $this->db->where('rtv.value',$params['coldCallcontacttype']);
            $getContactType=$this->db->get();
            $tem=$this->db->last_query();
            if($getContactType){
                if($getContactType->num_rows()>0){
                    $getContactTypeRes=$getContactType->result();
                    foreach($getContactTypeRes as $kr=>$vr){
                        $contactType=$vr->reference_type_value_id;
                    }
                }
            }
        }

        $data_update_leadDetails=array("name"=>$lead_name,"email"=>$lead_email,"is_dnd"=>$lead_is_dnd,"fk_company_id"=>$fk_company_id,"fk_qualification_id"=>$fk_qualification_id,"fk_institution_id"=>$fk_institution_id,"fk_contact_type_id"=>$contactType,"logic_item_name"=>$logic_item_name,"logic_item_count"=>$logic_item_count);

        $this->db->where('lead_id',$leadId);
        $res_update_leadDetails=$this->db->update('lead',$data_update_leadDetails);




        $history_id=NULL;
        if($insert_into_branchLead==1) {
            $data_branch_xref_lead_insert = array("lead_id" => $leadId, "branch_id" => $branch_id,"fk_course_id"=>$fk_course_id, "status" => $lead_stage, "created_by" => $userId, "created_on" => date('Y-m-d H:i:s'), "is_active" => 1, "lead_number" => $final_lead_number,"fk_user_xref_lead_history_id"=>$history_id,"next_followup_date"=>$next_followup_date);
            $res_branch_xref_lead_insert=$this->db->insert('branch_xref_lead',$data_branch_xref_lead_insert);
            $branch_xref_lead_id=$this->db->insert_id();

            $this->db->select('value');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_value_id',$fk_qualification_id);
            $get_qualification=$this->db->get();
            if($get_qualification){
                if($get_qualification->num_rows()>0){
                    $get_qualification_row=$get_qualification->result();
                    $data_insert_education_info=array('lead_id'=>$leadId,"course"=>$get_qualification_row[0]->value,"fk_course_id"=>$fk_qualification_id,"created_on"=>date('Y-m-d H:i:s'),"created_by"=>$userId);
                    $res_branch_xref_lead_insert=$this->db->insert('lead_educational_info',$data_insert_education_info);
                }

            }

        }
        else{
            $branch_xref_lead_id=NULL;
        }
        $data_insert_timeline=array('lead_id'=>$leadId,'branch_xref_lead_id'=>$branch_xref_lead_id,"type"=>"Before Lead Conversion","description"=>$description,"created_by"=>$_SERVER['HTTP_USER'],"created_on"=>date('Y-m-d H:i:s'));
        $res_insert_timeline=$this->db->insert('timeline',$data_insert_timeline);
        $db_response=array("status"=>true,"message"=>"success","data"=>array());
        return $db_response;


    }

    function addGuestLead($params){
        //print_r($params);exit;
        $this->db->select("b.name as branchName,b.fk_city_id,rtvCity.value cityName,rtvState.value stateName,rtvCountry.value countryName");
        $this->db->from("branch b");
        $this->db->JOIN('reference_type_value rtvCity','rtvCity.reference_type_value_id=b.fk_city_id','LEFT');
        $this->db->JOIN('reference_type_value rtvState','rtvState.reference_type_value_id=rtvCity.parent_id','LEFT');
        $this->db->JOIN('reference_type_value rtvCountry','rtvCountry.reference_type_value_id=rtvState.parent_id','LEFT');
        $this->db->where("b.is_active",1);
        $this->db->where("b.branch_id",$params['branchId']);
        $query=$this->db->get();
        if($query) {
            if ($query->num_rows() > 0) {
                $resp = $query->result();
                $cityName=NULL;
                $stateName=NULL;
                $countryName=NULL;
                foreach($resp as $k_row=>$v_row){
                    $cityName=$v_row->cityName;
                    $stateName=$v_row->stateName;
                    $countryName=$v_row->countryName;
                }

                $this->db->query('SET @msg=NULL');
                $this->db->query('SET @contact_id=NULL');
                $storeproc = 'CALL `Sp_Add_Edit_Lead`("' . $params['contactName'] . '", "' . $params['contactPhone'] . '", "' . $params['contactEmail'] . '", "'.$cityName.'", "'.$countryName.'", NULL, @contact_id, NULL, NULL, @msg,NULL, "'.$params['contactCourseText'].'","'.$params['contactDescription'].'",NULL,NULL,NULL,'.$params['branchId'].','.$params['userId'].',NULL,NULL,"","",NULL)';
                $res_insert = $this->db->query($storeproc);
                if ($res_insert)
                {
                    $lead_query_response = $this->db->query("SELECT @contact_id as lead_id");
                    $lead_response_result = $lead_query_response->result();
                    $query_response = $this->db->query("SELECT @msg as message");
                    $query_response_result = $query_response->result();
                    $lead_id=$lead_response_result[0]->lead_id;
                    $this->db->select('branch_xref_lead_id,lead_number');
                    $this->db->from('branch_xref_lead');
                    $this->db->where('lead_id',$lead_id);
                    $get_branch_lead_id=$this->db->get();
                    $branch_lead_resp = $get_branch_lead_id->result();
                    $branch_xref_lead_id=$branch_lead_resp[0]->branch_xref_lead_id;
                    $lead_number=$branch_lead_resp[0]->lead_number;
                    $second_level_Id=$params['counselorId'];

                    $data['lead_id']=$lead_number;
                    $data['branch']=$params['branchId'];
                    $data['user']=$params['counselorId'];
                    $data['visitedDate']=date('Y-m-d H:i:s');
                    $data['hdnBranchVisitId'] = 0;
                    $data['common_date']=$params['chosenDate'];
                    $data['CommentType']=$params['CommentType'];
                    $data['lead_stage']=$params['leadStage'];
                    $data['dateType']=$params['dateType'];
                    $data['currentLeadStage']='M3';
                    $data['branchVisitComments']=$params['contactDescription'];

                    $lead_conversion=$this->ConvertToLeadBranchVisitor($data);
                    $this->db->select("bxl.branch_xref_lead_id,l.`name` as lead_name,CONCAT_WS('|',l.name,CONCAT_WS('',b.code,ba.code,bxl.lead_number),l.email,l.phone) as text,CONCAT_WS('|',bxl.branch_xref_lead_id,CONCAT_WS('',b.code,ba.code,bxl.lead_number),l.name) as id,CONCAT_WS('|',CONCAT_WS('',b.code,".$params['batchCode'].",bxl.lead_number)) as enroll");
                    $this->db->from('branch_xref_lead bxl');
                    $this->db->join('lead l', 'bxl.lead_id=l.lead_id');
                    $this->db->join('branch b', 'bxl.branch_id=b.branch_id');
                    $this->db->join('batch ba', 'bxl.fk_batch_id=ba.batch_id','left');
                    $this->db->join('batch_xref_class bxc', 'ba.batch_id=bxc.fk_batch_id','left');
                    $this->db->where('bxl.lead_id', $lead_id);
                    $leadData = $this->db->get();
                    if($leadData)
                    {
                        if($leadData->num_rows()>0)
                        {
                            $lead_data=$leadData->result();
                        }
                    }
                    $message = 'Lead added successfully';
                    $status = true;
                    foreach ($query_response_result as $klog => $vlog) {
                        if ($vlog->message != '') {
                            $message = $vlog->message;
                            $status = false;
                        }
                    }
                    $db_response = array("status" => $status, "message" => $message, "data" => $lead_data);
                }
                else
                {
                    $error = $this->db->error();
                    $db_response = array("status" => false, "message" => $error['message'], "data" => array());
                }
            }
            else
            {
                $db_response = array("status" => false, "message" => "Invalid details", "data" => array("message" => "Invalid details"));
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response = array("status" => false, "message" => $error['message'], "data" => array());
        }
        return $db_response;
    }

    public function ConvertToLeadBranchVisitor($data)
    {
        $error = false;
        $this->db->select('bxl.branch_xref_lead_id , bxl.lead_id ,l.name');
        $this->db->from('branch_xref_lead bxl');
        $this->db->join('lead l', 'bxl.lead_id=l.lead_id');
        $this->db->where('bxl.lead_number', $data['lead_id']);
        $this->db->where('bxl.branch_id', $data['branch']);
        $leadData = $this->db->get();
        if ($leadData)
        {
            $leadData = $leadData->result();
            if (count($leadData) > 0)
            {
                $error = false;
            }
            else
            {
                $error = true;
                return 'nolead';
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        $this->db->select('u.user_id, e.name');
        $this->db->from('user u');
        $this->db->join('employee e', 'u.user_id=e.user_id');
        $this->db->where('u.user_id', $data['user']);
        $this->db->where('u.is_active', 1);
        $this->db->where('u.is_seconday_counsellor', 1);
        $user = $this->db->get();
        if ($user)
        {
            $user = $user->result();
            if (count($user) > 0)
            {
                $error = false;
            }
            else
            {
                $error = true;
                return 'nouser';
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        $branch_lead_id = $leadData[0]->branch_xref_lead_id;
        $lead_id = $leadData[0]->lead_id;
        if ($data['hdnBranchVisitId'] > 0) {
            $this->db->select('second_level_counsellor_id');
            $this->db->from('branch_visitor');
            $this->db->where('status', 1);
            $this->db->where('branch_visitor_id', $data['hdnBranchVisitId']);
            $result = $this->db->get();
            if ($result) {
                $result = $result->result();
                if (count($result) > 0) {
                    $exist_counseler_id = $result[0]->second_level_counsellor_id;
                } else {
                    $exist_counseler_id = 0;
                }
            } else {
                $exist_counseler_id = 0;
            }
        }
        else{
            $exist_counseler_id = 0;
        }
        if($data['user']!=$exist_counseler_id)
        {
            $this->db->select('branch_visitor_id');
            $this->db->from('branch_visitor');
            $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
            $this->db->where('second_level_counsellor_id', $data['user']);
            $this->db->where('status', 1);
            $result = $this->db->get();
            if ($result) {
                $result = $result->result();
                if (count($result) > 0) {
                    $error = true;
                    $this->db->select('branch_visitor_id');
                    $this->db->from('branch_visitor');
                    $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
                    $this->db->where('second_level_counsellor_id', $data['user']);
                    $this->db->where('status', 1);
                    $this->db->where('date(branch_visited_date)="' . date('Y-m-d', strtotime($data['visitedDate'])) . '"');
                    $result = $this->db->get();
                    if ($result) {
                        $result = $result->result();
                        if (count($result) > 0) {
                            $error = true;
                            return 'alreadyDate';
                        } else {
                            $error = false;
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                } else {
                    $error = false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        if ($data['hdnBranchVisitId'] == 0) {
            $this->db->select('branch_visitor_id');
            $this->db->from('branch_visitor');
            $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
            $this->db->where('status', 1);
            $this->db->where('date(branch_visited_date)="' . date('Y-m-d', strtotime($data['visitedDate'])) . '"');
            $result = $this->db->get();
            if ($result) {
                $result = $result->result();
                if (count($result) > 0) {
                    $error = true;
                    return 'alreadyDate';
                } else {
                    $error = false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        if ($data['hdnBranchVisitId'] == 0) {
            $this->db->select('fk_branch_xref_lead_id');
            $this->db->from('branch_visitor');
            $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
            $this->db->where('date(branch_visited_date)="' . date('Y-m-d', strtotime($data['visitedDate'])) . '"');
            $this->db->where('status', 1);

            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
                    $error = true;
                    return 'duplicate';
                } else {
                    $error = false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }

        if(!$error){
            $this->db->trans_start();
            if ($data['hdnBranchVisitId'] > 0) {
                $data_update =array(
                    'fk_branch_xref_lead_id' =>$branch_lead_id,
                    'second_level_counsellor_id' =>$data['user'],
                    'created_by' =>$_SERVER['HTTP_USER'],
                    'created_date' =>date('Y-m-d H:i:s'),
                    'branch_visited_date' =>date('Y-m-d H:i:s',strtotime($data['visitedDate'])),
                    'branch_visit_comments' => $data['branchVisitComments']
                );
                $this->db->where('fk_branch_xref_lead_id', $branch_lead_id);
                $this->db->where('branch_visitor_id', $data['hdnBranchVisitId']);
                $this->db->update('branch_visitor', $data_update);
            } else {
                $data_insert =array(
                    'fk_branch_xref_lead_id' =>$branch_lead_id,
                    'second_level_counsellor_id' =>$data['user'],
                    'created_by' =>$_SERVER['HTTP_USER'],
                    'created_date' =>date('Y-m-d H:i:s'),
                    'branch_visited_date' =>date('Y-m-d H:i:s',strtotime($data['visitedDate'])),
                    'branch_visit_comments' => $data['branchVisitComments'],
                    'comment_type'=>$data['CommentType']
                );
                $this->db->insert('branch_visitor', $data_insert);
            }
            $data_insert =array(
                'lead_id' =>$lead_id,
                'branch_xref_lead_id' =>$branch_lead_id,
                'type' =>'Allotted Counsellor',
                'description' =>$data['branchVisitComments'].', Allotted '.$leadData[0]->name.' to '.$user[0]->name,
                'created_by' =>$_SERVER['HTTP_USER'],
                'created_on' =>date('Y-m-d H:i:s'),
            );
            $this->db->insert('timeline', $data_insert);
            $data['branchXrefLeadId']=$branch_lead_id;
            $changeStatus=$this->saveBranchComments($data);
            if($changeStatus['status']===false){
                return array('error' => 'dberror', 'msg' => $changeStatus['status']);
            }
            //timeline



            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    function saveBranchComments($params)
    {
        $releaseDue=$this->releaseLeadIfExist($params['branchXrefLeadId']);
        $data_xref_lead_update=array();
        if($params['hdnBranchVisitId']==0){
            $comments='';
            $enrollDate='';
            $leadId=NULL;
            $this->db->select('lead_id,logic_item_name,logic_item_count');
            $this->db->from('branch_xref_lead');
            $this->db->where('branch_xref_lead_id', $params['branchXrefLeadId']);

            $res_all = $this->db->get();
            if ($res_all) {
                $res_all = $res_all->result();

                if (count($res_all) > 0) {
                    if (isset($res_all[0]->lead_id)) {
                        $maxCount=$this->getDNDCountConstant();
                        $leadDetails = $res_all[0];
                        $logic_item_name = $leadDetails->logic_item_name;
                        $logic_item_count = $leadDetails->logic_item_count;
                        $leadStage = $params['lead_stage'];
                        $leadStagePrefix = substr($leadStage, 0, 2);
                        if ($leadStagePrefix != '' && $leadStagePrefix == 'L_') {
                            $statusExplode = explode('_', $leadStage);
                            if ($leadStage == $logic_item_name) {
                                $logic_item_name = $logic_item_name;
                                $logic_item_count = $logic_item_count + 1;
                                $nextLeadStage = $params['currentLeadStage'];
                            } else {
                                $logic_item_name = $leadStage;
                                $logic_item_count = 1;
                                $nextLeadStage = $params['currentLeadStage'];
                            }

                            if ($maxCount == $logic_item_count) {
                                $logic_item_name = NULL;
                                $logic_item_count = 0;
                                $nextLeadStage = $statusExplode[2];
                            }

                        } else {
                            $nextLeadStage = $leadStage;
                            $logic_item_name = NULL;
                            $logic_item_count = 0;
                        }
                        if ($params['common_date'] != NULL) {
                            if ($params['dateType'] != '' && $params['dateType'] == 'Expected Visited Date'){
                                $data_xref_lead_update['expected_visit_date'] = $params['common_date'];
                            } else if($params['dateType'] != '' && $params['dateType'] == 'Expected Enrollment Date'){
                                $data_xref_lead_update['expected_enroll_date'] = $params['common_date'];
                            } else if($params['dateType'] != '' && $params['dateType'] == 'Expected Admission Date'){
                                $data_xref_lead_update['expected_admission_date'] = $params['common_date'];
                            } else if ($params['dateType'] != '') {
                                $data_xref_lead_update['next_followup_date'] = $params['common_date'];
                            } else {

                            }
                        }
                        $data_xref_lead_update['logic_item_name'] = $logic_item_name;
                        $data_xref_lead_update['logic_item_count'] = $logic_item_count;
                        $data_xref_lead_update['logic_item_name'] = $logic_item_name;
                        if($nextLeadStage!=''){

                            $this->db->select('lead_stage_id,ifnull(call_after_days,0) as call_after_days');
                            $this->db->from('lead_stage');
                            $this->db->where('lead_stage',$nextLeadStage);
                            $query_leadStage=$this->db->get();
                            $leadStageDetails=$query_leadStage->result();
                            foreach($leadStageDetails as $k_leadStage=>$v_leadStage){
                                $lead_stage=$v_leadStage->lead_stage_id;

                            }
                            $getLeadStageId=$lead_stage;
                            $data_xref_lead_update['status']=$getLeadStageId;
                            $data_xref_lead_update['updated_by']=$_SERVER['HTTP_USER'];
                            $data_xref_lead_update['updated_on']=date('Y-m-d H:i:s');
                            $this->db->where('branch_xref_lead_id',$params['branchXrefLeadId']);
                            $res1=$this->db->update('branch_xref_lead',$data_xref_lead_update);

                            if($res1){
                                $db_response=array('status'=>true,'message'=>'Success','data'=>array());
                            }
                            else{
                                $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>array());
                            }
                        }
                        else{
                            $db_response=array('status'=>false,'message'=>'Invalid status','data'=>array());
                        }
                    }
                    else{
                        $db_response=array('status'=>false,'message'=>'Invalid lead details','data'=>array());
                    }
                }
                else{
                    $db_response=array('status'=>false,'message'=>'Invalid lead details','data'=>array());
                }

            }
            else{
                $db_response=array('status'=>false,'message'=>'Something went wrong','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>true,'message'=>'Success','data'=>array());
        }
        return $db_response;
    }

    function releaseLeadIfExist($branchXrefLeadId){

        $this->db->select('fk_lead_call_schedule_head_id,fk_lead_id');
        $this->db->from('lead_user_xref_lead_due');
        $this->db->where('fk_branch_xref_lead_id', $branchXrefLeadId);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if (count($res_all) > 0) {
                //remove from lead_user_xref_lead_history, lead_user_xref_lead_release
                $data_insert =array(
                    'release_count' =>1,
                    'released_by' =>$_SERVER['HTTP_USER'],
                    'released_on' =>date('Y-m-d H:i:s'),
                    'fk_lead_call_schedule_head_id' =>$res_all[0]->fk_lead_call_schedule_head_id
                );
                $this->db->insert('lead_user_xref_lead_release_head', $data_insert);
                $releaseHead=$this->db->insert_id();
                $data_insert1 =array(
                    'fk_user_id' =>$_SERVER['HTTP_USER'],
                    'fk_branch_xref_lead_id' => $branchXrefLeadId,
                    'fk_lead_id' => $res_all[0]->fk_lead_id,
                    'fk_lead_user_xref_lead_release_head_id' => $releaseHead,
                    'fk_lead_call_schedule_head_id' =>$res_all[0]->fk_lead_call_schedule_head_id
                );
                $this->db->insert('lead_user_xref_lead_release', $data_insert1);

                $this->db->where('fk_branch_xref_lead_id',$branchXrefLeadId);
                $this->db->delete('lead_user_xref_lead_due');

            }

        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }

    }
}