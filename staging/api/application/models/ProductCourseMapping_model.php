<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-11
 * Time: 02:11 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProductCourseMapping_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
    }
    public function getProductsInformation($val)
    {
        $query = $this->db->query("SELECT code as id,CONCAT_WS(' | ',code,name) as text,name FROM product WHERE code like '%".$val."%' or name like '%".$val."%'");
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    public function getProductIdByCode($productCode = '')
    {
        $this->db->select('product_id');
        $this->db->from('product');
        $this->db->where('code',$productCode);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $data = $query->row();
            $productId = $data->product_id;
        }
        else{
            $productId = 0;
        }
        return $productId;
    }
    public function checkCourseMappingExist($courseId)
    {
        $this->db->select("product_xref_course_id");
        $this->db->from("product_xref_course");
        $this->db->where("fk_course_id", $courseId);
        $this->db->where("is_active", 0);
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'Course already mapped with products',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Course not mapped',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function addProductCourseMapping($inputData)
    {
        $courseId = $inputData['courseId'];
        $userId = $inputData['userID'];
        $mappings = explode(',',$inputData['productMappings']);
        $finalArray = array();
        $invalidProductCount = 0;
        for($m=0;$m<count($mappings);$m++)
        {
            $productMappings = explode('||',$mappings[$m]);
            $productId = $this->getProductIdByCode($productMappings[0]);
            /*if($productId != 0)
            {
                $finalArray [] =array('fk_product_id'=>$productId,'fk_course_id' => $courseId, 'quantity' => $productMappings[1],'created_by' => $userId);
            }
            else{
                //no product found
                $invalidProductCount = $invalidProductCount+1;
            }*/
            if($productId != 0)
            {
                $isDeleted = $this->getDeletedProductStatus($courseId,$productId);
                if($isDeleted === true)//match with deleted status
                {
                    //update quantity and status
                    $data = array(
                        'quantity' => $productMappings[1],
                        'is_active' => 0,
                        'updated_by' => $userId,
                        'updated_on' => date('Y-m-d H:i:s')
                    );
                    $this->db->where('fk_product_id',$productId);
                    $this->db->where('fk_course_id',$courseId);
                    $this->db->update('product_xref_course',$data);
                }
                else
                {//insert
                    $data = array(
                        'fk_product_id'=>$productId,
                        'fk_course_id'=>$courseId,
                        'quantity' => $productMappings[1],
                        'created_by' => $userId
                    );
                    $this->db->insert('product_xref_course',$data);
                }
            }
            else{
                //no product found
                $invalidProductCount = $invalidProductCount+1;
            }

        }
        /*if($invalidProductCount > 0)
        {
            $db_response=array("status"=>true,"message"=>'Invalid Product',"data"=>[]);
        }
        else{
            if(count($finalArray)>0)
            {
                $insertMapping = $this->db->insert_batch('product_xref_course',$finalArray);
                if($insertMapping)
                {
                    $db_response=array("status"=>true,"message"=>'Product mapped to course successfully',"data"=>[]);
                }
                else
                {
                    $error = $this->db->error();
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>[]);
                }
            }
            else{
                //no products for mapping
                $db_response=array("status"=>true,"message"=>'No products for mapping',"data"=>[]);
            }
        }*/
        if($invalidProductCount > 0)
        {
            $db_response=array("status"=>true,"message"=>'Invalid Product',"data"=>[]);
        }
        else
        {
            $db_response=array("status"=>true,"message"=>'Product mapped to course successfully',"data"=>[]);
        }
        return $db_response;
    }
    public function checkProductMapExist($params)
    {
        $this->db->select("product_xref_course_id");
        $this->db->from("product_xref_course");
        $this->db->where("fk_course_id", $params['productCourseId']);
        $this->db->where("is_active", 0);
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid Mapping',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getProductMappingDetailsById($data)
    {
        $this->db->select('pc.fk_course_id,pc.fk_product_id,pc.quantity,p.name as product_name,p.code,c.name');
        $this->db->from('product_xref_course pc');
        $this->db->join('product p','p.product_id=pc.fk_product_id','left');
        $this->db->join('course c','c.course_id=pc.fk_course_id','left');
        $this->db->where('pc.fk_course_id', $data['productCourseId']);
        $this->db->where('pc.is_active', 0);
        $list = $this->db->get();
        $list_final ['courseId'] = $data['productCourseId'];
        if($list)
        {
            $list = $list->result();
            if(count($list) > 0)
            {
                foreach($list as $val)
                {
                    $list_final ['name'] =  $val->name;
                }
                $list_final ['mappings'] = $list;
                $db_response=array("status"=>true,"message"=>"success","data"=>$list_final);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getProductsByCourse($courseId)
    {
        $this->db->select('fk_product_id');
        $this->db->from('product_xref_course');
        $this->db->where('fk_course_id',$courseId);
        $this->db->where('is_active',0);
        $query= $this->db->get();
        return $query->result_array();
    }
    public function getDeletedProductStatus($courseId,$productId)
    {
        $this->db->select('product_xref_course_id');
        $this->db->from('product_xref_course');
        $this->db->where('fk_course_id',$courseId);
        $this->db->where('fk_product_id',$productId);
        $this->db->where('is_active',1);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $deleteStatus = true;
        }
        else{
            $deleteStatus = false;
        }
        return $deleteStatus;
    }
    public function updateProductCourseMapping($inputData)
    {
        $courseId = $inputData['courseId'];
        $existingProducts = $this->getProductsByCourse($courseId);
        $existProductIds = array();
        foreach($existingProducts as $extProd)
        {
            $existProductIds[] = $extProd['fk_product_id'];
        }
        $mappings = explode(',',$inputData['productMappings']);
        $newProducts = array();
        $invalidProductCount = 0;
        for($m=0;$m<count($mappings);$m++)
        {
            $productMappings = explode('||',$mappings[$m]);
            $productId = $this->getProductIdByCode($productMappings[0]);
            if($productId != 0)
            {
                $newProducts []= $productId;
                if(in_array($productId,$existProductIds)){
                    //update quantity
                    $data = array(
                        'quantity' => $productMappings[1],
                        'updated_by' => $inputData['userID'],
                        'updated_on' => date('Y-m-d H:i:s')
                    );
                    $this->db->where('fk_product_id',$productId);
                    $this->db->where('fk_course_id',$courseId);
                    $this->db->update('product_xref_course',$data);
                }
                else{
                    //check with deleted status
                    $isDeleted = $this->getDeletedProductStatus($courseId,$productId);
                    if($isDeleted === true)//match with deleted status
                    {
                        //update quantity and status
                        $data = array(
                            'quantity' => $productMappings[1],
                            'is_active' => 0,
                            'updated_by' => $inputData['userID'],
                            'updated_on' => date('Y-m-d H:i:s')
                        );
                        $this->db->where('fk_product_id',$productId);
                        $this->db->where('fk_course_id',$courseId);
                        $this->db->update('product_xref_course',$data);
                    }
                    else
                    {//insert
                        $data = array(
                            'fk_product_id'=>$productId,
                            'fk_course_id'=>$courseId,
                            'quantity' => $productMappings[1],
                            'created_by' => $inputData['userID']
                        );
                        $this->db->insert('product_xref_course',$data);
                    }
                }
            }
            else{
                $invalidProductCount = $invalidProductCount+1;
            }
        }
        $differentialProductIds=array_values(array_diff($existProductIds,$newProducts));
        if(count($differentialProductIds)>0)
        for($k=0;$k<count($differentialProductIds);$k++)
        {
            $data = array(
                'is_active' => 1,
                'updated_by' => $inputData['userID'],
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->db->where('fk_product_id',$differentialProductIds[$k]);
            $this->db->where('fk_course_id',$courseId);
            $this->db->update('product_xref_course',$data);
        }
        $db_response=array("status"=>true,"message"=>'Mapping saved successfully',"data"=>[]);
        return $db_response;
    }
    public function deleteProductMapping($data)
    {
        $this->db->where('fk_course_id',$data['courseId']);
        $del_res= $this->db->update('product_xref_course',array('is_active'=>1));
        if($del_res)
        {
            $status=true;
            $message="Product mapping deleted successfully";
        }
        else
        {
            $error = $this->db->error();
            $status=FALSE;
            $message=$error['message'];
        }
        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function productCourseMappingDataTables()
    {
        $table = 'product_xref_course';
        $primaryKey = 'pc`.`product_xref_course_id';
        $columns = array(
            array( 'db' => '`c`.`name` as course_name',         'dt' => 'course_name',                  'field' => 'course_name' ),
            array( 'db' => '`p`.`name` as product_name',        'dt' => 'product_name',                 'field' => 'product_name' ),
            array( 'db' => '`p`.`code`',                        'dt' => 'code',                         'field' => 'code' ),
            array( 'db' => '`rtv`.`value` as `product_type`',   'dt' => 'product_type',                 'field' => 'product_type'),
            array( 'db' => '`pc`.`quantity`',                   'dt' => 'quantity',                     'field' => 'quantity'),
            array( 'db' => '`rtv1`.`value` as `publication`',   'dt' => 'publication',                  'field' => 'publication'),
            array( 'db' => '`pc`.`created_on`',                 'dt' => 'created_on',                   'field' => 'created_on'),
            array( 'db' => '`pc`.`product_xref_course_id`',      'dt' => 'product_xref_course_id',       'field' => 'product_xref_course_id' ),
            array( 'db' => '`pc`.`fk_course_id` as courseId',    'dt' => 'courseId',                    'field' => 'courseId' ),
            array( 'db' => '`pc`.`updated_on`',    'dt' => 'updated_on',                    'field' => 'updated_on' ),
            array( 'db' => '(select count(product_xref_course_id) from product_xref_course pxc where `pc`.`fk_course_id`=`pxc`.`fk_course_id` and pxc.is_active=0) as product_count',    'dt' => 'product_count',                    'field' => 'product_count' )
        );
        $globalFilterColumns = array(
            array( 'db' => '`c`.`name`',            'dt' => 'name',         'field' => 'name' ),
            array( 'db' => '`p`.`name`',            'dt' => 'name',         'field' => 'name' ),
            array( 'db' => '`p`.`code`',            'dt' => 'code',         'field' => 'code' ),
            array( 'db' => '`rtv`.`value`',         'dt' => 'value',        'field' => 'value'),
            array( 'db' => '`pc`.`quantity`',       'dt' => 'quantity',     'field' => 'quantity'),
            array( 'db' => '`rtv1`.`value`',        'dt' => 'value',        'field' => 'value'),
            array( 'db' => '`pc`.`created_on`',     'dt' => 'created_on',   'field' => 'created_on')
        );
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        $joinQuery = "FROM `product_xref_course` AS `pc` LEFT JOIN `product` p ON (p.`product_id`= `pc`.`fk_product_id`) LEFT JOIN `course` c ON (c.course_id=pc.fk_course_id) JOIN `reference_type_value` AS `rtv` ON (`rtv`.`reference_type_value_id` = `p`.`fk_product_type_id`) LEFT JOIN `reference_type_value` AS `rtv1` ON (`rtv1`.`reference_type_value_id` = `p`.`fk_publication_id`)  ";
        $extraWhere = "pc.is_active=0";
        $groupBy = "";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery,$extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['product_xref_course_id']=encode($responseData['data'][$i]['product_xref_course_id']);
            $responseData['data'][$i]['courseId']=encode($responseData['data'][$i]['courseId']);
            $responseData['data'][$i]['created_date']=date('j M, Y',strtotime($responseData['data'][$i]['created_on']));
            if($responseData['data'][$i]['updated_on'] != '')
            $responseData['data'][$i]['updated_date']=date('j M, Y',strtotime($responseData['data'][$i]['updated_on']));
            else
                $responseData['data'][$i]['updated_date'] = date('j M, Y',strtotime($responseData['data'][$i]['created_on']));
        }
        return $responseData;
    }
}