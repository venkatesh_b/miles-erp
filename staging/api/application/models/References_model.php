<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class References_model extends CI_Model
{
    var $referencestypes=array();
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;

    }

    public function GetReferences($id = NULL)
    {

        $this->db->select('*');

        if ($id === NULL)
        {

        }
        else{
         $this->db->where('reference_type_id', $id);
        }


        $query = $this->db->get('reference_type');

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
        else{
            return FALSE;
        }

    }
    public function getReferenceValuesList($id = NULL){
        try{
            $query = $this->db->query("call Sp_Get_ReferenceValues($id)");
        if(!$query)
        {
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else{
            if ($query->num_rows() > 0)
            {
                
                $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
            }
            else{
                $db_response=array("status"=>false,"message"=>'No records found',"data"=>array());
            }
            
            }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        
            return $db_response;
        
        
    }
    public function getAddReferenceValueFileds_main($id){

        $this->db->select('reference_type_id,name,parent_id,order');
       
        if ($id === NULL)
        {

        }
        else{
         $this->db->where('reference_type_id', $id);
        }
        $query = $this->db->get('reference_type');
        
        if ($query->num_rows() > 0)
        {

            $qresult=$query->result();
            foreach($qresult as $qresultrow){
                
                $this->referencestypes[]=$qresultrow;
                
                if($qresultrow->parent_id!=''){

                   $this->getAddReferenceValueFileds_main($qresultrow->parent_id);

                }
                else{
                    //return $this->referencestypes[];
                }
                
            }
            
        }

    }
    public function getAddReferenceValueFileds($id){

        $this->referencestypes=array();
        $this->getAddReferenceValueFileds_main($id);
        return $this->referencestypes;
        

    }
    public function getReferenceValues($reference_type_id=NULL,$parent_id=NULL){

        try{
            $this->db->select('reference_type_value_id,value');
       
        if ($reference_type_id === NULL)
        {

        }
        else{
         $this->db->where('reference_type_id', $reference_type_id);
        }
        if ($parent_id === NULL)
        {

        }
        else{
         $this->db->where('parent_id', $parent_id);
        }

         $this->db->where('is_active', 1);

        $query = $this->db->get('reference_type_value');
        if(!$query){
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else{
            if ($query->num_rows() > 0)
        {
           
            $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
        }
        else{
            $db_response=array("status"=>false,"message"=>'No Records Found',"data"=>array());
        }
        }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        
        return $db_response;
    }
    
    public function getReferenceValuesUsingName($name=NULL,$parent_id=NULL){
        
        $reference_type_id = $this->getReferenceType($name);
        if(count($reference_type_id['data']) > 0)
        {
            try{
                $this->db->select('reference_type_value_id,value');

                if ($name !== NULL)
                {
                 $this->db->where('reference_type_id', $reference_type_id['data'][0]->reference_type_id);
                }
                if ($parent_id === NULL)
                {

                }
                else{
                 $this->db->where('parent_id', $parent_id);
                }

                $this->db->where('is_active', 1);

                $query = $this->db->get('reference_type_value');
                if(!$query){
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
                else{
                    if ($query->num_rows() > 0)
                    {

                        $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
                    }
                    else{
                        $db_response=array("status"=>false,"message"=>'No Records Found',"data"=>array());
                    }
                }
            } catch (Exception $ex) {
                $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
            }
        }else
        {
            $db_response=array("status"=>false,"message"=>'No Records Found',"data"=>array());
        }
        
        
        return $db_response;
    }
    /**
     * function name : getReferenceValuesByName model
     * Params 
     *      - name (Refrence type name)
     * return 
     *      Success - list
     *      Error/no records - blank array
     * 
     * author : Abhilash
     */
    public function getReferenceValuesByName($name,$parent='')
    {
        if($name == 'country')
        {
            $this->db->select('rtv.reference_type_value_id,rtv.value,rt.name, rt.reference_type_id');
            $this->db->from('reference_type rt');
            $this->db->join('reference_type_value rtv', 'rt.reference_type_id=rtv.reference_type_id');
            $this->db->where('rt.name', $name);
            $this->db->order_by('rtv.value','asc');
            $res_all = $this->db->get();
    //        die($this->db->last_query());
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
    //                print_r($res_all);die;
                    return $res_all;
                } else {
                    return false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        else if($name == 'state')
        {
            $this->db->select('rts.reference_type_value_id,rts.`value` as state,rtc.reference_type_value_id countryId, rtc.`value` as country');
            $this->db->from('reference_type rt');
            $this->db->join('reference_type_value rts', 'rts.reference_type_id=rt.reference_type_id', 'left');
            $this->db->join('reference_type_value rtc', 'rts.parent_id=rtc.reference_type_value_id', 'left');
            $this->db->where('rt.name', $name);
            $this->db->group_by('rts.reference_type_value_id');
            $this->db->order_by('rts.parent_id');

            $res_all = $this->db->get();
    //        die($this->db->last_query());
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
    //                print_r($res_all);die;
                    return $res_all;
                } else {
                    return false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        else if($name == 'city')
        {
            $this->db->select('rtct.reference_type_value_id,rtct.`value` as city, rts.reference_type_value_id stateId,rts.`value` as state, rtc.reference_type_value_id countryId,rtc.`value` as country');
            $this->db->from('reference_type rt');
            $this->db->join('reference_type_value rtct', 'rtct.reference_type_id=rt.reference_type_id', 'left');
            $this->db->join('reference_type_value rts', 'rts.reference_type_value_id=rtct.parent_id', 'left');
            $this->db->join('reference_type_value rtc', 'rtc.reference_type_value_id=rts.parent_id', 'left');
            $this->db->where('rt.name', $name);
            $this->db->group_by('rtct.reference_type_value_id');
            $this->db->order_by('rtc.reference_type_value_id');

            $res_all = $this->db->get();
    //        die($this->db->last_query());
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
    //                print_r($res_all);die;
                    return $res_all;
                } else {
                    return false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
        else
        {
            $this->db->select('rtv.reference_type_value_id,rtv.value,rt.name, rt.reference_type_id');
            $this->db->from('reference_type rt');
            $this->db->join('reference_type_value rtv', 'rt.reference_type_id=rtv.reference_type_id');
            $this->db->where('rt.name', $name);
            if($parent!=''){
                $this->db->where('rtv.parent_id', $parent);
            }
            $this->db->order_by('rtv.order','asc');
            $this->db->order_by('rtv.value','asc');
            $res_all = $this->db->get();
//            die($this->db->last_query());
            if ($res_all) {
                $res_all = $res_all->result();
                if (count($res_all) > 0) {
    //                print_r($res_all);die;
                    return $res_all;
                } else {
                    return false;
                }
            } else {
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
        }
    }

    public function AddReferenceValue($data){
        
        try{
            
            $this->db->select('value');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_id',$data['reference_type_id']);
            $this->db->where('value',$data['reference_type_val']);
            if(!empty($data['reference_type_val_parent']) && trim($data['reference_type_val_parent'])!='' && $data['reference_type_val_parent']!=NULL)
            {
                $this->db->where('parent_id',$data['reference_type_val_parent']);
            }
            $this->db->where('is_active',1);
            
            $query = $this->db->get();
            if(!$query){
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
            else{
            if ($query->num_rows() > 0)
            {
                $db_response=array("status"=>false,"message"=>'Already Exist',"data"=>array("reference_type_val"=>'Already Exist.'));
            }
            else{
                $data_insert = array(
                'reference_type_id' => $data['reference_type_id'] ,
                'value' => $data['reference_type_val'] ,
                'fk_created_by' => $_SERVER['HTTP_USER'] ,
                'created_date' => date("Y-m-d H:i:s"),
                'parent_id' => $data['reference_type_val_parent'],
                'is_active' => $data['reference_type_is_active'],
                'description' => $data['reference_type_val_description']  
                );
                $res_insert=$this->db->insert('reference_type_value', $data_insert);
                $ReferenceId = $this->db->insert_id();
                if($res_insert){
                    $db_response=array("status"=>true,"message"=>'success',"data"=>array("ReferenceId"=>$ReferenceId));
                }
                else{
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
            }
            }
            
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        
        return $db_response;



    }
    function UpdateReferenceValue($data){


        try{
            $this->db->select('value');
            $this->db->from('reference_type_value');
            $this->db->where('reference_type_id',$data['reference_type_id']);
            $this->db->where('value',$data['reference_type_val']);
            if(!empty($data['reference_type_val_parent']) && trim($data['reference_type_val_parent'])!='' && $data['reference_type_val_parent']!=NULL)
            {
                $this->db->where('parent_id',$data['reference_type_val_parent']);
            }
            $this->db->where('is_active',1);
            $this->db->where('reference_type_value_id!=', $data['reference_type_val_id']);
            
            $query = $this->db->get();
            if(!$query){
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
            else{
            if ($query->num_rows() > 0)
            {
                $db_response=array("status"=>false,"message"=>'Already Exist.',"data"=>array("reference_type_val"=>'Already Exist.'));
            }
            else{
                $data_update = array(
            'reference_type_id' => $data['reference_type_id'] ,
            'value' => $data['reference_type_val'] ,
            'fk_created_by' => 1 ,
            'created_date' => date("Y-m-d H:i:s"),
            'parent_id' => $data['reference_type_val_parent'],
            'is_active' => $data['reference_type_is_active'],
            'description' => $data['reference_type_val_description']  
            );

            $this->db->where('reference_type_value_id', $data['reference_type_val_id']);
            $res_update=$this->db->update('reference_type_value', $data_update);
            if($res_update){
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else{
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
            }
            }
            
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }

        return $db_response;



    }
    function RemoveReferenceValue($data){

        try{
            $data_update = array(
            'is_active' => 0
            );

            $this->db->where('reference_type_value_id', $data['reference_type_val_id']);
            $res_update=$this->db->update('reference_type_value', $data_update);
            if($res_update){
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else{
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        } catch (Exception $ex) {
                $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        return $db_response;

    }
    public function getReferenceType($name = NULL)
    {

        $this->db->select('reference_type_id,name');
        $this->db->where('name', $name);
        $query = $this->db->get('reference_type');
        if($query){
          if ($query->num_rows() > 0)
          {
              
              $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
          }
          else{
              $db_response=array("status"=>false,"message"=>"No Records Found","data"=>array());
          }  
        }
        else{
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array()); 
        }
        return $db_response;
        
        

    }
    var $first;
    public function getAllReferencesIteration($id){
        $this->db->select('reference_type_value_id id, value, is_active, parent_id, description');
        $this->db->from('reference_type_value');
        $this->db->where('reference_type_value_id', $id);
        //$this->db->where('is_active', '1');

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();
            $this->first[] = (array)$res_all[0];
            if (count($res_all) > 0) {
                if($res_all[0]->parent_id != null)
                {
                     $this->getSingleReferenceValue($res_all[0]->parent_id); 
                }
                else
                {
                    //return $res_all;
                }
            } else {
                return false;
            }
            //return $this->first;
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function getSingleReferenceValue($id){
        $r=$this->getAllReferencesIteration($id);
        return $this->first;
    }

    public function referencesDataTables($reference_type)
    {

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );

        $table = 'reference_type';
        // Table's primary key
        $primaryKey = 'rt`.`reference_type_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes

        if(strtolower($reference_type) == 'country')
        {
            $columns = array(
                array( 'db' => '`rtv`.`value` as country'                      ,'dt' => 'country'                    ,'field' => 'country' ),
                array( 'db' => '`rtv`.`reference_type_value_id`'    ,'dt' => 'reference_type_value_id'  ,'field' => 'reference_type_value_id' ),
            );

            $globalFilterColumns = array(
                array( 'db' => '`rtv`.`value`'  , 'dt' => 'value'   ,'field' => 'value' )
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = "FROM reference_type rt JOIN reference_type_value rtv ON rt.reference_type_id=rtv.reference_type_id ";
        }
        elseif(strtolower($reference_type) == 'state')
        {
            $columns = array(
                array( 'db' => '`rtv`.`value` as state'                      ,'dt' => 'state'                    ,'field' => 'state' ),
                array( 'db' => '`rtvc`.`value` as country'                      ,'dt' => 'country'                    ,'field' => 'country' ),
                array( 'db' => '`rtv`.`reference_type_value_id`'    ,'dt' => 'reference_type_value_id'  ,'field' => 'reference_type_value_id' ),
            );

            $globalFilterColumns = array(
                array( 'db' => '`rtv`.`value`'                      ,'dt' => 'state'                    ,'field' => 'state' ),
                array( 'db' => '`rtvc`.`value`'                      ,'dt' => 'country'                    ,'field' => 'country' )
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = "FROM reference_type rt JOIN reference_type_value rtv ON rt.reference_type_id=rtv.reference_type_id JOIN reference_type_value rtvc ON rtv.parent_id=rtvc.reference_type_value_id ";
        }
        elseif(strtolower($reference_type) == 'city')
        {
            $columns = array(
                array( 'db' => '`rtv`.`value` as city'              ,'dt' => 'city'                     ,'field' => 'city' ),
                array( 'db' => '`rtvs`.`value` as state'            ,'dt' => 'state'                    ,'field' => 'state' ),
                array( 'db' => '`rtvc`.`value` as country'          ,'dt' => 'country'                  ,'field' => 'country' ),
                array( 'db' => '`rtv`.`reference_type_value_id`'    ,'dt' => 'reference_type_value_id'  ,'field' => 'reference_type_value_id' ),
            );

            $globalFilterColumns = array(
                array( 'db' => '`rtv`.`value`'  ,'dt' => 'city'     ,'field' => 'city' ),
                array( 'db' => '`rtvs`.`value`' ,'dt' => 'state'    ,'field' => 'state' ),
                array( 'db' => '`rtvc`.`value`' ,'dt' => 'country'  ,'field' => 'country' )
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = "FROM reference_type rt JOIN reference_type_value rtv ON rt.reference_type_id=rtv.reference_type_id JOIN reference_type_value rtvs ON rtv.parent_id=rtvs.reference_type_value_id JOIN reference_type_value rtvc ON rtvs.parent_id=rtvc.reference_type_value_id ";
        }
        elseif(strtolower($reference_type) == 'tag')
        {
            $columns = array(
                array( 'db' => 'TRIM(`rtv`.`value`) as tag_name'          ,'dt' => 'tag_name'                 ,'field' => 'tag_name' ),
                array( 'db' => '`rtv`.`description`'                ,'dt' => 'description'              ,'field' => 'description' ),
                array( 'db' => '`rtv`.`reference_type_value_id`'    ,'dt' => 'reference_type_value_id'  ,'field' => 'reference_type_value_id' ),
            );

            $globalFilterColumns = array(
                array( 'db' => 'TRIM(`rtv`.`value`)'      ,'dt' => 'tag_name'        ,'field' => 'tag_name' ),
                array( 'db' => '`rtv`.`description`', 'dt' => 'description' ,'field' => 'description' )
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = "FROM reference_type rt JOIN reference_type_value rtv ON rt.reference_type_id=rtv.reference_type_id ";
        }
        elseif(strtolower($reference_type) == 'company')
        {
            $columns = array(
                array( 'db' => 'TRIM(`rtv`.`value`) as company_name'          ,'dt' => 'company_name'                 ,'field' => 'company_name' ),
                array( 'db' => '`rtv`.`description`'                ,'dt' => 'description'              ,'field' => 'description' ),
                array( 'db' => '`rtv`.`reference_type_value_id`'    ,'dt' => 'reference_type_value_id'  ,'field' => 'reference_type_value_id' ),
            );

            $globalFilterColumns = array(
                array( 'db' => 'TRIM(`rtv`.`value`)'      ,'dt' => 'company_name'        ,'field' => 'company_name' ),
                array( 'db' => '`rtv`.`description`', 'dt' => 'description' ,'field' => 'description' )
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = "FROM reference_type rt JOIN reference_type_value rtv ON rt.reference_type_id=rtv.reference_type_id ";
        }
        elseif(strtolower($reference_type) == 'post qualification')
        {
            $columns = array(
                array( 'db' => 'TRIM(`rtv`.`value`) as postqualification_name'          ,'dt' => 'postqualification_name'                 ,'field' => 'postqualification_name' ),
                array( 'db' => '`rtv`.`description`'                ,'dt' => 'description'              ,'field' => 'description' ),
                array( 'db' => '`rtv`.`reference_type_value_id`'    ,'dt' => 'reference_type_value_id'  ,'field' => 'reference_type_value_id' ),
            );

            $globalFilterColumns = array(
                array( 'db' => 'TRIM(`rtv`.`value`)'      ,'dt' => 'postqualification_name'        ,'field' => 'postqualification_name' ),
                array( 'db' => '`rtv`.`description`', 'dt' => 'description' ,'field' => 'description' )
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = "FROM reference_type rt JOIN reference_type_value rtv ON rt.reference_type_id=rtv.reference_type_id ";
        }
        elseif(strtolower($reference_type) == 'qualification')
        {
            $columns = array(
                array( 'db' => '`rtv`.`value` as qualification'                      ,'dt' => 'qualification'                    ,'field' => 'qualification' ),
                array( 'db' => '`rtvc`.`value` as postqualification'                      ,'dt' => 'postqualification'                    ,'field' => 'postqualification' ),
                array( 'db' => '`rtv`.`reference_type_value_id`'    ,'dt' => 'reference_type_value_id'  ,'field' => 'reference_type_value_id' ),
            );

            $globalFilterColumns = array(
                array( 'db' => '`rtv`.`value`'                      ,'dt' => 'qualification'                    ,'field' => 'qualification' ),
                array( 'db' => '`rtvc`.`value`'                      ,'dt' => 'postqualification'                    ,'field' => 'postqualification' )
            );

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
             * If you just want to use the basic configuration for DataTables with PHP
             * server-side, there is no need to edit below this line.
             */
            $joinQuery = "FROM reference_type rt JOIN reference_type_value rtv ON rt.reference_type_id=rtv.reference_type_id JOIN reference_type_value rtvc ON rtv.parent_id=rtvc.reference_type_value_id ";
        }

        $extraWhere = "rt.`name`='".$reference_type."'";
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        /*for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['reference_type_value_id']=encode($responseData['data'][$i]['reference_type_value_id']);
        }*/
        return $responseData;
    }

}