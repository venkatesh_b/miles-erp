<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @controller Name Source wise report
 * @category        Model
 * @author          Parameshwar
 */
class SourceReport_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    /**
     * Function to get Reports by source
     *
     * @return - array
     * @created date - 18 April 2016
     * @author : Parameshwar
     */
    public function getSourceReport($branch, $course, $stage){
        $query = $this->db->query("call Sp_Get_Sourcewise_Leads_Report({$branch},{$course},{$stage})");
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                $this->db->close();
                $this->load->database();
                return $res;
            }
            else{
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        return $responseData;
    }

}
