<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class StudentAttendance_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }

    public function getStudents($params){
        $status=true;$message='success';$data=[];
        $classId=$params['classId'];
        $students="SELECT l.name,bxl.branch_xref_lead_id,CONCAT_WS(\"\",b.code,ba.code,bxl.lead_number) as enrollNo FROM batch_xref_class bxc,branch_xref_lead bxl,branch b,batch ba,lead l Where bxc.fk_batch_id=bxl.fk_batch_id and b.branch_id=bxl.branch_id and ba.batch_id=bxl.fk_batch_id and l.lead_id=bxl.lead_id and bxl.is_active=1 and bxc.batch_xref_class_id=".$classId;
        $res=$this->db->query($students);
        $data['students']=[];
        if($res){
            if($res->num_rows()>0){
                $rows=$res->result();
                foreach($rows as $k=>$v){
                    $v->attendanceStatus='Not taken';
                    $checkExist=$this->checkAttendanceExist($classId,$v->branch_xref_lead_id);
                    if($checkExist=='no record exist'){

                        $v->attendanceStatus='not taken';

                    }
                    if($checkExist=='present'){
                        $v->attendanceStatus='present';
                    }
                    if($checkExist=='absent'){
                        $v->attendanceStatus='absent';
                    }
                }
                $status=true;$message='success';$data['students']=$rows;
            }
            else{
                $status=false;$message='No records found';
            }
        }
        else{
            $status=false;$message='Something went wrong.';
        }
        $data['oldStudents']=[];
        $old_students ="SELECT l.name as text,CONCAT_WS('|',bxl.branch_xref_lead_id,CONCAT_WS('',b.code,ba.code,bxl.lead_number),l.name) as id
                        FROM lead_attendence la
                        LEFT JOIN branch_xref_lead bxl ON la.fk_lead_id=bxl.branch_xref_lead_id
                        LEFT JOIN lead l ON bxl.lead_id=l.lead_id
                        LEFT JOIN branch b ON bxl.branch_id=b.branch_id
                        LEFT JOIN batch_xref_class bxc ON la.fk_batch_xref_class_id=bxc.batch_xref_class_id
                        LEFT JOIN batch ba ON bxc.fk_batch_id=ba.batch_id
                        WHERE bxc.batch_xref_class_id=".$classId." AND la.student_type='old'";
        $res_old=$this->db->query($old_students);
        if($res_old)
        {
            if($res_old->num_rows()>0)
            {
                $data['oldStudents']=$res_old->result();
            }
        }
        $data['guestStudents']=[];
        $guest_students ="SELECT l.name as text,CONCAT_WS('|',bxl.branch_xref_lead_id,CONCAT_WS('',b.code,ba.code,bxl.lead_number),l.name) as id
                        FROM lead_attendence la
                        LEFT JOIN branch_xref_lead bxl ON la.fk_lead_id=bxl.branch_xref_lead_id
                        LEFT JOIN lead l ON bxl.lead_id=l.lead_id
                        LEFT JOIN branch b ON bxl.branch_id=b.branch_id
                        LEFT JOIN batch_xref_class bxc ON la.fk_batch_xref_class_id=bxc.batch_xref_class_id
                        LEFT JOIN batch ba ON bxc.fk_batch_id=ba.batch_id
                        WHERE bxc.batch_xref_class_id=".$classId." AND la.student_type='guest'";
        $res_guest=$this->db->query($guest_students);
        if($res_guest)
        {
            if($res_guest->num_rows()>0)
            {
                $data['guestStudents']=$res_guest->result();
            }
        }
        if(count($data['oldStudents'])==0 && count($data['guestStudents'])==0 && count($data['students'])==0)
        {
            $status=false;$message='No students found';
        }
        else
        {
            $status=true;$message='Success';
        }
        return array('status'=>$status,'message'=>$message,'data'=>$data);
    }
    public function getOldStudents($params){
        $status=true;$message='success';$data=[];
        $classId=$params['classId'];
        $val=$params['searchVal'];
        $students="SELECT CONCAT_WS('|',l.name,CONCAT_WS('',b.code,ba.code,bxl.lead_number),l.email,l.phone) as text,CONCAT_WS('|',bxl.branch_xref_lead_id,CONCAT_WS('',b.code,ba.code,bxl.lead_number),l.name) as id FROM batch_xref_class bxc,branch_xref_lead bxl,branch b,batch ba,lead l,batch bat where bxl.fk_batch_id!=bxc.fk_batch_id and b.branch_id=bxl.branch_id and ba.batch_id=bxl.fk_batch_id and l.lead_id=bxl.lead_id and bxl.is_active=1 and bxc.batch_xref_class_id=$classId and bat.batch_id=bxc.fk_batch_id and bxl.branch_id=bat.fk_branch_id and bxl.fk_course_id=bat.fk_course_id and (l.name like '%".$val."%' or l.email like '%".$val."%' or l.phone like '%".$val."%' or CONCAT_WS('',b.code,ba.code,bxl.lead_number) like '%".$val."%')";
        $res=$this->db->query($students);
        if($res){
            if($res->num_rows()>0){
                $status=true;$message='success';$data=$res->result();
            }
            else{
                $status=false;$message='No records found';$data=[];
            }
        }
        else{
            $status=false;$message='Something went wrong.';$data=[];
        }
        return array('status'=>$status,'message'=>$message,'data'=>$data);
    }
    public function checkAttendanceExist($classId='',$leadId=''){
        $attendanceStatus='';
        $attendanceQuery="select if(is_attended=1,'present','absent') as attendance from lead_attendence where fk_batch_xref_class_id=$classId and fk_lead_id=$leadId";
        $res=$this->db->query($attendanceQuery);
        if($res){
            if($res->num_rows()>0){
                $row=$res->result();
                $attendanceStatus=$row[0]->attendance;
            }
            else{
                $attendanceStatus='no record exist';
            }
        }
        else{
            $attendanceStatus='error';
        }
        return $attendanceStatus;
    }
    public function saveAttendance($params){
        $status=true;$message='success';$data=[];
        $classId=$params['classId'];
        $currentDateTime=date('Y-m-d H:i:s');//guestattendedLeads
        if(isset($params['attendedLeads']) && $params['attendedLeads']!=''){
            //attended leads
            $exp=explode(',',$params['attendedLeads']);
            if(count($exp)>0){
                for($i=0;$i<count($exp);$i++){

                    $checkExist=$this->checkAttendanceExist($classId,$exp[$i]);
                    if($checkExist=='no record exist'){

                        $data_insert=[];
                        $data_insert['fk_batch_xref_class_id']=$classId;
                        $data_insert['fk_lead_id']=$exp[$i];
                        $data_insert['is_attended']=1;
                        $data_insert['student_type']='current';
                        $data_insert['fk_created_by']=$_SERVER['HTTP_USER'];
                        $data_insert['created_date']=$currentDateTime;
                        $this->db->insert('lead_attendence',$data_insert);

                    }
                    if($checkExist=='present'){

                    }
                    if($checkExist=='absent'){
                        $data_update=[];
                        $data_update['is_attended']=1;
                        $data_update['fk_updated_by']=$_SERVER['HTTP_USER'];
                        $data_update['updated_date']=$currentDateTime;
                        $this->db->where('fk_batch_xref_class_id',$classId);
                        $this->db->where('fk_lead_id',$exp[$i]);
                        $this->db->update('lead_attendence',$data_update);
                    }

                }
            }
        }
        if(isset($params['absentedLeads']) && $params['absentedLeads']!=''){
            //abesented leads
            $exp=explode(',',$params['absentedLeads']);
            if(count($exp)>0){
                for($i=0;$i<count($exp);$i++){

                    $checkExist=$this->checkAttendanceExist($classId,$exp[$i]);
                    if($checkExist=='no record exist'){

                        $data_insert=[];
                        $data_insert['fk_batch_xref_class_id']=$classId;
                        $data_insert['fk_lead_id']=$exp[$i];
                        $data_insert['is_attended']=0;
                        $data_insert['student_type']='current';
                        $data_insert['fk_created_by']=$_SERVER['HTTP_USER'];
                        $data_insert['created_date']=$currentDateTime;
                        $this->db->insert('lead_attendence',$data_insert);

                    }
                    if($checkExist=='present'){
                        $data_update=[];
                        $data_update['is_attended']=0;
                        $data_update['fk_updated_by']=$_SERVER['HTTP_USER'];
                        $data_update['updated_date']=$currentDateTime;
                        $this->db->where('fk_batch_xref_class_id',$classId);
                        $this->db->where('fk_lead_id',$exp[$i]);
                        $this->db->update('lead_attendence',$data_update);
                    }
                    if($checkExist=='absent'){

                    }

                }
            }
        }
        $delete_query = "DELETE FROM lead_attendence where student_type in ('old') and fk_batch_xref_class_id=$classId";
        $delete_res = $this->db->query($delete_query);
        if(isset($params['specialattendedLeads']) && $params['specialattendedLeads']!='') {
            $exp = explode(',', $params['specialattendedLeads']);
            if ($delete_res){
                if (count($exp) > 0) {
                    for ($i = 0; $i < count($exp); $i++) {
                        $data_insert = [];
                        $data_insert['fk_batch_xref_class_id'] = $classId;
                        $data_insert['fk_lead_id'] = $exp[$i];
                        $data_insert['is_attended'] = 1;
                        $data_insert['student_type'] = 'old';
                        $data_insert['fk_created_by'] = $_SERVER['HTTP_USER'];
                        $data_insert['created_date'] = $currentDateTime;
                        $this->db->insert('lead_attendence', $data_insert);
                    }
                }
            }
        }

        $delete_query = "DELETE FROM lead_attendence where student_type in ('guest') and fk_batch_xref_class_id=$classId";
        $delete_res = $this->db->query($delete_query);
        if(isset($params['guestattendedLeads']) && $params['guestattendedLeads']!='') {
            $exp = explode(',', $params['guestattendedLeads']);
            if ($delete_res){
                if (count($exp) > 0) {
                    for ($i = 0; $i < count($exp); $i++)
                    {
                        $data_insert = [];
                        $data_insert['fk_batch_xref_class_id'] = $classId;
                        $data_insert['fk_lead_id'] = $exp[$i];
                        $data_insert['is_attended'] = 1;
                        $data_insert['student_type'] = 'guest';
                        $data_insert['fk_created_by'] = $_SERVER['HTTP_USER'];
                        $data_insert['created_date'] = $currentDateTime;
                        $this->db->insert('lead_attendence', $data_insert);
                    }
                }
            }
        }
        $data_update_main=[];
        $data_update_main['attendence_status']=1;
        $data_update_main['fk_updated_by']=$_SERVER['HTTP_USER'];
        $data_update_main['updated_date']=$currentDateTime;
        $this->db->where('batch_xref_class_id',$classId);
        $res_update_main=$this->db->update('batch_xref_class',$data_update_main);

        return array('status'=>$status,'message'=>$message,'data'=>$params);
    }

}
