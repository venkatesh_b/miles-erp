<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @Modal Name      Venue
 * @category        Model
 * @author          Abhilash
 */
class Subject_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    
    public function getSubjectById($id) {
        $this->db->select('s.subject_id, s.fk_course_id, s.code AS subject_code, s.name, s.is_active, c.name course_name, c.is_active course_active');
        $this->db->from('subject s');
        $this->db->join('course c', 's.fk_course_id=c.course_id', 'left');
        $this->db->where('s.subject_id', $id);
        $result = $this->db->get();
//        die($this->db->last_query());
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }else{
                return 'badrequest';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function getAllSubjectList(){
        $this->db->select('s.subject_id, s.name');
        $this->db->from('subject s');
        $this->db->where('s.is_active', '1');
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    /**
     * Get subject by course
     */
    public function getAllSubjectByCourse($id) {
        $this->db->select('c.course_id');
        $this->db->from('course c');
        $this->db->where('c.course_id', $id);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nocourse';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('s.subject_id, s.code AS subject_code, s.name');
        $this->db->from('subject s');
        $this->db->where('s.fk_course_id', $id);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }else{
                return 'badrequest';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function getAllSubjectByFaculty($id){
        $final = [];
        $this->db->select('f.fk_subject_id');
        $this->db->from('faculty f');
        $this->db->where('f.faculty_id', $id);
        $result = $this->db->get();
//        die($this->db->last_query());
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                $this->db->select('s.subject_id, s.name');
                $this->db->from('subject s');
                $this->db->where('s.subject_id IN ('.$result[0]->fk_subject_id.')');
                $this->db->where('s.is_active ', 1);
                $res = $this->db->get();
//                die($this->db->last_query());
                if ($res) {
                    $res = $res->result();
                    if (count($res) > 0) {
                        return $res;
                    }
                }else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }else{
                return 'nofaculty';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function saveSubject($data) {
        /* For Course */
        $this->db->select('course_id');
        $this->db->from('course');
        $this->db->where('course_id', $data['course']);
        $this->db->where('is_active', '1');
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nocourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        /*For subject code*/
        $this->db->select('*');
        $this->db->from('subject');
//        $this->db->where('subject_id!='.$id);
        $this->db->where('code', $data['subjectCode']);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = false;
                return 'codeexist';
            } else {
                $valid = true;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $data_insert = array(
            'name' => $data['subjectName'],
            'fk_course_id' => $data['course'],
            'code' => $data['subjectCode'],
            'is_active' => '1',
            'fk_created_by' => $_SERVER['HTTP_USER'],
            'created_date' => date('Y-m-d H:i:s')
        );
        return $this->db->insert('subject', $data_insert);
    }
    
    public function editSubject($id, $data) {
        /*For Id*/
        $this->db->select('*');
        $this->db->from('subject');
        $this->db->where('subject_id', $id);
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /*For subject code*/
        $this->db->select('*');
        $this->db->from('subject');
        $this->db->where('subject_id!='.$id);
        $this->db->where('code', $data['subjectCode']);
        $query = $this->db->get();
//        die($this->db->last_query());
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = false;
                return 'codeexist';
            } else {
                $valid = true;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        /* For Course */
        $this->db->select('course_id');
        $this->db->from('course');
        $this->db->where('course_id', $data['course']);
        $this->db->where('is_active', '1');
        $query = $this->db->get();
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                $valid = true;
            } else {
                $valid = false;
                return 'nocourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $data_update = array(
            'name' => $data['subjectName'],
            'fk_course_id' => $data['course'],
            'code' => $data['subjectCode'],
            'is_active' => '1',
            'fk_updated_by' => $_SERVER['HTTP_USER'],
            'updated_date' => date('Y-m-d H:i:s')
        );
        $this->db->where(array('subject_id' => $id));
        return $this->db->update('subject', $data_update);
    }
    
    public function deleteSubject($id) {
        $this->db->select('*');
        $this->db->from('subject');
        $this->db->where('subject_id', $id);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if ($res_all) {

                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );
                
                $this->db->where('subject_id', $id);
                $res_update = $this->db->update('subject', $data_update);

                if ($this->db->error()['code'] == '0') {
                    if ($res_update) {
                        if ($is_active) {
                            return 'active';
                        } else {
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    function getSubjectDatatables(){
        $table = 'subject';
        // Table's primary key
        $primaryKey = 's`.`subject_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`s`.`name`'                                 ,'dt' => 'name'               ,'field' => 'name' ),
            array( 'db' => '`c`.`name` AS course'                        ,'dt' => 'course'               ,'field' => 'course' ),
            array( 'db' => '`s`.`code` AS subject_code'                   ,'dt' => 'subject_code'         ,'field' => 'subject_code' ),
            array( 'db' => 'if(`s`.`is_active`=1,1,0) as `is_active`'  ,'dt' => 'is_active'        ,'field' => 'is_active' ),
            array( 'db' => '`s`.`subject_id`'                             ,'dt' => 'subject_id'           ,'field' => 'subject_id' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`s`.`name`'             ,'dt' => 'name'          ,'field' => 'name' ),
            array( 'db' => '`s`.`code`'             ,'dt' => 'subject_code'   ,'field' => 'subject_code' ),
            array( 'db' => '`c`.`name`'            ,'dt' => 'course'         ,'field' => 'course' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM subject s left join course c ON s.fk_course_id=c.course_id ";
        $extraWhere = "";
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        return $responseData;
    }
    
}
