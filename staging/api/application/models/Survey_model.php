<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @Model Name      Survey
 * @category        Model
 * @author          Abhilash
 * @Description     For Survey CRUD Services
 */
class Survey_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    
    /**
     * Function get question typr
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getQuestionTypes(){
        $this->db->select('reference_type_id');
        $this->db->from('reference_type');
        $this->db->where('name', 'Question Type');
        $ref_type_id = $this->db->get();
        if ($ref_type_id) {
            $ref_type_id = $ref_type_id->result();
            if (count($ref_type_id) > 0) {
                $ref_type_id = $ref_type_id[0]->reference_type_id;
            } else {
                return 'norefoptions';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('reference_type_value_id, reference_type_id, value, is_active');
        $this->db->from('reference_type_value');
        $this->db->where('reference_type_id', $ref_type_id);

        $ref_type_val_id = $this->db->get();
        if ($ref_type_val_id) {
            $ref_type_val_id = $ref_type_val_id->result();
            if (count($ref_type_val_id) > 0) {
                return $ref_type_val_id;
            } else {
                return 'nooptions';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    /**
     * Function get All questons datatables
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getSurveyDataTable($id){
        $table = 'question';
        // Table's primary key
        $primaryKey = 'q`.`question_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`q`.`title`'                                     ,'dt' => 'title'                  ,'field' => 'title' ),
            array( 'db' => '`r`.`value` AS option_type'                      ,'dt' => 'option_type'            ,'field' => 'option_type' ),
            array( 'db' => '`q`.`question_id`'                              ,'dt' => 'question_id'            ,'field' => 'question_id' ),
            array( 'db' => 'if(`q`.`is_active`=1,1,0) as `question_active`'  ,'dt' => 'question_active'        ,'field' => 'question_active' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`q`.`title`'    ,'dt' => 'title'            ,'field' => 'title' ),
            array( 'db' => '`r`.`value`'    ,'dt' => 'option_type'      ,'field' => 'option_type' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM question q left join reference_type_value r ON q.fk_question_type_id=r.reference_type_value_id ";
        $extraWhere = "q.fk_course_id=".$id;
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        return $responseData;
    }
    
    /**
     * Function get questions by ID
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getSurveyById($id){
        $this->db->select('q.question_id,q.title, q.is_active, qo.value, rt.value AS `option`, q.fk_course_id as course_id');
        $this->db->from('question q');
        $this->db->where('q.question_id', $id);
        $this->db->join('question_option qo', 'q.question_id=qo.fk_question_id', 'left');
        $this->db->join('reference_type_value rt', 'q.fk_question_type_id=rt.reference_type_value_id', 'left');
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }else{
                return 'badrequest';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    /**
     * Function get questions list by course
     * 
     * @return - array
     * @author : Abhilash
     */
    public function getSurveyListByCourse($id){
        $this->db->select('q.question_id, qo.question_option_id ,q.title, q.is_active, qo.value, rt.value AS `option`');
        $this->db->from('question q');
        $this->db->where('q.fk_course_id', $id);
        $this->db->join('question_option qo', 'q.question_id=qo.fk_question_id', 'left');
        $this->db->join('reference_type_value rt', 'q.fk_question_type_id=rt.reference_type_value_id', 'left');
        $this->db->where('q.is_active', '1');
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }else{
                return 'badrequest';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    /**
     * Function get questions and answers for the leads
     * 
     * @return - array
     * @author : Abhilash
     */

    public function getSurveyAnswersForLead($leadId)
    {
        /*SELECT q.question_id,q.fk_question_type_id,lsr.answer,rtv.`value` as option_type FROM question q
JOIN reference_type_value rtv ON q.fk_question_type_id=rtv.reference_type_value_id
JOIN lead_sr_answer lsr ON lsr.fk_question_id=q.question_id*/

        $this->db->select('q.question_id,q.fk_question_type_id,lsr.answer,rtv.`value` as option_type');
        $this->db->from('question q');
        $this->db->join('reference_type_value rtv', 'q.fk_question_type_id=rtv.reference_type_value_id');
        $this->db->join('lead_sr_answer lsr', 'lsr.fk_question_id=q.question_id');
        $this->db->where('lsr.fk_branch_xref_lead_id', $leadId);
        $this->db->where('lsr.answer!=', '');
        $result = $this->db->get();
        if ($result)
        {
            $result = $result->result();
            if (count($result) > 0)
            {
                return $result;
            }
            else
            {
                return 'badrequest';
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    /**
     * Function save survey answers for the leads
     * 
     * @return - array
     * @author : Abhilash
     */
    public function saveSurveyAnswersForLead($data,$followup)
    {
        $branch_xref_lead_id = 0;
        if(is_array($data))
        {
            $this->db->trans_start();
            foreach ($data as $k => $v)
            {
                /*For Question id for same course*/
                $this->db->select('q.question_id,q.fk_question_type_id');
                $this->db->from('question q');
                $this->db->where('q.fk_course_id', $v['courseId']);
                $this->db->where('q.question_id', $v['id']);
                $result = $this->db->get();
                if($result)
                {
                    $result = $result->result();
                    if(count($result) > 0)
                    {
                        $option = $result[0]->fk_question_type_id;
                        $this->db->select('reference_type_value_id,value');
                        $this->db->from('reference_type_value');
                        $this->db->where('reference_type_value_id',$option);
                        $res = $this->db->get();
                        if($res)
                        {
                           $res = $res->result(); 
                           if(count($res) > 0)
                           {
                               $option = $res[0]->value;
                               $optionId = $res[0]->reference_type_value_id;
                           }
                           else
                           {
                               return 'noOption';
                           }
                        }
                        else
                        {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                    }
                    else
                    {
                        return 'noQuestion';
                    }
                }
                else
                {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
                $branch_xref_lead_id = $v['leadId'];
                /*For lead*/
                $this->db->select('branch_xref_lead_id');
                $this->db->from('branch_xref_lead');
                $this->db->where('branch_xref_lead_id', $v['leadId']);
                $this->db->where('is_active', '1');
                $result = $this->db->get();
                if($result){
                    $result = $result->result();
                    if(count($result) <= 0){
                        return 'nolead';
                    }
                }else{
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
                
                /*If present*/
                $this->db->select('fk_branch_xref_lead_id');
                $this->db->from('lead_sr_answer');
                $this->db->where('fk_branch_xref_lead_id', $v['leadId']);
                $this->db->where('fk_question_id', $v['id']);
                $result = $this->db->get();
                if($result)
                {
                    $result = $result->result();
                        if(count($result) > 0)
                        {
                            $update = array
                            (
                                'fk_branch_xref_lead_id' => $v['leadId'],
                                'fk_question_id' => $v['id'],
                                'answer' => $v['value'],
                                'fk_updated_by' => $_SERVER['HTTP_USER'],
                                'updated_date' => date('Y-m-d H:i:s'),
                            );
                            $this->db->where('fk_branch_xref_lead_id', $v['leadId']);
                            $this->db->where('fk_question_id', $v['id']);
                            $this->db->update('lead_sr_answer', $update);
                        }
                        else
                        {
                            if($v['value']!='')
                            {
                                $insert = array(
                                    'fk_branch_xref_lead_id' => $v['leadId'],
                                    'fk_question_id' => $v['id'],
                                    'answer' => $v['value'],
                                    'fk_created_by' => $_SERVER['HTTP_USER'],
                                    'created_date' => date('Y-m-d H:i:s'),
                                );
                                $this->db->insert('lead_sr_answer', $insert);
                            }
                        }

                }
                else
                {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            }
            else
            {
                $this->db->trans_commit();

                if($followup != '')
                {

                    $call_schedule_head_id = $this->getHeadId($branch_xref_lead_id);
                    $this->db->where('fk_lead_stage_id = 20');
                    $this->db->where('fk_lead_call_schedule_head_id',$call_schedule_head_id);
                    $this->db->delete("lead_user_xref_lead_due", array("fk_branch_xref_lead_id" => $branch_xref_lead_id));

                    if($this->db->affected_rows() > 0)
                    {
                        $data_lead_user_xref_lead_history['fk_user_id'] = $_SERVER['HTTP_USER'];
                        $data_lead_user_xref_lead_history['fk_branch_xref_lead_id'] = $branch_xref_lead_id;
                        $data_lead_user_xref_lead_history['fk_lead_id'] = $branch_xref_lead_id;
                        $data_lead_user_xref_lead_history['fk_lead_call_schedule_head_id'] = $call_schedule_head_id;
                        $data_lead_user_xref_lead_history['lead_status'] = 'M7';
                        $data_lead_user_xref_lead_history['description'] = 'SR Followup';
                        $data_lead_user_xref_lead_history['call_type'] = 'Outgoing';

                        $data_lead_user_xref_lead_history['created_on'] = date('Y-m-d H:i:s');
                        $this->db->insert('lead_user_xref_lead_history', $data_lead_user_xref_lead_history);

                        $update = array
                        (
                            'student_followup_date' => date('Y-m-d H:i:s'),
                            'fk_updated_by' => $_SERVER['HTTP_USER'],
                            'updated_date' => date('Y-m-d H:i:s'),
                        );
                        $this->db->where('fk_branch_xref_lead_id', $branch_xref_lead_id);
                        $this->db->update('branch_xref_lead_id', $update);
                    }

                }
                return true;
            }
        }
    }

    function getHeadId($id){
        $this->db->select("fk_lead_call_schedule_head_id");
        $this->db->from("lead_user_xref_lead_due");
        $this->db->where("fk_lead_stage_id",20);
        $this->db->where("fk_branch_xref_lead_id",$id);
        $query = $this->db->get();
        if($query) {
            foreach ($query->result() as $k) {
                return $k->fk_lead_call_schedule_head_id;
            }
            return false;
        }
        else{
            return false;
        }


    }
    /**
     * Function save question
     * 
     * @return - array
     * @author : Abhilash
     */
    public function saveSurvey($data){
        $this->db->select('course_id');
        $this->db->from('course');
        $this->db->where('course_id', $data['course']);
        $course = $this->db->get();
        if ($course) {
            $course = $course->result();
            if (count($course) <= 0) {
                return 'nocourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('reference_type_id');
        $this->db->from('reference_type');
        $this->db->where('name', 'Question Type');
        $ref_type_id = $this->db->get();
        if ($ref_type_id) {
            $ref_type_id = $ref_type_id->result();
            if (count($ref_type_id) > 0) {
                $ref_type_id = $ref_type_id[0]->reference_type_id;
            } else {
                return 'norefoptions';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value');
        $this->db->where('value', $data['optionType']);
        $this->db->where('reference_type_id', $ref_type_id);

        $ref_type_val_id = $this->db->get();
        if ($ref_type_val_id) {
            $ref_type_val_id = $ref_type_val_id->result();
            if (count($ref_type_val_id) > 0) {
                $ref_type_val_id = $ref_type_val_id[0]->reference_type_value_id;
            } else {
                return 'nooptions';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->trans_start();
        $insert = array(
            'title' => $data['txtTitle'],
            'fk_course_id' => $data['course'],
            'fk_question_type_id' => $ref_type_val_id,
            'is_active' => 1,
            'fk_created_by' => $_SERVER['HTTP_USER'],
            'created_date' => date('Y-m-d H:i:s'),
        );
        $res_insert = $this->db->insert('question', $insert);
        $last_id = $this->db->insert_id();
        if($data['optionType'] == 'radio' || $data['optionType'] == 'checkbox'){
            foreach ($data['options'] as $key => $value) {
                $insert = array(
                    'fk_question_id' => $last_id,
                    'value' => $value['value'],
                    'is_active' => 1,
                    'fk_created_by' => $_SERVER['HTTP_USER'],
                    'created_date' => date('Y-m-d H:i:s'),
                );
                $res_insert = $this->db->insert('question_option', $insert);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    
    /**
     * Function edit question by Id
     * 
     * @return - array
     * @author : Abhilash
     */
    public function editSurvey($id, $data){
        $this->db->select('course_id');
        $this->db->from('course');
        $this->db->where('course_id', $data['course']);
        $course = $this->db->get();
        if ($course) {
            $course = $course->result();
            if (count($course) <= 0) {
                return 'nocourse';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('question_id');
        $this->db->from('question');
        $this->db->where('question_id', $id);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) <= 0) {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('reference_type_id');
        $this->db->from('reference_type');
        $this->db->where('name', 'Question Type');
        $ref_type_id = $this->db->get();
        if ($ref_type_id) {
            $ref_type_id = $ref_type_id->result();
            if (count($ref_type_id) > 0) {
                $ref_type_id = $ref_type_id[0]->reference_type_id;
            } else {
                return 'norefoptions';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        
        $this->db->select('reference_type_value_id');
        $this->db->from('reference_type_value');
        $this->db->where('value', $data['optionType']);
        $this->db->where('reference_type_id', $ref_type_id);

        $ref_type_val_id = $this->db->get();
        if ($ref_type_val_id) {
            $ref_type_val_id = $ref_type_val_id->result();
            if (count($ref_type_val_id) > 0) {
                $ref_type_val_id = $ref_type_val_id[0]->reference_type_value_id;
            } else {
                return 'nooptions';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
        $this->db->trans_start();
        $update = array(
            'title' => $data['txtTitle'],
            'fk_course_id' => $data['course'],
            'fk_question_type_id' => $ref_type_val_id,
            'is_active' => 1,
            'fk_updated_by' => $_SERVER['HTTP_USER'],
            'updated_date' => date('Y-m-d H:i:s'),
        );
        $this->db->where('question_id', $id);
        $res_update = $this->db->update('question', $update);
        $this->db->where('fk_question_id', $id);
        $this->db->delete('question_option');
        if($data['optionType'] == 'radio' || $data['optionType'] == 'checkbox'){
            foreach ($data['options'] as $key => $value) {
                $insert = array(
                    'fk_question_id' => $id,
                    'value' => $value['value'],
                    'is_active' => 1,
                    'fk_created_by' => $_SERVER['HTTP_USER'],
                    'created_date' => date('Y-m-d H:i:s'),
                );
                $res_insert = $this->db->insert('question_option', $insert);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    
    /**
     * Function .active/inactive question by Id
     * 
     * @return - array
     * @author : Abhilash
     */
    public function deleteSurvey($id) {
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('question_id', $id);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if ($res_all) {

                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );
                
                $this->db->select('fk_branch_xref_lead_id');
                $this->db->from('lead_sr_answer');
                $this->db->where('fk_question_id', $id);
                $result = $this->db->get();
                if($result){
                    $result = $result->result();
                    if(count($result)>0){
                        return 'alreadyused';
                    }else{
                        //
                    }
                }else{
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }

                $this->db->where('question_id', $id);
                $res_update = $this->db->update('question', $data_update);
                /*Remove dependency with the Answer table*/
                $this->db->delete('lead_sr_answer', array('fk_question_id' => $id));

                if ($this->db->error()['code'] == '0') {
                    if ($res_update) {
                        if ($is_active) {
                            return 'active';
                        } else {
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
}