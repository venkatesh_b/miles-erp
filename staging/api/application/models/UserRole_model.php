<?php

/* 
 * Created By: V Parameshwar. Date: 03-02-2016
 * Purpose: Services for User role.
 * 
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class UserRole_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
        $this->load->helper('encryption');
        
    }
    public function checkrole($param){
        $this->db->select("rtv.reference_type_value_id,rtv.`value`,rtv.description");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='User Role'");
        $this->db->where("rtv.reference_type_value_id",$param);
        $this->db->order_by("rtv.`value`");
        $query = $this->db->get();
        if($query){
        if ($query->num_rows() > 0){
            $res=$query->result();
            //exit;
            $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
        }
        else{
            $db_response=array("status"=>false,"message"=>'Invalid Role ID',"data"=>array());
        }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
        
    }
    public function rolelist() {
        
        $this->db->select("rtv.reference_type_value_id as roleId,rtv.`value` as roleName");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='User Role'");
        $this->db->where("rtv.`is_active`",1);
        $this->db->group_by("rtv.reference_type_value_id");
        $this->db->order_by("rtv.`value`");
        $query = $this->db->get();
        if($query){
        if ($query->num_rows() > 0){
            $res=$query->result();
            foreach($res as $k_res=>$v_res){
                $v_res->roleId=encode($v_res->roleId);
            }
            //exit;
            $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
        }
        else{
            $db_response=array("status"=>false,"message"=>'No Roles Found',"data"=>array());
        }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function roles(){
        
        $this->db->select("rtv.reference_type_value_id,rtv.`value`,rtv.description,rtv.created_date,rtv.fk_created_by,count(uxr.fk_role_id) as totalCount");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->join('user_xref_role uxr','uxr.fk_role_id=rtv.reference_type_value_id','left');
        $this->db->where("rt.`name`='User Role'");
        $this->db->where("rtv.`is_active`",1);
        $this->db->group_by("rtv.reference_type_value_id");
        $this->db->order_by("rtv.`value`");
        $query = $this->db->get();
        if($query){
        if ($query->num_rows() > 0){
            $res=$query->result();
            
            foreach($res as $k=>$v){
                $roleid=$v->reference_type_value_id;
                $v->reference_type_value_id=encode($v->reference_type_value_id);
                $subquery='';
                
                $this->db->select("e.`name`");
                $this->db->from("user u");
                $this->db->join("user_xref_role uxr","uxr.fk_user_id=u.user_id");
                $this->db->join("employee e","e.user_id=u.user_id");
                $this->db->where("uxr.fk_role_id",$roleid);
                $this->db->order_by("e.`name`");
                $subquery = $this->db->get();
                if($subquery){
                    if ($subquery->num_rows() > 0){
                    $ressub=$subquery->result();
                     $v->users=$ressub;
                    }
                }
                else{
                }
                
                
                
            }
            
            //exit;
            $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
        }
        else{
            $db_response=array("status"=>false,"message"=>'No Roles Found',"data"=>array());
        }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    function getRolePermissions($data){
            
            $roleId=$data['RoleId'];
            $query = $this->db->query("call Sp_Get_Menu_Role($roleId)");
            if(!$query){
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
            else{
                if ($query->num_rows() > 0)
                {
                    
                    $res_array=$query->result();
                    $this->db->close();
                    $this->load->database();
                    $parents_indexes=array();
                    foreach($res_array as $k=>$v){
                        
                            $this->db->select("app_menu_id,menu_order");
                            $this->db->from("role_xref_menu_order");
                            $this->db->where("app_menu_id",$v->app_menu_id);
                            $this->db->where("fk_role_id",$roleId);
                            $checkingAlreadyenabled= $this->db->get();
                            if ($checkingAlreadyenabled->num_rows() > 0){
                                $v->isEnabled=1;
                                $res_existmenuitem=$checkingAlreadyenabled->result();
                            
                                foreach($res_existmenuitem as $k_exist_menu=>$v_exist_menu){
                                    $v->menu_order=$v_exist_menu->menu_order;
                                }
                            }
                            else{
                                $v->isEnabled=0;
                            }
                            
                            $this->db->select("ca.component_access_id as actionId,ca.service_url as actionUrl,rt.`value` actionName");
                             $this->db->from("component cm");
                             $this->db->join("component_access ca","cm.component_id=ca.fk_component_id");
                             $this->db->join("reference_type_value rt","rt.reference_type_value_id=ca.fk_action_id");
                            $this->db->where("cm.component_id",$v->fk_component_id);
                            $subquery_compnt_actions = $this->db->get();
                            $v->actions=array();
                            if($subquery_compnt_actions){
                                
                            if ($subquery_compnt_actions->num_rows() > 0){
                            $res_compnt_actions=$subquery_compnt_actions->result();
                            
                            foreach($res_compnt_actions as $k_access=>$v_access){
                                
                                    $v_access->isEnabled=0;
                                    $this->db->select('rc.role_component_access_id');
                                    $this->db->from('role_component_access rc');
                                    $this->db->where('rc.fk_component_access_id',$v_access->actionId);
                                    $this->db->where('rc.fk_role_id',$roleId);
                                    $this->db->where('rc.is_accessible',1);
                                    $check_role_access = $this->db->get();
                                    if($check_role_access){
                                        if ($check_role_access->num_rows() > 0){
                                            $v_access->isEnabled=1;
                                        }
                                    }
                            }
                            
                            $v->actions=$res_compnt_actions;
                            }
                            }
                            
                            
                             
                        $tot_array[]=(array)$v;
                        if($v->parent_id==''){
                            $parents_indexes[]=$k;
                        }
                    }
                    $new = array();
                    foreach ($tot_array as $a){
                        $new[$a['parent_id']][] = $a;
                    }
                    for($mn=0;$mn<count($parents_indexes);$mn++){
                        
                        $tree[] = $this->createTree($new, array($tot_array[$parents_indexes[$mn]]))[0];
                        
                    }
                    $db_response=array("status"=>true,"message"=>'success',"data"=>$tree);
                }
                else{
                    $db_response=array("status"=>false,"message"=>'No Menu found',"data"=>array());
                }
            }
            return $db_response;
            
    }
    function createTree(&$list, $parent){
                $tree = array();
                foreach ($parent as $k=>$l){
                if(isset($list[$l['app_menu_id']])){
                    $l['submenu'] = $this->createTree($list, $list[$l['app_menu_id']]);
                }
                $tree[] = $l;
                } 
                return $tree;
     }
     public function checkAccess($param){
         
        $this->db->select("ca.component_access_id");
        $this->db->from('component_access ca');
        $this->db->where("ca.component_access_id",$param);
        $query = $this->db->get();
        
        if($query){
        if ($query->num_rows() > 0){
            $res=$query->result();
            //exit;
            $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
        }
        else{
            $db_response=array("status"=>false,"message"=>'Invalid Tag ID',"data"=>array());
        }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
        
    }
     public function checkMenu($param){
         
        $this->db->select("am.app_menu_id");
        $this->db->from('app_menu am');
        $this->db->where("am.app_menu_id",$param);
        $query = $this->db->get();
        
        if($query){
        if ($query->num_rows() > 0){
            $res=$query->result();
            //exit;
            $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
        }
        else{
            $db_response=array("status"=>false,"message"=>'Invalid Tag ID',"data"=>array());
        }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
        
    }
     function saveAccessPermissions($params){
        
        $alreadyadded=array();
        $tobeadded=array();
        $alreadyaddedMenuids=array();
        $tobeaddedMenuids=array();
        $roleId=$params['roleId'];
        //$roleDescription=$params['roleDescription'];
        $access_ids=explode(',',$params['accessId']);
        $menu_ids=explode(',',$params['menuId']);
        $menu_order=$params['menu_order'];
        $tobeadded=$access_ids;
        $tobeaddedMenuids=$menu_ids;
        //getting existing menu ids for this role //
        $this->db->select("rxm.app_menu_id");
        $this->db->from('role_xref_menu_order rxm');
        $this->db->where("rxm.fk_role_id",$roleId);
        $query = $this->db->get();
        
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $k=>$v){
                    $alreadyaddedMenuids[]=$v->app_menu_id;
                }
            }
            else{
                $alreadyaddedMenuids=array();
            }
        }
        else{
            $alreadyaddedMenuids=array();
        }
            $tobeaddedMenuids=array_values(array_unique($tobeaddedMenuids));
            $alreadyaddedMenuids=array_values(array_unique($alreadyaddedMenuids));
            $diffinalreadyMenuids=array_values(array_diff($alreadyaddedMenuids,$tobeaddedMenuids));
            $diffintobeaddedMenuids=array_values(array_diff($tobeaddedMenuids,$alreadyaddedMenuids));
        //--------//
            
            $data_insert_menuids=array();
            if(count($diffintobeaddedMenuids)>0){
                for($ij=0;$ij<count($diffintobeaddedMenuids);$ij++){
                    if(trim($diffintobeaddedMenuids[$ij])!=''){
                        $data_insert_menuids[]=array(
                            'app_menu_id' => $diffintobeaddedMenuids[$ij],
                            'fk_role_id' => $roleId);
                    }
                }
                $res_insert_menuids=$this->db->insert_batch('role_xref_menu_order', $data_insert_menuids);

                if($res_insert_menuids>=0){
                    $db_response=array("status"=>true,"message"=>"success","data"=>array());
                }
                else{
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());

                }
            }
            else{
                 $db_response=array("status"=>true,"message"=>"success","data"=>array());
            }
            if(count($diffinalreadyMenuids)>0){
                $this->db->where('fk_role_id', $roleId);
                $this->db->where_in('app_menu_id', $diffinalreadyMenuids);
                $res_delete=$this->db->delete('role_xref_menu_order');

                if($res_delete){
                    $db_response=array("status"=>true,"message"=>'success',"data"=>array());
                }
                else{
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
            }
            else{
                 $db_response=array("status"=>true,"message"=>"success","data"=>array());
            }
            
            
        
        //getting existing accesses for this role //
        $this->db->select("rc.fk_component_access_id");
        $this->db->from('role_component_access rc');
        $this->db->where("rc.fk_role_id",$roleId);
        $this->db->where("rc.is_accessible",1);
        $query = $this->db->get();
        
        if($query){
            if ($query->num_rows() > 0){
                $res=$query->result();
                foreach($res as $k=>$v){
                    $alreadyadded[]=$v->fk_component_access_id;
                }
            }
            else{
                $alreadyadded=array();
            }
        }
        else{
            $alreadyadded=array();
        }
            $tobeadded=array_values(array_unique($tobeadded));
            $alreadyadded=array_values(array_unique($alreadyadded));
            $diffinalready=array_values(array_diff($alreadyadded,$tobeadded));
            $diffintobeadded=array_values(array_diff($tobeadded,$alreadyadded));
        //--------//
            for($o=0;$o<count($menu_order);$o++){
                
                $data_update_order='';
                $data_update_order = array(
                'menu_order' => $menu_order[$o]['order']
                );
                $this->db->where('app_menu_id', $menu_order[$o]['id']);
                $this->db->where('fk_role_id', $roleId);
                $res_update_order=$this->db->update('role_xref_menu_order', $data_update_order);
            }
           
            
//            $data_update_roledetails = array(
//             'description' => $roleDescription
//            );
//            $this->db->where('reference_type_value_id', $roleId);
//            $res_update=$this->db->update('reference_type_value', $data_update_roledetails);
                
            $data_insert_accessids=array();
            if(count($diffintobeadded)>0){
                for($ij=0;$ij<count($diffintobeadded);$ij++){
                    if(trim($diffintobeadded[$ij])!=''){
                        $data_insert_accessids[]=array(
                            'fk_component_access_id' => $diffintobeadded[$ij],
                            'is_accessible' => 1,
                            'fk_role_id' => $roleId);
                    }
                }
                $res_insert_accessids=$this->db->insert_batch('role_component_access', $data_insert_accessids);

                if($res_insert_accessids>=0){
                    $db_response=array("status"=>true,"message"=>"success","data"=>array());
                }
                else{
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());

                }
            }
            else{
                 $db_response=array("status"=>true,"message"=>"success","data"=>array());
            }
            if(count($diffinalready)>0){
                $data_update = array(
                 'is_accessible' => 0
                );
                $this->db->where('fk_role_id', $roleId);
                $this->db->where_in('fk_component_access_id', $diffinalready);
                $res_update=$this->db->update('role_component_access', $data_update);

                if($res_update){
                    $db_response=array("status"=>true,"message"=>'success',"data"=>array());
                }
                else{
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
            }
            else{
                 $db_response=array("status"=>true,"message"=>"success","data"=>array());
            }
            return $db_response;
    }
    function assignMenu($params){
        
        
        $data_insert=array('fk_role_id' => $params['roleId'],
                            'fk_menu_id' => $params['menuId']);
         $res_insert=$this->db->insert('role_xref_menu', $data_insert);
         if($res_insert){
             $db_response=array("status"=>true,"message"=>'success',"data"=>array());
         }
         else{
             $error = $this->db->error(); 
             $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
         }
        
         return $db_response;
    }

    public function rolesDataTables()
    {
        $table = 'reference_type';
        // Table's primary key
        $primaryKey = 'rt`.`reference_type_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`rtv`.`value`',                         'dt' => 'value',                    'field' => 'value' ),
            array( 'db' => '`rtv`.`description`',                   'dt' => 'description',              'field' => 'description' ),
            array( 'db' => 'count(u.user_id) as `totalCount`', 'dt' => 'totalCount',               'field' => 'totalCount' ),
            array( 'db' => '`rtv`.`reference_type_value_id`',       'dt' => 'reference_type_value_id',  'field' => 'reference_type_value_id' ),
            array( 'db' => '`rtv`.`created_date`',                  'dt' => 'created_date',             'field' => 'created_date'),
            array( 'db' => '`rtv`.`fk_created_by`',                 'dt' => 'fk_created_by',            'field' => 'fk_created_by' ),
            array( 'db' => 'GROUP_CONCAT(" ",emp.`name` ORDER BY emp.`name` ASC) as `Users`',   'dt' => 'Users',                    'field' => 'Users' ),
        );

        $globalFilterColumns = array(
            array( 'db' => '`rtv`.`value`',         'dt' => 'value',        'field' => 'value' ),
            array( 'db' => '`rtv`.`description`',   'dt' => 'description',  'field' => 'description' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM `reference_type` AS `rt` JOIN `reference_type_value` AS `rtv` ON (`rtv`.`reference_type_id` = `rt`.`reference_type_id`) LEFT JOIN `user_xref_role` AS `uxr` ON (`uxr`.`fk_role_id` = `rtv`.`reference_type_value_id`) LEFT JOIN `user` u ON (u.user_id = uxr.fk_user_id AND u.is_active=1) LEFT JOIN `employee` emp ON emp.user_id = u.user_id  ";
        $extraWhere = "`rt`.`name` = 'User Role' AND `rtv`.`is_active`=1 ";
        $groupBy = "rtv.reference_type_value_id";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['reference_type_value_id']=encode($responseData['data'][$i]['reference_type_value_id']);
        }
        return $responseData;
    }
}