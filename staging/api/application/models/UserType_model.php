<?php

/* 
 * Created By: Abhilash. Date: 24-02-2016
 * Purpose: Services for User Type(Common Master).
 * 
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class UserType_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;
    }
    public function checkUserType($param, $id){
//        print_r($id);
//        print_r($id!=0);die;
        /*$this->db->select("rtv.reference_type_value_id,rtv.`value`,rtv.description");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rt.reference_type_id=rtv.reference_type_id','left');
        $this->db->where("rtv.value=",$param);
        $this->db->where("rt.`name`='User Type'");

        if($id!=0){
            $this->db->where("rtv.reference_type_value_id <> ",$id);
        }
//        $this->db->where("rtv.is_active=",'1');*/
        /*$qry = "select * from reference_type rt 
                join reference_type_value rtv on rtv.reference_type_id=rt.reference_type_id
                where rt.name='User Type' 
                AND rtv.value='".$param."'";*/
        $qry = "select * from reference_type_value rtv
                where  rtv.value='".$param."' AND rtv.reference_type_id=5";

        $query = $this->db->query($qry);

        if($query){
            if ($query->num_rows() > 0){
                if($id != 0){
                    return 'present';
                }
                else
                {
                    $query = $query->result();
                    if($query[0]->reference_type_value_id == $id){
                        return 'notpresent';
                    }else{
                        return 'present';
                    }
                }
                
            }
            else{
                return 'notpresent';
            }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
//        return $db_response;
        
    }
    
    public function checkUserTypeById($param){
        $this->db->select("rtv.reference_type_value_id,rtv.`value`,rtv.description");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='User Type'");
        $this->db->where("rtv.reference_type_value_id",$param);
        
        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
        
    }
    
    public function getAllUserType(){
        
        $this->db->select("rtv.reference_type_value_id as typeId,rtv.`value` as typeName,rtv.description as typeDescription");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='User Type'");
//        $this->db->where("rtv.is_active=1");
        $this->db->order_by("rtv.reference_type_value_id", "desc");
        $query = $this->db->get();
        if($query){
        if ($query->num_rows() > 0){
            $res=$query->result();
            $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
        }
        else{
            $db_response=array("status"=>false,"message"=>'No Companies Found',"data"=>array());
        }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
        
        
    }
    
    public function getUserTypeById($id){
        $this->db->select("rtv.reference_type_value_id as typeId,rtv.`value` as typeName,rtv.description as typeDescription");
        $this->db->from('reference_type rt');
        $this->db->join('reference_type_value rtv','rtv.reference_type_id=rt.reference_type_id');
        $this->db->where("rt.`name`='User Type'");
        $this->db->where("rtv.`reference_type_value_id`",$id);
        $query = $this->db->get();
        if($query){
        if ($query->num_rows() > 0){
            $res=$query->result()[0];
            $db_response=array("status"=>true,"message"=>'success',"data"=>$res);
        }
        else{
            $db_response=array("status"=>false,"message"=>'No Companies Found',"data"=>array());
        }
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    
    public function addUserType($data, $userId){
        $isUserTypePresent = $this->checkUserType($data['UserTypeName'], 0);
        if($isUserTypePresent === 'present')
        {
            return 'duplicate';
        }
        else {
            $this->db->trans_begin();
            $data_insert = array(
                'reference_type_id' => 5,
                'value' => $data['UserTypeName'],
                'description' => $data['UserTypeDescription'],
                'is_active' => 1,
                'fk_created_by' => $userId,
                'created_date' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('reference_type_value', $data_insert);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    
    public function updateUserType($data, $id, $userId){
        $isUserTypeIdPresent = $this->checkUserTypeById($data['id']);
        if(!$isUserTypeIdPresent)
        {
            return 'notpresent';
        }
        else {
            $isUserTypeIdPresent = $this->checkUserType($data['UserTypeName'], $id);
            if($isUserTypeIdPresent === 'present')
            {
                return 'duplicate';
            }
            else if($isUserTypeIdPresent === 'notpresent')
            {
                $this->db->trans_begin();
                $data_update = array(
                    'reference_type_id' => 5,
                    'value' => $data['UserTypeName'],
                    'description' => $data['UserTypeDescription']
                );
                $this->db->where('reference_type_value_id', $id);
                $this->db->update('reference_type_value', $data_update);
                
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            }
        }
    }
    
    public function deleteUserType($data, $id, $userId)
    {
        $isUserTypeIdPresent = $this->checkUserTypeById($data['id']);
        if(!$isUserTypeIdPresent)
        {
            return 'notpresent';
        }
        else {
            $isUserTypeIdPresent = $this->checkUserType($data['id'], $id);
            if($isUserTypeIdPresent)
            {
                return 'duplicate';
            }
            else 
            {
                $this->db->trans_begin();
                $data_update = array(
                    'is_active' => (int)$data['status']
                );
                $this->db->where('reference_type_value_id', $id);
                $this->db->update('reference_type_value', $data_update);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                } else {
                    $this->db->trans_commit();
                    return array('status'=>true, 'msg'=>(int)$data['status']===1?'Activated':'Deactivated');
                }
            }
        }
    }
}
