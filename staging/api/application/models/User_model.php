<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @Model Name      User_model
 * @category        Model
 * @author          Abhilash
 * @Description     For user/employee CRUD Services
 */
class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('Branch_model', 'branch');
        $this->db->db_debug = false;
    }
    
    public function getSecondaryCounselorList($branchId) {
        $this->db->select('u.user_id, e.name');
        $this->db->from('user u');
        $this->db->join('employee e', 'u.user_id=e.user_id');
        $this->db->join('branch_xref_user bxu', 'u.user_id=bxu.user_id');
        $this->db->where('u.is_seconday_counsellor', 1);
        $this->db->where('u.is_active', 1);
        $this->db->where('bxu.branch_id', $branchId);
        
        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if ($res_all) {
                return $res_all;
            } else {
                return false;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    public function getAllUser() {
        $result = [];
        $qry = "select 
                e.user_id, e.email, e.name, e.image, e.phone,e.employee_code,u.created_date, uxr.fk_role_id, rtv.value as role
                from user as u 
                left join employee as e on u.user_id = e.user_id
                left join user_xref_role as uxr on u.user_id = uxr.fk_user_id
                left join reference_type_value as rtv on uxr.fk_role_id=rtv.reference_type_value_id AND u.is_active=1 order by u.created_date desc";

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                foreach ($query as $key => $val) {
                    $userBranch = [];
                    $tot = 0;
                    /* For Branch */
                    $branch = $this->branch->getUserSpecificBranch($val['user_id']);
;
                    if(is_array($branch) && isset($branch['error']) && $branch['error'] == 'dberror')
                    {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    else if(!$branch)
                    {
                        $branch = [];
                    }
                    else
                    {
                        $userBranch = $branch;
                    }
                    $result[] = (object) [
                                'userId' => $val['user_id'],
                                'email' => $val['email'],
                                'name' => $val['name'],
                                'image' => $val['image'],
                                'phone' => $val['phone'],
                                'role_id' => $val['fk_role_id'],
                                'role' => $val['role'],
                                'branch' => $userBranch,
                                /*'totalBranch' => $tot,*/
                                'created_date' => $val['created_date'],
                                'employee_code' => $val['employee_code']
                    ];
                }
            }
//            die;
            return $result;
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    public function getSingleUser($id)
    {
        $result = [];
        $qry = "select 
                if(u.is_seconday_counsellor=1,1,0) as counsellor,e.user_id, e.joining_date , e.email, e.name, e.image, e.phone,if(e.sex=1,1,0) as sex,e.dob,e.employee_code,uxr.fk_role_id, rtv.value as role
                from user as u 
                left join employee as e on u.user_id = e.user_id
                left join user_xref_role as uxr on u.user_id = uxr.fk_user_id
                left join reference_type_value as rtv on uxr.fk_role_id=rtv.reference_type_value_id
                where u.user_id =" . $id . "";

        $query = $this->db->query($qry);
        if ($query)
        {
            $query = $query->result_array();
            if (count($query) > 0)
            {
                foreach ($query as $key => $val)
                {
                    $userBranch = '';
                    $tot = 0;
                    /* For User type*/
                    $qry = "select 
                            rtv.value as userType
                            from reference_type rt
                                left join reference_type_value rtv on rt.reference_type_id=rtv.reference_type_id
                                inner join user u on u.fk_user_type_id=rtv.reference_type_value_id
                            where u.user_id=".$id." AND rt.name='User Type'";
                    $query = $this->db->query($qry);
                    if ($query)
                    {
                        $query = $query->result_array();
                        if (count($query) > 0)
                        {
                            $userType = $query[0]['userType'];
                        }
                    }
                    else
                    {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    /* For Branch */
                    $branch = $this->branch->getUserSpecificBranch($val['user_id']);
                    if(is_array($branch) && isset($branch['error']) && $branch['error'] == 'dberror')
                    {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                    else if(!$branch)
                    {
                        $branch = [];
                    }
                    else
                    {
                        $userBranch = $branch;
                    }
                    if($val['dob']=='0000-00-00')
                    {
                        $val['dob']='';
                    }
                    else
                    {
                        $val['dob']=(new DateTime($val['dob']))->format('Y-m-d');
                    }
                    if($val['joining_date']=='0000-00-00')
                    {
                        $val['joining_date']='';
                    }
                    else
                    {
                        $val['joining_date']=(new DateTime($val['joining_date']))->format('Y-m-d');
                    }
                    $result[] = (object) [
                                'userId' => $val['user_id'],
                                'email' => $val['email'],
                                'name' => $val['name'],
                                'image' => $val['image'],
                                'phone' => $val['phone'],
                                'sCounselor' => $val['counsellor'],
                                'role_id' => encode($val['fk_role_id']),
                                'role' => $val['role'],
                                'userType' => $userType,
                                'branch' => $userBranch,
                                /*'totalBranch' => $tot,*/
                                'sex' => $val['sex'],
                                'dob' => $val['dob'],
                                'join' => $val['joining_date'],
                                'employee_code' => $val['employee_code']
                    ];
                }
            }
            return $result;
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    public function createUser($data) {
        $this->db->db_debug = FALSE;
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('login_id', $data['email']);
        $this->db->where('is_active', 1);
        $user_check = $this->db->get();
        if ($user_check)
        {
            /*$user = $user_check->result();
            if (count($user)>=1)
            {
                return 'duplicate';
            }
            else*/
            {
                $this->db->select('*');
                $this->db->from('employee');
                $this->db->where('employee_code', $data['employee_code']);
                $empcode_check = $this->db->get();
                if($empcode_check->num_rows()>0)
                {
                    return 'duplicate employee code';
                }
                else
                {
                    $this->db->select('reference_type_id');
                    $this->db->from('reference_type');
                    $this->db->where('name', 'User Role');
                    $ref_roletype_chk = $this->db->get();

                if ($ref_roletype_chk)
                {
                    $ref_roletype_val = $ref_roletype_chk->result();
                    if ($ref_roletype_val)
                    {
                        $this->db->select('reference_type_value_id, value, is_active');
                        $this->db->from('reference_type_value');
                        $this->db->where('reference_type_id', $ref_roletype_val[0]->reference_type_id);
                        $this->db->where('is_active', '1');
                        $ref_type_val = $this->db->get();
                        if ($ref_type_val)
                        {
                            $ref_type = $ref_type_val->result();
                            if ($ref_type)
                            {
                                $found = false;
                                foreach ($ref_type as $key => $val)
                                {
                                    if ($val->reference_type_value_id == $data['role'])
                                    {
                                        $found = true;
                                        break;
                                    }
                                }
                                if (preg_match("/,/", $data['branch']))
                                {
                                    $branches = explode(',', $data['branch']);
                                    $random_branch = array_rand($branches);
                                    $selBranches = $branches[$random_branch];
                                }
                                else
                                {
                                    $branches = $selBranches = $data['branch'];
                                }

                                $this->db->select('*');
                                $this->db->from('branch');
                                $this->db->where_in('branch_id', $branches);
                                $usr_branch_chk = $this->db->get();
                                if ($usr_branch_chk)
                                {
                                    $usr_branch = $usr_branch_chk->result();
                                    if (count($usr_branch) > 0 && count($usr_branch) == count($branches))
                                    {
                                        $foundBranch = true;
                                    }
                                    else
                                    {
                                        return 'nobranch';
                                    }
                                }
                                else
                                {
                                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                }
                                /**/
                                if ($found && $foundBranch)
                                {
                                   $data_insert = array(
                                        'login_id' => $data['email'],
                                        'created_date' => date('Y-m-d H:i:s'),
                                        'fk_default_branch_id' => $selBranches,
                                        'fk_user_type_id' => 2,
                                        'is_seconday_counsellor' => (int)$data['sCounselor'],
                                    );
                                    /*Default user type id is 2 -> employee*/
                                    $res_insert = $this->db->insert('user', $data_insert);
                                    if ($this->db->error()['code'] == '0') {
                                        if ($res_insert)
                                        {
                                            $last_updated_id = $this->db->insert_id();
                                            $client_secret = base64_encode($data['email']);
                                            $data_insert2 = array(
                                                'user_id' => $last_updated_id,
                                                'email' => $data['email'],
                                                'name' => $data['name'],
                                                'phone' => $data['phone'],
                                                'sex' => (int)$data['sex'],
                                                'employee_code' => $data['employee_code'],
                                                'dob' => is_null($data['dob'])?'':date('Y-m-d H:i:s',strtotime($data['dob'])),
                                                'joining_date' => date('Y-m-d H:i:s',strtotime($data['join']))
                                            );
                                            $data_insert1 = array(
                                                'fk_role_id' => $data['role'],
                                                'fk_user_id' => $last_updated_id,
                                                /*'days' => '4',*/ /*Need to implement*/
                                                'updated_date' => date('Y-m-d H:i:s')
                                            );

                                            $data_insert3 = array(
                                                'user_id' => $last_updated_id,
                                                'secret' => $client_secret,
                                                'created_at' => date('Y-m-d H:i:s'),
                                                'status' => '1'
                                            );

                                            $data_insert4 = array();
                                            if (preg_match("/,/", $data['branch']))
                                            {
                                                foreach ($branches as $value) {
                                                    $data_insert4 = [
                                                        'user_id' => $last_updated_id,
                                                        'branch_id' => $value,
                                                        'days' => '0',
                                                        'updated_date' => date('Y-m-d H:i:s'),
                                                        'created_date' => date('Y-m-d H:i:s')
                                                    ];
                                                    $this->db->insert('branch_xref_user', $data_insert4);
                                                }
                                            }
                                            else
                                            {
                                                $data_insert4 = [
                                                    'user_id' => $last_updated_id,
                                                    'branch_id' => $branches,
                                                    'days' => '0',
                                                    'updated_date' => date('Y-m-d H:i:s'),
                                                    'created_date' => date('Y-m-d H:i:s')
                                                ];
                                                $this->db->insert('branch_xref_user', $data_insert4);
                                            }

                                            //Transaction model
                                            $this->db->trans_begin();
                                            $this->db->insert('user_xref_role', $data_insert1);
                                            $res_insert = $this->db->insert('employee', $data_insert2);
                                            $res_insert = $this->db->insert('oauth_clients', $data_insert3);
                                            $this->db->trans_complete();
                                            if ($this->db->trans_status() === FALSE)
                                            {
                                                $this->db->trans_rollback();
                                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                            }
                                            else
                                            {
                                                $this->db->trans_commit();
                                                /**/
                                                $res_client_id = $this->db->insert_id();
                                                $this->db->select('scope_id')->where('role_id', $data['role']);
                                                $query = $this->db->get('oauth_scopes_roles');
                                                $enabledscopes = $query->result();

                                                foreach ($enabledscopes as $clientscopes)
                                                {
                                                    $scopes = array(
                                                        "client_id" => $res_client_id,
                                                        "scope_id" => $clientscopes->scope_id,
                                                        "created_at" => date("Y-m-d H:i:s"));
                                                    $res_insert_client_scopes = $this->db->insert('oauth_client_scopes', $scopes);
                                                }
                                                /**/
                                                return true;
                                            }
                                        }
                                        else
                                        {
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                    }
                                }
                                else
                                {
                                    return 'norolefound';
                                }
                            }
                            else
                            {
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                        }
                        else
                        {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                    }
                    else
                    {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                }
                else
                {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }
            }
        }
    }

    public function changeStatusUser($id) {

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $id);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if ($res_all) {

                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );

                $this->db->where('user_id', $id);
                $res_update = $this->db->update('user', $data_update);

                if ($this->db->error()['code'] == '0') {
                    if ($res_update) {
                        if ($is_active) {
                            return 'active';
                        } else {
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }

    public function updateUser($id, $data) {
        
        /*$this->db->select('*');
        $this->db->from('user');
        $this->db->where('login_id', $data['email']);
        $this->db->where('user_id<>', $id);

        $res_all = $this->db->get();
        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) >= 1) {
                return 'duplicate';
            }
        }else{
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }*/
        
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $id);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();
            if (count($res_all) >= 1) {
                    
                $this->db->select('*');
                $this->db->from('employee');
                $this->db->where('employee_code', $data['employee_code']);
                $this->db->where('user_id<>', $id);
                $res_all = $this->db->get();
                
                if($res_all->num_rows()>0){
                    return 'duplicate employee code';
                }
                else{
                $this->db->select('reference_type_id');
                $this->db->from('reference_type');
                $this->db->where('name', 'User Role');

                $res_all = $this->db->get();

                if ($res_all) {
                    $res_all = $res_all->result();
                    if ($res_all) {
                        $this->db->select('reference_type_value_id, value, is_active');
                        $this->db->from('reference_type_value');
                        $this->db->where('reference_type_id', $res_all[0]->reference_type_id);
                        $this->db->where('is_active', '1');

                        $res_all = $this->db->get();

                        if ($res_all) {
                            $res_all = $res_all->result();

                            if ($res_all) {
                                $found = false;
                                foreach ($res_all as $key => $val) {
                                    if ($val->reference_type_value_id == $data['role']) {
                                        $found = true;
                                        break;
                                    }
                                }
                                /**/
                                if (preg_match("/,/", $data['branch'])){
                                    $branches = explode(',', $data['branch']);
                                    $random_branch = array_rand($branches);
                                    $selBranches = $branches[$random_branch];
                                } else {
                                    $branches = $selBranches = $data['branch'];
                                }
                                $this->db->select('*');
                                $this->db->from('branch');
                                $this->db->where_in('branch_id', $branches);

                                $res_all = $this->db->get();

                                if ($res_all) {
                                    $res_all = $res_all->result();
                                    if (count($res_all) > 0 && count($res_all) == count($branches)) {
                                        $foundBranch = true;
                                    } else {
                                        return 'nobranch';
                                    }
                                } else {
                                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                }
                                /**/
                                if ($found && $foundBranch) {
                                    $data_update1 = array(
                                        'login_id' => $data['email'],
                                        'updated_date' => date('Y-m-d H:i:s'),
                                        'fk_default_branch_id' => $selBranches,
                                        'is_seconday_counsellor' => (int)$data['sCounselor'],
                                        'updated_date' => date('Y-m-d H:i:s'),
                                    );
                                    
                                    $data_update2 = array(
                                        'email' => $data['email'],
                                        'name' => $data['name'],
                                        'phone' => $data['phone'],
                                        'sex' => (int)$data['sex'],
                                        'employee_code' => $data['employee_code'],
                                        'dob' => $data['dob'],
                                        'joining_date' => date('Y-m-d H:i:s',strtotime($data['join']))
                                    );

                                    $data_update3 = array(
                                        'fk_role_id' => $data['role'],
                                        'fk_user_id' => $id,
                                        'fk_updated_by' => $_SERVER['HTTP_USER'],
                                        'updated_date' => date('Y-m-d H:i:s')
                                    );

                                    //Transaction model
                                    $this->db->trans_begin();
                                    $this->db->where('user_id', $id);
                                    $this->db->update('user', $data_update1);
                                    $this->db->where('user_id', $id);
                                    $res_insert = $this->db->update('employee', $data_update2);
                                    $this->db->where('fk_user_id', $id);
                                    $res_insert = $this->db->update('user_xref_role', $data_update3);
                                    $data_update = array();
                                    
                                    //all are created and updated to in-active
//                                        $qry = "update branch_xref_user set is_active=0 where user_id=" . $id;
//                                        $this->db->query($qry);
                                    $this->db->delete('branch_xref_user', array("user_id" => $id));
                                    if (preg_match("/,/", $data['branch'])){
                                        foreach ($branches as $key => $val) {
                                            $this->db->select('branch_id');
                                            $this->db->from('branch_xref_user');
                                            $this->db->where('branch_id', $val);
                                            $this->db->where('user_id', $id);

                                            $res_all = $this->db->get();
                                            if ($res_all) {
                                                $res_all = $res_all->result();
                                                if (count($res_all) <= 0) {
                                                    $data_insert = [
                                                        'user_id' => $id,
                                                        'branch_id' => $val,
                                                        'days' => 0,
                                                        'updated_date' => date('Y-m-d H:i:s'),
                                                        'created_date' => date('Y-m-d H:i:s')
                                                    ];
                                                    $this->db->insert('branch_xref_user', $data_insert);
                                                }
                                            } else {
                                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                            }
                                        }
                                    } else {
                                        $data_insert = [
                                            'user_id' => $id,
                                            'branch_id' => $data['branch'],
                                            'days' => 0,
                                            'updated_date' => date('Y-m-d H:i:s'),
                                            'created_date' => date('Y-m-d H:i:s')
                                        ];
                                        $this->db->insert('branch_xref_user', $data_insert);
                                    }
                                    $this->db->trans_complete();
                                    if ($this->db->trans_status() === FALSE) {
                                        $this->db->trans_rollback();
                                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                                    } else {
                                        $this->db->trans_commit();
                                        return true;
                                    }
                                } else {
                                    return 'norolefound';
                                }
                            } else {
                                return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                            }
                        } else {
                            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                        }
                    } else {
                        return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }
            } else {
                return 'badrequest';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function checkUserExist($userId=''){
        $this->db->select('u.user_id as userID, u.login_id,r.fk_role_id,rf.`value` as `userType`,rf1.`value` as `userRole`');
        $this->db->from('user u');
	$this->db->join('user_xref_role r', 'r.fk_user_id = u.user_id');
	$this->db->join('reference_type_value rf1', 'rf1.reference_type_value_id = r.fk_role_id');
        $this->db->join('reference_type_value rf', 'rf.reference_type_value_id = u.fk_user_type_id');
        $this->db->where('u.user_id', $userId);
        $query = $this->db->get();
        if($query){
            $res=$query->result();
            if ($query->num_rows() > 0)
            {
                   $db_response=array("status"=>true,"message"=>"Valid User","data"=>$res);
            }
            else{
                   $db_response=array("status"=>false,"message"=>"Invalid User","data"=>array("message"=>"Invalid User"));
            }
        }
        else{
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array("message"=>$error['message']));
        }
        return $db_response;
    }

    public function usersListDataTables()
    {
        $table = 'user';
        // Table's primary key
        $primaryKey = 'u`.`user_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        //e.user_id, e.email, e.name, e.image, e.phone,e.employee_code,u.created_date, uxr.fk_role_id, rtv.value AS role,GROUP_CONCAT(b.`name`) AS branches,count(b.`name`) AS branchesCount
        $columns = array(
            array( 'db' => '`e`.`name`'                             ,'dt' => 'name'             ,'field' => 'name' ),
            array( 'db' => '`e`.`phone`'                            ,'dt' => 'phone'            ,'field' => 'phone' ),
            array( 'db' => '`e`.`email`'                            ,'dt' => 'email'            ,'field' => 'email' ),
            array( 'db' => '`e`.`employee_code`'                    ,'dt' => 'employee_code'    ,'field' => 'employee_code' ),
            array( 'db' => 'rtv.value AS role'                      ,'dt' => 'role'             ,'field' => 'role' ),
            array( 'db' => 'count(b.`name`) AS `branchesCount`'     ,'dt' => 'branchesCount'    ,'field' => 'branchesCount' ),
            array( 'db' => '`e`.`joining_date`'                     ,'dt' => 'joining_date'     ,'field' => 'joining_date' ),
            array( 'db' => '`u`.`created_date`'                     ,'dt' => 'created_date'     ,'field' => 'created_date', 'formatter' => function( $d, $row ) {
                    return date( 'd M,Y', strtotime($d));
                }),

            array( 'db' => '`e`.`user_id`'                          ,'dt' => 'user_id'          ,'field' => 'user_id' ),
            array( 'db' => '`e`.`image`'                            ,'dt' => 'image'            ,'field' => 'image' ),
            array( 'db' => '`uxr`.`fk_role_id`'                     ,'dt' => 'fk_role_id'       ,'field' => 'fk_role_id' ),
            array( 'db' => 'GROUP_CONCAT(" ",b.`name` ORDER BY b.name ASC) AS `branches`'   ,'dt' => 'branches'         ,'field' => 'branches' ),
            array( 'db' => 'if(`u`.`is_active`=1,1,0) as `is_active`','dt' => 'is_active'           ,'field' => 'is_active'),
        );

        $globalFilterColumns = array(
            array( 'db' => '`e`.`email`'                            ,'dt' => 'email'            ,'field' => 'email' ),
            array( 'db' => '`e`.`name`'                             ,'dt' => 'name'             ,'field' => 'name' ),
            array( 'db' => '`e`.`phone`'                            ,'dt' => 'phone'            ,'field' => 'phone' ),
            array( 'db' => '`e`.`employee_code`'                    ,'dt' => 'employee_code'    ,'field' => 'employee_code' ),
            array( 'db' => 'rtv.value'                      ,'dt' => 'role'             ,'field' => 'role' )
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = "FROM user AS u LEFT JOIN employee as e on u.user_id = e.user_id LEFT JOIN user_xref_role as uxr on u.user_id = uxr.fk_user_id LEFT JOIN branch_xref_user as bxu on bxu.user_id=u.user_id LEFT JOIN branch as b ON b.branch_id=bxu.branch_id LEFT JOIN reference_type_value as rtv on uxr.fk_role_id=rtv.reference_type_value_id";
        $extraWhere = "";
        $groupBy = "u.user_id";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        for($i=0;$i<count($responseData['data']);$i++)
        {
//            $responseData['data'][$i]['user_id']=encode($responseData['data'][$i]['user_id']);
            if(isset($responseData['data'][$i]['joining_date']) && strlen($responseData['data'][$i]['joining_date'])>0 && $responseData['data'][$i]['joining_date']!='' && $responseData['data'][$i]['joining_date']!='0000-00-00')
                $responseData['data'][$i]['joining_date']=(new DateTime($responseData['data'][$i]['joining_date']))->format('d M,Y');
            else
                $responseData['data'][$i]['joining_date'] = ' ---- ';
        }
        return $responseData;
    }
    function updateUserStatus($params){
        $userId=$params['userID'];
        $toStatus=(int)$params['toStatus'];
        $data_update_user=array('is_active'=>$toStatus);
        $this->db->where('user_id',$userId);
        $res=$this->db->update('user',$data_update_user);
        $status='';$message='';$data=array();

        if($res){
            $status=TRUE;$message="Updated successfully";$data=array('message'=>'Updated successfully');

        }
        else{
            $status=FALSE;$message="Failed! Please try again.";$data=array('message'=>'Failed! Please try again.');
        }
        $db_response=array("status"=>$status,"message"=>$message,"data"=>$data);
        return $db_response;


    }
    function updateUserDefaultBranch($params){
       $data_update=array('fk_default_branch_id'=>$params['branchId']);
        $this->db->where("user_id",$params['userId']);
        $res_update=$this->db->update('user',$data_update);
        if($res_update){
            $status=TRUE;$message='Success';$data=array("message"=>"Branch update successfully.");
        }
        else{
            $status=FALSE;$message='Failed';$data=array("message"=>"Branch update failed.");
        }
        $db_response=array("status"=>$status,"message"=>$message,"data"=>$data);
        return $db_response;

    }
}
