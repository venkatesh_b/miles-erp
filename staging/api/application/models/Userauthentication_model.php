<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userauthentication_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = FALSE;

    }
    function userLogin($data){
        
        $this->db->trans_begin();
        

        $this->db->select('u.user_id as userID, u.login_id ,r.fk_role_id,rf.`value` as `userType`,rf1.`value` as `userRole`');
        $this->db->from('user u');
		$this->db->join('user_xref_role r', 'r.fk_user_id = u.user_id');
		$this->db->join('reference_type_value rf1', 'rf1.reference_type_value_id = r.fk_role_id');
        $this->db->join('reference_type_value rf', 'rf.reference_type_value_id = u.fk_user_type_id');
        $this->db->where('login_id', $data['user_email_id']);
        $this->db->where('u.is_active',1);
        
        
        $query = $this->db->get();
        
        
        if($query){
            if ($query->num_rows() > 0)
            {
                foreach($query->result() as $k=>$v){
                    if($v->userID){
                        if($v->userType=='Employee'){
                            $data_update=array();
                            $data_update_user=array();
                            if(!empty($data['user_name'])){
                                $data_update['name']=$data['user_name'];
                            }
                            if(!empty($data['phone_number'])){
                                $data_update['phone']=$data['phone_number'];
                            }
                            if(!empty($data['profile_image'])){
                                $data_update['image']=$data['profile_image'];
                            }
                            if(!empty($data['user_dob'])){
                                $data_update['dob']=date('Y-m-d', strtotime($data['user_dob']));
                            }
                            if(trim($data['gender'])!=''){
                                $data_update['sex']=$data['gender'];
                            }
                            if(!empty($data['account_id'])){
                                $data_update_user['oauth_uid']=$data['account_id'];
                            }
                            
                            
                           if(count($data_update)>0){
                            $this->db->where('email', $data['user_email_id']);
                            $res_update=$this->db->update('employee', $data_update);
                            }
                            if(count($data_update_user)>0){
                            $this->db->where('user_id', $v->userID);
                            $res_update_user=$this->db->update('user', $data_update_user);
                            }
                            $data_login_history['fk_user_id']=$v->userID;
                            $data_login_history['logged_in_date_time']=date("Y-m-d H:i:s");
                            $data_login_history['ip']=$data['user_ipaddress'];
                            $data_login_history['browser']=$data['user_browser'];
                            $data_login_history['platform']=$data['user_platform'];
                            $res_insert_log=$this->db->insert('user_login_history', $data_login_history);

                            if(((isset($res_update) && $res_update)  || count($data_update)==0) && ($res_update_user || count($data_update_user)==0) && $res_insert_log)
                            {
                                $this->db->select('user_id as userID,email,name as userName,phone,image,dob,sex');
                                $this->db->where('user_id',$v->userID);
                                $employee_details=$this->db->get('employee');
                                if($employee_details)
                                {
                                  $employee_details_res=$employee_details->result();
                                     foreach($employee_details_res as $emp_k=>$emp_v)
                                     {
                                        foreach($emp_v as $subk=>$subv)
                                        {
                                            $mainarray[$subk]=$subv;
                                        }
                                    }
                                }
                                $mainarray['userID']=$v->userID;
                                $mainarray['userType']=$v->userType;
                                $mainarray['userRole']=$v->userRole;
                                $mainarray['email']=$v->login_id;
                                //$mainarray['role']=$v->userRole;
                                $db_response=array("status"=>true,"message"=>"success","data"=>$mainarray);
                            }
                            else{
                                //updation failed
                                $error = $this->db->error(); 
                                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                            }
                            
                        }
                        else{
                            
                            $db_response=array("status"=>true,"message"=>"success","data"=>array('userID' => $v->user_id, 'email' => $v->login_id,'userType'=>$v->userType, 'role' => $v->userRole,'username' =>$data['user_name']));
                        }
                        
                        
                    }else{
                        $db_response=array("status"=>false,"message"=>"No User Found!","data"=>array());
                    }
                }
           }
            else{
                
                $db_response=array("status"=>false,"message"=>"No User Found!","data"=>array());
            }
        }else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
            $this->db->trans_complete();
            
            
            if ($this->db->trans_status() === FALSE)
            {
                
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }

        return $db_response;
        
    }


    function userLogout($data)
    {
            $this->db->trans_begin();
            $logouttime=date('Y-m-d H:i:s');
            $data_update=array('log_out_date_time'=>$logouttime);
            $this->db->order_by("user_login_history_id", "desc");
            $this->db->limit(1);
            $this->db->where('fk_user_id', $data['userid']);
            $res_update=$this->db->update('user_login_history', $data_update);
            
            
            $query = $this->db->query('select oct.id from oauth_sessions oct,oauth_clients oc where oct.client_id=oc.id and  oc.user_id='.$data['userid'].' order by oct.id desc limit 1');
            
            
		$result = $query->result_array();
		if (count($result) === 1) {
			$existing_session_id=$result[0]['id'];
			$data_update=array("is_valid" => "0");
			
			$this->db->where('session_id', $existing_session_id);
			$this->db->where('is_valid', 1);
			$res_update_token=$this->db->update('oauth_access_tokens', $data_update);
			
			
		}
                
                
            if($res_update && $res_update_token){
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else{
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }  
            if ($this->db->trans_status() === FALSE)
            {
                
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }
        
        return $db_response;
    }

    public function getsecret_id($client_id = '') {

        $this->db->select('secret');
        $this->db->where('user_id', $client_id);
        $query = $this->db->get('oauth_clients');
        
        if($query){
            if ($query->num_rows() > 0) {
                $secretcodes = $query->result();
                return $secretcodes[0]->secret;
            } else {
                return FALSE;
            }
        }else{
            return 'dberror';
        }
    }

    public function getscopes($client_id = '') {

        $this->db->select('scope_id');
        $this->db->where('client_id', $client_id);
        $query = $this->db->get('oauth_client_scopes');
        
        if($query){
            if ($query->num_rows() > 0)
            {
                return $query->result();
            }else{
                return FALSE;
            }
        }else{
            return 'dberror';
        }
    }

    public function userCustomLogin($data){

        $this->db->trans_begin();

        $this->db->select('u.user_id as userID, u.login_id ,r.fk_role_id,rf.`value` as `userType`,rf1.`value` as `userRole`,e.name as username,e.image,e.sex');
        $this->db->from('user u');
        $this->db->join('employee e', 'e.user_id = u.user_id');
        $this->db->join('user_xref_role r', 'r.fk_user_id = u.user_id');
        $this->db->join('reference_type_value rf1', 'rf1.reference_type_value_id = r.fk_role_id');
        $this->db->join('reference_type_value rf', 'rf.reference_type_value_id = u.fk_user_type_id');
        $this->db->where('login_id', $data['user_email_id']);
        $this->db->where('u.is_active',1);

        $query = $this->db->get();
        if($query){
            if ($query->num_rows() > 0)
            {
                foreach($query->result() as $k=>$v){
                    if($v->userID)
                    {
                        if($v->userType=='Employee')
                        {
                            $data_login_history['fk_user_id']=$v->userID;
                            $data_login_history['logged_in_date_time']=date("Y-m-d H:i:s");
                            $data_login_history['ip']=$data['user_ipaddress'];
                            $data_login_history['browser']=$data['user_browser'];
                            $data_login_history['platform']=$data['user_platform'];
                            $res_insert_log=$this->db->insert('user_login_history', $data_login_history);
                            $db_response=array("status"=>true,"message"=>"success","data"=>array('userID' => $v->userID, 'email' => $v->login_id,'userType'=>$v->userType, 'role' => $v->userRole,'username' =>$v->username,'image' =>$v->image,'sex' =>$v->sex));
                        }
                        else
                        {
                            $db_response=array("status"=>true,"message"=>"success","data"=>array('userID' => $v->userID, 'email' => $v->login_id,'userType'=>$v->userType, 'role' => $v->userRole,'username' =>$v->username,'image' =>$v->image,'sex' =>$v->sex));
                        }
                    }
                    else
                    {
                        $db_response=array("status"=>false,"message"=>"No User Found!","data"=>array());
                    }
                }
            }
            else
            {

                $db_response=array("status"=>false,"message"=>"No User Found!","data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        $this->db->trans_complete();


        if ($this->db->trans_status() === FALSE)
        {

            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

        return $db_response;
    }
    
}

?>

