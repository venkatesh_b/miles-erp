<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @Modal Name      Venue
 * @category        Model
 * @author          Abhilash
 */
class Venue_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    
    public function getVenueById($id) {
        $this->db->select('*');
        $this->db->from('venue v');
        $this->db->where('v.venue_id', $id);
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }else{
                return 'badrequest';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function getAllVenue() {
        $this->db->select('v.venue_id, v.name,  rtv.reference_type_value_id city_id, rtv.value city');
        $this->db->from('venue v');
        $this->db->join('reference_type_value rtv', 'v.fk_city_id=rtv.reference_type_value_id');
        $this->db->where('v.is_active ', 1);
//        $this->db->join('refrense_type rt', 'rtv.reference_type_id=rt.reference_type_id');
        $result = $this->db->get();
        if ($result) {
            $result = $result->result();
            if (count($result) > 0) {
                return $result;
            }else{
                return 'nodata';
            }
        }else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function checkVenue($data,$venueid=0)
    {
        $this->db->select('venue_id');
        $this->db->from('venue v');
        $this->db->where('v.name ', $data['venueName']);
        $this->db->where('v.fk_city_id', $data['city']);
        if($venueid!=0)
        {
            $this->db->where('v.venue_id !=', $venueid);
        }
        $venue_res = $this->db->get();
        $num = $venue_res->num_rows();
        return $num;
    }
    public function saveVenue($data) {
        /* For City */
        $this->db->select('rtv.reference_type_value_id');
        $this->db->from('reference_type_value rtv');
        $this->db->join('reference_type rt', 'rtv.reference_type_id=rt.reference_type_id', 'left');
        $this->db->where('rtv.reference_type_value_id', $data['city']);
        $this->db->where('rt.name', 'city');
        $this->db->where('rtv.is_active', '1');
        $query = $this->db->get();
        if ($query)
        {
            //$query = $query->result_array();
            $valid_cty = $query->num_rows();
            if ($valid_cty == 0)
            {
                return 'nocity';
            }
            else
            {
                $venue_count=$this->checkVenue($data);
                if($venue_count > 0)
                {
                    return 'duplicate';
                }
                else
                {
                    $data_insert = array(
                        'name' => $data['venueName'],
                        'fk_city_id' => $data['city'],
                        'is_active' => '1',
                        'fk_created_by' => $_SERVER['HTTP_USER'],
                        'created_date' => date('Y-m-d H:i:s'),
                        'address' => $data['address']
                    );
                    return $this->db->insert('venue', $data_insert);
                }
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function editVenue($id, $data) {
        /*For Id*/
        $this->db->select('venue_id');
        $this->db->from('venue');
        $this->db->where('venue_id', $id);
        $valid_venue = $this->db->get();
        if ($valid_venue)
        {
            $valid_venue = $valid_venue->num_rows();
            //$query = $valid_venue->result_array();
            if ($valid_venue == 0)
            {
                return 'nodata';
            }
            else
            {
                $this->db->select('rtv.reference_type_value_id');
                $this->db->from('reference_type_value rtv');
                $this->db->join('reference_type rt', 'rtv.reference_type_id=rt.reference_type_id', 'left');
                $this->db->where('rtv.reference_type_value_id', $data['city']);
                $this->db->where('rt.name', 'city');
                $this->db->where('rtv.is_active', '1');
                $ref_cty = $this->db->get();
                if ($ref_cty)
                {
                    //$query = $query->result_array();
                    $ref_cty = $ref_cty->num_rows();
                    if ($ref_cty == 0)
                    {
                        return 'nocity';
                    }
                    else
                    {
                        $venue_count = $this->checkVenue($data,$id);
                        if($venue_count > 0)
                        {
                            return 'duplicate';
                        }
                        else
                        {
                            $data_update = array(
                                'name' => $data['venueName'],
                                'fk_city_id' => $data['city'],
                                'is_active' => '1',
                                'fk_updated_by' => $_SERVER['HTTP_USER'],
                                'updated_date' => date('Y-m-d H:i:s'),
                                'address' => $data['address']
                            );
                            $this->db->where(array('venue_id' => $id));
                            return $this->db->update('venue', $data_update);
                        }
                    }
                }
                else
                {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            }
        }
        else
        {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    public function deleteVenue($id) {
        $this->db->select('*');
        $this->db->from('venue');
        $this->db->where('venue_id', $id);

        $res_all = $this->db->get();

        if ($res_all) {
            $res_all = $res_all->result();

            if ($res_all) {

                $is_active = '';

                $is_active = !$res_all[0]->is_active;

                $data_update = array(
                    'is_active' => $is_active
                );
                
                $this->db->where('venue_id', $id);
                $res_update = $this->db->update('venue', $data_update);

                if ($this->db->error()['code'] == '0') {
                    if ($res_update) {
                        if ($is_active) {
                            return 'active';
                        } else {
                            return 'deactivated';
                        }
                    } else {
                        return false;
                    }
                } else {
                    return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
                }
            } else {
                return 'nodata';
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    
    function getVenueDatatables(){
        $table = 'venue';
        // Table's primary key
        $primaryKey = 'v`.`venue_id';
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => '`v`.`name`'                                     ,'dt' => 'name'               ,'field' => 'name' ),
            array( 'db' => '`r`.`value` AS city'                            ,'dt' => 'city'               ,'field' => 'city' ),
            array( 'db' => 'if(`v`.`is_active`=1,1,0) as `is_active`'    ,'dt' => 'is_active'        ,'field' => 'is_active' ),
            array( 'db' => '`v`.`address`'                                     ,'dt' => 'address'               ,'field' => 'address' ),
            array( 'db' => '`v`.`venue_id`'                                 ,'dt' => 'venue_id'           ,'field' => 'venue_id' )
        );

        $globalFilterColumns = array(
            array( 'db' => '`v`.`name`'    ,'dt' => 'name'            ,'field' => 'name' ),
            array( 'db' => '`r`.`value`'    ,'dt' => 'city'         ,'field' => 'city' ),
        );

        // SQL server connection information
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
        $joinQuery = " FROM venue v left join reference_type_value r ON v.fk_city_id=r.reference_type_value_id ";
        $extraWhere = "";
        $groupBy = "";

        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery, $extraWhere,$groupBy));

        return $responseData;
    }
    
}
