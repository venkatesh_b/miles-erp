<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-19
 * Time: 06:59 PM
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class WarehouseGrn_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db->db_debug = false;
        $this->load->helper('encryption');
    }
    public function checkGtnNoExist($param)
    {
        $this->db->select("stockflow_id");
        $this->db->from("stockflow");
        $this->db->where("sequence_number", $param['gtnNo']);
        $this->db->where('type','GTN');
        $this->db->where('requested_mode','branch');
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid Number',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function checkVendorGtnNoExist($vendorId)
    {
        $this->db->select("stockflow_id");
        $this->db->from("stockflow");
        $this->db->where("fk_receiver_id", $vendorId);
        $this->db->where('type','GTN');
        $this->db->where('requested_mode','warehouse');
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid Name',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getVendorGtnProductDetails($vendor)
    {
        $this->db->select('s.sequence_number,s.fk_requested_id,s.requested_mode,p.product_id,p.name,p.code,si.quantity');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','si.fk_stockflow_id= s.stockflow_id','left');
        $this->db->join('product p','si.fk_product_id=p.product_id','left');
        $this->db->where('s.fk_receiver_id',$vendor['vendorId']);
        $this->db->where('s.receiver_mode','vendor');
        $this->db->where('s.requested_mode','warehouse');
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            if(count($list) > 0)
            {
                foreach($list as $val)
                {
                    $list_final ['name'] =  $val->name;
                    $list_final ['request_mode'] = $val->requested_mode;
                    $list_final ['request_id'] = $val->fk_requested_id;
                    $list_final ['sequence_number'] = $val->sequence_number;
                }
                $list_final ['products'] = $list;
                $db_response=array("status"=>true,"message"=>"success","data"=>$list_final);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getGtnProductDetails($gtnNo)
    {
        $this->db->select('s.fk_requested_id,s.requested_mode,p.product_id,p.name,p.code,si.quantity,s.stockflow_id');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','si.fk_stockflow_id= s.stockflow_id','left');
        $this->db->join('product p','si.fk_product_id=p.product_id','left');
        $this->db->where('s.sequence_number',$gtnNo['gtnNo']);
        $list = $this->db->get();
        if($list)
        {
            $list = $list->result();
            if(count($list) > 0)
            {
                foreach($list as $val)
                {
                    $list_final ['name'] =  $val->name;
                    $list_final ['request_mode'] = $val->requested_mode;
                    $list_final ['request_id'] = $val->fk_requested_id;
                    $list_final ['stockflow_id'] = $val->stockflow_id;
                }
                $list_final ['products'] = $list;
                $db_response=array("status"=>true,"message"=>"success","data"=>$list_final);
            }
            else
            {
                $db_response=array("status"=>false,"message"=>"No Data Found","data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    public function getGtnNumbers($val)
    {
        $query = $this->db->query("SELECT CONCAT_WS('|',fk_requested_id,requested_mode) as id,sequence_number as text FROM stockflow WHERE sequence_number like '%".$val."%' and type='GTN' and requested_mode='branch' and `status`=0 ");
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    public function getProductList($val)
    {
        $query = $this->db->query("SELECT product_id as id,CONCAT_WS(' | ',code,name) as text FROM product WHERE code like '%".$val."%' or name like '%".$val."%'");
        if($query->num_rows()>0)
        {
            $data=$query->result_array();
            $db_response=array('status'=>true,'message'=>'success','data'=>$data);
        }
        else
        {
            $db_response=array('status'=>false,'message'=>'No Details Found','data'=>array());
        }
        return $db_response;
    }
    public function getVendorList($val)
    {
        $qry = "select inventory_vendor_id id, name as text from inventory_vendor where is_active=0 and (name like '%".$val."%' or `email` like '%".$val."%')";

        $query = $this->db->query($qry);
        if ($query) {
            $query = $query->result_array();
            if (count($query) > 0) {
                return $query;
            }
        } else {
            return array('error' => 'dberror', 'msg' => $error = $this->db->error()['message']);
        }
    }
    public function checkGtnExist($gtnNo,$receiverMode)
    {
        $this->db->select('sequence_number');
        $this->db->from('stockflow');
        $this->db->where('type','GTN');
        $this->db->where('sequence_number',$gtnNo);
        if(isset($receiverMode) && $receiverMode=='vendor')
            $this->db->where('receiver_mode',$receiverMode);
        else
            $this->db->where('requested_mode','branch');
        $query = $this->db->get();

        if($query->num_rows()>0)
        {
            $existStatus = true;
        }
        else
        {
            $existStatus = false;
        }
        return $existStatus;
    }
    public function getPreviousRecords()
    {
        $this->db->select('sequence_number');
        $this->db->from('stockflow');
        $this->db->where('type','GRN');
        $this->db->order_by('stockflow_id','desc');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $recordId = $query->row();
            $previousId = $recordId->sequence_number;
        }
        else{
            $previousId = 0;
        }
        return $previousId;
    }
    public function addWarehouseGrn($data)
    {
        $receiverMode = $data['receiverMode'];
        $receiverObjectId = $data['receiverObjectId'];
        $requestMode = $data['requestMode'];
        $requestObjectId = $data['requestObjectId'];
        $createdBy = $data['userID'];
        $invoiceNumber = $data['invoice_number'];
        $invoiceAttachment = $data['invoice_file'];
        $grnRemarks = $data['remarks'];
        $createdDate = date('Y-m-d H:i:s');
        $products = $data['products'];
        $gtnNo =$data['grnNo'];
        $stock_flow_id =$data['stock_flow_id'];
        if(trim($stock_flow_id)==''){
            $stock_flow_id=NULL;
        }
        $previousRecord = $this->getPreviousRecords();
        if($previousRecord == 0)
        {
            $sequenceNumber = 20001;
        }
        else{
            $sequenceNumber = $previousRecord+1;
        }
        $gtnExist = $this->checkGtnExist($gtnNo,$requestMode);
        if($gtnExist === true)
        {
            $this->db->where('sequence_number',$gtnNo);
            $updateGtn = $this->db->update('stockflow',array('type'=>'GTN','status'=>1));
            $grnId = $this->db->insert('stockflow',array('type'=>'GRN','sequence_number'=>$sequenceNumber,'receiver_mode'=>$receiverMode,'fk_receiver_id'=>$receiverObjectId,'requested_mode'=>$requestMode,'fk_requested_id'=>$requestObjectId,'created_by'=>$createdBy,'created_date'=>$createdDate,'invoice_number'=>$invoiceNumber,'invoice_attachment'=>$invoiceAttachment,'remarks'=>$grnRemarks,'fk_gtn_stockflow_id'=>$stock_flow_id));
            if($grnId)
            {
                $lastInsertedId = $this->db->insert_id();
                for($p=0;$p<count($products);$p++)
                {
                    $product = explode('|',$products[$p]);
                    $data = array(
                        'fk_stockflow_id'=>$lastInsertedId,
                        'fk_product_id'=>$product[0],
                        'quantity' => $product[1],
                    );
                    $this->db->insert('stockflow_item',$data);
                }
                $status = true;
                $message = 'Received successfully';
            }
            else{
                $error = $this->db->error();
                $status=FALSE;
                $message=$error['message'];
            }
        }
        else{
            $status =FALSE;
            $message = 'Invalid GTN No';
        }

        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function addWarehouseGrnFromVendor($data)
    {

        $receiverMode = $data['receiverMode'];
        $receiverObjectId = $data['receiverObjectId'];
        $requestMode = $data['requestMode'];
        $requestObjectId = $data['requestObjectId'];
        $invoiceNumber = $data['invoice_number'];
        $invoiceAttachment = $data['invoice_file'];
        $grnRemarks = $data['remarks'];
        $createdBy = $data['userID'];
        $createdDate = date('Y-m-d H:i:s');
        $products = explode(',',$data['products']);
        //need check received stock is less than request stock
        $previousRecord = $this->getPreviousRecords();
        if($previousRecord == 0)
        {
            $sequenceNumber = 20001;
        }
        else{
            $sequenceNumber = $previousRecord+1;
        }
        $grnId = $this->db->insert('stockflow',array('sequence_number'=>$sequenceNumber,'type'=>'GRN','receiver_mode'=>$receiverMode,'fk_receiver_id'=>$receiverObjectId,'requested_mode'=>$requestMode,'fk_requested_id'=>$requestObjectId,'created_by'=>$createdBy,'created_date'=>$createdDate,'invoice_number'=>$invoiceNumber,'invoice_attachment'=>$invoiceAttachment,'remarks'=>$grnRemarks,'approval_status'=>'pending','approval_by'=>$createdBy,'approval_date'=>$createdDate,'approval_remarks'=>'Pending for approval'));
        if($grnId)
        {
            $lastInsertedId = $this->db->insert_id();
            for($p=0;$p<count($products);$p++)
            {
                $product = explode('||',$products[$p]);
                $data = array(
                    'fk_stockflow_id'=>$lastInsertedId,
                    'fk_product_id'=>$product[0],
                    'quantity' => $product[1],
                );
                $this->db->insert('stockflow_item',$data);



                //check products already exist for branch or not
                //$productId = $this->getProductExistForBranch($product[0],$requestObjectId);
            }
            $data_ins_hist=array();
            $data_ins_hist['fk_stock_flow_id']=$lastInsertedId;
            $data_ins_hist['approval_status']='pending';
            $data_ins_hist['remarks']='Pending for approval';
            $data_ins_hist['created_by']=$createdBy;
            $data_ins_hist['created_on']=$createdDate;
            $res_ins=$this->db->insert('grn_approval_history',$data_ins_hist);
            $status = true;
            $message = 'Stock received successfully';
        }
        else{
            $error = $this->db->error();
            $status=FALSE;
            $message=$error['message'];
        }
        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function warehouseGrnDataTables()
    {
        $table = 'stockflow s';
        $primaryKey = 's`.`stockflow_id';
        $columns = array(
            array( 'db' => 'CONCAT_WS("","'.INVENTORY_WAREHOUSE_PREFIX.'",`s`.`sequence_number`) as new_sequence_number', 'dt' => 'new_sequence_number',          'field' => 'new_sequence_number' ),
            array( 'db' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA"))))) as receiveName',              'dt' => 'receiveName',           'field' => 'receiveName'),
            array( 'db' => '`s`.`created_date` as receivedOn',                     'dt' => 'receivedOn',          'field' => 'receivedOn'),
            array( 'db' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA"))))) as requestedName',              'dt' => 'requestedName',           'field' => 'requestedName'),
            array( 'db' => '`s`.`created_date` as sentOn',                     'dt' => 'sentOn',          'field' => 'sentOn' ),
            array( 'db' => 'sum(si.quantity) as total',                 'dt' => 'total',                    'field' => 'total' ),
            array( 'db' => 's.invoice_number',                 'dt' => 'invoice_number',                    'field' => 'invoice_number' ),
            array( 'db' => 'if(s.requested_mode="vendor",s.approval_status,"----") as approval_status',              'dt' => 'approval_status',           'field' => 'approval_status'),
            array( 'db' => 'if(s.requested_mode="vendor",s.approval_remarks,"----") as approval_remarks',              'dt' => 'approval_remarks',           'field' => 'approval_remarks'),
            array( 'db' => 's.invoice_attachment',                 'dt' => 'invoice_attachment',                    'field' => 'invoice_attachment' ),
            array( 'db' => 'sum(ifnull(si.returned_quantity,0)) as returnTotal',  'dt' => 'returnTotal',              'field' => 'returnTotal'),
            array( 'db' => '`s`.`stockflow_id`',                     'dt' => 'stockflow_id',          'field' => 'stockflow_id' ),
            array( 'db' => '`s`.`sequence_number`',                     'dt' => 'sequence_number',          'field' => 'sequence_number' ),
            array( 'db' => 'if(s.requested_mode="vendor",(if(s.approval_status="approved",0,1)),0) as is_editable',              'dt' => 'is_editable',           'field' => 'is_editable')

        );
        $globalFilterColumns = array(
            array( 'db' => 'CONCAT_WS("","'.INVENTORY_WAREHOUSE_PREFIX.'",`s`.`sequence_number`)', 'dt' => 'new_sequence_number',          'field' => 'new_sequence_number' ),
            array( 'db' => '`s`.`sequence_number`',            'dt' => 'sequence_number',         'field' => 'sequence_number' ),
            array( 'db' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))',              'dt' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))',           'field' => 'if(s.receiver_mode="branch",(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_receiver_id),"NA")))))'),
            array( 'db' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))',              'dt' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))',           'field' => 'if(s.requested_mode="branch",(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode="vendor",(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode="warehouse",(select name from warehouse where warehouse_id=s.fk_requested_id),"NA")))))'),
            array( 'db' => '`s`.`created_date`',                     'dt' => 'created_date',          'field' => 'created_date' ),
            array( 'db' => 's.invoice_number',                 'dt' => 'invoice_number',                    'field' => 'invoice_number' ),
            array( 'db' => 's.approval_status',                 'dt' => 'approval_status',                    'field' => 'approval_status' ),
        );
        $sql_details = array(
            'user' => SITE_HOST_USERNAME,
            'pass' => SITE_HOST_PASSWORD,
            'db'   => SITE_DATABASE,
            'host' => SITE_HOST_NAME
        );
        $joinQuery = "from stockflow s JOIN stockflow_item si ON s.stockflow_id=si.fk_stockflow_id";
        $extraWhere = "s.type='GRN' and s.receiver_mode='warehouse'";
        $groupBy = "s.stockflow_id";
        $responseData=(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $globalFilterColumns, $joinQuery,$extraWhere,$groupBy));
        for($i=0;$i<count($responseData['data']);$i++)
        {
            $responseData['data'][$i]['receivedOn']=date('j M, Y',strtotime($responseData['data'][$i]['receivedOn']));
            $responseData['data'][$i]['sentOn']=date('j M, Y',strtotime($responseData['data'][$i]['sentOn']));
            $responseData['data'][$i]['approval_status']=ucfirst($responseData['data'][$i]['approval_status']);
            $responseData['data'][$i]['invoice_attachment']=urlencode(basename('./'.$responseData['data'][$i]['invoice_attachment']));
        }
        return $responseData;
    }
    function getWareHouseStockProductWise($type,$productId=0){
        if($type=='outward'){
            $wareHouse_query="SELECT s.created_date,si.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(quantity) as quantity,IFNULL(s.remarks,'') as remarks,if(s.receiver_mode='branch',(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_receiver_id),'NA'))))) as receiveName,
                          if(s.requested_mode='branch',(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_requested_id),'NA'))))) as requestedName,if(s.status=0,'In Transit','Delivered') as deliverStatus FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=$productId group by s.stockflow_id";
        }
        elseif($type=='inward'){
            $wareHouse_query="SELECT s.created_date,si.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(quantity-returned_quantity) as quantity,IFNULL(s.remarks,'') as remarks,if(s.receiver_mode='branch',(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_receiver_id),'NA'))))) as receiveName,
if(s.requested_mode='branch',(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_requested_id),'NA'))))) as requestedName FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=$productId and s.requested_mode!='vendor' group by s.stockflow_id
UNION ALL
SELECT s.created_date,si.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(quantity-returned_quantity) as quantity,IFNULL(s.remarks,'') as remarks,if(s.receiver_mode='branch',(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_receiver_id),'NA'))))) as receiveName,
if(s.requested_mode='branch',(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_requested_id),'NA'))))) as requestedName FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=$productId and s.requested_mode='vendor' and s.approval_status='approved' group by s.stockflow_id";
        }
        elseif($type=='liquidate'){
            $wareHouse_query="SELECT s.created_date,si.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(quantity-returned_quantity) as quantity,IFNULL(s.remarks,'') as remarks,if(s.receiver_mode='branch',(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_receiver_id),'NA'))))) as receiveName,
if(s.requested_mode='branch',(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_requested_id),'NA'))))) as requestedName FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=$productId and s.requested_mode!='vendor' group by s.stockflow_id
UNION ALL
SELECT s.created_date,si.fk_product_id as productId,p.name as productName,rtv.value as producType,sum(quantity-returned_quantity) as quantity,IFNULL(s.remarks,'') as remarks,if(s.receiver_mode='branch',(select name from branch where branch_id=s.fk_receiver_id),(if(s.receiver_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_receiver_id),(if(s.receiver_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_receiver_id),'NA'))))) as receiveName,
if(s.requested_mode='branch',(select name from branch where branch_id=s.fk_requested_id),(if(s.requested_mode='vendor',(select name from inventory_vendor where inventory_vendor_id=s.fk_requested_id),(if(s.requested_mode='warehouse',(select name from warehouse where warehouse_id=s.fk_requested_id),'NA'))))) as requestedName FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and si.fk_product_id=$productId and s.requested_mode='vendor' and s.approval_status='approved' group by s.stockflow_id";
        }
        $res=$this->db->query($wareHouse_query);

        if($res){
            if($res->num_rows()>0){
                $rows=$res->result();
                foreach($rows as $k=>$v){
                    $v->created_date=date('d M, Y',strtotime($v->created_date));
                }
                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;
    }
    function getWareHouseStock(){
        $warehousestock_query="SELECT productId,productName,producType,sum(received) as received,sum(dispatched) as dispatched,sum(received-dispatched) as inHand,sum(intransit) as inTransit,remarks from (
SELECT si.fk_product_id as productId,p.name as productName,rtv.value as producType,0 as received,sum(quantity) as dispatched,0 as intransit,s.remarks FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id group by si.fk_product_id
UNION ALL
SELECT si.fk_product_id as productId,p.name as productName,rtv.value as producType,0 as received,0 as dispatched,sum(quantity) as intransit,s.remarks FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and s.`status`=0 group by si.fk_product_id
UNION ALL
SELECT si.fk_product_id as productId,p.name,rtv.value as producType,sum(if(s.requested_mode='vendor',(if(s.approval_status='approved',(quantity-returned_quantity),0)),(quantity-returned_quantity))) as received,0 as dispatched,0 as intransit,s.remarks FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id group by si.fk_product_id) p group by 1;";

        $res=$this->db->query($warehousestock_query);

        if($res){
            if($res->num_rows()>0){
                $rows=$res->result();

                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;
    }

    function getWareHouseStockByProductId($productId){
        $warehousestock_query="SELECT productId,productName,producType,sum(received) as received,sum(dispatched) as dispatched,sum(received-dispatched) as inHand,sum(intransit) as inTransit,remarks from (
SELECT si.fk_product_id as productId,p.name as productName,rtv.value as producType,0 as received,sum(quantity) as dispatched,0 as intransit,s.remarks FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id group by si.fk_product_id
UNION ALL
SELECT si.fk_product_id as productId,p.name as productName,rtv.value as producType,0 as received,0 as dispatched,sum(quantity) as intransit,s.remarks FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GTN' and s.requested_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id and s.`status`=0 group by si.fk_product_id
UNION ALL
SELECT si.fk_product_id as productId,p.name,rtv.value as producType,sum(if(s.requested_mode='vendor',(if(s.approval_status='approved',(quantity-returned_quantity),0)),(quantity-returned_quantity))) as received,0 as dispatched,0 as intransit,s.remarks FROM `stockflow` s ,stockflow_item si,product p,reference_type_value rtv WHERE s.type='GRN' and s.receiver_mode='warehouse' and si.fk_stockflow_id=s.stockflow_id and si.fk_product_id=p.product_id and rtv.reference_type_value_id=p.fk_product_type_id group by si.fk_product_id) p WHERE p.productId = ".$productId." group by 1;";

        $res=$this->db->query($warehousestock_query);

        if($res){
            if($res->num_rows()>0){
                $rows=$res->result();

                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;
    }
    public function liquidateWarehouseStockByProduct($productId,$liquidateStock)
    {
        $receiverMode = 'warehouse';
        $receiverObjectId = 1;
        $requestMode = 'warehouse';
        $requestObjectId = 1;
        $gtnRemarks= 'Liquidation';
        $weight = '0';
        $invoiceNumber = 0;
        $invoiceAttachment = '';
        $createdBy=$_SERVER['HTTP_USER'];
        $createdDate = date('Y-m-d H:i:s');

        $previousRecord = $this->getPreviousRecords();
        if($previousRecord == 0)
        {
            $sequenceNumber = 10001;
        }
        else
        {
            $sequenceNumber = $previousRecord+1;
        }

        $grnId = $this->db->insert('stockflow',array('sequence_number'=>$sequenceNumber,'type'=>'GTN','receiver_mode'=>$receiverMode,'fk_receiver_id'=>$receiverObjectId,'requested_mode'=>$requestMode,'fk_requested_id'=>$requestObjectId,'remarks'=>$gtnRemarks,'weight'=>$weight,'invoice_number'=>$invoiceNumber,'invoice_attachment'=>$invoiceAttachment,'created_by'=>$createdBy,'created_date'=>$createdDate,'status'=>'1','approval_status'=>'approved','approved_by'=>1));
        if($grnId)
        {
            $lastInsertedId = $this->db->insert_id();
            $data = array(
                'fk_stockflow_id'=>$lastInsertedId,
                'fk_product_id'=>$productId,
                'quantity' => $liquidateStock
            );
            $this->db->insert('stockflow_item',$data);
            $status = true;
            $message = 'Transferred successfully';
        }
        else
        {
            $error = $this->db->error();
            $status=FALSE;
            $message=$error['message'];
        }
        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
    public function getWareHouseGrnProducts($seqNum)
    {
        $this->db->select('si.quantity,p.`name` as productName');
        $this->db->from('stockflow s');
        $this->db->join('stockflow_item si','s.stockflow_id=si.fk_stockflow_id','left');
        $this->db->join('product p','p.product_id=si.fk_product_id');
        $this->db->where('s.sequence_number',$seqNum);
        $query = $this->db->get();
        if($query){
            if($query->num_rows()>0){
                $rows=$query->result();
                $db_response=array('status'=>true,'message'=>'success','data'=>$rows);
            }
            else{
                $db_response=array('status'=>false,'message'=>'no records','data'=>array());
            }
        }
        else{
            $db_response=array('status'=>false,'message'=>'no records','data'=>array());
        }
        return $db_response;
    }
    public function checkInvoiceNoExist($param)
    {
        $this->db->select("stockflow_id");
        $this->db->from("stockflow");
        $this->db->where("invoice_number", $param['invoiceNumber']);
        if(isset($param['not_stockFlowId']) && $param['not_stockFlowId']!=''){
            $this->db->where("stockflow_id!=", $param['not_stockFlowId']);
        }
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $db_response=array("status"=>false,"message"=>'Invoice number already exist',"data"=>array("invoiceNumber"=>'Invoice number already exist'));
            }
            else
            {
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
        }
        else
        {
            $error = $this->db->error();
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        return $db_response;
    }
    function getGrnDetails($stockFlowId){
        $this->db->select("s.stockflow_id,s.fk_requested_id,s.invoice_number,s.invoice_attachment,s.remarks,CONCAT_WS('','".INVENTORY_WAREHOUSE_PREFIX."',`s`.`sequence_number`) as sequence_number");
        $this->db->from("stockflow s");
        $this->db->where("s.stockflow_id", $stockFlowId);
        $list = $this->db->get();
        if($list)
        {
            if($list->num_rows()>0)
            {
                $dlist=$list->result();
                foreach($dlist as $key=>$val){
                    $val->invoice_attachment=urlencode(basename('./'.$val->invoice_attachment));
                    $this->db->select('si.fk_product_id,si.quantity,p.code');
                    $this->db->from("stockflow_item si");
                    $this->db->join("product p",'p.product_id=si.fk_product_id');
                    $this->db->where("si.fk_stockflow_id", $val->stockflow_id);
                    $prlist = $this->db->get();
                    $val->products=$prlist->result();
                }
                $db_response=array("status"=>true,"message"=>'success',"data"=>$dlist[0]);

            }
            else
            {
                $db_response=array("status"=>false,"message"=>'Invalid GRN',"data"=>array());
            }
        }
        else
        {

            $db_response=array("status"=>false,"message"=>'Invalid GRN',"data"=>array());
        }
        return $db_response;
    }
    public function editWarehouseGrnFromVendor($data)
    {
        $receiverMode = $data['receiverMode'];
        $receiverObjectId = $data['receiverObjectId'];
        $requestMode = $data['requestMode'];
        $requestObjectId = $data['requestObjectId'];
        $invoiceNumber = $data['invoice_number'];
        $invoiceAttachment = $data['invoice_file'];
        $grnRemarks = $data['remarks'];
        $createdBy = $data['userID'];
        $createdDate = date('Y-m-d H:i:s');
        $products = explode(',',$data['products']);
        $stockFlowId=$data['stockFlowId'];
        $this->db->where('stockflow_id',$stockFlowId);
        $data_update_stockflow=array('requested_mode'=>$requestMode,'fk_requested_id'=>$requestObjectId,'updated_by'=>$createdBy,'updated_date'=>$createdDate,'invoice_number'=>$invoiceNumber,'remarks'=>$grnRemarks,'approval_status'=>'pending','approval_by'=>$createdBy,'approval_date'=>$createdDate,'approval_remarks'=>'Pending for approval');
        if($invoiceAttachment!='NA')
        $data_update_stockflow['invoice_attachment']=$invoiceAttachment;
        $grnId = $this->db->update('stockflow',$data_update_stockflow);
        if($grnId)
        {
            $lastInsertedId = $stockFlowId;
            $this->db->where('fk_stockflow_id',$lastInsertedId);
            $this->db->delete('stockflow_item');

            for($p=0;$p<count($products);$p++)
            {
                $product = explode('||',$products[$p]);
                $data = array(
                    'fk_stockflow_id'=>$lastInsertedId,
                    'fk_product_id'=>$product[0],
                    'quantity' => $product[1],
                );
                $this->db->insert('stockflow_item',$data);
                //check products already exist for branch or not
                //$productId = $this->getProductExistForBranch($product[0],$requestObjectId);
            }
                $data_ins_hist=array();
                $data_ins_hist['fk_stock_flow_id']=$lastInsertedId;
                $data_ins_hist['approval_status']='pending';
                $data_ins_hist['remarks']='Pending for approval';
                $data_ins_hist['created_by']=$createdBy;
                $data_ins_hist['created_on']=$createdDate;
                $res_ins=$this->db->insert('grn_approval_history',$data_ins_hist);
            $status = true;
            $message = 'Stock updated successfully';
        }
        else{
            $error = $this->db->error();
            $status=FALSE;
            $message=$error['message'];
        }
        return array("status"=>$status,"message"=>$message,"data"=>[]);
    }
}

