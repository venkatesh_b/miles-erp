<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 7/7/16
 * Time: 10:51 AM
 */
include_once("config.php");
$fh = fopen('response.log', 'a+');
$sendgrid_response=$HTTP_RAW_POST_DATA;
$sendgrid_response=json_decode($HTTP_RAW_POST_DATA);
//$response_data='SENDGRID ---- '.count($sendgrid_response).'----'.$sendgrid_response[0]->marketing_campaign_id.'----'.date('Y-m-d H:i:s', strtotime($sendgrid_response[0]->timestamp)).'----@@@@'.$HTTP_RAW_POST_DATA;

if ( $fh )
{
// Dump body
    fwrite($fh, print_r($HTTP_RAW_POST_DATA, true));
    fclose($fh);
}
//TEST RESPONSE
// Create connection
$conn = mysqli_connect(SITE_HOST_NAME, SITE_HOST_USERNAME, SITE_HOST_PASSWORD, SITE_DATABASE);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sendgrid_campaign_ids = [];
$current_datetime=date('Y-m-d H:i:s');
for($i=0;$i<count($sendgrid_response);$i++)
{
    $camp_query = "SELECT campaign_id,`status`,sendgrid_campaign_id FROM campaign WHERE sendgrid_campaign_id='".$sendgrid_response[$i]->marketing_campaign_id."' AND `status`='started'";
    $camp_status_res = $conn->query($camp_query);
    if ($camp_status_res->num_rows == 0)
    {
        $update_camp_query = "UPDATE campaign SET `status`='started',updated_date='".$current_datetime."' WHERE sendgrid_campaign_id='".$sendgrid_response[$i]->marketing_campaign_id."'";
        $camp_update_res = $conn->query($update_camp_query);
    }
    $update_camp_contact_query = "UPDATE campaign_contact cc JOIN campaign cp ON cp.campaign_id=cc.fk_campaign_id SET cc.`status`='".$sendgrid_response[$i]->event."',cc.updated_date = '".$current_datetime."',cc.`sendgrid_status`='".$sendgrid_response[$i]->event."' WHERE cc.email='".$sendgrid_response[$i]->email."' AND cp.sendgrid_campaign_id='".$sendgrid_response[$i]->marketing_campaign_id."'";
    $camp_contact_status_res = $conn->query($update_camp_contact_query);
    if(!in_array($sendgrid_response[$i]->marketing_campaign_id, $sendgrid_campaign_ids)){
        $sendgrid_campaign_ids[]=$sendgrid_response[$i]->marketing_campaign_id;
    }
}
$update_camp_query = "UPDATE campaign SET `status`='completed',updated_date='".$current_datetime."' WHERE sendgrid_campaign_id='".$sendgrid_response[$i]->marketing_campaign_id."'";
$camp_update_res = $conn->query($update_camp_query);