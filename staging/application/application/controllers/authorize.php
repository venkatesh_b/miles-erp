<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 18/2/16
 * Time: 12:20 PM
 */
class Authorize extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('encrypt');
        $this->load->library('email');
    }
    public function index()
    {
        if(!isset($_POST['accountId']))
        {
            echo "-1";
        }
        else
        {
            $params = array('emailId'=>$_POST['emailId'], 'userName'=>$_POST['userName'], 'gender'=>$_POST['gender'], 'profileImage'=>$_POST['profileImage'], 'accountId'=>$_POST['accountId']);
            $url=API_URL.'index.php/Userauthentication/Login';
            $post_field_string = http_build_query($params, '', '&');

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field_string);

            curl_setopt($ch, CURLOPT_POST, true);
            
            curl_setopt($ch,CURLOPT_HTTPHEADER,array('context: default'));


            $response = curl_exec($ch);

            curl_close ($ch);

            $json_data=json_decode($response);

            if($json_data->status == true)
            {

                $user_data=$json_data->data;
                $userData = array();
                $accessToken=$userData['accessToken']=$user_data->accessToken;
                $userId=$userData['userId']=$user_data->userID;
                $userData['Name']=$user_data->userName;
                $userData['Role']=$user_data->userRole;
                $userData['profileImage']=$user_data->image;
                $userData['gender']=$user_data->sex;
                $this->session->set_userdata('userData',$userData);
                $headers = array();
                $headers[] = "context: default";
                $headers[] = "Authorizationtoken: Bearer $accessToken";
                $headers[] = "user: $userId";
                $url=API_URL.'index.php/ApplicationMenu/getApplicationMenu/Userid/'.$user_data->userID;
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                $output=curl_exec($ch);
                curl_close($ch);
               $menu_output=json_decode($output);
                if($menu_output->status == true)
                {
                    $menuData=[];
                    if(isset($menu_output->data->AppMenu) && count($menu_output->data->AppMenu)>0)
                    {
                        $menuData=$menu_output->data->AppMenu;
                    }
                    if($menuData!=null && count($menuData)>0){
                        if(isset($menuData[0]->childs) && $menuData[0]->childs!=null && count($menuData[0]->childs)>0)
                            $this->session->set_userdata('indexURL',$menuData[0]->childs[0]->url);
                        else
                            $this->session->set_userdata('indexURL',$menuData[0]->url);
                    }else{
                        $this->session->set_userdata('indexURL','nomenu');
                    }
                }
                $headerDetails=array();
                $headerDetails['accessToken']='Bearer '.$accessToken;
                $headerDetails['context']='default';
                $headerDetails['user']=$userId;
                $data=array("status"=>$menu_output->status,"message"=>$this->session->userdata('indexURL'),"data"=>$headerDetails);
                echo json_encode($data);
            }
            else
            {
                echo -1;
            }
        }
    }

    public function customLogin()
    {
        $params = array('emailId'=>$_POST['loginEmail'], 'loginPassword'=>$_POST['loginPassword']);
        $url=API_URL.'index.php/Userauthentication/CustomLogin';
        $post_field_string = http_build_query($params, '', '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field_string);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch,CURLOPT_HTTPHEADER,array('context: default'));


        $response = curl_exec($ch);

        curl_close ($ch);

        $json_data=json_decode($response);
       // echo $response;exit;

        if($json_data->status == true)
        {
            $user_data=$json_data->data;
            $userData = array();
            $accessToken=$userData['accessToken']=$user_data->accessToken;
            $userId=$userData['userId']=$user_data->userID;
            $userData['Name']=$user_data->username;
            $userData['Role']=$user_data->role;
            $userData['profileImage']=$user_data->image;
            $userData['gender']=$user_data->sex;
            $this->session->set_userdata('userData',$userData);
            $headers = array();
            $headers[] = "context: default";
            $headers[] = "Authorizationtoken: Bearer $accessToken";
            $headers[] = "user: $userId";
            $url=API_URL.'index.php/ApplicationMenu/getApplicationMenu/Userid/'.$user_data->userID;
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $output=curl_exec($ch);
            curl_close($ch);
            $menu_output=json_decode($output);
            if($menu_output->status == true)
            {
                $menuData=[];
                if(isset($menu_output->data->AppMenu) && count($menu_output->data->AppMenu)>0)
                {
                    $menuData=$menu_output->data->AppMenu;
                }
                if($menuData!=null && count($menuData)>0){
                    if(isset($menuData[0]->childs) && $menuData[0]->childs!=null && count($menuData[0]->childs)>0)
                        $this->session->set_userdata('indexURL',$menuData[0]->childs[0]->url);
                    else
                        $this->session->set_userdata('indexURL',$menuData[0]->url);
                }else{
                    $this->session->set_userdata('indexURL','nomenu');
                }
            }
            $headerDetails=array();
            $headerDetails['accessToken']='Bearer '.$accessToken;
            $headerDetails['context']='default';
            $headerDetails['user']=$userId;
            $data=array("status"=>$menu_output->status,"message"=>$this->session->userdata('indexURL'),"data"=>$headerDetails);
            echo json_encode($data);
        }
        else
        {
            echo -1;
        }
    }
}