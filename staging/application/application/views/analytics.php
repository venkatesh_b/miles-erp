<div class="contentpanel">
    <div class="content-header-wrap">
        <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
        <h3 class="content-header">Analytics </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="tooltipped" data-position="top" onclick="treportsFilters()" data-tooltip="Reports filter" class="viewsidePanel" href="javascript:;"><i class="icon-filter" ></i></a></li>
            </ul>
        </div>
    </div>
    <div class="content-body" id="contentBody">
        <div class="fixed-wrap clearfix">
            <div class="reports-filter-widget clearfix">
                <form id="analyticFilterForm">
                    <h4 class="heading-uppercase font-bold p10 mt0">Filters<a class="reports-filters-close" onclick="treportsFiltersClose()"><span class="icon-times"></span></a></h4>
                    <!--<div class="col-sm-12 clearfix mb10">
                        <div class="col-sm-2 col-md-1 mt10 text-right heading-uppercase font-bold pr0">
                            Courses
                        </div>
                        <div class="col-sm-10 col-md-11">
                            <div class="input-field col-sm-12 p0 custom-multiselect-wrap">
                                <select multiple class="chosen-select" id="ddlCourseList" data-placeholder="Choose Courses">
                                    <option value="">--Select--</option>
                                </select>
                                <label class="select-label">Courses</label>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-sm-12 mt10 clearfix">
                        <div class="col-sm-2 col-md-1 mt10 text-right heading-uppercase font-bold pr0">
                            Branches
                        </div>
                        <div class="col-sm-10 col-md-11">
                            <div class="input-field custom-multiselect-wrap">
                                <select multiple class="chosen-select" id="ddlBranchesList" data-placeholder="Choose Branches">
                                    <option value="" disabled>--Select--</option>
                                </select>
                                <label class="select-label">Branches</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 mt10">
                        <div class="col-sm-2 col-md-1 mt10 text-right heading-uppercase font-bold pr0">
                            Period
                        </div>
                        <div class="col-sm-10 col-md-11">
                            <div class="input-field col-sm-3 mt0 p0 mr15">
                                <input id="txtFromDate" type="date" placeholder="dd/mm/yyyy" class="datepicker relative enablePastDatesWithCurrentDate" />
                                <label class="datepicker-label datepicker" for="txtFromDate">From Date</label>
                            </div>
                            <div class="input-field col-sm-3 mt0 p0">
                                <input id="txtToDate" type="date" placeholder="dd/mm/yyyy" class="datepicker relative enablePastDatesWithCurrentDate" />
                                <label class="datepicker-label datepicker" for="txtToDate">To Date</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-2 col-md-1"></div>
                        <div class="col-sm-10 col-md-11 col-sm-offset-2 col-md-offset-1">
                        <button id="btnSaveContact" onclick="generateAnalyticsReport()" class="btn blue-btn" type="button" value="Add"><i class="icon-right mr8"></i>Generate Report</button>
                        <button data-dismiss="modal" class="btn blue-light-btn" type="button"><i class="icon-times mr8"></i>Cancel</button>
                        <button class="btn blue-light-btn" type="button" value="Clear" onclick="clearAnalyticsReport()"><i class="icon-trash mr8"></i>Clear All</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="report-status-widget-wrap clearfix">
                <div class="report-status-widget">
                    <div>
                        <span>#Branches</span>
                        <span id="branchesCount">09</span>
                    </div>

                </div>
                <div class="report-status-widget">
                    <div>
                        <span>#Courses</span>
                        <span id="courseCount">09</span>
                    </div>
                </div>
                <div class="report-status-widget">
                    <div>
                        <span>#Collection</span>
                        <span class="currency-rupee"><i class="fa fa-rupee"></i><span id="collectedAmount">1,23,70,456</span></span>
                    </div>
                </div>
                <div class="report-status-widget">
                    <div>
                        <span>#Dues</span>
                        <span class="currency-rupee"><i class="fa fa-rupee"></i><span id="dueAmount">1,23,70,456</span></span>
                    </div>
                </div>
            </div>
            <div class="reports-graphs-widget mt15">
                <div class="col-sm-12 p0">
                    <div class="col-sm-4 p0 white-bg clearfix">
                        <div class="white-bg mr10 p10">
                            <h3 class="heading-uppercase font-bold plr15 mt0">Yearly Report</h3>
                            <div class="reports-graphs-widget-item">
                                <div id="graphOne" style="width:95%; height: 280px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 pr0">
                        <div class="white-bg mr0 p10">
                            <h3 class="heading-uppercase font-bold plr15 mt0">Branches Target</h3>
                            <div class="reports-graphs-widget-item">
                                <div id="graphTwo" style="width:95%; height: 280px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p0 mt15" id="defaultGraphs">
                    <div class="col-sm-4 p0 clearfix">
                        <div class="white-bg mr10 p10">
                            <h3 class="heading-uppercase font-bold plr15 mt0">Courses Percentage</h3>
                            <div class="reports-graphs-widget-item">
                                <div id="graphThree" style="width:95%; height: 280px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 clearfix pr0">
                        <div class="white-bg mr10 p10">
                            <h3 class="heading-uppercase font-bold plr15 mt0">Yearly Report</h3>
                            <div class="reports-graphs-widget-item">
                                <div id="graphFour" style="width:95%; height: 280px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    docReady(function(){
        $(".chosen-select").chosen();
        analyticsFilters();
        analyticsWidgets(0,0,0);
        leadStudentsYearlyReport(0,0,0);
        leadStudentCourseBranchWiseReport(0,0,0);
        achievedLeadsCourseWiseReport();
        leadStudentMonthlyReport();
    });
    $('#ddlBranchesList').change(function ()
    {
        multiSelectChosenDropdown($(this));
    });
    function treportsFilters()
    {
        $('.reports-filter-widget').slideToggle();
    }
    function treportsFiltersClose()
    {
        $('.reports-filter-widget').slideUp();
    }
    function analyticsFilters()
    {
        var ajaxurl = API_URL + 'index.php/Analytics/getAnalyticsFilters';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: analyticsFiltersResponse});
    }
    function analyticsFiltersResponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            var filtersData=response.data;
            /*$('#ddlCourseList').empty();
            $('#ddlCourseList').append($('<option disabled></option>').val('').html('--Select--'));
                if (filtersData.courses.length > 0) {
                    $.each(filtersData.courses, function (key, value) {
                        $('#ddlCourseList').append($('<option></option>').val(value.course_id).html(value.course_name));
                    });
                    $('#ddlCourseList').trigger("chosen:updated");
                }
            $("#ddlCourseList").chosen({
                no_results_text: "Oops, nothing found!",
                width: "95%"
            });*/

            $('#ddlBranchesList').empty();
            $('#ddlBranchesList').append($('<option disabled></option>').val('').html('--Select--'));
            if (filtersData.branches.length > 0) {
                $.each(filtersData.branches, function (key, value) {
                    $('#ddlBranchesList').append($('<option></option>').val(value.branch_id).html(value.branch_name));
                });
                $('#ddlBranchesList').trigger("chosen:updated");
            }
            $("#ddlBranchesList").chosen({
                no_results_text: "Oops, nothing found!",
                width: "95%"
            });
        }
    }
    function generateAnalyticsReport()
    {
        var flag=0;
        var selMulti='',branchIds='',fromData='',toDate='';
        $("#analyticFilterForm #ddlBranchesList").parent().parent().removeClass("required");
        /*
        selMulti = $.map($("#ddlCourseList option:selected"), function (el, i) {
            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).val();
            }
        });
        courseIds = selMulti.join(",");*/
        selMulti = $.map($("#ddlBranchesList option:selected"), function (el, i) {
            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).val();
            }
        });
        branchIds = selMulti.join(",");
        /*if (courseIds == '')
        {
            $("#ddlCourseList").parent().parent().addClass("required");
            $("#ddlCourseList").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }*/
        if (branchIds == '')
        {
            $("#ddlBranchesList").parent().parent().addClass("required");
            $("#ddlBranchesList").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        fromData = $("#txtFromDate").val();
        toDate = $("#txtToDate").val();
        if(flag == 0)
        {
            $("#defaultGraphs").hide();
            analyticsWidgets(branchIds,fromData,toDate);
            leadStudentsYearlyReport(branchIds,fromData,toDate);
            leadStudentCourseBranchWiseReport(branchIds,fromData,toDate);
        }
    }
    function clearAnalyticsReport()
    {
        $("#defaultGraphs").show();
        analyticsWidgets(0,0,0);
        leadStudentsYearlyReport(0,0,0);
        leadStudentCourseBranchWiseReport(0,0,0);
        achievedLeadsCourseWiseReport();
        leadStudentMonthlyReport();
        $('.reports-filter-widget').slideUp();
    }
    function analyticsWidgets(branchIds,fromDate,toDate)
    {
        var ajaxurl = API_URL + 'index.php/Analytics/getAnalyticalWidgetsData';
        var userID=$('#hdnUserId').val();
        var params = {branchIds: branchIds,fromDate:fromDate,toDate:toDate};
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl,params: params, headerParams: headerParams, action: 'list', onSuccess: analyticsWidgetsResponse});
    }
    function analyticsWidgetsResponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            var widgetsData=response.data;
            $("#branchesCount").html(widgetsData.branches);
            $("#courseCount").html(widgetsData.courses);
            $("#collectedAmount").html(moneyFormat(widgetsData.collection));
            $("#dueAmount").html(moneyFormat(widgetsData.dues));
        }
    }
    function leadStudentsYearlyReport(branchIds,fromDate,toDate)
    {
        var ajaxurl = API_URL + 'index.php/Analytics/getLeadStudentYearlyReport';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: leadStudentsYearlyReportResponse});
    }
    function leadStudentsYearlyReportResponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            var responseData=response.data;
            var years=[],visitors=[],students=[],student_target=[],visitor_target=[],others=[];
            $.each(responseData, function (index, value)
            {
                $.each(value, function (key, value)
                {
                    if(key=='report_year')
                    {
                        years.push(value);
                    }
                    else if(key=='student_target')
                    {
                        student_target.push(parseInt(value));
                    }
                    else if(key=='students')
                    {
                        students.push(parseInt(value));
                    }
                    else if(key=='visitor_target')
                    {
                        visitor_target.push(parseInt(value));
                    }
                    else if(key=='visitors')
                    {
                        visitors.push(parseInt(value));
                    }
                    else
                    {
                        others.push(parseInt(value));
                    }
                })
            });
            //graph One
            $('#graphOne').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: false
                },
                xAxis: {
                    lineColor: '#1b558c',
                    lineWidth: 1,
                    categories: years
                },
                yAxis: [{
                    lineColor: '#1b558c',
                    lineWidth: 1,
                    gridLineWidth: 0,
                    min: 0,
                    title: {
                        text: false
                    }
                }, {
                    title: {
                        text: false
                    },
                    opposite: true
                }],
                legend: {
                    enabled: true,
                    shadow: false,
                    align: 'right',
                    verticalAlign: 'top',
                    x: 0,
                    y: 0
                },
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Enrollment Target',
                    color: '#1b558c',
                    data: student_target,
                    pointPadding: 0.3,
                    pointPlacement: -0.2
                }, {
                    name: 'Enrollment Achieved',
                    color: '#f26522',
                    data: students,
                    pointPadding: 0.4,
                    pointPlacement: -0.2
                },{
                    name: 'Visitors Target',
                    color: '#01AAEF',
                    type: 'spline',
                    data: visitor_target
                },
                {
                    name: 'Visitors Achieved',
                    color: '#19BF6A',
                    type: 'spline',
                    data: visitors
                }
                ]
            });
        }
    }
    function leadStudentCourseBranchWiseReport(branchIds,fromDate,toDate)
    {
        var ajaxurl = API_URL + 'index.php/Analytics/LeadStudentCourseYearlyReport';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: leadStudentCourseBranchWiseReportResponse});
    }
    function leadStudentCourseBranchWiseReportResponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            var responseData=response.data;
            var responseLeadsData = responseData['leadstudents'];
            var responseAchievedLeadsData = responseData['achievedleadstudents'];
            var courses = responseData['courses'];
            var branches=[],targetAchieved=[],series=[],Leads=[],TargetLeads=[];
            $.each(responseAchievedLeadsData, function (index, responseAchievedLeadvalue)
            {
                $.each(responseAchievedLeadvalue, function (key, value)
                {
                    if(key=='branch_name')
                    {
                        targetAchieved.push(parseInt(responseAchievedLeadvalue['enrollments'])+parseInt(responseAchievedLeadvalue['visitors']));
                    }
                })
            });
            $.each(responseLeadsData, function (index, responseLeadvalue)
            {
                $.each(responseLeadvalue, function (key, value)
                {
                    if(key=='branch_name')
                    {
                        if(jQuery.inArray( value, branches ) == -1)
                        {
                            branches.push(value);
                        }
                    }
                })
            });

            for(var ls=0;ls<branches.length;ls++)
            {
                for(var ld=0;ld<responseLeadsData.length;ld++)
                {
                    if(branches[ls] == responseLeadsData[ld]['branch_name'])
                    {
                        for(var cs=0;cs<courses.length;cs++)
                        {
                            if(courses[cs]['name'] == responseLeadsData[ld]['course_name'])
                            {
                                if(Leads[courses[cs]['name']] == undefined)
                                {
                                    Leads[courses[cs]['name']]=[];
                                }
                                if(TargetLeads[courses[cs]['name']] == undefined)
                                {
                                    TargetLeads[courses[cs]['name']]=[];
                                }
                                Leads[courses[cs]['name']].push(parseInt(responseLeadsData[ld]['enrollments'])+parseInt(responseLeadsData[ld]['visitors']));
                                TargetLeads[courses[cs]['name']].push(parseInt(responseLeadsData[ld]['target_enrollments'])+parseInt(responseLeadsData[ld]['target_visitors']));
                            }
                        }
                    }
                }
            }
            $('#graphTwo').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: false
                },
                xAxis: {
                    lineColor: '#1b558c',
                    lineWidth: 1,
                    categories: branches
                },
                yAxis: [{
                    lineColor: '#1b558c',
                    lineWidth: 1,
                    gridLineWidth: 0,
                    min: 0,
                    title: {
                        text: false
                    }
                }, {
                    title: {
                        text: false
                    },
                    opposite: true
                }],
                legend: {
                    enabled: true,
                    shadow: false,
                    align: 'right',
                    verticalAlign: 'top',
                    x: 0,
                    y: 0
                },
                tooltip: {
                    shared: true
                },
                plotOptions: {
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0
                    }
                },
                series: []
            });

            var chart = $('#graphTwo').highcharts();
            for(var cs= 0,sd=0;cs<courses.length;cs++,sd++)
            {
                series[sd] = new Object();
                series[sd+1] = new Object();
                series[sd].pointPadding = 0.4;
                series[sd+1].pointPadding = 0.4;

                if(courses[cs]['name'] == 'CPA')
                {
                    series[sd].color = '#002f09';
                    series[sd+1].color = '#00a651';
                }
                else if(courses[cs]['name'] == 'CMA')
                {
                    series[sd].color = '#00122c';
                    series[sd+1].color = '#0076a3';
                }
                if(cs == 0)
                {
                    series[sd].pointPlacement = -0.2;
                    series[sd+1].pointPlacement = -0.2;
                    series[sd].name = courses[cs]['name']+' target';
                    series[sd+1].name = courses[cs]['name'];
                    series[sd].data = TargetLeads[courses[cs]['name']];
                    series[sd+1].data = Leads[courses[cs]['name']];
                }
                else
                {
                    series[sd].pointPlacement = 0.1;
                    series[sd+1].pointPlacement = 0.1;
                    series[sd].name = courses[cs]['name']+' target';
                    series[sd+1].name = courses[cs]['name'];
                    series[sd].data = TargetLeads[courses[cs]['name']];
                    series[sd+1].data = Leads[courses[cs]['name']];
                }
                chart.addSeries(series[sd]);
                chart.addSeries(series[sd+1]);
            }
            var targetseries = new Object();
            targetseries.name = 'Targets Achieved';
            targetseries.type = 'spline';
            targetseries.color = '#00aaef';
            targetseries.data = targetAchieved;
            chart.addSeries(targetseries);
        }
    }

    function achievedLeadsCourseWiseReport()
    {
        var ajaxurl = API_URL + 'index.php/Analytics/AchievedLeadsCourseWiseReport';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: achievedLeadsCourseWiseReportResponse});
    }
    function achievedLeadsCourseWiseReportResponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            var responseData=response.data;
            var overallAchievedLeads=0;
            $.each(responseData, function (index, responseAchievedLeads)
            {
                $.each(responseAchievedLeads, function (key, value)
                {
                    if(key=='leads')
                    {
                        overallAchievedLeads+=parseInt(value);
                    }
                })
            });
            if(responseData.length > 0)
            {
                var achievedseries = [],selected='',color='';
                for(var cs=0;cs<responseData.length;cs++)
                {
                    selected=false;
                    if(responseData[cs].course_name == 'CPA')
                    {
                        color='#00a651';
                        selected=true;
                    }
                    else if(responseData[cs].course_name == 'CMA')
                    {
                        color='#ed1c24';
                    }
                    achievedseries.push({name:responseData[cs].course_name,y:parseFloat(((responseData[cs].leads/overallAchievedLeads)*100).toFixed(2)),color:color,selected:selected});
                }
                $('#graphThree').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: false
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
                    },
                    legend: {
                        layout: 'horizontal',
                        align: 'right',
                        borderWidth: 0,
                        verticalAlign: 'top',
                        x: 0,
                        y: 0
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Course',
                        colorByPoint: true,
                        data: achievedseries
                    }]
                });
            }
        }
    }
    function leadStudentMonthlyReport()
    {
        var ajaxurl = API_URL + 'index.php/Analytics/LeadStudentMonthlyReport';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: leadStudentMonthlyReportResponse});
    }
    function leadStudentMonthlyReportResponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            var responseData=response.data;
            var months=[],target=[],visitor_target=[],achieved=[];
            $.each(responseData, function (index, value)
            {
                $.each(value, function (key, value)
                {
                    if(key=='month')
                    {
                        months.push(value.substring(0, 3));
                    }
                    else if(key=='target')
                    {
                        target.push(parseInt(value));
                    }
                    else if(key=='visitor_target')
                    {
                        visitor_target.push(parseInt(value));
                    }
                    else if(key=='achieved')
                    {
                        achieved.push(parseInt(value));
                    }
                });
            });
            for(var t=0;t<target.length;t++)
            {
                target[t]=parseInt(target[t])+parseInt(visitor_target[t]);
            }
            $('#graphFour').highcharts({
                rangeSelector : {
                    selected : 1
                },
                title: {
                    text: false
                },
                xAxis: {
                    lineColor: '#1b558c',
                    lineWidth: 1,
                    categories: months,
                    plotLines: [{
                        value: 2,
                        width: 1,
                        color: '#1b558c',
                        dashStyle: 'shortdash',
                        label: {
                            text: 'Quarter 1'
                        }
                    },
                        {
                            value: 5,
                            width: 1,
                            color: '#1b558c',
                            dashStyle: 'shortdash',
                            label: {
                                text: 'Quarter 2'
                            }
                        },
                        {
                            value: 8,
                            width: 1,
                            color: '#1b558c',
                            dashStyle: 'shortdash',
                            label: {
                                text: 'Quarter 3'
                            }
                        },
                        {
                            value: 11,
                            width: 1,
                            color: '#1b558c',
                            dashStyle: 'shortdash',
                            label: {
                                text: 'Quarter 4'
                            }
                        }]

                },
                yAxis: {
                    lineColor: '#1b558c',
                    lineWidth: 1,
                    gridLineWidth: 0,
                    title: {
                        text: false
                    }
                },
                tooltip: {
                    crosshairs: true,
                    shared: true
                },
                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    borderWidth: 0,
                    verticalAlign: 'top',
                    x: 0,
                    y: 0
                },
                series: [{
                    name: 'Targets',
                    data: target,
                    color:'#1b558c',
                    marker : {
                        symbol: 'circle',
                        enabled : true,
                        radius : 3
                    }
                }, {
                    name: 'Achieved',
                    data: achieved,
                    color:'#f26522',
                    marker : {
                        symbol: 'circle',
                        enabled : true,
                        radius : 3
                    }
                }]
            });
        }
    }
</script>