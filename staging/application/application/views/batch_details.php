<div class="contentpanel">
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="branchId" value="<?php echo $Id ?>" />
    <input type="hidden" id="batchId" value="<?php echo $Sub_id ?>" />
    <input type="hidden" id="BatchCode" value="0" />
    <input type="hidden" id="userBranchId" value="0" />
    <input type="hidden" id="CourseId" value="0" />
    <input type="hidden" id="CourseName" value="0" />
    <input type="hidden" id="branchSubjectId" value="0" />
    <input type="hidden" id="userId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnClassId" value="0" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header" id="batchHeading">No Batch Found</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="tooltipped" data-position="left" data-tooltip="Close" href="<?php echo BASE_URL; ?>app/branchinfo/<?php echo $Id ?>" class="text-dec-none" id="linkBatchDetails"><i class="icon-times"></i></a></li>
                <li><a access-element="add" onclick="manageBatchSubject(this, 0)" data-position="top" data-tooltip="Add new class" href="javascript:;" class="text-dec-none tooltipped"><i class="icon-plus-circle"></i></a></li>
                <li><a access-element="import" data-toggle="modal" data-target="#importModal" onclick="resetImport(this)" class="tooltipped" data-position="top" data-tooltip="Import class schedule" href="javascript:;" class="text-dec-none"><i class="icon-import1"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody">
        <div class="fixed-wrap clearfix animated fadeIn">
            <div class="batch-details clearfix">
                <div class="col-sm-5 p0" id="batchinfo">

                </div>
                <div class="col-sm-7 p0">
                    <div id="batch_chart" style=""></div>
                </div>
            </div>
            <div class="batch-table-view">
                <h3 class="heading text-uppercase" id="batchCurrentDescription"> </h3>
                <div class="mt10" id="batchCurrentFaculty">

                </div>
                <table class="table table-responsive mt15 customised-table" id="batch-subject-list">
                    <thead>
                        <tr>
                            <th rowspan="2" class="no-sort">Class#</th>
                            <th rowspan="2" class="no-sort">Date</th>
                            <th rowspan="2" class="no-sort">Subject</th>
                            <th rowspan="2" class="no-sort">Faculty</th>
                            <th rowspan="2" class="no-sort">Topic</th>
                            <th rowspan="2" class="no-sort">Venue</th>
                            <th rowspan="2" class="no-sort">Cups Used</th>
                            <th rowspan="2" class="no-sort">Plates Used</th>
                            <th colspan="3" class="no-sort text-center">Attendance</th>
                            <th rowspan="2" class="no-sort"></th>
                        </tr>
                        <tr>
                            <th class="no-sort">Regular</th>
                            <th class="no-sort">Demo</th>
                            <th class="no-sort">Old</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="atten-table-view">
                <table class="table table-responsive customised-table mt15" id="studentList">
                    <thead>
                    <tr class="animated fadeInUp">
                        <th>Student Activities</th>
                        <th id="studentAttendanceHeader">Attendance(0/0)</th>
                        <th>Total Fee</th>
                        <th>Concession</th>
                        <th>Balance</th>
                        <th>Material</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="studentAttendanceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <div class="pull-right">
                    <a class="slide-down f18 white-color tooltipped" href="javascript:" onclick="resetSpecialAttendance()" data-tooltip="Add Special Attendance" data-position="left" ><i class="icon-plus-circle"></i></a>
                </div>
                <h4 class="modal-title" id="myModalLabelAttendance">Attendance</h4>
            </div>
            <div style="display:none;" class="hide-data hide-box clearfix border-bottom pb10">
                <form name="studentsAutocompleteFormName" id="studentsAutocompleteForm">
                    <div class="col-sm-12 mt10">

                        <div class="multiple-radio">
                            <label class="inline-block">Student Type</label>
                            <span class="inline-radio">
                                <input type="radio" checked="checked" value="Old Student" id="typOldStudent" class="with-gap" name="studenttype">
                                <label for="typOldStudent">Old Student</label>
                            </span>
                            <span class="inline-radio">
                                <input type="radio" value="Guest" id="typGuest" class="with-gap" name="studenttype">
                                <label for="typGuest">Guest</label>
                            </span>
                        </div>



                        <div class="input-field" id="oldStudentsWrapper" >
                            <input type="text" class="validate" id="txtOldStudents" >
                            <label for="txtOldStudents">Student Name</label>
                            <input type="hidden" name="hdnOldStudentId" id="hdnOldStudentId" value="0"/>
                            <input type="hidden" name="hdnOldStudentEnrollNo" id="hdnOldStudentEnrollNo" value="0"/>
                            <input type="hidden" name="hdnOldStudentName" id="hdnOldStudentName" value="0"/>
                        </div>

                        <div id="guestStudentsWrapper" style="display:none">
                            <div class="input-field">
                                <input type="text" class="validate mb10" id="txtGuestName" >
                                <label for="txtGuestName">Name <em>*</em></label>
                            </div>
                            <div class="input-field">
                                <input type="text" class="validate mb10" id="txtGuestMobile" onkeypress="return isPhoneNumber(event)" maxlength="15" min-length="8" >
                                <label for="txtGuestMobile">Mobile <em>*</em></label>
                            </div>
                            <div class="input-field">
                                <input type="text" class="validate mb10" id="txtGuestEmail"  >
                                <label for="txtGuestMobile">Email <em>*</em></label>
                            </div>
                            <div class="input-field" id="CommonCallStatusInformation">
                                <select id="CallAnswerTypes" class="mb10" onchange="getCallAnswerTypes(this)">
                                    <option value="" selected>--Select--</option>
                                </select>
                                <label for="CallAnswerTypes" class="select-label">Lead Stage <em>*</em></label>
                            </div>
                            <div class="input-field" id="CommonCallStatusDatePicker">
                                <input type="date" class="datepicker relative enableFutureDates mb10" placeholder="dd/mm/yyyy" id="txtCommonCallStatusDatePicker">
                                <label for="txtCommonCallStatusDatePicker" id="txtLabelCommonCallStatusDatePicker">Next Followup Date <em>*</em></label>
                            </div>
                            <div class="input-field">
                                <select id="txtsecondaryUser" name="txtsecondaryUser" class="validate">
                                    <option value="" selected>--Select--</option>
                                </select>
                                <label for="txtsecondaryUser" class="select-label">Secondary Counselor <em>*</em></label>
                            </div>
                            <div class="input-field">
                                <textarea id="txtLeadDescription" class="materialize-textarea mb10"></textarea>
                                <label class="" for="txtLeadDescription">Known information about this lead <em>*</em></label>
                            </div>
                        </div>
                        <div class="col-sm-12 p0 mt10">
                            <button type="button" class="btn blue-btn btn-xs" onclick="addSpecialStudentAttendance(this)"> <i class="icon-right mr8"></i> Add Student </button>
                            <button type="button" onclick="closeOldStudentsAutocomplete()" class="btn blue-light-btn btn-xs"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </div>

            </div>
            <form action="/" method="post" id="BranchForm" novalidate="novalidate">
                <div class="modal-body modal-scroll clearfix">
                    <div class="row">
                        <div id="regularStudentsWrapper">
                            <ul class="check-view check-view-noicon">
                            </ul>
                            <table class="table modal-table-list mb0" >
                                <thead>
                                <tr>
                                    <th width="45%">Name</th>
                                    <th width="45%">Enrollment No</th>

                                    <th width="10%"></th>
                                </tr>
                                </thead>
                            </table>
                            <div style="max-height:220px;overflow-y: auto;" >
                                <table class="table modal-table-list mb0" id="regularStudentsWrapperTable" >
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="widget-header light-dark-blue relative mt15">
                            <h4 class="heading-uppercase f14 m0 p10">Special Attendies</h4>
                        </div>
                        <div id="specialStudentsWrapper">
                            <ul class="check-view check-view-noicon">
                            </ul>
                            <div style="max-height:220px;overflow-y: auto;" >
                                <table class="table modal-table-list mb0" id="specialStudentsWrapperTable" >
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>


                </div>

            </form>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button id="actionButton" type="button" class="btn blue-btn" onclick="confirmSaveAttendance(this)"><i class="icon-right mr8"></i>Save</button>
                <button type="button" class="btn blue-light-btn" data-dismiss="modal" aria-label="Close"><i class="icon-times mr8"></i>Cancel</button>


            </div>
        </div>
    </div>
</div>

<!--Modal create-->
<div class="modal fade" id="create-subject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeCreateSubjectModal()"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <h4 class="modal-title" id="subjectModalLabel">Create</h4>
            </div>
            <div class="modal-body modal-scroll clearfix">
                <form id="create-subjectForm">
                    <div class="col-sm-12">
                        <div class="input-field">
                            <label class="datepicker-label datepicker">Date <em>*</em></label>
                            <input id="subjectDate" type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <select id="faculty">
                                <option value="" selected>--Select--</option>
                            </select>
                            <label class="select-label">Faculty</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <select id="subject" name="subject" class="multiple formSubmit">
                                <option value="" >--Select--</option>
                            </select>
                            <label class="select-label">Subject</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <input id="topic" type="text" class="validate" name="topic" class="validate" class="formSubmit">
                            <label for="topic">Topic <em>*</em></label>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="input-field">
                            <select id="venue">
                                <option value="" selected>--Select--</option>
                            </select>
                            <label class="select-label">Venue</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field mt0">
                            <input id="nCUsed" type="text" class="validate" value="" onkeypress="return isNumberKey(event)" >
                            <label for="nCUsed" class="active">No. of cups used </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-field mt0">
                            <input id="nPUsed" type="text" class="validate" value="" onkeypress="return isNumberKey(event)" >
                            <label for="nPUsed" class="active">No. of plates used </label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="actionCreateSubjectButton" onclick="saveBatchSubject(this)" type="button" class="btn blue-btn" ><i class="icon-right mr8"></i>Save</button>
                <button type="button" onclick="closeCreateSubjectModal()" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--Import Model-->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-modal="right" modal-width="750">
    <form id="importForm" method="post">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"><i class="icon-times strong"></i></span></button>
                    <h4 class="modal-title">Import</h4>
                </div>
                <div class="modal-body modal-scroll clearfix">
                    <div class="col-sm-12">
                        <div class="file-field input-field">
                            <div class="btn file-upload-btn">
                                <span>Upload Document</span>
                                <input type="file" id="txtfilebrowser">
                            </div>
                            <div class="file-path-wrapper custom">
                                <input type="text" placeholder="Upload single file" class="file-path validate">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 mt20">
                        <a class="btn blue-btn btn-sm" href="<?php echo BASE_URL . 'templates/ClassTemplate.csv'; ?>" id="btnDownloadSample"><i class="icon-file mr8"></i>Download Sample</a>
                    </div>

                    <div class="col-sm-12 mb20 mt20">
                        <div class="col-sm-3 p0 ">
                            <div class="batch-info ml0 grey-bg">
                                <div class="ptb7">
                                    <div class="camp-text f12">Total Classes</div>
                                    <div class="camp-num red f14" id="TotalUploadedContacts">-</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 p0 ">
                            <div class="batch-info grey-bg">
                                <div class="ptb7">
                                    <div class="camp-text f12">Uploaded</div>
                                    <div class="camp-num thick-blue f14" id="UploadedContacts">-</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 p0 ">
                            <div class="batch-info grey-bg">
                                <div class="ptb7">
                                    <div class="camp-text f12">Failed</div>
                                    <div class="camp-num violet f14" id="FailedContacts">-</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3 p0 ">
                            <div class="batch-info mr0 grey-bg">
                                <div class="ptb7">
                                    <div class="camp-text f12">Duplicate</div>
                                    <div class="camp-num light-black f14" id="DuplicateContacts">-</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-responsive table-striped table-custom" id="uploadedContactsList">
                            <colgroup>
                                <col width="10%">
                                <col width="35%">
                                <col width="50%">
                            </colgroup>
                            <thead>
                            <tr>
                                <th>Row No.</th>
                                <th>Topic</th>
                                <th>Status</th>
                            </tr>
                            <tr>
                                <td class="border-none p0 lh10">&nbsp;</td>
                            </tr>
                            <!-- <tr><td class="border-none p0 lh5">&nbsp;</td></tr>-->
                            </thead>
                            <tbody>
                            <tr class="">
                                <td colspan="3" align='center'>No Data Found</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <a class="btn blue-btn" id="importBtn" href="javascript:;" onclick="importTopics(this)"><i class="icon-right mr8"></i>Upload</a>
                    <a href="javascropt:;" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal fade bs-example-modal-sm" id="bookIssueModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
    <div class="modal-dialog alert-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <h4 class="modal-title" id="">Books Issued </h4>
            </div>
            <form action="/" method="post" id="" novalidate="novalidate">

                <div class="modal-body clearfix">
                    <div class="row">
                        <div class="clearfix ">
                            <div class="col-sm-12 p0">
                                <div class="col-sm-5" style="padding-left: 8px;">
                                    <p><label class="bold f14 light-black">Name : </label> <span id="studentName"></span></p>
                                </div>
                                <div class="col-sm-7 p0">
                                    <p><label class="bold f14 light-black">Enrollment No : </label> <span id="enrollNum"></span></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <table class="table modal-table-list mt10" id="booksList">
                                <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Quantity</th>
                                    <th>Issued</th>
                                    <th width="13%"></th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <div id="booksissuefooter">
                    <button id="" type="submit" class="btn blue-btn" onclick="saveIssueBooks()"><i class="icon-right mr8"></i>Issue</button>
                    <button data-dismiss="modal" aria-label="Close" type="button" class="btn blue-light-btn"><i class="icon-times mr8"></i>Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    if('<?php echo $userData['Role']; ?>'=='Superadmin'){
        $context = 'Branches';
        $pageUrl = 'branches';
        $serviceurl = 'branches';
    }
    else if('<?php echo $userData['Role']; ?>'=='Branch Head'){
        $context = 'Dashboard';
        $pageUrl = 'branch_dashboard';
        $serviceurl = 'branch_dashboard';
        $('#linkBatchDetails').attr('href','<?php echo BASE_URL; ?>app/'+$serviceurl);
    }
    var glbBranchId='';
    docReady(function () {
        if($("#userBranchList").length > 0)
        {
            glbBranchId = $('#userBranchList').val();
            $("#userBranchList").parentsUntil('div.BranchName-dropdown').remove();
        }

        $('input[type=radio][name=studenttype]').change(function()
        {
            if (this.value == 'Old Student')
            {
                $("#oldStudentsWrapper").slideDown();
                $("#guestStudentsWrapper").slideUp();
                $('#studentsAutocompleteForm #oldStudentsWrapper').find('input').val('');
                $('#studentsAutocompleteForm #oldStudentsWrapper label').removeClass("active");
            }
            else if (this.value == 'Guest')
            {
                var branchId=$("#branchId").val();
                branchVisitleadStatuses(this,'M3');
                listsecondLevelCounsellors(branchId);
                $("#oldStudentsWrapper").slideUp();
                $("#guestStudentsWrapper").slideDown();
                $('#studentsAutocompleteForm #guestStudentsWrapper').find('input').val('');
                $('#studentsAutocompleteForm #guestStudentsWrapper').find('textarea').val('');
                $('#studentsAutocompleteForm #guestStudentsWrapper label').removeClass("active");
            }
        });
        loadClassDatatables();
        batchInfoPageLoad();
        getBatchGraph(this);
    });

    $('#faculty').change(function () {
        getSubjectBasedOnFaculty('list', 0);
    });

    $('#subject').change(function () {
        multiSelectDropdown($(this));
    });
    
    function getBatchGraph(This){
        var dates = [];
        var regular = [];
        var guest = [];
        var old = [];
        
        var ajaxurl=API_URL+'index.php/Batch/getBatchGraphData';
        var userID=$('#hdnUserId').val();
        var action='list';
        var batchId='<?php echo $Sub_id; ?>';
        var params={batch_id:batchId};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This: This, method: 'GET', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
            if (response.data.length == 0 || response == -1 || response['status'] == false) {
                $('#batch_chart').html('<div class="text-center">No classes present</div>');
            } else {
                var rawData = response.data;
                var dates = [];
                var regular = [];
                var guest = [];
                var old = [];
                for(var a in rawData){
                    dates.push(rawData[a].classDate);
                    if(rawData[a].totalBatchCount!=0){
                        regular.push(parseFloat(rawData[a].classAttendanceCount));
                        guest.push(parseFloat(rawData[a].classGuestAttendanceCount));
                        old.push(parseFloat(rawData[a].classPreviousAttendanceCount));
                        /*regular.push(((rawData[a].classAttendanceCount/rawData[a].totalBatchCount)/100));
                        guest.push(((rawData[a].classGuestAttendanceCount/rawData[a].totalBatchCount)/100));
                        old.push(((rawData[a].classPreviousAttendanceCount/rawData[a].totalBatchCount)/100));*/
                    }else{
                        regular.push("");
                        guest.push("");
                        old.push("");
                    }
                }
                var Column_colors = ['#00a651', '#ed1c24', '#6192d3'];
                $('#batch_chart').highcharts({
                        colors:Column_colors,
                        chart: {
                            type: 'column',
                            spacingLeft: 75,
                            spacingBottom: 25,
                            spacingTop: 25,
                            spacingRight: 2,
                            width: 550,
                            height: 300
                        },
                        legend: {
                            align: 'right',
                            verticalAlign: 'top',
                            x: 0,
                            y: -5,
                            floating:true,
                            itemStyle: {
                                color: '#205081',
                                fontWeight: 'normal'
                            }
                        },
                        title: {
                            text: 'Classwise Attendence',
                            align: "left",
                            style: {
                                color: '#205081',
                            }
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: dates,
                            crosshair: false,
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: ''
                            },
                            gridLineWidth: 0,
                            lineWidth: 1,
                            lineColor: "#C0D0E0"
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:14px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                /*'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',*/
                                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true,
                            backgroundColor: '#000',
                            borderColor: "#000",
                            style: {
                                "color": "#fff",
                            },
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            },
                            series: {
                                pointWidth: 5
                            }
                        },
                        series: [{
                            name: 'Regular',
                            data: regular

                        }, {
                            name: 'Guest',
                            data: guest

                        }, {
                            name: 'Old Students',
                            data: old
                        }]
                    });	
            }
        }});
    }

    function batchInfoPageLoad() {
        var userId = $('#userId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/Batch/getBatchById';
        var params = {'batch_id': '<?php echo $Sub_id ?>', 'branch_id': '<?php echo $Id ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: batchInfo});

        var ajaxurl = API_URL + 'index.php/Batch/getStudentListByBatch';
        var params = {'batch_id': '<?php echo $Sub_id ?>', 'branch_id': '<?php echo $Id ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: studentList});

        var ajaxurl = API_URL + 'index.php/Batch/getTotalClasses';
        var params = {'batch_id': '<?php echo $Sub_id ?>', 'branch_id': '<?php echo $Id ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: studentAttendanceHeader});
    }

    function studentList(response){
        var dashboard = '';
        if (response.data.length == 0 || response == -1 || response['status'] == false) {
            dashboard += '<tr><td colspan="6" class="text-center">No student found</td></tr>';
            $('#studentList tbody').html(dashboard);
        } else {
            var rawData = response.data;
            for(var i in rawData){
                dashboard += '<tr>';
                dashboard += '<td>';
                dashboard += '<span class="display-block bold">'+rawData[i].student_name+'</span>';
                dashboard += '<span class="display-block f12">'+rawData[i].enrollment_number+'</span></td>';
                var attendedClassPercentage= Math.floor(parseFloat(rawData[i].attendedClassPercentage));
                var attendedClass="bold";
                if(attendedClassPercentage<40)
                {
                    attendedClass="bold red";
                }
                else if(attendedClassPercentage>=40 && attendedClassPercentage<70){
                    attendedClass="bold orange";
                }
                else if(attendedClassPercentage>=70){
                    attendedClass="bold darkblue";
                }
                else
                {
                    attendedClass="bold";
                }
                if(!rawData[i].amountpayable)
                    rawData[i].amountpayable=' -- ';
                if(~rawData[i].concessionamount)
                    rawData[i].concessionamount=' -- ';
                if(!rawData[i].balanceamount)
                    rawData[i].balanceamount=' -- ';
                dashboard += '<td class="'+attendedClass+'">'+attendedClassPercentage+'%</td>';
                dashboard += '<td>'+rawData[i].amountpayable+'</td>';
                dashboard += '<td>'+rawData[i].concessionamount+'</td>';
                dashboard += '<td>'+rawData[i].balanceamount+'</td>';
                dashboard += '<td>';
                if(rawData[i].quantity == null)
                    rawData[i].quantity=0;
                if(rawData[i].adhoc_quantity == null)
                    rawData[i].adhoc_quantity=0;
                if(rawData[i].issued == null)
                    rawData[i].issued=0;

                //if(rawData[i].is_publication_fee_paid==1)
                {
                    var pending = ((parseInt(rawData[i].quantity) + parseInt(rawData[i].adhoc_quantity)) - parseInt(rawData[i].issued));
                    dashboard += '<span data-toggle="modal" class="label-count" onclick="getStudentCourseBooks(this,'+rawData[i].branch_xref_lead_id+','+rawData[i].batch_id+',\''+rawData[i].student_name+'\',\''+rawData[i].enrollment_number+'\')"><i class="icon-actions"></i>';
                    if(pending != 0){
                        dashboard += ' <label>'+(pending)+'</label>';
                    }
                    dashboard += '</span>';
                }
                dashboard += '</td>';
                dashboard += '</tr>';
            }

            $('#studentList tbody').html(dashboard);
        }
    }

    function studentAttendanceHeader(response){
        var html="Attendance(0/0)";
        if(response.status===true){
            html="Attendance("+response.data.currentClassCount+"/"+response.data.totalClassCount+")";
        }
        else{

        }
        $('#studentAttendanceHeader').html(html);


    }

    function batchInfo(response) {
        var dashboard = '';
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            $('#batchHeading').html('<div class="text-center">No Batch Found</div>');
        }
        else
        {
            var rawData = response.data;
            $("#CourseId").val(rawData.fk_course_id);
            $("#CourseName").val(rawData.courseName);
            $("#BatchCode").val(rawData.code);
            $('#userBranchList').val(rawData.fk_branch_id);
            $('#userBranchList').material_select();
            rawData.code=rawData.branchCode+'-'+rawData.courseName+'-M7-'+rawData.code;
            $('#batchHeading').html(rawData.code);

            dashboard += '<table class="table-responsive table table-custom-view">';
            dashboard += '<tr>';
            dashboard += '<td width="34%">';
            dashboard += '<span class="table-icon"><i class="icon-paper-board"></i> </span>';
            dashboard += '<span>Batch Info';
            dashboard += '<label class="display-block anchor-blue">'+rawData.code+'</label>';
            dashboard += '</span>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<table class="table">';
            dashboard += '<tr>';
            dashboard += '<td>';
            dashboard += '<span>Academic Start Date</span>';
            dashboard += '<label class="display-block">'+rawData.acedemic_start+'</label>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<span>Marketing Start Date</span>';
            dashboard += '<label class="display-block">'+rawData.marketing_start+'</label>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<span>Marketing End Date</span>';
            dashboard += '<label class="display-block">'+rawData.marketing_end+'</label>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '</table>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '<tr>';
            dashboard += '<td>';
            dashboard += '<span class="table-icon"><i class="icon-calendar2"></i> </span>';
            dashboard += '<span>Classes Schedules </span>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<table class="table">';
            dashboard += '<tr>';
            dashboard += '<td class="border-right">';
            dashboard += '<span class="">Previous</span>';
            var oldClass = rawData.oldClass?rawData.oldClass:' -- ';
            var presentClass = rawData.currentClass?rawData.currentClass:' -- ';
            var upcomingClass = rawData.upcomingClass?rawData.upcomingClass:' -- ';
            dashboard += '<label class="display-block" style="color: #53c24d;">'+oldClass+'</label>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<span class="">Next</span>';
            dashboard += '<label class="display-block anchor-blue">'+upcomingClass+'</label>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '</table>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '<tr>';
            dashboard += '<td>';
            dashboard += '<span class="table-icon"><i class="icon-users1"></i> </span>';
            dashboard += '<span>Students Admission </span>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<table class="table">';
            dashboard += '<tr>';
            dashboard += '<td class="border-right">';
            dashboard += '<span>Total Seats</span>';
            dashboard += '<label class="display-block bold">'+rawData.target+'</label>';
            dashboard += '</td>';
            dashboard += '<td class="border-right">';
            dashboard += '<span>Enrolled</span>';
            dashboard += '<label class="display-block bold">'+rawData.enrolled+'</label>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<span>Dropouts</span>';
            dashboard += '<label class="display-block bold red">'+rawData.dropouts+'</label>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '</table>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '<tr>';
            dashboard += '<td>';
            dashboard += '<span class="table-icon"><i class="icon-usd-currency1"></i> </span>';
            dashboard += '<span>Dues / Overdues </span>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<table class="table">';
            dashboard += '<tr>';
            dashboard += '<td class="border-right">';
            dashboard += '<span>Due Amount</span>';
            dashboard += '<label class="display-block bold">'+moneyFormat(rawData.amountDue)+'</label>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<span>Percentage</span>';
            dashboard += '<label class="display-block bold red">'+Math.floor(parseFloat(rawData.duePercentage))+'%</label>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '</table>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '<tr>';
            dashboard += '<td>';
            dashboard += '<span class="table-icon"><i class="icon-graph2"></i> </span>';
            dashboard += '<span>Inventory Report </span>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<table class="table">';
            dashboard += '<tr>';
            dashboard += '<td class="border-right">';
            dashboard += '<span>Stock</span>';
            var stock = Object.size(rawData.stock)>0?rawData.stock:' -- ';
            var issued = Object.size(rawData.issued)>0?rawData.issued:' -- ';
            var pending = (Object.size(rawData.pending)>0 || Object.size(rawData.issued)>0)?rawData.pending:' -- ';
            dashboard += '<label class="display-block bold">'+stock+'</label>';
            dashboard += '</td>';
            dashboard += '<td class="border-right">';
            dashboard += '<span>Issued</span>';
            dashboard += '<label class="display-block bold">'+issued+'</label>';
            dashboard += '</td>';
            dashboard += '<td>';
            dashboard += '<span>Pending</span>';
            dashboard += '<label class="display-block bold red">'+pending+'</label>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '</table>';
            dashboard += '</td>';
            dashboard += '</tr>';
            dashboard += '</table>';

            $('#batchinfo').html(dashboard);
            if(rawData.currentClass != ''){
                dashboard = 'Perspective Schedule for '+rawData.currentClass+' '+rawData.courseName+' Batch at '+rawData.venueName;
                $('#batchCurrentDescription').html(dashboard);

                dashboard = '<label class="bblue-text font-normal mb0">Co-ordinator</label>';
                var faculty = rawData.facultyName == null?' -- ':rawData.facultyName;
                dashboard += '<h4 class="heading m0"> '+faculty+' </h4>';
            }else{
                dashboard = '';
//                dashboard = 'Perspective Schedule -- ';
//                $('#batchCurrentDescription').html(dashboard);
//                dashboard = '<label class="bblue-text font-normal mb0">Co-ordinator</label>';
//                dashboard += '<h4 class="heading m0"> -- </h4>';
            }
            $('#batchCurrentFaculty').html(dashboard);
        }
    }

    function loadClassDatatables() {
        var userId = $('#userId').val();
        var batchId = $('#batchId').val();
        var ajaxurl = API_URL + 'index.php/Batch/getBatchSubjectList';
        var params = {'batch_id': batchId};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        $("#batch-subject-list").dataTable().fnDestroy();
        var CityTableObj = $('#batch-subject-list').DataTable({
            "fnDrawCallback": function () {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1) {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "bPaginate": false,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No class found",
                "sZeroRecords": "No class found",
                "sProcessing": "<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url: ajaxurl,
                type: 'GET',
                data: params,
                headers: headerParams,
                error: function (response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false
            }],
            "order": [[ 1, "asc" ]],
            columns: [
                {
                    data: null, render: function (data, type, row, meta)
                    {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                {
                    return data.topic_date;
                }
                },
                {
                    data: null, render: function (data, type, row)
                {
                    return data.code;
                }
                },
                {
                    data: null, render: function (data, type, row)
                {
                    return data.faculty_name;
                }
                },
                {
                    data: null, render: function (data, type, row)
                {
                    return data.topic;
                }
                },
                {
                    data: null, render: function (data, type, row)
                {
                    return data.venue_name;
                }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return '<span class="bold">'+data.no_of_cups_used+'</span>';
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return '<span class="bold">'+data.no_of_plates_used+'</span>';
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return ' <span class="bold">'+data.regularAttendance+'</span>';
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return '<span class="bold"> '+data.guestAttendance+' </span>';
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return '<span class="bold">'+data.oldAttendance+'</span>';
                    }
                },
                {
                    data: null, render: function (data, type, row)
                {
                    var rawHtml = '';
                    var status = data.topic_active == 1 ? 'activate' : 'inactivate';
                    var statusText = data.topic_active == 1 ? 'Inactive' : 'Active';
                    var rawHtml = '';
                    if(data.attendence_status == 1){
                        if(data.topic_date_past == 0){
                            //attendance taken
                            rawHtml = "<a data-target=\"#studentAttendanceModal\" data-toggle=\"modal\" id=\"anchorViewAttendance\" class=\"v-btn\" href=\"javascript:;\" onclick=\"getStudentsListAttendance(this,'view',"+data.batch_xref_class_id+" )\">View Attendance</a>";
                        }
                    }
                    else{
                        if(data.topic_date_past == 0){
                            //not taken
                            rawHtml = "<a data-target=\"#studentAttendanceModal\" data-toggle=\"modal\" id=\"anchorViewAttendance\" class=\"v-btn\" href=\"javascript:;\" onclick=\"getStudentsListAttendance(this,'take',"+data.batch_xref_class_id+" )\">Take Attendance</a>";
                        }
                    }
                    if(data.topic_date_status == 0){
                        rawHtml += '<span class="dropdown feehead-list pull-right custom-dropdown-style">';
                        rawHtml += '<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                        rawHtml += '<i class="fa fa-ellipsis-v f18 plr10"></i>';
                        rawHtml += '</a>';
                        rawHtml += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        rawHtml += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        rawHtml += '<li><a access-element="edit" href="javascript:;" onclick="manageBatchSubject(this,' + data.batch_xref_class_id + ')">Edit</a></li>';
                        rawHtml += '<li><a access-element="delete" href="javascript:;" onclick="deleteBatchSubject(this,\'' + data.batch_xref_class_id + '\',\'' + data.topic_active + '\')">' + statusText + '</a></li>';
                        rawHtml += '</ul>';
                        rawHtml += '</span>';
                    }
                    return rawHtml;
                }
                }
            ],
            "createdRow": function (row, data, index)
            {
                /*if (data.topic_status == 'current')
                {
                    $(row).addClass('batch-active');
                }
                else if (data.topic_status == 'new')
                {
                    $(row).addClass('');
                }
                else
                {
                    $(row).addClass('batch-complete');
                }*/
                if(data.topic_active == 0 || data.topic_active == 0)
                {
                    $(row).addClass('disabled-branchview');
                }
            }
        });
        DTSearchOnKeyPressEnter(CityTableObj);
    }

    function manageBatchSubject(This, id) {
        notify('Processing..', 'warning', 10);
        var userId = $('#userId').val();
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var CourseId = $('#CourseId').val();
        $.when(
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    requestUrl: API_URL + "index.php/Venue/getAllVenue",
                    params: {'course_id': CourseId},
                    action: action
                }
            ),
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    requestUrl: API_URL + "index.php/Faculty/getAllFaculty",
                    action: action
                }
            )
        ).then(function (response1, response2) {
            var venue = response1[0];
            var faculty = response2[0];

            buildVenueDropdown(venue);
            buildFacultyDropdown(faculty);

            if (id == 0) {
                //add
                $('#branchSubjectId').val(0);
                $('#create-subject').modal('show');
                alertify.dismissAll();
            } else {
                //edit
                $('#branchSubjectId').val(id);
                $('#actionCreateSubjectButton').html('Update');
                $('#subjectModalLabel').html('Edit class');

                var ajaxurl = API_URL + "index.php/Batch/getSubjectForBatch";
                var params = {'batch_xref_subject_id': id};
                action = 'edit';
                headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
                commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                    alertify.dismissAll();
                    if (response == -1 || response.data.length == 0 || response['status'] == false) {
                        //

                    } else {
                        var rawData = response.data[0];
                        getSubjectBasedOnFaculty('edit', rawData.fk_faculty_id);
                        $('#faculty').val(rawData.fk_faculty_id);
                        $('#faculty').material_select();
                        $('#topic').val(rawData.topic);
                        $('#nPUsed').val(rawData.no_of_plates_used);
                        $('#nCUsed').val(rawData.no_of_cups_used);

                        var today = new Date(rawData.topic_date);
                        var dd = today.getDate();
                        var mm = today.getMonth()+1; //January is 0!
                        var yyyy = today.getFullYear();

                        if(dd<10) {
                            dd='0'+dd
                        }

                        if(mm<10) {
                            mm='0'+mm
                        }

                        today = yyyy+'/'+mm+'/'+dd;
                        setDatePicker('#subjectDate', today);

                        if(rawData.attendence_status){
                            $('#subjectDate').attr('disabled','true');
                        }

                        $('#venue').val(rawData.fk_venue_id);
                        $('#venue').material_select();
                        setTimeout(function () {
                            $('#subject').val(rawData.fk_subject_id);
                            $('#subject').material_select();
                            $('#create-subject').modal('show');
                        }, 350);
                        Materialize.updateTextFields();
                    }
                }});
            }
        });
    }

    function getSubjectBasedOnFaculty(action, faculty) {
        if (faculty == 0) {
            faculty = $('#faculty').val();
        }
        var userId = $('#userId').val();
//        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/Subject/getAllSubjectByFaculty';
        var params = {'faculty_id': faculty};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'manage', onSuccess: function (response) {
            buildSubjectDropdown(response);
        }});
    }

    function saveBatchSubject(This) {
        $('#create-subjectForm span.required-msg').remove();
        $('#create-subjectForm input, #create-subjectForm .select-dropdown').removeClass("required");

        var flag = 0;
        var subjectDate = $('#subjectDate').val();
//        var subject = $('#subject').val();
        var topic = $('#topic').val();
        var faculty = $('#faculty').val();
        var venue = $('#venue').val();
        var nPUsed = $("#nPUsed").val();
        var nCUsed = $("#nCUsed").val();

        var selMulti = $.map($("#subject option:selected"), function (el, i)
        {
            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'all')
            {
                return $(el).val();
            }
        });
        var subject = selMulti.join(",");

        if (topic == '') {
            $("#topic").addClass("required");
            $("#topic").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (!(/^[a-zA-Z0-9 ,.]*$/.test(topic))) {
            $("#topic").addClass("required");
            $("#topic").after('<span class="required-msg">Only alphanumeric</span>');
            flag = 1;
        }

        if (subjectDate == '') {
            $("#subjectDate").addClass("required");
            $("#subjectDate").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }

        if (faculty == '') {
            $("#faculty").parent().find('.select-dropdown').addClass("required");
            $("#faculty").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (venue == '') {
            $("#venue").parent().find('.select-dropdown').addClass("required");
            $("#venue").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if (subject == '') {
            $("#subject").parent().find('.select-dropdown').addClass("required");
            $("#subject").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        var ajaxurl, type, method, action, headerParams = '';
        var params = [];
        var userID = $('#userId').val();
        if (flag != 1) {
            notify('Processing..', 'warning', 10);
            var id = $('#branchSubjectId').val();
            var batchId = $('#batchId').val();
            if (id == 0) {
                ajaxurl = API_URL + 'index.php/Batch/saveSubjectForBatch';
                action = 'add';
            } else {
                ajaxurl = API_URL + 'index.php/Batch/editSubjectForBatch/batch_xref_subject_id/' + id;
                action = 'edit';
            }

            params = {batchId: batchId, subjectDate: subjectDate, subject: subject, topic: topic, faculty: faculty, venue: venue, no_of_plates_used: nPUsed, no_of_cups_used: nCUsed};
            method = "POST";
            headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: method, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: function (response) {
                alertify.dismissAll();
                if (response['status'] == false || response == -1) {
                    if(Object.size(response.data)> 0){
                        for (var a in response.data) {
                            id = '#' + a;
                            if (a == 'subject' || a == 'faculty' || a == 'venue') {
                                $(id).parent().find('.select-dropdown').addClass("required");
                                $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else if (a == 'id') {
                                notify(response.data[a], 'error', 10);
                            } else {
                                $(id).addClass("required");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                                notify(response.data[a], 'error', 10);
                            }
                        }
                    }else{
                        notify(response.message, 'error', 10);
                    }
                    Materialize.updateTextFields();
                } else {
                    notify('Saved successfully', 'success', 10);
                    loadClassDatatables();
                    batchInfoPageLoad();
                    closeCreateSubjectModal();
                }
            }});
        } else {
//            console.log('error');
        }
    }

    function deleteBatchSubject(This, id, currentStatus) {
        var status = currentStatus == 1 ? 'inactivate' : 'activate';
        var conf = "Are you sure want to " + status + " this topic?";
        customConfirmAlert('Topic Status', conf);
        $("#popup_confirm #btnTrue").attr("onclick", "deleteBatchSubjectConfirm(this,'" + id + "')");
    }

    function deleteBatchSubjectConfirm(This, id) {
        var userId = $('#userId').val();
        var ajaxurl = API_URL + 'index.php/Batch/deleteBatchSubject/batch_xref_subject_id/' + id;
        var type = "POST";
        var action = 'delete';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: 'delete', onSuccess: deleteBatchSubjectResponse});
    }

    function deleteBatchSubjectResponse(response) {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false) {
            notify(response.message, 'error', 10);
        } else {
            notify(response.message, 'success', 10);
            loadClassDatatables();
            batchInfoPageLoad();
        }
    }

    function closeCreateSubjectModal() {
        $('#create-subjectForm')[0].reset();

        $('#branchSubjectId').val(0);

        $('#subject').find('option:gt(0)').remove();
        $('#venue').find('option:gt(0)').remove();
        $('#faculty').find('option:gt(0)').remove();
        $('#reference_type_val_id').val('');

        clearDatePicker('#subjectDate');

        $('#create-subjectForm label').removeClass("active");
        $('#create-subjectForm span.required-msg').remove();
        $('#create-subjectForm input, #create-subjectForm .select-dropdown').removeClass("required");

        $("select[name=subject]").val(0);
        $("select[name=venue]").val(0);
        $("select[name=faculty]").val(0);
        $('select').material_select();

        $('#subjectModalLabel').html('Create class');

        $('#actionCreateSubjectButton').html('Save');

        $('#create-subject').modal('hide');
    }

    function importTopics(This) {

        var ajaxurl = API_URL + 'index.php/Batch/BulkAddClass';
        var userID = $('#userId').val();
        var action = 'import';
        var fileUpload = document.getElementById("txtfilebrowser");

        var selectedFile = $("#txtfilebrowser").val();
        var extension = selectedFile.split('.');
        if (selectedFile.length <= 0){
            $("#txtfilebrowser").focus();
            notify('Select a file', 'error', 5);
            return;
        } else if (extension[1] != "csv") {
            $("#txtfilebrowser").focus();
            notify('Please upload a .csv file', 'error', 5);
            return;
        } else {
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};

            if (fileUpload.value != null) {
                var uploadFile = new FormData();
                var files = $("#txtfilebrowser").get(0).files;
                var batch = $("#batchId").val();
                if (files.length > 0) {
                    uploadFile.append("file", files[0]);
                    uploadFile.append("batch", batch);
                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        dataType: "json",
                        data: uploadFile,
                        contentType: false,
                        processData: false,
                        headers: headerParams,
                        beforeSend: function () {
                            $(This).attr("disabled", "disabled");
                            alertify.dismissAll();
                            notify('Processing..', 'warning', 10);
                        }, success: function (response) {
                            $(This).removeAttr("disabled");
                            finalresp(response);
                        }, error: function (response) {
                            alertify.dismissAll();
                            if (response.responseJSON.message == "Access Token Expired") {
                                logout();
                            } else if (response.responseJSON.message == "Invalid credentials") {
                                logout();
                            } else {
                                notify('Something went wrong. Please try again', 'error', 10);
                            }

                        }
                    });
                } else {
                    $("#txtfilebrowser").parent().find('.file-path-wrapper').addClass("required");
                    $("#txtfilebrowser").parent().find('.file-path-wrapper').after('<span class="required-msg">required field</span>');
                }
            } else {

            }
        }
    }
    function finalresp(response) {
        if (response.status === true) {
            alertify.dismissAll();
            var counts = response.data.Counts;
            $('#TotalUploadedContacts').html(counts.total_count);
            $('#UploadedContacts').html(counts.successCount);
            $('#FailedContacts').html(counts.failedCount);
            $('#DuplicateContacts').html(counts.duplicateCount);
            var records = response.data.Records;
            var rowsHtml = "";
            $.each(records, function (key, value) {
                rowsHtml += "<tr>";
                rowsHtml += "<td>" + value.row_number + "</td>";
                rowsHtml += "<td>" + value.topic + "</td>";
                rowsHtml += "<td>";
                if (value.status == 0) {
                    //pending
                    rowsHtml += "<span class='label f11 status-label font-normal label-warning'>Pending</span>";
                }
                if (value.status == 1) {
                    //success
                    rowsHtml += "<span class='label f11 status-label font-normal label-success'>Success</span>";
                    value.error_log = 'Add/Update Successfully';
                }
                if (value.status == 2) {
                    //fail
                    if (value.error_log == 'Duplicate entry.') {
                        rowsHtml += "<span class='label f11 status-label font-normal label-warning'>Duplicate</span>";
                    } else {
                        rowsHtml += "<span class='label f11 font-normal status-label label-danger'>Failed</span>";
                    }
                }

                rowsHtml += "<span style=\"width: 275px;\" class='text-success display-inline-block valign-middle ellipsis' title='" + value.error_log + "'>" + value.error_log + "</span>";
                rowsHtml += "</td>";
                rowsHtml += "</tr>";
            });
            $('#uploadedContactsList tbody').html(rowsHtml);
            $("#txtfilebrowser").val('');
            $("#txtfilebrowser").trigger('change');
            notify("Uploaded Successfully", "success", 10);
            loadClassDatatables();
            batchInfoPageLoad();
        } else {
            alertify.dismissAll();
            notify(response.message, "error", 10);
        }
    }
    function resetImport(This) {
        $('#importForm')[0].reset();
        $('#TotalUploadedContacts').html('-');
        $('#UploadedContacts').html('-');
        $('#FailedContacts').html('-');
        $('#DuplicateContacts').html('-');
        var rowsHtml = "<tr><td colspan='3' align='center'>No Data Found</td></tr>";
        $('#uploadedContactsList tbody').html(rowsHtml);
//        $('#importBtn').removeAttr('disabled');
        $(This).attr("data-target", "#importModal");
        $(This).attr("data-toggle", "modal");
    }

    //attendance related code here //
    function getStudentsListAttendance(This,type,classId){
        resetStudentsListAttendance();
        $('#hdnClassId').val(classId);
        getOldStudentsAutocomplete();
        if(type=='view'){
            $('#myModalLabelAttendance').html('View Attendance');
        }
        if(type=='take'){
            $('#myModalLabelAttendance').html('Take Attendance');
        }
        var userID= $('#hdnUserId').val();
        var action='list';
        var ajaxurl = API_URL + 'index.php/StudentAttendance/getStudents';
        var params = {classId: $('#hdnClassId').val()};
        var method = "GET";
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This, method: method, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getStudentsListAttendanceResponse});

    }
    function getStudentsListAttendanceResponse(response){
        $("#regularStudentsWrapperTable tbody").empty();
        if(response.status===true){
            var Students=response.data.students;
            if(Students.length>0) {
                $.each(Students, function (i, item) {
                    var listItemHtml = '';
                    listItemHtml += "<tr>";
                    var attendanceStatus = item.attendanceStatus;
                    var attendanceStatusCheckbox = " ";
                    var attendanceTooltip = "";
                    /*if (attendanceStatus == 'not taken') {
                        attendanceStatusCheckbox = " checked=\"checked\" ";
                    }
                    else*/
                    if (attendanceStatus == 'present') {
                        attendanceStatusCheckbox = " checked=\"checked\" ";
                    }
                    else if (attendanceStatus == 'absent')
                    {
                        attendanceStatusCheckbox = " ";
                    }
                    else
                    {
                        attendanceStatusCheckbox = " ";
                    }
                    listItemHtml += "<td>";
                    listItemHtml += item.name;
                    listItemHtml += "</td>";
                    listItemHtml += "<td>";
                    listItemHtml += item.enrollNo;
                    listItemHtml += "</td>";
                    listItemHtml += "<td class=\"text-center\"><div class=\"checklist-item float-none\">";
                    listItemHtml += "<input type=\"checkbox\" id=\"checkStudentAttendance_" + item.branch_xref_lead_id + "\" class=\"filled-in\" " + attendanceTooltip + " name=\"checkStudentAttendance\" " + attendanceStatusCheckbox + " value=\"" + item.branch_xref_lead_id + "\">";
                    listItemHtml += "<label for=\"checkStudentAttendance_" + item.branch_xref_lead_id + "\"></label>";
                    listItemHtml += "</div></td>";
                    listItemHtml += "</tr>";
                    $("#regularStudentsWrapperTable tbody").append(listItemHtml);
                });
            }
            else{
                var listItemHtml='';
                listItemHtml += "<tr>";
                listItemHtml += "<td class='text-center' colspan='3'>No students found</td>";
                listItemHtml += "</tr>";
                $("#regularStudentsWrapperTable tbody").append(listItemHtml);
            }

            var oldStudents=response.data.oldStudents;
            var guestStudents=response.data.guestStudents;
            //var oldStudents= $.merge(oldStudents,guestStudents);
            $('#specialStudentsWrapperTable tbody').empty('');
            $.each(oldStudents, function (i, item) {
                var split=item.id.split("|");
                var obj = new Object();
                obj.id=split[0];
                obj.enrollno = split[1];
                glbSpecialStudentsAttendance[split[1]] = obj;
                var listItemHtml='';
                listItemHtml+="<tr id=\"listnumber"+rowNumAttendance+"\">";
                listItemHtml+="<td>";
                listItemHtml+=split[2];
                listItemHtml+="</td>";
                listItemHtml+="<td>";
                listItemHtml+=split[1];
                listItemHtml+="</td>";
                listItemHtml+="<td class=\"text-center\"><div class=\"checklist-item float-none\">";
                listItemHtml+="<a data-tooltip=\"Remove Special Attendance\" data-position=\"left\" class=\"tooltipped\" href=\"javascript:;\" onclick=\"removeOldStudentAttendance(this,"+rowNumAttendance+",'"+split[1]+"')\"><i class=\"icon-trash\"></i> </a>";
                listItemHtml+="</div></td>";
                listItemHtml+="</tr>";
                $('#specialStudentsWrapperTable tbody').append(listItemHtml);
                rowNumAttendance = rowNumAttendance+1;
            });
            $.each(guestStudents, function (i, item) {
                var split=item.id.split("|");
                var obj = new Object();
                obj.id=split[0];
                obj.enrollno = split[1];
                glbSpecialStudentsGuestAttendance[split[1]] = obj;
                var listItemHtml='';
                listItemHtml+="<tr id=\"listnumber"+rowNumAttendance+"\">";
                listItemHtml+="<td>";
                listItemHtml+=split[2];
                listItemHtml+="</td>";
                listItemHtml+="<td>";
                listItemHtml+=split[1];
                listItemHtml+="</td>";
                listItemHtml+="<td class=\"text-center\"><div class=\"checklist-item float-none\">";
                listItemHtml+="<a data-tooltip=\"Remove Special Attendance\" data-position=\"left\" class=\"tooltipped\" href=\"javascript:;\" onclick=\"removeOldStudentAttendance(this,"+rowNumAttendance+",'"+split[1]+"')\"><i class=\"icon-trash\"></i> </a>";
                listItemHtml+="</div></td>";
                listItemHtml+="</tr>";
                $('#specialStudentsWrapperTable tbody').append(listItemHtml);
                rowNumAttendance = rowNumAttendance+1;
            });


        }
        else{
            var listItemHtml='';
            listItemHtml += "<tr>";
            listItemHtml += "<td class='text-center' colspan='3'>No students found</td>";
            listItemHtml += "</tr>";
            $("#regularStudentsWrapperTable tbody").append(listItemHtml);
            alertify.dismissAll();
            notify(response.message,'error',10);
        }
        buildpopover();
    }
    var rowNumAttendance=0;
    var glbSpecialStudentsAttendance = new Object();
    var glbSpecialStudentsGuestAttendance = new Object();
    function resetStudentsListAttendance(){
        $("#regularStudentsWrapperTable tbody").empty();
        $("#specialStudentsWrapperTable tbody").empty();
        $('#hdnClassId').val(0);
        closeOldStudentsAutocomplete();
        glbSpecialStudentsAttendance = new Object();
        glbSpecialStudentsGuestAttendance = new Object();
        rowNumAttendance =0;
    }
    function addSpecialStudentAttendance()
    {
        alertify.dismissAll();
        var chosenStudentType = $("input[name='studenttype']:checked"). val();
        if(chosenStudentType == 'Old Student')
        {
            addSpecialOldStudentAttendance();
        }
        else if(chosenStudentType == 'Guest')
        {
            addSpecialGuestStudentAttendance();
        }

    }
    function addSpecialOldStudentAttendance()
    {
        $('#studentsAutocompleteForm span.required-msg').remove();
        $('#studentsAutocompleteForm input').removeClass("required");
        $("#studentsAutocompleteForm").removeAttr("disabled");
        var eneteredText=$('#txtOldStudents').val().trim();
        var hdnValidStudentId=$('#hdnOldStudentId').val().trim();
        var hdnValidStudentEnrollNo=$('#hdnOldStudentEnrollNo').val().trim();
        var hdnValidStudentName=$('#hdnOldStudentName').val().trim();

        var flag=0;
        if(eneteredText=='')
        {
            $("#txtOldStudents").addClass("required");
            $("#txtOldStudents").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if(eneteredText!='' && hdnValidStudentId=='0')
        {
            $("#txtOldStudents").addClass("required");
            $("#txtOldStudents").after('<span class="required-msg">Invalid Student</span>');
            flag = 1;
        }
        else
        {

        }
        if(flag==1)
        {
            return false;
        }
        else
        {
            if(typeof glbSpecialStudentsAttendance[hdnValidStudentEnrollNo]!='undefined')
            {
                $("#txtOldStudents").addClass("required");
                $("#txtOldStudents").after('<span class="required-msg">Student already added</span>');
            }
            else
            {
                var obj = new Object();
                obj.id=hdnValidStudentId;
                obj.enrollno = hdnValidStudentEnrollNo;
                glbSpecialStudentsAttendance[hdnValidStudentEnrollNo] = obj;
                var listItemHtml='';
                listItemHtml+="<tr id=\"listnumber"+rowNumAttendance+"\">";
                listItemHtml+="<td>";
                listItemHtml+=hdnValidStudentName;
                listItemHtml+="</td>";
                listItemHtml+="<td>";
                listItemHtml+=hdnValidStudentEnrollNo;
                listItemHtml+="</td>";
                listItemHtml+="<td class=\"text-center\"><div class=\"checklist-item float-none\">";
                listItemHtml+="<a data-tooltip=\"Remove Special Attendance\" data-position=\"left\" class=\"tooltipped\" href=\"javascript:;\" onclick=\"removeOldStudentAttendance(this,"+rowNumAttendance+",'"+hdnValidStudentEnrollNo+"')\"><i class=\"icon-trash\"></i> </a>";
                listItemHtml+="</div></td>";
                listItemHtml+="</tr>";
                $('#specialStudentsWrapperTable tbody').append(listItemHtml);
                buildpopover();
                closeOldStudentsAutocomplete();
                rowNumAttendance = rowNumAttendance+1;
            }
        }
    }
    function addSpecialGuestStudentAttendance()
    {
        $('#studentsAutocompleteForm #guestStudentsWrapper input').removeClass("required");
        $('#studentsAutocompleteForm #guestStudentsWrapper select').removeClass("required");
        $('#studentsAutocompleteForm #guestStudentsWrapper textarea').removeClass("required");
        $('#studentsAutocompleteForm #guestStudentsWrapper span.required-msg').remove();
        var guestName=$("#txtGuestName").val();
        var guestMobile=$("#txtGuestMobile").val();
        var guestEmail=$("#txtGuestEmail").val();
        var leadStage=$("#CallAnswerTypes").val();
        var ChosenDate=$("#txtCommonCallStatusDatePicker").val();
        var guestInformation=$("#txtLeadDescription").val();
        var counselorId=$("#txtsecondaryUser").val();
        var regx_txtContactName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
        var regx_txtContactEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var regx_txtContactPhone = /^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/;
        var flag=0;
        if (guestName == '')
        {
            $("#txtGuestName").addClass("required");
            $("#txtGuestName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtContactName.test($('#txtGuestName').val()) === false) {
            $("#txtGuestName").addClass("required");
            $("#txtGuestName").after('<span class="required-msg">Invalid Name</span>');
            flag = 1;
        }
        if (guestMobile == '' && guestEmail == '')
        {
            $("#txtGuestMobile").addClass("required");
            $("#txtGuestMobile").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            $("#txtGuestEmail").addClass("required");
            $("#txtGuestEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if (guestMobile!='' && regx_txtContactPhone.test($('#txtGuestMobile').val()) === false)
        {
            $("#txtGuestMobile").addClass("required");
            $("#txtGuestMobile").after('<span class="required-msg">Invalid Phone Number</span>');
            flag = 1;
        }
        else if (guestEmail!='' && regx_txtContactEmail.test($('#txtGuestEmail').val()) === false)
        {
            $("#txtGuestEmail").addClass("required");
            $("#txtGuestEmail").after('<span class="required-msg">Invalid E-mail</span>');
            flag = 1;
        }
        if(leadStage == '')
        {
            $("#CallAnswerTypes").parent().find('.select-dropdown').addClass("required");
            $("#CallAnswerTypes").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        else
        {
            if(ChosenDate == '')
            {
                $("#txtCommonCallStatusDatePicker").addClass("required");
                $("#txtCommonCallStatusDatePicker").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag = 1;
            }
        }
        if (guestInformation == '') {
            $("#txtLeadDescription").addClass("required");
            $("#txtLeadDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if(flag == 1)
        {
            notify('All fields are mandatory','error',10);
            return false;
        }
        else
        {
            var userID= $('#hdnUserId').val();
            var courseId=$("#CourseId").val();
            var courseName=$("#CourseName").val();
            var branchId=$("#branchId").val();
            var batchCode=$("#BatchCode").val();
            notify('Processing..', 'warning', 10);
            var dateType=$('option:selected', $('#CallAnswerTypes')).attr('data-date');
            var CommentType=$('option:selected', $('#CallAnswerTypes')).text();
            var ajaxurl = API_URL + 'index.php/Lead/addGuestLead';
            var params = {userID: userID, contactName: guestName, contactEmail: guestEmail, contactPhone: guestMobile,contactCourse: courseId, contactCourseText: courseName,contactDescription:guestInformation,branchId:branchId,leadStage:leadStage,chosenDate:ChosenDate,dateType:dateType,CommentType:CommentType,counselorId:counselorId,batchCode:batchCode};
            var type = "POST";
            var action='add';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: saveGuestResponse});
        }
    }
    function saveGuestResponse(response)
    {
        if(response.status == true)
        {
            var leadData=response.data[0];
            var obj = new Object();
            obj.id=leadData['branch_xref_lead_id'];
            obj.enrollno = leadData['enroll'];
            glbSpecialStudentsGuestAttendance[leadData['enroll']] = obj;
            var listItemHtml='';
            listItemHtml+="<tr id=\"listnumber"+rowNumAttendance+"\">";
            listItemHtml+="<td>";
            listItemHtml+=leadData['lead_name'];
            listItemHtml+="</td>";
            listItemHtml+="<td>";
            listItemHtml+=leadData['enroll'];
            listItemHtml+="</td>";
            listItemHtml+="<td class=\"text-center\"><div class=\"checklist-item float-none\">";
            listItemHtml+="<a data-tooltip=\"Remove Special Attendance\" data-position=\"left\" class=\"tooltipped\" href=\"javascript:;\" onclick=\"removeOldStudentAttendance(this,"+rowNumAttendance+",'"+leadData['enroll']+"')\"><i class=\"icon-trash\"></i> </a>";
            listItemHtml+="</div></td>";
            listItemHtml+="</tr>";
            $('#specialStudentsWrapperTable tbody').append(listItemHtml);
            buildpopover();
            closeOldStudentsAutocomplete();
            $("#oldStudentsWrapper").slideUp();
            $("#guestStudentsWrapper").slideUp();
            $('#studentsAutocompleteForm #guestStudentsWrapper').find('input').val('');
            $('#studentsAutocompleteForm #guestStudentsWrapper').find('textarea').val('');
            $('#studentsAutocompleteForm #guestStudentsWrapper label').removeClass("active");
            rowNumAttendance = rowNumAttendance+1;
        }
    }
    function getOldStudentsAutocomplete(){
        $("#txtOldStudents").val('');
        $('#txtOldStudents').next('ul').remove();
        $("#hdnOldStudentId").val(0);
        $('#hdnOldStudentEnrollNo').val(0);
        $('#hdnOldStudentName').val(0);

        var headerParams = {
            action: 'list',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: $('#hdnUserId').val(),
            classId: $('#hdnClassId').val()
        };
        $("#txtOldStudents").jsonSuggest({
            onSelect:function(item){
                $("#txtOldStudents").val(item.text);

                var split=item.id.split("|");
                $("#hdnOldStudentId").val(split[0]);
                $("#hdnOldStudentEnrollNo").val(split[1]);
                $("#hdnOldStudentName").val(split[2]);
            },
            headers:headerParams,url:API_URL + 'index.php/StudentAttendance/getOldStudents' , minCharacters: 2});
    }
    //--------------//
    function closeOldStudentsAutocomplete(){
        $(".hide-data").slideUp();
        $('#studentsAutocompleteForm')[0].reset();
        Materialize.updateTextFields();
        $('#studentsAutocompleteForm span.required-msg').remove();
        $('#studentsAutocompleteForm input').removeClass("required");
        $("#studentsAutocompleteForm").removeAttr("disabled");
        $("#txtOldStudents").val('');
        $('#txtOldStudents').next('ul').remove();
        $("#hdnOldStudentId").val(0);
        $('#hdnOldStudentEnrollNo').val(0);
        $("#hdnOldStudentName").val(0);
        getOldStudentsAutocomplete();
    }
    function removeOldStudentAttendance(This,rownum,pcode)
    {
        if(glbSpecialStudentsAttendance[pcode])
            delete glbSpecialStudentsAttendance[pcode];
        else
            delete glbSpecialStudentsGuestAttendance[pcode];
        $('#listnumber'+rownum).remove();
        $("#"+$(This).attr("data-tooltip-id")).remove();
    }
    function confirmSaveAttendance(This){
        var attended=[];
        var absented=[];
        var specialattended=[];
        var guestattended=[];
        $('input[type=checkbox][name=checkStudentAttendance]').each(function () {
            if (this.checked) {
                attended.push($(this).val());
            }
            else{
                absented.push($(this).val());
            }
        });
        $.each(glbSpecialStudentsAttendance, function (i, item)
        {
            specialattended.push(item.id);
        });

        $.each(glbSpecialStudentsGuestAttendance, function (i, item)
        {
            guestattended.push(item.id);
        });

        attended=attended.join(',').trim();
        absented=absented.join(',').trim();
        specialattended=specialattended.join(',').trim();
        guestattended=guestattended.join(',').trim();

        if(attended=='' && absented=='' && specialattended=='' && guestattended==''){
            alertify.dismissAll();
            notify('No students found','error',10);
        }
        else{
            customConfirmAlert('Manage Attendance', 'Are you sure want to save?');
            $("#popup_confirm #btnTrue").attr("onclick","saveAttendance(this)");
        }
    }
    function saveAttendance(This){
        notify('Processing..', 'warning', 10);
        var attended=[];
        var absented=[];
        var specialattended=[];
        var guestattended=[];
        $('input[type=checkbox][name=checkStudentAttendance]').each(function () {
            if (this.checked) {
                attended.push($(this).val());
            }
            else{
                absented.push($(this).val());
            }
        });
        $.each(glbSpecialStudentsAttendance, function (i, item)
        {
            specialattended.push(item.id);
        });

        $.each(glbSpecialStudentsGuestAttendance, function (i, item)
        {
            guestattended.push(item.id);
        });

        attended=attended.join(',').trim();
        absented=absented.join(',').trim();
        specialattended=specialattended.join(',').trim();
        guestattended=guestattended.join(',').trim();

        if(attended=='' && absented=='' && specialattended=='' && guestattended==''){
            alertify.dismissAll();
            notify('No students found','error',10);
        }
        else{
            var userID= $('#hdnUserId').val();
            var action='list';
            var ajaxurl = API_URL + 'index.php/StudentAttendance/saveAttendance';
            var params = {classId: $('#hdnClassId').val(),attended:attended,absented:absented,specialattended:specialattended,guestattended:guestattended};
            var method = "POST";
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: This, method: method, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: saveAttendanceResponse});
        }
    }
    function saveAttendanceResponse(response){
        $('#popup_confirm').modal('hide');
        alertify.dismissAll();
        if(response.status===true){
            getBatchGraph(this);
            batchInfoPageLoad();
            loadClassDatatables();
            notify('Attendance saved successfully','success',10);
            $('#studentAttendanceModal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }
        else{
            notify('Something went wrong!','error',10);
        }
    }
    // Books issue code starts here //
    function getStudentCourseBooks(This,studentId,batchId,studentName,enrollNum)
    {
        $('#studentName').html(studentName);
        $('#enrollNum').html(enrollNum);
        $('#booksList tbody').html('');
        var ajaxurl = API_URL + 'index.php/BooksIssue/getStudentCourseBooksList';
        var params = {'batchId': batchId, 'studentId': studentId};
        var action = 'list';
        var userId = $('#userId').val();
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: studentBooksList});
    }
    function studentBooksList(response)
    {
        var booksList = '';
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            booksList += '<tr><td colspan="6" class="text-center">No items found</td></tr>';
            $('#booksList tbody').html(booksList);
        }
        else
        {
            var rawData = response.data;
            $.each(rawData, function (key, value) {
                booksList += '<tr>';
                booksList += '<td>'+value.name+'</td>';
                booksList += '<td>'+value.quantity+'</td>';
                booksList += '<td>'+value.issuedQuantity+'</td>';
                if(parseInt(value.quantity) > parseInt(value.issuedQuantity))
                    booksList += '<td><input class="form-control" data-available="'+value.availableQuantity+'" name="quantity" id="quantity_'+value.batch_id+'_'+value.leadId+'_'+value.product_id+'_'+value.quantity+'_'+value.issuedQuantity+'"></td>';
                else
                    booksList += '<td></td>';
                booksList += '</tr>';
            });
            $('#booksList tbody').html(booksList);
            if($('input[name="quantity"]').length >0)
            {
                $('#booksissuefooter').show();
            }
            else{
                $('#booksissuefooter').hide();
            }
        }
        $('#bookIssueModal').modal('show');
    }
    function saveIssueBooks()
    {
        alertify.dismissAll();
        var userID = $('#userId').val();
        var booksIssued = [];
        var flag = 0;
        if($('input[name="quantity"]').length >0){
            $('input[name="quantity"]').each(function() {
                var details = $(this).attr('id').split("_");//batchid details[1] leadid details[2] productid details[3]
                var available = parseInt($(this).attr('data-available'));
                if($(this).val()!= '')
                    var givenQuantity = parseInt($(this).val());
                else
                    var givenQuantity = 0;
                var requiredQunatity = parseInt(details[4]);
                var issuedQuantity = parseInt(details[5]);
                if(givenQuantity>(requiredQunatity-issuedQuantity))
                {
                    $(this).addClass("required");
                    alertify.dismissAll();
                    notify('Value should not be greater than quantity','error', 10);
                    flag = 1;
                    return false;
                }
                if(givenQuantity>available)
                {
                    $(this).addClass("required");
                    alertify.dismissAll();
                    notify('Stock not available','error', 10);
                    flag = 1;
                    return false;
                }
                else {
                    booksIssued.push(details[1] + '|' +details[2]+ '|' + details[3] + '|'+(issuedQuantity+givenQuantity));
                }
            });
        }
        else
        {
            notify('Already books are issued', 'error', 10);
            flag = 1;
        }

        if(flag == 1)
        {
            return false;
        }
        else
        {
            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/BooksIssue/saveBooksIssued';
            var params = {userID: userID,booksIssued:booksIssued};
            var type = "POST";
            var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: SaveBookIssueResponse});
        }
    }
    function SaveBookIssueResponse(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            loadClassDatatables();
            batchInfoPageLoad();
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            $('#bookIssueModal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    }
    // Books issue code ends here //
    function resetSpecialAttendance()
    {
        $("#studentsAutocompleteForm #typOldStudent" ).prop( "checked", true );
        $("#studentsAutocompleteForm #oldStudentsWrapper").show();
        $("#studentsAutocompleteForm #guestStudentsWrapper").hide();
    }
    function listsecondLevelCounsellors(branchId)
    {
        $('#txtsecondaryUser').empty();
        $('#txtsecondaryUser').material_select();
        var userID = $('#userId').val();
        var ajaxurl = API_URL + 'index.php/User/getSecondaryCounselorList/branch_id/'+branchId;
        var type = "GET";
        var action='list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: function(response)
        {

            //$('#txtsecondaryUser').material_select();
            $('#txtsecondaryUser').empty();
            $('#txtsecondaryUser').append($('<option selected></option>').val('').html('--Select--'));
            if (response.data.length > 0) {

                $.each(response.data, function (key, value)
                {
                    if(userID != value.user_id)
                    {
                        $('#txtsecondaryUser').append($('<option></option>').val(value.user_id).html(value.name));
                    }
                });
            }
            $('#txtsecondaryUser').material_select();
        }
        });
    }
</script>

