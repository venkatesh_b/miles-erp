<div class="contentpanel">
    <input type="hidden" id="branchId" value="<?php echo $Id ?>" />
    <input type="hidden" id="batchId" value="<?php echo $Sub_id ?>" />
    <input type="hidden" id="userBranchId" value="0" />
    <input type="hidden" id="CourseId" value="0" />
    <input type="hidden" id="branchSubjectId" value="0" />
    <input type="hidden" id="userId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">No Batch Found</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="tooltipped" data-position="left" data-tooltip="Close" href="<?php echo BASE_URL; ?>app/branchinfo/<?php echo $Id ?>" class="text-dec-none"><i class="icon-times"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <div class="col-sm-8 batch-data" id="batchInfo">

                </div>
                <div class="col-sm-4 pr0"> <img src="<?php echo BASE_URL; ?>assets/images/column-chart.png" class="max-width100p"> </div>
            </div>
            <div class="clearfix"></div>
            <div class="batch-head p0 branches-info branch-head-access">
                <div class=" display-inline-block pl0 relative batch-wrap">
                    <div class="mr5 clearfix">
                        <h4 class="heading-uppercase mb5">STUDENTS (M7)</h4>
                        <div class="col-sm-12 batch-data p0 branches-info">
                            <div class="icon-data pl10"> <i class="icon-users1"></i> </div>
                            <div class="batch-total-info" id="batch-student-m7-details">

                            </div>
                        </div>
                    </div> 
                </div>
                <div class=" display-inline-block p0 relative batch-wrap">
                    <div class="mr5 clearfix">
                        <h4 class="heading-uppercase mb5">Marketing</h4>
                        <div class="col-sm-12 batch-data p0">
                            <div class="icon-data pl10"> <i class="icon-file"></i> </div>
                            <div class="batch-total-info" id="batch-student-stage-details">

                            </div>
                        </div>
                    </div> 
                </div>
                <div class=" display-inline-block pr0 relative batch-wrap">
                    <div class=" clearfix"> 
                        <h4 class="heading-uppercase mb5">DUE/OVERDUE</h4>
                        <div class="col-sm-12 batch-data p0">
                            <div class="icon-data pl10"> <i class="fa fa-thumbs-o-down"></i> </div>
                            <div class="batch-total-info" id="batch-student-due-details">

                            </div>
                        </div>
                    </div> 
                </div>
                <div class=" display-inline-block p0 relative batch-paid">
                    <div class="ml5 clearfix"> 
                        <h4 class="heading-uppercase mb5">PAID</h4>
                        <div class="col-sm-12 batch-data p0">
                            <div class="icon-data pl10"> <i class="icon-file"></i> </div>
                            <div class="batch-total-info"  id="batch-student-paid-details">

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-sm-9 p0 mt5">
                <table class="table table-responsive table-striped table-custom mt5" id="batch-subject-list">
                    <thead>
                        <tr class="">
                            <th>Date</th>
                            <th>Subject</th>
                            <th>Topic</th>
                            <th>Faculty </th>
                            <th class="border-r-none">Venue </th>
                            <th class="no-sort"> 
                                <button data-toggle="modal" data-target="#importModal" onclick="resetImport(this)" class="btn blue-btn btn-xs pull-right margin0 tooltipped" type="button" data-position="top" data-tooltip="Import" >Import</button>
                                <span class="f16 pull-right mr15"> <a onclick="manageBatchSubject(this, 0)" href="javascript:;" class="viewsidePanel tooltipped"  data-position="top" data-tooltip="Add"><i class="icon-plus-circle f18"></i></a> </span> 
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="col-sm-3 mt10 pr0">
                <div class="widget-right border border-blue">
                    <div class="widget-header light-dark-blue ">
                        <h4 class="heading-uppercase f14 m0 text-center p10">Student Enrolled <!--(120)--></h4>
                    </div>
                    <div class="widget-content bg-white clearfix">
                        <div class="col-sm-12">
                            <div class="filter-search mt10">
                                <input type="search" placeholder="Search">
                                <span class="search-icon"><i class="icon-search"></i></span>
                            </div>
                        </div>
                        <div class="col-sm-12 student-enrolled-list">
                            <p class="label-text ellipsis">No students found</p>
                        </div>
                        <!--                        <div class="col-sm-12 student-enrolled-list">
                                                    <label class="label-data width-full">Sripal Jain <span class="pull-right"><i class="fa fa-money mr8 fee-paid"></i><i class="fa fa-book mr8 books-issued"></i></span></label>
                                                    <p class="label-text ellipsis">16CPT3872</p>
                                                </div>
                                                <div class="col-sm-12 student-enrolled-list">
                                                    <label class="label-data width-full">Srinivas Chinnam <span class="pull-right"><i class="fa fa-money mr8 fee-due"></i><i class="fa fa-book mr8 books-not-issued"></i></span></label>
                                                    <p class="label-text ellipsis">16CPT3827</p>
                                                </div>
                                                <div class="col-sm-12 student-enrolled-list">
                                                    <label class="label-data width-full">Sripal Jain<span class="pull-right"><i class="fa fa-money mr8 fee-partaial-paid"></i><i class="fa fa-book mr8 books-partial-issued"></i></span></label>
                                                    <p class="label-text ellipsis">16CPT3872</p>
                                                </div>
                                                <div class="col-sm-12 student-enrolled-list">
                                                    <label class="label-data width-full">Srinivas Chinnam<span class="pull-right"><i class="fa fa-money mr8 fee-paid"></i><i class="fa fa-book mr8 books-issued"></i></span></label>
                                                    <p class="label-text ellipsis">16CPT3827</p>
                                                </div>-->
                    </div>
                </div>
                <!--<img src="images/student-enrolled.png" class="max-width100p">--> 
            </div>
            <!--</div>-->
        </div>
        <!-- InstanceEndEditable --></div>
    <!--Modal create-->
    <div class="modal fade" id="create-subject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="closeCreateSubjectModal()"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                    <h4 class="modal-title" id="subjectModalLabel">Create</h4>
                </div>
                <div class="modal-body modal-scroll clearfix">
                    <form id="create-subjectForm"> 
                        <div class="col-sm-12">
                            <div class="input-field">
                                <label class="datepicker-label datepicker">Date <em>*</em></label>
                                <input id="subjectDate" type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <select id="faculty">
                                    <option value="" selected>--Select--</option>
                                </select>
                                <label class="select-label">Faculty</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <select id="subject" multiple name="subject" class="multiple formSubmit">
                                    <option value="" >--Select All--</option>
                                </select>
                                <label class="select-label">Subject</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <input id="topic" type="text" class="validate" name="topic" class="validate" class="formSubmit">
                                <label for="topic">Topic <em>*</em></label>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="input-field">
                                <select id="venue">
                                    <option value="" selected>--Select--</option>
                                </select>
                                <label class="select-label">Venue</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="actionCreateSubjectButton" onclick="saveBatchSubject(this)" type="button" class="btn blue-btn" ><i class="icon-right mr8"></i>Save</button>
                    <button type="button" onclick="closeCreateSubjectModal()" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--Import Model-->
    <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         data-modal="right" modal-width="750">
        <form id="importForm" method="post">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title">Import</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12">
                            <div class="file-field input-field">
                                <div class="btn file-upload-btn">
                                    <span>Upload Document</span>
                                    <input type="file" id="txtfilebrowser">
                                </div>
                                <div class="file-path-wrapper custom">
                                    <input type="text" placeholder="Upload single file" class="file-path validate">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 mt20">
                            <a class="btn blue-btn btn-sm" href="<?php echo BASE_URL . 'templates/TopicTemplate.csv'; ?>" id="btnDownloadSample"><i class="icon-file mr8"></i>Download Sample</a>
                        </div>

                        <div class="col-sm-12 mb20 mt20">
                            <div class="col-sm-3 p0 ">
                                <div class="batch-info ml0 grey-bg">
                                    <div class="ptb7">
                                        <div class="camp-text f12">Total Contacts</div>
                                        <div class="camp-num red f14" id="TotalUploadedContacts">-</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 p0 ">
                                <div class="batch-info grey-bg">
                                    <div class="ptb7">
                                        <div class="camp-text f12">Uploaded</div>
                                        <div class="camp-num thick-blue f14" id="UploadedContacts">-</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 p0 ">
                                <div class="batch-info grey-bg">
                                    <div class="ptb7">
                                        <div class="camp-text f12">Failed</div>
                                        <div class="camp-num violet f14" id="FailedContacts">-</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3 p0 ">
                                <div class="batch-info mr0 grey-bg">
                                    <div class="ptb7">
                                        <div class="camp-text f12">Duplicate</div>
                                        <div class="camp-num light-black f14" id="DuplicateContacts">-</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <table class="table table-responsive table-striped table-custom" id="uploadedContactsList">
                                <colgroup>
                                    <col width="10%">
                                    <col width="35%">
                                    <col width="50%">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>Row No.</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                    </tr>
                                    <tr>
                                        <td class="border-none p0 lh10">&nbsp;</td>
                                    </tr>
                                    <!-- <tr><td class="border-none p0 lh5">&nbsp;</td></tr>-->
                                </thead>
                                <tbody>
                                    <tr class="">
                                        <td colspan="3" align='center'>No Data Found</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <a class="btn blue-btn" id="importBtn" href="javascript:;" onclick="importTopics(this)"><i class="icon-right mr8"></i>Upload</a>
                        <a href="javascropt:;" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    $context = 'Branches';
    $pageUrl = 'branches';
    $serviceurl = 'branches';
    docReady(function () {
        batchInfoPageLoad();
        loadSubjectDatatables();
    });

    function batchInfoPageLoad()
    {
        var userId = $('#userId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/Batch/getBatchById';
        var params = {'batch_id': '<?php echo $Sub_id ?>', 'branch_id': '<?php echo $Id ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'manage', onSuccess: batchInfo});

        var ajaxurl = API_URL + 'index.php/Batch/getLeadStageCounts';
        var params = {'branchId': '<?php echo $Id ?>', 'batchId': '<?php echo $Sub_id ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'manage', onSuccess: function (response) {
                var dashboard = '';
                if (response.data.length == 0 || response == -1 || response['status'] == false)
                {
                    dashboard += '<div class="text-center">No info found</div>';
                } else
                {
                    var rawData = response.data;
                    dashboard += '<div class="batch-data-info mb10">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">Retail</p>';
                    dashboard += '<label class="label-data f18">' + rawData.student['retail'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    dashboard += '<div class="batch-data-info mb10 pl0">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">Corporate</p>';
                    dashboard += '<label class="label-data f18">' + rawData.student['corporate'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    dashboard += '<div class="batch-data-info mb10">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">Group</p>';
                    dashboard += '<label class="label-data f18">' + rawData.student['institute'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    dashboard += '<div class="batch-data-info mb10 p0">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">Total</p>';
                    dashboard += '<label class="label-data f18">' + rawData.student['total'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    $('#batch-student-m7-details').html(dashboard);
                    dashboard = '';

                    dashboard += '<div class="batch-data-info mb10">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">M6</p>';
                    dashboard += '<label class="label-data f18">' + rawData.marketing['M6'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    dashboard += '<div class="batch-data-info mb10">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">M5</p>';
                    dashboard += '<label class="label-data f18">' + rawData.marketing['M5'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    dashboard += '<div class="batch-data-info mb10">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">M3+</p>';
                    dashboard += '<label class="label-data f18">' + rawData.marketing['M3p'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    dashboard += '<div class="batch-data-info mb10">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">M3</p>';
                    dashboard += '<label class="label-data f18">' + rawData.marketing['M3'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    $('#batch-student-stage-details').html(dashboard);
                    dashboard = '';

                    /* Due/overdue */
                    dashboard += '<div class="batch-due mb10 pl5 pr0">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">Due/Overdue</p>';
                    dashboard += '<label class="label-data f18">' + moneyFormat(rawData.dueamount['amountDue']) + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    dashboard += '<div class="batch-due mb10 ml5">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">Percentage</p>';
                    dashboard += '<label class="label-data f18">' + rawData.dueamount['duePercentage'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    dashboard += '<div class="batch-due mb10">';
                    dashboard += '<div class="pt10">';
                    dashboard += '<p class="label-text p-clr">Students</p>';
                    dashboard += '<label class="label-data f18">' + rawData.dueamount['studentCount'] + '</label>';
                    dashboard += '</div>';
                    dashboard += '</div>';
                    $('#batch-student-due-details').html(dashboard);
                    dashboard = '';

<?php if (strtolower($userData['Role']) == 'superadmin') { ?>
                        $("body").addClass("batch-hideshow");
                        /*Paid html*/
                        dashboard += '<div class="batch-total-due mb10">';
                        dashboard += '<div class="pt10">';
                        dashboard += '<p class="label-text p-clr">Paid</p>';
                        dashboard += '<label class="label-data f18">' + moneyFormat(rawData.paid['amountPaid']) + '</label>';
                        dashboard += '</div>';
                        dashboard += '</div>';
                        dashboard += '<div class="batch-total-due mb10">';
                        dashboard += '<div class="pt10">';
                        dashboard += '<p class="label-text p-clr">Students</p>';
                        dashboard += '<label class="label-data f18">' + rawData.paid['studentCount'] + '</label>';
                        dashboard += '</div>';
                        dashboard += '</div>';
                        $('#batch-student-paid-details').html(dashboard);
<?php } ?>
                }
            }});
    }

    function batchInfo(response)
    {
        var dashboard = '';
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            dashboard += '<div class="text-center">No Batch found</div>';
        } else
        {
            var rawData = response.data;
            dashboard = '';
            dashboard += '<div class="col-sm-3 mtb10">';
            dashboard += '<p class=" label-text">Batch Name</p>';
            dashboard += '<label class="label-data darkblue">' + rawData.code + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-3 mtb10">';
            dashboard += '<p class=" label-text">Created Date</p>';
            dashboard += '<label class="label-data">' + rawData.created_date + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-3 mtb10">';
            dashboard += '<p class=" label-text">Course</p>';
            dashboard += '<label class="label-data">' + rawData.courseName + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-3 mtb10">';
            dashboard += '<p class=" label-text">Status</p>';
            dashboard += '<label class="label-data">' + rawData.status + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-3 mtb10">';
            dashboard += '<p class=" label-text">Marketing Start Date</p>';
            dashboard += '<label class="label-data">' + rawData.marketing_start + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-3 mtb10">';
            dashboard += '<p class=" label-text">Marketing End Date</p>';
            dashboard += '<label class="label-data">' + rawData.marketing_end + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-3 mtb10">';
            dashboard += '<p class=" label-text">Academic Start Date</p>';
            dashboard += '<label class="label-data">' + rawData.acedemic_start + '</label>';
            dashboard += '</div>';
            dashboard += '<div class="col-sm-3 mtb10">';
            dashboard += '<p class=" label-text">Academic End Date</p>';
            dashboard += '<label class="label-data">' + rawData.acedemic_end + '</label>';
            dashboard += '</div>';
            $('#batchInfo').html(dashboard);

            $('.content-header').html(rawData.code);
            $('#CourseId').val(rawData.fk_course_id);
        }
    }

    function manageBatchSubject(This, id) {
        var userId = $('#userId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var CourseId = $('#CourseId').val();
        $.when(
                commonAjaxCall
                (
                        {
                            This: This,
                            headerParams: headerParams,
                            requestUrl: API_URL + "index.php/Venue/getAllVenue",
                            params: {'course_id': CourseId},
                            action: action
                        }
                ),
                commonAjaxCall
                (
                        {
                            This: This,
                            headerParams: headerParams,
                            requestUrl: API_URL + "index.php/Faculty/getAllFaculty",
                            action: action
                        }
                )
                ).then(function (response1, response2) {
            var venue = response1[0];
            var faculty = response2[0];

            buildVenueDropdown(venue);
            buildFacultyDropdown(faculty);

            if (id == 0) {
                //add
                $('#branchSubjectId').val(0);
                $('#create-subject').modal('show');
            } else {
                //edit
                $('#branchSubjectId').val(id);
                $('#actionCreateSubjectButton').html('Update');
                $('#subjectModalLabel').html('Edit');

                var ajaxurl = API_URL + "index.php/Batch/getSubjectForBatch";
                var params = {'batch_xref_subject_id': id};
                action = 'edit';
                headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
                commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                        if (response == -1 || response.data.length == 0 || response['status'] == false) {

                        } else {
                            var rawData = response.data[0];
                            getSubjectBasedOnFaculty('edit', rawData.fk_faculty_id);
                            $('#faculty').val(rawData.fk_faculty_id);
                            $('#faculty').material_select();
                            $('#topic').val(rawData.topic);
//                        $('#subjectDate').val(rawData.topic_date);
                            setDatePicker('#subjectDate', rawData.topic_date);
                            $('#venue').val(rawData.fk_venue_id);
                            $('#venue').material_select();
                            setTimeout(function () {
                                $('#subject').val(rawData.fk_subject_id);
                                $('#subject').material_select();
                                $('#create-subject').modal('show');
                            }, 250);
                            Materialize.updateTextFields();
                        }
                    }});
            }
        });
    }

    $('#faculty').change(function () {
        getSubjectBasedOnFaculty('list', 0);
    });

    $('#subject').change(function () {
        multiSelectDropdown($(this));
    });

    function getSubjectBasedOnFaculty(action, faculty) {
        if (faculty == 0) {
            faculty = $('#faculty').val();
        }
        var userId = $('#userId').val();
//        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/Subject/getAllSubjectByFaculty';
        var params = {'faculty_id': faculty};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'manage', onSuccess: function (response) {
                buildSubjectDropdown(response);
            }});
    }

    function saveBatchSubject() {
        $('#create-subjectForm label').removeClass("active");
        $('#create-subjectForm span.required-msg').remove();
        $('#create-subjectForm input, #create-subjectForm .select-dropdown').removeClass("required");

        var flag = 0;
        var subjectDate = $('#subjectDate').val();
//        var subject = $('#subject').val();
        var topic = $('#topic').val();
        var faculty = $('#faculty').val();
        var venue = $('#venue').val();

        var selMulti = $.map($("#subject option:selected"), function (el, i)
        {
            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'all')
            {
                return $(el).val();
            }
        });
        var subject = selMulti.join(",");

        if (topic == '') {
            $("#topic").addClass("required");
            $("#topic").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (!(/^[a-zA-Z0-9 ,.]*$/.test(topic))) {
            $("#topic").addClass("required");
            $("#topic").after('<span class="required-msg">Only alphanumeric</span>');
            flag = 1;
        }

        if (subjectDate == '') {
            $("#subjectDate").addClass("required");
            $("#subjectDate").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }

        if (faculty == '') {
            $("#faculty").parent().find('.select-dropdown').addClass("required");
            $("#faculty").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (venue == '') {
            $("#venue").parent().find('.select-dropdown').addClass("required");
            $("#venue").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if (subject == '') {
            $("#subject").parent().find('.select-dropdown').addClass("required");
            $("#subject").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        var ajaxurl, type, method, action, headerParams = '';
        var params = [];
        var userID = $('#userId').val();
        if (flag != 1) {
            var id = $('#branchSubjectId').val();
            var batchId = $('#batchId').val();
            if (id == 0) {
                ajaxurl = API_URL + 'index.php/Batch/saveSubjectForBatch';
                action = 'add';
            } else {
                ajaxurl = API_URL + 'index.php/Batch/editSubjectForBatch/batch_xref_subject_id/' + id;
                action = 'edit';
            }

            params = {batchId: batchId, subjectDate: subjectDate, subject: subject, topic: topic, faculty: faculty, venue: venue};
            method = "POST";
            headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: method, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: function (response) {
                    if (response['status'] == false || response == -1) {
                        for (var a in response.data) {
                            id = '#' + a;
                            if (a == 'subject' || a == 'faculty' || a == 'venue') {
                                $(id).parent().find('.select-dropdown').addClass("required");
                                $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else if (a == 'id') {
                                notify(response.data[a], 'error', 10);
                            } else {
                                $(id).addClass("required");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            }
                        }
                        Materialize.updateTextFields();
                    } else {
                        alertify.dismissAll();
                        notify('Saved successfully', 'success', 10);
                        loadSubjectDatatables();
                        closeCreateSubjectModal();
                    }
                }});
        } else {
//            console.log('error');
        }
    }

    function deleteBatchSubject(This, id, currentStatus) {
        var status = currentStatus == 1 ? 'inactivate' : 'activate';
        var conf = "Are you sure want to " + status + " this topic?";
        customConfirmAlert('Topic Status', conf);
        $("#popup_confirm #btnTrue").attr("onclick", "deleteBatchSubjectConfirm(this,'" + id + "')");
    }

    function deleteBatchSubjectConfirm(This, id) {
        var userId = $('#userId').val();
        var ajaxurl = API_URL + 'index.php/Batch/deleteBatchSubject/batch_xref_subject_id/' + id;
        var type = "POST";
        var action = 'delete';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: 'delete', onSuccess: deleteBatchSubjectResponse});
    }

    function deleteBatchSubjectResponse(response) {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false) {
            notify(response.message, 'error', 10);
        } else {
            notify(response.message, 'success', 10);
            loadSubjectDatatables();
        }
    }

    function closeCreateSubjectModal() {
        $('#create-subjectForm')[0].reset();

        $('#branchSubjectId').val(0);

        $('#subject').find('option:gt(0)').remove();
        $('#venue').find('option:gt(0)').remove();
        $('#faculty').find('option:gt(0)').remove();
        $('#reference_type_val_id').val('');

        clearDatePicker('#subjectDate');

        $('#create-subjectForm label').removeClass("active");
        $('#create-subjectForm span.required-msg').remove();
        $('#create-subjectForm input, #create-subjectForm .select-dropdown').removeClass("required");

        $("select[name=subject]").val(0);
        $("select[name=venue]").val(0);
        $("select[name=faculty]").val(0);
        $('select').material_select();

        $("#countryModalLabel").html("Create City");

        $('#subjectModalLabel').html('Create');

        $('#actionCreateSubjectButton').html('Save');

        $('#create-subject').modal('hide');
    }

    function loadSubjectDatatables() {
        var userId = $('#userId').val();
        var batchId = $('#batchId').val();
        var ajaxurl = API_URL + 'index.php/Batch/getBatchSubjectList';
        var params = {'batch_id': batchId};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        $("#batch-subject-list").dataTable().fnDestroy();
        var CityTableObj = $('#batch-subject-list').DataTable({
            "fnDrawCallback": function () {
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
                    {
                        "sSearch": "<span class='icon-search f16'></span>",
                        "sEmptyTable": "No data found",
                        "sZeroRecords": "No data found",
                        "sProcessing": "<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                    },
            ajax: {
                url: ajaxurl,
                type: 'GET',
                data: params,
                headers: headerParams,
                error: function (response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false
            }],
            columns: [
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.topic_date;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.code;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.topic;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.faculty_name;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.venue_name;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        var rawHtml = '';
                        var status = data.topic_active == 1 ? 'activate' : 'inactivate';
                        var statusText = data.topic_active == 1 ? 'Inactive' : 'Active';
                        rawHtml += '';
                        rawHtml += '<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        rawHtml += '<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                        rawHtml += '<i class="fa fa-ellipsis-v f18 plr10"></i>';
                        rawHtml += '</a>';
                        rawHtml += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        rawHtml += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        rawHtml += '<li><a access-element="edit" href="javascript:;" onclick="manageBatchSubject(this,' + data.batch_xref_class_id + ')">Edit</a></li>';
                        rawHtml += '<li><a access-element="delete" href="javascript:;" onclick="deleteBatchSubject(this,\'' + data.batch_xref_class_id + '\',\'' + data.topic_active + '\')">' + statusText + '</a></li>';
                        rawHtml += '</ul>';
                        rawHtml += '</div>';
                        return rawHtml;
                    }
                }
            ],
            "createdRow": function (row, data, index)
            {
                if (data.topic_status == 'current') {
                    $(row).addClass('lt-red-bg');
                } else if (data.topic_status == 'new') {
                    $(row).addClass('lt-green-bg');
                } else {
                    $(row).addClass('lt-blue-bg');
                }
            }
        });
        DTSearchOnKeyPressEnter(CityTableObj);
    }

    function importTopics(This) {

        var ajaxurl = API_URL + 'index.php/Batch/BulkAddClass';
        var userID = $('#userId').val();
        var action = 'import';
        var fileUpload = document.getElementById("txtfilebrowser");

        var selectedFile = $("#txtfilebrowser").val();
        var extension = selectedFile.split('.');
        if (selectedFile.length <= 0){
            $("#txtfilebrowser").focus();
            notify('Select a file', 'error', 5);
            return;
        } else if (extension[1] != "csv") {
            $("#txtfilebrowser").focus();
            notify('Please upload a .csv file', 'error', 5);
            return;
        } else {
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};

            if (fileUpload.value != null) {
                var uploadFile = new FormData();
                var files = $("#txtfilebrowser").get(0).files;
                var batch = $("#batchId").val();
                if (files.length > 0) {
                    uploadFile.append("file", files[0]);
                    uploadFile.append("batch", batch);
                    $.ajax({
                        type: 'POST',
                        url: ajaxurl,
                        dataType: "json",
                        data: uploadFile,
                        contentType: false,
                        processData: false,
                        headers: headerParams,
                        beforeSend: function () {
                            $(This).attr("disabled", "disabled");
                            alertify.dismissAll();
                            notify('Processing..', 'error', 10);
                        }, success: function (response) {
                            $(This).removeAttr("disabled");
                            finalresp(response);
                        }, error: function (response) {
                            alertify.dismissAll();
                            if (response.responseJSON.message == "Access Token Expired") {
                                logout();
                            } else if (response.responseJSON.message == "Invalid credentials") {
                                logout();
                            } else {
                                notify('Something went wrong. Please try again', 'error', 10);
                            }

                        }
                    });
                } else {
                    $("#txtfilebrowser").parent().find('.file-path-wrapper').addClass("required");
                    $("#txtfilebrowser").parent().find('.file-path-wrapper').after('<span class="required-msg">required field</span>');
                }
            } else {

            }
        }
    }
    function finalresp(response) {
        if (response.status === true) {
            alertify.dismissAll();
            var counts = response.data.Counts;
            $('#TotalUploadedContacts').html(counts.total_count);
            $('#UploadedContacts').html(counts.successCount);
            $('#FailedContacts').html(counts.failedCount);
            $('#DuplicateContacts').html(counts.duplicateCount);
            var records = response.data.Records;
            var rowsHtml = "";
            $.each(records, function (key, value) {
                rowsHtml += "<tr>";
                rowsHtml += "<td>" + value.row_number + "</td>";
                rowsHtml += "<td>" + value.topic + "</td>";
                rowsHtml += "<td>";
                if (value.status == 0) {
                    //pending
                    rowsHtml += "<span class='label f11 status-label font-normal label-warning'>Pending</span>";
                }
                if (value.status == 1) {
                    //success
                    rowsHtml += "<span class='label f11 status-label font-normal label-success'>Success</span>";
                    value.error_log = 'Add/Update Successfully';
                }
                if (value.status == 2) {
                    //fail
                    if (value.error_log == 'Duplicate entry.') {
                        rowsHtml += "<span class='label f11 status-label font-normal label-warning'>Duplicate</span>";
                    } else {
                        rowsHtml += "<span class='label f11 font-normal status-label label-danger'>Failed</span>";
                    }
                }

                rowsHtml += "<span style=\"width: 275px;\" class='text-success display-inline-block valign-middle ellipsis' title='" + value.error_log + "'>" + value.error_log + "</span>";
                rowsHtml += "</td>";
                rowsHtml += "</tr>";
            });
            $('#uploadedContactsList tbody').html(rowsHtml);
            $("#txtfilebrowser").val('');
            $("#txtfilebrowser").trigger('change');
            notify("Uploaded Successfully", "success", 10);
            loadSubjectDatatables();
        } else {
            alertify.dismissAll();
            notify(response.message, "error", 10);
        }
    }
    function resetImport(This) {
        $('#importForm')[0].reset();
        $('#TotalUploadedContacts').html('-');
        $('#UploadedContacts').html('-');
        $('#FailedContacts').html('-');
        $('#DuplicateContacts').html('-');
        var rowsHtml = "<tr><td colspan='3' align='center'>No Data Found</td></tr>";
        $('#uploadedContactsList tbody').html(rowsHtml);
//        $('#importBtn').removeAttr('disabled');
        $(This).attr("data-target", "#importModal");
        $(This).attr("data-toggle", "modal");
    }
</script>
