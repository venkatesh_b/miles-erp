<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-17
 * Time: 12:30 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Branch GRN</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="" data-toggle="modal"><i class="icon-plus-circle" onclick="manageBranchGrn(this,0)"></i></a></li>
                <!--<li><a><i class="icon-filter"></i></a></li>-->
                <!--<li>
                    <input class="content-header-search" type="text" placeholder="search"/>
                    <a class="content-header-search-btn"><i class="icon-search"></i></a>
                </li>-->
                <li><a></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="branchGrnList">
                    <thead>
                    <tr>
                        <th>GRN No.</th>
                        <th>Received By</th>
                        <th>Received On</th>
                        <th>Sent By</th>
                        <th>Sent On</th>
                        <th>No. of Items </th>
                        <th>Invoice</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="mybranchGrnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="800">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Create GRN</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <form name="branch_grn" id="branch_grn">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="grn_no" type="text" name="grn_no" value="" onkeypress="return isNumberKey(event)" class="validate">
                                    <label for="grn_no">GDN No. <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12 sent-field">
                                <div class="input-field">
                                    <input id="sent_by" name="sent_by" type="text"  class="validate" disabled="disabled">
                                    <input id="request_mode_id" type="hidden" name="request_mode_id" >
                                    <label for="sent_by">Sent By <em>*</em></label>
                                    <input id="stock_flow_id" type="hidden" name="stock_flow_id" >
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtinvoice" name="txtinvoice" type="text" class="validate" maxlength="20" />
                                    <label for="txtinvoice">Invoice Number <em>*</em></label>

                                </div>
                            </div>
                            <div class="col-sm-12 mb15">
                                <div class="file-field input-field">
                                    <div class="btn file-upload-btn">
                                        <span>Upload Invoice <em>*</em></span>
                                        <input type="file" id="txtfileinvoice">
                                    </div>
                                    <div class="file-path-wrapper custom">
                                        <input type="text" placeholder="Upload single file" class="file-path validate">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="txtRemarks" name="txtRemarks" class="materialize-textarea" maxlength="255"></textarea>
                                    <label for="txtRemarks">Remarks</label>
                                </div>
                            </div>
                        </form>
                        <form name="branchGrnForm" id="branchGrnForm">
                            <div class="col-sm-12">
                                <div class="product-map-table p0">
                                    <table class="table table-responsive table-bordered mb0" id="branch-grn-products">
                                        <thead>
                                        <tr>
                                            <th>Product Code</th>
                                            <th>Quantity</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="branchGrn_btn" class="btn blue-btn" onclick="saveBranchGrn(this)"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="branchGrnProducts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="600">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="branchGrnModal"></h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12 product-map-table p0" id="gtnProductsBranch">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save Changes</button>-->
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script>
    docReady(function(){
        buildBranchGrnDataTable();
    });
    $( "#userBranchList" ).change(function() {
        buildBranchGrnDataTable();
        changeDefaultBranch();
    });
    function buildBranchGrnDataTable()
    {
        var ajaxurl=API_URL+'index.php/BranchGrn/branchGrnList';
        var userID=$('#hdnUserId').val();
        var params={branchId:$('#userBranchList').val()};
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#branchGrnList").dataTable().fnDestroy();
        $tableobj=$('#branchGrnList').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                data:params,
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            "order": [[ 0, "desc" ]],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" data-target=\"#branchGrnProducts\" data-toggle=\"modal\"  onclick=\"getBranchGrnProduct(this,"+data.sequence_number+",'"+data.new_sequence_number+"')\">"+data.new_sequence_number+"</a></div>";
                    //return data.sequence_number;
                    return html;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receiveName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receivedOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.requestedName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.sentOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.total;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    if(data.invoice_number.trim()=='' || data.invoice_number==null){
                        html='----';
                    }
                    else{
                        html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" onclick=\"downloadInvoice('"+data.invoice_attachment+"')\">"+data.invoice_number+"</a></div>";
                    }
                    return html;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function manageBranchGrn(This,Id)
    {
        $('#branchGrn_btn').html('');
        if(Id==0)
        {
            $('#branchGrn_btn').html('<i class="icon-right mr8"></i>Add');
        }
        else
        {
            $('#branchGrn_btn').html('<i class="icon-right mr8"></i>Update');
        }
        $('#branch_grn')[0].reset();
        $('#branch_grn span.required-msg').remove();
        $('#branch_grn input').removeClass("required");
        $("#branch_grn").removeAttr("disabled");
        $('#branchGrnForm')[0].reset();
        Materialize.updateTextFields();
        $('#branchGrnForm span.required-msg').remove();
        $('#branchGrnForm input').removeClass("required");
        $("#branchGrnForm").removeAttr("disabled");
        $('#branch-grn-products tbody').html('');
        $('#sent_by').val('');
        $('#request_mode_id').val('');
        $('#stock_flow_id').val('');
        $('#hdnProductId').val('');
        var userID = $("#hdnUserId").val();
        if (Id == 0)
        {
            $("#myModalLabel").html("Create GRN");
            getWarehouseGtnNumbers();
        }
        $('#mybranchGrnModal').modal('show');
    }
    function getWarehouseGtnNumbers()
    {
        branchGrnProducts ='';
        var headerParams = {
            action: 'add',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $("#grn_no").jsonSuggest({
            onSelect:function(item){
                $("#grn_no").val(item.id);
                getGtnProductsByNo(item.id);
            },
            headers:headerParams,url:API_URL + 'index.php/BranchGrn/getWarehouseGtnNumbers' , minCharacters: 2});
    }
    function getGtnProductsByNo(gtnId)
    {
        var userID = $("#hdnUserId").val();
        var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall
        (
            {
                This: this,
                headerParams: headerParams,
                params: {gtnId: gtnId},
                requestUrl: API_URL + 'index.php/BranchGrn/getGtnProductDetails',
                action: 'add',
                onSuccess : setGtnProductResponse
            }
        )
    }
    function setGtnProductResponse(response)
    {
        $('#branch-grn-products tbody').html('');
        alertify.dismissAll();
        var details = response;
        if (details == -1 || details['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var productData = details.data;
            var products = '';
            $('#sent_by').val(productData.request_mode);
            $("#sent_by").next('label').addClass("active");
            $("#sent_by").attr('disabled',true);
            $('#request_mode_id').val(productData.request_id);
            $("#request_mode_id").next('label').addClass("active");
            $("#request_mode_id").attr('disabled',true);
            $('#stock_flow_id').val(productData.stockflow_id);
            var branchGrnProducts = new Object();
            $.each(productData.products, function (key, value) {
                products +='<tr><td>'+value.code+'</td>';
                products +='<td><span id="txtQuantity_'+value.product_id+'">'+value.quantity+'</span>';
                products +='<div class="col-sm-4 pl0"><input class="modal-table-textfiled m0" type="hidden" name="return_quantity" id="txtReturn_'+value.product_id+'" /></div></td>';
            });
            $('#branch-grn-products tbody').append(products);
        }
    }
    function saveBranchGrn(This)
    {
        var sentBy = $('#sent_by').val();
        var sentById = $('#request_mode_id').val();
        var stock_flow_id = $('#stock_flow_id').val();
        var grnNo = $('#grn_no').val();
        var flag = 0;
        $('#branch_grn span.required-msg').remove();
        $('#branch_grn input').removeClass("required");
        var branchId=$('#userBranchList').val();
        var userID = $("#hdnUserId").val();
        var returnProducts = [];
        var totalProducts = 0;
        var grnRemarks = $('#txtRemarks').val().trim();
        var grnInvoice = $('#txtinvoice').val().trim();
        var files = $("#txtfileinvoice").get(0).files;
        var selectedFile = $("#txtfileinvoice").val();
        var fileUpload = document.getElementById("txtfileinvoice");
        var InValidExtensions = ['exe']; //array of in valid extensions
        $('input[name="return_quantity"]').each(function() {
            totalProducts = totalProducts+1;
            var currentVal = parseInt($(this).val());
            var res = $(this).attr('id').split("_");
            var actualQuantity = parseInt($('#txtQuantity_'+res[1]).html());
            var returnQuantity = 0;
            var returnRemarks = '';
            returnProducts.push(res[1] + '|' +actualQuantity+ '|' + returnQuantity + '|'+returnRemarks);
        });
        if (grnNo == '')
        {
            $("#grn_no").addClass("required");
            $("#grn_no").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (grnInvoice == '')
        {
            $("#txtinvoice").addClass("required");
            $("#txtinvoice").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (fileUpload.value == null || fileUpload.value == '') {
            alertify.dismissAll();
            notify('Upload the invoice', 'error', 10);
            flag = 1;
        }
        else{
            var fileName = selectedFile;
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            if ($.inArray(fileNameExt, InValidExtensions) == -1){

            }
            else{
                alertify.dismissAll();
                notify('Upload valid invoice', 'error', 10);
                flag = 1;
            }
        }
        if(grnNo !='' && totalProducts ==0)
        {
            alertify.dismissAll();
            $("#grn_no").addClass("required");
            $("#grn_no").after('<span class="required-msg">No products for this GTN</span>');
            notify('No products for this GTN','error', 10);
            flag=1;
        }
        if(flag == 1)
        {
            return false;
        }
        else
        {
            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/BranchGrn/addBranchGrn';
            var params = {userID: userID,grnNo:grnNo, receiverMode: 'branch',receiverObjectId:branchId, requestMode: sentBy ,requestObjectId :sentById, returnProducts : returnProducts};
            var type = "POST";
            var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            //commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveBranchGrnResponse});
            var uploadFile = new FormData();
            if (files.length > 0) {
                uploadFile.append("file", files[0]);
                uploadFile.append("image", 'invoice_' + new Date().getTime());
            }
            uploadFile.append('userID', userID);
            uploadFile.append('grnNo', grnNo);
            uploadFile.append('requestMode', sentBy);
            uploadFile.append('requestObjectId', sentById);
            uploadFile.append('receiverMode', 'branch');
            uploadFile.append('receiverObjectId', branchId);
            uploadFile.append('grnRemarks', grnRemarks);
            uploadFile.append('grnInvoice', grnInvoice);
            uploadFile.append('returnProducts', returnProducts);
            uploadFile.append('stock_flow_id', stock_flow_id);
            $.ajax({
                type: type,
                url: ajaxurl,
                dataType: "json",
                data: uploadFile,
                contentType: false,
                processData: false,
                headers: headerParams,
                beforeSend: function () {
                    $(This).attr("disabled", "disabled");
                }, success: function (response) {
                    alertify.dismissAll();
                    $(This).removeAttr("disabled");
                    SaveBranchGrnResponse(response);
                }, error: function (response) {
                    alertify.dismissAll();
                    notify('Something went wrong. Please try again', 'error', 10);
                }
            });
        }
    }
    function SaveBranchGrnResponse(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
            if(response.data.invoiceNumber){
                $("#txtinvoice").addClass("required");
                $("#txtinvoice").after('<span class="required-msg">'+response.data.invoiceNumber+'</span>');
                flag = 1;
            }
        }
        else
        {
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            $('#mybranchGrnModal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildBranchGrnDataTable();
        }
    }
    function getBranchGrnProduct(This,seqNum,newseqNum)
    {
        $('#branchGrnModal').html('GRN No. '+newseqNum);
        $('#gtnProductsBranch').html('');
        var ajaxurl = API_URL + 'index.php/BranchGrn/getBranchGrnProducts';
        var params = {sequenceNumber:seqNum};
        var userID = $('#hdnUserId').val();
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({
            This: this,
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: 'list',
            onSuccess: function (response) {
                var stockhtml = '';
                stockhtml += "<table class=\"table table-responsive table-bordered mb0\">";
                stockhtml += "<thead>";
                stockhtml += "<tr class=\"\">";
                stockhtml += "<th>Product Name</th>";
                stockhtml += "<th>Quantity</th>";
                stockhtml += "</tr>";
                stockhtml += "</thead>";
                stockhtml += "<tbody>";
                if (response.status === true && response.data.length > 0) {
                    $.each(response.data, function (key, value) {
                        stockhtml += "<tr class=\"\">";
                        stockhtml += "<td>" + value.productName + "</td>";
                        stockhtml += "<td>" + value.quantity + "</td>";
                        stockhtml += "</tr>";
                    });
                }
                else {
                    stockhtml = "<tr colspan='2'>No records found</tr>";
                }
                stockhtml += "</tbody>";
                stockhtml += "</table>";
                $('#gtnProductsBranch').html(stockhtml);
            }
        });
    }
</script>
