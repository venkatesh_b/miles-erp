<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-19
 * Time: 04:46 PM
 */
?>

<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Branch GDN</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="" data-toggle="modal"><i class="icon-plus-circle" onclick="manageBranchGtn(this,0)"></i></a></li>
                <!--<li><a><i class="icon-filter"></i></a></li>-->

                <li><a></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="branch-gtn-list">
                    <thead>
                    <tr>
                        <th>GDN No.</th>
                        <th>Received By</th>
                        <th>Received On</th>
                        <th>Sent By</th>
                        <th>Sent On</th>
                        <th>No. of Items </th>
                        <th>Invoice</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="myBranchGtnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="700">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Create GDN</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <form name="branchGtnForm" id="branchGtnForm">
                            <div class="col-sm-12">
                                <div id="addNewProduct">
                                    <div class="col-sm-12 p0">

                                        <div class="multiple-radio">
                                            <!--<label>Received From</label>-->
                                        <span class="inline-radio" >
                                            <input id="txtWarehouse" class="with-gap" type="radio" name="transferTo" value="warehouse" checked>
                                            <label for="txtWarehouse">Warehouse</label>
                                        </span>
                                        <span class="inline-radio">
                                            <input id="txtBranch" class="with-gap" type="radio" name="transferTo" value="branch">
                                            <label for="txtBranch" >Branch</label>
                                        </span>
                                        </div>
                                        <div class="col-sm-12 p0 ">
                                            <div id="branchWrapper" class="input-field">
                                                <select id="transfer_to_branch" name="transfer_to_branch">
                                                </select>
                                                <label class="select-label">Branch <em>*</em></label>


                                            </div>
                                        </div>
                                        <div class="col-sm-12 p0">
                                            <div class="input-field">
                                                <input id="txtinvoice" name="txtinvoice" type="text" class="validate" maxlength="20" />
                                                <label for="txtinvoice">Invoice Number <em>*</em></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 mb15 p0">
                                            <div class="file-field input-field">
                                                <div class="btn file-upload-btn">
                                                    <span>Upload Invoice <em>*</em></span>
                                                    <input type="file" id="txtfileinvoice">
                                                </div>
                                                <div class="file-path-wrapper custom">
                                                    <input type="text" placeholder="Upload single file" class="file-path validate">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 p0">
                                            <div class="input-field">
                                                <textarea id="txtRemarks" name="txtRemarks" class="materialize-textarea" maxlength="255"></textarea>
                                                <label for="txtRemarks">Remarks</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-11 p0">
                                        <div class="col-sm-6 pl0">
                                            <div class="input-field">
                                                <input id="txtProductCode" name="txtProductCode" type="text" class="validate" />
                                                <input id="hdnProductId" name="hdnProductId" type="hidden" class="validate" />
                                                <label for="txtProductCode" >Product Code <em>*</em></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pl0">
                                            <div class="input-field">
                                                <input id="txtProductQuantity" name="txtProductQuantity" type="text" class="validate" onkeypress="return isNumberKey(event)" />
                                                <input id="hdnProductQuantity" name="hdnProductQuantity" type="hidden" />
                                                <label for="txtProductQuantity" >Quantity <em>*</em></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-1 pl0">
                                    <a class="m10 ml0 display-inline-block cursor-pointer" onclick="addNewGtnProduct()"> <i class="icon-plus-circle f24 sky-blue"></i> </a>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="product-map-table p0">
                                    <table class="table table-responsive table-bordered mb0" id="branch-gtn">
                                        <thead>
                                        <tr>
                                            <th>Product Code</th>
                                            <th>Quantity</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="branchGtn_btn" class="btn blue-btn" onclick="saveBranchGtn(this)"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="branchGtnProducts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="600">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="branchGtnModal"></h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12 product-map-table p0" id="gtnProductsBranch">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save Changes</button>-->
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script>
    docReady(function(){
        buildBranchGtnDataTable();
        $('input[type=radio][name=transferTo]').change(function() {
            $('#branchWrapper span.required-msg').remove();
            $('#branchWrapper select').removeClass("required");
            $('#branchGtnForm span.required-msg').remove();
            $('#branchGtnForm input').removeClass("required");
            $("#branchGtnForm").removeAttr("disabled");
            if (this.value == 'branch') {
                getGtnBranches($('#userBranchList').val());
            }
            if (this.value == 'warehouse') {
                $("#branchWrapper").hide();
                $('#transfer_to_branch').val('');
                $('#transfer_to_branch').material_select();
            }
        });
    });
    function getGtnBranches(branchId)
    {

        $("#branchWrapper").show();
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/WarehouseGtn/getBranches';
        var action = 'add';
        var params = {};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response){
                $('#transfer_to_branch').empty();
                $('#transfer_to_branch').append($('<option  selected></option>').val('').html('--Select--'));
                if (response.status == true) {

                    if (response.data.length > 0) {

                        $.each(response.data, function (key, value) {
                            if(value.id!=branchId)
                                $('#transfer_to_branch').append($('<option></option>').val(value.id).html(value.text));
                        });
                    }
                }
                $('#transfer_to_branch').material_select();

            }});
    }

    $( "#userBranchList" ).change(function() {
        buildBranchGtnDataTable();
        changeDefaultBranch();
    });
    function buildBranchGtnDataTable()
    {
        var ajaxurl=API_URL+'index.php/BranchGtn/branchGtnList';
        var userID=$('#hdnUserId').val();
        var params={branchId:$('#userBranchList').val()};
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#branch-gtn-list").dataTable().fnDestroy();
        $tableobj=$('#branch-gtn-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                data:params,
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            "order": [[ 0, "desc" ]],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" data-target=\"#branchGtnProducts\" data-toggle=\"modal\"  onclick=\"getBranchGtnProduct(this,"+data.sequence_number+",'"+data.new_sequence_number+"')\">"+data.new_sequence_number+"</a></div>";
                    return html;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receiveName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receivedOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.requestedName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.sentOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.total;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    if(data.invoice_number.trim()=='' || data.invoice_number==null){
                        html='----';
                    }
                    else{
                        html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" onclick=\"downloadInvoice('"+data.invoice_attachment+"')\">"+data.invoice_number+"</a></div>";
                    }
                    return html;

                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    var rowNum=0;
    var grnProducts = new Object();
    function manageBranchGtn(This,Id)
    {
        $('#branchGtn_btn').html('');
        if(Id==0)
        {
            $('#branchGtn_btn').html('<i class="icon-right mr8"></i>Add');
        }
        else
        {
            $('#branchGtn_btn').html('<i class="icon-right mr8"></i>Update');
        }
        $('#branchGtnForm')[0].reset();
        Materialize.updateTextFields();
        $('#branchGtnForm span.required-msg').remove();
        $('#branchGtnForm input').removeClass("required");
        $("#branchGtnForm").removeAttr("disabled");
        $('#branch-gtn tbody').html('');
        grnProducts = new Object();
        rowNum =0;
        $('#hdnSentBy').val('');
        $('#hdnProductId').val('');
        $('#hdnProductQuantity').val('');
        var userID = $("#hdnUserId").val();
        if (Id == 0)
        {
            $("#myModalLabel").html("Create GDN");
            getProducts()
        }
        $('input[type=radio][name=transferTo][value=warehouse]').click();
        $('input[type=radio][name=transferTo][value=warehouse]').trigger('change');
        $('#myBranchGtnModal').modal('show');

    }
    function getProducts()
    {
        $('#txtProductCode').next('ul').remove();
        var headerParams = {
            action: 'add',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $("#txtProductCode").jsonSuggest({
            onSelect:function(item){
                var product = item.text;
                var productId = item.id;
                var productIdQuantity = productId.split(' | ');
                var productName = product.split(' | ');
                $("#txtProductCode").val(productName[0]);
                $("#hdnProductId").val(productIdQuantity[0]);
                $("#hdnProductQuantity").val(parseInt(productIdQuantity[1]));
            },
            headers:headerParams,url:API_URL + 'index.php/BranchGtn/getProducts' , minCharacters: 2});
    }
    function addNewGtnProduct()
    {
        var productCode = $("#txtProductCode").val();
        var productId = $("#hdnProductId").val();
        var productQuantity = $("#hdnProductQuantity").val();
        var quantity = $("#txtProductQuantity").val();
        if(quantity!='')
            var quantity = parseInt($("#txtProductQuantity").val());
        if(productQuantity!='')
            var productQuantity = parseInt($("#hdnProductQuantity").val());
        $('#branchGtnForm span.required-msg').remove();
        $('#branchGtnForm input').removeClass("required");
        var flag = 0;
        var regx_productName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
        if (productCode == '')
        {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_productName.test($('#txtProductCode').val()) === false) {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (productCode != '' && productId == '')
        {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (quantity == '')
        {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (quantity <= 0) {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">Should be >0</span>');
            flag = 1;
        }else if(quantity>productQuantity) {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">Insufficient Quantity</span>');
            flag = 1;
        }
        if (flag == 1) {
            return false;
        } else {
            var products = "";
            if(typeof grnProducts[productId]!='undefined')
            {
                $("#txtProductCode").addClass("required");
                $("#txtProductCode").after('<span class="required-msg">Product already added</span>');
            }
            else
            {
                var obj = new Object();
                obj.quantity=quantity;
                grnProducts[productId] = obj;
                products +='<tr id=rownum'+rowNum+'><td>'+productCode+'</td>';
                products +='<td><div class="col-sm-11 p0">'+quantity+'</div><div class="col-sm-1 p0"> <span class="filter-selectall-wrap pull-right"><a href="javascript:;" class="reportFilter-unselectall mr0" onclick="removeProduct('+rowNum+',\''+productId+'\')"><i class="icon-times red"></i></a></span> </div></td></tr>';
                $('#branch-gtn tbody').append(products);
                $('#txtProductCode').val('');
                $('#txtProductQuantity').val('');
                $('#hdnProductId').val('');
                $('#hdnProductQuantity').val('');
                $(".addNewProduct label").removeClass("active");
                rowNum = rowNum+1;
            }
        }
    }
    function removeProduct(rowNum,pcode)
    {
        delete grnProducts[pcode];
        $('#rownum'+rowNum).remove();
    }
    function saveBranchGtn(This)
    {
        $('#branchGtnForm span.required-msg').remove();
        $('#branchGtnForm input').removeClass("required");
        $("#branchGtnForm").removeAttr("disabled");
        var transferTo = $("input[name=transferTo]:checked").val();
        var sentBy = $('#hdnSentBy').val();
        var gtnRemarks = $('#txtRemarks').val().trim();
        var gtnInvoice = $('#txtinvoice').val().trim();
        var files = $("#txtfileinvoice").get(0).files;
        var selectedFile = $("#txtfileinvoice").val();
        var fileUpload = document.getElementById("txtfileinvoice");
        var InValidExtensions = ['exe']; //array of in valid extensions
        var flag = 0;
        var finalProducts = [];
        var userID = $("#hdnUserId").val();
        var branchId=$('#userBranchList').val();
        var receiverMode='';
        var receiverObjectId=0;
        if (gtnInvoice == '')
        {
            $("#txtinvoice").addClass("required");
            $("#txtinvoice").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (fileUpload.value == null || fileUpload.value == '') {
            alertify.dismissAll();
            notify('Upload the invoice', 'error', 10);
            flag = 1;
        }
        else{
            var fileName = selectedFile;
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            if ($.inArray(fileNameExt, InValidExtensions) == -1){

            }
            else{
                alertify.dismissAll();
                notify('Upload valid invoice', 'error', 10);
                flag = 1;
            }
        }
        if(transferTo=='warehouse'){
            receiverMode='warehouse';
            receiverObjectId=1;
        }
        else if(transferTo=='branch'){
            receiverMode='branch';
            receiverObjectId=$('#transfer_to_branch').val().trim();
            if(receiverObjectId=='' || receiverObjectId==null){
                $('#transfer_to_branch').parent().find('.select-dropdown').addClass('required');
                $("#transfer_to_branch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;

            }
        }
        else{
            receiverMode='warehouse';
            receiverObjectId=1;
        }
        $.each(grnProducts, function (i, item)
        {
            finalProducts.push(i+'||'+item.quantity);
        });
        finalProducts= finalProducts.join(',');
        if(finalProducts == '')//products mapped
        {
            notify('No products added','error', 10);
            flag=1;
        }
        if(flag == 1)
        {
            return false;
        }
        else
        {
            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/BranchGtn/addBranchGtn';
            var params = {userID: userID, requestMode : 'branch',requestObjectId:branchId, receiverMode: receiverMode ,receiverObjectId :receiverObjectId, products : finalProducts};
            var type = "POST";
            var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            var uploadFile = new FormData();
            if (files.length > 0) {
                uploadFile.append("file", files[0]);
                uploadFile.append("image", 'invoice_' + new Date().getTime());
            }
            uploadFile.append('userID', userID);
            uploadFile.append('requestMode', 'branch');
            uploadFile.append('requestObjectId', branchId);
            uploadFile.append('receiverMode', receiverMode);
            uploadFile.append('receiverObjectId', receiverObjectId);
            uploadFile.append('gtnRemarks', gtnRemarks);
            uploadFile.append('gtnInvoice', gtnInvoice);
            uploadFile.append('products', finalProducts);
            $.ajax({
                /*type: 'POST',*/
                type: type,
                url: ajaxurl,
                dataType: "json",
                data: uploadFile,
                contentType: false,
                processData: false,
                headers: headerParams,
                beforeSend: function () {
                    $(This).attr("disabled", "disabled");
                }, success: function (response) {
                    alertify.dismissAll();
                    $(This).removeAttr("disabled");
                    saveBranchGtnResponse(response);
                }, error: function (response) {
                    alertify.dismissAll();
                    notify('Something went wrong. Please try again', 'error', 10);
                }
            });
            //commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: saveBranchGtnResponse});
        }
    }
    function saveBranchGtnResponse(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
            if(response.data.invoiceNumber){
                $("#txtinvoice").addClass("required");
                $("#txtinvoice").after('<span class="required-msg">'+response.data.invoiceNumber+'</span>');
                flag = 1;
            }
        }
        else
        {
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            $('#myBranchGtnModal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildBranchGtnDataTable();
        }
    }
    function getBranchGtnProduct(This,seqNum,newseqNum)
    {
        $('#branchGtnModal').html('GDN No. '+newseqNum);
        $('#gtnProductsBranch').html('');
        var ajaxurl = API_URL + 'index.php/BranchGtn/getBranchGtnProducts';
        var params = {sequenceNumber:seqNum};
        var userID = $('#hdnUserId').val();
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({
            This: this,
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: 'list',
            onSuccess: function (response) {
                var stockhtml = '';
                stockhtml += "<table class=\"table table-responsive table-bordered mb0\">";
                stockhtml += "<thead>";
                stockhtml += "<tr class=\"\">";
                stockhtml += "<th>Product Name</th>";
                stockhtml += "<th>Quantity</th>";
                stockhtml += "</tr>";
                stockhtml += "</thead>";
                stockhtml += "<tbody>";
                if (response.status === true && response.data.length > 0) {
                    $.each(response.data, function (key, value) {
                        stockhtml += "<tr class=\"\">";
                        stockhtml += "<td>" + value.productName + "</td>";
                        stockhtml += "<td>" + value.quantity + "</td>";
                        stockhtml += "</tr>";
                    });
                }
                else {
                    stockhtml = "<tr colspan='2'>No records found</tr>";
                }
                stockhtml += "</tbody>";
                stockhtml += "</table>";
                $('#gtnProductsBranch').html(stockhtml);
            }
        });
    }

</script>

