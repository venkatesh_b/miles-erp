<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnBranchVisitId" value="0" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Branch Visit</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="left" data-tooltip="Add Branch Visit" class="viewsidePanel" onclick="ManageBranchVisit(this,0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0 branch-visit-scroll" style="">
                <table class="table table-responsive table-striped table-custom min-height-td" id="leads-list">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Course</th>
                        <th>Level </th>
                        <th class="no-sort">Info</th>
                        <th class="text-center">First Level Comments</th>
                        <th class="text-center">Second Level Comments</th>
                        <th class="no-sort"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="branch-visit-list-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageBranchVisitForm" method="post">
                        <div class="modal-header">
                            <button onclick="closeBranchVisit()" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Branch Visit</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-10">
                                <div class="multiple-radio">
                                    <span class="inline-radio">
                                        <input id="rdoLead" class="with-gap" type="radio" name="contactType" value="lead" checked />
                                        <label for="rdoLead">Lead</label>
                                    </span>
                                    <span class="inline-radio" >
                                        <input id="rdoContact" class="with-gap" type="radio" name="contactType" value="contact"  />
                                        <label for="rdoContact">Contact</label>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-10">
                                <div class="input-field">
                                    <input id="txtBranchVisitorLead" type="text" class="validate">
                                    <label for="txtBranchVisitorLead">Mobile number <em>*</em></label>
                                </div>
                                <div class="input-field hidden">
                                    <input id="txtBranchVisitorContact" type="text" class="validate">
                                    <label for="txtBranchVisitorContact">Mobile number <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-2 p0 mt2">
                                <div class="input-field">
                                    <button type="button" class="btn blue-btn" id="btnLeadSnapshot" onclick="getLeadBranchVisit(this)">Go</button>
                                </div>
                            </div>
                            <div id="BranchVisitscreen"></div>
                        </div>
                        <div class="modal-footer" id="btnWrapper">
                            <button onclick="saveBranchVisit(this)" id="btnSaveBranchVisit" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Save</button>
                            <button onclick="closeBranchVisit()" data-dismiss="modal" class="btn blue-light-btn" type="button"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
var $counter = 0;
docReady(function(){
    buildbranchVisitListDataTable();
    $("#txtBranchVisitorLead").keydown(function(e)
    {
        if(e.which === 13)
        {
            e.preventDefault();
            return false;
        }
    });
    getBranchVisitorLeadInfo();
});

$('input[type=radio][name=contactType]').change(function() {
    $('#BranchVisitscreen').html('');
    if($(this).val() == 'contact')
    {
        $('input#txtBranchVisitorLead').parent("div.input-field").addClass("hidden");
        $('input#txtBranchVisitorContact').parent("div.input-field").removeClass("hidden");
        $('input#txtBranchVisitorContact').val('');
        if($counter==0)
            getBranchVisitorContactInfo();
        $counter++;
    }
    else
    {
        $('input#txtBranchVisitorContact').parent("div.input-field").addClass("hidden");
        $('input#txtBranchVisitorLead').parent("div.input-field").removeClass("hidden");
        $('input#txtBranchVisitorLead').val('');
    }

    $('#ManageBranchVisitForm label').removeClass("active");
});

$('#userBranchList').change(function(){
    changeDefaultBranch(this);
    buildbranchVisitListDataTable();
});
function buildbranchVisitListDataTable()
{
    var ajaxurl=API_URL+'index.php/BranchVisit/getLeadsList';
    var userID=$('#hdnUserId').val();
    var branchId=$('#userBranchList').val();
    var params={branchId:branchId};
    var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
    $("#leads-list").dataTable().fnDestroy();
    $tableobj= $('#leads-list').DataTable( {
        "fnDrawCallback": function()
        {
            buildpopover();
            verifyAccess();
            var $api = this.api();
            var pages = $api.page.info().pages;
            if(pages > 1)
            {
                $('.dataTables_paginate').css("display", "block");
                $('.dataTables_length').css("display", "block");
            }
            else
            {
                $('.dataTables_paginate').css("display", "none");
                $('.dataTables_length').css("display", "none");
            }
        },
        dom: "Bfrtip",
        bInfo: false,
        "serverSide": true,
        "bProcessing": true,
        "oLanguage":
        {
            "sSearch": "<span class='icon-search f16'></span>",
            "sEmptyTable": "No leads found",
            "sZeroRecords": "No leads found",
            "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
        },
        ajax:
        {
            url:ajaxurl,
            type:'GET',
            data:params,
            headers:headerParams,
            error:function(response)
            {
                DTResponseerror(response);
            }
        },
        "columnDefs": [
            {
                "targets": 'no-sort',
                "orderable": false
            }
        ],
        "order": [[ 0, "desc" ]],
        columns:
            [
                {
                    data: null, render: function ( data, type, row )
                {
                    var leadName="";
                    leadName+='<div class="img-wrapper img-wrapper-leadinfo light-blue3-bg" style="width:70px;height:63px;margin-left:-8px; position:absolute;top:50%;margin-top:-32px"><a class="tooltipped" data-position="top" data-tooltip="Click here for lead info" href="'+BASE_URL+'app/leadinfo/'+data.branch_xref_lead_id_encode+'/'+$context+'~'+$pageUrl+'~'+$pageUrl+'"><img src="<?php echo BASE_URL; ?>assets/images/male-icon.jpg" style="width:33px;height:33px"><span class="img-text" style="word-break:break-word">'+data.lead_number+'</span></a></div>';
                    leadName+='<div style="padding-left:85px"><div class="f14 font-bold">';
                    leadName+= data.lead_name;
                    leadName+='</div>';
                    leadName+='</div>';
                    return leadName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.course_name;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.level;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var Qualificatin_name = (data.qualification == null || data.qualification == '') ? "" :'<lable class="clabel icon-label"><a href="javascript:;" class="dark-sky-blue tooltipped" data-position="top" data-tooltip="Qualification"><i class="icon-qualification ml5 mt2"></i></a></lable>'+data.qualification;
                    var info="";
                    info+='<p><lable class="clabel icon-label"><a href="javascript:;" class="dark-sky-blue tooltipped" data-position="top" data-tooltip="Email"><i class="icon-mail ml5 mt2"></i></a></lable>'+data.email+'</p>';
                    info+='<p><lable class="clabel icon-label"><a href="javascript:;" class="dark-sky-blue tooltipped" data-position="top" data-tooltip="Phone"><i class="icon-phone ml5 mt2"></i></a></lable>'+data.phone+'</p>';
                    info+= '<p> '+Qualificatin_name+'</p>';
                    return info;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var branch_comments='';
                    branch_comments+="<p>"+data.branch_visit_comments+"</p>";
                    branch_comments+="<p>"+data.created_date+" By ";
                    branch_comments+=""+data.branch_vist_name+"</p>";
                    return branch_comments;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var counselor_comments='';
                    counselor_comments+="<p>"+data.second_level_counsellor_comments+"</p>";
                    if(data.is_counsellor_visited==1){
                        counselor_comments+="<p>"+data.second_level_counsellor_updated_date+"";
                    }
                    else{
                        counselor_comments+="<p>"+data.created_date+"";
                    }
                    counselor_comments+=" By "+data.counsellor_name+"</p>";
                    return counselor_comments;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var action='';
                    action+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                    action+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                    action+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    action+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    action+='<li><a access-element="edit" href="javascript:;" onclick="getBranchVisitDetails(this,\''+data.branch_visitor_id+'\')">Edit</a></li>';
                    action+='<li><a access-element="delete" href="javascript:;" onclick="removeBranchVisitConfirm(this,\''+data.branch_visitor_id+'\')">Delete</a></li>';
                    action+='</ul></div>';
                    return action;
                }
                }
            ]
    });
    DTSearchOnKeyPressEnter();
}

function removeBranchVisitConfirm(This,branchVisitId){
    customConfirmAlert('Branch Visit Status', 'Are you sure want to delete this branch visit ?');
    $("#popup_confirm #btnTrue").attr("onclick","removeBranchVisit(this,"+branchVisitId+")");
}
function removeBranchVisit(This,branchVisitId){
    var UserId = $('#hdnUserId').val();
    var branchId=$('#userBranchList').val();
    var ajaxurl = API_URL + 'index.php/BranchVisit/deleteBranchVisit';
    var type = "POST";
    var action = 'delete';
    var params={branchId:branchId,branchVisitId:branchVisitId};
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
    commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, params:params,headerParams: headerParams, action: action, onSuccess: function(response){
        if (response == -1 || response['status'] == false)
        {
            alertify.dismissAll();
            notify(response.message, 'error', 10);
        }
        else{
            buildbranchVisitListDataTable();
            alertify.dismissAll();
            notify(response.message, 'success', 10);
        }
    }
    });
    $('#popup_confirm').modal('hide');

}

function getLeadBranchVisit()
{
    $('#ManageBranchVisitForm span.required-msg').remove();
    $('#ManageBranchVisitForm input').removeClass("required");

    var contactType = $('input[name=contactType]:checked').val();

    var txtBranchVisit = '';/*$('#txtBranchVisit').val();*/
    if(contactType == 'contact'){
        txtBranchVisit = $('input#txtBranchVisitorContact').val();
    }else{
        txtBranchVisit = $('input#txtBranchVisitorLead').val();
    }
    var error = false;
    if (txtBranchVisit == "")
    {
        $("#txtBranchVisit").addClass('required');
        $("#txtBranchVisit").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        error = true;
    }
    else if(!(/^[0-9]+$/.test(txtBranchVisit)))
    {
        $("#txtBranchVisit").addClass("required");
        $("#txtBranchVisit").after('<span class="required-msg">Must contain only Numbers</span>');
        error = true;
    }

    if(!error)
    {
        notify('Processing..', 'warning', 10);
        var UserId = $('#hdnUserId').val();
        var branchId=$('#userBranchList').val();
        var hdnBranchVisitId=$('#hdnBranchVisitId').val();
        var contactType = $('input[name=contactType]:checked').val();
        var params={};

        if(contactType == 'contact'){
            var ajaxurl = API_URL + 'index.php/BranchVisit/getContactSnapshot/lead_id/';
            var params={lead_id:txtBranchVisit,branch_id:branchId,hdnBranchVisitId:hdnBranchVisitId,contactType:contactType};
        }else{
            var ajaxurl = API_URL + 'index.php/BranchVisit/getLeadSpanshot/';
            var params={lead_id:txtBranchVisit,branch_id:branchId,hdnBranchVisitId:hdnBranchVisitId,contactType:contactType};
        }

        var datatype = "GET";
        $action = 'list';
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
        commonAjaxCall({This: this, method: datatype, requestUrl: ajaxurl, headerParams: headerParams, params:params, action: $action, onSuccess: function(response){
            alertify.dismissAll();
            if (response == -1 || response['status'] == false)
            {
                $("#txtBranchVisit").addClass("required");
                $("#txtBranchVisit").after('<span class="required-msg">'+response.message+'</span>');
                $('#BranchVisitscreen').html('');
                notify(response.message, 'error', 10);
            }
            else {
                var rawData = response.data;

                if(contactType=='contact')
                {
                    var content = '<div class="col-sm-12">';
                    content += '<div class="light-blue3-bg clearfix pt10 pb10">';
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Name</span>';
                    content += '<p>' + rawData.name + '</p>';
                    content += '</div>';
                    content += '</div>';

                    content += '<input type="hidden" id="currentStage" value="' + rawData.lead_stage + '"/>';

                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Email</span>';
                    content += '<p class="ellipsis" title="'+rawData.email+'">' + rawData.email + '</p>';
                    content += '</div>';
                    content += '</div>';

                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Phone</span>';
                    content += '<p>' + rawData.phone + '</p>';
                    content += '</div>';
                    content += ' </div>';
                    content += '</div>';
                    content += '</div>';

                    content += '<div class="col-sm-12 mt10">';
                    content += '<div class="input-field">';
                    content += '<select id="txtCourse" name="txtCourse" class="validate" class="formSubmit">';
                    content += '<option value="" selected>--Select--</option>';
                    content += '</select>';
                    content += '<label class="select-label">Course <em>*</em></label>';
                    content += '</div>';
                    content += '</div>';

                    content += '<div class="col-sm-12">';
                    content += "<div class=\"input-field mt10\">";
                    content += "<textarea id=\"txtBranchVisitComments\" name=\"txtBranchVisitComments\" class=\"materialize-textarea\"></textarea>";
                    content += "<label for=\"txtBranchVisitComments\">Comments <em>*</em></label>";
                    content += "</div>";
                    content += "</div>";

                    content += '<div class="col-sm-12">';
                    content += '<div class="input-field">';
                    content += '<input type="date" class="datepicker relative enablePast7Dates" placeholder="dd/mm/yyyy" id="visitedDate">';
                    content += '<label class="datepicker-label">Visited Date <em>*</em></label>';
                    content += '</div>';
                    content += '</div>';

                    content += '<div class="col-sm-12">';
                    content += '<div class="input-field">';
                    content += '<input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="txtContactCallStatusDatePicker">';
                    content += '<label class="datepicker-label">Expected Enrollment Date <em>*</em></label>';
                    content += '</div>';
                    content += '</div>';

                    content += '<div class="col-sm-12"><label>Counselor</label></div>';
                    content += '<div class="col-sm-12">';
                    content += '<div class="input-field">';
                    content += '<select id="txtsecondaryUser" name="txtsecondaryUser" class="validate" class="formSubmit">';
                    content += '<option value="" selected>--Select--</option>';
                    content += '</select>';
                    content += '<label class="select-label">Secondary Counselor <em>*</em></label>';
                    content += '</div>';
                    content += '</div>';
                    content += '</div>';

                    $('#BranchVisitscreen').html(content);
                    $('#btnWrapper').show();

                    $('#visitedDate').attr('disabled',false);

                    enableDatePicker();
                    var today = new Date();
                    if($('#hdnBranchVisitId').val()>0) {
                        var today = new Date(rawData.visited_date);
                    }

                    var dd = today.getDate();
                    var mm = today.getMonth()+1; //January is 0!
                    var yyyy = today.getFullYear();

                    if(dd<10) {
                        dd='0'+dd
                    }

                    if(mm<10) {
                        mm='0'+mm
                    }
                    today = yyyy+'/'+mm+'/'+dd;
                    if($('#visitedDate').length>0) {

                        setDatePicker('#visitedDate', today);
                    }

                    getCourses();

                    ajaxurl = API_URL + 'index.php/User/getSecondaryCounselorList/branch_id/'+branchId;
                    commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: $action, onSuccess: function(response){
                        if (response == -1 || response['status'] == false || response.data.length <= 0) {
                            alertify.dismissAll();
                            if(Object.size(response.data) > 0){
                                $('#btnWrapper').hide();
                                $('#txtsecondaryUser').find('option:gt(0)').remove();
                                $('#txtsecondaryUser').material_select();
                                notify(response.message, 'error', 10);
                            }else{
                                $('#txtsecondaryUser').find('option:gt(0)').remove();
                                $('#txtsecondaryUser').material_select();
                                notify(response.message, 'error', 10);
                            }
                        } else {
                            alertify.dismissAll();
                            $('#txtsecondaryUser').find('option:gt(0)').remove();
                            var userRawData = response.data;
                            for (var b = 0; b < userRawData.length; b++) {
                                var o = $('<option/>', {value: userRawData[b]['user_id']})
                                    .text(userRawData[b]['name']);
                                o.appendTo('#txtsecondaryUser');
                            }
                            $("#txtsecondaryUser option:contains('<?php echo $userData['Name']; ?>')").remove();

                            if($("#txtsecondaryUser > option").length == 1){
                                notify('No Counselor Found', 'error', 10);
                            }
                            if(hdnBranchVisitId>0) {
                                $('#txtsecondaryUser').val(rawData.second_level_counsellor_id);
                            }
                            $('#txtsecondaryUser').material_select();
                        }
                    }});

                }
                else if(contactType=='lead')
                {
                    var content = '<div class="col-sm-12">';
                    content += '<div class="light-blue3-bg clearfix pt10 pb10">';
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Name</span>';
                    content += '<p>'+rawData.name+'</p>';
                    content += '</div>';
                    content += '</div>';
                    content += '<input type="hidden" id="currentStage" value="'+rawData.lead_stage+'"/>';
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Email</span>';
                    content += '<p class="ellipsis" title="'+rawData.email+'">'+rawData.email+'</p>';
                    content += '</div>';
                    content += '</div>';
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Phone</span>';
                    content += '<p>'+rawData.phone+'</p>';
                    content += '</div>';
                    content += ' </div>';
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Branch</span>';
                    content += '<p>'+rawData.branch_name+'</p>';
                    content += '</div>';
                    content += '</div>';
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Course</span>';
                    content += '<p>'+rawData.course_name+'</p>';
                    content += '</div>';
                    content += '</div>';


                    if(typeof rawData.expected_visit_date !== 'undefined' && rawData.expected_visit_date !== null)
                    {
                        content += '<div class="col-sm-6 mb5">';
                        content += '<div class="input-field data-head-name">';
                        content += '<span class="display-block ash">Expected Visited Date</span>';
                        content += '<p>'+rawData.expected_visit_date+'</p>';
                        content += '</div>';
                        content += '</div>';
                    }
                    var counsellor = rawData.counsellor;
                    if(($('#hdnBranchVisitId').val()!=0 && rawData.is_counsellor_visited==1))
                    {
                        content += '<div class="col-sm-6 mb5">';
                        content += '<div class="input-field data-head-name">';
                        content += '<span class="display-block ash">Allotted Counsellor Name</span>';
                        content += '<p>'+rawData.counsellor+'</p>';
                        content += '</div>';
                        content += '</div>';
                    }
                    if(($('#hdnBranchVisitId').val()!=0 && rawData.is_counsellor_visited==1))
                    {
                        content += '<div class="col-sm-6 mb5">';
                        content += '<div class="input-field data-head-name">';
                        content += '<span class="display-block ash">Counsellor Updated Date</span>';
                        content += '<p>'+rawData.second_level_counsellor_updated_date+'</p>';
                        content += '</div>';
                        content += '</div>';
                    }
                    if(($('#hdnBranchVisitId').val()!=0 && rawData.is_counsellor_visited==1))
                    {
                        content += '<div class="col-sm-12 mb5">';
                        content += '<div class="input-field data-head-name">';
                        content += '<span class="display-block ash">Counsellor Comment</span>';
                        content += '<p>'+rawData.second_level_counsellor_comments+'</p>';
                        content += '</div>';
                        content += '</div>';
                    }
                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">Current Stage</span>';
                    content += '<p>'+rawData.lead_stage+'</p>';
                    content += '</div>';
                    content += '</div>';

                    var date_label='';
                    var date_text='';
                    if(rawData.lead_stage=='M5')
                    {
                        date_label="Expected Enrollment Date";
                        var date_text=rawData.expected_enroll_date;
                    }
                    else if(rawData.lead_stage=='M6')
                    {
                        date_label="Expected Admission Date";
                        date_text=rawData.expected_admission_date;
                    }
                    else
                    {
                        date_label="Next Followup Date";
                        date_text=rawData.next_followup_date;
                    }

                    content += '<div class="col-sm-6 mb5">';
                    content += '<div class="input-field data-head-name">';
                    content += '<span class="display-block ash">'+date_label+'</span>';
                    content += '<p>'+date_text+'</p>';
                    content += '</div>';
                    content += '</div>';
                    content += '</div>';
                    content += '</div><div id="DataMessagePayment" class="col-sm-12 mt15 text-center"></div>';

                    if(rawData.lead_stage!='M7' && ($('#hdnBranchVisitId').val()==0 || ($('#hdnBranchVisitId').val()!=0 && rawData.is_counsellor_visited==0)))
                    {
                        content +="<div class=\"col-sm-12\" id=\"commentTypeWrapper\" style=\"display: block\">";

                        content +="<div id=\"CommonCallStatusInformation\">";
                        content +="<div class=\"input-field mt10\">";
                        content +="<select id=\"CallAnswerTypes\" onchange=\"getCallAnswerTypes(this)\">";

                        content +="</select>";
                        content +="<label for=\"CallAnswerTypes\" class=\"select-label\">Answer</label>";
                        content +="</div>";
                        content +="</div>";

                        content +="<div id=\"\">";
                        content +="<div class=\"input-field  mt0\" >";
                        content +="<input type='date' class='datepicker relative enableFutureDates' placeholder='dd/mm/yyyy' id='txtCommonCallStatusDatePicker'>";
                        content +="<label for =\"txtCommonCallStatusDatePicker\" class=\"datepicker-label\">Next Followup Date <em>*</em></label>";
                        content +="</div>";
                        content +="</div>";
                        content +="<div class=\"input-field mt10\">";
                        content +="<textarea id=\"txtBranchVisitComments\" name=\"txtBranchVisitComments\" class=\"materialize-textarea\"></textarea>";
                        content +="<label for=\"txtBranchVisitComments\">Comments <em>*</em></label>";
                        content +="</div>";
                        content +="</div>";
                        content +="</div>";
                        content += '<div class="col-sm-12">';
                        content += '<div class="input-field">';
                        content += '<input type="date" class="datepicker relative enablePast7Dates" placeholder="dd/mm/yyyy" id="visitedDate">';
                        content += '<label class="datepicker-label">Visited Date <em>*</em></label>';
                        content += '</div>';
                        content += '</div>';
                        content += '<div class="col-sm-12"><label>Counselor</label></div>';
                        content += '<div class="col-sm-12">';
                        content += '<div class="input-field">';
                        content += '<select id="txtsecondaryUser" name="txtsecondaryUser" class="validate" class="formSubmit">';
                        content += '<option value="" selected>--Select--</option>';
                        content += '</select>';
                        content += '<label class="select-label">Secondary Counselor <em>*</em></label>';
                        content += '</div>';
                        content += '</div>';
                        content += '</div>';
                    }

                    if(rawData.lead_stage!='M7' && ($('#hdnBranchVisitId').val()==0 || ($('#hdnBranchVisitId').val()!=0 && rawData.is_counsellor_visited==0))){
                        $('#btnWrapper').show();
                    }else{
                        $('#btnWrapper').hide();
                    }

                    $('#BranchVisitscreen').html(content);
                    $("#DataMessagePayment").hide();
                    $("#DataMessagePayment").html('');
                    if(rawData.lead_stage=='M7')
                    {
                        $("#DataMessagePayment").show();
                        $("#DataMessagePayment").html("<b style='color:red;'>Already Converted to Student</b>");
                    }
                    $('#CallAnswerTypes,#ddlCalltype').material_select();

                    branchVisitleadStatuses(this,rawData.lead_stage);

                    if($('#hdnBranchVisitId').val()==0)
                    {
                        $('#txtBranchVisit').attr('disabled',false);
                        $('#CommonCallStatusInformation').show();
                        $('#CommonCallStatusDatePicker').show();
                        $('#CallAnswerTypes').attr('disabled',false);
                        $('#txtCommonCallStatusDatePicker').attr('disabled',false);
                        $('#visitedDate').attr('disabled',false);
                        $('#btnLeadSnapshot').show();
                    }
                    else
                    {
                        $('#txtBranchVisitComments').val(rawData.branch_visit_comments);
                        $("#txtBranchVisitComments").next('label').addClass("active");
                        //rawData.comment_type='Interested Next Batch';
                        if(rawData.comment_type!=null && rawData.comment_type!='')
                        {
                            $('#btnLeadSnapshot').hide();
                            $('#txtBranchVisit').attr('disabled',true);

                            $('#CommonCallStatusInformation').hide();
                            $('#CommonCallStatusDatePicker').hide();
                            $('#CallAnswerTypes').attr('disabled',true);
                            $('#txtCommonCallStatusDatePicker').attr('disabled',true);
                            $('#visitedDate').attr('disabled',true);
                        }
                    }
                    var today = new Date();
                    if($('#hdnBranchVisitId').val()>0) {
                        var today = new Date(rawData.visited_date);
                    }

                    var dd = today.getDate();
                    var mm = today.getMonth()+1; //January is 0!
                    var yyyy = today.getFullYear();

                    if(dd<10) {
                        dd='0'+dd
                    }

                    if(mm<10) {
                        mm='0'+mm
                    }
                    today = yyyy+'/'+mm+'/'+dd;
                    if($('#visitedDate').length>0)
                    {
                        setDatePicker('#visitedDate', today);
                    }

                    if(rawData.next_followup_date)
                    {
                        var next_followup_date = new Date(rawData.next_followup_date);
                        var dd = next_followup_date.getDate();
                        var mm = next_followup_date.getMonth()+1; //January is 0!
                        var yyyy = next_followup_date.getFullYear();
                        if(dd<10)
                        {
                            dd='0'+dd
                        }
                        if(mm<10)
                        {
                            mm='0'+mm
                        }
                        next_followup_date = yyyy+'/'+mm+'/'+dd;
                        setTimeout(function(){ setDatePicker('#txtCommonCallStatusDatePicker', next_followup_date); }, 50);
                    }
                    var type = 'GET';
                    ajaxurl = API_URL + 'index.php/User/getSecondaryCounselorList/branch_id/'+branchId;
                    commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: $action, onSuccess: function(response){
                        if (response == -1 || response['status'] == false || response.data.length <= 0) {
                            alertify.dismissAll();
                            if(Object.size(response.data) > 0){
                                $('#btnWrapper').hide();
                                $('#txtsecondaryUser').find('option:gt(0)').remove();
                                $('#txtsecondaryUser').material_select();
                                notify(response.message, 'error', 10);
                            }else{
                                $('#txtsecondaryUser').find('option:gt(0)').remove();
                                $('#txtsecondaryUser').material_select();
                                notify(response.message, 'error', 10);
                            }
                        } else {
                            alertify.dismissAll();
                            $('#txtsecondaryUser').find('option:gt(0)').remove();
                            var userRawData = response.data;
                            for (var b = 0; b < userRawData.length; b++) {
                                var o = $('<option/>', {value: userRawData[b]['user_id']})
                                    .text(userRawData[b]['name']);
                                o.appendTo('#txtsecondaryUser');
                            }
                            $("#txtsecondaryUser option:contains('<?php echo $userData['Name']; ?>')").remove();

                            if($("#txtsecondaryUser > option").length == 1){
                                notify('No Counselor Found', 'error', 10);
                            }
                            if(hdnBranchVisitId>0) {
                                $('#txtsecondaryUser').val(rawData.second_level_counsellor_id);
                            }
                            $('#txtsecondaryUser').material_select();
                        }
                    }});
                }
            }
        }});
    }
}

function getBranchVisitDetails(This,branchVisitId)
{
    var UserId = $('#hdnUserId').val();
    var branchId=$('#userBranchList').val();
    var ajaxurl = API_URL + 'index.php/BranchVisit/getBranchVisitDetails';
    var type = "GET";
    var action = 'list';
    $('#hdnBranchVisitId').val(0);
    var params={branchId:branchId,branchVisitId:branchVisitId};
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
    $.when(
            commonAjaxCall(
                {
                    This: this, method: type, requestUrl: ajaxurl, params:params,headerParams: headerParams, action: action
                }
            )
        ).then(function (response1){
            if (response1 == -1 || response1['status'] == false)
            {
                alertify.dismissAll();
                notify(response1.message, 'error', 10);
            }
            else{

                ManageBranchVisit(this,'');
                $('#hdnBranchVisitId').val(branchVisitId);

                $('#txtBranchVisitorLead').val(response1.data[0].lead_number);
                Materialize.updateTextFields();
                getLeadBranchVisit(this);

            }
        });
}

function getBranchVisitorLeadInfo()
{
    var type = $('input[name=contactType]:checked').val();
    var ajaxUrl = API_URL + 'index.php/FeeAssign/getLeadsInformation';
    var headerParams =
    {
        action: 'list',
        context: $context,
        serviceurl: $pageUrl,
        pageurl: $pageUrl,
        Authorizationtoken: $accessToken,
        user: '<?php echo $userData['userId']; ?>'
    };
    $('input#txtBranchVisitorLead').jsonSuggest
    ({
        onSelect:function(item)
        {
            $('input#txtBranchVisitorLead').val(item.id);
            $("#btnLeadSnapshot").click();
        },
        headers:headerParams,
        url:ajaxUrl,
        minCharacters: 2
    });
}

function getBranchVisitorContactInfo()
{
    var type = $('input[name=contactType]:checked').val();
    var ajaxUrl = API_URL + 'index.php/BranchVisit/getBranchVisitorContactInformation';
    var headerParams =
    {
        action: 'list',
        context: $context,
        serviceurl: $pageUrl,
        pageurl: $pageUrl,
        Authorizationtoken: $accessToken,
        user: '<?php echo $userData['userId']; ?>'
    };
    $('input#txtBranchVisitorContact').jsonSuggest
    ({
        onSelect:function(item)
        {
            $('input#txtBranchVisitorContact').val(item.id);
            $("#btnLeadSnapshot").click();
        },
        headers:headerParams,
        url:ajaxUrl,
        minCharacters: 2
    });
}

function getCourses()
{
    var userID = $('#hdnUserId').val();
    var ajaxurl = API_URL + 'index.php/Courses/getAllCourseList';
    var action = 'add';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    var response =
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: getCoursesResponse});
}
function getCoursesResponse(response)
{
    $('#txtCourse').empty();
    $('#txtCourse').append($('<option disabled selected></option>').val('').html('--Select--'));
    if (response.status == true) {

        if (response.data.length > 0) {

            $.each(response.data, function (key, value) {

                $('#txtCourse').append($('<option></option>').val(value.courseId).html(value.name));
            });
        }
    }
    $('#txtCourse').material_select();
}
function ManageBranchVisit(This, id){
    $('#ManageBranchVisitForm span.required-msg').remove();
    $('#ManageBranchVisitForm input,#ManageBranchVisitForm .select-dropdown').removeClass("required");
    $('input:radio[name=contactType]').val(['lead']);
    $('input#txtBranchVisitorContact').parent("div.input-field").addClass("hidden");
    $('input#txtBranchVisitorLead').parent("div.input-field").removeClass("hidden");
    $('input#txtBranchVisitorLead').val('');
    $('#ManageBranchVisitForm label').removeClass("active");
    $('#txtBranchVisit').attr('disabled',false);
    $('#btnLeadSnapshot').show();
    $('#BranchVisitscreen').html('');
    $('#hdnBranchVisitId').val(0);
    $('#txtBranchVisit').val('');
    $('#btnWrapper').hide();
    $('#branch-visit-list-modal').modal('show');
}

function closeBranchVisit(){
    $('#ManageBranchVisitForm span.required-msg').remove();
    $('#ManageBranchVisitForm input,#ManageBranchVisitForm .select-dropdown').removeClass("required");
    $('#ManageBranchVisitForm label').removeClass("active");
    $('#BranchVisitscreen').html('');
    $('#txtBranchVisit').val('');
    $('#btnWrapper').hide();
    $('#branch-visit-list-modal').modal('toggle');
    $('input#txtBranchVisitorContact').val('');
    $('input#txtBranchVisitorLead').val('');
    //$('input[name=contactType]:checked').val('lead')
    //.prop('checked',true);
    $('input:radio[name=contactType]').val(['lead']);
}

function saveBranchVisit(This){
    $('#ManageBranchVisitForm span.required-msg').remove();
    $('#ManageBranchVisitForm input,#ManageBranchVisitForm .select-dropdown, .materialize-textarea').removeClass("required");

    var typetext = $('input[name=contactType]:checked').val();

    if(typetext == 'contact')
    {
        var txtBranchVisit = $('#txtBranchVisitorContact').val();
        var txtsecondaryUser = $('#txtsecondaryUser').val();
        var txtCourse = $('#txtCourse').val();
        var txtBranch = $('#userBranchList').val();
        var visitedDate = $('#visitedDate').val();
        var branchVisitComments=$('#txtBranchVisitComments').val();
        var date = $("#txtContactCallStatusDatePicker").val();

        if(branchVisitComments==''){
            $("#txtBranchVisitComments").addClass("required");
            $("#txtBranchVisitComments").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        }
        //visitedDate
        if(visitedDate==''){
            $("#visitedDate").addClass("required");
            $("#visitedDate").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        }
        if(date==''){
            $("#txtContactCallStatusDatePicker").addClass("required");
            $("#txtContactCallStatusDatePicker").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        }
        if (txtsecondaryUser === null || txtsecondaryUser == '') {
            $("#txtsecondaryUser").parent().find('.select-dropdown').addClass("required");
            $("#txtsecondaryUser").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            error = true;
        }
        if (txtCourse === null || txtCourse == '') {
            $("#txtCourse").parent().find('.select-dropdown').addClass("required");
            $("#txtCourse").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            error = true;
        }
    }
    else
    {
        var txtBranchVisit = $('#txtBranchVisitorLead').val();
        var txtsecondaryUser = $('#txtsecondaryUser').val();
        var txtCourse = $('#txtCourse').val();
        var txtBranch = $('#userBranchList').val();
        var visitedDate = $('#visitedDate').val();
        var branchVisitComments=$('#txtBranchVisitComments').val();
        var CommentType=$('option:selected', $('#CallAnswerTypes')).text();
        var callAnswer = $('#CallAnswerTypes').val().trim();
        var currentLeadStage=$('#currentStage').val().trim();
        //var callType = $("#ddlCalltype").val();
        var dateType='';
        var lead_stage='';
        var date='';
        var error = false;
        var hdnBranchVisitId=$('#hdnBranchVisitId').val();

        if (txtBranchVisit == "") {
            $("#txtBranchVisit").addClass('required');
            $("#txtBranchVisit").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        } else if(!(/^[0-9]+$/.test(txtBranchVisit))){
            $("#txtBranchVisit").addClass("required");
            $("#txtBranchVisit").after('<span class="required-msg">Must contain only Numbers</span>');
            error = true;
        }

        dateType=$('option:selected', $('#CallAnswerTypes')).attr('data-date');
        if(hdnBranchVisitId==0 && callAnswer==""){
            $("#CallAnswerTypes").parent().find('.select-dropdown').addClass("required");
            $("#CallAnswerTypes").parent().after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        }
        else {
            lead_stage = callAnswer;
            date = $("#txtCommonCallStatusDatePicker").val();
            if(hdnBranchVisitId==0 && date==''){
                $("#txtCommonCallStatusDatePicker").addClass("required");
                $("#txtCommonCallStatusDatePicker").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                error = true;
            }
        }

        if(branchVisitComments==''){
            $("#txtBranchVisitComments").addClass("required");
            $("#txtBranchVisitComments").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        }
        if(visitedDate==''){
            $("#visitedDate").addClass("required");
            $("#visitedDate").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        }
        if (txtsecondaryUser === null || txtsecondaryUser == '') {
            $("#txtsecondaryUser").parent().find('.select-dropdown').addClass("required");
            $("#txtsecondaryUser").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            error = true;
        }
    }

    if(!error){
        notify('Processing..', 'warning', 10);
        var UserId = $('#hdnUserId').val();

        if(typetext == 'contact'){
            var ajaxurl = API_URL + 'index.php/BranchVisit/saveContactBranchVisit';
            var params = {'lead_id': txtBranchVisit, 'user': txtsecondaryUser, 'branch': txtBranch, 'visitedDate': visitedDate, common_date:date, branchVisitComments:branchVisitComments,course: txtCourse, type: typetext};
        }else{
            var ajaxurl = API_URL + 'index.php/BranchVisit/saveBranchVisit';
            var params = {'lead_id': txtBranchVisit, 'user': txtsecondaryUser, 'branch': txtBranch, 'visitedDate': visitedDate,common_date:date,branchVisitComments:branchVisitComments,CommentType:CommentType,hdnBranchVisitId:hdnBranchVisitId,lead_stage:lead_stage,dateType:dateType,currentLeadStage:currentLeadStage, type: typetext};
        }

        var type = "POST";
        if(hdnBranchVisitId>0){
            var action = 'edit';
        }
        else{
            var action = 'add';
        }

        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
        commonAjaxCall({This: this, params:params, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: function(response){
            alertify.dismissAll();
            if (response == -1 || response['status'] == false)
            {
                if(response.data.length){
                    var id = '';
                    notify('Validation Error', 'error', 10);
                    for (var a in response.data) {
                        id = '#' + a;
                        if (a == 'txtsecondaryUser') {
                            $('#txtsecondaryUser').parent().find('.select-dropdown').addClass("required");
                            $('#txtsecondaryUser').parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                        } else if (a == 'txtsecondaryUser') {
                            $('#txttextarea1').addClass("required");
                            $('#txttextarea1').after('<span class="required-msg">' + response.data[a] + '</span>');
                        } else if (a == 'txtsecondaryUser') {
                            $("#txtBranchVisit").addClass("required");
                            $("#txtBranchVisit").after('<span class="required-msg">' + response.data[a] + '</span>');
                        }
                    }
                } else {
                    alertify.dismissAll();
                    notify(response.message, 'error', 10);
                }
            } else {
                notify(response.message, 'success', 10);
                buildbranchVisitListDataTable();
                closeBranchVisit();
            }
        }});
    } else {
        alertify.dismissAll();
        notify('Validation Error', 'error', 10);
    }
}

</script>