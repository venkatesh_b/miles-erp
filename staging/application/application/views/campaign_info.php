<?php
/**
 * Created by PhpStorm.
 * User: Parameshwar.V
 * Date: 04-07-2016
 * Time: 02:19 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnTemplatePath" value="" readonly />

    <div class="content-header-wrap">
        <h3 class="content-header">View Campaign</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="" href="<?php echo APP_REDIRECT;?>campaign"><i class="icon-times" ></i></a></li>
                <li><a></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 mb20 p0">
                <div class="col-sm-9 p0 ">
                    <div class="col-sm-12 white-bg">
                        <div class="col-sm-4 mtb10">
                            <p class=" label-text">Campaign Name</p>
                            <label class="label-data darkblue" id="lblCampaignName">----</label>
                        </div>
                        <div class="col-sm-4 mtb10">
                            <p class=" label-text">Start Date</p>
                            <label class="label-data" id="lblCampaignStartDate">----</label>
                        </div>
                        <div class="col-sm-4 mtb10">
                            <p class=" label-text">Event Status</p>
                            <label class="label-data" id="lblCampaignStatus">----</label>
                        </div>
                        <div class="col-sm-4 mtb10">
                            <p class=" label-text">Description</p>
                            <label class="label-data" id="lblCampaignDesc">----</label>
                        </div>
                    </div>
                    <div class="col-sm-12 p0 mt5">

                        <div class="col-sm-12 p0">
                            <table class="table table-responsive table-striped table-custom" id="campaign-info-list">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone </th>
                                    <th>Status</th>
                                </tr>

                                </thead>

                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 pr0">
                    <div class="col-sm-12 white-bg p0">
                        <h4 class="heading-uppercase mb5 pl10">Report Statistics</h4>
                        <!--<img class="max-width100p mCS_img_loaded" src="<?php /*echo BASE_URL; */?>assets/images/report.png">-->
                        <div id="campaign-info-piechart" style="min-width: 250px; height: 250px; max-width: 600px; margin: 0 auto"></div>
                    </div>
                    <div class="col-sm-12 p0 mt15">
                        <div class="widget-right border border-blue">
                            <div class="widget-header light-dark-blue relative">
                                <h4 class="heading-uppercase f14 m0 p10"> Template Used <a data-position="top" data-tooltip="Click here for template preview"  class="pull-right slide-down f21 tooltipped" href="javascript:;" onclick="showPreviewCampaignTemplate(this)"> <i class="icon-eye"></i> </a> </h4>
                                <!--<img class="max-width100p mCS_img_loaded" src="<?php /*echo BASE_URL; */?>assets/images/template.png">-->
                                <iframe name="ifrm_camp_template" id="ifrm_camp_template" src="" frameborder="0"></iframe>
                            </div>
                        </div>

                    </div>
                </div>
            </div>



        </div>
    </div>
    <!--Modal-->

    <!-- InstanceEndEditable --></div>
<script type="text/javascript">
    $context='Campaign';
    $pageUrl='campaign';
    var campaignId='<?php echo $Id; ?>';
    docReady(function(){
        getCampaignInfo();

    });
    function getCampaignInfo(){
        notify('Processing','warning',100);
        var userID = $('#hdnUserId').val();
        var campaignID = campaignId;
        var ajaxurl = API_URL + 'index.php/Campaign/getCampaignDetails';
        var params = {campaignId:campaignID};
        var headerParams = {context: 'default', Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: function (response){
            if(response.status===true){
                var details=response.data[0];
                if(details.name==null || details.name.trim()=='')
                    details.name='----';
                if(details.start_date_formatted==null || details.start_date_formatted.trim()=='')
                    details.start_date_formatted='----';
                if(details.status==null || details.status.trim()=='')
                    details.status='----';
                if(details.description==null || details.description.trim()=='')
                    details.description='----';
                $('#lblCampaignName').html(details.name.trim());
                $('#lblCampaignStartDate').html(details.start_date_formatted.trim());
                $('#lblCampaignStatus').html(details.status.trim());
                $('#lblCampaignDesc').html(details.description.trim());
                $('#ifrm_camp_template').attr('src',API_URL+details.template_path.trim());
                $('#hdnTemplatePath').val(details.template_path.trim());
                buildCampaignInfoDataTable();

                var ajaxurlPiechart = API_URL + 'index.php/Campaign/getCampaignInfoPiechart';
                commonAjaxCall({This: this, requestUrl: ajaxurlPiechart, params: params, headerParams: headerParams, action: 'add', onSuccess: function (response){
                    getPieChart(response);
                }});

            }
            else{
                alertify.dismissAll();
                notify('Invalid Campaign','error',10);
                setTimeout(function () {
                    window.location.href = APP_REDIRECT + "campaign";
                }, 1000);
                //setTimeout()
            }
        }});
    }
    function buildCampaignInfoDataTable(){
        var ajaxurl=API_URL+'index.php/Campaign/campaignInfoList';
        var campaignID = campaignId;
        var userID=$('#hdnUserId').val();
        var params = {campaignId:campaignID};
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#campaign-info-list").dataTable().fnDestroy();
        $tableobj=$('#campaign-info-list').DataTable( {
            "fnDrawCallback": function()
            {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                }
                else
                {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                }
            },
            "footerCallback": function ( row, data, start, end, display ) {
                $('#campaign-info-list_wrapper div.DTHeader').html('Shortlisted list from the group');
            },
            dom: "<'DTHeader'>Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No data found",
                "sZeroRecords": "No data found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"

            },
            ajax:
            {
                url:ajaxurl,
                type:'GET',
                data:params,
                headers:headerParams,
                error:function(response)
                {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [
                {
                    "targets": 'no-sort',
                    "orderable": false
                }
            ],
            columns:
                [
                    {
                        data: null, render: function ( data, type, row )
                    {
                        if(data.name==null || data.name.trim()=='')
                            data.name='----';
                        return data.name;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        if(data.email==null || data.email.trim()=='')
                            data.email='----';
                        return data.email;
                    }
                    },
                    {
                        data: null,render: function ( data, type, row )
                    {
                        if(data.phone==null || data.phone.trim()=='')
                            data.phone='----';
                        return data.phone;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        if(data.status==null || data.status.trim()=='')
                            data.status='----';
                        return data.status;
                    }
                    }
                ]
        });
        DTSearchOnKeyPressEnter();
        alertify.dismissAll();
    }
    function getPieChart(source) {
        // Build the chart
            if (source.status === true) {
                var pieChartDataSub=[];
                var colorCode='';
                $.each(source.data, function (i, item) {
                    if(item.status=='Pending')
                        colorCode='#6192d3';
                    else if(item.status=='Bounce')
                        colorCode='#ed1c24';
                    else if(item.status=='Processed')
                        colorCode='#FFA500';
                    else if(item.status=='Delivered')
                        colorCode='#00a651';
                    pieChartDataSub.push({name:item.status,y:parseFloat(item.percentage),color:colorCode});
                });
                var pieChartData = {
                    name: 'Contacts',
                    colorByPoint: true,
                    data: pieChartDataSub
                };
                $('#campaign-info-piechart').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: ' '
                    },
                    tooltip: {

                        formatter: function() {
                            var tooltip;
                            tooltip =  '<span><b>' + this.key + '</b></span>: <b>' + this.y + ' %</b><br/>';
                            return tooltip;
                        }
                    },
                    legend: {
                        itemWidth: 100,
                        itemStyle: {
                            fontSize: '10px'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                distance: -30,
                                color: 'white',
                                formatter: function() {
                                    return this.y+' %';
                                },
                                style: {
                                    textShadow: false ,
                                    fontFamily:"arial",
                                    fontSize:'11px'
                                }
                            },
                            showInLegend: true,
                            point:{
                                events : {
                                    legendItemClick: function(e){
                                        e.preventDefault();
                                    }
                                }
                            }
                        }
                    },
                    series: [pieChartData]
                });
             }
    }
    function showPreviewCampaignTemplate(This){

        if($('#hdnTemplatePath').val().trim()!='') {
            var strs=$('#hdnTemplatePath').val().replace(/\//g, "~");
            strs=strs.replace(/\./g, "^");
            var templateURL = APP_REDIRECT + "campaign_template_preview/"+strs;
            window.open(
                templateURL,
                '_blank' // <- This is what makes it open in a new window.
            );
        }
        else{
            alertify.dismissAll();
            notify('Please upload the template','error',10);
        }
    }
</script>

