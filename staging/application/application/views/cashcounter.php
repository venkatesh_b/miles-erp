<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Cash Counter Report</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="report"><a class="" href="javascript:" onclick="javascript:filterCashCounterReportExport(this)"><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Cash Counter / Branch</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="branchFilter"></div>
                    </div>
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Mode of Payment</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="modeofpaymentFilter">
                            <a class="active" data-id="Cash">Cash</a>
                            <a class="active" data-id="DD">DD</a>
                            <a class="active" data-id="Cheque">Cheque</a>
                            <a class="active" data-id="Credit card">Credit card</a>
                        </div>

                    </div>


                </div>
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Company</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="companyFilter"></div>
                    </div>
                    <div class="col-sm-6 reports-filter-wrapper pl0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Receipt Mode</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="receiptmodeFilter">
                            <a class="active" data-id="Offline">Offline</a>
                            <a class="active" data-id="Online">Online</a>
                            <a class="active" data-id="Net Banking">Net Banking</a>
                        </div>

                    </div>

                </div>
            <div class="col-sm-12 p0">
            <div class="col-sm-6 reports-filter-wrapper pl0">
                <div class="multiple-checkbox reports-filters-check">
                    <label class="heading-uppercase">Course</label>
                    <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                </div>
                <div class="boxed-tags" id="courseFilter"></div>
            </div>
            <div class="col-sm-6 reports-filter-wrapper pl0">

                <div class="input-field col-sm-4 mt0 p0">
                    <div class="input-field mt0">
                        <label class="datepicker-label datepicker">From Date </label>
                        <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtFromDate">
                    </div>
                </div>
                <div class="input-field col-sm-4 mt0 pr0">
                    <div class="input-field mt0">
                        <label class="datepicker-label datepicker">To Date </label>
                        <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtToDate">
                    </div>
                </div>

            </div>
            </div>
            <div class="col-sm-6 mt0 pl0">

                <a class="btn blue-btn btn-sm" href="javascript:" onclick="javascript:filterCashCounterReport(this)">Proceed</a>
            </div>
                <!--<div class="col-sm-6 p0 ">

                    <div class="input-field col-sm-4 mt0 p10">
                        <div class="input-field mt0">
                            <label class="datepicker-label datepicker">From Date </label>
                            <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtFromDate">
                        </div>
                    </div>
                    <div class="input-field col-sm-4 mt0 p10">
                        <div class="input-field mt0">
                            <label class="datepicker-label datepicker">To Date </label>
                            <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtToDate">
                        </div>
                    </div>

                </div>-->

            <div id="cashcounter-report-list-wrapper" access-element="report">
                <div class="col-sm-12 clearfix mt15 p0">
                    <table class="table table-inside table-custom branchwise-report-table" id="cashcounter-report-list">
                        <thead>
                        <tr class="">
                            <th>Enroll No.</th>
                            <th>Name</th>
                            <th>Receipt No.</th>
                            <th>Mode of Payment</th>
                            <th>Remark</th>
                            <th class="text-right">Amount</th>
                            <th>Username / Cashier</th>
                        </tr>
                        </thead>

                    </table>
                </div>

            </div>

            <div id="receiptdeletion-report-list-wrapper" access-element="report">
                <div class="col-sm-12 clearfix mt15 p0">
                    <table class="table table-inside table-custom branchwise-report-table" id="receiptdeletion-report-list">
                        <thead>
                        <tr class="">
                            <th>Enroll No.</th>
                            <th>Name</th>
                            <th>Receipt No.</th>
                            <th>Remark</th>
                            <th class="text-right">Amount</th>
                            <th>Username / Cashier</th>
                            <th>Deleted / Reversal By</th>
                            <th>Deleted / Reversal Date</th>
                        </tr>
                        </thead>

                    </table>
                </div>
                <div class="col-sm-12 p0">
                    <div class="bblue-text text-center">
                        <div class="paid-payment">
                            <span class="display-block pl0 text-center">Paid</span>
                            <span class="display-block pl0 text-left font-bold icon-rupee" id="spanPaidAmnt">0</span>
                        </div>
                        <div class="deleted-payment">
                            <span class="display-block pl0 text-center">Delete</span>
                            <span class="display-block pl0 text-left font-bold icon-rupee" id="spanDeletedAmnt">0</span>
                        </div>
                        <div class="overall-payment">
                            <span class="display-block pl0 text-center">Total</span>
                            <span class="display-block font-bold icon-rupee" id="spanTotalAmnt">-</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 clearfix p0">
                    <h4 class="mb30 heading-uppercase">Signatures</h4>
                    <div class="col-sm-4 mb30 pl0 ash">Accountant / Cashier</div>
                    <div class=" col-sm-4 ash">Branch Head</div>
                </div>
            </div>



        </div>

        <!-- InstanceEndEditable --></div>
</div>

<script>
    docReady(function () {
        $("#cashcounter-report-list-wrapper").hide();
        $("#receiptdeletion-report-list-wrapper").hide();
        cashCounterListReportPageLoad();
        enableDatePicker();

    });

</script>

