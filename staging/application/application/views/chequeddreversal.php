<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">cheque / DD / Reversal Report</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="report"><a class="" href="javascript:" onclick="javascript:filterChequeDDReversalReportExport(this)"><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">

                <div class="col-sm-12 p0">
                    <div class="col-sm-6 reports-filter-wrapper p0">
                        <div class="multiple-checkbox reports-filters-check">
                        </div>
                        <div class="col-sm-4 p0">
                            <div class="input-field mt5">
                                <select id="banksWrapper">

                                </select>
                                <label class="select-label">Bank</label>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6 reports-filter-wrapper p0">

                        <div class="input-field col-sm-4 mt0">
                            <div class="input-field mt0">
                                <label class="datepicker-label datepicker">From Date </label>
                                <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtFromDate">
                            </div>
                        </div>
                        <div class="input-field col-sm-4 mt0 ">
                            <div class="input-field mt0">
                                <label class="datepicker-label datepicker">To Date </label>
                                <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtToDate">
                            </div>
                        </div>

                    </div>

                </div>
            <div class="col-sm-12 p0">

                <div class="col-sm-6 reports-filter-wrapper p0">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Course</label>
                        <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                    </div>
                    <div class="boxed-tags" id="courseFilter"></div>
                </div>
                <div class="col-sm-6 reports-filter-wrapper">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Branch</label>
                        <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                    </div>
                    <div class="boxed-tags" id="branchFilter"></div>
                </div>

            </div>
            <div class="col-sm-6 mt0 pl0">
                <a class="btn blue-btn btn-sm" href="javascript:" onclick="javascript:filterChequeDDReversalReport(this)">Proceed</a>
            </div>



            <div id="chequeDDReversal-report-list-wrapper" access-element="report">
                <div class="col-sm-12 clearfix mt15 p0">
                    <table class="table table-inside table-custom branchwise-report-table" id="chequeDDReversal-report-list">
                        <thead>
                        <tr class="">
                            <th>Enroll No.</th>
                            <th>Name</th>
                            <th>Receipt No.</th>
                            <th>Remark</th>
                            <th class="text-right">Amount</th>
                            <th>Username / Cashier</th>
                            <th>Receipt Date</th>
                            <th>Deleted / Reversal By</th>
                            <th>Deleted / Reversal Date</th>
                        </tr>
                        </thead>

                    </table>
                </div>
                <div class="col-sm-12 clearfix p0">
                    <h4 class="mb30 heading-uppercase">Signatures</h4>
                    <div class="col-sm-4 mb30 pl0 ash">Accountant / Cashier</div>
                    <div class=" col-sm-4 ash">Branch Head</div>
                </div>
            </div>
        </div>

        <!-- InstanceEndEditable --></div>
</div>

<script>
    docReady(function () {
        $("#chequeDDReversal-report-list-wrapper").hide();
        chequeDDReversalListReportPageLoad();
        enableDatePicker();
    });
    /* Cheque DD Reversal list report starts here */
    function chequeDDReversalListReportPageLoad()
    {
        var userId = $('#userId').val();
        var action = 'report';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

        var ajaxurl=API_URL+'index.php/ChequeDDReversal/getAllConfigurations';
        var params = {};
        commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
            var dashboard = '';
            if (response.data.length == 0 || response == -1 || response['status'] == false) {
            } else {
                var RawData = response.data;
                var html = '';
                if(RawData.branch.length > 0){
                    for(var a in RawData.branch){
                        html += '<a class="active" data-id="'+RawData.branch[a].id+'" href="javascript:;">'+RawData.branch[a].name+'</a>'
                    }
                    $('#branchFilter').html(html);
                }
                html = '';
                if(RawData.course.length > 0){
                    for(var a in RawData.course){
                        html += '<a class="active" data-id="'+RawData.course[a].courseId+'" href="javascript:;">'+RawData.course[a].name+'</a>'
                    }
                    $('#courseFilter').html(html);
                }

                html = '';
                $('#banksWrapper').append($('<option></option>').val('').html('--Select--'));
                if(RawData.banks.length > 0){
                    for(var a in RawData.banks){

                        $('#banksWrapper').append($('<option></option>').val(RawData.banks[a].bankId).html(RawData.banks[a].bankName));
                    }
                    $('#banksWrapper').material_select();
                }
                buildpopover();
            }
        }});
    }
    function filterChequeDDReversalReport(This){
        $("#banksWrapper").parent().find('.select-dropdown').removeClass("required");
        $("#txtFromDate").removeClass("required");
        $("#txtToDate").removeClass("required");
        $('#banksWrapper').parent().next('label').removeClass("active");
        $('#banksWrapper').parent().next('span.required-msg').remove();
        $('#txtFromDate').parent().find('label').removeClass("active");
        $('#txtFromDate').parent().find('span.required-msg').remove();
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();
        var branchId = [];
        var courseId = [];
        var selMulti = $.map($("#banksWrapper option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).val();
            }
        });
        var bankIds = selMulti.join(",");

        var selMulti = $.map($("#banksWrapper option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).text();
            }
        });
        var bankIdsText = selMulti.join(",");
        var fromDate=$('#txtFromDate').val();
        var toDate=$('#txtToDate').val();
        if(fromDate.trim()!='') {
            var txtfromDate = dateConvertion(fromDate);
        }
        if(toDate.trim()!='') {
            var txttoDate = dateConvertion(toDate);
        }
        var error = true;

        $("#branchFilter > .active").each(function() {
            error = false;
            branchId.push($(this).attr('data-id'));
        });
        $("#courseFilter > .active").each(function() {
            error = false;
            courseId.push($(this).attr('data-id'));
        });
        var branchId=branchId.join(', ');
        var courseId=courseId.join(', ');
        if(bankIds.trim()==''){
            $("#banksWrapper").parent().find('.select-dropdown').addClass("required");
            $("#banksWrapper").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            error=true;
            alertify.dismissAll();
            notify('Select any one bank','error');
            return false;
        }
        else if($('#txtFromDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select from date','error');
            return false;

        }
        else if($('#txtToDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select to date','error');
            return false;

        }
        else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
            $("#txtFromDate").addClass("required");
            $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">Invalid</span>');
            alertify.dismissAll();
            notify('Invalid dates','error');
            error=true;
            return false;
        }
        else if(branchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one branch','error');
            return false;
        }
        else if(courseId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one course','error');
            return false;
        }
        else{
            error=false;
        }

        if(!error){
            var action = 'report';
            var ajaxurl=API_URL+'index.php/ChequeDDReversal/getChequeDDReversalListReport';
            var params = {'branch': branchId, 'course': courseId,fromDate:fromDate,toDate:toDate,bank:bankIds};
            var userID=$('#userId').val();
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            $("#chequeDDReversal-report-list-wrapper").show();
            $("#chequeDDReversal-report-list").dataTable().fnDestroy();
            $tableobj = $('#chequeDDReversal-report-list').DataTable( {
                "fnDrawCallback": function() {
                    $("#chequeDDReversal-report-list thead th").removeClass("icon-rupee");
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                    }
                    verifyAccess();
                    buildpopover();
                },
                "footerCallback": function ( row, data, start, end, display ) {
                    $('#chequeDDReversal-report-list_wrapper div.DTHeader').html('CHEQUE / DD / REVERSAL');
                },
                dom: "<'DTHeader'>Bfrtip",
                bInfo: false,
                "serverSide": false,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No Records Found",
                    "sZeroRecords": "No Records Found",
                    "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
                },
                "bProcessing": true,
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    data:params,
                    headers:headerParams,
                    error:function(response) {
                        DTResponseerror(response);
                    }
                },
                columns: [
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.enrollNo;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.leadName;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.receiptNo;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.remarks;
                    }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        return moneyFormat(data.amount);
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.userName;

                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.receipt_date;

                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.deletedBy;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.deletedOn;
                    }
                    }
                ]
            } );
            DTSearchOnKeyPressEnter();

        } else {
            alertify.dismissAll();
            notify('Select any filter', 'error', 10);
        }
    }
    function filterChequeDDReversalReportExport(This){

        $("#banksWrapper").parent().find('.select-dropdown').removeClass("required");
        $("#txtFromDate").removeClass("required");
        $("#txtToDate").removeClass("required");
        $('#banksWrapper').parent().find('label').removeClass("active");
        $('#banksWrapper').parent().find('span.required-msg').remove();
        $('#txtFromDate').parent().find('label').removeClass("active");
        $('#txtFromDate').parent().find('span.required-msg').remove();
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();
        var branchId = [];
        var branchIdText = [];
        var courseId = [];
        var courseIdText = [];
        var fromDate=$('#txtFromDate').val();
        var toDate=$('#txtToDate').val();
        if(fromDate.trim()!='') {
            var txtfromDate = dateConvertion(fromDate);
        }
        if(toDate.trim()!='') {
            var txttoDate = dateConvertion(toDate);
        }
        var selMulti = $.map($("#banksWrapper option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).val();
            }
        });
        var bankIds = selMulti.join(",");

        var selMulti = $.map($("#banksWrapper option:selected"), function (el, i) {

            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).text();
            }
        });
        var bankIdsText = selMulti.join(",");
        var error = true;
        $("#branchFilter > .active").each(function() {
            error = false;
            branchId.push($(this).attr('data-id'));
            branchIdText.push($(this).text());
        });
        $("#courseFilter > .active").each(function() {
            error = false;
            courseId.push($(this).attr('data-id'));
            courseIdText.push($(this).text());
        });

        var branchId=branchId.join(', ');
        var courseId=courseId.join(', ');
        if(bankIds.trim()==''){
            $("#banksWrapper").parent().find('.select-dropdown').addClass("required");
            $("#banksWrapper").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            error=true;
            alertify.dismissAll();
            notify('Select any one bank','error');
            return false;
        }
        else if($('#txtFromDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select from date','error');
            return false;

        }
        else if($('#txtToDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select to date','error');
            return false;
        }
        else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
            $("#txtFromDate").addClass("required");
            $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">Invalid</span>');
            alertify.dismissAll();
            notify('Invalid dates','error');
            error=true;
            return false;
        }
        else if(branchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one branch','error');
            return false;
        }
        else if(courseId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one course','error');
            return false;
        }
        else{
            error=false;
        }

        if(!error){
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var action = 'report';
            var ajaxurl=API_URL+'index.php/ChequeDDReversal/getChequeDDReversalListReportExport';
            var params = {'branch': branchId, 'course': courseId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '),fromDate:fromDate,toDate:toDate,bank:bankIds,bankText:bankIdsText};
            var userID=$('#userId').val();
            var type='GET';
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            var response =
                commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterChequeDDReversalReportExportResponse});
        } else {
            alertify.dismissAll();
            notify('Select any filter', 'error', 10);
        }
    }
    function filterChequeDDReversalReportExportResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            var download=API_URL+'Download/downloadReport/'+response.file_name;
            window.location.href=(download);
        }
        else{
            notify(response.message,'error');
        }
    }
    /* Cheque DD Reversal list report ends here */

</script>

