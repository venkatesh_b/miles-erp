<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">City </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="left" data-tooltip="Add City" class="viewsidePanel"  onclick="getCityDetails(this,'')" href='javascript:;'><i class="icon-plus-circle" ></i></a></li>
                <!--<li><a></a></li>-->
            </ul>
        </div>   
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" access-element="list"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom table-info" id="cities-list">
                    <thead>
                        <tr>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                        </tr>
                    </thead>
                    <tbody id="cityList">
                    </tbody>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeCityModal(this)"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="countryModalLabel">Create City</h4>
                    </div>
                    <form action="/" method="post" id="countryForm" novalidate="novalidate">
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="country" name="country" class="validate" class="formSubmit">
                                        <option value="default" selected>---Select--</option>
                                    </select>
                                    <label class="select-label">Country <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="state" name="state" class="validate" class="formSubmit">
                                        <option value="default" selected>---Select--</option>
                                    </select>
                                    <label class="select-label">State <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="reference_type_val" type="text" name="reference_type_val" class="validate" class="formSubmit" required>
                                    <label for="reference_type_val">City Name <em>*</em></label>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="reference_type_val_id" name="reference_type_val_id" value="0" />
                    </form>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="actionButton" type="submit" class="btn blue-btn" onclick="AddCity(this)"><i class="icon-right mr8"></i>Add</button>
                        <button type="button" class="btn blue-light-btn" onclick="closeCityModal(this)"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildCitiesReferenceDataTable();
        //getCountriesDropdown();
    });

    function buildCitiesReferenceDataTable()
    {
        var userId = $('#userId').val();
        var ajaxurl=API_URL+'index.php/Referencevalues/getReferenceValuesList';
        var params = {'reference_type':$pageUrl};
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        $("#cities-list").dataTable().fnDestroy();
        var CityTableObj = $('#cities-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No cities found",
                "sZeroRecords": "No cities found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.city;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.state;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    //return data.country;
                    var referenceTypeData='<span class="p0">'+data.country+'</span>';
                    referenceTypeData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                    referenceTypeData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    referenceTypeData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                    referenceTypeData+='</a>';
                    referenceTypeData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    referenceTypeData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    referenceTypeData+='<li><a access-element="edit" href="javascript:;" onclick="getCityDetails(this,'+data.reference_type_value_id+')">Edit</a></li>';
                    referenceTypeData+='</ul>';
                    referenceTypeData+='</div>';
                    return referenceTypeData;

                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter(CityTableObj);
    }
    
</script>
