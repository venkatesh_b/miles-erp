<input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">Cold Calling Scheduler</h3>
        <div class="content-header-btnwrap">
            <ul>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" access-element="List"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">

            <div class="col-sm-12 clearfix p0">

                <div class=" pull-left mt55"><h4 class="heading-uppercase mb0 mt5 font-bold">Leads Stages</h4></div>
                <div class="icons-remember">
                    <p class=" pull-left pr10 m0 pt10">
                        <span class="calls-prevday"><i class="fa fa-phone"></i></span>
                        Yesterday Calls
                    </p>
                    <p class=" pull-left pr10 m0 pt10">
                        <span class="calls-perday"><i class="fa fa-phone"></i></span>
                        Today Calls
                    </p>
                    <p class="pull-left m0 pt10 ">
                        <span class="calls-inhand"><i class="fa fa-phone"></i></span>
                        Calls In Hand
                    </p>
                </div>
                <div class="pull-right callschedule-dtfilter">
                    <h4 class="heading-uppercase pl20 mb5">Filters</h4>
                    <div class="pt2 pull-right"><button type="button" class="btn blue-btn" onclick="genereteColdCallingData(this)" id="btnSetColdCallDate" ><i class="icon-right mr8"></i>Go</button></div>
                    <div class="pull-right mr15 w200">
                        <div class="input-field col-sm-11 p0 custom-select-wrap">
                            <select class="chosen-select" id="txtQuickContactSource">

                            </select>
                            <label class="select-label">Source</label>
                        </div>
                    </div>
                    <div class="pull-right w200">
                        <div class="input-field col-sm-11 mt0 p0 custom-multiselect-wrap"><!-- class="custom-select-nomargin" -->
                            <select data-placeholder="Choose Tags" id="txtQuickContactTag" class="chosen-select">

                            </select>
                            <label class="select-label">Tag</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p0 call-schedule-scroll">
                <table class="table table-responsive table-custom callschedule-table" id="CallScheduleDetails">
                    <thead>

                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="col-sm-12 clearfix p0"> <button type="Submit" class="btn blue-btn" onclick="saveCallSchedules(this)" id="btnSaveCallSchedules" disabled ><i class="icon-right mr8"></i>Save</button></div>
        </div>

        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    function saveCallSchedules(This){
        notify('Processing..', 'warning', 10);
        var callSchedules=[];

        $('input[name="txtColdCalling"]').each(function() {
            if($(this).val()) {
                var currentVal = parseInt($(this).val());
                var res = $(this).attr('id').split("_");
                callSchedules.push(res[1] + '-' + $(this).val());

            }

        });
        var callSchedules=callSchedules.join('|');

        var ajaxurl=API_URL+'index.php/CallSchedule/addCallSchedule';
        var userID=$('#hdnUserId').val();
        var action='schedule';
        var branchId=$('#userBranchList').val();
        var type="POST";
        var tagValue = $("#txtQuickContactTag").val();
        var sourceValue = $("#txtQuickContactSource").val();
        var params={branchId:branchId,callSchedules:callSchedules,chosenTag:tagValue,chosenSource:sourceValue};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response=
            commonAjaxCall({This:This,method:type,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:saveCallSchedulesResponse});

    }
    function saveCallSchedulesResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            $('#btnSaveCallSchedules').attr('disabled',false);
            notify(response.message,'success',10);
            genereteColdCallingData(this);
        }
        else{
            notify(response.message,'error',10);
        }
    }
    docReady(function(){
        getLeadsTags(this);
        getLeadSourcesDropdown();
        genereteColdCallingData(this);
    });
    /*function genereteColdCallingData(This){

        $.when(getAvailableLeadCallCounts(This)).then(function(){

            getAvailableCounts(This);
            getCallsScheduleData(This);
        });

        $('#btnSaveCallSchedules').attr('disabled',true);
    }*/
    function genereteColdCallingData(This)
    {
        var tagValue = $("#txtQuickContactTag").val();
        var sourceValue = $("#txtQuickContactSource").val();
        $('#btnSaveCallSchedules').attr('disabled',true);
        var ajaxurl=API_URL+'index.php/CallSchedule/getLeadColdCallingCount';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        var params={branchId:branchId,chosenTag:tagValue,chosenSource:sourceValue};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        //var response=
          //  commonAjaxCall({This:This,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:buildLeadCallsScheduleCountsData});

        $.when(

            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    requestUrl: API_URL+'index.php/CallSchedule/getCallCounts',
                    params: params,
                    action: action
                }
            ),
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    requestUrl: API_URL+'index.php/CallSchedule/getCallSchedules',
                    params: params,
                    action: action
                }
            )
        ).then(function (response2,response3) {

            buildCallsScheduleData(response3[0]);
            buildCallsScheduleCountsData(response2[0]);
            userWorkLoad();
            buildpopover();

        });


    }
    function getAvailableCounts(This){

        var ajaxurl=API_URL+'index.php/CallSchedule/getCallCounts';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        var params={branchId:branchId};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        var response=
            commonAjaxCall({This:This,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:buildCallsScheduleCountsData});

    }
    function buildCallsScheduleCountsData(response){

        var coldCallingCount=0;
        var rowHtml='';
        if(response.status===true){
            rowHtml+="<tr><tr class='text-center'>";
            rowHtml+="<th  class=\"text-center\" style=\"vertical-align:middle;\">Calls vs Users</th>";
            rowHtml+="<th class=\"p5\" style=\"text-align:left\"><span class=\"cold-call-num\" id=\"coldCallingWrapper\">200</span>Cold Calling</th>";
            rowHtml+="<th class=\"p5\" class=\"current-batch\" style=\"vertical-align:middle;text-align:left\">User Work Load</th>";
            rowHtml+="</tr>";
            rowHtml+="<tr><td class=\"border-none p0 lh10\">&nbsp;</td></tr>";
            $('#CallScheduleDetails thead').html(rowHtml);



            coldCallingCount=response.data.count;
            $('#coldCallingWrapper').html(coldCallingCount);
            $('#coldCallingBalanceWrapper').html(coldCallingCount);


        }




    }

    function getCallsScheduleData(This){
        var ajaxurl=API_URL+'index.php/CallSchedule/getCallSchedules';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        var params={branchId:branchId};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        var response=
            commonAjaxCall({This:This,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:buildCallsScheduleData});

    }

    var CallsScheduleDataResponse='';
    var glbleadCallTxtBxs='';
    function buildCallsScheduleData(response){
        var rowsHtml='';
        var leadCallTxtBxs=[];
        CallsScheduleDataResponse=response;
        if(CallsScheduleDataResponse.status===true){

            $.each(CallsScheduleDataResponse.data,function(key,value){
                var userImage='';
                if(value.image && CheckFileExists(value.image))
                {
                    userImage += '<img src="'+value.image+'">';

                }
                else
                {
                    userImage += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png">';
                }
                rowsHtml+="<tr>";
                rowsHtml+="<td>";
                rowsHtml+="<div class=\"userimage-div-2 clearfix display-inline-block\">";
                rowsHtml+="<div class=\"image-user-wrap pull-left\">";
                rowsHtml+=userImage;
                rowsHtml+="</div>";
                rowsHtml+="<div class=\"call-user-wrap display-inline-block\">";
                rowsHtml+="<span class=\"call-username ellipsis tooltipped\" data-position=\"top\" data-delay=\"50\" data-tooltip='"+value.name+"'>"+value.name+"</span>";
                rowsHtml+="<span class=\"call-userrole\">"+value.role+"</span>";
                rowsHtml+="</div>";
                rowsHtml+="</div>";
                rowsHtml+="<div class=\"call-user-2 display-inline-block\">";
                rowsHtml+="<span class=\"call-prev-avg-2\"><i class=\"fa fa-phone\"></i>"+value.previousCallsCount+"<input type=\"hidden\" id=\"previousCallCountWrapper_"+value.user_id+"\" value=\""+value.previousCallsCount+"\" readonly disabled/></span>";
                rowsHtml+="<span class=\"call-avg-2\"><i class=\"fa fa-phone\"></i>"+value.todaysCallsCnt+"<input type=\"hidden\" id=\"previousCallCountWrapper_"+value.user_id+"\" value=\""+value.todaysCallsCnt+"\" readonly disabled/></span>";
                //rowsHtml+="<span class=\"calls-2\"><i class=\"fa fa-phone\"></i>"+value.inHandCallsCount+"<input type=\"hidden\" id=\"inHandCallCountWrapper_"+value.user_id+"\" value=\""+value.inHandCallsCount+"\" readonly disabled/></span>";
                //rowsHtml+="<span class=\"calls-2\"><i class=\"fa fa-phone\"></i><input class='form-control inp-call-schedule' type=\"text\" id=\"inHandCallCountWrapper_"+value.user_id+"\" value=\""+value.inHandCallsCount+"\"/></span>";
                if(value.inHandCallsCount>0)
                {
                    rowsHtml+="<span class=\"calls-2\"><i class=\"fa fa-phone\"></i><input class='form-control inp-call-schedule' type=\"text\" id=\"inHandCallCountWrapper_"+value.user_id+"\" value=\""+value.inHandCallsCount+"\"/></span>";
                    rowsHtml += "<a href=\"javascript:;\" access-element='release' data-tooltip=\"Release Calls\" data-position=\"top\" title=\"Release Calls\" class=\"ml15 tooltipped\" onclick=\"confirmColdCallRelease(this,'" + value.user_id + "',"+value.inHandCallsCount+")\"><i class=\"fa fa-external-link-square pt10\"></i></a>";
                }
                else
                {
                    rowsHtml+="<span class=\"calls-2\"><i class=\"fa fa-phone\"></i>"+value.inHandCallsCount+"<input type=\"hidden\" id=\"inHandCallCountWrapper_"+value.user_id+"\" value=\""+value.inHandCallsCount+"\" readonly disabled/></span>";
                }
                rowsHtml+="</div>";
                rowsHtml+="</td>";
                rowsHtml+="<td  style=\"text-align:left\"><input class=\"callschedule-input-2\" name=\"txtColdCalling\" id='txtColdCalling_"+value.user_id+"' type=\"text\" maxlength=\"3\"></td>";
                rowsHtml+="<td style=\"text-align:left\" id=\"WorkLoadWrapper_"+value.user_id+"\">0</td>";
                rowsHtml+="</tr>";



                /*rowsHtml+="<tr class='animated fadeInUp'>";
                rowsHtml+="<td>";
                rowsHtml+="<div class=\"userimage-div clearfix\">";
                rowsHtml+="<div class=\"image-user pull-left\">";
                rowsHtml+="<img src='"+value.image+"'>";
                rowsHtml+="</div>";
                rowsHtml+="<div class=\"call-user\">";
                rowsHtml+="<span class=\"call-username ellipsis tooltipped\" data-position=\"top\" data-delay=\"50\" data-tooltip="+value.name+">"+value.name+"</span>";
                rowsHtml+="<span class=\"call-userrole\">"+value.role+"</span>";
                rowsHtml+="<span class=\"call-avg\"><i class=\"fa fa-phone\"></i>"+value.previousCallsCount+"<input type=\"hidden\" id=\"previousCallCountWrapper_"+value.user_id+"\" value=\""+value.previousCallsCount+"\" readonly disabled/></span>";
                rowsHtml+="<span class=\"calls\"><i class=\"fa fa-phone\"></i>"+value.inHandCallsCount+"<input type=\"hidden\" id=\"inHandCallCountWrapper_"+value.user_id+"\" value=\""+value.inHandCallsCount+"\" readonly disabled/></span>";
                if(value.inHandCallsCount>0) {
                    rowsHtml += "<a href=\"javascript:;\" access-element='release' data-tooltip=\"Release Calls\" data-position=\"top\" class=\"tooltipped\" onclick=\"confirmColdCallRelease(this,'" + value.user_id + "',"+value.inHandCallsCount+")\"><i class=\"fa fa-external-link-square\"></i></a>";
                }
                rowsHtml+="</div>";
                rowsHtml+="</div>";
                rowsHtml+="</td>";
                rowsHtml+="<td><input class=\"callschedule-input\" name=\"txtColdCalling\" id='txtColdCalling_"+value.user_id+"' type=\"text\" maxlength=\"3\"></td>";

                rowsHtml+="<td id=\"WorkLoadWrapper_"+value.user_id+"\">0</td>";
                rowsHtml+="</tr>";*/



            });
            var remainingCountsHtml='';
            remainingCountsHtml+="<tr class='animated fadeInUp'>";
            remainingCountsHtml+="<td class=\"remaining-tr\">";
            remainingCountsHtml+="<div class=\"call-user\"><span class=\"call-username\" >Call Balance</span></div></td>";
            remainingCountsHtml+="<td style=\"text-align:left\" id='coldCallingBalanceWrapper'>0</td>";
            remainingCountsHtml+="<td style=\"text-align:left\"><span id=\"TotalWorkLoadWrapper\" style=\"visibility:hidden;\">0</span></td>";
            remainingCountsHtml+="</tr>";

        }
        else{
            rowsHtml+="<tr><td colspan='22' class='text-center'>No users found</td></tr>";
        }
        $('#CallScheduleDetails tbody').html(rowsHtml+remainingCountsHtml);
        leadCallTxtBxs.push('input[name=txtColdCalling]');
        leadCallTxtBxs=leadCallTxtBxs.join(',');
        glbleadCallTxtBxs=leadCallTxtBxs;
        $(leadCallTxtBxs).keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        $(leadCallTxtBxs).blur(function (e) {
            $(leadCallTxtBxs).removeClass('required');
            $('#btnSaveCallSchedules').attr('disabled','disabled');
            var total=0;
            var selectedName=$(this).prop('name');
            var headerCountId='';
            var footerBalanceCountId='';
                if(selectedName=='txtColdCalling'){
                    headerCountId='#coldCallingWrapper';
                    footerBalanceCountId='#coldCallingBalanceWrapper';
                }
                else{
                    var sp=selectedName.split('_');
                    headerCountId='#LeadcoldCallingWrapper_'+sp[1];
                    footerBalanceCountId='#LeadcoldCallingBalanceWrapper_'+sp[1];
                }
            var coldCallingCount=parseInt($(headerCountId).html());
            var isValid=1;//0-invalid,1-valid
            $('input[name="'+selectedName+'"]').each(function(index) {
                var var1ref=$('input[name="'+selectedName+'"]:eq('+index+')');
                var var1=var1ref.val();
                var currentVal=parseInt(var1);
                if(currentVal>coldCallingCount){
                    currentVal=0;
                    var1ref.val(null);

                    //alert('Should be less than available count ('+coldCallingCount+')');
                    var ModelTitle="Call Schedule";
                    var Description='Should be less than available count (' + coldCallingCount + ')';
                    customAlert(ModelTitle,Description);
                    $(this).addClass('required');
                    $('#btnSaveCallSchedules').attr('disabled','disabled');
                    return false;
                    isValid=0;
                }
                else{
                    if(var1) {

                        total = total + currentVal;
                    }
                }


            });
            if(isValid==0){
                $(footerBalanceCountId).html('-');
            }
            else if(isValid==1 && total>coldCallingCount){
                //alert('Total Should be less than available count ('+coldCallingCount+')');
                var ModelTitle="Call Schedule";
                var Description='Should be less than available count (' + coldCallingCount + ')';
                customAlert(ModelTitle,Description);
                $(this).addClass('required');
                $(footerBalanceCountId).html('-');
                isValid=0;
            }
            else if(isValid==1 && total<=coldCallingCount){
                $(footerBalanceCountId).html(coldCallingCount-total);
                userWorkLoad();
            }
            if(isValid==1 && (coldCallingCount-total)<coldCallingCount){
                $('#btnSaveCallSchedules').attr('disabled',false);
            }
        });

    }
    function validateCounts(leadCallTxtBxs) {
        var leadCallTxtBxs=glbleadCallTxtBxs;
        //console.log(leadCallTxtBxs);
        $(leadCallTxtBxs).removeClass('required');
        //$('#btnSaveCallSchedules').attr('disabled','disabled');
        var txtBxs = leadCallTxtBxs.split(',');
        //console.log(txtBxs);
        $.each(txtBxs, function (key, value){

            var total = 0;
            var selectedName = $(value).prop('name');

            var headerCountId = '';
            var footerBalanceCountId = '';
            if (selectedName == 'txtColdCalling') {
                headerCountId = '#coldCallingWrapper';
                footerBalanceCountId = '#coldCallingBalanceWrapper';
            }
            else {
                var sp = selectedName.split('_');
                headerCountId = '#LeadcoldCallingWrapper_' + sp[1];
                footerBalanceCountId = '#LeadcoldCallingBalanceWrapper_' + sp[1];
            }
            var coldCallingCount = parseInt($(headerCountId).html());
            var isValid = 1;//0-invalid,1-valid
            $('input[name="' + selectedName + '"]').each(function (index) {
                var var1 = $('input[name="' + selectedName + '"]:eq(' + index + ')').val();
                var var1_id = $('input[name="' + selectedName + '"]:eq(' + index + ')').prop('id');
                var var1_ref = $('input[name="' + selectedName + '"]:eq(' + index + ')');
                //console.log(var1_id);
                var currentVal = parseInt(var1);
                if (currentVal > coldCallingCount) {
                    var ModelTitle="Call Schedule";
                    var Description='Should be less than available count (' + coldCallingCount + ')';
                    customAlert(ModelTitle,Description);


                    var1_ref.addClass('required');
                    $('#btnSaveCallSchedules').attr('disabled', 'disabled');
                    return false;
                    isValid = 0;
                }
                else {
                    if (var1) {

                        total = total + currentVal;
                    }
                }


            });
            if (isValid == 0) {
                $(footerBalanceCountId).html('-');
            }
            else if (isValid == 1 && total > coldCallingCount) {
                var ModelTitle="Call Schedule";
                var Description='Total Should be less than available count (' + coldCallingCount + ')';
                customAlert(ModelTitle,Description);
                isValid = 0;
            }
            else if (isValid == 1 && total <= coldCallingCount) {
                $(footerBalanceCountId).html(coldCallingCount - total);

            }
            if (isValid == 1 && (coldCallingCount - total) < coldCallingCount) {
                $('#btnSaveCallSchedules').attr('disabled', false);


            }
        });

    }
    $('#userBranchList').change(function(){
        changeDefaultBranch(this);
        genereteColdCallingData(this);
    });
    function confirmColdCallRelease(This,assignedUserId,releaseCnt){

        customConfirmAlert('Release Calls', 'Are you sure want to release the calls from the user ?');
        $("#popup_confirm #btnTrue").attr("onclick","coldCallRelease(this,"+assignedUserId+","+releaseCnt+")");
    }
    function coldCallRelease(This,assignedUserId,releaseCnt){

            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/CallSchedule/releaseScheduledCalls';
            var releaseCnt = releaseCnt;
            var userID = $('#hdnUserId').val();
            var releaseCount = $('#inHandCallCountWrapper_'+assignedUserId).val();
            if(releaseCount > releaseCnt)
            {
                alertify.dismissAll();
                notify('Cannot release more than assigned calls','error',10);
            }
            else
            {
                var action = 'release';
                var branchId = $('#userBranchList').val();
                var type = "POST";
                var params = {branchId: branchId, assignedUserId: assignedUserId, releaseCnt: releaseCount};
                var headerParams = {
                    action: action,
                    context: $context,
                    serviceurl: $pageUrl,
                    pageurl: $pageUrl,
                    Authorizationtoken: $accessToken,
                    user: userID
                };

                var response =
                    commonAjaxCall({
                        This: This,
                        method: 'POST',
                        requestUrl: ajaxurl,
                        params: params,
                        headerParams: headerParams,
                        action: action,
                        onSuccess: saveReleaseCallSchedulesResponse
                    });
            }


    }
    function saveReleaseCallSchedulesResponse(response){
        $('#popup_confirm').modal('hide');
        if(response.status===true){
            alertify.dismissAll();
            notify(response.message,'success',10);
            genereteColdCallingData(this);
        }
        else{
            alertify.dismissAll();
            notify(response.message,'error',10);
        }
    }

    var leadColdCallCountResponse='';
    function buildLeadCallsScheduleCountsData(response){
        leadColdCallCountResponse=response;

    }

    function userWorkLoad(){

        if(CallsScheduleDataResponse.status===true){
            var totalworkload=0;
            $.each(CallsScheduleDataResponse.data,function(key,value){
                var workload=0;
                workload+=parseInt($('#inHandCallCountWrapper_'+value.user_id).val());
                var exp='txtColdCalling_'+value.user_id;
                if($("#"+exp) && $("#"+exp).val()!='') {
                    workload += parseInt($("#" + exp).val());
                }
                $("#"+'WorkLoadWrapper_'+value.user_id).html(workload);
                totalworkload+=workload;
            });
        }
            $('#TotalWorkLoadWrapper').html(totalworkload);
        //WorkLoadWrapper_{user_id}=callsInHand_userid+txtColdCalling_{user_id}+txtLeadColdCalling_{stageId}|{user_id}+
    }

    function getLeadsTags(This)
    {
        var $field = $("#txtQuickContactTag");
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Lead/getContactConfigurations';
        var params = {};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: This, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
            $field.empty();
            $field.append($('<option selected></option>').val('').html('--Choose Tag--'));
            if (response.data.tags.length > 0) {
                $.each(response.data.tags, function (key, value) {
                    $field.append($('<option></option>').val(value.id).html(value.tagName));
                });
            }
            $field.chosen({
                no_results_text: "Oops, nothing found!",
                width: "95%"
            });
            $field.trigger("chosen:updated");
        }});
    }

    function getLeadSourcesDropdown() {
        var $field = $("#txtQuickContactSource");
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Contact Source'};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function(response){
            $field.empty();
            $field.append($('<option selected></option>').val('').html('--Choose Source--'));
            if (response.status == true)
            {
                if (response.data.length > 0)
                {
                    $.each(response.data, function (key, value) {
                        $field.append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                    });
                }
                $field.trigger("chosen:updated");
            }
            $field.chosen({
                no_results_text: "Oops, nothing found!",
                width: "95%"
            });
        }
        });
    }




</script>
