<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">Company </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="tooltipped" data-position="top" data-tooltip="Add Company" class="viewsidePanel" onclick="getCompanyDetails(this,'')" href="javascript:;"><i class="icon-plus-circle" ></i></a></li>
                <!--<li><a></a></li>-->
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" > <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0" access-element="list">
                <table class="table table-responsive table-striped table-custom table-info" id="companies-list">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th class="border-r-none">Description</th>
                        <th class="no-sort"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="companyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeCompanyModal(this)"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="tagModalLabel">Create Company</h4>
                    </div>
                    <form action="/" method="post" id="companyForm" novalidate="novalidate">
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="companyName" type="text" name="companyName" class="validate" class="formSubmit" required>
                                    <label for="companyName">Company Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="companyDescription" name="companyDescription" class="materialize-textarea"></textarea>
                                    <label for="companyDescription">Company Description</label>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="reference_type_val_id" name="reference_type_val_id" value=""/>
                    </form>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue-btn" onclick="AddCompany(this)"><i class="icon-right mr8"></i>Submit</button>
                        <button type="button" class="btn blue-light-btn" onclick="closeCompanyModal(this)"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildCompanyReferenceDataTable();
    });

    function buildCompanyReferenceDataTable()
    {
        var userId = $('#userId').val();
        var ajaxurl=API_URL+'index.php/Referencevalues/getReferenceValuesList';
        var params = {'reference_type':'company'};
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        $("#companies-list").dataTable().fnDestroy();
        $tableobj = $('#companies-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    'width': '30%',
                    data: null, render: function ( data, type, row )
                {
                    return data.company_name;
                }
                },
                {
                    'width': '69%',
                    data: null, render: function ( data, type, row )
                {
                    if(data.description==null || data.description.trim()==''){
                        data.description='----';
                    }
                    return data.description;
                }
                },
                {
                    'width': '1%',
                    data: null, render: function ( data, type, row )
                {
                    var referenceTypeData='<div class="dropdown feehead-list pull-right custom-dropdown-style pl20">';
                    referenceTypeData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    referenceTypeData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                    referenceTypeData+='</a>';
                    referenceTypeData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    referenceTypeData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    referenceTypeData+='<li><a access-element="edit" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick="getCompanyDetails(this,'+data.reference_type_value_id+')">Edit</a></li>';
                    referenceTypeData+='</ul>';
                    referenceTypeData+='</div>';
                    return referenceTypeData;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function getCompanyDetails(This, id) {

        $('#companyForm')[0].reset();
        $('#companyForm input').removeClass("required");
        $('#companyForm textarea').removeClass("required");
        $('#companyForm label').removeClass("active");

        if (id == '' || id == 0) {
            $("#tagModalLabel").html("Create Company");
            $("#reference_type_val_id").val(id);
            $('#companyModal').modal('toggle');
        } else {
            notify('Processing..', 'warning', 10);
            $("#tagModalLabel").html("Edit Company");
            var userId = $('#userId').val();
            var ajaxurl = API_URL + "index.php/Company/getCompanyById/company_id/" + id;
            var params = {};
            var action = 'list';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: function (response) {
                if (response == -1 || response['status'] == false)
                {
                } else
                {
                    alertify.dismissAll();

                    $('#companyName').val(response.data[0].value);
                    $('#companyName').next('label').addClass('active');
                    $('#reference_type_val_id').val(response.data[0].id);
                    if(response.data[0].description!=null && response.data[0].description.trim()!='') {
                        $('#companyDescription').val(response.data[0].description);
                        $('#companyDescription').next('label').addClass('active');
                    }

                    $('#companyModal').modal('toggle');
                }
            }
            });
        }
    }

    function closeCompanyModal(This) {
        $('#companyForm')[0].reset();

        $('#reference_type_val_id').val('');

        $('#companyForm span.required-msg').remove();
        $('#companyForm input').removeClass("required");
        $('#companyForm textarea').removeClass("required");

        $('#companyModal').modal('toggle');

        $("#tagModalLabel").html("Create Tag");
    }
    function AddCompany(This) {
        $('#companyForm span.required-msg').remove();
        $('#companyForm input').removeClass("required");
        $('#companyForm textarea').removeClass("required");
        var id = $('#reference_type_val_id').val();
        var companyNameRegx=$rgx_allow_alpha_numeric_space;
        var url = '';
        var action = 'add';
        var name = $('#companyName').val().trim();
        var description = $('#companyDescription').val().trim();
        var data = '';
        if (id != '')
        {
            $("#tagModalLabel").html("Edit Company");
            data = {
                companyName: name,
                companyDescription: description,
                companyId: id
            };
            action = 'edit';
            url = API_URL + "index.php/Company/editCompanyReference";
        } else {
            action = 'add';
            $("#tagModalLabel").html("Create Company");
            data = {
                companyName: name,
                companyDescription: description
            };
            url = API_URL + "index.php/Company/addCompanyReference";
        }

        var flag = 0;
        if (name == '') {
            $("#companyName").addClass("required");
            $("#companyName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        /*else if (!(companyNameRegx.test(name)))
        {
            $("#companyName").addClass("required");
            $("#companyName").after('<span class="required-msg">'+$rgx_allow_alpha_numeric_space_message+'</span>');
            flag = 1;
        }*/

        /*if (description == '') {
            $("#companyDescription").addClass("required");
            $("#companyDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if (!(/^([a-zA-Z ])/.test(description)))
        {
            $("#companyDescription").addClass("required");
            $("#companyDescription").after('<span class="required-msg">Invalid Description</span>');
            flag = 1;
        }*/

        if (flag == 0) {
            var ajaxurl = url;
            var params = data;

            var userId = $('#userId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                if (response == -1 || response['status'] == false)
                {
                    var id = '';
                    alertify.dismissAll();
                    //notify(response['message'], 'error', 10);
                    for (var a in response.data) {
                        if (a == 'reference_type_val') {
                            id = '#companyName';
                        } else {

                            id = '#' + a;
                        }
                        $(id).addClass("required");
                        $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                    }
                    $('#companyModal').removeAttr('disabled');
                } else
                {
                    buildCompanyReferenceDataTable();
                    $('#companyForm')[0].reset();
                    $('#companyModal').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    alertify.dismissAll();
                    notify('Data Saved Successfully', 'success', 10);
                }
            }});

        } else {
            return false;
        }
    }

</script>