<input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
<input type="hidden" id="hdnContactId" value="0" readonly />
<input type="hidden" id="hdnLeadId" value="0" readonly />

<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->

    <div class="content-header-wrap">
            <h3 class="content-header">Contacts</h3>
             <div class="content-header-btnwrap">
                <ul>
                    <li access-element="filters"><a class="tooltipped" data-position="top" data-tooltip="Filters" data-toggle="modal" data-target="#filters" class=""><i class="icon-filter"></i></a></li>
                    <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Add Contact" href="javascript:;" onclick="ManageContact(this,0)"><i class="icon-plus-circle"></i></a></li>
                    <li access-element="import" ><a class="tooltipped" data-position="top" data-tooltip="Import Contacts" href="javascript:;" onclick="resetImport(this)"><i class="icon-import1"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
            <div access-element="list" class="fixed-wrap clearfix">

                <div class="col-sm-12 p0 mb10">
                    <div class="col-5">
                        <div class="contact-wrap branch-green-bg"> <span class="green-bg"><img src="<?php echo BASE_URL;?>assets/images/contacts1.png"></span> </div>
                        <div class="contact-wrap-text mr5 branch-green-bg-light">
                            <div class="display-inline-block contact-green-color">Overall</div>
                            <div class="contact-blue" id="OverallCount">----</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="contact-wrap branch-red-bg"> <span class="red-bg"><img src="<?php echo BASE_URL;?>assets/images/unscheduled.png"></span> </div>
                        <div class="contact-wrap-text mr5 branch-red-bg-light">
                            <div class="display-inline-block contact-red-color">Unscheduled</div>
                            <div class="contact-blue" id="UnscheduledCount">----</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="contact-wrap branch-blue-bg"> <span class="blue-bg"><img src="<?php echo BASE_URL;?>assets/images/Leads.png"></span> </div>
                        <div class="contact-wrap-text mr5 branch-blue-bg-light">
                            <div class="display-inline-block contact-blue-color">Database(Leads)</div>
                            <div class="contact-blue" id="DBLeadCount">----</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="contact-wrap branch-yellow-bg"> <span class="yellow-bg"><img src="<?php echo BASE_URL;?>assets/images/dnd.png"></span> </div>
                        <div class="contact-wrap-text mr5 branch-yellow-bg-light">
                            <div class="display-inline-block contact-yellow-color">Database(DND)</div>
                            <div class="contact-blue" id="DBDNDCount">----</div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="contact-wrap orange-bg"> <span class="branch-orange-bg"><img src="<?php echo BASE_URL;?>assets/images/contacts5.png"></span> </div>
                        <div class="contact-wrap-text branch-orange-bg-light">
                            <div class="display-inline-block orange-color">Enrolled</div>
                            <div class="contact-blue" id="EnrolledCount">----</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p0 clearfix">
                    <div class="pull-right pr10 pt10 f12 contact-legends"> <span class=" pull-left pl10 m0 pt10"> <i class="fa fa-user mr2 valign-middle ash f16"></i> Lead </span> <span class="pull-left pl10 m0 pt10"> <i class="fa fa-user mr2 valign-middle dark-sky-blue f16"></i> Student </span> <span class="pull-left pl10 m0 pt10"> <i class="fa fa-ban mr2 valign-middle red f16"></i> DND </span> <span class="pull-left pl10 m0 pt10"> <i class="fa fa-phone mr2 valign-middle green f16"></i> Cold Calling </span> <span class="pull-left pl10 m0 pt10"> <i class="fa fa-users mr2 valign-middle orange f14"></i> Duplicate </span> </div>
                </div>
                <div class="col-sm-12 p0" access-element="list">
                    <table class="table table-responsive table-striped table-custom" id="contacts-list">
                        <thead>
                            <tr class="">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>City</th>
                                <th class='border-r-none'>Tags</th>
                                <th class="text-center no-sort w25" ></th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <form id="formTagOutStationContacts">

                    <input type="hidden" value="" id="hdnTaggingType" readonly disabled />
                <button type="button" class="btn blue-btn mr5" onclick="ManageTagOutStationContacts(this,1)" id="btnManageTagOutStations">Tag Out Stations
                </button>
                <button type="button" class="btn blue-btn" onclick="ManageTagOutStationContacts(this,0)" id="btnManageTagOutStations">Tag All Out Stations
                    </button>
                <div id="OutStationsTagWrapper" style="display: none">
                    <div class="col-sm-12 mt10 pl0 clearfix" style="display: none" id="checkUncheckWrapper">
                        <span class="inline-checkbox">
                            <input id="checkUncheck" class="filled-in" type="checkbox">
                            <label for="checkUncheck" class="acc-info-label text-uppercase">Check/Uncheck All</label>
                        </span>
                    </div>
                    <div class="col-sm-12 p0 clearfix">
                    <div class="col-sm-2 mt10 pl0">
                        <div class="input-field">
                            <select id="txtContactBranch" class="">

                            </select>
                            <label class="select-label">Branch <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-3 mt15 pl0">
                    <button type="button" class="btn blue-btn ml15" onclick="tagOutStationContacts(this)" id="btnTagOutStations">Proceed
                    </button>
                        </div>
                    </div>
                </div>
                    </form>
            </div>
            <!--Create Contact Modal-->
            
            <div class="modal fade" id="createContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 data-modal="right" modal-width="500">
                <form id="ManageConcatForm" method="post">
                    
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Contact</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtContactName" type="text" class="validate">
                                    <label for="txtContactName">Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtContactEmail" type="email">
                                    <label for="txtContactEmail" data-error="wrong" data-success="right">Email <!--<em>*</em>--></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtContactPhone" type="text" class="validate" onkeypress="return isPhoneNumber(event)" maxlength="15">
                                    <label for="txtContactPhone">Phone <!--<em>*</em>--></label>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 mb10">
                                <div class="input-field">
                                    <select id="txtContactCountry">
                                        <option value="" selected>--Select--</option>
                                        
                                    </select>
                                    <label class="select-label">Country <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-6 mb10">
                                <div class="input-field">
                                    <select id="txtContactCity">
                                        <option value=""  selected>--Select--</option>
                                        
                                    </select>
                                    <label class="select-label">City <em>*</em></label>
                                </div>
                            </div>
                            <!--<div class="col-sm-12">
                                <div class="input-field">
                                    <select multiple id="txtContactTag">
                                        <option value="" disabled selected>--Select--</option>
                                       
                                    </select>
                                    <label class="select-label">Tags <em>*</em></label>
                                </div>
                            </div>-->
                            <div class="col-sm-12 mb20" id="contactType-corporate-tags">
                                <div class="input-field col-sm-11 p0 custom-multiselect-wrap"><!-- class="custom-select-nomargin" -->
                                    <select multiple data-placeholder="Choose Tags" id="txtContactTag" class="chosen-select">
                                        <option value="" disabled>--Select--</option>
                                    </select>
                                    <label class="select-label">Tags<!-- <em>*</em>--></label>
                                </div>
                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetTagWrapper(this)" href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ContactTagWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtContactTagWrapper" type="text" class="validate" value="">
                                            <label for="txtContactTagWrapper">Tag Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="addTagWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" id="contactType-source">
                                <div class="input-field col-sm-11 p0 custom-select-wrap">
                                    <select class="chosen-select" id="txtContactSource">
                                        <option value="" disabled selected>--Select--</option>

                                    </select>
                                    <label class="select-label">Source <em>*</em></label>
                                </div>
                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetContactSourceWrapper(this)" href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ContactSourceWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtContactSourceWrapper" type="text" class="validate" value="">
                                            <label for="txtContactSourceWrapper">Source <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="addContactSourceWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="hint-msg">Note: Email or Phone is mandatory <em>*</em></label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue-btn" onclick="saveContact(this)" id="btnSaveContact"><i class="icon-right mr8"></i>Save
                            </button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                    class="icon-times mr8"></i>Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
   


            <!--View Contact Model-->

            <div class="modal fade" id="viewContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 data-modal="right" modal-width="500">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title">Contact Details</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Name</span>
                                    <p id="viewContactName">---</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Email</span>
                                    <p id="viewContactEmail">---</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <span class="display-block ash">Phone</span>
                                    <p id="viewContactPhone">---</p>
                                </div>
                            </div>

                            <!--<div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Company</span>
                                    <p id="viewContactCompany">---</p>
                                </div>
                            </div>-->
                            <!--<div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Branch</span>
                                    <p id="viewContactBranch">---</p>
                                </div>
                            </div>-->
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Country</span>
                                    <p id="viewContactCountry">---</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">City</span>
                                    <p id="viewContactCity">---</p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <span class="display-block ash">Tags</span>
                                    <p id="viewContactTags">---</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Source</span>
                                    <p id="viewContactSource">----</p>
                                </div>
                            </div>
                            <!--<div class="col-sm-12">
                                <div class="input-field">
                                    <select>
                                        <option value="" disabled selected>&#45;&#45;Select&#45;&#45;</option>
                                        <option value="1">Branch 1</option>
                                        <option value="2">Branch 2</option>
                                        <option value="3">Branch 3</option>
                                    </select>
                                    <label class="select-label">Select Branch</label>
                                </div>
                            </div>-->
                            <!--<div class="col-sm-12">
                                <button class="btn blue-btn mt15" type="button"><i class="icon-users1 mr8"></i>Convert to Lead</button>
                            </div>-->




                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <!--<button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save
                            </button>-->
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                    class="icon-times mr8"></i>Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>


            <!--Import Model-->

            <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 data-modal="right" modal-width="600">
                <form id="importForm" method="post">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title">Import</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="file-field input-field">
                                    <div class="btn file-upload-btn">
                                        <span>Upload Document</span>
                                        <input type="file" id="txtfilebrowser">
                                    </div>
                                    <div class="file-path-wrapper custom">
                                        <input type="text" placeholder="Upload single file" class="file-path validate">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mt20">
                                <a class="btn blue-btn btn-sm" href="<?php echo BASE_URL.'templates/ContactsTemplate.csv'; ?>" id="btnDownloadSample"><i class="icon-file mr8"></i>Download Sample</a>
                            </div>

                            <div class="col-sm-12 mb20 mt20">
                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info ml0 grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Total Contacts</div>
                                            <div class="camp-num red f14" id="TotalUploadedContacts">-</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Uploaded</div>
                                            <div class="camp-num thick-blue f14" id="UploadedContacts">-</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Failed</div>
                                            <div class="camp-num violet f14" id="FailedContacts">-</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info mr0 grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Duplicate</div>
                                            <div class="camp-num light-black f14" id="DuplicateContacts">-</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-responsive table-striped table-custom" id="uploadedContactsList">
                                    <colgroup>
                                        <col width="15%">
                                        <col width="35%">
                                        <col width="50%">
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th>Row No.</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                    </tr>
                                    <tr>
                                        <td class="border-none p0 lh10">&nbsp;</td>
                                    </tr>
                                    <!-- <tr><td class="border-none p0 lh5">&nbsp;</td></tr>-->
                                    </thead>
                                    <tbody>
                                        <tr class="">
                                            <td colspan="3" align='center'>No Data Found</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <a class="btn blue-btn" href="javascript:;" onclick="importContacts(this)"><i class="icon-right mr8"></i>Upload
                            </a>
                            <a class="btn blue-light-btn" data-dismiss="modal"><i
                                    class="icon-times mr8"></i>Cancel
                            </a>
                        </div>
                    </div>
                </div>
            </form>
            </div>
            <!--Filter Modal-->
            <div class="modal fade" id="filters" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Filters</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="">
                                <div class="input-field mt0">
                                    <input class="filled-in" id="all" type="checkbox" name="filtersContact" value="All">
                                    <label class="f14" for="all">All</label>
                                </div>
                                <div class="input-field mt0">
                                    <input class="filled-in" id="dnd_contact" type="checkbox" name="filtersContact" value="DND">
                                    <label class="f14" for="dnd_contact">DND Contact</label>
                                </div>
                                <div class="input-field mt0">
                                    <input class="filled-in" id="contact_OutStation" type="checkbox" name="filtersContact" value="OutStation">
                                    <label class="f14" for="contact_OutStation">Out Station</label>
                                </div>
                                <div class="input-field mt0">
                                    <input class="filled-in" id="contact_list" type="checkbox" name="filtersContact" value="Contacts">
                                    <label class="f14" for="contact_list">Contacts</label>
                                </div>
                                <div class="input-field mt0">
                                    <input class="filled-in" id="contact_lead" type="checkbox" name="filtersContact" value="ContactLead">
                                    <label class="f14" for="contact_lead">Contact to Lead</label>
                                </div>
                                <div class="input-field mt0">
                                    <input class="filled-in" id="contact_student" type="checkbox" name="filtersContact" value="ContactStudent">
                                    <label class="f14" for="contact_student">Contact to Student (M7)</label>
                                </div>
                                <div class="input-field mt0">
                                    <input class="filled-in" id="merge_contact" type="checkbox" name="filtersContact" value="MergeContact">
                                    <label class="f14" for="merge_contact">Merge Contacts</label>
                                </div>
                                <div class="input-field mt10 custom-multiselect-wrap">
                                    <select multiple data-placeholder="Choose City" id="ddlContactCity" class="chosen-select">

                                    </select>
                                    <label class="select-label" for="ddlContactCity">Cities<!-- <em>*</em>--></label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue-btn" onclick="applyContactFilters()"><i class="icon-right mr8"></i>Apply</button>
                            <button type="button" class="btn blue-light-btn" onclick="resetContactFilters()"><i class="icon-right mr8"></i>Clear</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        <!--Merge Duplicate contacts-->
        <div class="modal fade" id="mergeDuplicateContacts" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <form id="formMergeDuplicateContacts">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Merge Duplicate Contacts</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12 p0">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtEmail" type="text" class="validate" value="" disabled readonly>
                                    <label for="txtEmail">Email</label>
                                </div>
                            </div>
                            <div class="col-sm-12" id="fillDuplicatesContent">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn" onclick="saveDuplicateContacts(this)"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
            </form>
        </div>

        <!--Cold Calling  Modal-->
        <div class="modal fade" id="coldCalling" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <form id="ManageColdCallForm">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Cold Calling</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input id="txtColdCallContactName" type="text" class="validate" value=""  >
                                        <label for="txtColdCallContactName">Name <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input id="txtColdCallContactEmail" type="text" class="validate" value=""  >
                                        <label for="txtColdCallContactEmail">Email <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-6 pl0">
                                    <div class="input-field">
                                        <input id="txtColdCallContactPhone" type="text" class="validate" value="" disabled onkeypress="return isPhoneNumber(event)" maxlength="15" >
                                        <label for="txtColdCallContactPhone">Phone</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 pr0">
                                    <div class="input-field">
                                        <select id="txtColdCallContactCity" disabled>

                                        </select>
                                        <label class="select-label">City</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <select id="txtColdCallContactCountry" disabled>
                                        </select>
                                        <label class="select-label">Country</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtColdCallStatus">
                                        <option value="" selected>--Select--</option>
                                        <option value="switchoff">Switch Off</option>
                                        <option value="notlifted">Not Lifted</option>
                                        <option value="notexist">Number Not Exist</option>
                                        <option value="answered">Answered</option>
                                    </select>
                                    <label class="select-label">Call Status</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="multiple-radio" id="callStatusInfo">
                                <!--<span class="inline-radio">
                                    <input class="with-gap" name="txtColdcallStatusInfo" type="radio" id="callStatusInfoIntrested" value="callStatusInfoIntrested" checked />
                                    <label for="callStatusInfoIntrested">Interested</label>
                                </span>-->
                                <span class="inline-radio">
                                    <input class="with-gap" name="txtColdcallStatusInfo" type="radio" id="callStatusInfoIntrestedTransfer" value="callStatusInfoIntrestedTransfer" />
                                    <label for="callStatusInfoIntrestedTransfer">Interested</label>
                                </span>
                                 <span class="inline-radio">
                                    <input class="with-gap" name="txtColdcallStatusInfo" type="radio" id="callStatusInfoNotintrested" value="callStatusInfoNotintrested"  />
                                    <label for="callStatusInfoNotintrested">Not interested</label>
                                </span>
                                 <span class="inline-radio">
                                    <input class="with-gap" name="txtColdcallStatusInfo" type="radio" id="callStatusInfoDND"  value="callStatusInfoDND" />
                                    <label for="callStatusInfoDND">DND</label>
                                </span>
                                </div>
                            </div>
                            <div class="coldcall-info">
                                <div class="col-sm-12" id="coldcalling-branch">
                                    <div class="input-field">
                                        <select id="txtColdCallContactBranch">

                                        </select>
                                        <label class="select-label">Branch <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <select id="txtColdCallContactCourse">

                                        </select>
                                        <label class="select-label">Course <em>*</em></label>
                                    </div>
                                </div>

                                <!--<div class="col-sm-12" id="coldCallingContactTypeWrapper">

                                </div>-->

                                <div class="col-sm-12" id="coldCallingContactTypeWrapper" >

                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <select id="txtColdCallContactPostQualification">

                                        </select>
                                        <label class="select-label">Qualification Level <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <select id="txtColdCallContactQualification">

                                        </select>
                                        <label class="select-label">Qualification <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-12 mb10" id="contactType-corporate-wrap">
                                    <div class="input-field col-sm-11 p0">
                                        <select id="txtColdCallContactCompany" class="custom-select-nomargin">

                                        </select>
                                        <label class="select-label">Company <em>*</em></label>
                                    </div>

                                    <div class="col-sm-1 p0" access-element="add reference">
                                        <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetCompanyWrapper(this)"><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                    </div>
                                    <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ComapnyWrapper">
                                        <div class="col-sm-8 p0 ">
                                            <div class="input-field mt15">
                                                <input id="txtComapnyNameWrapper" type="text" class="validate" value="">
                                                <label for="txtComapnyNameWrapper">Company Name <em>*</em></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 p0 ">
                                            <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="addCompanyWrapper(this)"></span></a>
                                            <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 mb10" id="contactType-Institution-wrap">
                                    <div class="input-field col-sm-11 p0">
                                        <select id="txtColdCallContactInstitution" class="custom-select-nomargin">

                                        </select>
                                        <label class="select-label">Institution <em>*</em></label>
                                    </div>
                                    <div class="col-sm-1 p0" access-element="add reference">
                                        <a class="mt10 ml5 display-inline-block cursor-pointer showAddInstituteWrap-btn" onclick="resetInstitutionWrapper(this)"><span class="icon-plus-circle f24 mt20 diplay-inline-block"></span></a>
                                    </div>
                                    <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="InstitutionWrapper">
                                        <div class="col-sm-8 p0">
                                            <div class="input-field mt15">
                                                <input id="txtInstitutionNameWrapper" type="text" class="validate" value="">
                                                <label for="txtInstitutionNameWrapper">Institution Name <em>*</em></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 p0">
                                            <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="addInstitutionWrapper(this)"></span></a>
                                            <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-12 mt10">
                                <div class="input-field">
                                    <textarea id="ColdCallDescription" name="ColdCallDescription" class="materialize-textarea"></textarea>
                                    <label for="ColdCallDescription">Description <em>*</em></label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue-btn" onclick="saveContactColdCalling(this)"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>




            <!-- InstanceEndEditable --></div>
  </div>
<script type="text/javascript">
    docReady(function(){
        refreshContactsView();
    });

    function refreshContactsView(){
        buildContactsDataTable();
        getLeadCountsData();
        getCitiesListByCountry(0);
    }

    function buildContactsDataTable()
    {
        var ajaxurl=API_URL+'index.php/Lead/getContactsList';
        var userID=$('#hdnUserId').val();
        var action='list';
        var params={appliedFiltersString:appliedFiltersString,appliedCityFilters:appliedCityFilters};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#contacts-list").dataTable().fnDestroy();
        $tableobj=$('#contacts-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
                $("#checkUncheck").prop('checked', false);
                resetTagOutStationContacts();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No contacts found",
                "sZeroRecords": "No contacts found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    data: null, render: function ( data, type, row ) {
                    //return data.name;
                    var leadsData = '';
                    if(data.is_lead==1){
                        leadsData += '<p class="popper" data-toggle="popover" data-placement="top" data-title="Lead Details" data-trigger="hover" data-title="Lead Details">';
                        leadsData+='<input type="checkbox" class="checkbox-view" name="" value="'+data.lead_id+'" disabled >';
                        if(data.lead_stage == 'M7')
                        {
                            leadsData += '<i class="fa fa-user mr5 valign-middle dark-sky-blue f16"></i>';
                        }
                        else
                        {
                            leadsData += '<i class="fa fa-user mr5 valign-middle ash f16"></i>';
                        }

                        leadsData += data.name;
                        leadsData += '</p>';
                        leadsData += '<div class="popper-content hide"><div class="plr15">';
                        leadsData += '<p><label class="popover-label ash f14">Branch :</label>'+data.branch_name+'</p>';
                        leadsData += '<p><label class="popover-label ash f14">Course :</label>'+data.course_name+'</p>';
                        leadsData += '<p><label class="popover-label ash f14">Lead Stage :</label>'+data.lead_stage+'</p>';
                        leadsData += '</div></div>';
                    }
                    else if(data.is_scheduled==1) {
                        leadsData += "<p class=\"popper\" data-toggle=\"popover\" data-placement=\"top\" data-trigger=\"hover\" data-title=\"Cold Calling\">";
                        leadsData+='<input type="checkbox" class="checkbox-view" name="txtCheckBoxContact" value="'+data.lead_id+'">';
                        leadsData += "<a href=\"javascript:;\" data-target=\"#coldCalling\" data-toggle=\"modal\" onclick=\"contactManageColdCalling(this,'"+data.lead_id+"')\">";
                        leadsData += "<i class=\"fa fa-phone green mr5 valign-middle f16\"></i>";
                        leadsData += "</a>";
                        leadsData += data.name;
                        leadsData += "</p>";
                        leadsData += "<div class=\"popper-content hide\"><div class=\"plr15\">";
                        leadsData += "<p>";
                        leadsData += "<label class=\"popover-label ash f14\">User Name :</label>";
                        leadsData += data.name;
                        leadsData += "</p>";
                        leadsData += "<p>";
                        leadsData += " <label class=\"popover-label ash f14\">Branch :</label>";
                        leadsData += "----";
                        leadsData += "</p>";
                        leadsData += "</div></div>";
                    }
                    else if(data.is_dnd==1 && data.is_duplicate == 0)
                    {
                        leadsData+='<p>';
                        leadsData+='<input type="checkbox" class="checkbox-view" name="txtCheckBoxContact" value="'+data.lead_id+'">';
                        leadsData += '<i class="fa fa-ban red mr5 valign-middle f16"></i>'+data.name+'';
                        leadsData+='</p>';
                    }
                    else if(data.is_duplicate==0) {

                        leadsData+='<input type="checkbox" class="checkbox-view" name="txtCheckBoxContact" value="'+data.lead_id+'">';
                            leadsData += data.name;

                        
                    }
                    else if(data.is_duplicate==1)
                    {
                        leadsData+='<p>';
                        leadsData+='<input type="checkbox" class="checkbox-view" name="txtCheckBoxContact" value="'+data.lead_id+'">';
                        if(data.is_dnd==1)
                            leadsData += '<i class="fa fa-ban red mr5 valign-middle f16"></i>';
                        leadsData += "<a access-element='merge' class=\"tooltipped\" data-position=\"top\" data-tooltip=\"Merge Duplicate Contacts\" href=\"javascript:;\" onclick=\"populateContactDuplicates(this,'" + data.lead_id + "')\"><i class=\"fa fa-users f14 orange mr5 valign-middle\"></i></a>"+data.name+"";
                        leadsData+='</p>';
                    }
                    return leadsData;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.email;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.phone;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var contactData='';
                    contactData+=data.city;
                    if(data.branch_code && data.branch_code!=null && data.branch_code.trim()!='') {
                        contactData += '<span class="display-block">' + data.branch_code + '<i class="ml5 fa fa-tag"></i></span>';
                    }
                    return contactData;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        //return data.email;
                        var leadTagsData='';
                        leadTagsData+='<p class="max-width130px label-tags ellipsis popper" data-toggle="popover" data-title="Tags" data-placement="top" data-trigger="hover">';
                        var tagsList=data.tags.split(',');
                        for(var i=0;i<2;i++)
                        {
                            if(tagsList[i] && i==0)
                            {
                                leadTagsData+='<label class="label label-success font-normal p5 f12 mr5">'+tagsList[i]+'</label>';
                            }
                            else if(tagsList[i] && i==1)
                            {
                                leadTagsData+='<label class="label label-info font-normal p5 f12 mr5">'+tagsList[i]+'</label>';
                            }
                        }
                        leadTagsData+='</p>';
                        leadTagsData+='<div class="popper-content hide"><div class="plr15">';
                        /*for(var i=0;i<tagsList.length;i++){
                            leadTagsData+='<p><label class="popover-label">'+tagsList[i]+'</label></p>';
                        }*/
                        leadTagsData+='<p><label class="popover-label ash f14">'+data.tags+'</label></p>';
                        leadTagsData+='</div></div>';
                        return leadTagsData;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        var actionHtml='';
                        actionHtml+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        actionHtml+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                        actionHtml+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        actionHtml+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        //actionHtml+='<li><a access-element="manage" href="javascript:;" onclick="viewContactDetails(this,\''+data.lead_id+'\')">Manage</a></li>';
                        actionHtml+='<li><a access-element="edit" href="javascript:;" onclick="ManageContact(this,\''+data.lead_id+'\')">Edit</a></li>';
                        actionHtml+='</ul></div>';
                        
                        //return '<div class="text-center"><a href="javascript:;" onclick="viewContactDetails(this,\''+data.lead_id+'\')" access-element="view"><i class="fa fa-eye"></i></a></div>';
                        
                        return actionHtml;
                        
                    }
                }
            ],
            "createdRow": function ( row, data, index )
            {

                if(data.status == 0)
                {
                    $(row).addClass('disabled-branchview');
                }
            }
        } );
        DTSearchOnKeyPressEnter();
//        alertify.dismissAll();  //for removing message when add/edit/filters are used
    }
    
    function getLeadCountsData(){
        var ajaxurl=API_URL+'index.php/Lead/getContactCounts';
        var userID=$('#hdnUserId').val();
        var action='list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response=
        commonAjaxCall({This:this, requestUrl:ajaxurl,headerParams:headerParams,action:action,onSuccess:renderDataToLeadCountsGrid});

    }
    function renderDataToLeadCountsGrid(response){
        var OverallCount=0;
        var UnscheduledCount=0;
        var DBLeadCount=0;
        var DBDNDCount=0;
        var EnrolledCount=0;
        if(response.status===true){
            if(response.data.length == 0)
            {

            }
            else{
            OverallCount=response.data.OverallCount;
            UnscheduledCount=response.data.UnscheduledCount;
            DBLeadCount=response.data.DBLeadCount;
            DBDNDCount=response.data.DBDNDCount;
            EnrolledCount=response.data.EnrolledCount;
            
            }
        }
        else{
        notify(response.message,'error',10);
        }
        
        $('#OverallCount').html(OverallCount);
        $('#UnscheduledCount').html(UnscheduledCount);
        $('#DBLeadCount').html(DBLeadCount);
        $('#DBDNDCount').html(DBDNDCount);
        $('#EnrolledCount').html(EnrolledCount);
            
    }
    function getContactsData(){
        var ajaxurl=API_URL+'index.php/Lead/getContacts';
        var userID=$('#hdnUserId').val();
        var action='list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response=
        commonAjaxCall({This:this, requestUrl:ajaxurl,headerParams:headerParams,action:action,onSuccess:renderDataToContactsGrid});
    }
    function renderDataToContactsGrid(response){
        var rowsHtml='';
        if(response.status===true){
            if(response.data.contacts.length == 0)
            {
                    // no data found
                    rowsHtml+="<tr><td colspan='8' align='center'><h3>No Data Found</h3></td></tr>";
                    
            }
            else{
                
                $.each(response.data.contacts, function( key, value ) {
                    rowsHtml+="<tr class=''>";
                    rowsHtml+="<td>";
                    
                    rowsHtml+="<p class=\"popper\" data-toggle=\"popover\" data-placement=\"top\" data-title=\"Lead Details\" data-trigger=\"hover\" data-title=\"Lead Details\">";
                    rowsHtml+="<input type=\"checkbox\" class=\"checkbox-view\" name=\"lead_check\">"
                    rowsHtml+="<i class=\"fa fa-user mr5 valign-middle ash f16\"></i>";
                    rowsHtml+=value.name;
                    rowsHtml+="</p>"
                    rowsHtml+="<div class=\"popper-content hide\">";
                    rowsHtml+="<p><label class=\"popover-label\">Branch :</label>----</p>";
                    rowsHtml+="<p><label class=\"popover-label\">Course :</label>----</p>";
                    rowsHtml+="<p><label class=\"popover-label\">Lead Stage :</label>----</p>";
                    rowsHtml+="</div>";
                    rowsHtml+="</td>";
                    rowsHtml+="<td>";
                    rowsHtml+="----";
                    rowsHtml+="</td>";
                    rowsHtml+="<td>";
                    rowsHtml+=value.email;
                    rowsHtml+="</td>";
                    rowsHtml+="<td>";
                    rowsHtml+=value.phone;
                    rowsHtml+="</td>";
                    rowsHtml+="<td>";
                    rowsHtml+=value.city;
                    rowsHtml+="</td>";
                    rowsHtml+="<td>";
                    rowsHtml+="----";
                    rowsHtml+="</td>";
                    rowsHtml+="<td>";
                    rowsHtml+="<p class=\"max-width130px label-tags ellipsis popper\" data-toggle=\"popover\" data-title=\"Tags\" data-placement=\"top\" data-trigger=\"hover\">";
                    for(var i=0;i<2;i++)
                    {
                        if(value.tags[i])
                        {
                            rowsHtml+="<label class=\"label label-success font-normal p5 f12\">"+value.tags[i].tagName+"</label>"; 
                            i=i+1;
                        }
                        if(value.tags[i])
                        {
                            rowsHtml+="<label class=\"label label-info font-normal p5 f12\">"+value.tags[i].tagName+"</label>"; 
                            i=i+1;
                        }
                        if(value.tags[i])
                        {
                            rowsHtml+="<label class=\"label label-danger font-normal p5 f12\">"+value.tags[i].tagName+"</label>";
                        }
                    }
                    rowsHtml+="</p>";
                    rowsHtml+="<div class=\"popper-content hide\">";
                    for(var i=0;i<value.tags.length;i++){
                        rowsHtml+="<p><label class=\"popover-label\">"+value.tags[i].tagName+"</label></p>";
                    }
                    rowsHtml+="</div>";
                    rowsHtml+="</td>";
                    rowsHtml+="<td class=\"text-center\">";
                    rowsHtml+="<span><a href=\"javascript:;\" onclick=\"viewContactDetails(this,'"+value.lead_id+"')\" access-element=\"view\"><i class=\"fa fa-eye\"></i></a></span>";
                    rowsHtml+="</td>";
                    rowsHtml+="</tr>"
                });
            }
                    $('#contacts_list tbody').html(rowsHtml);
                    buildpopover();
        }
        else{
            notify('something went wrong','error',10);
        }
    }
    function importContacts(This){
    
        var ajaxurl=API_URL+'index.php/Lead/BulkAddContact';
        var userID=$('#hdnUserId').val();
        var action='import';
        var fileUpload = document.getElementById("txtfilebrowser");
        
        var selectedFile = $("#txtfilebrowser").val();
        var extension = selectedFile.split('.');
        if (extension[1] != "csv") {
            $("#txtfilebrowser").focus();
            notify('Please choose a .csv file', 'error', 5);
            return;
        }
        else{
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        
    if (fileUpload.value != null) {
        var uploadFile = new FormData();
        var files = $("#txtfilebrowser").get(0).files;
        if (files.length > 0) {
            uploadFile.append("file", files[0]);
            //commonAjaxCall({This:This,method:'POST',requestUrl:ajaxurl,params:uploadFile,headerParams:headerParams,action:action,onSuccess:finalresp});
            
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            dataType: "json",
            data: uploadFile,
            contentType: false,
            processData: false,
            headers: headerParams,
            beforeSend:function(){
                $(This).attr("disabled","disabled");
                alertify.dismissAll();
                notify('Processing..', 'warning', 10);
            },success: function (response){
                $(This).removeAttr("disabled");
                finalresp(response);
            },error:function(response){
                alertify.dismissAll();
                if(response.responseJSON.message=="Access Token Expired"){
                    logout();
                }
                else if(response.responseJSON.message=="Invalid credentials"){
                    logout();
                }
                else{
                    notify('Something went wrong. Please try again', 'error', 10);
                }
                
            }
        });
        }
        else{
            $("#txtfilebrowser").parent().find('.file-path-wrapper').addClass("required");
            $("#txtfilebrowser").parent().find('.file-path-wrapper').after('<span class="required-msg">required field</span>');
        }
    }
    else{
    
        }
        }
    }
    function finalresp(response){
        if(response.status===true){
        
        var counts=response.data.Counts;
        $('#TotalUploadedContacts').html(counts.total_count);
        $('#UploadedContacts').html(counts.successCount);
        $('#FailedContacts').html(counts.failedCount);
        $('#DuplicateContacts').html(counts.duplicateCount);
        var records=response.data.Records;
        var rowsHtml="";
        $.each(records, function( key, value ) {
            rowsHtml+="<tr>";
            rowsHtml+="<td>"+value.row_number+"</td>";
            rowsHtml+="<td>"+value.name+"</td>";
            rowsHtml+="<td>";
            if(value.status==0){
                //pending
                rowsHtml+="<span class='label f11 status-label font-normal label-warning'>Pending</span>";
            }
            if(value.status==1){
                //success
                rowsHtml+="<span class='label f11 status-label font-normal label-success'>Success</span>";
                value.error_log='Add/Update Contact Successfully';
            }
            if(value.status==2){
                //fail
                if(value.error_log=='Contact already exists.'){
                    rowsHtml+="<span class='label f11 status-label font-normal label-warning'>Duplicate</span>";
                }
                else {
                    rowsHtml += "<span class='label f11 font-normal status-label label-danger'>Failed</span>";
                }
            }
            
            rowsHtml+="<span class='text-success display-inline-block valign-middle w150 ellipsis' title='"+value.error_log+"'>"+value.error_log+"</span>";
            rowsHtml+="</td>";
            rowsHtml+="</tr>";
        });
        $('#uploadedContactsList tbody').html(rowsHtml);
            $("#txtfilebrowser").val('');
            $("#txtfilebrowser").trigger('change');
            alertify.dismissAll();
            notify("Uploaded Successfully","success",10);
            buildContactsDataTable();
            getLeadCountsData();
        }
        else{
            alertify.dismissAll();
            notify(response.message,"error",10);
        }
    }
    function resetImport(This){
    
        $('#importForm')[0].reset();
        $('#TotalUploadedContacts').html('-');
        $('#UploadedContacts').html('-');
        $('#FailedContacts').html('-');
        $('#DuplicateContacts').html('-');
        var rowsHtml="<tr><td colspan='3' align='center'>No Data Found</td></tr>";
        $('#uploadedContactsList tbody').html(rowsHtml);
        $(This).attr("data-target","#importModal");
        $(This).attr("data-toggle","modal");
    }

    var appliedFiltersString='',appliedCityFilters=[];
    function applyContactFilters(){
        notify('Processing..', 'warning', 10);
        var appliedFilters=[];
        $('input[name="filtersContact"]:checked').each(function()
        {
            appliedFilters.push(this.value)
        });
        appliedFiltersString=appliedFilters.join(',');
        //appliedCityFilters
        var selMulti = $.map($("#ddlContactCity option:selected"), function (el, i)
        {
            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All') {
                return $(el).val();
            }
        });
        appliedCityFilters = selMulti.join(",");
        buildContactsDataTable();
        $('#filters').modal('hide');
        alertify.dismissAll();
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }
    function resetContactFilters(){
        $('input[name="filtersContact"]').attr('checked',false);
        $("#ddlContactCity").empty().trigger('chosen:updated');
        getCitiesListByCountry(0);
        applyContactFilters();
    }
    function contactManageColdCalling(This,Id){
        getColdCallBranches();
        getColdCallPostQualifications();
        //getColdCallQualifications();
        getColdCallCompanies();
        getColdCallInstitutions();
        getColdCallContactTypes();
        $('#callStatusInfoIntrestedTransfer').click();
        ManageColdCall(This,Id);
    }
    function saveContactColdCalling(This) {

        $('#ManageColdCallForm input').removeClass("required");
        $('#ManageColdCallForm select').removeClass("required");
        $('#ManageColdCallForm textarea').removeClass("required");
        $('#ManageColdCallForm span.required-msg').remove();
        var leadId = $("#hdnLeadId").val();
        var userID = $("#hdnUserId").val();

        var regx_txtContactName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
        var regx_txtContactEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var ColdCallContactName = $("#txtColdCallContactName").val();
        var ColdCallContactEmail = $("#txtColdCallContactEmail").val();
        var ColdCallStatus = $("#txtColdCallStatus option:selected").val();
        var ColdcallStatusInfo = $("input[name=txtColdcallStatusInfo]:checked").val();
        var ColdCallContactCourse = $("#txtColdCallContactCourse option:selected").val();
        var ColdCallContactBranch = $("#txtColdCallContactBranch option:selected").val();
        if (ColdcallStatusInfo == 'callStatusInfoIntrested') {
            var ColdCallContactBranch = $("#userBranchList option:selected").val();
        }

        var coldCallcontacttype = $("input[name=coldCallcontacttype]:checked").val();
        var ColdCallContactPostQualification = $("#txtColdCallContactPostQualification option:selected").val();
        var ColdCallContactQualification = $("#txtColdCallContactQualification option:selected").val();
        var ColdCallContactCompany = $("#txtColdCallContactCompany option:selected").val();
        var ColdCallContactInstitution = $("#txtColdCallContactInstitution option:selected").val();
        var ColdCallDescription = $('#ColdCallDescription').val();

        var flag = 0;
        if (ColdCallContactName == '') {
            $("#txtColdCallContactName").addClass("required");
            $("#txtColdCallContactName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtContactName.test($('#txtColdCallContactName').val()) === false) {
            $("#txtColdCallContactName").addClass("required");
            $("#txtColdCallContactName").after('<span class="required-msg">Invalid Name</span>');
            flag = 1;
        }
        if (ColdCallContactEmail == '') {
            $("#txtColdCallContactEmail").addClass("required");
            $("#txtColdCallContactEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_txtContactEmail.test($('#txtColdCallContactEmail').val()) === false) {
            $("#txtColdCallContactEmail").addClass("required");
            $("#txtColdCallContactEmail").after('<span class="required-msg">Invalid E-mail</span>');
            flag = 1;
        }
        if (ColdCallStatus == '') {

            $("#txtColdCallStatus").parent().find('.select-dropdown').addClass("required");
            $("#txtColdCallStatus").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;

        } else {

            if (ColdCallStatus == 'answered') {



                if (ColdcallStatusInfo == 'callStatusInfoIntrested' || ColdcallStatusInfo == 'callStatusInfoIntrestedTransfer') {

                    if (ColdCallContactCourse == '') {

                        $("#txtColdCallContactCourse").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactCourse").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;

                    }
                    if (ColdcallStatusInfo == 'callStatusInfoIntrestedTransfer' && ColdCallContactBranch == '') {
                        $("#txtColdCallContactBranch").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactBranch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }

                    if (ColdCallContactPostQualification == '') {
                        $("#txtColdCallContactPostQualification").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactPostQualification").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }

                    if (ColdCallContactQualification == '') {
                        $("#txtColdCallContactQualification").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactQualification").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }

                    if (coldCallcontacttype == 'Corporate' && ColdCallContactCompany == '') {
                        $("#txtColdCallContactCompany").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactCompany").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }

                    if (coldCallcontacttype == 'University' && ColdCallContactInstitution == '') {
                        $("#txtColdCallContactInstitution").parent().find('.select-dropdown').addClass("required");
                        $("#txtColdCallContactInstitution").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                        flag = 1;
                    }

                } else if (ColdcallStatusInfo == 'callStatusInfoNotintrested') {



                } else {

                }


            } else {

            }


        }
        if (ColdCallDescription == '') {
            $("#ColdCallDescription").addClass("required");
            $("#ColdCallDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (!(/^([a-zA-Z ])/.test(ColdCallDescription)))
        {
            $("#ColdCallDescription").addClass("required");
            $("#ColdCallDescription").after('<span class="required-msg">Invalid Description</span>');
            flag = 1;
        }
        if (flag == 0) {

            var ajaxurl = API_URL + 'index.php/Lead/saveColdCall';

            var params = {leadId: leadId, ColdCallContactName: ColdCallContactName, ColdCallContactEmail: ColdCallContactEmail, ColdCallStatus: ColdCallStatus, ColdcallStatusInfo: ColdcallStatusInfo, ColdCallContactCourse: ColdCallContactCourse, ColdCallContactBranch: ColdCallContactBranch, coldCallcontacttype: coldCallcontacttype, ColdCallContactQualification: ColdCallContactQualification, ColdCallContactCompany: ColdCallContactCompany, ColdCallContactInstitution: ColdCallContactInstitution, ColdCallDescription: ColdCallDescription};
            var action = 'status';
            var type = "POST";
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            var response =
                commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: saveContactColdCallingResponse});

        }


    }
    function saveContactColdCallingResponse(response) {

        alertify.dismissAll();
        if (response.status === true) {
            refreshContactsView();
            notify(response.message, 'success', 10);
            $('#coldCalling').modal('hide');
            $('body').find('#coldCalling').removeClass('modal-open');
            $('.modal-backdrop').remove();

        } else {
            notify(response.message, 'error', 10);
        }
    }
    </script>
