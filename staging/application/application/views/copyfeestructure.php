<?php 
$Id?$Id:0;
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="feeId" value="0" readonly />
    <input type="hidden" id="feeTypeId" value="<?php echo $Id ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Fee Structure Copy</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="tooltipped" data-position="left" data-tooltip="Close" href="<?php echo BASE_URL; ?>app/feestructure"><i class="icon-times"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" access-element="add"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <form id="feeStructure" >
                <div class="col-sm-12 p0">
                    <div class="col-sm-4 pl0">
                        <div class="input-field">
                            <input id="name" type="text" class="validate">
                            <label for="name">Name <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-4 pl0">
                        <div class="input-field">
                            <label class="datepicker-label datepicker">Effective Date <em>*</em></label>
                            <input id="effective" type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                    <div class="col-sm-4 pl0">
                        <div class="input-field">
                            <select multiple id="branch" name="branch" class="validate" class="formSubmit">
                                <option value="" >--Select All--</option>
                            </select>
                            <label class="select-label">Branch</label>
                        </div>
                    </div>
                    <div class="col-sm-4 pl0">
                        <div class="input-field">
                            <select id="course">
                                <option value="" selected>--select--</option>
                            </select>
                            <label class="select-label">Course <em>*</em></label>
                        </div>
                    </div>

                </div>
            
            <div class="col-sm-12 p0">
                <h4 class="heading-uppercase">Include <em class="red">*</em></h4>
                <div class="col-sm-12 pl0" style="display:none">
                    <div class="multiple-checkbox">
                        <span class="inline-checkbox">
                            <input type="checkbox" class="filled-in" id="class-room" />
                            <label for="class-room">Class Room Training</label>
                        </span>
                        <span class="inline-checkbox">
                            <input type="checkbox" class="filled-in" id="onlineC" />
                            <label for="onlineC">Online Training</label>
                        </span>
                    </div>
                </div>
                <div class="col-sm-12 pl0" >
                    <div class="multiple-checkbox">
                        <span class="inline-checkbox">
                            <input type="checkbox" class="filled-in" id="partial-payment" />
                            <label for="partial-payment">Partial Payment Allow</label>
                        </span>
                        <span class="inline-checkbox">
                            <input type="checkbox" class="filled-in" id="regular-fee" />
                            <label for="regular-fee">Regular Fee</label>
                        </span>
                    </div>
                </div>
                <div class="col-sm-12 p0" style="/*display:none*/" >
                    <div class="col-sm-4 pl0">
                        <div class="input-field">
                            <select id="Trainingtype">
                                <option value="" selected>--select--</option>
                            </select>
                            <label class="select-label">Training Type <em>*</em></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                    <div class="col-sm-4 pl0">
                        <div class="input-field">
                            <select id="Feetype">
                                <option value="" disabled selected>--select--</option>
                            </select>
                            <label class="select-label">Fee Type <em>*</em></label>
                        </div>
                    </div>
                    <div class="col-sm-4 pl0 feeStructureCreate-companyname">
                        <div class=" col-sm-11 p0">
                            <div class="input-field">
                                <select id="corporate" name="corporate" class="validate custom-select-nomargin formSubmit">
                                    <option value="default" selected>---Select---</option>
                                </select>
                                <label class="select-label">Corporate Company <em>*</em></label>
                            </div>
                        </div>
                        <div class="col-sm-1 p0">
                            <a data-position="right" data-tooltip="Add Company" class="tooltipped mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn"><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                        </div>
                        <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap">
                            <div class="col-sm-8 p0 ">
                                <div class="input-field mt15">
                                    <input id="addC_name" type="text" class="validate" value="">
                                    <label for="addC_name">Corporate Company  Name</label>
                                </div>
                            </div>
                            <div class="col-sm-4 p0 ">
                                <a onclick="feeStructureCreateFeeTypeValue(this)" class="mt20 pl5 ml5 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green"></span></a>
                                <a onclick="feeStructureHideFeeTypeValue()" class="mt20 pl5 ml5 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 p0 feeStructureCreate-groupname">
                        <div class="col-sm-11 p0">
                            <div class="input-field">
                                <select id="groupvalue" name="groupvalue" class="validate formSubmit custom-select-nomargin">
                                    <option value="default" selected>---Select---</option>
                                </select>
                                <label class="select-label">Institute Name <em>*</em></label>
                            </div>
                        </div>
                        <div class="col-sm-1 p0">
                            <a data-position="right" data-tooltip="Add Institute" class="tooltipped mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn"><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                        </div>
                        <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap">
                            <div class="col-sm-8 p0 ">
                                <div class="input-field mt15">
                                    <input id="addI_name" type="text" class="validate" value="">
                                    <label for="addI_name">Institute Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-4 p0 ">
                                <a onclick="feeStructureCreateFeeTypeValue(this)" class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green"></span></a>
                                <a onclick="feeStructureHideFeeTypeValue()" class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                            </div>
                        </div>
                    </div>

                </div>
                

                
                <div class="col-sm-12 p0">
                    <div class="col-sm-4 pl0">
                        <div class="input-field">
                            <textarea id="decription" class="materialize-textarea"></textarea>
                            <label for="decription">Description</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p0">
                <h4 class="heading-uppercase">Items</h4>
                <table id="fee-item-list" class="table table-responsive table-striped table-custom table-info pb200">
                    <thead>
                        <tr>
                            <th class="w200">Fee Head</th>
                            <th>Company</th>
                            <th class="w150 text-right">Due Day</th>
                            <th class="w150 text-right border-r-none">Amount</th>
                            <th class="w25 text-right"></th>
                        </tr>
                        <tr><td class="border-none p0 lh10">&nbsp;</td></tr>
                    </thead>
                    <tbody>
                        <tr class="td-nobg">
                            <td colspan="2 pl0">
                                <div class="col-sm-12 p0 -ml8">
                                    <div class="col-sm-4 pl0">
                                        <div class="input-field m0">
                                            <select id="fee-head">
                                                <option value="" selected>--select--</option>
                                            </select>
                                            <label class="select-label">Fee Head</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 pl0">
                                        <a class="mt8 display-block" href="javascript:;" onclick="addFeeHead('', '')"><i class="icon-plus-circle f26"></i></a>
                                    </div>
                                </div>
                            </td>
                            <td class="w150 text-right"><span class="display-block">Due Days</span><span class="display-block font-bold  f14" id="days"></span></td>
                            <td class="w150 text-right"><span class="display-block">Total Fee</span><span class="display-block  font-bold f14" id="total"></span></td>
                        </tr>
                    </tbody>
                </table>
                <button onclick="copyFeeStructure(this)" style="margin-top:-430px;" class="btn blue-btn"><i class="icon-right mr8"></i>Save</button>
            </div>
            </form>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    $context = 'Fee Structure';
    $pageUrl = 'feestructure';
    $serviceurl = 'feestructure';
    docReady(function () {
        loadFeeStructure();
        $('form').attr('onsubmit', 'return false');
    });
    $('#branch').change(function () {
        multiSelectDropdown($(this));

    });
    function loadFeeStructure()
    {
        var userId = $('#userId').val();
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        $.when(
            commonAjaxCall
            (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Branch/getBranchList',
                        action: action
                    }
            ),
            commonAjaxCall
            (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Courses/getAllCourseList',
                        action: action
                    }
            ),
            commonAjaxCall
            (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name: 'Company'},
                        action: action
                    }
            ),
            commonAjaxCall
            (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/FeeHead/getFeeHeadList',
                        action: action
                    }
            ),
            commonAjaxCall
            (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name: 'Institution'},
                        action: action
                    }
            ),
            commonAjaxCall
            (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name: 'Fee Type'},
                        action: action
                    }
            ),
            commonAjaxCall
            (
                {
                    This: this,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                    params: {name: 'Training Type'},
                    action: action
                }
            )
        ).then(function (response1, response2, response3, response4, response5, response6, response7) {
            var branch = response1[0];
            var course = response2[0];
            var corporate = response3[0];
            var feehead = response4[0];
            var groupData = response5[0];
            var feeTypeData = response6[0];
            var trainingTypeData = response7[0];
            /*Set Date as current, for edit and add functions*/
            setDatePicker('#effective', '0000-00-00');

            branchDropDown(branch);
            $('#branch').material_select();

            selectOption = 1;
            branchCourse(course);
            trainingTypeDropDown(trainingTypeData);
            feeTypeDropDown(feeTypeData);

            corporateDropDown(corporate);

            feeHeadDropDown(feehead);

            groupDropDown(groupData);
            
            var id = $('#feeTypeId').val();
            if(id > 0){
                var userId = $('#userId').val();
                var action = 'edit';
                var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/FeeStructure/getSingleFeeStructure',
                        params: {'fee_id': id},
                        action: action,
                        onSuccess: buidFeeStructureData
                    }
                )
            }
        });
    }
    
</script>
