<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 10/5/16
 * Time: 6:23 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">Daily Revenue Collection</h3>
        <div class="content-header-btnwrap" access-element="Report">
            <ul>
                <li>
                    <a href="javascript:" class="" onclick="filterDailyRevenueCollectionReportExport()" ><i class="fa fa-file-excel-o"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="content-body" id="contentBody">
        <div class="fixed-wrap clearfix">
            <div class="col-sm-6 reports-filter-wrapper pl0">
                <div class="multiple-checkbox reports-filters-check">
                    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
                    <label class="heading-uppercase">Company</label>
                    <span class="filter-selectall-wrap">
                        <a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:;"><i class="icon-right green"></i></a>
                        <a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:;"><i class="icon-times red"></i></a>
                    </span>
                </div>
                <div class="boxed-tags" id="feeCompanyList">

                </div>
            </div>
            <div class="col-sm-6 reports-filter-wrapper pl0">
                <div class="multiple-checkbox reports-filters-check">
                    <label class="heading-uppercase">Mode of Payment</label>
                </div>
                <div class="boxed-tags radio-tags" id="feePaymentType">
                    <a data-value="cash" class="active">Cash</a>
                    <a data-value="other">Other</a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6 reports-filter-wrapper pl0">
                <div class="multiple-checkbox reports-filters-check mb5"></div>
                <div class="col-sm-4 mt0 p0">
                    <div class="input-field">
                        <input id="txtRevenuePostDate" type="date" placeholder="dd/mm/yyyy" class="datepicker relative enablePastDatesWithCurrentDate" />
                        <label for="txtRevenuePostDate">Date</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 pl0 mt15">
                <a class="btn blue-btn btn-sm" hrhref="javascript:;" onclick="getRevenueCollection()">Proceed</a>
            </div>

            <div class="col-sm-12 p0" id="revenueCollectionDetails">

            </div>
            <div class="col-sm-12 clearfix p0">
                <h4 class="mb30 heading-uppercase">Signatures</h4>
                <div class="col-sm-4 mb30 pl0 ash">Accountant / Cashier</div>
                <div class=" col-sm-4 ash">Branch Head</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
docReady(function(){
    getFeeCompanies();
});
var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
var selectedPaymentTypeText=$("#feePaymentType > .active").text();
function getFeeCompanies()
{
    var userId = $('#hdnUserId').val();
    var action = 'list';
    var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
    var ajaxurl = API_URL + 'index.php/Company/getFeeCompanyList';
    commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess:feeCompaniesResponse });
}
function feeCompaniesResponse(response)
{
    var feeCompanies='';
    for(var i=0;i<response['data'].length;i++)
    {
        if(i==0)
            feeCompanies+='<a data-id="'+response['data'][i]['company_id']+'" class="active">';
        else
            feeCompanies+='<a data-id="'+response['data'][i]['company_id']+'">';
        feeCompanies+=response['data'][i]['name'];
        feeCompanies+='</a>';
    }
    $("#feeCompanyList").html(feeCompanies);
}
function getRevenueCollection()
{
    alertify.dismissAll();
    var userId = $('#hdnUserId').val();
    var selectedFeeCompany = [];
    $("#feeCompanyList > .active").each(function()
    {
        selectedFeeCompany.push($(this).attr('data-id'));
    });
    selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
    var selectedRevenueDate=$("#txtRevenuePostDate").val();
    if(selectedFeeCompany.length == 0)
    {
        notify('Select atleast one company','error');
        return false;
    }
    else if(selectedPaymentType == '' || selectedPaymentType == undefined)
    {
        notify('Select any one payment type','error');
        return false;
    }
    else
    {
        var action = 'list';
        var selectedBranch = $("#userBranchList").val();
        var params = {branchId:selectedBranch,companyId:selectedFeeCompany,PaymentMode:selectedPaymentType,selectedDate:selectedRevenueDate};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/DailyRevenue/getRevenueCollectionDetails';
        commonAjaxCall({This: this, headerParams: headerParams, params:params,requestUrl: ajaxurl, action: action, onSuccess:revenueCollectionResponse});
    }
}
function revenueCollectionResponse(response)
{
    var response_data='';
    if(response['status']==true)
    {
        var company_name='',cash_in_hand= 0,other_in_hand=0;
        var revenueResponse=response['data'];
        for(var rc=0;rc<Object.keys(revenueResponse).length;rc++)
        {

            company_name=Object.keys(revenueResponse)[rc];
            response_data+='<div class="col-sm-4 clearfix pl0">';
            response_data+='<h5 class="text-center bblue white-color p5 m0 text-uppercase">'+company_name+'</h5>';
            response_data+='<table class="table table-responsive table-custom adhoc-attendance pb10">';
            response_data+='<thead>';
            response_data+='<tr>';
            response_data+='<th class="text-left">Particulers</th>';
            response_data+='<th class="text-right">Amount</th>';
            response_data+='</tr>';
            response_data+='<tr>';
            response_data+='<td class="border-none p0 lh10">&nbsp;</td>';
            response_data+='</tr>';
            response_data+='</thead>';
            response_data+='<tbody>';
            response_data+='<tr>';
            response_data+='<td colspan="2" class="text-left bblue font-bold f14">Today Fee Head wise Collection</td>';
            response_data+='</tr>';
            if(revenueResponse[company_name]['collection'].length > 0)
            {
                response_data+='<tr>';
                response_data+='<td colspan="2" class="p0" style="border-bottom:none !important">';
                response_data+='<div class="table-scroll">';
                response_data+='<table class="table table-responsive table-custom adhoc-attendance m0">';
                response_data+='<tbody>';
                for(var cl=0;cl<revenueResponse[company_name]['collection'].length;cl++)
                {
                    response_data+='<tr>';
                    response_data+='<td>'+revenueResponse[company_name]['collection'][cl]['name']+'</td>';
                    response_data+='<td class="text-right">'+moneyFormat(revenueResponse[company_name]['collection'][cl]['amount_paid'])+'</td>';
                    response_data+='</tr>';
                }
                response_data+='</tbody>';
                response_data+='</table>';
                response_data+='</div>';
                response_data+='</td>';
                response_data+='</tr>';
                response_data+='<tr>';
                response_data+='<td>Total</td>';
                response_data+='<td class="text-right">'+moneyFormat(revenueResponse[company_name]['total_collection'])+'</td>';
                response_data+='</tr>';
            }
            else
            {
                response_data+='<tr>';
                response_data+='<td colspan="2">No amount collected for the day</td>';
                response_data+='</tr>';
            }

            response_data+='<tr>';
            response_data+='<td colspan="2" class="text-left bblue font-bold f14">Deposits</td>';
            response_data+='</tr>';


            if(revenueResponse[company_name]['bank_deposits'].length > 0)
            {
                for(var bd=0;bd<revenueResponse[company_name]['bank_deposits'].length;bd++)
                {
                    response_data+='<tr>';
                    response_data+='<td>';
                    response_data+='<span class="display-block p0">'+revenueResponse[company_name]['bank_deposits'][bd]['bank_name']+'</span>';
                    response_data+='<span class="p0"> A/C: '+revenueResponse[company_name]['bank_deposits'][bd]['acc_no']+'</span>';
                    response_data+='</td>';
                    response_data+='<td class="text-right">'+moneyFormat(revenueResponse[company_name]['bank_deposits'][bd]['deposited_amount'])+'</td>';
                    response_data+='</tr>';
                }
            }
            else
            {
                response_data+='<tr>';
                response_data+='<td colspan="2">';
                response_data+='<span class="display-block p0">No Deposits done for the day</span>';
                response_data+='</td>';
                response_data+='</tr>';
            }


            if(revenueResponse[company_name]['in_hand'].length > 0)
            {
                cash_in_hand=parseInt(revenueResponse[company_name]['in_hand'][0]['opening_balance']);
                other_in_hand=parseInt(revenueResponse[company_name]['in_hand'][0]['closing_balance']);
            }
            else
            {
                cash_in_hand=0;
                other_in_hand=0;
            }

            response_data+='<tr>';
            response_data+='<td colspan="2" class="text-left bblue font-bold f14">Cash in Hand</td>';
            response_data+='</tr>';
            if(cash_in_hand == 0 && other_in_hand ==0)
            {
                response_data+='<tr>';
                response_data+='<td colspan="2">No posting done for the day</td>';
                response_data+='</tr>';
            }
            else
            {
                response_data+='<tr>';
                response_data+='<td>Cash in Hand</td>';
                response_data+='<td class="text-right">'+moneyFormat(cash_in_hand)+'</td>';
                response_data+='</tr>';
                response_data+='<tr>';
                response_data+='<td>Other In Hand</td>';
                response_data+='<td class="text-right">'+moneyFormat(other_in_hand)+'</td>';
                response_data+='</tr>';
            }


            response_data+='</tbody>';
            response_data+='</table>';
            response_data+='</div>';
        }
        $("#revenueCollectionDetails").html(response_data);
    }
}


function filterDailyRevenueCollectionReportExport(This)
{
    alertify.dismissAll();
    var userId = $('#hdnUserId').val();
    var selectedFeeCompany = [];
    var selectedFeeCompanyText = [];
    $("#feeCompanyList > .active").each(function()
    {
        selectedFeeCompany.push($(this).attr('data-id'));
        selectedFeeCompanyText.push($(this).text());
    });
    var selectedPaymentType=$("#feePaymentType > .active").attr('data-value');
    var selectedPaymentTypeText=$("#feePaymentType > .active").text();
    var selectedRevenueDate=$("#txtRevenuePostDate").val();
    if(selectedFeeCompany.length == 0)
    {
        notify('Select atleast one company','error');
        return false;
    }
    else if(selectedPaymentType == '' || selectedPaymentType == undefined)
    {
        notify('Select any one payment type','error');
        return false;
    }
    else
    {
        selectedFeeCompanyText=selectedFeeCompanyText.join(',');
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var action = 'report';
        var selectedBranch = $("#userBranchList").val();
        var ajaxurl = API_URL + 'index.php/DailyRevenue/getFeeDetailsReportExport';
        var params = {branchId:selectedBranch,companyId:selectedFeeCompany,PaymentMode:selectedPaymentType,selectedDate:selectedRevenueDate,selectedFeeCompanyText:selectedFeeCompanyText,PaymentModeText:selectedPaymentTypeText};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: This, requestUrl: ajaxurl, method: 'GET', params:params, headerParams: headerParams, action: action, onSuccess: filterExcelReportExportResponse});
    }

}

function filterExcelReportExportResponse(response)
{
    alertify.dismissAll();
    if(response.status===true){
        var download=API_URL+'Download/downloadReport/'+response.file_name;
        window.location.href=(download);
    }
    else{
        notify(response.message,'error');
    }
}
</script>