<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Due List Report</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="report"><a class="" href="javascript:" onclick="javascript:filterDueListReportExport(this)"><i class="fa fa-file-excel-o"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
            <div class="col-sm-6 reports-filter-wrapper pl0">
                <div class="multiple-checkbox reports-filters-check">
                    <label class="heading-uppercase">Branch</label>
                    <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:" data-value="batch-filter"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:" data-value="batch-filter"><i class="icon-times red"></i></a></span>
                </div>
                <div class="boxed-tags" id="branchFilter"></div>
            </div>

            <div class="col-sm-6 reports-filter-wrapper">
                <div class="multiple-checkbox reports-filters-check">
                    <label class="heading-uppercase">Course</label>
                    <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:" data-value="batch-filter"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:" data-value="batch-filter"><i class="icon-times red"></i></a></span>
                </div>
                <div class="boxed-tags" id="courseFilter"></div>
            </div>
            </div>
            <div class="col-sm-12 p0">
                <!--<div class="col-sm-6 reports-filter-wrapper pl0">
                    <div class="input-field col-sm-11 p0 custom-multiselect-wrap col-sm-8">
                        <select multiple data-placeholder="Choose Batches" id="ddlBatchesList" class="chosen-select">
                            <option value="" disabled>--Choose Batch--</option>
                        </select>
                        <label class="select-label">Batches</label>
                    </div>
                </div>-->
                <div class="col-sm-6 reports-filter-wrapper p0">
                    <div>
                        <label class="heading-uppercase">Batch Dates</label>
                    </div>
                    <div class="input-field col-sm-4 mt0 pl0">
                        <div class="input-field mt0">
                            <label class="datepicker-label datepicker">From Date </label>
                            <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtFromDate">
                        </div>
                    </div>
                    <div class="input-field col-sm-4 mt0 pl0">
                        <div class="input-field mt0">
                            <label class="datepicker-label datepicker">To Date </label>
                            <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtToDate">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 reports-filter-wrapper mb5">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Company</label>
                        <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                    </div>
                    <div class="boxed-tags" id="companyFilter"></div>
                </div>
            </div>
            <div class="col-sm-12 p0">
            <div class="col-sm-6 reports-filter-wrapper pl0">
                <div class="multiple-checkbox reports-filters-check">
                    <label class="heading-uppercase">Fee Head</label>
                    <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:"><i class="icon-times red"></i></a></span>
                </div>
                <div class="boxed-tags" id="feeheadFilter"></div>
            </div>
            <div class="col-sm-6 reports-filter-wrapper">
                <div>
                    <label class="heading-uppercase">Due Till</label>
                </div>
                <div class="input-field col-sm-4 mt0 p0">
                    <label class="datepicker-label datepicker">Due Till </label>
                    <input type="date" class="datepicker relative enablePastDatesWithCurrentDate" placeholder="dd/mm/yyyy" id="txtDueFromDate">
                </div>
            </div>
                </div>

            <div class="col-sm-6 mt0 pl0">
                <a class="btn blue-btn btn-sm" href="javascript:" onclick="javascript:filterDueListReport(this)">Proceed</a>
                <a access-element='send email' class="btn blue-btn btn-sm" href="javascript:" onclick="javascript:sendEmailDueList(this)">Send E-Mail</a>
            </div>
            <div id="due-report-list-wrapper" access-element="report">
                <div class="col-sm-12 clearfix mt15 p0">
                    <table class="table table-inside table-custom branchwise-report-table" id="due-report-list">
                        <thead>
                        <tr class="">
                            <th>Branch ID</th>
                            <th>Enroll No.</th>
                            <th>Name</th>
                            <th>Batch</th>
                            <th>Company</th>
                            <th>Fee head</th>
                            <th class="text-right">Payable</th>
                            <th class="text-right">Paid</th>
                            <th class="text-right">Due From</th>
                        </tr>
                        </thead>

                    </table>
                </div>
                <div class="col-sm-12 clearfix p0">
                    <h4 class="mb30 heading-uppercase">Signatures</h4>
                    <div class="col-sm-4 mb30 pl0 ash">Accountant / Cashier</div>
                    <div class=" col-sm-4 ash">Branch Head</div>
                </div>
            </div>
        </div>

        <!-- InstanceEndEditable --></div>
</div>

<script>
    docReady(function () {
        $("#due-report-list-wrapper").hide();
        dueListReportPageLoad();
        enableDatePicker();
        /*var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        }

        if(mm<10) {
            mm='0'+mm
        }
        today = yyyy+'/'+mm+'/'+dd;
        if($('#txtDueFromDate').length>0) {

            setDatePicker('#txtDueFromDate', today);
        }*/
    });
    var dueListConfigResponse='';
    $(document).ready(function () {

        $('input[name="batch_curr_prev"]').click(function () {
            var selectedCheck=[];
            $('input[name="batch_curr_prev"]').each(function () {
                if ($(this).prop('checked')) {
                    selectedCheck.push($(this).val());
                }

            });
            var html = '';
            if ($.inArray('current', selectedCheck) != -1)
            {
                if(dueListConfigResponse.currentbatches.length > 0){
                    for(var a in dueListConfigResponse.currentbatches){
                        html += '<a class="active" data-id="'+dueListConfigResponse.currentbatches[a].batch_id+'" href="javascript:;">'+dueListConfigResponse.currentbatches[a].code+'</a>'
                    }

                }
            }
            if ($.inArray('previous', selectedCheck) != -1)
            {
                if(dueListConfigResponse.previousbatches.length > 0){
                    for(var a in dueListConfigResponse.previousbatches){
                        html += '<a class="active" data-id="'+dueListConfigResponse.previousbatches[a].batch_id+'" href="javascript:;">'+dueListConfigResponse.previousbatches[a].code+'</a>'
                    }

                }
            }
            $('#batchFilter').html(html);
        });
    });
    /* due list report starts here */
    function dueListReportPageLoad()
    {
        var userId = $('#userId').val();
        var action = 'report';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

        var ajaxurl=API_URL+'index.php/DueList/getAllConfigurations';
        var params = {};
        commonAjaxCall({This:this, headerParams:headerParams, requestUrl:ajaxurl,action:action,onSuccess:function(response){
            var dashboard = '';
            //return false;
            if (response.data.length == 0 || response == -1 || response['status'] == false) {
            } else {
                dueListConfigResponse=response.data;
                var RawData = response.data;
                var html = '';
                if(RawData.branch.length > 0){
                    for(var a in RawData.branch)
                    {
                        html += '<a class="active" data-id="'+RawData.branch[a].id+'" href="javascript:;" data-value="batch-filter">'+RawData.branch[a].name+'</a>'
                    }
                    $('#branchFilter').html(html);
                }
                html = '';
                if(RawData.course.length > 0){
                    for(var a in RawData.course){
                        html += '<a class="active" data-id="'+RawData.course[a].courseId+'" href="javascript:;" data-value="batch-filter">'+RawData.course[a].name+'</a>'
                    }
                    $('#courseFilter').html(html);
                }
                $('#ddlBatchesList').empty();
                var batchesList= $.merge(RawData.previousbatches,RawData.currentbatches);
                $('#ddlBatchesList').append($('<option></option>').val('').html('--Select All--'));
                if (batchesList.length > 0)
                {
                    $.each(batchesList, function (key, value)
                    {
                        $('#ddlBatchesList').append($('<option></option>').val(value.batch_id).html(value.code));
                    });
                }
                $("#ddlBatchesList").chosen({
                    no_results_text: "Oops, nothing found!",
                    width: "95%"
                });
                $('#ddlBatchesList').trigger("chosen:updated");
                $("#ddlBatchesList").next('label').addClass("active");

                html = '';
                if(RawData.company.length > 0){
                    for(var a in RawData.company){
                        html += '<a class="active" data-id="'+RawData.company[a].company_id+'" href="javascript:;">'+RawData.company[a].name+'</a>'
                    }
                    $('#companyFilter').html(html);
                }

                html = '';
                if(RawData.feehead.length > 0){
                    for(var a in RawData.feehead){
                        html += '<a class="active" data-id="'+RawData.feehead[a].fee_head_id+'" href="javascript:;">'+RawData.feehead[a].name+'</a>'
                    }
                    $('#feeheadFilter').html(html);
                }
                buildpopover();
            }
        }});
    }
    function filterDueListReport(This)
    {
        $("#txtFromDate").removeClass("required");
        $("#txtToDate").removeClass("required");
        $('#txtFromDate').parent().find('label').removeClass("active");
        $('#txtFromDate').parent().find('span.required-msg').remove();
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();
        var branchId = [];
        var courseId = [];
        var companyId = [];
        var feeheadId = [];
        var dueFromDate=$('#txtDueFromDate').val();
        /*var selMultiBatches = $.map($("#ddlBatchesList option:selected"), function (el, i)
        {
            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All')
            {
                return $(el).val();
            }
        });
        var chosenBatchIds = selMultiBatches.join(",");*/

        var error = true;
        $("#branchFilter > .active").each(function() {
            error = false;
            branchId.push($(this).attr('data-id'));
        });
        $("#courseFilter > .active").each(function() {
            error = false;
            courseId.push($(this).attr('data-id'));
        });
        $("#companyFilter > .active").each(function() {
            error = false;
            companyId.push($(this).attr('data-id'));
        });
        $("#feeheadFilter > .active").each(function() {
            error = false;
            feeheadId.push($(this).attr('data-id'));
        });

        var branchId=branchId.join(', ');
        var courseId=courseId.join(', ');
        var companyId=companyId.join(', ');
        var feeheadId=feeheadId.join(', ');
        var fromDate=$('#txtFromDate').val();
        var toDate=$('#txtToDate').val();
        if(fromDate.trim()!='') {
            var txtfromDate = dateConvertion(fromDate);
        }
        if(toDate.trim()!='') {
            var txttoDate = dateConvertion(toDate);
        }

        if(branchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one branch','error');
            return false;
        }
        else if(courseId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one course','error');
            return false;
        }
        else if($('#txtFromDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select from date','error');
            return false;
        }
        else if($('#txtToDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select to date','error');
            return false;

        }
        else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
            $("#txtFromDate").addClass("required");
            $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">Invalid</span>');
            alertify.dismissAll();
            notify('Invalid dates','error');
            error=true;
            return false;
        }
        else if(companyId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one company','error');
            return false;
        }
        else if(feeheadId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one fee head','error');
            return false;
        }
        else{
            error=false;
        }

        if(!error){
            var total=0;
            var pageTotal=0;

            var action = 'report';
            var ajaxurl=API_URL+'index.php/DueList/getDueListReport';
            //var params = {'branch': branchId, 'course': courseId, 'batch': chosenBatchIds,company:companyId,feehead:feeheadId,dueFromDate:dueFromDate};
            var params = {branch: branchId, course: courseId, fromDate:fromDate,toDate:toDate,company:companyId,feehead:feeheadId,dueFromDate:dueFromDate};
            var userID=$('#userId').val();
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            $("#due-report-list-wrapper").show();
            $("#due-report-list").dataTable().fnDestroy();
            $tableobj = $('#due-report-list').DataTable( {
                "fnDrawCallback": function() {
                    $("#due-report-list thead th").removeClass("icon-rupee");
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                    }
                    verifyAccess();
                    buildpopover();
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": false,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No Records Found",
                    "sZeroRecords": "No Records Found",
                    "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                },
                "bProcessing": true,
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    data:params,
                    headers:headerParams,
                    error:function(response) {
                        DTResponseerror(response);
                    }
                },
                columns: [
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.branchName;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.enrollNo;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.leadName;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.batchCode;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.companyName;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.feeheadName;
                    }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        return moneyFormat(data.amountPayable);
                    }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        return moneyFormat(data.amountPaid);
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        var dueDayhtml='';
                        var dueDays=data.dueDays;
                        if(dueDays==0){
                            dueDayhtml+="<div class=\"text-right rprt-nodue\">";
                            dueDayhtml+=Math.abs(dueDays)+' Days';
                            dueDayhtml+="</div>";
                        }
                        else if(dueDays<0){
                            dueDayhtml+="<div class=\"text-right rprt-due\">";
                            dueDayhtml+=Math.abs(dueDays)+' Days <span class="f10"><i class="icon-arrow-down"></i></span>';
                            dueDayhtml+="</div>";
                        }
                        else if(dueDays>0){
                            dueDayhtml+="<div class=\"text-right rprt-todaydue\">";
                            dueDayhtml+=Math.abs(dueDays)+' Days <span class="f10"><i class="icon-arrow-up"></i></span>';
                            dueDayhtml+="</div>";
                        }
                        return dueDayhtml;
                    }
                    }

                ]
            } );
            DTSearchOnKeyPressEnter();

        } else {
            alertify.dismissAll();
            notify('Select any filter', 'error', 10);
        }
    }
    function filterDueListReportExport(This){
        $("#txtFromDate").removeClass("required");
        $("#txtToDate").removeClass("required");
        $('#txtFromDate').parent().find('label').removeClass("active");
        $('#txtFromDate').parent().find('span.required-msg').remove();
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();

        var branchId = [];
        var branchIdText = [];
        var courseId = [];
        var courseIdText = [];
        var companyId = [];
        var companyIdText = [];
        var feeheadId=[];
        var feeheadIdText=[];
        var dueFromDate=$('#txtDueFromDate').val();
        var error = true;
        $("#branchFilter > .active").each(function() {
            error = false;
            branchId.push($(this).attr('data-id'));
            branchIdText.push($(this).text());
        });
        $("#courseFilter > .active").each(function() {
            error = false;
            courseId.push($(this).attr('data-id'));
            courseIdText.push($(this).text());
        });

        $("#companyFilter > .active").each(function() {
            error = false;
            companyId.push($(this).attr('data-id'));
            companyIdText.push($(this).text());
        });
        $("#feeheadFilter > .active").each(function() {
            error = false;
            feeheadId.push($(this).attr('data-id'));
            feeheadIdText.push($(this).text());
        });
        /*var selMultiBatchIds = $.map($("#ddlBatchesList option:selected"), function (el, i)
        {
            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All')
            {
                return $(el).val();
            }
        });
        var selMultiBatchNames = $.map($("#ddlBatchesList option:selected"), function (el, i)
        {
            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'All')
            {
                return $(el).text();
            }
        });
        var chosenBatchIds = selMultiBatchIds.join(",");
        var chosenbatchNames = selMultiBatchNames.join(",");*/

        var branchId=branchId.join(', ');
        var courseId=courseId.join(', ');
        var companyId=companyId.join(', ');
        var feeheadId=feeheadId.join(', ');
        var fromDate=$('#txtFromDate').val();
        var toDate=$('#txtToDate').val();
        if(fromDate.trim()!='') {
            var txtfromDate = dateConvertion(fromDate);
        }
        if(toDate.trim()!='') {
            var txttoDate = dateConvertion(toDate);
        }
        /*
        else if(chosenBatchIds.length==0){
            error=true;
            alertify.dismissAll();
            notify('Select any one batch','error');
            return false;
        }*/
        if(branchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one branch','error');
            return false;
        }
        else if(courseId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one course','error');
            return false;
        }
        else if($('#txtFromDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select from date','error');
            return false;
        }
        else if($('#txtToDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select to date','error');
            return false;

        }
        else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
            $("#txtFromDate").addClass("required");
            $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">Invalid</span>');
            alertify.dismissAll();
            notify('Invalid dates','error');
            error=true;
            return false;
        }
        else if(companyId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one company','error');
            return false;
        }
        else if(feeheadId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one fee head','error');
            return false;
        }
        else{
            error=false;
        }

        if(!error){
            var total=0;
            var pageTotal=0;
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var action = 'report';
            var ajaxurl=API_URL+'index.php/DueList/getDueListReportExport';
            //var params = {'branch': branchId, 'course': courseId, 'batch': chosenBatchIds,'company':companyId,'feehead':feeheadId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '), 'batchText': chosenbatchNames,'companyText': companyIdText.join(', '), 'feeheadText': feeheadIdText.join(', '),dueFromDate:dueFromDate};
            var params = {'branch': branchId, 'course': courseId,fromDate:fromDate,toDate:toDate,'company':companyId,'feehead':feeheadId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '),'companyText': companyIdText.join(', '), 'feeheadText': feeheadIdText.join(', '),dueFromDate:dueFromDate};
            var userID=$('#userId').val();
            var type='GET';
            //return false;
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            var response =
                commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: filterDueListReportExportResponse});


        } else {
            alertify.dismissAll();
            notify('Select any filter', 'error', 10);
        }
    }
    function filterDueListReportExportResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            var download=API_URL+'Download/downloadReport/'+response.file_name;
            window.location.href=(download);
        }
        else{
            notify(response.message,'error');
        }
    }
    function sendEmailDueList(This){
        $("#txtFromDate").removeClass("required");
        $("#txtToDate").removeClass("required");
        $('#txtFromDate').parent().find('label').removeClass("active");
        $('#txtFromDate').parent().find('span.required-msg').remove();
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();
        var branchId = [];
        var branchIdText = [];
        var courseId = [];
        var courseIdText = [];
        var batchId = [];
        var batchIdText = [];
        var companyId = [];
        var companyIdText = [];
        var feeheadId=[];
        var feeheadIdText=[];
        var dueFromDate=$('#txtDueFromDate').val();
        var fromDate=$('#txtFromDate').val();
        var toDate=$('#txtToDate').val();
        if(fromDate.trim()!='') {
            var txtfromDate = dateConvertion(fromDate);
        }
        if(toDate.trim()!='') {
            var txttoDate = dateConvertion(toDate);
        }
        var error = true;
        $("#branchFilter > .active").each(function() {
            error = false;
            branchId.push($(this).attr('data-id'));
            branchIdText.push($(this).text());
        });
        $("#courseFilter > .active").each(function() {
            error = false;
            courseId.push($(this).attr('data-id'));
            courseIdText.push($(this).text());
        });
        /*$("#batchFilter > .active").each(function() {
            error = false;
            batchId.push($(this).attr('data-id'));
            batchIdText.push($(this).text());
        });*/
        $("#companyFilter > .active").each(function() {
            error = false;
            companyId.push($(this).attr('data-id'));
            companyIdText.push($(this).text());
        });
        $("#feeheadFilter > .active").each(function() {
            error = false;
            feeheadId.push($(this).attr('data-id'));
            feeheadIdText.push($(this).text());
        });

        var branchId=branchId.join(', ');
        var courseId=courseId.join(', ');
        /*var batchId=batchId.join(', ');*/
        var companyId=companyId.join(', ');
        var feeheadId=feeheadId.join(', ');
        /*else if(batchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one batch','error');
            return false;
        }*/


        if(branchId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one branch','error');
            return false;
        }
        else if(courseId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one course','error');
            return false;
        }
        else if($('#txtFromDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select from date','error');
            return false;
        }
        else if($('#txtToDate').val().trim()==''){
            error = true;
            alertify.dismissAll();
            notify('Select to date','error');
            return false;

        }
        else if(new Date(txtfromDate).getTime()>new Date(txttoDate).getTime()){
            $("#txtFromDate").addClass("required");
            $("#txtFromDate").after('<span class="required-msg">Invalid</span>');
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">Invalid</span>');
            alertify.dismissAll();
            notify('Invalid dates','error');
            error=true;
            return false;
        }
        else if(companyId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one company','error');
            return false;
        }
        else if(feeheadId.trim()==''){
            error=true;
            alertify.dismissAll();
            notify('Select any one fee head','error');
            return false;
        }
        else{
            error=false;
        }

        if(!error){
            alertify.dismissAll();
            notify('Processing', 'warning', 40);
            var total=0;
            var pageTotal=0;

            var action = 'send email';
            var ajaxurl=API_URL+'index.php/DueList/sendEmailDues';
            //var params = {'branch': branchId, 'course': courseId, 'batch': batchId,'company':companyId,'feehead':feeheadId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '), 'batchText': batchIdText.join(', '),'companyText': companyIdText.join(', '), 'feeheadText': feeheadIdText.join(', '),dueFromDate:dueFromDate};
            var params = {'branch': branchId, 'course': courseId, fromDate:fromDate,toDate:toDate,'company':companyId,'feehead':feeheadId,'branchText': branchIdText.join(', '), 'courseText': courseIdText.join(', '),'companyText': companyIdText.join(', '), 'feeheadText': feeheadIdText.join(', '),dueFromDate:dueFromDate};
            var userID=$('#userId').val();
            var type='GET';
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            commonAjaxCall({This: This, requestUrl: ajaxurl, method: type, params:params, headerParams: headerParams, action: action, onSuccess: sendEmailDueListResponse});

        } else {
            alertify.dismissAll();
            notify('Select any filter', 'error', 10);
        }
    }
    function sendEmailDueListResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            notify(response.message, 'success', 10);
        }
        else{
            notify(response.message, 'error', 10);
        }
    }
    /* due list report ends here */
</script>

