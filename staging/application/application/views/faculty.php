<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
<input type="hidden" id="hdnUserId" name="hdnUserId" value="<?php echo $userData['userId']; ?>"/>
<input type="hidden" id="facultyId" name="subjectId" value="0"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Faculty </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" onclick="getFacultyDetails(this, 0)" data-position="top" data-tooltip="Add Faculty" class="viewsidePanel"><i class="icon-plus-circle" ></i></a></li>
                <!--<li><a></a></li>-->
            </ul>
        </div>   
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" > <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0" access-element="list">
                <table class="table table-responsive table-striped table-custom table-info" id="faculty-list">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Subjects</th>
                            <th class="border-r-none">Status</th>
                            <th class="no-sort" width="10%"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="facultyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeFacultyModal()"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="facultyModalLabel">Create Faculty</h4>
                    </div>
                    <form action="/" method="post" id="facultyForm" novalidate="novalidate">
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="facultyName" type="text" name="facultyName" class="validate" class="formSubmit" required>
                                    <label for="facultyName">Faculty Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="subject" name="subject" multiple class="validate" class="formSubmit multiple" >
                                        <option value="" >--Select All--</option>
                                    </select>
                                    <label class="select-label">Subject <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="comment" class="materialize-textarea" name="comment" class="validate" class="formSubmit"></textarea>
                                    <label for="address">Comment </label>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="actionButton" type="submit" class="btn blue-btn" onclick="AddFaculty(this)"><i class="icon-right mr8"></i>Add</button>
                        <button type="button" class="btn blue-light-btn" onclick="closeFacultyModal()"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildFacultyDataTable();
    });
    
    $("#subject").change(function () {
        multiSelectDropdown($(this));
    });

    function buildFacultyDataTable() {
        var userId = $('#hdnUserId').val();
        var ajaxurl=API_URL+'index.php/Faculty/getFacultyDatatables';
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        $("#faculty-list").dataTable().fnDestroy();
        $tableobj=$('#faculty-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false
            }],
            columns: [
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.name;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        
                        var view = '<span class="popper p10" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Subjects"><a  href="javascript:;" class="anchor-blue p10">'+data.total+'</a></span>';
                        var subject = data.subject==null?' -- ':data.subject;
                        view += '<div class="popper-content hide">';
                            view += '<p>'+subject+'</p>';
                        view += '</div>';
                        return view;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        var status='';
                        if(data.is_active == 1)
                        {
                            status="Active";
                        }
                        else
                        {
                            status="Inactive";
                        }
                        return status;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        var rawHtml = '';
                        var status = data.is_active == 1 ? 'Active' : 'Inactive';
                        var statusText = data.is_active == 1 ? 'Inactive' : 'Active';
                        rawHtml += '';
//                        rawHtml += status;
                        rawHtml += '<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        rawHtml += '<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                        rawHtml += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        rawHtml += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        rawHtml += '<li><a access-element="edit" href="javascript:;" onclick="getFacultyDetails(this,\'' + data.faculty_id + '\')">Edit</a></li>';
                        rawHtml += '<li><a access-element="delete" href="javascript:;" onclick="deleteFaculty(this,\'' + data.faculty_id + '\', \'' + data.is_active + '\')">' + statusText + '</a></li>';
                        rawHtml += '</ul></div>';
                        return rawHtml;
                    }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    
    function getFacultyDetails(This, id){
        notify('Processing..', 'warning', 10);
        $('#facultyForm .select-label').removeClass("active");
        $('#facultyForm input').removeClass("required");
        var data = [];
        var method = 'GET';
        var action = 'edit';
        var userID = $('#hdnUserId').val();
        
        var ajaxurl = API_URL + 'index.php/Subject/getAllSubjectList';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: function(response){
            buildSubjectDropdown(response);
            
            if(id == 0){
                //add
                $('#facultyId').val('0');
                $('#facultyModalLabel').html('Create Faculty');
                $('#actionButton').html('<i class="icon-right mr8"></i>Add');
                alertify.dismissAll();
                $('#facultyModal').modal('show');
            } else{
                $('#facultyId').val(id);
                //edit
                $('#facultyModalLabel').html('Edit Faculty');
                $('#actionButton').html('<i class="icon-right mr8"></i>Update');
                var params = {faculty_id: id};
                ajaxurl = API_URL + "index.php/faculty/getFacultyById";
                var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: method, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
                    if (response == -1 || response['status'] == false) {
                        alertify.dismissAll();
                        notify(response['message'], 'error', 10);
                    } else {
                        var rawData = response.data[0];
                        $('#facultyName').val(rawData.name);
                        $('#facultyName').next('label').addClass("active");
                        $('#comment').val(rawData.comments);
                        $('#comment').next('label').addClass("active");
                        var sel_subject = [];
                        var subjects = rawData.fk_subject_id.split(',');
                        if (subjects.length > 0) {
                            $.each(subjects, function (i, item) {
                                sel_subject.push(item);
                            });
                        }
                        $('#subject').val(sel_subject);
                        $('#subject').material_select();
                        alertify.dismissAll();
                        $('#facultyModal').modal('show');
                    }
                }});

            }
        }});
    }
    
    function closeFacultyModal(){
        $('#facultyForm')[0].reset();

	$('#subject').find('option:gt(0)').remove();
        $('#facultyId').val(0);
        $('#facultyForm label').removeClass("active");
        $('#facultyForm span.required-msg').remove();
        $('#facultyForm input').removeClass("required");
        
        $('#facultyForm input, .materialize-textarea').removeClass("required");

        $("select[name=subject]").val(0);
        $('select[name=subject]').material_select();

        $('#facultyModal').modal('toggle');

        $("#facultyModalLabel").html("Create Faculty");
    }
    
    function AddFaculty(This){
        notify('Processing..', 'warning', 10);
        $('#facultyForm span.required-msg').remove();
        $('#facultyForm input, .materialize-textarea').removeClass("required");
        var data ,url , action = '';
        
        var id = $('#facultyId').val();
        
        var facultyName = $('#facultyName').val();
        var comment = $('#comment').val();
        
        var selMulti = $.map($("#subject option:selected"), function (el, i)
        {
            if ($(el).val() != null && $(el).val().trim() != '' && $(el).val().trim() != 'default' && $(el).val().trim() != 'all')
            {
                return $(el).val();
            }
        });
        var subject = selMulti.join(",");
        
        var flag = 0;

        if (subject == '' || subject == null) {
            $("#subject").parent().find('.select-dropdown').addClass("required");
            $("#subject").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (facultyName == '') {
            $("#facultyName").addClass("required");
            $("#facultyName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (comment.trim.length > 0 && !(/^[a-zA-Z0-9 ,.]+$/.test(comment))) {
            $("#comment").addClass("required");
            $("#comment").after('<span class="required-msg">Only Alphanumeric</span>');
            flag = 1;
        }
        if (flag == 0) {
            data = {
                facultyName: facultyName,
                comment: comment,
                subject: subject
            };
            if(id == 0){
                //add
                url = API_URL + "index.php/Faculty/saveFaculty";
                action = 'add';
            }else{
                //edit
                url = API_URL + "index.php/Faculty/editFaculty/faculty_id/"+id;
                action = 'edit';
            }
            var userId = $('#hdnUserId').val();
            var ajaxurl = url;
            var params = data;
            var method = 'POST';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, method: method, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false) {
                    var id = '';
                    if (Object.size(response.data) > 0) {
                        for (var a in response.data) {
                            id = '#' + a;
                            if (a == 'subject') {
                                $(id).parent().find('.select-dropdown').addClass("required");
                                $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else if (a == 'id'){
                                notify(response.data[a], 'error', 10);
                            } else{
                                $(id).addClass("required");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            }
                        }
                    }else {
                        notify(response.message, 'error', 10);
                    }
                } else {
                    notify('Data Saved Successfully', 'success', 10);
                    buildFacultyDataTable();
                    closeFacultyModal();
                }
            }});
        }
    }
    
    function deleteFaculty(This, facultyId, currentStatus) {
        var status = currentStatus == 1 ? 'inactivate' : 'activate';
        var conf = "Are you sure want to " + status + " this faculty ?";
        customConfirmAlert('Faculty Status', conf);
        $("#popup_confirm #btnTrue").attr("onclick", "deleteFacultyConfirm(this,'" + facultyId + "')");
    }
    function deleteFacultyConfirm(This, facultyId) {
        notify('Processing..', 'warning', 10);
        var userId = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Faculty/deleteFaculty/faculty_id/' + facultyId;
        var method = "POST";
        var action = 'delete';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: method, requestUrl: ajaxurl, headerParams: headerParams, action: 'delete', onSuccess: deleteFacultyResponse});
    }
    function deleteFacultyResponse(response) {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false) {
            notify(response.message, 'error', 10);
        } else {
            notify(response.message, 'success', 10);
            buildFacultyDataTable();
        }
    }
</script>
