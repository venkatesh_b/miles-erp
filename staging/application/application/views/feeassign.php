<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 17/2/16
 * Time: 12:31 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnUserRole" value="<?php echo $userData['Role']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Fee Assign</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Assign fee" class="viewsidePanel" onclick="ManageFeeAssign(this,0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="feeassign-list">
                    <thead>
                    <tr>
                        <th>Candidate Name</th>
                        <th>Candidate Number</th>
                        <th>Course</th>
                        <th class="text-right">Amount Payable</th>
                        <th class="text-right">Amount Paid</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="FeeAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageFeeAssignForm" method="post" onsubmit="showLeadInfo();return false;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Fee Assign</h4>
                            <input type="hidden" id="hdnFeeAssignId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                            <div class="col-sm-11 pl0">
                                <div class="input-field">
                                    <input id="txtLeadNumber" type="text" class="validate">
                                    <label for="txtLeadNumber">Mobile Number <em>*</em></label>

                                </div>
                            </div>
                                <div class="col-sm-1 p0 mt2 sky-blue">
                                    <!--<a href="javascript:;" class="btn blue-btn"  onclick="showLeadInfo(this)">GO</a>-->
                                    <input type="submit" value="GO" class="btn blue-btn">
                                </div>
                            </div>

                            <input type="hidden" id="hdnCourseId" value="">
                            <input type="hidden" id="hdnBranchId" value="">
                            <input type="hidden" id="hdnBranchxceflead" value="">
                            <input type="hidden" id="payableAmount" value="">
                            <input type="hidden" id="feeStructureId" value="">
                            <input type="hidden" id="typeOfFee" value="">
                            <input type="hidden" id="company" value="">
                            <input type="hidden" id="Institutional" value="">
                            <div id="LeadData">
                                <div class="col-sm-12 mtb10">
                                    <div id="leadInfoData" ></div>
                                </div>
                                <div class="col-sm-12" id="CourseList" style="display:none">
                                    <div class="input-field">
                                        <select id="ddlCourse">
                                            <option value="" selected>--select--</option>
                                        </select>
                                        <label class="select-label">Course <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-12" id="typeOfClass" style="display:none">
                                    <div class="input-field">
                                        <select id="Trainingtype">
                                            <option value="" selected>--select--</option>
                                        </select>
                                        <label class="select-label">Training Type <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-12" id="feetypes" style="display:none">
                                    <div class="input-field">
                                        <select id="ddlFeetypes"  name="ddlFeetypes">
                                            <option value="" selected>--Select--</option>
                                        </select>
                                        <label class="select-label">Fee Type</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt0 feeStructureCreate-companyname" id="Corporatetypes" style="display:none">
                                    <div class="input-field">
                                        <select id="corporate"  name="corporate" class="validate custom-select-nomargin formSubmit">
                                            <option value="default" disabled selected>---Select---</option>
                                        </select>
                                        <label class="select-label">Corporate Company <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt0 feeStructureCreate-groupname" id="groupTypes" style="display:none">
                                    <div class="input-field">
                                        <select id="groupvalue"  name="groupvalue" class="validate formSubmit custom-select-nomargin">
                                            <option value="default" disabled selected>---Select---</option>
                                        </select>
                                        <label class="select-label">Institute Name <em>*</em></label>
                                    </div>
                                </div>
                                <div id="tabs" class="mt10"></div>
                                <div id="DataMessage" class="p5 valign-middle" style="display:none;float: left;">

                                </div>
                                <div id="AssignNewFee" style="display: none;">
                                    <a access-element="list" class='btn blue-btn btn-sm' onclick="assignNewFee()">Assign new fee</a>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveButton"  class="btn blue-btn" onclick="SaveCandidateFeeAssignDetails(this)"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelButton"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ModifyFee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ModifyFee" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Fee Modification</h4>
                            <input type="hidden" id="hdnCandidateFeeItemId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">

                                <div id="leadInfoSnapShot" >
                                </div>
                        </div>


                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveModifyFeeButton"  class="btn blue-btn" onclick="saveModifyFee(this)"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelModifyFeeButton"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
$context = 'Fee Assign';
$pageUrl = 'feeassign';
$serviceurl = 'feeassign';
    docReady(function(){
        buildFeeAssignListDataTable();
        loadFeeStructure();
        var headerParams =
        {
            action: 'add',
            context: 'Fee Assign',
            serviceurl: 'feeassign',
            pageurl: 'feeassign',
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $('input#txtLeadNumber').jsonSuggest({
            onSelect:function(item)
            {
                $('input#txtLeadNumber').val(item.id);
                $('input#hdnCourseId').val(item.fk_course_id);

            },
            headers:headerParams,
            url:API_URL + 'index.php/FeeAssign/getLeadsInformation',
            minCharacters: 2
        });
    });

    function loadFeeStructure()
    {  
        var userId = $('#hdnUserId').val();
        var action = 'add';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        $.when(
            commonAjaxCall
            (
                {
                    This: this,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                    params: {name: 'Company'},
                    action: action
                }
            ),commonAjaxCall
              (
        {
            This: this,
            headerParams: headerParams,
            requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
            params: {name: 'Institution'},
            action: action
        }
         ),
            commonAjaxCall
            (
                {
                    This: this,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                    params: {name: 'Fee Type'},
                    action: action
                }
            ),
            commonAjaxCall
            (
                {
                    This: this,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                    params: {name: 'Training Type'},
                    action: action
                }
            ),
            commonAjaxCall
            (
                {
                    This: this,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/Courses/getAllCourseList',
                    action: action
                }
            )
        ).then(function (response,response1,response2,response3,response4) {
            var corporate = response[0];
            var groupData = response1[0];
            var feeTypeData = response2[0];
            var trainingTypeData = response3[0];
            var courseData = response4[0];
            trainingTypeDropDown(trainingTypeData);
            corporateDropDown(corporate);
            groupDropDown(groupData);
            courseDropDown(courseData);
            $('#ddlFeetypes').find('option:gt(0)').remove();
            if (feeTypeData.data.length == 0 || feeTypeData == -1 || feeTypeData['status'] == false)
            {
                
            }
            else
            {
                var branchRawData = feeTypeData.data.reverse();
                for (var b = 0; b < branchRawData.length; b++) {
                    var o = $('<option/>', {value: branchRawData[b]['reference_type_value_id']})
                            .text(branchRawData[b]['value']);
                    o.appendTo('#ddlFeetypes');
                }
                $('#ddlFeetypes').material_select();
            }


        });
    }
    function courseDropDown(response)
    {
        $('#ddlCourse').find('option:gt(0)').remove();
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            //
        }
        else
        {
            var courseRawData = response.data;
            for (var b = 0; b < courseRawData.length; b++)
            {
                var o = $('<option/>', {value: courseRawData[b]['courseId']})
                    .text(courseRawData[b]['name']);
                o.appendTo('#ddlCourse');
            }
            $('#ddlCourse').val($("#hdnCourseId").val());
            $('#ddlCourse').material_select();
        }
    }
    function getDropDownFeeTypeByTextFilter(filter, identifier){
        var userId = $('#hdnUserId').val();
        var action = 'add';
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Fee Type'};
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This:this,method:'GET',requestUrl:ajaxurl,params:params,headerParams:headerParams,action:'add',onSuccess:function(response){
            $(identifier).find('option:gt(0)').remove();
            if (response.data.length == 0 || response == -1 || response['status'] == false) {
                
            } else {
                var filterArray = filter.split(',');
                var RawData = response.data.reverse();
                for (var b = 0; b < RawData.length; b++) {
                    for (var a = 0; a < filterArray.length; a++) {
                        if(filterArray[a] == RawData[b]['value']){
                            var o = $('<option/>', {value: RawData[b]['reference_type_value_id']})
                                    .text(RawData[b]['value']);
                            o.appendTo(identifier);
                        }
                    }
                }
                $(identifier).material_select();
            }
        }});
    }
    function buildFeeAssignListDataTable()
    {
            var ajaxurl=API_URL+'index.php/FeeAssign/getFeeAssignList';
            var userID=$('#hdnUserId').val();
            var branchID=$('#userBranchList').val();
            var params = {branchID: branchID};
            var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            $("#feeassign-list").dataTable().fnDestroy();
            $tableobj= $('#feeassign-list').DataTable( {
                "fnDrawCallback": function() {
                    $("#feeassign-list thead th").removeClass("icon-rupee");
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                    }
                    buildpopover();
                    verifyAccess();
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": true,
                "bProcessing": true,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No records found",
                    "sZeroRecords": "No records found",
                    "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                },
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    headers:headerParams,
                    data:params,
                    error:function(response) {
                        DTResponseerror(response);
                    }
                },
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false
                } ],
                columns: [
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.lead_name;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            return data.lead_number;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.course_name;
                    }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        var amount_payable=(data.amount_payable == null || data.amount_payable == '') ? "0.00":data.amount_payable;
                        return moneyFormat(amount_payable);
                    }
                    },

                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        var amount_paid=(data.amount_paid == null || data.amount_paid == '') ? "0.00":data.amount_paid;
                        CandidateData="";
                        CandidateData+=moneyFormat(amount_paid);
                        CandidateData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        CandidateData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                        CandidateData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                        CandidateData+='</a>';
                        CandidateData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        CandidateData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        CandidateData+='<li><a  href="javascript:;" access-element="edit" onclick="ModifyFeeAssign(this,\''+data.candidate_fee_id+'\')">Edit</a></li>';
                        CandidateData+='</ul>';
                        CandidateData+='</div>';
                        return CandidateData;
                    }
                    },


                ]
            } );
            DTSearchOnKeyPressEnter();
    }
    function ModifyFeeAssign(This,Id){
        $("#hdnCandidateFeeItemId").val(Id);

        var UserId = $('#hdnUserId').val();
        var candidate_fee_item_id=$('#hdnCandidateFeeItemId').val();
        var ajaxurl = API_URL + 'index.php/FeeAssign/getFeeAssignByCandidateItem';
        var type = "GET";
        var action = 'edit';
        var params={candidate_fee_item_id:candidate_fee_item_id};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: UserId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl,params:params, headerParams: headerParams, action: action, onSuccess: function(response){

            ModifyFeeAssignResponse(response);

        }});
        $(This).attr("data-target", "#ModifyFee");
        $(This).attr("data-toggle", "modal");
    }
    function ModifyFeeAssignResponse(response){
        if(response.status===false){
            notify(response.message,'error',10);
        }
        else
        {
            rawData=response.data[0];
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }
            if(rawData.companyName==null || rawData.companyName==''){
                rawData.companyName='----';
            }
            if(rawData.institution==null || rawData.institution==''){
                rawData.institution='----';
            }
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }

            var content = '<div class="col-sm-12 ">';
            content += '<div class="light-blue3-bg clearfix pt10 pb10">';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Name</span>';
            content += '<p>' + rawData.name + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Email</span>';
            content += '<p>' + rawData.email + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Phone</span>';
            content += '<p>' + rawData.phone + '</p>';
            content += '</div>';
            content += ' </div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Branch</span>';
            content += '<p>' + rawData.branchName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Course</span>';
            content += '<p>' + rawData.courseName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Qualification</span>';
            content += '<p>' + rawData.qualification + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Company</span>';
            content += '<p>' + rawData.companyName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Institution</span>';
            content += '<p>' + rawData.institution + '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            var disableAmnt='';
            if(rawData.is_modification_allowed==0) {
                var disableAmnt=' readyonly disabled ';
            }
            if(rawData.is_concession_pending==1) {
                var disableAmnt=' readyonly disabled ';
            }
            if(rawData.is_modification_allowed==1 && rawData.is_concession_pending==0){
                disableAmnt='';
            }
            var Amounts = "";
            Amounts += '<div class="col-sm-12 mt15">'
                + '<table class="table table-responsive table-striped table-custom" id="feeassign-tb-list">'
                + '<thead><tr>'
                + '<th>Fee Head</th><th>Due days</th><th>Amount</th><th>Modification</th></tr>'
                + '</thead><tbody>';

            $.each(response.data, function (key, value) {
                //$('#txtContactBranch').append($('<option></option>').val(value.id).html(value.name));
                Amounts += '<tr>'
                    + '<td>' + value.feeHeadName + '</td>'
                    + '<td>'+ value.due_days+'</td>'
                    + '<td>'+ value.amount_payable+'</td>'
                    + '<td>'
                    + '<input type="hidden" id="hdnActualAmountPayable_'+value.candidate_fee_item_id+'" name="hdnActualAmountPayable[]" value="' + (parseFloat(value.concession_amount)+parseFloat(value.right_of_amount)) + '" /> '
                    +'<div class="col-sm-12 pl0"><input style="width: 80%;" class="modal-table-textfiled m0" type="text" placeholder="Amount" name="txtNewAmountPayable" id="txtNewAmountPayable_'+value.candidate_fee_item_id+'" value="' + value.modified_amount + '" onkeypress="return isNumberKey(event)" '+disableAmnt+'  /></div></td></tr>';
            });

            Amounts += '</tbody></table></div><div id="DataMessagePayment" class="col-sm-12 mt15 text-center"></div>';
            $('#leadInfoSnapShot').html(content+Amounts);
            $("#DataMessagePayment").hide();
            $("#DataMessagePayment").html('');
            if(rawData.is_modification_allowed==0) {
                $("#DataMessagePayment").show();
                $("#DataMessagePayment").html("<b style='color:red;'>Already Fee Collected</b>");
                $('#saveModifyFeeButton').attr('disabled',true);
            }
            if(rawData.is_concession_pending==1) {
                $("#DataMessagePayment").show();
                $("#DataMessagePayment").html("<b style='color:red;'>Concession request is in pending.</b>");
                $('#saveModifyFeeButton').attr('disabled',true);
            }
            if(rawData.is_modification_allowed==1 && rawData.is_concession_pending==0){
                $('#saveModifyFeeButton').attr('disabled',false);
            }


        }

    }
    function saveModifyFee(This){
        var UserId = $('#hdnUserId').val();
        var userRole = $("#hdnUserRole").val();
        $("input[name='txtNewAmountPayable']").removeClass("required");
        $("input[name='txtNewAmountPayable']").next('span.required-msg').remove();
        $("input[name='txtNewAmountPayable']").removeClass("active");
        var flag=0;
        var feeModifications=[];
        var sub_flag=0;
        $.each($("input[name='txtNewAmountPayable']"),function(key, value){
            if($(this).val()=='' || $(this).val()==0) {
                $(this).addClass("required");
                $(this).after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag=1;

            }
            else{
                var res = $(this).attr('id').split("_");
                if($(this).val()<parseFloat($('#hdnActualAmountPayable_'+res[1]).val())){
                    $(this).addClass("required");
                    $(this).after('<span class="required-msg"> >=' + $('#hdnActualAmountPayable_'+res[1]).val() + '</span>');
                    sub_flag=1;

                }
                else{
                    feeModifications.push(res[1] + '-' + $(this).val());
                }

            }
            if(sub_flag==1){
                flag=1;
            }

        });
        var feeModifications=feeModifications.join('|');
        if(flag==0) {
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var candidate_fee_item_id = $('#hdnCandidateFeeItemId').val();
            var ajaxurl = API_URL + 'index.php/FeeAssign/modifyFeeAssign';
            var type = "POST";
            var action = 'edit';
            var params = {feeModifications:feeModifications,candidate_fee_id:candidate_fee_item_id};
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: UserId,
                userrole:userRole
            };
            commonAjaxCall({
                This: This,
                method: type,
                requestUrl: ajaxurl,
                params: params,
                headerParams: headerParams,
                action: action,
                onSuccess: function (response) {

                    saveModifyFeeResponse(response);

                }
            });
        }
    }
    function saveModifyFeeResponse(response)
    {
        alertify.dismissAll();
        if(response.status===true)
        {
            buildFeeAssignListDataTable();
            loadFeeStructure();
            notify(response.message,'success',10);
            $('#ModifyFee').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
        else
        {
            notify(response.message,'error',10)
        }
    }
    function assignNewFee()
    {
        $("#feeassign-tb-list > tbody").html("");
        $("#CourseList").show();
        $("#typeOfClass").show();
        $("#feetypes").show();
        $("#Trainingtype").val('');
        $("#ddlFeetypes").val('');
        $("#Trainingtype").removeAttr("disabled");
        $('#Trainingtype').material_select();
        $("#ddlFeetypes").removeAttr("disabled");
        $('#ddlFeetypes').material_select();
        $("#saveButton").removeAttr("disabled");
        $("#closeButton").removeAttr("disabled");
        $("#DataMessage").hide();
        $("#DataMessage").html("");
        $("#AssignNewFee").hide();
    }
    $('#ddlCourse').on('change', function ()
    {
        $("#hdnCourseId").val($(this).val());
        showLeadAssignFeeStructureInfo();
    });
    $('#Trainingtype').on('change', function () {
    showLeadAssignFeeStructureInfo();
});
    function showLeadAssignFeeStructureInfo()
    {
        var course=$("#hdnCourseId").val();
        var branch=$("#hdnBranchId").val();
        var userID = $('#hdnUserId').val();
        var trainingType=$("#Trainingtype").val();
        var feeType=$("#ddlFeetypes").val();
        var corporate=$("#company").val();
        var groupvalue=$("#Institutional").val();
        $("#tabs").hide();
        $('#ManageFeeAssignForm span.required-msg').remove();
        $('#ManageFeeAssignForm input').removeClass("required");
        $('#ManageFeeAssignForm span.required-msg').remove();
        $('#ManageFeeAssignForm select').removeClass("required");

        var flag=0;

        if ((trainingType == '' || trainingType == null))
        {
            $("#Trainingtype").parent().find('.select-dropdown').addClass("required");
            $("#Trainingtype").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if ((feeType == '' || feeType == null))
        {
            $("#ddlFeetypes").parent().find('.select-dropdown').addClass("required");
            $("#ddlFeetypes").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if (feeType == '13')
        {
            if (groupvalue == '' || groupvalue == null)
            {
                $("#groupvalue").parent().find('.select-dropdown').addClass("required");
                $("#groupvalue").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;
            }
        }
        else if (feeType == '12')
        {
            if (corporate == '' || corporate == null)
            {
                $("#corporate").parent().find('.select-dropdown').addClass("required");
                $("#corporate").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;
            }
        }

        if(flag==0){

            if (feeType == '13')
            {
                var data = {
                    branch:branch,
                    course:course,
                    feeType:feeType,
                    groupvalue:groupvalue,
                    UserID:userID,
                    trainingType:trainingType
                };
            }
            else if (feeType == '12' && corporate != "" && corporate != null)
            {
                var data = {
                    branch:branch,
                    course:course,
                    feeType:feeType,
                    corporate:corporate,
                    UserID:userID,
                    trainingType:trainingType
                };
            }
            else if (feeType == '11')
            {
                var data = {
                    branch:branch,
                    course:course,
                    feeType:feeType,
                    UserID:userID,
                    trainingType:trainingType
                };

            }
            var ajaxurl = API_URL + 'index.php/FeeAssign/getFeeStructureByLead';
            var params = data;
            var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: GetLeadFeeStructureInfoResponse});

        }

    }
    function GetLeadFeeStructureInfoResponse(response){
        alertify.dismissAll();
        if (response == -1 || response['status'] == false)
        {
            $("#buttons").show();
            $("#tabs").hide();
            $("#saveButton").attr('disabled',true);
            notify('No fee structure found for this lead', 'error', 10);
        }
        else
        {
            var feeStructureInfo = response['data'];
            $("#hdnFeeStructureId").val(feeStructureInfo[0]['fee_structure_id']);
            if(feeStructureInfo[0]['fk_type_id']==11 || feeStructureInfo[0]['fk_type_id']==12 || feeStructureInfo[0]['fk_type_id']==13)
            {
                $("#ddlFeetypes").val(feeStructureInfo[0]['fk_type_id']);
                $('#ddlFeetypes').material_select();
                $("#ddlFeetypes").next('label').addClass("active"); $("#feetypes").show();
            }
            $("#feeStructureId").val(feeStructureInfo[0]['fee_structure_id']);
            var Amounts="";
            Amounts+='<div class="col-sm-12 p0">'
                +'<table class="table table-responsive table-striped table-custom" id="feeassign-tb-list">'
                +'<thead><tr>'
                +'<th>Fee Head</th><th>Due Days</th><th>Amount</th></tr>'
                +'</thead><tbody>';

            if(feeStructureInfo[0]['ammounts']!="")
            {
                var rows=feeStructureInfo[0]['ammounts'];
                var total=0;

                for(var i=0;i<rows.length;i++){

                    Amounts+='<tr>'
                        +'<td>'+rows[i]['fee_head_name']+'</td>'
                        +'<td>'+rows[i]['due_days']+'</td>'
                        +'<td>'+rows[i]['amount']+'</td></tr>';
                    total+= parseInt(rows[i]['amount']);
                }
                Amounts+='<tr><td></td><td>Total</td></td><td>'+total+'</td></tr>';
            }
            Amounts+='</tbody></table></div>';
            $("#saveButton").removeAttr('disabled',false);
            $("#tabs").show();
            $("#tabs").html(Amounts);
        }
    }
    function SaveCandidateFeeAssignDetails(This)
    {
        var brachxcerflead= $("#hdnBranchxceflead").val();
        var FeeStructureId= $("#feeStructureId").val();
        var courseId = $('#ddlCourse').val();
        var userID = $('#hdnUserId').val();
        alertify.dismissAll();
        notify('Processing..', 'warning', 50);
        var ajaxurl = API_URL + 'index.php/FeeAssign/AddCandidate';
        var params = {brachxcerflead:brachxcerflead,FeeStructureId:FeeStructureId,userID:userID,courseId:courseId,pageURL:'feeassign'};
        var headerParams = {action:'add',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        commonAjaxCall({This:this,method:'POST',requestUrl:ajaxurl,params:params,headerParams:headerParams,action:'add',onSuccess:SaveCandidateResponse});
    }

    $( "#userBranchList" ).change(function() {
        buildFeeAssignListDataTable();
        changeDefaultBranch();
    });
</script>