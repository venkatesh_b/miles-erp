<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnUserRole" value="<?php echo $userData['Role']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Fee Defaulters</h3>
        <div class="content-header-btnwrap">
            <ul>

                    <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Add Fee Defaulter" class="viewsidePanel" onclick="ManageFeeDefaulter(this,0)"><i class="icon-plus-circle" ></i></a></li>

            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" id="feedetails-report-list-wrapper" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0 branch-visit-scroll" style="">
                <table class="table table-inside table-custom branchwise-report-table" id="feedetails-report-list">
                    <thead>
                    <tr class="">
                        <th>Enroll No.</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Batch</th>
                        <th class="text-right">Amount Payable</th>
                        <th class="text-right">Amount Paid</th>
                        <th class="text-right">Write Off Amount</th>
                        <th>Comments</th>
                    </tr>
                    </thead>
                </table>
                </div>
        </div>
        <!--Modal-->
        <div access-element="add" class="modal fade" id="FeeDefaulter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="650">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageFeeDefaulterForm" method="post" onsubmit="showDefaulterLeadInfo(this);return false;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Fee Defaulter</h4>
                            <input type="hidden" id="hdnFeeAssignId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="col-sm-11 pl0">
                                    <div class="input-field">
                                        <input id="txtLeadNumber" type="text" class="validate">
                                        <label for="txtLeadNumber">Mobile Number <em>*</em></label>

                                    </div>
                                </div>
                                <div class="col-sm-1 p0 mt2 sky-blue">
                                    <!--<a href="javascript:;" class="btn blue-btn"  onclick="showDefaulterLeadInfo(this)">GO</a>-->
                                    <input type="submit" value="GO" class="btn blue-btn">
                                </div>
                            </div>
                            <div id="leadInfoSnapShot" >
                            </div>
                        </div>
                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveModifyFeeButton"  class="btn blue-btn" onclick="saveFeeDefaulter()"><i class="icon-right mr8"></i>Save</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelModifyFeeButton"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildFeeDefaultersListDataTable();
        $("#txtBranchVisit").keydown(function(e){
            if(e.which === 13){

                $("#btnLeadSnapshot").click();
                e.preventDefault();
                return false;

            }
        });
        //buildLeadsDataTable();
    });
    $('#userBranchList').change(function(){
        changeDefaultBranch(this);
        buildFeeDefaultersListDataTable();
    });
    function buildFeeDefaultersListDataTable()
    {
        var branchId = [];
        var courseId = [];
        var batchId = [];
        var companyId = [];
        var feeheadId = [];
        var error = true;


        branchId.push($('#userBranchList').val());
        var branchId=branchId.join(', ');
        var courseId=courseId.join(', ');
        var batchId=batchId.join(', ');
        var companyId=companyId.join(', ');
        var feeheadId=feeheadId.join(', ');
        var total=0;
        var pageTotal=0;
        var action = 'list';
        var ajaxurl=API_URL+'index.php/FeeDefaulters/getFeeDefaultersList';
        var params = {'branch': branchId, 'course': courseId, 'batch': batchId,company:companyId,feehead:feeheadId};
        var userID=$('#hdnUserId').val();
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#feedetails-report-list-wrapper").show();
        $("#feedetails-report-list").dataTable().fnDestroy();
            $tableobj = $('#feedetails-report-list').DataTable( {
                "fnDrawCallback": function() {
                    $("#feedetails-report-list thead th").removeClass("icon-rupee");
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                    }
                    verifyAccess();
                    buildpopover();
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": false,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No Records Found",
                    "sZeroRecords": "No Records Found",
                    "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                },
                "bProcessing": true,
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    data:params,
                    headers:headerParams,
                    error:function(response) {
                        DTResponseerror(response);
                    }
                },
                columns: [
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.enrollNo;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.leadName;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        if(data.leadPhone==null || data.leadPhone.trim()=='')
                            data.leadPhone='----';

                        return data.leadPhone;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.batchCode;
                    }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        return moneyFormat(data.amountPayable);
                    }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        return moneyFormat(data.amountPaid);
                    }
                    },
                    {
                        data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        return moneyFormat(data.right_of_amount);
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        if(data.right_of_given_comments==null || data.right_of_given_comments.trim()=='')
                            data.right_of_given_comments='----';

                        return data.right_of_given_comments;
                    }
                    }

                ]
            } );
            DTSearchOnKeyPressEnter();


    }
    //Fee concession function starts
    function ManageFeeDefaulter(This)
    {
        $("#LeadData").hide();
        $('#ManageFeeDefaulterForm')[0].reset();
        $('#ManageFeeDefaulterForm label').removeClass("active");
        $('#ManageFeeDefaulterForm span.required-msg').remove();
        $('#ManageFeeDefaulterForm input').removeClass("required");
        $('#ManageFeeDefaulterForm select').removeClass("required");
        $('#leadInfoSnapShot').html("");
        $("select[name=ddlFeetypes]").val(0);
        $('#ddlFeetypes').material_select();
        $(This).attr("data-target", "#FeeDefaulter");
        $(This).attr("data-toggle", "modal");
    }
    function feeassignDefaulterReset(){
        $('#ddlFeetypes').find('option:gt(0)').remove();
        var r = $('<option/>', {value:"11"}).text("Retail");
        var c = $('<option/>', {value:"12"}).text("Corporate");
        var g = $('<option/>', {value:"13"}).text("Group");
        r.appendTo('#ddlFeetypes');
        c.appendTo('#ddlFeetypes');
        g.appendTo('#ddlFeetypes');
        $("#feetypes").show();
        $('#ddlFeetypes').material_select();
    }
    function showDefaulterLeadInfo(This)
    {
        var userID = $('#hdnUserId').val();
        var userRole = $("#hdnUserRole").val();
        var leadNumber=$("#txtLeadNumber").val();
        var RegexNumbers = /^[0-9]+$/;
        $('#leadInfoSnapShot').html("");
        feeassignDefaulterReset();

        $("#ddlFeetypes").attr("disabled",false);
        $('#ManageFeeDefaulterForm span.required-msg').remove();
        $('#ManageFeeDefaulterForm input').removeClass("required");
        $('#ManageFeeDefaulterForm textarea').removeClass("required");
        $("#online").attr("disabled",false);
        $("#clasRoom").attr("disabled",false);

        $("#ShowfeeDetails").hide();
        var flag=0;
        if(leadNumber == '')
        {
            $("#txtLeadNumber").addClass("required");
            $("#txtLeadNumber").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
            flag=1;
        }
        else if (RegexNumbers.test(leadNumber) === false)
        {
            $("#txtLeadNumber").addClass("required");
            $("#txtLeadNumber").after('<span class="required-msg">Accepts Numbers only</span>');
            flag = 1;
        }

        if(flag==0){
            var ajaxurl = API_URL + 'index.php/FeeDefaulters/getFeeDefaulterInfoByLeadNumber';
            var action = 'add';
            var params = {UserID: userID,LeadNumber:leadNumber};
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID,userRole:userRole};
            commonAjaxCall({This: this,  requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: GetDefaulterLeadInfoResponse});
        }

    }

    function GetDefaulterLeadInfoResponse(response)
    {
        if(response.status===false){
            notify(response.message,'error',10);
        }
        else
        {
            rawData=response.data[0];
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }
            if(rawData.companyName==null || rawData.companyName==''){
                rawData.companyName='----';
            }
            if(rawData.institution==null || rawData.institution==''){
                rawData.institution='----';
            }
            if(rawData.qualification==null || rawData.qualification==''){
                rawData.qualification='----';
            }

            var content = '<div class="col-sm-12 ">';
            content += '<div class="light-blue3-bg clearfix pt10 pb10">';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Name</span>';
            content += rawData.name;
            content += '<input type="hidden" id="hdnCandidateFeeId" value="' + rawData.candidate_fee_id + '" />';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Email</span>';
            content += '<p>' + rawData.email + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Phone</span>';
            content += '<p>' + rawData.phone + '</p>';
            content += '</div>';
            content += ' </div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Branch</span>';
            content += '<p>' + rawData.branchName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Course</span>';
            content += '<p>' + rawData.courseName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Qualification</span>';
            content += '<p>' + rawData.qualification + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Company</span>';
            content += '<p>' + rawData.companyName + '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="col-sm-6 mb5">';
            content += '<div class="input-field data-head-name">';
            content += '<span class="display-block ash">Institution</span>';
            content += '<p>' + rawData.institution + '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            var disableAmnt='';
            if(rawData.is_concession_request_pedning == 1)
            {
                disableAmnt=' readonly disabled ';
            }
            else if(rawData.is_modification_request_pedning == 1)
            {
                disableAmnt=' readonly disabled ';
            }
            else
            {
                disableAmnt='';
            }

            var Amounts = "";
            Amounts += '<div class="col-sm-12 mt15">'
                + '<table class="table table-responsive table-striped table-custom" id="feeassign-list">'
                + '<thead><tr>'
                + '<th>Fee Head</th><th>Due days</th><th>Amount</th><th style="width:100px">Write Off Amount</th><th>Comments</th></tr>'
                + '</thead><tbody>';
            for(var i=0;i<response.data.length;i++)
            {
                Amounts += '<tr>';
                Amounts += '<td>' + response.data[i].feeHeadName + '</td>';
                Amounts += '<td>'+  response.data[i].due_days + '</td>';
                Amounts += '<td>' + response.data[i].amount_payable + '</td>';
                Amounts += '<td><div class="col-sm-8 pl0">';
                Amounts += '<input type="hidden" name="hdnCandidateFeeItemId[]" value="' + response.data[i].candidate_fee_item_id + '" />';
                Amounts += '<input type="hidden" name="hdnAmountPayable[]" value="' + response.data[i].amount_payable + '" />';
                Amounts += '<input type="hidden" name="hdnDueAmountPayable[]" value="' + (response.data[i].amount_payable-response.data[i].amount_paid) + '" />';
                Amounts += '<input class="modal-table-textfiled m0" type="text" name="txtRightOfAmount[]" value="' + response.data[i].right_of_amount + '" onkeypress="return isNumberKey(event)" '+disableAmnt+' />';
                Amounts += '</div></td>';
                Amounts+='<td>';
                Amounts+='<div class="col-sm-12 p0">';
                if(response.data[i].right_of_given_comments==null){
                    response.data[i].right_of_given_comments='';
                }
                Amounts+='<textarea class="form-control table-textarea" rows="2" name="txtRightOfAmountDesc[]" '+disableAmnt+'>'+response.data[i].right_of_given_comments+'</textarea>';
                Amounts+='</div>';
                Amounts+='</td>';
                Amounts += '</tr>';
            }

            Amounts += '</tbody></table></div><div id="DataMessagePayment" class="col-sm-12 mt15"></div>';
            $('#leadInfoSnapShot').html(content+Amounts);
            $("#DataMessagePayment").hide();
            $("#DataMessagePayment").html('');
            if(rawData.is_concession_request_pedning == 1)
            {
                $("#DataMessagePayment").show();
                $("#DataMessagePayment").html("<b style='color:red;text-align:center;'>Concession request in pending! </b>");
                $('#saveModifyFeeButton').attr('disabled',true);
            }
            else if(rawData.is_modification_request_pedning == 1)
            {
                $("#DataMessagePayment").show();
                $("#DataMessagePayment").html("<b style='color:red;text-align:center;'>Modification request in pending! </b>");
                $('#saveModifyFeeButton').attr('disabled',true);
            }
            else
            {
                $('#saveModifyFeeButton').attr('disabled',false);
            }

        }
    }
    function saveFeeDefaulter()
    {
        var UserId = $('#hdnUserId').val();
        var userRole = $("#hdnUserRole").val();
        var flag=0;
        var index=0;
        var ConcessionFeeData = [];
        var candidateFeeId=$("#hdnCandidateFeeId").val();
        $('input[name^="txtRightOfAmount"]').removeClass("required");
        $('textarea[name^="txtRightOfAmountDesc"]').removeClass("required");
        $('textarea[name^="txtRightOfAmountDesc"]').each(function()
        {
            if($(this).val().trim()==''){
                $(this).addClass("required");
                flag=1;
            }
        });
        $('input[name^="txtRightOfAmount"]').each(function()
        {

            if(parseInt($(this).val()) > parseInt($('input[name^="hdnDueAmountPayable"]:eq('+index+')').val()))
            {
                $(this).addClass("required");
                notify('Cannot be more than due amount', 'error', 10);
                flag=1;
            }
            else
            {
                if($(this).val() >=0 )
                    ConcessionFeeData.push($('input[name^="hdnCandidateFeeItemId"]:eq('+index+')').val()+'@@@@@@'+$('input[name^="hdnAmountPayable"]:eq('+index+')').val()+'@@@@@@'+$(this).val()+'@@@@@@'+$('textarea[name^="txtRightOfAmountDesc"]:eq('+index+')').val());
            }
            index++;
        });
        if(flag==1)
        {
            return false;
        }
        else
        {
            alertify.dismissAll();
            notify('Processing..', 'warning', 50);
            var ajaxurl = API_URL + 'index.php/FeeDefaulters/UpdateCandidateFeeDefaulter';
            var params = {candidateFeeId:candidateFeeId,feeConcessionData: ConcessionFeeData};
            var action='add';
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: UserId,
                userRole:userRole
            };
            commonAjaxCall({
                method: 'POST',
                requestUrl: ajaxurl,
                params: params,
                headerParams: headerParams,
                action: action,
                onSuccess: function (response) {
                    alertify.dismissAll();
                    if(response.status===true){
                        buildFeeDefaultersListDataTable();
                        notify(response.message,'success',10);
                        $('#FeeDefaulter').modal('hide');
                        $('body').removeClass('modal-open');
                    }
                    else{
                        notify(response.message,'error',10);
                    }


                }
            });
        }
    }
    $(function(){
        var headerParams = {
            action:'list',
            context:$context,
            serviceurl:$pageUrl,
            pageurl:$pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $('input#txtLeadNumber').jsonSuggest({
            onSelect:function(item){
                $('input#txtLeadNumber').val(item.id);
            },
            headers:headerParams,url:API_URL + 'index.php/FeeAssign/getLeadsInformation' , minCharacters: 2});
    });
</script>