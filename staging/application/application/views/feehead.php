<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 17/2/16
 * Time: 12:31 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Fee Head</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Add Fee Head" class="viewsidePanel" onclick="ManageFeeHead(this,0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div  class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="FeeHead-list">
                    <thead>
                    <tr>
                        <th>Fee Head</th>
                        <th>Company Name</th>
                        <th>Description</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="myFeeHead" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageFeeHeadForm" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Company Details</h4>
                            <input type="hidden" id="hdnFeeHeadId" value="0" />
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtFeeHeadName" type="text" class="validate">
                                    <label for="txtFeeHeadName">Fee Head <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="ddlCompanies">
                                        <option value=""  selected>--Select--</option>

                                    </select>
                                    <label class="select-label">Company <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="txaDescription" class="materialize-textarea"></textarea>
                                    <label for="txaDescription">Description</label>
                                </div>
                            </div>
                            <div class="col-sm-12" access-element="status">
                                <div class="switch display-inline-block">
                                    <label class="display-block">Status</label>
                                    <label>
                                        Off
                                        <input type="checkbox" id="chkFreeHeadStatus" checked>
                                        <span class="lever"></span>
                                        On
                                    </label>
                                </div>
                            </div>
                            <input type="hidden" readonly value="1" id="hdnFreeHeadStatusValue"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="SaveButton" class="btn blue-btn" onclick="SaveFeeHead(this)"><i class="icon-right mr8"></i>Add</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--popup-modal-->
        <div class="modal fade bs-example-modal-sm" id="popup" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog alert-modal modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModelLabelFeehead">Heading</h4>
                    </div>
                    <div class="modal-body clearfix">

                        Description...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn btn-xs"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" class="btn blue-light-btn btn-xs" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>

                </div>
            </div>
        </div>
        <!--popup-modal-->
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildFeeHeadListDataTable();
    });

    function buildFeeHeadListDataTable()
    {
        $(document).ready(function() {
            var ajaxurl=API_URL+'index.php/FeeHead/getListOfFeeHead';
            var userID=$('#hdnUserId').val();
            var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
            $("#FeeHead-list").dataTable().fnDestroy();
            $tableobj=$('#FeeHead-list').DataTable( {
                "fnDrawCallback": function() {
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if(pages > 1)
                    {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                    }
                    buildpopover();
                    verifyAccess();
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": true,
                "bProcessing": true,
                "oLanguage":
                {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No records found",
                    "sZeroRecords": "No records found",
                    "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                },
                ajax: {
                    url:ajaxurl,
                    type:'GET',
                    headers:headerParams,
                    error:function(response) {
                        DTResponseerror(response);

                    }
                },
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false
                } ],

                columns: [
                    {
                        data: null, render: function ( data, type, row )
                    {

                        return data.name;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.company_name;
                    }
                    },
                    {
                        data: null, render: function ( data, type, row )
                    {
                        return data.description;
                    }
                    },




                    {
                        data: null, render: function ( data, type, row )
                    {
                        var roleUsersData='';

                        var status="";
                        if(data.status==0){
                            status= "In Active";

                        }else{
                            status= "Active";
                        }
                        roleUsersData+=status;
                        roleUsersData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        roleUsersData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                        roleUsersData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                        roleUsersData+='</a>';
                        roleUsersData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        roleUsersData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        roleUsersData+='<li><a access-element="edit" href="javascript:;" onclick="ManageFeeHead(this,\''+data.fee_head_id+'\')">Edit</a></li>';
                        roleUsersData+='<li><a access-element="delete" href="javascript:;" onclick="DeleteFeeHead(this,\''+data.fee_head_id+'\')">Delete</a></li>';

                        roleUsersData+='</ul>';
                        roleUsersData+='</div>';
                        return roleUsersData;
                    }
                    },


                ]
            } );
            DTSearchOnKeyPressEnter();

        } );
    }

</script>