<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 22/6/16
 * Time: 10:54 AM
 */
//print_r($userData);exit;
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
        <input type="hidden" id="hdnFeeCollectionId" value="<?php echo $Id ?>" />
        <h3 class="content-header">Fee Receipt</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="" href="<?php echo APP_REDIRECT;?><?php echo (($Id!='' && $Sub_id!='' && isset($exp[1]) && trim($exp[1])!='0' && trim($exp[1])!='') ? $exp[1].'/'.$branchUrl : 'feecollection'); ?>"><i class="icon-times"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0" id="feeReceiptData">


            </div>
        </div>
    </div>
</div>
<script>
    $context = 'Fee Collection';
    $pageUrl = 'feecollection';
    $serviceurl = 'feecollection';
    docReady(function () {
        if($("#userBranchList").length > 0)
            $("#userBranchList").parentsUntil('div.BranchName-dropdown').remove();
        feereceiptData();
    });
    function feereceiptData()
    {
        var ajaxurl = API_URL + 'index.php/FeeCollection/FeeCollectionDetails';
        var userID=$('#hdnUserId').val();
        var feeCollectionID=$('#hdnFeeCollectionId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var params = {feeCollectionID: feeCollectionID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: FeeReceiptResponse});
    }
    function FeeReceiptResponse(response)
    {
        var feereceipt_data=response['data'][0];
        var payableListModel='';
        payableListModel+='<div class="receipt-header">';
        payableListModel+='<span class="left-logo">';
        payableListModel+='<img src="'+BASE_URL+'assets/images/logo.png" />';
        payableListModel+='</span>';
        payableListModel+='</div>';
        payableListModel+='<div class="print_reciept p5">';
        payableListModel+='<div data-reciept="assets" class="wrapper">';
        payableListModel+='<p class="campus"></p>';
        payableListModel+='<h1>'+feereceipt_data['companyName']+'</h1>';
        payableListModel+='<p>'+feereceipt_data['branchName']+'</p>';
        payableListModel+='<table cellspacing="0" cellpadding="4" border="0" class="studentDetails">';
        payableListModel+='<tbody>';
        payableListModel+='<tr>';
        payableListModel+='<td>Student/Lead No:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['lead_number']+'</td>';
        payableListModel+='<td>Course:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['courseName']+'</td>';
        payableListModel+='</tr>';
        payableListModel+='<tr>';
        payableListModel+='<td>Batch:</td>';
        if(feereceipt_data['alias_name'] == null || feereceipt_data['alias_name'] == '')
            payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['batchInfo']+'</td>';
        else
            payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['batchInfo']+'('+feereceipt_data['alias_name']+')</td>';
        payableListModel+='<td>Receipt No:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['receipt_number']+'</td>';
        payableListModel+='</tr>';
        payableListModel+='<tr>';
        payableListModel+='<td>Name:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['studentName']+'</td>';
        payableListModel+='<td>Date:</td>';

        if(feereceipt_data['payment_type']=='Online')
            payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['receipt_date_time']+'</td>';
        else
            payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['receipt_date']+'</td>';

        //payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['receipt_date']+'</td>';
        payableListModel+='</tr>';
        payableListModel+='<tr>';
        payableListModel+='<td>Payment Mode:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['payment_type']+'</td>';

        payableListModel+='<td>Payment Type:</td>';
        payableListModel+='<td class="font-bold" colspan="7">'+feereceipt_data['payment_mode']+'</td>';
        payableListModel+='</tr>';
        payableListModel+='</tbody>';
        payableListModel+='</table>';
        payableListModel+='<table cellspacing="0" cellpadding="2" border="1" class="feeDetails mt20" data-genre="null">';
        payableListModel+='<thead>';
        payableListModel+='<tr>';
        payableListModel+='<td class="font-bold" data-genre="nullType">Feehead</td>';
        payableListModel+='<td class="font-bold" data-genre="nullInstallment" data-content="installment-header">Installment</td>';
        payableListModel+='<td class="font-bold" width="150" align="right" data-genre="nullTotal">Total Amount(<span class="icon-rupee"></span>)</td>';
        payableListModel+='</tr>';
        payableListModel+='</thead>';
        payableListModel+='<tbody>';
        payableListModel+='<tr>';
        payableListModel+='<td data-genre="nullType">'+feereceipt_data['feeHead']+'</td>';
        payableListModel+='<td data-genre="nullInstallment" data-content="installment">1</td>';
        payableListModel+='<td align="right" data-genre="nullTotal">'+moneyFormat(feereceipt_data['amount_paid'])+'</td>';
        payableListModel+='</tr>';
        payableListModel+='</tbody>';
        payableListModel+='</table>';
        payableListModel+='<table cellspacing="0" cellpadding="4" border="0" class="total">';
        payableListModel+='<tbody>';
        payableListModel+='<tr>';
        payableListModel+='<td colspan="6" style="text-transform: capitalize;"> Rupees '+NumberinWords(Math.abs(feereceipt_data['amount_paid']))+'</td>';
        payableListModel+='<td class="font-bold" width="150" align="right"><span class="icon-rupee"></span>'+moneyFormat(feereceipt_data['amount_paid'])+'</td>';
        payableListModel+='</tr>';
        payableListModel+='<tr></tr>';
        payableListModel+='</tbody>';
        payableListModel+='</table>';
        payableListModel+='</div>';
        payableListModel+='</div>';
        /*payableListModel+='<div class="col-sm-12 pl0" style="margin-top:120px;">';
        payableListModel+='<a class="btn blue-btn" target="_blank" href="'+APP_REDIRECT+'fee_receipt/'+feereceipt_data['fee_collection_id']+'"><i class="icon-right mr8"></i>Print</a>';
        payableListModel+='</div>';*/
        payableListModel+='</div>';
        $("#feeReceiptData").html(payableListModel);
    }
</script>