<!--popup-modal for alert -->
<div class="modal fade bs-example-modal-sm" id="popup_alert" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog alert-modal modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Heading</h4>
            </div>
            <div class="modal-body clearfix">
                Description...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue-btn btn-xs" data-dismiss="modal"><i class="icon-right mr8"></i>Ok</button>
            </div>
        </div>
    </div>
</div>

<!--popup-modal for confirm-->
<div class="modal fade bs-example-modal-sm" id="popup_confirm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog alert-modal modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Heading</h4>
            </div>
            <div class="modal-body clearfix">
                Description...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue-btn btn-xs" id="btnTrue" data-dismiss="modal" ><i class="icon-right mr8"></i>Ok</button>
                <button type="button" class="btn blue-light-btn btn-xs" data-dismiss="modal" ><i class="icon-times mr8"></i>Cancel</button>
            </div>
        </div>
    </div>
</div>

</section>
<script src="<?php echo BASE_URL;?>assets/js/bootstrap.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/modernizr.min.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/alertify.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/leftpanel.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/scripts.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/materialize.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/highcharts.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/funnel.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/exporting.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/jquery.scrolltabs.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/jquery.mousewheel.js"></script>
<script src="<?php echo BASE_URL;?>assets/js/jquery.jsonSuggest-2.js"></script>
<!-- chosen js -->
<script src="<?php echo BASE_URL; ?>assets/js/chosen.jquery.js"></script>
<!-- chosen js-->
<?php
if($main!='login')
{
    ?>
    <script src="<?php echo BASE_URL;?>assets/js/menuanimation.js"></script>
    <script src="<?php echo BASE_URL;?>assets/js/customvalidations.js"></script>
<?php
}
?>
</body>
</html>