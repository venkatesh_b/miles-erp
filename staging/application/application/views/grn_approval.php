<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-19
 * Time: 06:11 PM
 */
?>

<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Warehouse GRN Approval</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class=""  data-toggle="modal"><i class="icon-plus-circle" onclick="manageWarehouseGrn(this,0)"></i></a></li>
                <!--<li><a><i class="icon-filter"></i></a></li>-->

                <li><a></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="warehouse-gtn-list">
                    <thead>
                    <tr>
                        <th>GRN No.</th>
                        <th>Received By</th>
                        <th>Received On</th>
                        <th>Sent By</th>
                        <th>Sent On</th>
                        <th>No. of Items </th>
                        <th>Invoice</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <!--Modal-->

        <div class="modal fade" id="whGrnProducts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="600">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="whGrnModal"></h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">GRN No.</span>
                                    <p id="grnNoWrapper">----</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Invoice</span>
                                    <p id="invoiceWrapper">----</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Received By</span>
                                    <p id="receivedByWrapper">----</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Received On</span>
                                    <p id="receivedOnWrapper">----</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Sent By</span>
                                    <p id="sentByWrapper">----</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Sent On</span>
                                    <p id="sentOnWrapper">----</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">No of Items</span>
                                    <p id="noOfItemsWrapper">----</p>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Approval Status</span>
                                    <p id="approvalStatusWrapper">----</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 product-map-table p0" id="grnProductsWh">
                        </div>
                        <div class="col-sm-12 product-map-table p0 mt20" id="whGrnStatusChangeHistory">
                        </div>
                        <div class="col-sm-12 p0 mt20" id="whGRNStatusChangeFormWrapper">
                            <form name="whGRNStatusChangeForm" id="whGRNStatusChangeForm" >
                                <input type="hidden" name="hdnstockFlowId" id="hdnstockFlowId" value=""/>
                                <!--<div class="col-sm-12">
                                    <div class="input-field">
                                        <select id="approvalStatus" name="approvalStatus" >
                                            <option value="">--Select--</option>
                                            <option value="approved">Approve</option>
                                            <option value="rejected">Reject</option>
                                        </select>
                                        <label class="select-label">Status <em>*</em></label>
                                    </div>
                                </div>-->
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <textarea id="remarks" class="materialize-textarea validate mb0" name="remarks"></textarea>
                                        <label for="remarks">Comment <em>*</em></label>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue-btn" id="btnWHStatusChangeApprove" onclick="saveWHGrnStatusChange(this,'approved')"><i class="icon-right mr8"></i>Approve</button>
                        <button type="button" class="btn blue-btn" id="btnWHStatusChangeReject" onclick="saveWHGrnStatusChange(this,'rejected')"><i class="icon-right mr8"></i>Reject</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- InstanceEndEditable --></div>
</div>
<script>
    docReady(function(){
        buildWarehouseGrnDataTable();
    });
    function buildWarehouseGrnDataTable()
    {
        var ajaxurl=API_URL+'index.php/GrnApproval/warehouseGrnApprovalList';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#warehouse-gtn-list").dataTable().fnDestroy();
        $tableobj=$('#warehouse-gtn-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            "order": [[ 0, "desc" ]],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" data-target=\"#whGrnProducts\" data-toggle=\"modal\"  onclick=\"getGrnProduct(this,"+data.sequence_number+",'"+data.new_sequence_number+"',"+data.stockflow_id+")\">"+data.new_sequence_number+"</a></div>";
                    //return data.sequence_number;
                    return html;
                    //return data.sequence_number;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receiveName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receivedOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.requestedName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.sentOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.total;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    if(data.invoice_number.trim()=='' || data.invoice_number==null){
                        html='----';
                    }
                    else{
                        html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" onclick=\"downloadInvoice('"+data.invoice_attachment+"')\">"+data.invoice_number+"</a></div>";
                    }
                    return html;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    //data.approval_status= 'approval status';
                    if(data.approval_status==null || data.approval_status.trim()=='')
                        data.approval_status='----';
                    //var html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" data-target=\"#whGrnStatusChange\" data-toggle=\"modal\"  onclick=\"manageWHStatusChange(this,"+data.stockflow_id+",'"+data.new_sequence_number+"')\">"+data.approval_status+"</a></div>";
                    return data.approval_status;

                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function getGrnProduct(This,seqNum,newseqNum,stockflowId)
    {
        $('#whGrnStatusChangeModal').html('GRN No. '+newseqNum);
        $('#whGrnStatusChangeHistory').html('');
        $('#whGRNStatusChangeFormWrapper').hide();
        $('#btnWHStatusChangeApprove').hide();
        $('#btnWHStatusChangeReject').hide();

        $('#whGRNStatusChangeForm')[0].reset();
        $('#whGRNStatusChangeForm label').removeClass("active");
        $('#whGRNStatusChangeForm .select-label').removeClass("active");
        $('#whGRNStatusChangeForm input').removeClass("required");
        $('#whGRNStatusChangeForm select').removeClass("required");
        $('#whGRNStatusChangeForm textarea').removeClass("required");
        $('#whGRNStatusChangeForm span.required-msg').remove();
        $('#whGRNStatusChangeForm select').material_select();

        $('#grnNoWrapper').html('----');
        $('#invoiceWrapper').html('----');
        $('#receivedByWrapper').html('----');
        $('#receivedOnWrapper').html('----');
        $('#sentByWrapper').html('----');
        $('#sentOnWrapper').html('----');
        $('#noOfItemsWrapper').html('----');
        $('#approvalStatusWrapper').html('----');

        $('#hdnstockFlowId').val('');
        $('#whGrnModal').html('GRN No. '+newseqNum);
        $('#grnProductsWh').html('');
        var ajaxurl = API_URL + 'index.php/GrnApproval/getWareHouseGrnProducts';
        var params = {sequenceNumber:seqNum,stockflowId:stockflowId};
        var userID = $('#hdnUserId').val();
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({
            This: this,
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: 'list',
            onSuccess: function (response) {
                var stockhtml = '';
                var approval_status='';
                $('#hdnstockFlowId').val(stockflowId);
                if(response.data.details.sequence){
                    $('#grnNoWrapper').html(response.data.details.sequence.trim());
                }
                if(response.data.details.invoice_number){
                    $('#invoiceWrapper').html( "<a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" onclick=\"downloadInvoice('"+response.data.details.invoice_attachment+"')\">"+response.data.details.invoice_number+"</a>");



                }
                if(response.data.details.receiveName){
                    $('#receivedByWrapper').html(response.data.details.receiveName.trim());
                }
                if(response.data.details.receivedOn){
                    $('#receivedOnWrapper').html(response.data.details.receivedOn.trim());
                }
                if(response.data.details.requestedName){
                    $('#sentByWrapper').html(response.data.details.requestedName.trim());
                }
                if(response.data.details.sentOn){
                    $('#sentOnWrapper').html(response.data.details.sentOn.trim());
                }
                if(response.data.details.total){
                    $('#noOfItemsWrapper').html(response.data.details.total.trim());
                }
                if(response.data.details.approval_status){
                    approval_status=response.data.details.approval_status.trim();
                    $('#approvalStatusWrapper').html(approval_status);
                }

                stockhtml += "<table class=\"table table-responsive table-bordered mb0\">";
                stockhtml += "<thead>";
                stockhtml += "<tr class=\"\">";
                stockhtml += "<th>Product Name</th>";
                stockhtml += "<th>Quantity</th>";
                stockhtml += "</tr>";
                stockhtml += "</thead>";
                stockhtml += "<tbody>";
                if (response.status === true && response.data.products.length > 0) {
                    $.each(response.data.products, function (key, value) {
                        stockhtml += "<tr class=\"\">";
                        stockhtml += "<td>" + value.productName + "</td>";
                        stockhtml += "<td>" + value.quantity + "</td>";
                        stockhtml += "</tr>";
                    });
                }
                else {
                    stockhtml += "<tr ><td colspan='2'>No records found</td></tr>";
                }
                stockhtml += "</tbody>";
                stockhtml += "</table>";
                $('#grnProductsWh').html(stockhtml);

                var stockhtml = '';

                stockhtml += "<table class=\"table table-responsive table-bordered mb0\">";
                stockhtml += "<thead>";
                stockhtml += "<tr class=\"\">";
                stockhtml += "<th>Date</th>";
                stockhtml += "<th>User</th>";
                stockhtml += "<th>Comments</th>";
                stockhtml += "<th>Status</th>";
                stockhtml += "</tr>";
                stockhtml += "</thead>";
                stockhtml += "<tbody>";
                if (response.status === true && response.data.history.length > 0) {
                    //approval_status=response.data.history[0].approval_status;
                    $.each(response.data.history, function (key, value) {
                        stockhtml += "<tr class=\"\">";
                        stockhtml += "<td>" + value.created_on + "</td>";
                        stockhtml += "<td>" + value.name + "</td>";
                        stockhtml += "<td>" + value.remarks + "</td>";
                        stockhtml += "<td>" + value.approval_status + "</td>";
                        stockhtml += "</tr>";
                    });
                }
                else {
                    stockhtml += "<tr><td colspan='4' class='text-center'>No history found</td></tr>";
                }
                stockhtml += "</tbody>";
                stockhtml += "</table>";
                $('#whGrnStatusChangeHistory').html(stockhtml);
                if(approval_status==='' || approval_status.toUpperCase()=='PENDING'){
                    $('#whGRNStatusChangeFormWrapper').show();
                    $('#btnWHStatusChangeApprove').show();
                    $('#btnWHStatusChangeReject').show();
                }
                else{
                    $('#whGRNStatusChangeFormWrapper').hide();
                    $('#btnWHStatusChangeApprove').hide();
                    $('#btnWHStatusChangeReject').hide();
                }

            }
        });
    }
    function manageWHStatusChange(This,stockflowId,newseqNum)
    {
        $('#whGrnStatusChangeModal').html('GRN No. '+newseqNum);
        $('#whGrnStatusChangeHistory').html('');
        $('#whGRNStatusChangeFormWrapper').hide();
        $('#btnWHStatusChangeApprove').hide();
        $('#btnWHStatusChangeReject').hide();
        $('#whGRNStatusChangeForm label').removeClass("active");
        $('#whGRNStatusChangeForm .select-label').removeClass("active");
        $('#whGRNStatusChangeForm input').removeClass("required");
        $('#whGRNStatusChangeForm')[0].reset();
        $('#hdnstockFlowId').val('');
        var ajaxurl = API_URL + 'index.php/GrnApproval/warehouseGrnApprovalListHistory';
        var params = {stockflowId:stockflowId};
        var userID = $('#hdnUserId').val();
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({
            This: this,
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: 'list',
            onSuccess: function (response) {
                var stockhtml = '';

                stockhtml += "<table class=\"table table-responsive table-bordered mb0\">";
                stockhtml += "<thead>";
                stockhtml += "<tr class=\"\">";
                stockhtml += "<th>Date</th>";
                stockhtml += "<th>User</th>";
                stockhtml += "<th>Comments</th>";
                stockhtml += "<th>Status</th>";
                stockhtml += "</tr>";
                stockhtml += "</thead>";
                stockhtml += "<tbody>";
                if (response.status === true && response.data.length > 0) {
                    approval_status=response.data[0].approval_status;
                    $.each(response.data, function (key, value) {
                        stockhtml += "<tr class=\"\">";
                        stockhtml += "<td>" + value.created_on + "</td>";
                        stockhtml += "<td>" + value.name + "</td>";
                        stockhtml += "<td>" + value.remarks + "</td>";
                        stockhtml += "<td>" + value.approval_status + "</td>";
                        stockhtml += "</tr>";
                    });
                }
                else {
                    stockhtml += "<tr><td colspan='4' class='text-center'>No history found</td></tr>";
                }
                stockhtml += "</tbody>";
                stockhtml += "</table>";
                $('#whGrnStatusChangeHistory').html(stockhtml);
                if(approval_status==='' || approval_status.toUpperCase()=='PENDING'){
                    $('#whGRNStatusChangeFormWrapper').show();
                    $('#btnWHStatusChangeApprove').show();
                    $('#btnWHStatusChangeReject').show();
                }
                else{
                    $('#whGRNStatusChangeFormWrapper').hide();
                    $('#btnWHStatusChangeApprove').hide();
                    $('#btnWHStatusChangeReject').hide();
                }

            }
        });
    }
    function saveWHGrnStatusChange(This,status){
        $('#whGRNStatusChangeForm span.required-msg').remove();
        $('#whGRNStatusChangeForm input, .materialize-textarea').removeClass("required");
        var comments=$('#remarks').val().trim();
        var stockFlowId=$('#hdnstockFlowId').val().trim();
        var flag = 0;
        if(comments===''){
            $("#remarks").addClass("required");
            $("#remarks").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if(flag==0){
            notify('Processing', 'warning', 50);
            var ajaxurl = API_URL + 'index.php/GrnApproval/saveChangeStatus';
            var params={status:status,comments:comments,stockFlowId:stockFlowId};
            var userID = $('#hdnUserId').val();
            var method = 'POST';
            var action='list';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({
                This: This,
                method: method,
                requestUrl: ajaxurl,
                params: params,
                headerParams: headerParams,
                action: action,
                onSuccess: function (response) {
                    alertify.dismissAll();
                    if(response.status===true){
                        notify(response.message, 'success', 10);
                        $(this).attr("data-dismiss", "modal");
                        $('#whGrnProducts').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        buildWarehouseGrnDataTable();
                    }
                    else{
                        notify(response.message, 'error', 10);
                    }

                }
            });
        }
        else{
            return false
        }
    }
</script>