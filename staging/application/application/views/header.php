<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo BASE_URL;?>assets/images/faveicon.png" type="image/x-icon">
    <!-- TemplateBeginEditable name="doctitle" -->
    <title>::MILES::</title>
    <!-- TemplateEndEditable -->
    <link href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL;?>assets/css/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL;?>assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo BASE_URL;?>assets/css/icomoon.css" rel="stylesheet">
    <link href="<?php echo BASE_URL;?>assets/css/menu.css" rel="stylesheet">
    <link href="<?php echo BASE_URL;?>assets/css/alertify.css" rel="stylesheet">
    <link href="<?php echo BASE_URL;?>assets/css/materialize.css" rel="stylesheet">
    <link href="<?php echo BASE_URL;?>assets/css/adjustments.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/scrolltabs.css">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/jquery.mCustomScrollbar.min.css" />
    <link href="<?php echo BASE_URL;?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo BASE_URL;?>assets/css/styles.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/jquery-ui-1.8.9.custom.css" />
    <!-- chosen css-->
    <link href="<?php echo BASE_URL; ?>assets/css/chosen.css" rel="stylesheet">
    <!-- chosen css -->
    <!--[if lt IE 9]>
    <script src="<?php echo BASE_URL;?>assets/js/html5shiv.js"></script>
    <script src="<?php echo BASE_URL;?>assets/js/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo BASE_URL;?>assets/js/jquery.min.js"></script>
    <script src="<?php echo BASE_URL;?>assets/js/jquery-ui-1.11.0.js"></script>
    <script>
        var BASE_URL='<?php echo BASE_URL; ?>';
        var API_URL='<?php echo API_URL; ?>';
        var APP_REDIRECT='<?php echo APP_REDIRECT; ?>';
        $pageUrl='<?php echo $main;?>';
        $accessToken='<?php echo $accessToken;?>';
        $inputRequiredMessage='Required';
        $listRequiredMessage='Required';
        $phoneMinLengthMessage='Should be min 8 digits';
        $phoneMaxLengthMessage='Should be max 15 digits';
        $tableobj='';
        if($accessToken != '')
        {
            $(window).load(function() {
                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");
            });
        }
    </script>
    <script src="<?php echo BASE_URL;?>assets/js/menu.js"></script>
    <script src="<?php echo BASE_URL;?>assets/js/commonajax.js"></script>
    <script src="<?php echo BASE_URL;?>assets/js/tss_access.js"></script>
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
</head>
<body>
<?php
if($accessToken)
{
    echo '<div class="se-pre-con"></div>';
}
?>
<section>