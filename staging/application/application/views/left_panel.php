<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 10/2/16
 * Time: 2:31 AM
 */
?>
<div class="headerbar" id="header">
    <div class="logopanel"> <a href="#"> <img src="<?php echo BASE_URL;?>assets/images/milesforce_logo.png" style="width: 155px;margin-top: 6px;" class="logo" alt="logo" /> </a> </div>
    <div class="header-right">
        <div class="dropdown mheader-dropdown">
            <button data-toggle="dropdown" type="button" class="btn btn-primary dropdown-toggle" aria-expanded="false">
                <span class="user">
                    <?php echo $userData['Name']; ?>
                    <label><?php echo $userData['Role']; ?></label>
                </span>
                <span class="profile-pic">
                    <?php
                    if($userData['profileImage'] == '')
                    {
                        $profileImage=BASE_URL."assets/images/user-icon.png";
                    }
                    else
                    {
                        $profileImage=$userData['profileImage'];
                    }
                    ?>
                    <img src="<?php echo $profileImage;?>" class="img-circle">
                </span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="javascript:;" onclick="logout()">Log Out</a></li>
            </ul>
        </div>
    </div>

        <div class="dropdown BranchName-dropdown pull-right">
            <select id="userBranchList">
                <!--<option value="" selected>Select Branch</option>-->
            </select>
        </div>

</div>
<?php
    if($main!='nomenu')
    {
    ?>
    <div class="pageheader">
        <a class="menutoggle"><i class="fa fa-bars"></i></a>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb" id="breadCrumb">

            </ol>
        </div>
    </div>

        <div class="leftpanel" id="leftNav">
        <div class="leftpanelinner">
            <div class="nav-slider" id="nav-slider">
                <div class="slider-content" id="scollBox">
                    <ul class="nav nav-bracket tss-app-menu" id="left-panel-main-menus-ul">
                        <li class="search-menu" style="height:41px">
                            <a href="javascript:;" class="showSearch"><i class="fa fa-search "></i></a>
                            <div class="searchbox">
                                <input id="menuSearchDemo" type="text" placeholder="Search" class="ui-autocomplete-input" autocomplete="off">
                                <i class="fa fa-search pull-right"></i>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php
    }
    ?>

    <script type="text/javascript">
        docReady(function(){
            var breadCrumb = '<li><a href="javascript:;">Home</a></li>';
            var contextbreadCrumb = '<li class="active">'+$context+'</li>';
            var currentEle=$('li[tss-context="'+$context+'"]');
            if(currentEle.parent().parent().hasClass('nav-parent'))
            {
                var parentElement=currentEle.parent().parent().find('span').html();
                if($context != parentElement)
                    contextbreadCrumb = '<li><a href="javascript:;">'+parentElement+'</a></li>'+contextbreadCrumb;
            }
            breadCrumb = breadCrumb+contextbreadCrumb;
            $("ol#breadCrumb").html(breadCrumb);
        });
</script>