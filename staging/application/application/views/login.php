<script>
    var GOOGLE_CLIENT_ID='<?php echo GOOGLE_CLIENT_ID ?>';
    var GOOGLE_CLIENT_SECRET='<?php echo GOOGLE_CLIENT_SECRET ?>';
    var GOOGLE_REDIRECT_URI='<?php echo GOOGLE_REDIRECT_URI ?>';
    var GOOGLE_API_KEY='<?php echo GOOGLE_API_KEY ?>';
</script>
<style>
    /*body {
        pointer-events:none;
    }*/
</style>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/google-api.js"></script>
<div class="wrapper">
    <div class="miles-bg-image">
        <div class="login-page">
            <div class="logo"> <img src="<?php echo BASE_URL; ?>assets/images/milesforce_logo.png" style="width: 155px;" > </div>
            <p class=" mtb f12 display-inline-block">Login with Company Gmail Address</p>
            <div class="display-inline-block">
                <a class="btn btn-danger btn-sm google-btn has-spinner" href="javascript:;" onclick="login()">
                    <span class="p10 google-btn-text"><img src="<?php echo BASE_URL; ?>assets/images/google2.png">Login With Google</span>
                    <span class="spinner-loading">Loading<img src="<?php echo BASE_URL; ?>assets/images/loadingAnimation.gif" alt=""></span>
                </a>
            </div>
            <div class="display-inline-block mt10">
                <a class="" href="javascript:;" onclick="loginwithEmail()">
                    <span class="p10 google-btn-text">Custom Login</span>
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){ //on page all
        $('body').css('pointer-events','all'); //activate all pointer-events on body
    });
    function loginwithEmail()
    {
        var popoverContent='<form method="post" id="loginForm" novalidate="novalidate">';
        popoverContent+='<div class="col-sm-12">';
        popoverContent+='<div class="input-field">';
        popoverContent+='<input id="txtLoginEmail" type="text" class="validate">';
        popoverContent+='<label for="txtLoginEmail" class=""> Email <em>*</em></label>';
        popoverContent+='</div>';
        popoverContent+='</div>';
        popoverContent+='<div class="col-sm-12">';
        popoverContent+='<div class="input-field">';
        popoverContent+='<input id="pwdLoginPassword" type="password" class="validate">';
        popoverContent+='<label for="pwdLoginPassword" class=""> Password <em>*</em></label>';
        popoverContent+='</div>';
        popoverContent+='</div>';
        popoverContent+='</form>';
        customConfirmAlert("Login",popoverContent);
        $("#popup_confirm #btnTrue").attr("onclick","CheckCustomlogin()");
        $("#popup_confirm #btnTrue").removeAttr("data-dismiss");
    }
    function CheckCustomlogin()
    {
        var email=$("#txtLoginEmail").val();
        var password= $.trim($("#pwdLoginPassword").val());
        var regx_Email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var flag=0;

        $('#loginForm span.required-msg').remove();
        $('#loginForm input').removeClass("required");

        if (email == '')
        {
            $("#txtLoginEmail").addClass("required");
            $("#txtLoginEmail").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if (email!='' && regx_Email.test($('#txtLoginEmail').val()) === false)
        {
            $("#txtLoginEmail").addClass("required");
            $("#txtLoginEmail").after('<span class="required-msg">Invalid E-mail</span>');
            flag = 1;
        }
        if (password == '')
        {
            $("#pwdLoginPassword").addClass("required");
            $("#pwdLoginPassword").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if (password.length < 6)
        {
            $("#pwdLoginPassword").addClass("required");
            $("#pwdLoginPassword").after('<span class="required-msg">Cannot be less than 6</span>');
            flag = 1;
        }
        if(flag == 0)
        {
            var ajaxurl=BASE_URL+'index.php/authorize/customLogin';
            var headerParams = {context:'default'};
            var params = {loginEmail: email, loginPassword: password};
            $.ajax({
                async:false,
                type:"POST",
                url:ajaxurl,
                dataType:"json",
                data:params,
                headers: headerParams,
                beforeSend:function()
                {
                    $(".has-spinner").addClass('active');
                    $('.has-spinner .google-btn-text').hide();
                },
                success:function(response)
                {
                    if(response == -1)
                    {
                        $(".has-spinner").removeClass('active');
                        $('.has-spinner .google-btn-text').show();
                        notify('You are not authorised user','error' ,10);
                        gapi.auth.signOut();
                    }
                    else
                    {
                        $context='default';
                        headerParams = {context:$context,Authorizationtoken:response.data['accessToken'],user:response.data['user']};
                        ajaxurl=API_URL+'index.php/ApplicationMenu/getApplicationMenu/Userid/'+response.data['user'];
                        commonAjaxCall({This:this,requestUrl:ajaxurl,headerParams:headerParams,onSuccess:userAccessDetails}).then(function()
                            {
                                window.location=APP_REDIRECT+response.message;
                            }
                        );
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown)
                {
                    notify('Something went wrong. Please try again','error' ,10);
                }
            });
        }

    }
</script>