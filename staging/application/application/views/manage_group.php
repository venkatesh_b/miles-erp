<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 23/6/16
 * Time: 12:02 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnGroupId" value="<?php echo $Id  ?>" />
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />

    <div class="content-header-wrap">
        <h3 class="content-header" id="headerName">Create Group</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="" href="<?php echo APP_REDIRECT;?>group"><i class="icon-times" ></i></a></li>
            </ul>
        </div>
    </div><!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <form method="post" id="GroupCreationForm">
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 pl0">
                        <div class="input-field">
                            <input id="txtGroupName" class="validate" type="text">
                            <label class="" for="txtGroupName">Name <em>*</em></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                    <div class="col-sm-6 pl0">
                        <div class="input-field">
                            <textarea id="txaGroupDescription" class="materialize-textarea"></textarea>
                            <label for="txaGroupDescription">Description</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                    <div class="col-sm-4 reports-filter-wrapper p0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Branch</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:;"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:;"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="branchFilter"></div>
                    </div>
                    <div class="col-sm-4 reports-filter-wrapper p0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Course</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:;"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:;"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="courseFilter"></div>
                    </div>
                    <div class="col-sm-4 reports-filter-wrapper p0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Source</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:;"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:;"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="sourceFilter">  </div>
                    </div>
                </div>
                <div class="col-sm-12 p0">
                    <div class="col-sm-12 reports-filter-wrapper mtb10 p0">
                        <div class="multiple-checkbox reports-filters-check">
                            <label class="heading-uppercase">Tag</label>
                            <span class="filter-selectall-wrap"><a class="ml5 reportFilter-selectall tooltipped" data-position="top" data-delay="50" data-tooltip="Select All" href="javascript:;"><i class="icon-right green"></i></a><a class="reportFilter-unselectall tooltipped" data-position="top" data-delay="50" data-tooltip="Unselect All" href="javascript:;"><i class="icon-times red"></i></a></span>
                        </div>
                        <div class="boxed-tags" id="tagFilter"></div>
                    </div>
                </div>
                <div class="col-sm-12 p0 mtb10">
                    <div class="col-sm-6 mt15 p0">
                        <a class="btn blue-btn btn-sm" href="javascript:;" onclick="getGroupContactsData('add')">Proceed</a>
                    </div>
                </div>
                <div class="col-sm-12 mt20 p0" >
                    <!--<div>
                        <h4 class="heading-uppercase mb5 display-inline-block">Shortlisted list from the group (<span id="contactsCount">0</span>)</h4>
                    </div>-->
                    <table class="table table-inside table-custom branchwise-report-table" id="GroupContactsList">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th class="border-r-none">Tags</th>
                                <th class="no-sort">&nbsp;</th>
                            </tr>
                        </thead>

                    </table>
                </div>
                <div class="col-sm-12 p0 mtb10">
                    <div class="col-sm-6 mt15 p0">
                        <a id="btnAddEditGroup" class="btn blue-btn btn-sm" href="javascript:;" onclick="CreateContactGroup(this)">Add Group</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $context='Groups';
    $pageUrl='group';
    var groupId='<?php echo $Id; ?>';
    var branchId = [];
    var courseId = [];
    var sourceId = [];
    var tagId = [];
    docReady(function()
    {
        if(groupId == 0)
        {
            $('#btnAddEditGroup').html('Add Group');
        }
        else
        {
            $('#btnAddEditGroup').html('Edit Group');
        }
        ConfiguredGroupItems();
    });

    function ConfiguredGroupItems()
    {
        alertify.dismissAll();
        notify('Processing..', 'warning', 10);
        var userId = $('#hdnUserId').val();
        var action = 'add';
        var groupItems=['Branch','Course','Contact Source','Tag'];
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

        var ajaxurl=API_URL+'index.php/Campaign/getCampaignGroupItems';
        var params = {groupItems:groupItems};
        commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: ConfiguredGroupItemsReponse});
    }

    function ConfiguredGroupItemsReponse(response)
    {
        alertify.dismissAll();
        if(response.status==false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {

            var responseData=response.data;
            if(Object.keys(responseData).length == 0 || responseData == '')
            {
                notify('No items found', 'error', 10);
            }
            else
            {
                $.each(responseData,function(key, groupData){
                    var respdata='';
                    for(var g=0;g<groupData.length;g++)
                    {
                        respdata += '<a class="active" data-id="'+groupData[g]['Id']+'" href="javascript:;">'+groupData[g]['Name']+'</a>';
                    }
                    if(key == 'Branch')
                    {
                        $('#branchFilter').html(respdata);
                    }
                    else if(key == 'Course')
                    {
                        $('#courseFilter').html(respdata);
                    }
                    else if(key == 'Contact Source')
                    {
                        $('#sourceFilter').html(respdata);
                    }
                    else if(key == 'Tag')
                    {
                        $('#tagFilter').html(respdata);
                    }
                });
                if(groupId == 0)
                {

                }
                else
                {
                    EditGroup();
                }
            }
        }
    }
    var gruoupconcats=new Array();
    function insertoarray(lead){
        if ($.inArray(lead, gruoupconcats) != -1)
        {
            // found it
        }
        else {
            gruoupconcats.push(lead);
        }
    }
    function removefromarray(lead){
        if ($.inArray(lead, gruoupconcats) != -1)
        {
            // found it
            gruoupconcats.splice( $.inArray(lead, gruoupconcats), 1 );
        }
    }
    function getGroupContactsData(type)
    {
        alertify.dismissAll();
        var groupName= $.trim($("#txtGroupName").val());
        var groupItems = [];
        var error = true;
        var flag=0;
         branchId = [];
         courseId = [];
         sourceId = [];
         tagId = [];
         gruoupconcats=[];
        $('#GroupCreationForm span.required-msg').remove();
        $('#GroupCreationForm input').removeClass("required");

        $("#branchFilter > .active").each(function() {
            error = true;
            branchId.push($(this).attr('data-id'));
        });

        $("#courseFilter > .active").each(function() {
            error = true;
            courseId.push($(this).attr('data-id'));
        });
        $("#sourceFilter > .active").each(function() {
            error = true;
            sourceId.push($(this).attr('data-id'));
        });
        $("#tagFilter > .active").each(function() {
            error = true;
            tagId.push($(this).attr('data-id'));
        });
        /*if(groupName == '')
        {
            $("#txtGroupName").val('');
            $("#txtGroupName").addClass("required");
            $("#txtGroupName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }*/
        if(branchId.length == 0)
        {
            notify('Choose atleast one branch', 'error', 10);
            flag=1;
        }
        /*else if(courseId.length == 0 && sourceId.length == 0 && tagId.length == 0)
        {
            notify('Choose atleast one group item', 'error', 10);
            flag=1;
        }
*/
        if(flag == 1)
        {
            return false;
        }
        else
        {
            //process the request
            notify('Processing..', 'warning', 10);
            var userId = $('#hdnUserId').val();
            var action = 'add';
            var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
            var ajaxurl=API_URL+'index.php/Campaign/getFilteredGroupContacts';
            var groupData={branch:branchId,course:courseId,source:sourceId,tag:tagId};
            var params = {groupName:groupName,groupData:groupData};
            if(groupId==0){

            }
            else if(groupId!=0 && type=='edit'){
                action = 'edit';
                ajaxurl=API_URL+'index.php/Campaign/getFilteredGroupContactsEdit';
                params.groupId=groupId;
            }

            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response){
                if(response.status===true){
                    $.each(response.data, function (keys, values) {

                        insertoarray(values.lead_id+'@@@@'+values.email);
                    });

                    getGroupContactListDatatable(userId,action,headerParams,ajaxurl,groupData,params);
                }
            }});

            }
        }

    function getGroupContactListDatatable(userId,action,headerParams,ajaxurl,groupData,params){
        $("#GroupContactsList").dataTable().fnDestroy();
        $tableobj = $('#GroupContactsList').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                verifyAccess();
                buildpopover();
            },
            "footerCallback": function ( row, data, start, end, display ) {

                $('#GroupContactsList_wrapper div.DTHeader').html('Shortlisted list from the group (<span id="contactsCount">'+data.length+'</span>)');


            },
            dom: "<'DTHeader'>Bfrtip",
            bInfo: false,
            "serverSide": false,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No Records Found",
                "sZeroRecords": "No Records Found",
                "sProcessing":"<img src='"+BASE_URL+"assets/images/preloader.gif'>"
            },
            "bProcessing": true,
            ajax: {
                url:ajaxurl,
                type:'POST',
                data:params,
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [
                {
                    "targets": 'no-sort',
                    "orderable": false
                }
            ],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.name;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.email;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var leadTagsData='';
                    leadTagsData+='<p class="max-width130px label-tags ellipsis popper" data-toggle="popover" data-title="Tags" data-placement="top" data-trigger="hover">';
                    var tagsList=data.tags.split(',');
                    for(var i=0;i<2;i++)
                    {
                        if(tagsList[i] && i==0)
                        {
                            leadTagsData+='<label class="label label-success font-normal p5 f12 mr5">'+tagsList[i]+'</label>';
                        }
                        else if(tagsList[i] && i==1)
                        {
                            leadTagsData+='<label class="label label-info font-normal p5 f12 mr5">'+tagsList[i]+'</label>';
                        }
                    }
                    leadTagsData+='</p>';
                    leadTagsData+='<div class="popper-content hide"><div class="plr15">';
                    leadTagsData+='<p><label class="popover-label ash f14">'+data.tags+'</label></p>';
                    leadTagsData+='</div></div>';
                    return leadTagsData;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var trow='';
                    trow+='<div class="checklist-item float-none">';
                    trow+='<input type="checkbox" id="groupContactItem_'+data.lead_id+'" class="filled-in" name="groupContactItem[]" checked="checked" data-value="'+data.email+'" value="'+data.lead_id+'" onchange="groupContactItemCheck(this)">';
                    trow+='<label for="groupContactItem_'+data.lead_id+'"></label>';
                    trow+='</div>';
                    return trow;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
        alertify.dismissAll();
    }
    function GroupItemsReponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message,'error',10);
        }
        else
        {
            notify(response.message,'success',10);
            var contactsData=response.data;
            $("#GroupContactsList tbody").html('');
            if(contactsData == '' || contactsData.length == 0)
            {
                notify('No contacts found','success',10);
            }
            else
            {
                var trow='';
                $("#contactsCount").html(contactsData.length);
                for(var i=0;i < contactsData.length; i++)
                {
                    trow='<tr>';
                    trow+='<td>'+contactsData[i]['name']+'</td>';
                    trow+='<td>'+contactsData[i]['email']+'</td>';
                    trow+='<td>'+contactsData[i]['tags']+'</td>';
                    trow+='<td>';
                    trow+='<div class="checklist-item float-none">';
                    trow+='<input type="checkbox" id="groupContactItem_'+contactsData[i]['lead_id']+'" class="filled-in" name="groupContactItem[]" checked="checked" data-value="'+contactsData[i]['email']+'" value="'+contactsData[i]['lead_id']+'">';
                    trow+='<label for="groupContactItem_'+contactsData[i]['lead_id']+'"></label>';
                    trow+='</div>';
                    trow+='</td>';
                    trow+='</tr>';
                    $("#GroupContactsList tbody").append(trow);
                }
            }
        }
    }
    function groupContactItemCheck(This){
        if($(This).is(":checked")===true){
            insertoarray($(This).val()+'@@@@'+$(This).attr('data-value'));
        }
        if($(This).is(":checked")===false){
            removefromarray($(This).val()+'@@@@'+$(This).attr('data-value'));
        }
    }


    function CreateContactGroup(This)
    {
        alertify.dismissAll();

        $('#GroupCreationForm span.required-msg').remove();
        $('#GroupCreationForm input').removeClass("required");
        $('#GroupCreationForm select').removeClass("required");
        $('#GroupCreationForm textarea').find('label').addClass("active");
        $('#GroupCreationForm textarea').removeClass("required");
        var groupName= $.trim($("#txtGroupName").val());
        var groupDescription = $.trim($("#txaGroupDescription").val());
        var selectedContacts=[];
        var action = 'add';
        var userId = $('#hdnUserId').val();
        var flag=0;
        if(groupName == '')
        {
            $("#txtGroupName").val('');
            $("#txtGroupName").addClass("required");
            $("#txtGroupName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else
        {
            if($("#GroupContactsList tbody tr").length == 0)
            {
                notify('No contacts found to add in group', 'error', 10);
            }
            else
            {
                $.each( gruoupconcats, function( key, value ) {
                    var valueSplit=value.split('@@@@');

                    selectedContacts.push({
                        id: valueSplit[0],
                        email: valueSplit[1]
                    });
                });

                /*for(var k=0;k<gruoupconcats.length;k++){
                    //selectedContacts.push($(this).val());
                    var idE="#groupContactItem_"+gruoupconcats[k];
                    selectedContacts.push({
                        id: $(idE).val(),
                        email: $(idE).attr("data-value")
                    });
                    //selectedContacts[$(this).val()] = $(this).attr("data-value");
                    //selectedContacts.push(obj);
                }*/

                if(selectedContacts.length == 0)
                {
                    notify('Please select atleast one contact from the list', 'error', 10);
                }
                else
                {

                    var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
                    var ajaxurl=API_URL+'index.php/Campaign/addCampaignGroup';
                    var groupData={branch:branchId,course:courseId,source:sourceId,tag:tagId};
                    var params = {groupName:groupName,groupDescription:groupDescription,groupData:groupData,selectedContacts:selectedContacts};
                    if(groupId==0){
                        notify('Adding contacts', 'success', 50);
                    }
                    else if(groupId!=0){
                        notify('Updating contacts', 'success', 50);
                        action = 'edit';
                        ajaxurl=API_URL+'index.php/Campaign/editCampaignGroup';
                        params.groupId=groupId;
                    }
                    commonAjaxCall({This: This, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: AddCampaignGroupResponse});
                }
            }
        }
    }

    function AddCampaignGroupResponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message,'error',10);
        }
        else
        {
            notify(response.message,'success',10);
            window.location.href = APP_REDIRECT + "group";
        }
    }



    function EditGroup()
    {
        //groupId
        $("#headerName").html("Edit Group");
        alertify.dismissAll();
        notify('Processing..', 'warning', 10);
        var userId = $('#hdnUserId').val();
        var action = 'edit';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};

        var ajaxurl=API_URL+'index.php/Campaign/getGroupDetailsById';
        var params = {groupId:groupId};
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: GroupDetailsResponse});
    }
    function GroupDetailsResponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message, 'error', 10);
            setTimeout(function () {
                window.location.href = APP_REDIRECT + "group";
            }, 1500);

        }
        else
        {
            var group_basic = response.data['basic'];
            var group_items = response.data['items'];
            $("#txtGroupName").val(group_basic['name']);
            $('#txtGroupName').next('label').addClass("active");
            $('#branchFilter').find('a').removeClass('active');
            $('#courseFilter').find('a').removeClass('active');
            $('#sourceFilter').find('a').removeClass('active');
            $('#tagFilter').find('a').removeClass('active');
            if(group_basic['description']!='')
            {
                $("#txaGroupDescription").val(group_basic['description']);
                $('#txaGroupDescription').next('label').addClass("active");
            }
            $.each(group_items, function (key, value) {
                if(value.item_type=='branch'){

                    var branchItems=value.items.split(',');
                    for(var b=0;b<branchItems.length;b++){
                        $('#branchFilter').find('a[data-id="'+branchItems[b]+'"]').addClass('active');
                    }

                }
                else if(value.item_type=='course'){

                    var Items=value.items.split(',');
                    for(var b=0;b<Items.length;b++){
                        $('#courseFilter').find('a[data-id="'+Items[b]+'"]').addClass('active');
                    }
                }
                else if(value.item_type=='source'){

                    var Items=value.items.split(',');
                    for(var b=0;b<Items.length;b++){
                        $('#sourceFilter').find('a[data-id="'+Items[b]+'"]').addClass('active');
                    }
                }
                else if(value.item_type=='tag'){

                    var Items=value.items.split(',');
                    for(var b=0;b<Items.length;b++){
                        $('#tagFilter').find('a[data-id="'+Items[b]+'"]').addClass('active');
                    }
                }
            });
            getGroupContactsData('edit')

        }
    }

    /*function buildGroupContactsDataTable()
    {
        var ajaxurl=API_URL+'index.php/Campaign/GroupContactList';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        $("#group-list").dataTable().fnDestroy();
        $tableobj=$('#group-list').DataTable( {
            "fnDrawCallback": function()
            {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                }
                else
                {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No groups found",
                "sZeroRecords": "No groups found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax:
            {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response)
                {
                    DTResponseerror(response);
                }
            },
            "columnDefs":
                [
                    {
                        "targets": 'no-sort',
                        "orderable": false
                    }
                ],
            columns:
                [
                    {
                        data: null, render: function ( data, type, row )
                        {
                            //return data.name;
                            return (data.name == null || data.name == '') ? "----":data.name;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            return (data.email == null || data.email == '') ? "----":data.email;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            //return data.tags;
                            return (data.tags == null || data.tags == '') ? "----":data.tags;
                        }
                    },
                    {
                        data: null, render: function ( data, type, row )
                        {
                            var contactData='';
                            contactData='<div class="checklist-item float-none">';
                            contactData+='<input type="checkbox" id="groupContactItem_'+data.lead_id+'" class="filled-in" name="groupContactItem[]" checked="checked" data-value="'+data.email+'" value="'+data.lead_id+'">';
                            contactData+='<label for="groupContactItem_'+data.lead_id+'"></label>';
                            return data.contacts;
                        }
                    }
                ]
        });
        DTSearchOnKeyPressEnter();
    }*/
    function isElementExistInArray(Element,ElementArray){
        if ($.inArray(Element, ElementArray) != -1)
        {
           return true;
        }
        else{
            return false;
        }
    }
</script>