<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">Permissions</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="text-dec-none" href="<?php echo APP_REDIRECT;?>roles"><i class="icon-times"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body manage-role" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix" access-element="manage">
            <div class="col-sm-12 p0">
                <div class="col-sm-3 p0">
                    <div class="input-field ">
                        <select id="ddlManageRoles">

                        </select>
                        <label for="ddlManageRoles" class="select-label">Role Name</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 p0">
                <div class="col-sm-6 p0 mtb10" >
                    <!--<div class="input-field">-->
                        <p class=" label-text">Role Description</p>
                        <label class="label-data" id="roleDescription"></label>
                    <!--</div>-->
                    <input type="hidden" id="hdnRoleId" value="<?php echo $Id  ?>" />
                    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
                </div>
            </div>
                <h4 class="heading-uppercase display-inline-block">Role Access</h4>
                <span class="inline-checkbox pull-right">
                    <input id="checkUncheck" class="filled-in" type="checkbox">
                    <label for="checkUncheck" class="acc-info-label text-uppercase">Check/Uncheck All</label>
                </span>

            <div class="col-sm-12 clearfix white-bg" id="rolePermissionsSection">
                <!--menu-wrapper start-->

                <!--menu-wrapper end-->
            </div>

            <div class="col-sm-12 white-bg p20">
                <div class="" >
                    <button type="button" class="btn blue-btn" onclick="confirmSaveRolePermission('<?php echo $Id ?>')"><i class="icon-right mr8"></i>Save</button>
                    <button class="btn blue-light-btn" type="button" onclick="window.location='<?php echo APP_REDIRECT;?>roles'"><i class="icon-times mr8"></i>Cancel</button>
                </div>
            </div>
        </div>
    </div><!-- InstanceEndEditable -->
</div>
<script type="text/javascript">
    $context='Role Management';
    $pageUrl='roles';
    var roleId='<?php echo $Id; ?>'; roleName='';
    var chkFlag = 0;
    docReady(function(){
        getRolePermissions();
        $("#checkUncheck").change(function () {
            $("#rolePermissionsSection input:checkbox").prop('checked', $(this).prop("checked"));
        });
     });
    /*$("input[type='checkbox']").change(function () {
        chkFlag = 1;
        console.log(chkFlag);
    });*/
    $('.manage-role').on('change', 'input[type="checkbox"]', function() {
        chkFlag = 1;
    });
     function getRolePermissions(){
        var userID=$('#hdnUserId').val();
           var headerParams = {action:'manage',context:'Role Management',serviceurl:'roles',pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
           commonAjaxCall({
               This:this,
               asyncType:true,
               requestUrl:API_URL+'index.php/UserRole/roleComponentPermissions/roleId/<?php echo $Id; ?>',
               action:'manage',
               headerParams:headerParams,
               onSuccess:renderDataToRolePermissions
           });
    }
     
function renderDataToRolePermissions(rolePermissionsResponse){

    roleId=rolePermissionsResponse.data.RoleDetails[0].reference_type_value_id;
    roleName=rolePermissionsResponse.data.RoleDetails[0].value;
    var roleDescription=rolePermissionsResponse.data.RoleDetails[0].description;
    if(roleDescription == null || roleDescription == '')
        roleDescription = '----';
    $("#roleDescription").html(roleDescription);
    var userID=$('#hdnUserId').val();
    var headerParams = {action:'manage',context:'Role Management',serviceurl:'roles',pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
    commonAjaxCall({
        This:this,
        requestUrl:API_URL+'index.php/UserRole/rolesList',
        action:'',
        headerParams:headerParams,
        onSuccess:renderDataToRoles
    });

    var is_admin= 0,disabled='',actionStatus='';
    /*if(roleName == 'Superadmin')
    {
        is_admin=1;
    }
    else
    {
        disabled='disabled';
    }*/

    var rolePermissions=rolePermissionsResponse.data.RolePermissions;
    var rolePermissionsContent='';

    if(rolePermissions.length == 0)
    {
        rolePermissionsContent+='<div class="menu-wrapper clearfix">No Menu found</div>';
    }
    else
    {
        for(var rp=0;rp<rolePermissions.length;rp++)
        {
            if((rolePermissions[rp].actions.length>0) || ((typeof(rolePermissions[rp].submenu)!=undefined) && rolePermissions[rp].submenu.length>0))
            {
                rolePermissionsContent+='<div class="menu-wrapper clearfix">';
                rolePermissionsContent+='<div class="menu-item permission-menu">';
                rolePermissionsContent+='<div class="ordno">';
                rolePermissionsContent+='<label for="'+rolePermissions[rp].app_menu_id+'">Order No.</label>';
                rolePermissionsContent+='<input id="'+rolePermissions[rp].app_menu_id+'" type="text" class="validate menuOrder" value="'+rolePermissions[rp].menu_order+'"/>';

                rolePermissionsContent+='</div>';
                rolePermissionsContent+='<div class="menu-text">';
                rolePermissionsContent+='<span class="inline-checkbox">';
                rolePermissionsContent+='<span class="menu-check mmenu">';
                rolePermissionsContent+=rolePermissions[rp].name;
                rolePermissionsContent+='</span>';
                rolePermissionsContent+='</span>';
                rolePermissionsContent+='</div>';
                rolePermissionsContent+='</div>';
                if(typeof (rolePermissions[rp].actions) && rolePermissions[rp].actions.length>0)
                {
                    rolePermissionsContent+='<div class="permissions-menu-actions clearfix pl100">';
                    rolePermissionsContent+='<div class="col-sm-12 pl0">';
                    rolePermissionsContent+='<span class="pull-left mr15 mt5 gray">Actions</span>';
                    for(var rpa=0;rpa<rolePermissions[rp].actions.length;rpa++)
                    {
                        var actionStatus='';
                        if(is_admin == 1)
                        {
                            actionStatus="checked='checked' disabled";
                        }
                        else if(rolePermissions[rp].actions[rpa].isEnabled == 1)
                        {
                            actionStatus="checked='checked'";
                        }

                        rolePermissionsContent+='<span class="inline-checkbox">';
                        rolePermissionsContent+='<input id="mncreate_'+rolePermissions[rp].actions[rpa].actionId+'" class="filled-in role-manage" type="checkbox" value="'+rolePermissions[rp].actions[rpa].actionId+'" '+actionStatus+' >';
                        rolePermissionsContent+='<label for="mncreate_'+rolePermissions[rp].actions[rpa].actionId+'">'+rolePermissions[rp].actions[rpa].actionName+'</label>';
                        rolePermissionsContent+='</span>';
                    }
                    rolePermissionsContent+='</div>';
                    rolePermissionsContent+='</div>';
                }
                if( (typeof(rolePermissions[rp].submenu) && rolePermissions[rp].submenu!=undefined) && rolePermissions[rp].submenu.length > 0)
                {
                    for(var rps=0;rps<rolePermissions[rp].submenu.length;rps++)
                    {
                        if(typeof (rolePermissions[rp].submenu[rps].actions) && rolePermissions[rp].submenu[rps].actions.length>0)
                        {

                            rolePermissionsContent+='<div class="menu-item permission-menu pl100">';
                            rolePermissionsContent+='<div class="ordno">';
                            rolePermissionsContent+='<label for="'+rolePermissions[rp].submenu[rps].app_menu_id+'">Order No.</label>';
                            rolePermissionsContent+='<input id="'+rolePermissions[rp].submenu[rps].app_menu_id+'" type="text" class="validate menuOrder" value="'+rolePermissions[rp].submenu[rps].menu_order+'" />';
                            rolePermissionsContent+='</div>';
                            rolePermissionsContent+='<div class="menu-text">';
                            rolePermissionsContent+='<span class="inline-checkbox">';
                            rolePermissionsContent+='<span class="menu-check">';
                            rolePermissionsContent+=rolePermissions[rp].submenu[rps].name
                            rolePermissionsContent+='</span>';
                            rolePermissionsContent+='</span>';
                            rolePermissionsContent+='</div>';
                            rolePermissionsContent+='</div>';

                            rolePermissionsContent+='<div class="permissions-menu-actions clearfix pl100">';
                            rolePermissionsContent+='<div class="col-sm-12 pl0">';
                            rolePermissionsContent+='<span class="pull-left mr15 mt5 gray">Actions</span>';

                            for(var rpsa=0;rpsa<rolePermissions[rp].submenu[rps].actions.length;rpsa++)
                            {
                                var actionStatus='';
                                if(is_admin == 1)
                                {
                                    actionStatus="checked='checked' disabled";
                                }
                                else if(rolePermissions[rp].submenu[rps].actions[rpsa].isEnabled == 1)
                                {
                                    actionStatus="checked='checked'";
                                }

                                rolePermissionsContent+='<span class="inline-checkbox">';
                                rolePermissionsContent+='<input id="ms5_'+rolePermissions[rp].submenu[rps].actions[rpsa].actionId+'" class="filled-in role-manage" type="checkbox" value="'+rolePermissions[rp].submenu[rps].actions[rpsa].actionId+'" '+actionStatus+' />';
                                rolePermissionsContent+='<label for="ms5_'+rolePermissions[rp].submenu[rps].actions[rpsa].actionId+'">'+rolePermissions[rp].submenu[rps].actions[rpsa].actionName+'</label>';
                                rolePermissionsContent+='</span>';
                            }

                            rolePermissionsContent+='</div>';
                            rolePermissionsContent+='</div>';
                        }
                    }
                }
                rolePermissionsContent+='</div>';
            }
        }
    }
    $("#rolePermissionsSection").html(rolePermissionsContent);
}

function renderDataToRoles(rolesResponse)
{
    if(rolesResponse.status == true)
    {
        if(rolesResponse.data.length > 0)
        {
            var roleList='',selected='';
            for(var i=0;i<rolesResponse.data.length;i++)
            {
                selected='';
                if(rolesResponse.data[i].roleName == roleName)
                {
                    selected='selected'
                }
                roleList+='<option '+selected+' value="'+rolesResponse.data[i].roleId+'">'+rolesResponse.data[i].roleName+'</option>';
            }
            $("#ddlManageRoles").html(roleList);
            $('#ddlManageRoles').material_select();
        }
    }
    else
    {
        notify('Something went wrong. Please try again', 'error', 10);
    }
}
    function confirmSaveRolePermission(roleId)
    {
        if(chkFlag == 1)
        {
            var ModelTitle="Manage Role";
            var Description="Are you sure want to save ?";
            $("#popup_confirm #btnTrue").attr('onClick','saveRolePermissions("'+roleId+'")');
            customConfirmAlert(ModelTitle,Description);
        }else {
            //saveRolePermissions(roleId);
            alertify.dismissAll();
            notify('No actions found', 'error', 10);
        }
    }
</script>