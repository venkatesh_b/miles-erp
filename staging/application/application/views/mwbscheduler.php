<input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">MWB Scheduler</h3>
        <div class="content-header-btnwrap">
            <ul>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" access-element="List"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">

            <div class="col-sm-12 clearfix p0">

                <div class=" pull-left mt55"><h4 class="heading-uppercase mb0 mt5">Leads Stage</h4></div>
                <div class="icons-remember">
                    <p class=" pull-left pr10 m0 pt10">
                        <span class="calls-perday"><i class="fa fa-phone"></i></span>
                        Yesterday Calls
                    </p>
                    <p class="pull-left m0 pt10 ">
                        <span class="calls-inhand"><i class="fa fa-phone"></i></span>
                        Calls In Hand
                    </p>
                </div>
                <div class="pull-right callschedule-dtfilter">
                    <h4 class="heading-uppercase pl20 mb5">Schedule Calls</h4>
                    <div class="pt2 pull-right"><button type="button" class="btn blue-btn" onclick="setColdCallDates(this)" id="btnSetColdCallDate" ><i class="icon-right mr8"></i>Go</button></div>
                    <div class="pull-right">
                        <div class="input-field  mt0 mr15">
                            <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="txtToDate">
                            <label class="datepicker-label datepicker">Till Date</label>
                        </div>
                    </div>
                    <!--<div class="pull-right">
                        <div class="input-field  mt0">
                            <input type="date" class="datepicker relative enableDates" placeholder="dd/mm/yyyy" id="txtFromDate">
                            <label class="datepicker-label datepicker">From</label>
                        </div>
                    </div>-->




                </div>

            </div>
            <div class="col-sm-12 p0 call-schedule-scroll">
                <table class="table table-responsive table-custom callschedule-table" id="CallScheduleDetails">
                    <thead>

                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="col-sm-12 p0"> <button type="Submit" class="btn blue-btn" onclick="saveCallSchedules(this)" id="btnSaveCallSchedules" disabled ><i class="icon-right mr8"></i>Save</button></div>
        </div>

        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    //var coldCallFromDate=dateConvertion(new Date());
    var coldCallToDate=dateConvertion(new Date());
    function saveCallSchedules(This){
        notify('Processing..', 'warning', 10);
        var leadCallSchedules=[];
        if(leadColdCallCountResponse.data.count.stages) {
            $.each(leadColdCallCountResponse.data.count.stages, function (keyLead, valueLead) {
                var txt_name="txtLeadColdCalling_"+valueLead.stage_id;
                $('input[name="'+txt_name+'"]').each(function () {
                    if ($(this).val()) {
                        var currentVal = parseInt($(this).val());
                        var res = $(this).attr('id').split("_");
                        if(currentVal!='' && currentVal!=0)
                            leadCallSchedules.push(res[1] + '-' + currentVal);
                    }
                });
            });
        }

        if(leadColdCallCountResponse.data.count.Oldstages) {
            $.each(leadColdCallCountResponse.data.count.Oldstages, function (keyLead, valueLead) {
                var txt_name="txtLeadColdCalling_"+valueLead.stage_id;
                $('input[name="'+txt_name+'"]').each(function () {
                    if ($(this).val()) {
                        var currentVal = parseInt($(this).val());
                        var res = $(this).attr('id').split("_");
                        if(currentVal!='' && currentVal!=0)
                            leadCallSchedules.push(res[1] + '-' + currentVal);
                    }
                });
            });
        }
        var leadCallSchedules=leadCallSchedules.join('^');
        var ajaxurl=API_URL+'index.php/MWBCallSchedule/addCallSchedule';
        var userID=$('#hdnUserId').val();
        var action='schedule';
        var branchId=$('#userBranchList').val();
        var type="POST";
        //var fromDate=coldCallFromDate;
        var toDate=coldCallToDate;
        //var params={branchId:branchId,leadCallSchedules:leadCallSchedules,fromDate:fromDate,toDate:toDate};
        var params={branchId:branchId,leadCallSchedules:leadCallSchedules,toDate:toDate,type:'Self Employee'};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response=
            commonAjaxCall({This:This,method:type,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:saveCallSchedulesResponse});

    }
    function saveCallSchedulesResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            $('#btnSaveCallSchedules').attr('disabled',false);
            notify(response.message,'success',10);
            genereteColdCallingData(this);

        }
        else{
            notify(response.message,'error',10);
        }
    }
    docReady(function(){
        setColdCallDefaultDates();
        genereteColdCallingData(this);
    });

    function genereteColdCallingData(This){

        var ajaxurl=API_URL+'index.php/MWBCallSchedule/getLeadColdCallingCount';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        //var fromDate=coldCallFromDate;
        var toDate=coldCallToDate;

        var params={branchId:branchId,toDate:toDate,type:'Self Employee'};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        //var response=
        //  commonAjaxCall({This:This,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:buildLeadCallsScheduleCountsData});

        $.when(
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    requestUrl: API_URL+'index.php/MWBCallSchedule/getLeadColdCallingCount',
                    params: params,
                    action: action
                }
            ),
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    requestUrl: API_URL+'index.php/MWBCallSchedule/getCallSchedules',
                    params: params,
                    action: action
                }
            )
        ).then(function (response1,response3) {
            alertify.dismissAll();
            buildLeadCallsScheduleCountsData(response1[0]);
            buildCallsScheduleData(response3[0]);
            userWorkLoad();
            $('#btnSaveCallSchedules').attr('disabled',true);

        });


    }
    function getAvailableCounts(This){

        var ajaxurl=API_URL+'index.php/MWBCallSchedule/getCallCounts';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        //var fromDate=coldCallFromDate;
        var toDate=coldCallToDate;
        //var params={branchId:branchId,fromDate:fromDate,toDate:toDate};
        var params={branchId:branchId,toDate:toDate};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        var response=
            commonAjaxCall({This:This,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:buildCallsScheduleCountsData});

    }
    function buildCallsScheduleCountsData(response){
        //var coldCallingCount=0;
        if(response.status===true){
            //coldCallingCount=response.data.count;
            //$('#coldCallingWrapper').html(coldCallingCount);
            //$('#coldCallingBalanceWrapper').html(coldCallingCount);
        }
    }

    function getCallsScheduleData(This){
        var ajaxurl=API_URL+'index.php/MWBCallSchedule/getCallSchedules';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        //var fromDate=coldCallFromDate;
        var toDate=coldCallToDate;
        //var params={branchId:branchId,fromDate:fromDate,toDate:toDate};
        var params={branchId:branchId,toDate:toDate};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        var response=
            commonAjaxCall({This:This,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:buildCallsScheduleData});

    }

    var CallsScheduleDataResponse='';
    var glbleadCallTxtBxs='';
    function buildCallsScheduleData(response){
        var rowsHtml='';
        var leadCallTxtBxs=[];
        CallsScheduleDataResponse=response;
        if(CallsScheduleDataResponse.status===true){

            $.each(CallsScheduleDataResponse.data,function(key,value){
                var userImage='';
                if(value.image && CheckFileExists(value.image))
                {
                    userImage += '<img src="'+value.image+'">';

                }
                else
                {
                    userImage += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png">';
                }
                rowsHtml+="<tr class='animated fadeInUp'>";
                rowsHtml+="<td>";
                rowsHtml+="<div class=\"userimage-div clearfix\">";
                rowsHtml+="<div class=\"image-user pull-left\">";
                rowsHtml+=userImage;
                rowsHtml+="</div>";
                rowsHtml+="<div class=\"call-user\">";
                rowsHtml+="<span class=\"call-username ellipsis tooltipped\" data-position=\"top\" data-delay=\"50\" data-tooltip=\"Venkatesh\">"+value.name+"</span>";
                rowsHtml+="<span class=\"call-userrole\">"+value.role+"</span>";
                rowsHtml+="<span class=\"call-avg\"><i class=\"fa fa-phone\"></i>"+value.previousCallsCount+"<input type=\"hidden\" id=\"previousCallCountWrapper_"+value.user_id+"\" value=\""+value.previousCallsCount+"\" readonly disabled/></span>";
                if(value.inHandCallsCount>0)
                {
                    rowsHtml+="<span class=\"calls\"><i class=\"fa fa-phone\"></i><input class='form-control inp-call-schedule' type=\"text\" id=\"inHandCallCountWrapper_"+value.user_id+"\" value=\""+value.inHandCallsCount+"\"/></span>";
                    rowsHtml += "<a href=\"javascript:;\" access-element='release' title=\"Release Calls\" onclick=\"confirmColdCallRelease(this,'" + value.user_id + "',"+value.inHandCallsCount+")\"><i class=\"fa fa-external-link-square\"></i></a>";
                }
                else
                {
                    rowsHtml+="<span class=\"calls\"><i class=\"fa fa-phone\"></i>"+value.inHandCallsCount+"<input type=\"hidden\" id=\"inHandCallCountWrapper_"+value.user_id+"\" value=\""+value.inHandCallsCount+"\" readonly disabled/></span>";
                }
                rowsHtml+="</div>";
                rowsHtml+="</div>";
                rowsHtml+="</td>";
                //rowsHtml+="<td><input class=\"callschedule-input\" name=\"txtColdCalling\" id='txtColdCalling_"+value.user_id+"' type=\"text\" maxlength=\"3\"></td>";
                if(leadColdCallCountResponse.data.count.stages)
                {
                    $.each(leadColdCallCountResponse.data.count.stages, function (keyLead, valueLead)
                    {
                        //rowsHtml+="<th>"+value.stage+"<span class=\"display-block text-center\">"+leadColdCallCountResponse.data.count.stageCounts[value.stage_id]+"</th>";
                        rowsHtml += "<td><input class=\"callschedule-input\" type=\"text\" name=\"txtLeadColdCalling_" + valueLead.stage_id + "\" id=\"txtLeadColdCalling_" + valueLead.stage_id + "|" + value.user_id + "\"  ></td>";
                        leadCallTxtBxs.push("input[name=txtLeadColdCalling_" + valueLead.stage_id + "]");
                    });
                }

                if(leadColdCallCountResponse.data.count.Oldstages)
                {
                    $.each(leadColdCallCountResponse.data.count.Oldstages, function (keyLead, valueLead)
                    {
                        //rowsHtml+="<th>"+value.stage+"<span class=\"display-block text-center\">"+leadColdCallCountResponse.data.count.stageCounts[value.stage_id]+"</th>";
                        rowsHtml += "<td><input class=\"callschedule-input\" type=\"text\" name=\"txtLeadColdCalling_" + valueLead.stage_id + "\" id=\"txtLeadColdCalling_" + valueLead.stage_id + "|" + value.user_id + "\"  ></td>";
                        leadCallTxtBxs.push("input[name=txtLeadColdCalling_" + valueLead.stage_id + "]");
                    });
                }

                rowsHtml+="<td id=\"WorkLoadWrapper_"+value.user_id+"\">0</td>";
                rowsHtml+="</tr>";
            });
            var remainingCountsHtml='';
            remainingCountsHtml+="<tr class='animated fadeInUp'>";
            remainingCountsHtml+="<td class=\"remaining-tr\">";
            remainingCountsHtml+="<div class=\"call-user\"><span class=\"call-username\" >Call Balance</span></div></td>";
            //remainingCountsHtml+="<td id='coldCallingBalanceWrapper'>0</td>";
            if(leadColdCallCountResponse.data.count.stages && leadColdCallCountResponse.data.count.stages!='') {
                $.each(leadColdCallCountResponse.data.count.stages, function (keyLead, valueLead) {
                    remainingCountsHtml += "<td id=\"LeadcoldCallingBalanceWrapper_" + valueLead.stage_id + "\">0</td>";
                });
            }

            if(leadColdCallCountResponse.data.count.Oldstages && leadColdCallCountResponse.data.count.Oldstages!='') {
                $.each(leadColdCallCountResponse.data.count.Oldstages, function (keyLead, valueLead) {
                    remainingCountsHtml += "<td id=\"LeadcoldCallingBalanceWrapper_" + valueLead.stage_id + "\">0</td>";
                });
            }

            remainingCountsHtml+="<td><span id=\"TotalWorkLoadWrapper\" style=\"visibility:hidden;\">0</span></td>";
            remainingCountsHtml+="</tr>";

        }
        else{
            rowsHtml+="<tr><td colspan='21' class='text-center'>No users found</td></tr>";
        }
        $('#CallScheduleDetails tbody').html(rowsHtml+remainingCountsHtml);
        if(leadColdCallCountResponse.data.count.stages) {
            $.each(leadColdCallCountResponse.data.count.stages, function (keyLead, valueLead) {
                $('#LeadcoldCallingBalanceWrapper_'+valueLead.stage_id).html(leadColdCallCountResponse.data.count.stageCounts[valueLead.stage_id]);
            });
        }

        if(leadColdCallCountResponse.data.count.Oldstages) {
            $.each(leadColdCallCountResponse.data.count.Oldstages, function (keyLead, valueLead) {
                $('#LeadcoldCallingBalanceWrapper_'+valueLead.stage_id).html(leadColdCallCountResponse.data.count.OldstageCounts[valueLead.stage_id]);
            });
        }

        //leadCallTxtBxs.push('input[name=txtColdCalling]');
        leadCallTxtBxs=leadCallTxtBxs.join(',');
        glbleadCallTxtBxs=leadCallTxtBxs;
        $(leadCallTxtBxs).keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        $(leadCallTxtBxs).blur(function (e) {
            $(leadCallTxtBxs).removeClass('required');
            $('#btnSaveCallSchedules').attr('disabled','disabled');
            var total=0;
            var selectedName=$(this).prop('name');
            var headerCountId='';
            var footerBalanceCountId='';
            if(selectedName=='txtColdCalling'){
                headerCountId='#coldCallingWrapper';
                footerBalanceCountId='#coldCallingBalanceWrapper';
            }
            else{
                var sp=selectedName.split('_');
                headerCountId='#LeadcoldCallingWrapper_'+sp[1];
                footerBalanceCountId='#LeadcoldCallingBalanceWrapper_'+sp[1];
            }
            var coldCallingCount=parseInt($(headerCountId).html());
            var isValid=1;//0-invalid,1-valid
            $('input[name="'+selectedName+'"]').each(function(index) {
                var var1ref=$('input[name="'+selectedName+'"]:eq('+index+')');
                var var1=var1ref.val();
                var currentVal=parseInt(var1);
                if(currentVal>coldCallingCount){
                    currentVal=0;
                    var1ref.val(null);

                    //alert('Should be less than available count ('+coldCallingCount+')');
                    var ModelTitle="Call Schedule";
                    var Description='Cannot be more than available count (' + coldCallingCount + ')';
                    customAlert(ModelTitle,Description);
                    $(this).addClass('required');
                    $('#btnSaveCallSchedules').attr('disabled','disabled');
                    return false;
                    isValid=0;
                }
                else
                {
                    if(var1)
                    {
                        total = total + currentVal;
                    }
                }
            });
            if(isValid==0)
            {
                $(footerBalanceCountId).html('-');
            }
            else if(isValid==1 && total>coldCallingCount)
            {
                var ModelTitle="Call Schedule";
                var Description='Cannot be more than available count (' + coldCallingCount + ')';
                customAlert(ModelTitle,Description);
                $(this).addClass('required');
                $(footerBalanceCountId).html('-');
                isValid=0;
            }
            else if(isValid==1 && total<=coldCallingCount)
            {
                $(footerBalanceCountId).html(coldCallingCount-total);
                userWorkLoad();
            }
            if(isValid==1 && (coldCallingCount-total)<coldCallingCount)
            {
                $('#btnSaveCallSchedules').attr('disabled',false);
            }
        });
    }
    function validateCounts(leadCallTxtBxs)
    {
        var leadCallTxtBxs=glbleadCallTxtBxs;
        $(leadCallTxtBxs).removeClass('required');
        var txtBxs = leadCallTxtBxs.split(',');
        $.each(txtBxs, function (key, value)
        {
            var total = 0;
            var selectedName = $(value).prop('name');
            var headerCountId = '';
            var footerBalanceCountId = '';
            if (selectedName == 'txtColdCalling')
            {
                headerCountId = '#coldCallingWrapper';
                footerBalanceCountId = '#coldCallingBalanceWrapper';
            }
            else
            {
                var sp = selectedName.split('_');
                headerCountId = '#LeadcoldCallingWrapper_' + sp[1];
                footerBalanceCountId = '#LeadcoldCallingBalanceWrapper_' + sp[1];
            }
            var coldCallingCount = parseInt($(headerCountId).html());
            var isValid = 1;//0-invalid,1-valid
            $('input[name="' + selectedName + '"]').each(function (index)
            {
                var var1 = $('input[name="' + selectedName + '"]:eq(' + index + ')').val();
                var var1_id = $('input[name="' + selectedName + '"]:eq(' + index + ')').prop('id');
                var var1_ref = $('input[name="' + selectedName + '"]:eq(' + index + ')');
                //console.log(var1_id);
                var currentVal = parseInt(var1);
                if (currentVal > coldCallingCount)
                {
                    var ModelTitle="Call Schedule";
                    var Description='Cannot be more than available count (' + coldCallingCount + ')';
                    customAlert(ModelTitle,Description);
                    var1_ref.addClass('required');
                    $('#btnSaveCallSchedules').attr('disabled', 'disabled');
                    return false;
                    isValid = 0;
                }
                else
                {
                    if (var1)
                    {
                        total = total + currentVal;
                    }
                }
            });
            if (isValid == 0)
            {
                $(footerBalanceCountId).html('-');
            }
            else if (isValid == 1 && total > coldCallingCount)
            {
                var ModelTitle="Call Schedule";
                var Description='Total cannot be more than available count (' + coldCallingCount + ')';
                customAlert(ModelTitle,Description);
                isValid = 0;
            }
            else if (isValid == 1 && total <= coldCallingCount)
            {
                $(footerBalanceCountId).html(coldCallingCount - total);
            }
            if (isValid == 1 && (coldCallingCount - total) < coldCallingCount)
            {
                $('#btnSaveCallSchedules').attr('disabled', false);
            }
        });
    }
    $('#userBranchList').change(function()
    {
        changeDefaultBranch(this);
        genereteColdCallingData(this);
    });
    function confirmColdCallRelease(This,assignedUserId,releaseCnt)
    {
        customConfirmAlert('Release Calls', 'Are you sure want to release the calls from the user ?');
        $("#popup_confirm #btnTrue").attr("onclick","coldCallRelease(this,"+assignedUserId+","+releaseCnt+")");
    }
    function coldCallRelease(This,assignedUserId,releaseCnt)
    {
            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/MWBCallSchedule/releaseScheduledCalls';
            var releaseCnt = releaseCnt;
            var userID = $('#hdnUserId').val();
            var releaseCount = $('#inHandCallCountWrapper_'+assignedUserId).val();
            if(releaseCount > releaseCnt)
            {
                alertify.dismissAll();
                notify('Cannot release more than assigned calls','error',10);
            }
            else
            {
                var action = 'release';
                var branchId = $('#userBranchList').val();
                var type = "POST";
                //var fromDate=coldCallFromDate;
                var toDate=coldCallToDate;
                //var params = {branchId: branchId, assignedUserId: assignedUserId, releaseCnt: releaseCount,fromDate:fromDate,toDate:toDate};
                var params = {branchId: branchId, assignedUserId: assignedUserId, releaseCnt: releaseCount,toDate:toDate,type:'Self Employee'};
                var headerParams = {
                    action: action,
                    context: $context,
                    serviceurl: $pageUrl,
                    pageurl: $pageUrl,
                    Authorizationtoken: $accessToken,
                    user: userID
                };
                var response =
                    commonAjaxCall({
                        This: This,
                        method: 'POST',
                        requestUrl: ajaxurl,
                        params: params,
                        headerParams: headerParams,
                        action: action,
                        onSuccess: saveReleaseCallSchedulesResponse
                    });
            }
    }
    function saveReleaseCallSchedulesResponse(response){
        $('#popup_confirm').modal('hide');
        alertify.dismissAll();
        if(response.status===true)
        {
            notify(response.message,'success',10);
            genereteColdCallingData(this);
        }
        else
        {
            notify(response.message,'error',10);
        }
    }
    function getAvailableLeadCallCounts(This)
    {
        var ajaxurl=API_URL+'index.php/MWBCallSchedule/getLeadColdCallingCount';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        //var fromDate=coldCallFromDate;
        var toDate=coldCallToDate;
        //var params={branchId:branchId,fromDate:fromDate,toDate:toDate};
        var params={branchId:branchId,toDate:toDate};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        var response=
            commonAjaxCall({This:This,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:buildLeadCallsScheduleCountsData});

    }
    var leadColdCallCountResponse='';
    function buildLeadCallsScheduleCountsData(response)
    {
        leadColdCallCountResponse=response;
        var currentBatchCount=0;
        var oldBatchCount=0;
        var rowHtml='';
        if(leadColdCallCountResponse.status===true)
        {
            currentBatchCount = leadColdCallCountResponse.data.count.stages.length;
            oldBatchCount=leadColdCallCountResponse.data.count.Oldstages.length;
            rowHtml+="<tr>";
            rowHtml+="<tr class=\"text-center\">";
            rowHtml+="<th rowspan=\"2\" class=\"text-center\" style=\"vertical-align:middle;\">Calls vs Users</th>";
            //rowHtml+="<th class=\"text-center p5\" style=\"vertical-align:middle;\">Cold Calling</th>";
            if(currentBatchCount>0)
            {
                rowHtml += "<th class=\"p5\" colspan=\"" + currentBatchCount + "\" class=\"current-batch\">Current Batches</th>";
            }
            rowHtml+="<th class=\"p5\" colspan=\""+oldBatchCount+"\">Old Batches</th>";
            //rowHtml+="<th class=\"p5\"></th>";
            rowHtml+="<th class=\"p5\"></th>";
            rowHtml+="</tr>";
            //rowHtml+="<th><span class=\"display-block text-center\" id=\"coldCallingWrapper\">-</th>";
            if(leadColdCallCountResponse.data.count.stages!='')
            {
                $.each(leadColdCallCountResponse.data.count.stages, function (key, value)
                {
                    rowHtml += "<th>" + value.stage + "<span id=\"LeadcoldCallingWrapper_" + value.stage_id + "\" class=\"display-block text-center\">" + response.data.count.stageCounts[value.stage_id] + "</th>";
                });
            }

            if(leadColdCallCountResponse.data.count.Oldstages!='')
            {
                $.each(leadColdCallCountResponse.data.count.Oldstages, function (key, value)
                {
                    rowHtml += "<th>" + value.stage + "<span id=\"LeadcoldCallingWrapper_" + value.stage_id + "\" class=\"display-block text-center\">" + response.data.count.OldstageCounts[value.stage_id] + "</th>";
                });
            }

            rowHtml+="<th style=\"vertical-align:middle;\">User </br>Work Load</th>";
            rowHtml+="</tr>";
            rowHtml+="<tr><td class=\"border-none p0 lh10\">&nbsp;</td></tr>";
        }

        $('#CallScheduleDetails thead').html(rowHtml);
    }

    function userWorkLoad(){

        if(CallsScheduleDataResponse.status===true)
        {
            var totalworkload=0;
            $.each(CallsScheduleDataResponse.data,function(key,value){
                var workload=0;
                workload+=parseInt($('#inHandCallCountWrapper_'+value.user_id).val());
                if(leadColdCallCountResponse.data.count.stages && (leadColdCallCountResponse.data.count.stages!=''))
                {
                    $.each(leadColdCallCountResponse.data.count.stages, function (keyStages, valueStages)
                    {
                        var exp1="txtLeadColdCalling_"+valueStages.stage_id+"\\|"+value.user_id;
                        if($("#"+exp1) && $("#"+exp1).val()!='')
                        {
                            workload+= parseInt($("#"+exp1).val());
                        }
                    });
                }

                if(leadColdCallCountResponse.data.count.Oldstages && (leadColdCallCountResponse.data.count.Oldstages!=''))
                {
                    $.each(leadColdCallCountResponse.data.count.Oldstages, function (keyStages, valueStages)
                    {
                        var exp1="txtLeadColdCalling_"+valueStages.stage_id+"\\|"+value.user_id;
                        if($("#"+exp1) && $("#"+exp1).val()!='')
                        {
                            workload+= parseInt($("#"+exp1).val());
                        }
                    });
                }
                $("#"+'WorkLoadWrapper_'+value.user_id).html(workload);
                totalworkload+=workload;
            });
        }
        $('#TotalWorkLoadWrapper').html(totalworkload);
        //WorkLoadWrapper_{user_id}=callsInHand_userid+txtColdCalling_{user_id}+txtLeadColdCalling_{stageId}|{user_id}+
    }
    function setColdCallDates()
    {
        $("#txtToDate").removeClass("required");
        $('#txtToDate').parent().find('label').removeClass("active");
        $('#txtToDate').parent().find('span.required-msg').remove();
        var flag=0;
        var txtToDate=$('#txtToDate').val();
        if(txtToDate=='')
        {
            $("#txtToDate").addClass("required");
            $("#txtToDate").after('<span class="required-msg">required field</span>');
            flag = 1;
        }

        if(flag==0)
        {
            notify('Processing..', 'warning', 10);
            var toDate=dateConvertion(txtToDate);
            coldCallToDate = toDate;
            genereteColdCallingData(this);
        }
    }
    function dateConvertion(dt)
    {
        var orgDate=new Date(dt);
        var dd=orgDate.getDate();
        var mm=orgDate.getMonth()+1;
        if(dd<10)
        {
            dd='0'+dd;
        }
        if(mm<10)
        {
            mm='0'+mm;
        }

        var yy=orgDate.getFullYear();
        return (yy+'-'+mm+'-'+dd);
    }
    function setColdCallDefaultDates()
    {
        var $input = $('#txtToDate').pickadate({
            min: new Date(),
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 20, // Creates a dropdown of 15 years to control year
            onSet: function( arg ){
                if ( 'select' in arg ){ //prevent closing on selecting month/year
                    this.close();
                }
            }
        });
        var picker = $input.pickadate('picker');
        picker.set('select', coldCallToDate, {format: 'yyyy-mm-dd'});
    }
</script>
