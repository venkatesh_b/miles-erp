<?php
/**
 * Created by PhpStorm.
 * User: Abhilash
 * Date: 19/01/17
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly/>
    <div class="content-header-wrap">
        <h3 class="content-header">Net Enquiry</h3>

        <!--<div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a href="javascript:;" onclick="ManageLead(this)" class="tooltipped"
                                            data-position="top" data-tooltip="Add Lead"><i class="icon-plus-circle"></i></a>
                </li>
                <li access-element="import"><a class="tooltipped" data-position="top" data-tooltip="Import Leads"
                                               href="javascript:;" onclick="resetImport(this)"><i
                            class="icon-import1"></i></a></li>
            </ul>
        </div>-->
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="wrap-left col-sm-12 p0">
                <div class="col-sm-12 p0 mb10">
                    <div class="col-sm-3 p0 ">
                        <div class="contact-wrap branch-green-bg">
                            <span class="green-bg"><img
                                    src="<?php echo BASE_URL; ?>assets/images/new-leads1.png"></span>
                        </div>
                        <div class="contact-wrap-text mr5 branch-green-bg-light">
                            <div class="display-inline-block contact-green-color">Yesterdays Leads</div>
                            <div class="contact-blue" id="yesterdayLeads"></div>
                        </div>
                    </div>
                    <div class="col-sm-3 p0 ">
                        <div class="contact-wrap branch-red-bg">
                            <span class="red-bg"><img
                                    src="<?php echo BASE_URL; ?>assets/images/total-leads1.png"></span>
                        </div>
                        <div class="contact-wrap-text mr5 branch-red-bg-light">
                            <div class="display-inline-block contact-red-color">Todays Leads</div>
                            <div class="contact-blue" id="todaysLeads"></div>
                        </div>
                    </div>
                    <div class="col-sm-3 p0 ">
                        <div class="contact-wrap branch-blue-bg">
                            <span class="blue-bg"><img src="<?php echo BASE_URL; ?>assets/images/completed1.png"></span>
                        </div>
                        <div class="contact-wrap-text mr5 branch-blue-bg-light">
                            <div class="display-inline-block contact-blue-color">Total Leads</div>
                            <div class="contact-blue" id="totalLeads"></div>
                        </div>
                    </div>
                </div>
                <div access-element="list" class="col-sm-12 p0">
                    <table class="table table-responsive table-striped table-custom min-height-td" id="leads-list">
                        <thead>
                        <tr class="">
                            <th style="">Name</th>
                            <th style="width:60px;">Course</th>
                            <th style="width:50px;">Level</th>
                            <th class="no-sort border-r-none">Info</th>
                            <th class="no-sort"></th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>

        </div>
        <!--Create Lead Modal-->
        <div class="modal fade" id="createLead" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <form id="ManageCreateLeadForm">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="">Create Lead</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="leadName" type="text" class="validate">
                                    <label for="leadName">Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="LeadCourses">
                                    </select>
                                    <label class="select-label">Course <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="leadPhone" type="text" class="validate"
                                           onkeypress="return isPhoneNumber(event)" maxlength="15">
                                    <label for="leadPhone">Mobile No. <!--<em>*</em>--></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="leadEmail" type="email" class="">
                                    <label for="leadEmail">Email Address <!--<em>*</em>--></label>
                                </div>
                            </div>
                            <div class="col-sm-12 mb20" id="contactType-corporate-tags">
                                <div class="input-field col-sm-11 p0 custom-multiselect-wrap">
                                    <!-- class="custom-select-nomargin" -->
                                    <select multiple data-placeholder="Choose Tags" id="txtContactTag"
                                            class="chosen-select">
                                        <option value="" disabled>--Select--</option>
                                    </select>
                                    <label class="select-label">Tags<!-- <em>*</em>--></label>
                                </div>
                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn pull-right"
                                       onclick="resetTagsWrapper(this)" href="javascript:;"><span
                                            class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap leadTag-wrap"
                                     id="ContactTagWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtContactTagWrapper" type="text" class="validate" value="">
                                            <label for="txtContactTagWrapper">Tag Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-right"><span
                                                class="fa fa-check-circle f18 diplay-inline-block green"
                                                href="javascript:;"
                                                onclick="addLeadTagWrapper(this, '#txtContactTag')"></span></a>
                                        <a class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span
                                                class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" id="contactType-source">
                                <div class="input-field col-sm-11 p0 custom-select-wrap">
                                    <select class="chosen-select" id="txtContactSource">
                                        <option value="" disabled selected>--Select--</option>
                                    </select>
                                    <label class="select-label">Source <em>*</em></label>
                                </div>
                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn pull-right"
                                       onclick="resetLeadContactSourceWrapper(this)" href="javascript:;"><span
                                            class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap"
                                     id="ContactSourceWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtContactSourceWrapper" type="text" class="validate" value="">
                                            <label for="txtContactSourceWrapper">Source <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-right"><span
                                                class="fa fa-check-circle f18 diplay-inline-block green"
                                                href="javascript:;"
                                                onclick="addLeadContactSourceWrapper(this, '#ManageCreateLeadForm #txtContactSource')"></span></a>
                                        <a class="mt20 pl5 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span
                                                class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12" id="LeadCreateContactTypeWrapper">

                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtLeadContactPostQualification">

                                    </select>
                                    <label class="select-label">Qualification Level <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtLeadContactQualification">

                                    </select>
                                    <label class="select-label">Qualification <em>*</em></label>
                                </div>
                            </div>

                            <div class="col-sm-12 mb15" id="contactType-corporate-wrap">
                                <div class="input-field col-sm-11 p0 custom-select-wrap">
                                    <select id="txtLeadContactCompany" class="chosen-select">

                                    </select>
                                    <label class="select-label">Company <em>*</em></label>
                                </div>

                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn"
                                       onclick="LeadCreateresetCompanyWrapper(this)"><span
                                            class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap "
                                     id="ComapnyWrapper">
                                    <div class="col-sm-8 p0 ">
                                        <div class="input-field mt15">
                                            <input id="txtComapnyNameWrapper" type="text" class="validate" value="">
                                            <label for="txtComapnyNameWrapper">Company Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0 ">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span
                                                class="fa fa-check-circle f18 diplay-inline-block green"
                                                onclick="LeadCreateaddCompanyWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span
                                                class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mb20" id="contactType-Institution-wrap">
                                <div class="input-field col-sm-11 p0 custom-select-wrap">
                                    <!-- class="custom-select-nomargin" -->
                                    <select id="txtLeadContactInstitution" class="chosen-select">

                                    </select>
                                    <label class="select-label">Institution <em>*</em></label>
                                </div>
                                <div class="col-sm-1 p0" access-element="add reference">
                                    <a class="mt10 ml5 display-inline-block cursor-pointer showAddInstituteWrap-btn"
                                       onclick="LeadCreateresetInstitutionWrapper(this)"><span
                                            class="icon-plus-circle f24 mt20 diplay-inline-block"></span></a>
                                </div>
                                <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap"
                                     id="InstitutionWrapper">
                                    <div class="col-sm-8 p0">
                                        <div class="input-field mt15">
                                            <input id="txtInstitutionNameWrapper" type="text" class="validate" value="">
                                            <label for="txtInstitutionNameWrapper">Institution Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 p0">
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span
                                                class="fa fa-check-circle f18 diplay-inline-block green"
                                                onclick="LeadCreateaddInstitutionWrapper(this)"></span></a>
                                        <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span
                                                class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea class="materialize-textarea" id="LeadDescription"></textarea>
                                    <label for="LeadDescription" class="">Known information about this lead
                                        <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="hint-msg">Note: Mobile No. or Email Address is mandatory
                                    <em>*</em></label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue-btn" onclick="saveLead(this)"><i
                                    class="icon-right mr8"></i>Add
                            </button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                    class="icon-times mr8"></i>Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="callStatus" role="dialog" aria-labelledby="myModalLabel" data-modal="right"
             modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button onclick="closeLeadCallStatus(this)" type="button" class="close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Call Status</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix" id="CallStatusData">
                        <div class="col-sm-12 grey-bg">
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Name</span>
                                    <p id="BxrefLeadName"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Email</span>
                                    <p id="BxrefLeadEmail"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Phone</span>
                                    <p id="BxrefLeadPhone"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">City</span>
                                    <p id="BxrefLeadCity"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Country</span>
                                    <p id="BxrefLeadCountry"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Last Follow up</span>
                                    <p id="BxrefLeadCreatedDate"></p>
                                </div>
                            </div>
                        </div>
                        <form id="ManageCallStatus">
                            <input type="hidden" id="hdnBxrefLeadId" value="">
                            <input type="hidden" id="hdnLeadStage" value="">
                            <input type="hidden" id="hdnLead_id" value="">
                            <input type="hidden" id="hdnLeadStageText" value="">
                            <div class="col-sm-12 pt5">
                                <div class="input-field">
                                    <select id="callsTypes" onchange="CallStatusChange()">

                                    </select>
                                    <label for="callsTypes" class="select-label">Call Status</label>
                                </div>
                            </div>
                            <div class="col-sm-12 pt5" id="FutureDatesPicker" style="display:none;">
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input type="date" class="datepicker relative enableFutureDates"
                                               placeholder="dd/mm/yyyy" id="dpNextFollowUp">
                                        <label for="dpNextFollowUp" class="datepicker-label">Next Follow Up Date
                                            <em>*</em></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12" id="CallStatusInformation" style="display:none;">
                                <div class="multiple-radio">
                                    <span class="inline-radio">
                                        <input class="with-gap" name="callStatusInfo_radio" type="radio"
                                               id="callStatusInfoIntrested" value="callStatusInfoIntrested"/>
                                        <label for="callStatusInfoIntrested">Interested <span
                                                id="intrestedLevelLabel"></span></label>
                                    </span>
                                     <span class="inline-radio">
                                        <input class="with-gap" checked name="callStatusInfo_radio" type="radio"
                                               id="callStatusInfoNotintrested" value="callStatusInfoNotintrested"/>
                                        <label for="callStatusInfoNotintrested">Not interested <span>{M2}</span></label>
                                    </span>
                                     <span class="inline-radio">
                                        <input class="with-gap" name="callStatusInfo_radio" type="radio"
                                               id="callStatusInfoDND" value="callStatusInfoDND"/>
                                        <label for="callStatusInfoDND">DND <span>(L0)</span></label>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-12 pt5" id="CommonCallStatusInformation">
                                <div class="input-field">
                                    <select id="CallAnswerTypes" onchange="getCallAnswerTypes(this)">

                                    </select>
                                    <label for="CallAnswerTypes" class="select-label">Answer</label>
                                </div>
                            </div>
                            <div class="col-sm-12 pt5" id="CommonCallStatusDatePicker" style="display:none;">
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input type="date" class="datepicker relative enableFutureDates"
                                               placeholder="dd/mm/yyyy" id="txtCommonCallStatusDatePicker">
                                        <label for="txtCommonCallStatusDatePicker"
                                               id="txtLabelCommonCallStatusDatePicker" class="datepicker-label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 pt5" id="VisitedDatesPicker" style="display:none;">
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input type="date" class="datepicker relative enableFutureDates"
                                               placeholder="dd/mm/yyyy" id="dpvisitedDate">
                                        <label for="dpvisitedDate" class="datepicker-label">Next Visitied Date
                                            <em>*</em></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12" id="callStatusComments" style="display:none;">
                                <div class="input-field">
                                    <textarea id="txaCallStatusDescription" class="materialize-textarea"></textarea>
                                    <label class="" for="txaCallStatusDescription">Comments <em>*</em></label>
                                </div>
                            </div>
                            <div class="coldcall-info" style="display:none;">
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <select>
                                            <option value="" disabled selected>--Select--</option>
                                            <option value="1">CPA</option>
                                            <option value="2">CMA</option>
                                            <option value="3">PGDA</option>
                                        </select>
                                        <label class="select-label">Course</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <select>
                                            <option value="" disabled selected>--Select--</option>
                                            <option value="1">Hyderabad</option>
                                            <option value="2">Chennai</option>
                                            <option value="3">Bangalore</option>
                                        </select>
                                        <label class="select-label">Branch</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <input id="qlf" type="text" class="validate">
                                        <label for="qlf">Qualification</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <input id="cmp" type="text" class="validate">
                                        <label for="cmp">Company</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <input id="inst" type="text" class="validate">
                                        <label for="inst">Institution</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="changeCallStatus(this)" class="btn blue-btn"><i
                                class="icon-right mr8"></i>Save
                        </button>
                        <button type="button" onclick="closeLeadCallStatus(this)" class="btn blue-light-btn"><i
                                class="icon-times mr8"></i>Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!--Import Model-->
        <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="600">
            <form id="importForm" method="post">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title">Import</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="file-field input-field">
                                    <div class="btn file-upload-btn">
                                        <span>Upload Document</span>
                                        <input type="file" id="txtfilebrowser">
                                    </div>
                                    <div class="file-path-wrapper custom">
                                        <input type="text" placeholder="Upload single file" class="file-path validate">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mt20">
                                <a class="btn blue-btn btn-sm"
                                   href="<?php echo BASE_URL . 'templates/LeadsTemplate.csv'; ?>"
                                   id="btnDownloadSample"><i class="icon-file mr8"></i>Download Sample</a>
                            </div>

                            <div class="col-sm-12 mb20 mt20">
                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info ml0 grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Total Leads</div>
                                            <div class="camp-num red f14" id="TotalUploadedContacts">-</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Uploaded</div>
                                            <div class="camp-num thick-blue f14" id="UploadedContacts">-</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Failed</div>
                                            <div class="camp-num violet f14" id="FailedContacts">-</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3 p0 ">
                                    <div class="batch-info mr0 grey-bg">
                                        <div class="ptb7">
                                            <div class="camp-text f12">Duplicate</div>
                                            <div class="camp-num light-black f14" id="DuplicateContacts">-</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-responsive table-striped table-custom"
                                       id="uploadedContactsList">
                                    <colgroup>
                                        <col width="15%">
                                        <col width="35%">
                                        <col width="50%">
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th>Row No.</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                    </tr>
                                    <tr>
                                        <td class="border-none p0 lh10">&nbsp;</td>
                                    </tr>
                                    <!-- <tr><td class="border-none p0 lh5">&nbsp;</td></tr>-->
                                    </thead>
                                    <tbody>
                                    <tr class="">
                                        <td colspan="3" align='center'>No Data Found</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <a class="btn blue-btn" href="javascript:;" onclick="importContacts(this)"><i
                                    class="icon-right mr8"></i>Upload
                            </a>
                            <a class="btn blue-light-btn" data-dismiss="modal"><i
                                    class="icon-times mr8"></i>Cancel
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--fee assign model-->
        <div class="modal fade" id="FeeAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageFeeAssignForm" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Fee Assign</h4>
                            <input type="hidden" id="hdnFeeAssignId" value="0"/>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12" id="FeeassignLeadNumber">
                                <div class="col-sm-11 pl0">
                                    <div class="input-field">
                                        <input id="txtLeadNumber" type="text" class="validate">
                                        <label for="txtLeadNumber">Mobile Number <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-1 p0 mt2 sky-blue">
                                    <a href="javascript:;" class="btn blue-btn" onclick="showLeadInfo(this)">GO</a>

                                </div>
                            </div>

                            <input type="hidden" id="hdnCourseId" value="">
                            <input type="hidden" id="hdnBranchId" value="">
                            <input type="hidden" id="hdnBranchxceflead" value="">
                            <input type="hidden" id="payableAmount" value="">
                            <input type="hidden" id="feeStructureId" value="">
                            <input type="hidden" id="typeOfFee" value="">
                            <input type="hidden" id="company" value="">
                            <input type="hidden" id="Institutional" value="">
                            <div id="LeadData">
                                <div class="col-sm-12 mtb10">

                                    <div id="leadInfoData">

                                    </div>
                                </div>
                                <div class="col-sm-12" id="typeOfClass" style="display:none">
                                    <div class="multiple-checkbox" style="display: none">
                                    <span class="inline-checkbox">
                                    <input type="checkbox" class="filled-in" id="clasRoom"/>
                                    <label for="clasRoom">Class Room Training</label>
                                    </span>
                                    <span class="inline-checkbox">
                                    <input type="checkbox" class="filled-in" id="online"/>
                                    <label for="online">Online Training</label>


                                    </span>
                                    </div>
                                    <div class="input-field">
                                        <select id="Trainingtype">
                                            <option value="" selected>--select--</option>

                                        </select>
                                        <label class="select-label">Training Type <em>*</em></label>
                                    </div>
                                </div>

                                <div class="col-sm-12" id="feetypes" style="display:none">
                                    <div class="input-field">
                                        <select id="ddlFeetypes" name="ddlFeetypes">
                                            <option value="" selected>--select--</option>

                                        </select>
                                        <label class="select-label">Fee Type</label>

                                    </div>
                                </div>
                                <div class="col-sm-12" id="feeStructureType" style="display:none">
                                    <div class="input-field">
                                        <select id="ddlFeeStructuetypes" name="ddlFeeStructuetypes">
                                            <option value="" selected>--select--</option>

                                        </select>
                                        <label class="select-label">Fee Type</label>

                                    </div>
                                </div>


                                <div class="col-sm-12 mt0 feeStructureCreate-companyname" id="Corporatetypes"
                                     style="display:none">
                                    <div class="input-field">
                                        <select id="corporate" name="corporate"
                                                class="validate custom-select-nomargin formSubmit">
                                            <option value="default" disabled selected>---Select---</option>
                                        </select>
                                        <label class="select-label">Corporate Company <em>*</em></label>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt0 feeStructureCreate-groupname" id="groupTypes"
                                     style="display:none">
                                    <div class="input-field">
                                        <select id="groupvalue" name="groupvalue"
                                                class="validate formSubmit custom-select-nomargin">
                                            <option value="default" disabled selected>---Select---</option>
                                        </select>
                                        <label class="select-label">Institute Name <em>*</em></label>
                                    </div>

                                </div>
                                <div id="tabs" class="mt10"></div>
                                <div id="DataMessage" class="p5" style="display:none;"></div>
                            </div>
                        </div>


                        <div class="modal-footer" id="buttons">
                            <button type="button" disabled id="saveButton" class="btn blue-btn"
                                    onclick="SaveCandidateDetails(this)"><i class="icon-right mr8"></i>Save
                            </button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="cancelButton"><i
                                    class="icon-times mr8"></i>Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--fee assign model-->
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    $context = 'Net Enquiry';
    $pageUrl = 'netenquiry';
    $serviceurl = 'netenquiry';
    docReady(function () {
        glbBranchId = $('#userBranchList').val();
        buildLeadsDataTable();
        getQuickLeadCourses();
        ShowLeadsCount();
    });

    function buildLeadsDataTable() {
        $(document).ready(function () {
            var ajaxurl = API_URL + 'index.php/LeadsInfo/getNetLeadsList';
            var userID = $('#hdnUserId').val();
            var branchId = $('#userBranchList').val();
            var params = {branchId: branchId};
            var headerParams = {
                action: 'list',
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: userID
            };
            $("#leads-list").dataTable().fnDestroy();
            $tableobj = $('#leads-list').DataTable({
                "fnDrawCallback": function () {
                    buildpopover();
                    verifyAccess();
                    var $api = this.api();
                    var pages = $api.page.info().pages;
                    if (pages > 1) {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                    } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                    }
                },
                dom: "Bfrtip",
                bInfo: false,
                "serverSide": true,
                "bProcessing": true,
                "oLanguage": {
                    "sSearch": "<span class='icon-search f16'></span>",
                    "sEmptyTable": "No leads found",
                    "sZeroRecords": "No leads found",
                    "sProcessing": "<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                },
                ajax: {
                    url: ajaxurl,
                    type: 'GET',
                    headers: headerParams,
                    data: params,
                    error: function (response) {
                        DTResponseerror(response);
                    }
                },
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false
                }, {className: "p0", "targets": [4]}],
                "order": [[0, "desc"]],
                columns: [
                    {
                        data: null, render: function (data, type, row) {
                        var leadName = "";
                        var leadIcon = 'male-icon.jpg';
                        if (data.gender == 0) {
                            var leadIcon = 'female-icon.jpg';
                        }
                        leadName += '<div class="img-wrapper img-wrapper-leadinfo light-blue3-bg" style="width:70px;height:63px;margin-left:-8px; position:absolute;top:50%;margin-top:-32px"><a class=\'tooltipped\' data-position="top" data-tooltip="Click here for lead info" style="display:block;" href="' + BASE_URL + 'app/leadinfo/' + data.branch_xref_lead_id_encode +'/'+$context+'~'+$pageUrl+'~'+$pageUrl+'"><img src="<?php echo BASE_URL; ?>assets/images/' + leadIcon + '" style="width:33px;height:33px"><span class="img-text" style="word-break:break-word">' + data.lead_number + '</span></a></div>';
                        leadName += '<div style="padding-left:85px"><div class="f14 font-bold">';
                        leadName += data.lead_name;
                        if (data.comments.length > 0) {
                            $.each(data.comments, function (key, value) {
                                if (value.branch_visit_comments == null || value.branch_visit_comments.trim() == '')
                                    value.branch_visit_comments = '----';
                            });


                        }
                        if (data.comments.length > 0) {
                            //leadName+='</div>';
                        }
                        leadName += '</div>';
                        leadName += '<span class="pl0">Last follow up : ' + data.created_on + '</span></div>';
                        return leadName;
                    }
                    },
                    {
                        data: null, render: function (data, type, row) {
                        return data.course_name;
                    }
                    },
                    {
                        data: null, render: function (data, type, row) {
                        return data.level;
                    }
                    },
                    {
                        data: null, render: function (data, type, row) {
                        var company_name = (data.company == null || data.company == '') ? "" : '<p class=" ellipsis gmail-wrap"><lable class="clabel icon-label"><a class="dark-sky-blue tooltipped" data-position="top" data-tooltip="Company"  href="javascript:"><i class="icon-company ml5 mt2 f14"></i></a></lable>' + data.company + '</p>';
                        var Qualificatin_name = (data.qualification == null || data.qualification == '') ? "" : '<p class=" ellipsis gmail-wrap"><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Qualification" class="dark-sky-blue tooltipped"><i class="icon-qualification ml5 mt2"></i></a></lable>' + data.qualification + '</p>';
                        var infoEmail = (data.email == null || data.email == '') ? "" : '<p class=" ellipsis gmail-wrap"><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Email" class="dark-sky-blue tooltipped"><i class="icon-mail ml5 mt2"></i></a></lable>' + data.email + '</p>';
                        var infoPhone = (data.phone == null || data.phone == '') ? "" : '<p class=" ellipsis gmail-wrap"><lable class="clabel icon-label"><a href="javascript:;" data-position="top" data-tooltip="Phone" class="dark-sky-blue tooltipped"><i class="icon-phone ml5 mt2"></i></a></lable>' + data.phone + '</p>';
                        var info = "";
                        info += infoEmail;
                        info += infoPhone;
                        info += '' + Qualificatin_name + '';
                        return info;
                    }
                    },
                    {
                        data: null, render: function (data, type, row) {
                        var view = '';
                        view += '<div class="leads-icons pull-right">';
                        if (data.comments.length > 0) {
                            view += '<a href="javascript:" class="popper" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Comments"><img src="<?php echo BASE_URL; ?>assets/images/comment.png"></a>';
                            view += '<div class="popper-content hide">';
                            $.each(data.comments, function (key, value) {
                                if (value.branch_visit_comments == null || value.branch_visit_comments.trim() == '')
                                    value.branch_visit_comments = '----';
                                view += '<div class="border-bottom plr15 pb20 mb5">';
                                view += '<h4 class="col-md-12 anchor-blue p0 mt5 mb5">First Level Comments</h4>';
                                view += '<p class="p0">' + value.branch_visit_comments + '</p>';
                                view += '<p class="p0 mt5 ash">' + value.branch_visited_date + '<span class="ml15">By<span class="anchor-blue pl10">' + value.counseollor_name + '</span></span> </p>';
                                view += '</div>';
                                view += '<div class="border-bottom plr15 pb20 mb5">';
                                view += '<h4 class="col-md-12 anchor-blue p0 mt5 mb5">Second Level Comments</h4>';
                                view += '<p class="p0">' + value.second_level_comments + '</p>';
                                view += '<p class="p0 mt5 ash">' + value.counsellor_visited_date + '<span class="ml15">By<span class="anchor-blue pl10">' + value.second_counseollor_name + '</span></span> </p>';
                                view += '</div>';
                            });
                            view += '</div>';
                        }
                        view += '<span data-tooltip="Change Call Status" data-delay="50" data-position="top" class="tooltipped"><a onclick="manageLeadCallStatus(this,' + data.branch_xref_lead_id + ')" href="javascript:;"><img src="<?php echo BASE_URL; ?>assets/images/call.png"></a></span>';
                        /*view+='<span data-tooltip="Fee Assign" data-delay="50" data-position="top" class="tooltipped"><a href="javascript:" onclick="ManageLeadsFeeAssign(this,'+data.lead_number+','+data.course_id+')"><img src="<?php //echo BASE_URL; ?>assets/images/rupee.png"></a></span>';*/
                        view += '</div>';
                        return view;
                    }
                    }
                ]
            });
            DTSearchOnKeyPressEnter();
        });
    }

    function ShowLeadsCount(This) {
        var userID = $('#hdnUserId').val();
        var action = 'list';
        var ajaxurl = API_URL + 'index.php/LeadsInfo/showNetLeadsCount';
        var params = {userId: userID, branchId: glbBranchId};
        var headerParams = {
            action: action,
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: userID
        };
        commonAjaxCall({
            This: This,
            params: params,
            headerParams: headerParams,
            requestUrl: ajaxurl,
            action: 'list',
            onSuccess: ShowLeadsCountResponse
        });
    }
    function ShowLeadsCountResponse(response) {
        if (response.length == 0 || response == -1 || response['status'] == false) {

        } else {

            var yesterday = (response.yesterday == null || response.yesterday == '') ? "0" : response.yesterday;
            var today = (response.today == null || response.today == '') ? "0" : response.today;
            var total = (response.total == null || response.total == '') ? "0" : response.total;
            $("#yesterdayLeads").text(yesterday);
            $("#todaysLeads").text(today);
            $("#totalLeads").text(total);
        }
    }

    function getQuickLeadCourses() {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Courses/getBranchCoursesList';
        var params = {branchId: glbBranchId};
        var action = 'add';
        var headerParams = {
            action: action,
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: userID
        };
        commonAjaxCall({
            This: this,
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: action,
            onSuccess: getQuickLeadCoursesResponse
        });
    }
    function getQuickLeadCoursesResponse(response) {
        if (response.status == true) {
            $('#txtQuickLeadCourses').empty();
            $('#txtQuickLeadCourses').append($('<option selected></option>').val('').html('--Select--'));
            if (response.data.length > 0) {
                $.each(response.data, function (key, value) {

                    $('#txtQuickLeadCourses').append($('<option></option>').val(value.courseId).html(value.name));
                });
            }
        }
        else {
            $('#txtQuickLeadCourses').empty();
            $('#txtQuickLeadCourses').append($('<option selected></option>').val('').html('--Select--'));
        }
        $('#txtQuickLeadCourses').material_select();
    }
    var glbBranchId = '';
    $("#userBranchList").change(function () {
        changeDefaultBranch();
        glbBranchId = $('#userBranchList').val();
        getQuickLeadCourses();
        buildLeadsDataTable();
        ShowLeadsCount(this);
    });
</script>