<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->

    <div class="content-header-wrap">
        <h3 class="content-header">Post Qualification </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li><a class="tooltipped" data-position="top" data-tooltip="Add Post Qualification" class="viewsidePanel" onclick="getPostQualificationDetails(this,'')" href="javascript:;"><i class="icon-plus-circle" ></i></a></li>
                <!--<li><a></a></li>-->
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" > <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0" access-element="list">
                <table class="table table-responsive table-striped table-custom table-info" id="postqualification-list">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th class="border-r-none">Description</th>
                        <th class="no-sort"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="postqualificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closePostQualificationModal(this)"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="tagModalLabel">Create Post Qualification</h4>
                    </div>
                    <form action="/" method="post" id="postqualificationForm" novalidate="novalidate">
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="postqualificationName" type="text" name="postqualificationName" class="validate" class="formSubmit" required>
                                    <label for="postqualificationName">Post Qualification Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="postqualificationDescription" name="postqualificationDescription" class="materialize-textarea"></textarea>
                                    <label for="postqualificationDescription">Post Qualification Description </label>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="reference_type_val_id" name="reference_type_val_id" value=""/>
                    </form>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="submit" class="btn blue-btn" onclick="AddPostQualification(this)"><i class="icon-right mr8"></i>Submit</button>
                        <button type="button" class="btn blue-light-btn" onclick="closePostQualificationModal(this)"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildPostQualificationModalReferenceDataTable();
    });

    function buildPostQualificationModalReferenceDataTable()
    {
        var userId = $('#userId').val();
        var ajaxurl=API_URL+'index.php/Referencevalues/getReferenceValuesList';
        var params = {'reference_type':'Post Qualification'};
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        $("#postqualification-list").dataTable().fnDestroy();
        $tableobj = $('#postqualification-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                data:params,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    'width': '30%',
                    data: null, render: function ( data, type, row )
                {
                    return data.postqualification_name;
                }
                },
                {
                    'width': '69%',
                    data: null, render: function ( data, type, row )
                {
                    if(data.description==null || data.description.trim()==''){
                        data.description='----';
                    }
                    return data.description;
                }
                },
                {
                    'width': '1%',
                    data: null, render: function ( data, type, row )
                {
                    var referenceTypeData='<div class="dropdown feehead-list pull-right custom-dropdown-style pl20">';
                    referenceTypeData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    referenceTypeData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                    referenceTypeData+='</a>';
                    referenceTypeData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    referenceTypeData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    referenceTypeData+='<li><a access-element="edit" href="javascript:;" data-target="#myEdit" data-toggle="modal" onclick="getPostQualificationDetails(this,'+data.reference_type_value_id+')">Edit</a></li>';
                    referenceTypeData+='</ul>';
                    referenceTypeData+='</div>';
                    return referenceTypeData;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function getPostQualificationDetails(This, id) {

        $('#postqualificationForm')[0].reset();
        $('#postqualificationForm input').removeClass("required");
        $('#postqualificationForm textarea').removeClass("required");
        $('#postqualificationForm label').removeClass("active");

        if (id == '' || id == 0) {
            $("#tagModalLabel").html("Create Post Qualification");
            $("#reference_type_val_id").val(id);
            $('#postqualificationModal').modal('toggle');
        } else {
            notify('Processing..', 'warning', 50);
            $("#tagModalLabel").html("Edit Post Qualification");
            var userId = $('#userId').val();
            var ajaxurl = API_URL + "index.php/Company/getCompanyById/company_id/" + id;
            var params = {};
            var action = 'list';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: 'list', onSuccess: function (response) {
                if (response == -1 || response['status'] == false)
                {
                } else
                {
                    alertify.dismissAll();

                    $('#postqualificationName').val(response.data[0].value);
                    $('#postqualificationName').next('label').addClass('active');
                    $('#reference_type_val_id').val(response.data[0].id);
                    if(response.data[0].description!=null && response.data[0].description.trim()!='') {
                        $('#postqualificationDescription').val(response.data[0].description);
                        $('#postqualificationDescription').next('label').addClass('active');
                    }
                    $('#postqualificationModal').modal('toggle');
                }
            }
            });
        }
    }

    function closePostQualificationModal(This) {
        $('#postqualificationForm')[0].reset();

        $('#reference_type_val_id').val('');

        $('#postqualificationForm span.required-msg').remove();
        $('#postqualificationForm input').removeClass("required");
        $('#postqualificationForm textarea').removeClass("required");

        $('#postqualificationModal').modal('toggle');

        $("#tagModalLabel").html("Create Post Qualification");
    }
    function AddPostQualification(This) {
        $('#postqualificationForm span.required-msg').remove();
        $('#postqualificationForm input').removeClass("required");
        $('#postqualificationForm textarea').removeClass("required");
        var id = $('#reference_type_val_id').val();
        var postqualificationNameRegx=$rgx_allow_alpha_numeric_space;
        var url = '';
        var action = 'add';
        var name = $('#postqualificationName').val().trim();
        var description = $('#postqualificationDescription').val().trim();
        var data = '';
        if (id != '')
        {
            $("#tagModalLabel").html("Edit Post Qualification");
            data = {
                postqualificationName: name,
                postqualificationDescription: description,
                postqualificationId: id
            };
            action = 'edit';
            url = API_URL + "index.php/Company/editPostQualification";
        } else {
            action = 'add';
            $("#tagModalLabel").html("Create Post Qualification");
            data = {
                postqualificationName: name,
                postqualificationDescription: description
            };
            url = API_URL + "index.php/Company/addPostQualification";
        }

        var flag = 0;
        if (name == '') {
            $("#postqualificationName").addClass("required");
            $("#postqualificationName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        /*else if (!(postqualificationNameRegx.test(name)))
        {
            $("#postqualificationName").addClass("required");
            $("#postqualificationName").after('<span class="required-msg">'+$rgx_allow_alpha_numeric_space_message+'</span>');
            flag = 1;
        }*/

        /*if (description == '') {
            $("#postqualificationDescription").addClass("required");
            $("#postqualificationDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        else if (!(/^([a-zA-Z ])/.test(description)))
        {
            $("#postqualificationDescription").addClass("required");
            $("#postqualificationDescription").after('<span class="required-msg">Invalid Description</span>');
            flag = 1;
        }*/

        if (flag == 0) {
            notify('Processing..', 'warning', 50);
            var ajaxurl = url;
            var params = data;

            var userId = $('#userId').val();

            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var response = commonAjaxCall({This: This, headerParams: headerParams, asyncType: true, method: 'POST', params: params, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false)
                {
                    var id = '';
                    for (var a in response.data) {
                        if (a == 'reference_type_val') {
                            id = '#postqualificationName';
                        } else {
                            id = '#' + a;
                        }
                        $(id).addClass("required");
                        $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                    }
                    $('#postqualificationModal').removeAttr('disabled');
                } else
                {
                    buildPostQualificationModalReferenceDataTable();
                    $('#postqualificationForm')[0].reset();
                    $('#postqualificationModal').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    notify('Data Saved Successfully', 'success', 10);
                }
            }});

        } else {
            return false;
        }
    }
</script>