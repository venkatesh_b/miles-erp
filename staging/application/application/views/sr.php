<div class="contentpanel" id="courseview"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="surveyId" value="0" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">SR </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Create Question" class="viewsidePanel"  onclick="ManageSurvey(this, 0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>   
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix"> 
            <div class="col-sm-6 p0">
                <div class="col-sm-6 reports-filter-wrapper pl0">
                    <div class="multiple-checkbox reports-filters-check">
                        <label class="heading-uppercase">Course</label>
                    </div>
                    <div class="boxed-tags radio-tags" id="courseFilter"></div>
                </div>
                <div class="col-sm-6 mt15">
                    <a class="btn blue-btn btn-sm" href="javascript:;" onclick="filterCourseBasedSurvey(this)">Proceed</a>
                </div>
            </div>
            <div class="col-sm-12 clearfix p0">
                <table class="table table-responsive table-striped table-custom" id="survey-list" style="display: none; width: 100%;">
                    <thead>
                        <tr>
                            <th>Question</th>
                            <th>Question Type</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal1-->
        <div class="modal fade" id="SurveyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="SurveyForm"> 
                        <div class="modal-header">
                            <button type="button" onclick="closeSurveyModal()" class="close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Questions</h4>
                        </div>
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="course">
                                        <option value="" selected>Select</option>
                                    </select>
                                    <label class="select-label">Course </label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input type="text" class="" id="txtTitle">
                                    <label for="txtTitle"> Question Title <em>*</em></label>
                                </div>
                            </div>
                            <!--                            <div class="col-sm-12">
                                                            <label>Answer</label>
                                                        </div>-->
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="txtType">
                                        <option value="" selected>Select</option>
                                    </select>
                                    <label class="select-label">Question Type </label>
                                </div>
                            </div>

                            <div class="col-sm-12" id="addedOptions" style="display: none;">

                            </div>
                            <div class="col-sm-12" id="selectedOptions" style="display: none;">
                                <a class=" ml5 display-inline-block cursor-pointer tooltipped" data-position="top" data-tooltip="Add options" onclick="addSRField(0, 0)" >
                                    <span class="icon-plus-circle f24 diplay-inline-block sky-blue"></span>
                                </a> 
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue-btn" onclick="saveSurvey(this)" id="btnSave"><i class="icon-right mr8"></i>Add</button>
                            <button type="button" class="btn blue-light-btn" onclick="closeSurveyModal()"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- InstanceEndEditable --></div>
</div>
<script>
    docReady(function () {
        buidDefaultSurveyData();
    });
    function buidDefaultSurveyData() {
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/Survey/getDefaultSurveySettings';
        var params = {};
        commonAjaxCall({This: this, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                var dashboard = '';
                if (response == -1 || response['status'] == false || response.data.length == 0) {

                } else {
                    var RawData = response.data;
                    var html = '';

                    if (RawData.course.length > 0) {
                        for (var a in RawData.course) {
                            if (a == 0) {
                                html += '<a class="active" data-id="' + RawData.course[a].courseId + '" href="javascript:;">' + RawData.course[a].name + '</a>'
                            } else {
                                html += '<a data-id="' + RawData.course[a].courseId + '" href="javascript:;">' + RawData.course[a].name + '</a>'
                            }
                        }
                        $('#courseFilter').html(html);
                    }
                    html = '';

                    //Courses dropdown
                    for (var b = 0; b < RawData.course.length; b++) {
                        var o = $('<option/>', {value: RawData.course[b]['courseId']})
                                .text(RawData.course[b]['name']);
                        o.appendTo('#course');
                    }
                    $('#course').material_select();

                    //Question Type
                    for (var b = 0; b < RawData.questionTypes.length; b++) {
                        var o = $('<option/>', {value: RawData.questionTypes[b]['value']})
                                .text(RawData.questionTypes[b]['value'].capitalizeFirstLetter());
                        o.appendTo('#txtType');
                    }
                    $('#txtType').material_select();

                    buildpopover();
                }
            }});
    }
    String.prototype.capitalizeFirstLetter = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
    function getValues() {
        var result = $("input[name='options[]']").map(function () {
            if ($(this).val() == '' || $(this).val() == null) {
                $(this).addClass('required');
                return false;
            } else {
                return {'value': $(this).val()}
            }
        }).get();
        return result;
    }
    function saveSurvey(This) {
        $('#SurveyForm span.required-msg').remove();
        $('#SurveyForm input, #SurveyForm .select-dropdown').removeClass("required");
        $('#addedOptions input').removeClass("required");
        var error = false;
        var result = '';
        var title = $('#txtTitle').val();
        var course = $('#course').val();
        var selectedOption = $('#txtType').val();

        if (title == '' || title == null) {
            $("#txtTitle").addClass("required");
            $("#txtTitle").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        } else if (!(/^[a-zA-Z][a-zA-Z0-9 ?_\-.]*$/.test(title))) {
            $("#txtTitle").addClass("required");
            $("#txtTitle").after('<span class="required-msg">Invalid</span>');
            error = true;
        }

        if (selectedOption == '' || selectedOption == null) {
            $("#txtType").parent().find('.select-dropdown').addClass("required");
            $("#txtType").parent().after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        }

        if (course == '' || course == null) {
            $("#course").parent().find('.select-dropdown').addClass("required");
            $("#course").parent().after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            error = true;
        }

        if (selectedOption == 'input') {
            result = true;
        } else if (selectedOption == 'paragraph') {
            result = true;
        } else if (selectedOption == 'radio') {
            result = getValues();
        } else if (selectedOption == 'checkbox') {
            result = getValues();
        }
        for (var index in result) {
            if (!result[index]) {
                var msg = 'Option can\'t be blank';
                alertify.dismissAll();
                notify(msg, 'error', 10);
                return false;
            }
        }

        if (!error) {
            var params = {
                txtTitle: title,
                optionType: selectedOption,
                options: result,
                course: course,
            };
            var ajaxurl, action, method = '';
            var Id = $('#surveyId').val();
            if (Id == 0) {
                ajaxurl = API_URL + "index.php/Survey/saveSurvey";
                action = 'add';
            } else {
                ajaxurl = API_URL + "index.php/Survey/editSurvey/survey_id/" + Id;
                action = 'edit';
            }
            method = 'POST';
            var userId = $('#hdnUserId').val();
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, method: method, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
                    if (response == -1 || response['status'] == false) {
                        var id = '';
                        alertify.dismissAll();
                        if (Object.size(response.data) > 0) {
                            notify('Validation Error', 'error', 10);
                            for (var a in response.data) {
                                id = '#' + a;
                                if (a == '') {
                                    $(id).parent().find('.select-dropdown').addClass("required");
                                    $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                                } else {
                                    $(id).addClass("required");
                                    $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                                }
                            }
                        } else {
                            notify(response.message, 'error', 10);
                        }
                    } else {
                        alertify.dismissAll();
                        notify('Saved successfully', 'success', 10);
                        filterCourseBasedSurvey();
                        closeSurveyModal();
                    }
                }});
        } else {
            var msg = 'Validation Error';
            alertify.dismissAll();
            notify(msg, 'error', 10);
        }
    }
    function ManageSurvey(This, Id) {
        $('#selectedTextField').hide();
        $("#addedOptions").html('');
        $('#SurveyForm')[0].reset();
        $('#SurveyForm label').removeClass("active");
        $('#SurveyForm span.required-msg').remove();
        $('#surveyId').val(Id);
        if (Id == 0) {
            //add
            $('#myModalLabel').html('Create Question');
            $('#btnSave').html('<i class="icon-right mr8"></i>Add');
        } else {
            //edit
            $('#myModalLabel').html('Edit Question');
            $('#btnSave').html('<i class="icon-right mr8"></i>Update');
            var ajaxurl = API_URL + "index.php/Survey/getSurveyById";
            var action = 'add';
            var method = 'GET';
            var userId = $('#hdnUserId').val();
            var params = {'survey_id': Id};
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, method: method, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
                if (response == -1 || response['status'] == false) {
                    alertify.dismissAll();
                    notify('Something went wrong', 'error', 10);
                } else {
                    var data = response.data;
                    $('#txtTitle').val(data[0].title);
                    $('#txtType').val(data[0].option);
                    $('#txtType').material_select();
                    $('#course').val(data[0].course_id);
                    $('#course').material_select();
                    for (var index in data) {
                        addSRField(data[index].option, data[index].value);
                        var selectedOption = data[index].option;
                    }
                    if (selectedOption == 'input') {
                        $('#selectedOptions').hide();
                    } else if (selectedOption == 'paragraph') {
                        $('#selectedOptions').hide();
                    } else if (selectedOption == 'radio') {
                        $('#selectedOptions').show();
                    } else if (selectedOption == 'checkbox') {
                        $('#selectedOptions').show();
                    }
                    Materialize.updateTextFields();
                }
            }});
        }

        $('#SurveyModal').modal('show');
    }
    function deleteSurvey(This, surveyId, currentStatus) {
        var status = currentStatus == 1 ? 'inactivate' : 'activate';
        var conf = "Are you sure want to " + status + " this question ?";
        customConfirmAlert('Survey Status', conf);
        $("#popup_confirm #btnTrue").attr("onclick", "deleteSurveyConfirm(this,'" + surveyId + "')");
    }
    function deleteSurveyConfirm(This, surveyId) {
        var userId = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Survey/deleteSurvey/survey_id/' + surveyId;
        var type = "POST";
        var action = 'delete';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: 'delete', onSuccess: deleteSurveyResponse});
    }
    function deleteSurveyResponse(response) {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false) {
            notify(response.message, 'error', 10);
        } else {
            notify(response.message, 'success', 10);
            filterCourseBasedSurvey();
        }
    }
    function closeSurveyModal() {
        $('#surveyId').val(0);
        $('#selectedOptions').hide();
        $("#addedOptions").html('');
        $('#SurveyForm span.required-msg').remove();
        $('#SurveyForm input, #SurveyForm .select-dropdown').removeClass("required");
        $('#txtType').val(0);
        $('#txtType').material_select();
        $('#course').val(0);
        $('#course').material_select();
        $('#SurveyModal').modal('hide');
    }
    $('#txtType').on('change', function () {
        $('#selectedTextField').hide();
        $("#addedOptions").html('');
        var selectedOption = $('#txtType').val();
        var htmlData = '';
        if (selectedOption != '') {
            $('#selectedTextField').show();
        }
        if (selectedOption == 'input') {
            htmlData += '<div class="mb10">';
            htmlData += '<input class="form-control p5" type="text" aria-label="Text input with radio button" disabled placeholder="Answer">';
            htmlData += '</div>';
            $('#selectedOptions').hide();
        } else if (selectedOption == 'paragraph') {
            htmlData += '<div class="input-field">';
            htmlData += '<textarea id="textarea1" class="materialize-textarea grey-bg" disabled></textarea>';
            htmlData += '<label class="" for="textarea1">Long answer</label>';
            htmlData += '</div>';
            $('#selectedOptions').hide();
        } else if (selectedOption == 'radio') {
            htmlData += '<div class="input-group input-btn-group mb10">';
            htmlData += '<span class="inline-radio radio-btn pt2" disabled>';
            htmlData += '<input type="radio" name="group1" class="with-gap" id="radio" disabled>';
            htmlData += '<label class="mrl10 mt2" for=""></label>';
            htmlData += '</span>';
            htmlData += '<input placeholder="Answer options" type="text" name="options[]" aria-label="Text input with radio button" class="form-control">';
            htmlData += '<span class="filter-selectall-wrap filter-delete"><a href="javascript:;" onclick="removeSurveyFields(this)" class="reportFilter-unselectall ml5"><i class="icon-times red"></i></a></span>';
            htmlData += '</div>';
            $('#selectedOptions').show();
        } else if (selectedOption == 'checkbox') {
            htmlData += '<div class="input-group input-btn-group mb10">';
            htmlData += '<span class="inline-radio radio-btn pt2" disabled>';
            htmlData += '<input type="checkbox" name="group1" class="filled-in with-gap" id="checkbox" disabled>';
            htmlData += '<label class="filled-in-box mrl10 mt2 mb0" for=""></label>';
            htmlData += '</span>';
            htmlData += '<input  placeholder="Answer options" type="text" aria-label="Text input with radio button" name="options[]" class="form-control">';
            htmlData += '<span class="filter-selectall-wrap filter-delete"><a href="javascript:;" onclick="removeSurveyFields(this)" class="reportFilter-unselectall ml5"><i class="icon-times red"></i></a></span>';
            htmlData += '</div>';
            $('#selectedOptions').show();
        }
        $("#addedOptions").append(htmlData);
        $('#addedOptions').show();
//        buildpopover();
    });
    function removeSurveyFields(This) {
        $(This).closest('div').remove();
    }
    function addSRField(val1, val2) {
        var htmlData = '';
        if (val1 == 0) {
            var selectedOption = $('#txtType').val();
        } else {
            var selectedOption = val1;
        }
        var result = '';
        if (selectedOption == 'input') {
            result = true;
        } else if (selectedOption == 'paragraph') {
            result = true;
        } else if (selectedOption == 'radio') {
            result = getValues();
        } else if (selectedOption == 'checkbox') {
            result = getValues();
        }
        for (var index in result) {
            if (!result[index]) {
                var msg = 'Option can\'t be left blank';
                ;
                alertify.dismissAll();
                notify(msg, 'error', 10);
                return false;
            }
        }
        if (selectedOption == 'input') {
            htmlData += '<div class="mb10">';
            htmlData += '<input class="form-control p5" type="text" aria-label="Text input with radio button" disabled placeholder="Answer">';
            htmlData += '</div>';
        } else if (selectedOption == 'paragraph') {
            htmlData += '<div class="input-field">';
            htmlData += '<textarea id="textarea1" class="materialize-textarea grey-bg" disabled></textarea>';
            htmlData += '<label class="" for="textarea1">Longer Answer</label>';
            htmlData += '</div>';
        } else if (selectedOption == 'radio') {
            htmlData += '<div class="input-group input-btn-group mb10">';
            htmlData += '<span class="inline-radio radio-btn pt2" disabled>';
            htmlData += '<input type="radio" name="group1" class="with-gap" id="radio" disabled>';
            htmlData += '<label class="mrl10 mt2" for="radio"></label>';
            htmlData += '</span>';
            if (val1 == 0) {
                htmlData += '<input type="text" value="" name="options[]" aria-label="Text input with radio button" class="form-control" placeholder="Answer options">';
            } else {
                htmlData += '<input type="text" value="' + val2 + '" name="options[]" aria-label="Text input with radio button" class="form-control" placeholder="Answer options">';
            }
            htmlData += '<span class="filter-selectall-wrap filter-delete"><a href="javascript:;" onclick="removeSurveyFields(this)" class="reportFilter-unselectall ml5"><i class="icon-times red"></i></a></span>';
            htmlData += '</div>';
        } else if (selectedOption == 'checkbox') {
            htmlData += '<div class="input-group input-btn-group mb10">';
            htmlData += '<span class="inline-radio radio-btn pt2" disabled>';
            htmlData += '<input type="checkbox" name="group1" class="filled-in with-gap" id="checkbox" disabled>';
            htmlData += '<label class="filled-in-box mrl10 mt2 mb0" for=""></label>';
            htmlData += '</span>';
            if (val1 == 0) {
                htmlData += '<input placeholder="Answer options" type="text" value="" aria-label="Text input with radio button" name="options[]" class="form-control">';
            } else {
                htmlData += '<input  placeholder="Answer options" type="text" value="' + val2 + '" aria-label="Text input with radio button" name="options[]" class="form-control">';
            }
            htmlData += '<span class="filter-selectall-wrap filter-delete"><a href="javascript:;" onclick="removeSurveyFields(this)" class="reportFilter-unselectall ml5"><i class="icon-times red"></i></a></span>';
            htmlData += '</div>';
        }
        $("#addedOptions").append(htmlData);
        $('#addedOptions').show();
    }
    function filterCourseBasedSurvey() {
        var courseId = '';
        $("#courseFilter > .active").each(function () {
            error = false;
            courseId = $(this).attr('data-id');
        });
        var userID = $("#hdnUserId").val();
        var ajaxurl = API_URL + 'index.php/Survey/getAllSurveyDataTables/course_id/' + courseId;
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        $("#survey-list").dataTable().fnDestroy();
        $tableobj = $('#survey-list').DataTable({
            "fnDrawCallback": function () {
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
                    {
                        "sSearch": "<span class='icon-search f16'></span>",
                        "sEmptyTable": "No records found",
                        "sZeroRecords": "No records found",
                        "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
                    },
            ajax: {
                url: ajaxurl,
                type: 'GET',
                headers: headerParams,
                error: function (response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false
                }],
            columns: [
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.title;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return '<span class="text-capitalize">' + data.option_type + '<span>';
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        var rawHtml = '';
                        var status = data.question_active == 1 ? 'Active' : 'Inactive';
                        var statusText = data.question_active == 1 ? 'Inactive' : 'Active';
                        rawHtml += '';
                        rawHtml += status;
                        rawHtml += '<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        rawHtml += '<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                        rawHtml += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        rawHtml += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        rawHtml += '<li><a access-element="edit" href="javascript:;" onclick="ManageSurvey(this,\'' + data.question_id + '\')">Edit</a></li>';
                        rawHtml += '<li><a access-element="delete" href="javascript:;" onclick="deleteSurvey(this,\'' + data.question_id + '\', \'' + data.question_active + '\')">' + statusText + '</a></li>';
                        rawHtml += '</ul></div>';
                        return rawHtml;
                    }
                },
            ]
        });
        DTSearchOnKeyPressEnter();
        $("#survey-list").show();
    }
</script>
