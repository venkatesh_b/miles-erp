<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 27/1/17
 * Time: 10:22 AM
 */
?>
<input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <div class="content-header-wrap">
        <h3 class="content-header">SR Scheduler</h3>
        <div class="content-header-btnwrap">
            <ul></ul>
        </div>
    </div>
    <div class="content-body" id="contentBody" access-element="List"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 clearfix p0">
                <div class=" pull-left mt55"><h4 class="heading-uppercase mb0 mt5 font-bold">SR Scheduling</h4></div>
                <div class="icons-remember">
                    <p class=" pull-left pr10 m0 pt10">
                        <span class="calls-perday"><i class="fa fa-phone"></i></span>
                        Yesterday Calls
                    </p>
                    <p class="pull-left m0 pt10 ">
                        <span class="calls-inhand"><i class="fa fa-phone"></i></span>
                        Calls In Hand
                    </p>
                </div>
            </div>
            <div class="col-sm-12 p0 call-schedule-scroll">
                <table class="table table-responsive table-custom callschedule-table" id="CallScheduleDetails">
                    <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="col-sm-12 clearfix p0"> <button type="Submit" class="btn blue-btn" onclick="saveSRCallSchedules(this)" id="btnSaveCallSchedules" disabled ><i class="icon-right mr8"></i>Save</button></div>
        </div>
    </div>
</div>
<script>
    docReady(function(){
        genereteSRSchedularData(this);
    });
    var CallsScheduleDataResponse='';
    var glbleadCallTxtBxs='';
    function genereteSRSchedularData(This)
    {
        $('#btnSaveCallSchedules').attr('disabled',true);
        //var ajaxurl=API_URL+'index.php/CallSchedule/getStudentSchedularCount';
        var userID=$('#hdnUserId').val();
        var action='list';
        var branchId=$('#userBranchList').val();
        var params={branchId:branchId};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};

        //var response=
        //  commonAjaxCall({This:This,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:buildLeadCallsScheduleCountsData});

        $.when(

                commonAjaxCall
                (
                    {
                        This: This,
                        headerParams: headerParams,
                        requestUrl: API_URL+'index.php/SRCallSchedule/getStudentColdCallingCount',
                        params: params,
                        action: action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: This,
                        headerParams: headerParams,
                        requestUrl: API_URL+'index.php/SRCallSchedule/getStudentCallSchedules',
                        params: params,
                        action: action
                    }
                )
            ).then(function (response2,response3) {

                buildCallsScheduleData(response3[0]);
                buildCallsScheduleCountsData(response2[0]);
                userWorkLoad();
                buildpopover();

            });
    }

    function buildCallsScheduleData(response)
    {
        var rowsHtml='';
        var leadCallTxtBxs=[];
        CallsScheduleDataResponse=response;
        if(CallsScheduleDataResponse.status===true)
        {
            $.each(CallsScheduleDataResponse.data,function(key,value)
            {
                var userImage='';
                if(value.image && CheckFileExists(value.image))
                {
                    userImage += '<img src="'+value.image+'">';

                }
                else
                {
                    userImage += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png">';
                }
                rowsHtml+="<tr>";
                rowsHtml+="<td>";
                rowsHtml+="<div class=\"userimage-div-2 clearfix display-inline-block\">";
                rowsHtml+="<div class=\"image-user-wrap pull-left\">";
                rowsHtml+=userImage;
                rowsHtml+="</div>";
                rowsHtml+="<div class=\"call-user-wrap display-inline-block\">";
                rowsHtml+="<span class=\"call-username ellipsis tooltipped\" data-position=\"top\" data-delay=\"50\" data-tooltip='"+value.name+"'>"+value.name+"</span>";
                rowsHtml+="<span class=\"call-userrole\">"+value.role+"</span>";
                rowsHtml+="</div>";
                rowsHtml+="</div>";
                rowsHtml+="<div class=\"call-user-2 display-inline-block\">";
                rowsHtml+="<span class=\"call-avg-2\"><i class=\"fa fa-phone\"></i>"+value.previousCallsCount+"<input type=\"hidden\" id=\"previousCallCountWrapper_"+value.user_id+"\" value=\""+value.previousCallsCount+"\" readonly disabled/></span>";
                //rowsHtml+="<span class=\"call-avg-2\"><i class=\"fa fa-phone\"></i>"+value.todaysCallsCnt+"<input type=\"hidden\" id=\"previousCallCountWrapper_"+value.user_id+"\" value=\""+value.todaysCallsCnt+"\" readonly disabled/></span>";
                if(value.inHandCallsCount>0)
                {
                    rowsHtml+="<span class=\"calls-2\"><i class=\"fa fa-phone\"></i><input class='form-control inp-call-schedule' type=\"text\" id=\"inHandCallCountWrapper_"+value.user_id+"\" value=\""+value.inHandCallsCount+"\"/></span>";
                    rowsHtml += "<a href=\"javascript:;\" access-element='release' data-tooltip=\"Release Calls\" data-position=\"top\" title=\"Release Calls\" class=\"ml15 tooltipped\" onclick=\"confirmColdCallRelease(this,'" + value.user_id + "',"+value.inHandCallsCount+")\"><i class=\"fa fa-external-link-square pt10\"></i></a>";
                }
                else
                {
                    rowsHtml+="<span class=\"calls-2\"><i class=\"fa fa-phone\"></i>"+value.inHandCallsCount+"<input type=\"hidden\" id=\"inHandCallCountWrapper_"+value.user_id+"\" value=\""+value.inHandCallsCount+"\" readonly disabled/></span>";
                }
                rowsHtml+="</div>";
                rowsHtml+="</td>";
                rowsHtml+="<td  style=\"text-align:left\"><input class=\"callschedule-input-2\" name=\"txtColdCalling\" id='txtColdCalling_"+value.user_id+"' type=\"text\" maxlength=\"3\"></td>";
                rowsHtml+="<td style=\"text-align:left\" id=\"WorkLoadWrapper_"+value.user_id+"\">0</td>";
                rowsHtml+="</tr>";
            });
            var remainingCountsHtml='';
            remainingCountsHtml+="<tr class='animated fadeInUp'>";
            remainingCountsHtml+="<td class=\"remaining-tr\">";
            remainingCountsHtml+="<div class=\"call-user\"><span class=\"call-username\" >Call Balance</span></div></td>";
            remainingCountsHtml+="<td style=\"text-align:left\" id='coldCallingBalanceWrapper'>0</td>";
            remainingCountsHtml+="<td style=\"text-align:left\"><span id=\"TotalWorkLoadWrapper\" style=\"visibility:hidden;\">0</span></td>";
            remainingCountsHtml+="</tr>";
        }
        else
        {
            rowsHtml+="<tr><td colspan='22' class='text-center'>No users found</td></tr>";
        }
        $('#CallScheduleDetails tbody').html(rowsHtml+remainingCountsHtml);
        leadCallTxtBxs.push('input[name=txtColdCalling]');
        leadCallTxtBxs=leadCallTxtBxs.join(',');
        glbleadCallTxtBxs=leadCallTxtBxs;
        $(leadCallTxtBxs).keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        $(leadCallTxtBxs).blur(function (e) {
            $(leadCallTxtBxs).removeClass('required');
            $('#btnSaveCallSchedules').attr('disabled','disabled');
            var total=0;
            var selectedName=$(this).prop('name');
            var headerCountId='';
            var footerBalanceCountId='';
            if(selectedName=='txtColdCalling'){
                headerCountId='#coldCallingWrapper';
                footerBalanceCountId='#coldCallingBalanceWrapper';
            }
            else{
                var sp=selectedName.split('_');
                headerCountId='#LeadcoldCallingWrapper_'+sp[1];
                footerBalanceCountId='#LeadcoldCallingBalanceWrapper_'+sp[1];
            }
            var coldCallingCount=parseInt($(headerCountId).html());
            var isValid=1;//0-invalid,1-valid
            $('input[name="'+selectedName+'"]').each(function(index) {
                var var1ref=$('input[name="'+selectedName+'"]:eq('+index+')');
                var var1=var1ref.val();
                var currentVal=parseInt(var1);
                if(currentVal>coldCallingCount){
                    currentVal=0;
                    var1ref.val(null);

                    //alert('Should be less than available count ('+coldCallingCount+')');
                    var ModelTitle="Call Schedule";
                    var Description='Should be less than available count (' + coldCallingCount + ')';
                    customAlert(ModelTitle,Description);
                    $(this).addClass('required');
                    $('#btnSaveCallSchedules').attr('disabled','disabled');
                    return false;
                    isValid=0;
                }
                else{
                    if(var1) {

                        total = total + currentVal;
                    }
                }


            });
            if(isValid==0){
                $(footerBalanceCountId).html('-');
            }
            else if(isValid==1 && total>coldCallingCount){
                //alert('Total Should be less than available count ('+coldCallingCount+')');
                var ModelTitle="Call Schedule";
                var Description='Should be less than available count (' + coldCallingCount + ')';
                customAlert(ModelTitle,Description);
                $(this).addClass('required');
                $(footerBalanceCountId).html('-');
                isValid=0;
            }
            else if(isValid==1 && total<=coldCallingCount){
                $(footerBalanceCountId).html(coldCallingCount-total);
                userWorkLoad();
            }
            if(isValid==1 && (coldCallingCount-total)<coldCallingCount){
                $('#btnSaveCallSchedules').attr('disabled',false);
            }
        });

    }

    function buildCallsScheduleCountsData(response){

        var coldCallingCount=0;
        var rowHtml='';
        if(response.status===true)
        {
            rowHtml+="<tr><tr class='text-center'>";
            rowHtml+="<th  class=\"text-center\" style=\"vertical-align:middle;\">Calls vs Users</th>";
            rowHtml+="<th class=\"p5\" style=\"text-align:left\"><span class=\"cold-call-num\" id=\"coldCallingWrapper\"></span>Cold Calling</th>";
            rowHtml+="<th class=\"p5\" class=\"current-batch\" style=\"vertical-align:middle;text-align:left\">User Work Load</th>";
            rowHtml+="</tr>";
            rowHtml+="<tr><td class=\"border-none p0 lh10\">&nbsp;</td></tr>";
            $('#CallScheduleDetails thead').html(rowHtml);
            coldCallingCount=response.data;
            $('#coldCallingWrapper').html(coldCallingCount);
            $('#coldCallingBalanceWrapper').html(coldCallingCount);
        }
    }

    function userWorkLoad()
    {
        if(CallsScheduleDataResponse.status===true)
        {
            var totalworkload=0;
            $.each(CallsScheduleDataResponse.data,function(key,value){
                var workload=0;
                workload+=parseInt($('#inHandCallCountWrapper_'+value.user_id).val());
                var exp='txtColdCalling_'+value.user_id;
                if($("#"+exp) && $("#"+exp).val()!='') {
                    workload += parseInt($("#" + exp).val());
                }
                $("#"+'WorkLoadWrapper_'+value.user_id).html(workload);
                totalworkload+=workload;
            });
        }
        $('#TotalWorkLoadWrapper').html(totalworkload);
    }



    function saveSRCallSchedules(This){
        notify('Processing..', 'warning', 10);
        var callSchedules=[];

        $('input[name="txtColdCalling"]').each(function() {
            if($(this).val()) {
                var currentVal = parseInt($(this).val());
                var res = $(this).attr('id').split("_");
                callSchedules.push(res[1] + '-' + $(this).val());
            }
        });
        var callSchedules=callSchedules.join('|');

        var ajaxurl=API_URL+'index.php/SRCallSchedule/addSRCallSchedule';
        var userID=$('#hdnUserId').val();
        var action='schedule';
        var branchId=$('#userBranchList').val();
        var type="POST";
        var tagValue = $("#txtQuickContactTag").val();
        var sourceValue = $("#txtQuickContactSource").val();
        var params={branchId:branchId,callSchedules:callSchedules,chosenTag:tagValue,chosenSource:sourceValue};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        var response=
            commonAjaxCall({This:This,method:type,requestUrl:ajaxurl,params:params,headerParams:headerParams,action:action,onSuccess:saveCallSchedulesResponse});

    }
    function saveCallSchedulesResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            $('#btnSaveCallSchedules').attr('disabled',false);
            notify(response.message,'success',10);
            genereteSRSchedularData(this);
        }
        else{
            notify(response.message,'error',10);
        }
    }

    function confirmColdCallRelease(This,assignedUserId,releaseCnt){

        customConfirmAlert('Release Calls', 'Are you sure want to release the calls from the user ?');
        $("#popup_confirm #btnTrue").attr("onclick","SRColdCallRelease(this,"+assignedUserId+","+releaseCnt+")");
    }

    function SRColdCallRelease(This,assignedUserId,releaseCnt){

        notify('Processing..', 'warning', 10);
        var ajaxurl = API_URL + 'index.php/SRCallSchedule/releaseScheduledCalls';
        var releaseCnt = releaseCnt;
        var userID = $('#hdnUserId').val();
        var releaseCount = $('#inHandCallCountWrapper_'+assignedUserId).val();
        if(releaseCount > releaseCnt)
        {
            alertify.dismissAll();
            notify('Cannot release more than assigned calls','error',10);
        }
        else
        {
            var action = 'release';
            var branchId = $('#userBranchList').val();
            var type = "POST";
            var params = {branchId: branchId, assignedUserId: assignedUserId, releaseCnt: releaseCount};
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: userID
            };

            var response =
                commonAjaxCall({
                    This: This,
                    method: 'POST',
                    requestUrl: ajaxurl,
                    params: params,
                    headerParams: headerParams,
                    action: action,
                    onSuccess: saveReleaseCallSchedulesResponse
                });
        }


    }
    function saveReleaseCallSchedulesResponse(response){
        $('#popup_confirm').modal('hide');
        if(response.status===true){
            alertify.dismissAll();
            notify(response.message,'success',10);
            genereteSRSchedularData(this);
        }
        else{
            alertify.dismissAll();
            notify(response.message,'error',10);
        }
    }
</script>