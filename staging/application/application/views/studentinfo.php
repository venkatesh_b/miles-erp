<?php
/**
 * User: Abhilash
 */
$exp = 0;
if ($Sub_id != '') {
    $exp = explode('-', urldecode($Sub_id));
} else {
    $exp = 0;
}
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="leadId" value="<?php echo $Id ?>" />
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <input type="hidden" id="hdnLeadId" value="">
    <input type="hidden" id="hdnBranchXrefId" value="">
    <input type="hidden" id="hdnCoursefId" value="">
    <input type="hidden" id="hdnBatchfId" value="">

    <div class="content-header-wrap">
        <h3 class="content-header">Student Info</h3>

        <div class="content-header-btnwrap">
            <ul>
                <li><a class="" href="<?php echo APP_REDIRECT; ?><?php echo (($Sub_id != '' && isset($exp[1]) && trim($exp[1]) != '0' && trim($exp[1]) != '') ? $exp[1] : 'leads'); ?>"><i class="icon-times"></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" access-element="list"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="wrap-left col-sm-8 p0">
                <div class="wrap-head">
                    <table class="table table-responsive table-customised mb10">
                        <tr>
                            <td class="w140 text-center">
                                <div class="img-wrapper img-wrapper-leadinfo light-blue3-bg"> <img id="leadProfilePic" src="<?php echo BASE_URL; ?>assets/images/male-icon.jpg"> <span class="img-text" id="sequence_number"></span> </div>
                            </td>
                            <td class="p0 border-none" id="lead-info"></td>

                        </tr>

                    </table>
                </div>
                <div id="SecondCounselorHead" class="clearfix" style="display:none;">
                    <h4 class="heading-uppercase mb5"  >Second Level Counselor:</h4>
                    <div class="col-sm-12 p0 f13 white-bg valign-middle">

                        <div class="col-sm-4 p10 vertical-middle" id="createdDate"></div>
                        <div class="col-sm-4 p10 vertical-middle" id="createdBv"></div>
                        <div class="col-sm-4 p10 vertical-middle" id="secondCounsellerComments" style=""></div>
                    </div>
                </div>
                <div class="wrap-body">
                    <div class="tabs-section tabs-section-justified">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-justified mb0" id="tabs2" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#timeline" aria-controls="timeline" role="tab"
                                   data-toggle="tab">Timeline</a>
                            </li>
                            <li role="presentation">
                                <a href="#SR" onclick="loadSR()" aria-controls="SR" role="tab" data-toggle="tab">SR</a>
                            </li>
                            <li role="presentation">
                                <a href="#personal_info" aria-controls="personal_info" role="tab" data-toggle="tab">Personal
                                    Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#education_info" aria-controls="education_info" role="tab"
                                   data-toggle="tab">Education Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#other_info" aria-controls="other_info" role="tab" data-toggle="tab">Other
                                    Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#student_info" aria-controls="student_info" role="tab" data-toggle="tab" onclick="loadPlacementInfo()">Placement Assistance</a>
                            </li>
                            <li role="presentation">
                                <a href="#eligibility_info" aria-controls="eligibility_info" role="tab" data-toggle="tab" onclick="loadEligibilityDetails()">Eligibility</a>
                            </li>
                            <li role="presentation">
                                <a href="#student_reference" onclick="loadStudentReference(this)" aria-controls="student_reference" role="tab" data-toggle="tab">Student Reference</a>
                            </li>
                            <li role="presentation">
                                <a href="#fee_Deatails" onclick="loadFeeDetails(this)" aria-controls="fee_Deatails" role="tab" data-toggle="tab">Fee Details</a>
                            </li>
                            <li role="presentation" access-element="AdhocIssue">
                                <a href="#adhoc_stationary" onclick="loadStationaryIssue(this)" aria-controls="adhoc_stationary" role="tab" data-toggle="tab">Stationary</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="timeline">
                                <div class="tab-data">
                                    <div class="col-sm-12 mtb10 clearfix p0">
                                        <!--<div class="pull-left search-filter custom-select input-field">
                                            <select>
                                                <option>All type of messages</option>
                                               <option>Email</option>
                                                <option>Call</option>
                                            </select>
                                        </div>-->
                                        <div class="pull-right mt10">
                                            <a onclick="ManageTimeLine(this)" class="btn blue-btn" href="javascript:;" data-target="#addNotification" data-toggle="modal" ><i class="icon-plus-circle mr8"></i>Add Notes or Interaction</a>
                                        </div>

                                    </div>
                                    <div class="table-list">
                                        <table class="table table-responsive table-custom">
                                            <colgroup>
                                                <col width="20%">
                                                <col width="30%">
                                                <col width="30%">
                                                <col width="10%">
                                            </colgroup>
                                            <tbody id="timelines-list">

                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="personal_info">
                                <div class="col-sm-12 white-bg relative">
                                    <!--<a  class="lead-pinfo-edit-btn" onclick="ManageLeadsPersonalInformation(0)" data-target="#editPersonalinfo" data-toggle="modal"><i class="icon-edit f18"></i></a>-->
                                    <a onclick="ManagePersonalInfo(this)" class="lead-pinfo-edit-btn" data-target="#editPersonalinfo" data-toggle="modal"><i class="icon-edit f18"></i></a>
                                    <div class="col-sm-4 mtb10">

                                        <p class=" label-text">Name</p>
                                        <label class="label-data" id="leadName"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">

                                        <p class=" label-text">Gender</p>
                                        <label class="label-data" id="gender"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Course</p>
                                        <label class="label-data" id="leadCourse"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Mobile</p>
                                        <label class="label-data" id="leadmobile"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Alternative Mobile</p>
                                        <label class="label-data" id="leadAltNumber"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Secondary Mobile</p>
                                        <label class="label-data" id="secondaryLeadmobile"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Company</p>
                                        <label class="label-data" id="leadCompany"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Email</p>
                                        <label class="label-data" id="leadEmail"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Secondary Email</p>
                                        <label class="label-data" id="secondaryLeadEmail"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Designation</p>
                                        <label class="label-data" id="leadDesignation"></label>
                                    </div>
                                    <div class="col-sm-4 mtb10">
                                        <p class=" label-text">Education</p>
                                        <label class="label-data" id="leadEducation"></label>
                                    </div>
                                    <div class="col-sm-12 mtb10">
                                        <p class=" label-text">Address</p>
                                        <label class="label-data" id="leadAddress"></label>
                                    </div>
                                </div>


                            </div>
                            <div role="tabpanel" class="tab-pane" id="education_info">

                                <div class="col-sm-12 white-bg" id="LatestEducationalInfo-list">
                                </div>
                                <div class="col-sm-12 white-bg pt10" id="AllEducationalInfo-list">

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="other_info">
                                <div class="col-sm-12 white-bg ptb10">
                                    <ul class="other-info-check-list">
                                        <li>
                                            <span class="inline-checkbox">
                                                <input type="checkbox" class="filled-in" id="chkSignificanceOfCPA"/>
                                                <label for="chkSignificanceOfCPA">Significance of CPA course in your career progressive?</label>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="inline-checkbox">
                                                <input type="checkbox" class="filled-in" id="chkPursueTheCPA"/>
                                                <label for="chkPursueTheCPA">Willingness to pursue the CPA designation?</label>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="inline-checkbox">
                                                <input type="checkbox" class="filled-in" id="chkMilesCPA"/>
                                                <label for="chkMilesCPA">Response on Miles CPA Review course offerings?</label>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="inline-checkbox">
                                                <input type="checkbox" class="filled-in" id="chkValueAddition"  />
                                                <label for="chkValueAddition">Value-addition from today's presentation/discussion?</label>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="inline-checkbox">
                                                <input type="checkbox" class="filled-in" id="chkCmaNo"/>
                                                <label for="chkCmaNo">CMA No.</label>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-12 white-bg p0">

                                    <div class="col-sm-12 pb10">
                                        <button onclick="SaveOtherInfo(this)" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Save</button>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="student_info">
                                <div class="col-sm-12 white-bg ptb10">
                                    <!-- list -->
                                    <div class="col-sm-12 p0" id="resumeDownload"></div>
                                    <!-- list end -->
                                    <form id="manageResumeUpload" enctype="multipart/form-data">
                                        <div class="col-sm-12 p0">
                                            <div class="file-field input-field">
                                                <div class="btn file-upload-btn">
                                                    <span>Resume <em>*</em></span>
                                                    <input type="file" id="resumeAttachemnt">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input  class="file-path validate" id="" placeholder="Upload single file" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 p0">
                                            <div class="col-sm-12 p0">
                                                <div class="input-field">
                                                    <textarea id="resumeComment" class="materialize-textarea"></textarea>
                                                    <label for="resumeComment">Comment</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 white-bg p0">
                                            <div class="col-sm-12 pb10">
                                                <button onclick="SaveStudentResume(this)" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="student_reference">
                                <div class="col-sm-12 white-bg pt10 pl0">
                                    <div class="col-sm-10">
                                        <div class="input-field">
                                            <input id="txtLeadInfo" type="text" class="validate">
                                            <label for="txtLeadInfo">Mobile number <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 p0 mt2">
                                        <div class="input-field">
                                            <button id="btnLeadSnapshot" type="button" class="btn blue-btn" onclick="addStudentRefrence(this)">Add</button>
                                        </div>
                                    </div>
                                </div>
                                <input id="txtLeadInfoDetails" type="hidden" class="">
                                <div class="col-sm-12 white-bg ptb10">
                                    <!-- list -->
                                    <div class="col-sm-12 p0" >
                                        <!--<h5>Uploaded Documents</h5>-->
                                        <table id="reference_list" class="table table-responsive table-striped table-bordered table-custom" style="display: none;">
                                            <thead>
                                            <tr>
                                                <th style="padding:4px 8px;">Lead No.</th>
                                                <th style="padding:4px 8px;">Name</th>
                                                <th style="padding:4px 8px;width:15%;">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12 white-bg p15">
                                    <div class="input-field">
                                        <textarea id="lead_ref_comment" class="materialize-textarea"></textarea>
                                        <label for="lead_ref_comment">Comments</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 white-bg p0 pl15">
                                    <div class="col-sm-12 pb10">
                                        <button onclick="SaveStudentRefrence(this)" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Save</button>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="eligibility_info">
                                <div class="col-sm-12 white-bg ptb10">
                                    <form id="eligibilityForm" enctype="multipart/form-data">
                                        <div class="col-sm-12 p0">
                                            <div class="col-sm-5 white-bg p0">
                                                <div class="input-field">
                                                    <select id="eligibility" name="eligibility">
                                                        <option value=""  selected>--Select--</option>
                                                        <option value="Clear">Clear</option>
                                                        <option value="PGDA">PGDA</option>
                                                        <option value="Eligibility Check sent">Eligibility Check sent</option>
                                                    </select>
                                                    <label class="select-label">Eligibility <em>*</em></label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- list -->
                                        <div class="col-sm-12 p0" >
                                            <h5>Uploaded Documents</h5>
                                            <table id="eligibility_list" class="table table-responsive table-striped table-bordered table-custom">
                                                <thead>
                                                <tr>
                                                    <th>FIle Name</th>
                                                    <th style="width: 20%;">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- list end -->
                                        <div class="col-sm-12 p0">
                                            <div class="file-field input-field">
                                                <div class="btn file-upload-btn">
                                                    <span>Document Upload </span>
                                                    <input type="file" id="eligibilityAttachemnt">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input  class="file-path validate" id="" placeholder="Upload single file" type="text">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 white-bg p0">
                                            <div class="col-sm-12 pb10">
                                                <button onclick="SaveEligibility(this)" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="SR">
                                <div class="col-sm-12 white-bg" id="SR-list"> </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="fee_Deatails">
                                <div class="tab-data">
                                    <div class="col-sm-12 white-bg relative"></div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="adhoc_stationary">
                                <div class="tab-data">
                                    <div class="col-sm-12 white-bg relative"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>


            </div>
            <div class="sidebar-widget col-sm-4 pr0">
                <div class="sidebar-head light-dark-blue">
                    <h4 class="heading-uppercase m0">Team</h4>
                </div>

                <div class="sidebar-contenet  white-bg clearfix" id="History-list">


                </div>

            </div>


        </div>
        <!--Add Notification Modal-->
        <div class="modal fade" id="addNotification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="">Add Notifications or Interaction</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="form-wrapper clearfix">
                            <form id="manageTimeline" enctype="multipart/form-data">
                                <div class="icons-list">
                                    <ul>
                                        <li id="message" onclick="LeadTimelineTypes(this)" class="">
                                            <a href="javascript:;">
                                                <i class="icon-mail"></i>
                                                <span  class="f12 display-block">Message</span>
                                            </a>
                                        </li>
                                        <li id="call" onclick="LeadTimelineTypes(this)">
                                            <a href="javascript:;">
                                                <i class="icon-phone"></i>
                                                <span  class="f12 display-block">Call</span>
                                            </a>
                                        </li>
                                        <li id="Remainder" onclick="LeadTimelineTypes(this)">
                                            <a href="javascript:;">
                                                <i class="icon-clock"></i>
                                                <span  class="f12 display-block">Remainder</span>
                                            </a>
                                        </li>
                                        <li id="Meeting" onclick="LeadTimelineTypes(this)">
                                            <a href="javascript:;">
                                                <i class="icon-users1"></i>
                                                <span  class="f12 display-block">Meeting</span>
                                            </a>
                                        </li>
                                        <li id="Note" onclick="LeadTimelineTypes(this)">
                                            <a href="javascript:;">
                                                <i class="icon-edit-sqare"></i>
                                                <span  class="f12 display-block">Note</span>
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12 p0" id="timelineDp">
                                        <div class="input-field">
                                            <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="dpTimeline">
                                            <label for ="dpTimeline" class="datepicker-label">Date  <em>*</em></label>
                                        </div>
                                    </div>
                                    <input type="hidden" id="TypeOfTimeline" value="">
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <textarea id="txainforamtion" class="materialize-textarea"></textarea>
                                            <label for="txainformation">Information<em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="file-field input-field">
                                            <div class="btn file-upload-btn">
                                                <span>Attachments</span>
                                                <input  type="file" id="attachemntUpload">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input  class="file-path validate" id="" placeholder="Upload single file" type="text">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>


                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" onclick="AddLeadTimeLine(this)" class="btn blue-btn"><i class="icon-right mr8"></i>Save
                        </button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                class="icon-times mr8"></i>Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editPersonalinfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="">Edit Personal Info</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="form-wrapper clearfix">
                            <form id="ManageLeadsForm">
                                <div class="col-sm-12">
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input id="txtPinfoName" type="text" class="">
                                            <label for="txtPinfoName">Name <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <select id="ddlLeadInfoCourses" name="ddlLeadInfoCourses">
                                                <option value=""  selected>--Select--</option>

                                            </select>
                                            <label class="select-label">Courses <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input onkeypress="return isNumberKey(event)" maxlength="10" id="txtPinfoMobile" type="text" class="">
                                            <label for="txtPinfoMobile">Mobile</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0" id="gender">
                                        <div class="multiple-radio" ><label>Gender<em>*</em></label>
                                            <span class="inline-radio">
                                                <input class="with-gap"  name="txtUserGender" type="radio" id="male" value="male" />
                                                <label for="male">MALE</label>
                                            </span>
                                            <span class="inline-radio">
                                                <input class="with-gap" checked name="txtUserGender" type="radio" id="female" value="female"  />
                                                <label for="female">FEMALE</label>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input onkeypress="return isNumberKey(event)" maxlength="10" id="txtPinfoAltmobile" type="text" class="">
                                            <label for="txtPinfoAltmobile">Alternative Mobile <em>*</em></label>
                                        </div>
                                    </div>
                                    <!--<div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <select id="ddlLeadInfoCompanies" name="ddlLeadInfoCompanies">


                                            </select>
                                            <label class="select-label">Companies <em>*</em></label>
                                        </div>
                                    </div>-->
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input id="txtPinfoEmail" type="text" class="">
                                            <label for="txtPinfoEmail">Email <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12  p0" id="coldCallingContactTypeWrapper" >

                                    </div>

                                    <div class="col-sm-12 mb10 p0" id="contactType-corporate-wrap">
                                        <div class="input-field col-sm-11 p0">
                                            <select id="txtColdCallContactCompany" class="custom-select-nomargin">

                                            </select>
                                            <label class="select-label">Company <em>*</em></label>
                                        </div>

                                        <div class="col-sm-1 p0" access-element="add reference">
                                            <a class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn" onclick="resetCompanyWrapper(this)"><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                        </div>
                                        <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ComapnyWrapper">
                                            <div class="col-sm-8 p0 ">
                                                <div class="input-field mt15">
                                                    <input id="txtComapnyNameWrapper" type="text" class="validate" value="">
                                                    <label for="txtComapnyNameWrapper">Company Name <em>*</em></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 p0 ">
                                                <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="addCompanyWrapper(this)"></span></a>
                                                <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 mb10 p0" id="contactType-Institution-wrap">
                                        <div class="input-field col-sm-11 p0">
                                            <select id="txtColdCallContactInstitution" class="custom-select-nomargin">

                                            </select>
                                            <label class="select-label">Institution <em>*</em></label>
                                        </div>
                                        <div class="col-sm-1 p0" access-element="add reference">
                                            <a class="mt10 ml5 display-inline-block cursor-pointer showAddInstituteWrap-btn" onclick="resetInstitutionWrapper(this)"><span class="icon-plus-circle f24 mt20 diplay-inline-block"></span></a>
                                        </div>
                                        <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="InstitutionWrapper">
                                            <div class="col-sm-8 p0">
                                                <div class="input-field mt15">
                                                    <input id="txtInstitutionNameWrapper" type="text" class="validate" value="">
                                                    <label for="txtInstitutionNameWrapper">Institution Name <em>*</em></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 p0">
                                                <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" onclick="addInstitutionWrapper(this)"></span></a>
                                                <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0 mb20" >

                                        <div class="input-field col-sm-11 p0 custom-select-wrap ">
                                            <select class="chosen-select" id="ddlLeadInfoDesignation" name="ddlLeadInfoDesignation">
                                                <option value=""  selected>--Select--</option>

                                            </select>
                                            <label class="select-label">Designation <em>*</em></label>

                                        </div>
                                        <div class="col-sm-1 p0">
                                            <a onclick="resetDesignationWrapper(this)" class="mt10 ml5 display-inline-block cursor-pointer showAddCompanyWrap-btn"  href="javascript:;" ><span class="icon-plus-circle f24 mt20 diplay-inline-block sky-blue"></span></a>
                                        </div>
                                        <div class="col-sm-11 p0 coldCall-addCompanyWrap company-name-wrap" id="ContactTagWrapper">
                                            <div class="col-sm-8 p0 mt15">
                                                <div class="input-field">
                                                    <input id="txtLeadInfoDesignation" type="text" class="validate" value="">
                                                    <label for="txtLeadInfoDesignation">Designation <em>*</em></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 p0 ">
                                                <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-right"><span class="fa fa-check-circle f18 diplay-inline-block green" href="javascript:;" onclick="SaveLeadPersonalInfoDesignation(this)"></span></a>
                                                <a class="mt20 sky-blue display-inline-block cursor-pointer company-icon-cancel"><span class="fa fa-times-circle f18 diplay-inline-block  red"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <select id="ddlLeadInfoPostEducation" name="ddlLeadInfoPostEducation">
                                                <option value=""  selected>--Select--</option>

                                            </select>
                                            <label class="select-label">Qualification Level <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <select id="ddlLeadInfoEducation" name="ddlLeadInfoEducation">
                                                <option value=""  selected>--Select--</option>

                                            </select>
                                            <label class="select-label">Qualification <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <textarea id="ddlLeadAddress" class="materialize-textarea"></textarea>
                                            <label class="" for="ddlLeadAddress">Address</label>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" onclick="UpdatePersonalInfo(this)" class="btn blue-btn"><i class="icon-right mr8"></i>Save
                        </button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                class="icon-times mr8"></i>Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade"  id="EducationModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEducational"
             data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Education Info</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="form-wrapper clearfix">
                            <form id="ManageEducationalInfoForm">
                                <div class="col-sm-12">
                                    <input type="hidden" id="hdnEducationInfoId" value="0">
                                    <div class="col-sm-12 p0">
                                        <div class="input-field" id="txtCourseText">
                                            <input id="txtEduName" type="text" class="">
                                            <label for="txtEduName">Qualification  <em>*</em></label>
                                        </div>
                                        <div id="txtCourseDropdown">
                                            <div class="input-field" >
                                                <select id="txtLeadInfoPostQualification">

                                                </select>
                                                <label class="select-label">Qualification Level <em>*</em></label>
                                            </div>
                                            <div class="input-field">
                                                <select id="txtLeadInfotQualification">

                                                </select>
                                                <label class="select-label">Qualification <em>*</em></label>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input id="txtEduUniversity" type="text" class="">
                                            <label for="txtEduUniversity">University  <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <input maxlength="5"  id="txtEduPercentage" type="text" class="">
                                            <label for="txtEduPercentage">Percentage  <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">

                                            <!--<input type="date" class="datepicker relative enablePastDates" placeholder="dd/mm/yyyy" id="dpEduYearCompletion">
                                            <label for ="dpEduYearCompletion" class="datepicker-label">Year of Completion  <em>*</em></label>-->
                                            <select id="dpEduYearCompletion">

                                            </select>
                                            <label class="select-label">Year of Completion</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p0">
                                        <div class="input-field">
                                            <textarea id="txaEduComments" class="materialize-textarea"></textarea>
                                            <label for="txaEduComments">Comments</label>
                                        </div>
                                    </div>

                                </div>
                            </form>

                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" onclick="SaveEducationalInfoDetails(this)" class="btn blue-btn"><i class="icon-right mr8"></i>Save
                        </button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i
                                class="icon-times mr8"></i>Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!--calling-->
        <div class="modal fade" id="callStatus" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button onclick="closeLeadCallStatus(this)" type="button" class="close" ><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Call Status</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix" id="CallStatusData">
                        <div class="col-sm-12 grey-bg">
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Name</span>
                                    <p id="BxrefLeadName"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Email</span>
                                    <p id="BxrefLeadEmail"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Phone</span>
                                    <p id="BxrefLeadPhone"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">City</span>
                                    <p id="BxrefLeadCity"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Country</span>
                                    <p id="BxrefLeadCountry"></p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <span class="display-block ash">Last Follow up</span>
                                    <p id="BxrefLeadCreatedDate"></p>
                                </div>
                            </div>
                        </div>
                        <form id="ManageCallStatus">
                            <input type="hidden" id="hdnBxrefLeadId" value="">
                            <input type="hidden" id="hdnLeadStage" value="">
                            <input type="hidden" id="hdnLead_id" value="">
                            <input type="hidden" id="hdnLeadStageText" value="">
                            <div class="col-sm-12 pt5">
                                <div class="input-field">
                                    <select id="callsTypes" onchange="CallStatusChange()">

                                    </select>
                                    <label id="callsTypes" for="callsTypes" class="select-label">Call Status</label>
                                </div>
                            </div>
                            <div class="col-sm-12 pt5" id="FutureDatesPicker" style="display:none;">
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="dpNextFollowUp">
                                        <label for ="dpNextFollowUp" class="datepicker-label">Next Follow Up Date  <em>*</em></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12" id="CallStatusInformation" style="display:none;">
                                <div class="multiple-radio" >
                                    <span class="inline-radio">
                                        <input class="with-gap"  name="callStatusInfo_radio" type="radio" id="callStatusInfoIntrested" value="callStatusInfoIntrested" />
                                        <label for="callStatusInfoIntrested">Interested <span id="intrestedLevelLabel"></span></label>
                                    </span>
                                    <span class="inline-radio">
                                        <input class="with-gap" checked name="callStatusInfo_radio" type="radio" id="callStatusInfoNotintrested" value="callStatusInfoNotintrested"  />
                                        <label for="callStatusInfoNotintrested">Not interested <span>{M2}</span></label>
                                    </span>
                                    <span class="inline-radio">
                                        <input class="with-gap" name="callStatusInfo_radio" type="radio" id="callStatusInfoDND"  value="callStatusInfoDND" />
                                        <label for="callStatusInfoDND">DND <span>(L0)</span></label>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-12 pt5" id="CommonCallStatusInformation">
                                <div class="input-field">
                                    <select id="CallAnswerTypes" onchange="getCallAnswerTypes(this)">

                                    </select>
                                    <label for="CallAnswerTypes" class="select-label">Answer</label>
                                </div>
                            </div>
                            <div class="col-sm-12 pt5" id="CommonCallStatusDatePicker" style="display:none;">
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="txtCommonCallStatusDatePicker">
                                        <label for ="txtCommonCallStatusDatePicker" id="txtLabelCommonCallStatusDatePicker" class="datepicker-label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 pt5" id="VisitedDatesPicker" style="display:none;">
                                <div class="col-sm-12 p0">
                                    <div class="input-field">
                                        <input type="date" class="datepicker relative enableFutureDates" placeholder="dd/mm/yyyy" id="dpvisitedDate">
                                        <label for ="dpvisitedDate" class="datepicker-label">Next Visitied Date  <em>*</em></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12" id="callStatusComments" style="display:none;">
                                <div class="input-field">
                                    <textarea id="txaCallStatusDescription" class="materialize-textarea"></textarea>
                                    <label class="" for="txaCallStatusDescription">Comments <em>*</em></label>
                                </div>
                            </div>
                            <div class="coldcall-info" style="display:none;">
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <select>
                                            <option value="" disabled selected>--Select--</option>
                                            <option value="1">CPA</option>
                                            <option value="2">CMA</option>
                                            <option value="3">PGDA</option>
                                        </select>
                                        <label class="select-label">Course</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <select>
                                            <option value="" disabled selected>--Select--</option>
                                            <option value="1">Hyderabad</option>
                                            <option value="2">Chennai</option>
                                            <option value="3">Bangalore</option>
                                        </select>
                                        <label class="select-label">Branch</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <input id="qlf" type="text" class="validate" >
                                        <label for="qlf">Qualification</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <input id="cmp" type="text" class="validate" >
                                        <label for="cmp">Company</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <input id="inst" type="text" class="validate" >
                                        <label for="inst">Institution</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="changeCallStatus(this)" class="btn blue-btn"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" onclick="closeLeadCallStatus(this)" class="btn blue-light-btn" ><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- InstanceEndEditable --></div>
</div>
<script>

    $context = '<?php echo (($Sub_id != '' && isset($exp[0]) && trim($exp[0]) != '0' && trim($exp[0]) != '') ? $exp[0] : 'Student Relation'); ?>';
    $pageUrl = '<?php echo (($Sub_id != '' && isset($exp[1]) && trim($exp[1]) != '0' && trim($exp[1]) != '') ? $exp[1] : 'Students'); ?>';
    $serviceurl = '<?php echo (($Sub_id != '' && isset($exp[1]) && trim($exp[1]) != '0' && trim($exp[1]) != '') ? $exp[1] : 'students'); ?>';
    $glb_lead_id='';
    $glb_branchxref_lead_id='';
    docReady(function () {
        if($("#userBranchList").length > 0)
            $("#userBranchList").parentsUntil('div.BranchName-dropdown').remove();

        leadInfoPageLoadMain();
    });
    function leadInfoPageLoadMain(){
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/LeadsInfo/getleadId';
        var params = {'branch_xref_lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: leadInfoPageLoadMainResponse});
    }
    function leadInfoPageLoadMainResponse(response){
        if(response.status===true){
            if(response.data[0].lead_id){
                $('#leadId').val(response.data[0]['lead_id']);
                $glb_lead_id=response.data[0]['lead_id'];
                $glb_branchxref_lead_id='<?php echo $Id; ?>';
                $glb_batch_id=response.data[0]['leadBatchId'];

                var resume = '<p class="dark-sky-blue f12">';
                if (response.data[0].placement_file != null && response.data[0].placement_file != '') {
                    resume += '<a download target="_blank" href="' + BASE_URL + 'uploads/' + response.data[0].placement_file + '"> Download resume   <i class="icon-arrow-down-circle f16 pull-left"></i></a>';
                } else {
                    resume += '<span class="pull-left">No resume uploaded</span>';
                }
                if (response.data[0].placement_comments != null) {
                    resume += '<span class="pull-right">'+response.data[0].placement_comments+'</span>';
                } else {
                    resume += '<span class="pull-right">No comments found</span>';
                }
                resume += '</p>';
                $('#resumeDownload').html(resume);

                leadInfoPageLoad();
                $processedStatus=1;
                //getLeadInfoQualifications();
                getLeadInfoCompanies();
                getLeadInfoInstitutions();
                getLeadInfoContactTypes();
            }
        }
    }
    function leadInfoPageLoad(This)
    {
        var userId = $('#hdnUserId').val();
        var branchXrefId = $('#hdnBranchXrefId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        ajaxurl = API_URL + 'index.php/LeadsInfo/getleadById';
        params = {'lead_id': $glb_lead_id,'branch_xref_lead_id': $glb_branchxref_lead_id}
        commonAjaxCall({This: This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: leadInfoResponse});

        var ajaxurl = API_URL + 'index.php/LeadsInfo/getLeadHistoryById';
        var params = {'lead_id': $glb_lead_id,'branch_xref_lead_id': $glb_branchxref_lead_id}
        commonAjaxCall({This: This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: leadHistoryResponse});

        ajaxurl = API_URL + 'index.php/LeadsInfo/getLeadEducationalInfo';
        params = {'lead_id': $glb_lead_id,'branch_xref_lead_id': $glb_branchxref_lead_id}
        commonAjaxCall({This: This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: leadEducationalInfoResponse});

        ajaxurl = API_URL + 'index.php/LeadsInfo/getOtherInfoDetailsByLeadId';
        params = {'lead_id': $glb_lead_id,'branch_xref_lead_id': $glb_branchxref_lead_id}
        commonAjaxCall({This: This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: leadOtherInfoResponse});

        ajaxurl = API_URL + 'index.php/LeadsInfo/getTimelinesById';
        params = {'lead_id': $glb_lead_id,'branch_xref_lead_id': $glb_branchxref_lead_id}
        commonAjaxCall({This: This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: leadTimelinesResponse});

    }

    function leadTimelinesResponse(response) {
        var timelineInfo = "";
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            timelineInfo += '<div class="text-center">No Data found</div>';
        } else
        {
            for (var i = 0; i < response.data.length; i++) {
                var CreatedOn = (response.data[i].created_on == null || response.data[i].created_on == '') ? "----" : response.data[i].created_on;
                var Name = (response.data[i].name == null || response.data[i].name == '') ? "----" : response.data[i].name;
                var Designation = (response.data[i].Designation == null || response.data[i].Designation == '') ? "----" : response.data[i].Designation;
                var Description = (response.data[i].description == null || response.data[i].description == '') ? "----" : response.data[i].description;
                var TimelineType = (response.data[i].type == null || response.data[i].type == '') ? "----" : response.data[i].type;
                timelineInfo += '<tr><td>'
                    + '<p class="">' + CreatedOn + '</p></td><td>'
                    + '<p class="f14 font-bold">' + Name + '</p><span class="pl0">' + Designation + '</span></td>'
                    + '<td><p class="dark-sky-blue f12">' + Description + '</p></td>'
                    + '<td><p class="dark-sky-blue f12">' + TimelineType + '</p></td>';
                if (response.data[i].interaction_file != null) {
                    timelineInfo += '<td><p class="dark-sky-blue f12"><a target="_blank" href="' + BASE_URL + 'uploads/' + response.data[i].interaction_file + '"><i class="icon-arrow-down-circle f16"></i></a></p></td>';
                } else {
                    timelineInfo += '<td><p class="dark-sky-blue f12">----</p></td>';
                }
                timelineInfo += '</tr>';
            }

        }
        $('#timelines-list').html(timelineInfo);
    }
    function leadOtherInfoResponse(response) {
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
        } else
        {
            if (response.data[0].Significance_of_CPA == 1) {
                $("#chkSignificanceOfCPA").attr("checked", true);
            } else {
                $("#chkSignificanceOfCPA").attr("checked", false);
            }
            if (response.data[0].Willingness_to_pursue_the_CPA == 1) {
                $("#chkPursueTheCPA").attr("checked", true);
            } else {
                $("#chkPursueTheCPA").attr("checked", false);
            }
            if (response.data[0].Response_on_Miles_CPA == 1) {
                $("#chkMilesCPA").attr("checked", true);
            } else {
                $("#chkMilesCPA").attr("checked", false);
            }
            if (response.data[0].Value_addition == 1) {
                $("#chkValueAddition").attr("checked", true);
            } else {
                $("#chkValueAddition").attr("checked", false);
            }
            if (response.data[0].CMA_No == 1) {
                $("#chkCmaNo").attr("checked", true);
            } else {
                $("#chkCmaNo").attr("checked", false);
            }
            /*if(response.data[0].eligibility!=''){
             $('#otherInfoEligibility').val(response.data[0].eligibility);
             $('#otherInfoEligibility').material_select();
             }*/
        }


    }
    function leadInfoResponse(response) {
        var leadinfo = '';

        var userId = $('#hdnUserId').val();

        var action = "list";
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            leadinfo += '<div class="text-center">No Data found</div>';
        } else
        {
            $("#hdnBranchXrefId").val(response.data[0].branch_xref_lead_id);
            $("#hdnBatchfId").val(response.data[0].batch_id);

            $.when(
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name: 'Company'},
                        action: action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Courses/getAllCourseList',
                        action: action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name: 'Designation'},
                        action: action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name: 'Qualification'},
                        action: action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/LeadsInfo/getBranchVisitorById',
                        params: {branchXrefId: $('#hdnBranchXrefId').val()},
                        action: action
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: this,
                        headerParams: headerParams,
                        requestUrl: API_URL + 'index.php/Referencevalues/getReferenceValuesByName',
                        params: {name: 'Post Qualification'},
                        action: action
                    }
                )
            ).then(function (leadInfoDataCompanies, leadInfoDataCourses, leadInfoDesignations, leadInfoQualification, branchVisit,leadInfoPostQualification) {
                leadInfoDataCompanies = leadInfoDataCompanies[0];
                leadInfoDataCourses = leadInfoDataCourses[0];
                leadInfoDesignations = leadInfoDesignations[0];
                //leadInfoQualification = leadInfoQualification[0];
                leadInfoPostQualification = leadInfoPostQualification[0];
                var BranchVisitor = branchVisit[0];
                if (typeof BranchVisitor !== 'undefined' && BranchVisitor.data.length > 0) {

                    var branchVisitHtml = '';
                    branchVisitHtml += "<h4 class=\"heading-uppercase mb5\"  >Second Level Counselor:</h4>";
                    $.each(BranchVisitor.data, function (key, value) {
                        branchVisitHtml += "<div class=\"col-sm-12 p0 f13 white-bg valign-middle\">";
                        branchVisitHtml += "<div class=\"col-sm-4 p10 vertical-middle\" ><span><b>Counselling done on: </b></span>" + value.created_date + "</div>";
                        branchVisitHtml += "<div class=\"col-sm-4 p10 vertical-middle\" ><span><b>Counselling done by: </b></span>" + value.name + "</div>";
                        branchVisitHtml += "<div class=\"col-sm-4 p10 vertical-middle\"><span><b>Counselor comments: </b></span><span style=\"width: 100px;\" class=\"tooltipped ellipsis\" data-position=\"top\" data-tooltip=\"" + value.second_level_counsellor_comments + "\">" + value.second_level_counsellor_comments + "</div>";
                        branchVisitHtml += "</div>";
                    });

                    $("#SecondCounselorHead").show();
                    $('#SecondCounselorHead').html(branchVisitHtml);
                    (BranchVisitor.data[0].second_level_counsellor_comments.length > 0) ? $("#secondCounsellerComments").html("<span><b>Counselor comments:</b></span> <span style=\"width: 150px;\" class=\"tooltipped ellipsis\" data-position=\"top\" data-tooltip=\"" + BranchVisitor.data[0].second_level_counsellor_comments + "\">" + BranchVisitor.data[0].second_level_counsellor_comments + '</span>') : '';
//                    (BranchVisitor.data[0].second_level_counsellor_comments.length>0) ? $("#secondCounsellerComments").html("<a href=\"javascript:;\" class=\"tooltipped\" data-position=\"top\" data-tooltip=\""+BranchVisitor.data[0].second_level_counsellor_comments+"\" ><span><b>Counselor comments:</b></span> "+BranchVisitor.data[0].second_level_counsellor_comments+'</a>') : '';
                    (BranchVisitor.data[0].name.length > 0) ? $("#createdBv").html("<span><b>Counselling done by:</b></span> " + BranchVisitor.data[0].name) : '';
                    (BranchVisitor.data[0].created_date.length > 0) ? $("#createdDate").html("<span><b>Counselling done on:</b></span> " + BranchVisitor.data[0].created_date) : '';

                }
                getLeadPersonalInfoCompanies(leadInfoDataCompanies);
                getLeadPersonalInfoCourses(leadInfoDataCourses);
                getLeadPersonalInfoDesignations(leadInfoDesignations);
                getLeadPersonalInfoPostQualification(leadInfoPostQualification,response.data[0].parent_qual_id,response.data[0].qual_id);
                //getLeadPersonalInfoQualification(leadInfoQualification);

                var last_modified = (response.data[0].last_modified == null || response.data[0].last_modified == '') ? response.data[0].created_by : response.data[0].last_modified;
                var last_follow_up = (response.data[0].created_on == null || response.data[0].created_on == '') ? "----" : response.data[0].created_on;
                var next_follow_up = (response.data[0].next_followup_date == null || response.data[0].next_followup_date == '') ? "----" : response.data[0].next_followup_date;
                var Lead_name = (response.data[0].name == null || response.data[0].name == '') ? "----" : response.data[0].name;
                var Lead_email = (response.data[0].email == null || response.data[0].email == '') ? "----" : response.data[0].email;
                var Lead_company = (response.data[0].company == null || response.data[0].company == '') ? "----" : response.data[0].company;
                var Lead_phone = (response.data[0].phone == null || response.data[0].phone == '') ? "----" : response.data[0].phone;
                var Lead_course = (response.data[0].course == null || response.data[0].course == '') ? "----" : response.data[0].course;
                var Lead_course_id = (response.data[0].course_id == null || response.data[0].course_id == '') ? "----" : response.data[0].course_id;
                var Lead_stage = (response.data[0].leadStage == null || response.data[0].leadStage == '') ? "----" : response.data[0].leadStage;
                var Lead_contactSource = (response.data[0].contactSource == null || response.data[0].contactSource == '') ? "----" : response.data[0].contactSource;
                leadinfo += '<div class="col-sm-10 p0"><p class="f14 font-bold p5 m0">' + Lead_name + '</p><p class="pl5 pt0 m0">'
                    + '<lable class="clabel">Email : </lable>'
                    + '<a href="mailto:' + Lead_email + '" class="dark-sky-blue">' + Lead_email + '</a></p>'
                    + '<p class="pl5 pt0 m0"><lable class="clabel">Mobile : </lable>'
                    + '<a href="callto:' + Lead_email + '" class="dark-sky-blue">' + Lead_phone + '</a></p>'
                    + '<p class="pl5 pt0 m0"> <lable class="clabel">Company : </lable>' + Lead_company + '</p>';
                if(Lead_contactSource!='----'){
                    leadinfo += '<p class="pl5 pt0 m0"> <lable class="clabel">Source : </lable>'+Lead_contactSource+'</p>';
                }
                leadinfo +='</div>';
                leadinfo += '<div class="col-sm-2 p15 text-center blue-border-left blue-border-left">'
                    + '<p class="mt30">Course</p>'
                    + '<span class="f14 font-bold pl0">' + Lead_course + '</span></div>'
//                        + '<div class="col-sm-3 text-center p15 blue-border-left"> <p class="mt30">Lead Stage</p><span class="f14 font-bold pl0">' + Lead_stage + '</span><a class="ml5 mr5 tooltipped" onclick="manageLeadCallStatus(this,' + $("#hdnBranchXrefId").val() + ')" data-toggle="modal" data-target="#callStatus" href="javascript:;" data-position="top" data-tooltip="Change Call Status" ><i class="fa fa-phone green  valign-middle f14"></i></a></div>'
                    + '<ul class="list-info col-sm-12 mt0 p0">'
                    + '<li class=""><label>Last Modified By:</label> ' + last_modified + '</li>'
                    + '<li class=""><label>Last Follow Up:</label> ' + last_follow_up + '</li>'
                    + '<li class=""><label>Next Follow Up:</label> ' + next_follow_up + '</li></ul>';
                (response.data[0].lead_number == null || response.data[0].lead_number == '') ? "----" : $("#sequence_number").text(response.data[0].lead_number);
                $('#hdnCoursefId').val(Lead_course_id);

                $('#lead-info').html(leadinfo);
                (response.data[0].name == null || response.data[0].name == '') ? $('#leadName').html("----") : $('#leadName').html(response.data[0].name);
                (response.data[0].course == null || response.data[0].course == '') ? $('#leadCourse').html("----") : $('#leadCourse').html(response.data[0].course);
                (response.data[0].phone == null || response.data[0].phone == '') ? $('#leadmobile').html("----") : $('#leadmobile').html(response.data[0].phone);
                (response.data[0].alternate_mobile == null || response.data[0].alternate_mobile == '') ? $('#leadAltNumber').html("----") : $('#leadAltNumber').html(response.data[0].alternate_mobile);
                (response.data[0].company == null || response.data[0].company == '') ? $('#leadCompany').html("----") : $('#leadCompany').html(response.data[0].company);
                (response.data[0].email == null || response.data[0].email == '') ? $('#leadEmail').html("----") : $('#leadEmail').html(response.data[0].email);
                (response.data[0].company == null || response.data[0].company == '') ? $('#leadCompany').html("----") : $('#leadCompany').html(response.data[0].company);
                (response.data[0].qualification == null || response.data[0].qualification == '') ? $('#leadEducation').html("----") : $('#leadEducation').html(response.data[0].qualification);
                (response.data[0].designation == null || response.data[0].designation == '') ? $('#leadDesignation').html("----") : $('#leadDesignation').html(response.data[0].designation);
                (response.data[0].secondary_email == null || response.data[0].secondary_email == '') ? $('#secondaryLeadEmail').html("----"):$('#secondaryLeadEmail').html(response.data[0].secondary_email);
                (response.data[0].secondary_phone == null || response.data[0].secondary_phone == '') ? $('#secondaryLeadmobile').html("----"):$('#secondaryLeadmobile').html(response.data[0].secondary_phone);
                /*(response.data[0].address == null || response.data[0].address == '') ? $('#leadAddress').html("----") : $('#leadAddress').html(response.data[0].address);*/
                if(response.data[0].address == null){
                    $('#leadAddress').html("----");
                    $('#ddlLeadAddress').val('');
                    $('#ddlLeadAddress').next('label').removeClass("active");
                }else if(response.data[0].address == ''){
                    $('#leadAddress').html("----");
                    $('#ddlLeadAddress').val('');
                    $('#ddlLeadAddress').next('label').removeClass("active");
                }else{
                    $('#leadAddress').html(response.data[0].address);
                    $('#ddlLeadAddress').val(response.data[0].address);
                    $('#ddlLeadAddress').next('label').addClass("active");
                }

                /*if(response.data[0].eligibility!=''){
                 $('#eligibility').val(response.data[0].eligibility);
                 $('#eligibility').material_select();
                 }*/

                $("#hdnLeadId").val(response.data[0].lead_id);


                $('#txtPinfoName').val(response.data[0].name);
                $("#txtPinfoName").next('label').addClass("active");
                if(response.data[0].phone.trim()!='') {
                    $('#txtPinfoMobile').val(response.data[0].phone);
                    $('#txtPinfoMobile').attr('disabled', true);
                    $("#txtPinfoMobile").next('label').addClass("active");
                    $("#txtPinfoMobile").prop('readonly', true);
                }

                if (response.data[0].gender == 1) {
                    $("#gender").text("Male");
                    $("#male").attr('checked', 'checked');

                    $("#leadProfilePic").attr('src', "<?php echo BASE_URL; ?>assets/images/male-icon.jpg");

                }
                if (response.data[0].gender == 0) {
                    $("#gender").text("Female");
                    $("#female").attr('checked', 'checked');
                    $("#leadProfilePic").attr('src', "<?php echo BASE_URL; ?>assets/images/female-icon.jpg");
                }

                if (response.data[0].alternate_mobile != null)
                {
                    $('#txtPinfoAltmobile').val(response.data[0].alternate_mobile);
                    $("#txtPinfoAltmobile").next('label').addClass("active");
                }


                $('#txtPinfoEmail').val(response.data[0].email);
                $("#txtPinfoEmail").next('label').addClass("active");

                $('#ddlLeadInfoCompanies').val(response.data[0].companyID);
                $('#ddlLeadInfoCompanies').material_select();

                $('#ddlLeadInfoCourses').val(response.data[0].course_id);
                $('#ddlLeadInfoCourses').material_select();

                $('#ddlLeadInfoDesignation').val(response.data[0].desi_id);
                //$('#ddlLeadInfoDesignation').material_select();
                $('#ddlLeadInfoDesignation').trigger("chosen:updated");

                $("input[name='coldCallcontacttype'][value='" + response.data[0].contactType + "']").prop('checked', true);
                $("input[name='coldCallcontacttype']:checked").click();
                if (response.data[0].companyID != null) {
                    $('#txtColdCallContactCompany').val(response.data[0].companyID);
                    $('#txtColdCallContactCompany').material_select();
                }
                if (response.data[0].fk_institution_id != null) {
                    $('#txtColdCallContactInstitution').val(response.data[0].fk_institution_id);
                    $('#txtColdCallContactInstitution').material_select();
                }

                if (response.data[0].is_feeexist == 1) {
                    $('#ddlLeadInfoCourses').attr('disabled', true);
                    $("input[name='coldCallcontacttype']").attr('disabled', true);
                    $('#txtColdCallContactCompany').attr('disabled', true);
                    $('#txtColdCallContactInstitution').attr('disabled', true);
                }


                buildpopover();
                if($processedStatus == 0){
                    alertify.dismissAll();
                }else if($processedStatus == 1){
                    $processedStatus=0;
                }
            });
        }
    }
    function leadHistoryResponse(response) {
        var History = "";
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            History += '<div class="text-center">No Data found</div>';
        } else
        {
            for (var i = 0; i < response.data.length; i++)
            {
                if (i == 0)
                {
                    var LeadHis_mail = (response.data[i].email == null || response.data[i].email == '') ? "----" : response.data[i].email;
                    var LeadHis_designation = (response.data[i].Designation == null || response.data[i].Designation == '') ? "----" : response.data[i].Designation;
                    var LeadHis_name = (response.data[i].name == null || response.data[i].name == '') ? "----" : response.data[i].name;
                    var LeadHis_phone = (response.data[i].phone == null || response.data[i].phone == '') ? "----" : response.data[i].phone;
                    var userImage='';
                    if(response.data[i].image && CheckFileExists(response.data[i].image))
                    {
                        userImage += '<img src="'+response.data[i].image+'" class="mCS_img_loaded">';

                    }
                    else
                    {
                        userImage += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png" class="mCS_img_loaded">';
                    }
                    History += '<div class="p-following clearfix">'
                        + '<h4 class="text-uppercase anchor-blue pl10 f14">Present Following</h4>'
                        + '<div class="col-sm-12 ptb10 userworking-info-wrap">'
                        + '<div class="userworking-img"> '+userImage+' </div>'
                        + '<div class="pull-left pl5 pt5 inline-lable clearfix">'
                        + '<label class="label-data ellipsis">' + LeadHis_name + '</label>'
                        + '<p class="label-text ellipsis">' + LeadHis_designation + '</p>'
                        + '</div><div class="col-sm-12 mt5 p0 user-mail-custom">'
                        + '<p class="m0 clearfix">'
                        + '<lable class="clabel">Email :</lable>'
                        + '<a class="dark-sky-blue" href="mailto:' + LeadHis_mail + '">' + LeadHis_mail + '</a></p>'
                        + '<p class="m0">'
                        + '<lable class="clabel">Mobile :</lable>'
                        + '<a class="dark-sky-blue" href="callto:' + LeadHis_phone + '">' + LeadHis_phone + '</a></p>'
                        + '</div>'
                        + '</div>'
                        + '</div>';
                } else
                {
                    LeadHis_mail = (response.data[i].name == null || response.data[i].name == '') ? "----" : response.data[i].name;
                    LeadHis_designation = (response.data[i].Designation == null || response.data[i].Designation == '') ? "----" : response.data[i].Designation;
                    var userImage='';
                    if(response.data[i].image && CheckFileExists(response.data[i].image))
                    {
                        userImage += '<img src="'+response.data[i].image+'" class="mCS_img_loaded">';

                    }
                    else
                    {
                        userImage += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png" class="mCS_img_loaded">';
                    }
                    History += '<div class="col-sm-12 ptb10 userworking-info-wrap border-bottom">'
                        + '<div class="userworking-img"> '+userImage+' </div>'
                        + '<div class="pull-left pl5 pt5 inline-lable">'
                        + '<label class="label-data ellipsis">' + LeadHis_mail + '</label>'
                        + '<p class="label-text ellipsis">' + LeadHis_designation + '</p>'
                        + '</div>'
                        + '</div>';

                }
            }

        }
        $('#History-list').html(History);

    }
    function leadEducationalInfoResponse(response) {

        var LatestEducationalInfo = "";
        var AllEducationalInfo = "";
        AllEducationalInfo += '<a access-element="add" class="tooltipped" data-position="top" data-tooltip="Add Eduction Info" style="float:right;" href="javascript:;"  onclick="ManageEducationalInfo(this,0)" ><i class="icon-plus-circle f18"></i></a>';
        AllEducationalInfo += '<table id="EducationInfoTable" class="table table-responsive table-striped table-custom"><thead>'
            + '<tr> <th>Qualification</th>'
            + '<th>University</th>'
            + '<th>Year of Completion</th>'
            + '<th>Percentage</th>'
            + '<th>Comments</th>'
            + '<th></th>'
            + '</tr>'
            + '<tr><td class="border-none p0 lh10">&nbsp;</td></tr> </thead>';
        LatestEducationalInfo += '<a access-element="add" class="lead-pinfo-add-btn" onclick="ManageStudentEducationalInfo(this,0)" ><i class="icon-plus-circle f18"></i></a>';
        if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            LatestEducationalInfo += '<center class="ptb10">No Data found</center>';
            AllEducationalInfo += '<tbody><tr><td colspan="6" class="text-center">No Details Found</td></tr></tbody>';
        } else {
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].course == null || response.data[i].course.trim() == '')
                    response.data[i].course = '----';
                if (response.data[i].university == null || response.data[i].university.trim() == '')
                    response.data[i].university = '----';
                if (response.data[i].year_of_completion == null || response.data[i].year_of_completion.trim() == '' || response.data[i].year_of_completion.trim() == 0)
                    response.data[i].year_of_completion = '----';
                if (response.data[i].percentage == null || response.data[i].percentage.trim() == '')
                    response.data[i].percentage = '----';
                else
                    response.data[i].percentage = response.data[i].percentage + ' %';
                if (response.data[i].comments == null || response.data[i].comments.trim() == '')
                    response.data[i].comments = '----';


            }
            //+ '<a access-element="delete" class="lead-pinfo-delete-btn" onclick="DeleteEducatonalInfoById(this,' + response.data[0].lead_educational_info_id + ')"><i class="icon-trash f16"></i></a>';
            LatestEducationalInfo += '<div class="col-sm-4 mtb10">'
                + '<p class=" label-text">Qualification</p><label class="label-data">' + response.data[0].course + '</label></div>'
                + '<div class="col-sm-4 mtb10">'
                + '<p class=" label-text">University</p><label class="label-data">' + response.data[0].university + '</label>'
                + '</div>'
                + '<div class="col-sm-4 mtb10">'
                + '<p class=" label-text">Year of Completion</p><label class="label-data">' + response.data[0].year_of_completion + '</label></div>'
                + '<div class="col-sm-4 mtb10">'
                + '<p class=" label-text">Percentage</p><label class="label-data">' + response.data[0].percentage + ' </label></div>'
                + '<div class="col-sm-8 mtb10"><p class=" label-text">comments</p> <label class="label-data">' + response.data[0].comments + '</label>'
                + '</div>'
                + '<a access-element="edit" class="lead-pinfo-edit-btn" onclick="ManageEducationalInfo(this,' + response.data[0].lead_educational_info_id + ')"><i class="icon-edit f16"></i></a>'
            if (response.data.length > 0) {

                AllEducationalInfo += '<tbody>';
                for (var i = 0; i < response.data.length; i++) {
                    AllEducationalInfo += '<tr>'
                        + '<td>' + response.data[i].course + '</td>'
                        + '<td>' + response.data[i].university + '</td>'
                        + '<td>' + response.data[i].year_of_completion + '</td>'
                        + '<td>' + response.data[i].percentage + ' </td>'
                        + '<td>' + response.data[i].comments + '</td>'
                        + ' <td>'
                        + '<div class="dropdown feehead-list pull-right custom-dropdown-style">'
                        + '<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" href="#" data-target="#" id="dLabel"> <i class="fa fa-ellipsis-v f18 plr10"></i> </a>'
                        + '<ul aria-labelledby="dLabel" class="dropdown-menu pull-right">'
                        + '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>'
                        + '<li access-element="edit"><a onclick="ManageEducationalInfo(this,' + response.data[i].lead_educational_info_id + ')" href="#">Edit</a></li>'
                        + '<li access-element="delete"><a onclick="DeleteEducatonalInfoById(this,' + response.data[i].lead_educational_info_id + ')" href="#">Delete</a></li>'
                        + '</ul></div></td></tr>';
                }
                AllEducationalInfo += '</tbody>';
            }
            else{
                AllEducationalInfo += '<tbody><tr><td colspan="6" class="text-center">No Details Found</td></tr></tbody>';
            }



        }
        AllEducationalInfo += '</tbody></table>';
        $('#LatestEducationalInfo-list').html('');
        $('#AllEducationalInfo-list').html(AllEducationalInfo);
    }
    function getLeadInfoPostQualifications(parent_id,qual_id) {

        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Post Qualification'};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
                $('#txtLeadInfoPostQualification').empty();
                $('#txtLeadInfoPostQualification').append($('<option selected></option>').val('').html('--Select--'));
                if (response.status == true) {

                    if (response.data.length > 0) {

                        $.each(response.data, function (key, value) {

                            $('#txtLeadInfoPostQualification').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                        });
                    }
                }
                if(parent_id!='') {
                    $('#txtLeadInfoPostQualification').val(parent_id);
                }
                $('#txtLeadInfoPostQualification').material_select();
                getLeadInfoQualifications(parent_id,qual_id);
            }});
    }
    $('#txtLeadInfoPostQualification').change(function(){
        var parent_id='';
        if($('#txtLeadInfoPostQualification').length>0){
            parent_id=$('#txtLeadInfoPostQualification').val();
            if(parent_id==''){
                parent_id='N/A';
            }
        }
        getLeadInfoQualifications(parent_id,'');
    });
    function getLeadInfoQualifications(parent_id,qual_id) {

        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Qualification',parent_id:parent_id};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
                $('#txtLeadInfotQualification').empty();
                $('#txtLeadInfotQualification').append($('<option selected></option>').val('').html('--Select--'));
                if (response.status == true) {

                    if (response.data.length > 0) {

                        $.each(response.data, function (key, value) {

                            $('#txtLeadInfotQualification').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                        });
                    }
                }

                if(qual_id!=''){
                    $('#txtLeadInfotQualification').val(qual_id);
                }
                $('#txtLeadInfotQualification').material_select();

            }});
    }


    function getLeadInfoContactTypes() {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Contact Type'};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadInfoContactTypesResponse});
    }
    function getLeadInfoContactTypesResponse(response) {

        var rowHtml = '';
        if (response.status === true) {

            if (response.data.length > 0) {
                rowHtml += "<div class=\"multiple-radio\" id=\"contacatTypes\">";
                rowHtml += "<label>Lead Type</label>";
                $.each(response.data, function (key, value) {

                    rowHtml += "<span class=\"inline-radio\">";
                    rowHtml += "<input class=\"with-gap\" name=\"coldCallcontacttype\" type=\"radio\" id=\"contacttype" + value.reference_type_value_id + "\" value=\"" + value.value + "\" checked />";
                    rowHtml += "<label for=\"contacttype" + value.reference_type_value_id + "\">" + value.value + "</label>";
                    rowHtml += "</span>";

                });
                rowHtml += "</div>";
            }

        }
        $('#coldCallingContactTypeWrapper').html(rowHtml);
        $("input[name='coldCallcontacttype']").click(function () {
            $('#ManageLeadsForm input').removeClass("required");
            $('#ManageLeadsForm select').removeClass("required");
            $('#ManageLeadsForm textarea').removeClass("required");
            $('#ManageLeadsForm span.required-msg').remove();
            var contacttypeVal = $(this).val();
            $('#ComapnyWrapper').slideUp();
            $('#InstitutionWrapper').slideUp();
            if (contacttypeVal == 'Corporate') {
                $("#contactType-Institution-wrap").hide();
                $("#contactType-corporate-wrap").slideDown();
            }
            if (contacttypeVal == 'University') {
                $("#contactType-corporate-wrap").hide();
                $("#contactType-Institution-wrap").slideDown();
            }
            if (contacttypeVal == 'Retail') {
                $("#contactType-corporate-wrap").hide();
                $("#contactType-Institution-wrap").hide();
            }
        });
    }

    function getLeadInfoQualifications1() {
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Qualification'};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadInfoQualificationsResponse1});
    }
    function getLeadInfoQualificationsResponse1(response) {
        $('#txtColdCallContactQualification').empty();
        $('#txtColdCallContactQualification').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtColdCallContactQualification').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                });
            }
        }
        $('#txtColdCallContactQualification').material_select();
    }
    function getLeadInfoCompanies() {

        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Company'};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadInfoCompaniesResponse});

    }
    function getLeadInfoCompaniesResponse(response) {
        $('#txtColdCallContactCompany').empty();
        $('#txtColdCallContactCompany').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtColdCallContactCompany').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                });
            }
        }
        $('#txtColdCallContactCompany').material_select();
    }
    function getLeadInfoInstitutions() {

        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Referencevalues/getReferenceValuesByName';
        var params = {name: 'Institution'};
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getLeadInfoInstitutionsResponse});

    }
    function getLeadInfoInstitutionsResponse(response) {

        $('#ManageColdCallForm span.required-msg').remove();
        $('#ManageColdCallForm input').removeClass("required");
        $('#ManageColdCallForm select').removeClass("required");

        $('#txtColdCallContactInstitution').empty();
        $('#txtColdCallContactInstitution').append($('<option selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#txtColdCallContactInstitution').append($('<option></option>').val(value.reference_type_value_id).html(value.value));
                });
            }
        }
        $('#txtColdCallContactInstitution').material_select();
    }
    String.prototype.capitalizeFirstLetter = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
    function loadSR() {
        var courseId = $('#hdnCoursefId').val();
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/Survey/getSurveyListByCourse';
        var params = {'course_id': courseId};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function (response) {
            var htmlData = '';
            var allFields = '';
            if (response == -1 || response['status'] == false) {
                htmlData = '<p class="text-center">No Questions found</p>';
                $('#SR-list').html(htmlData);
            } else {
                htmlData = '';
                var rawHtml = response.data;
                var currentQuestionId = '';
                var counter = 1;
                for (var index in rawHtml) {
                    var identityName = 'question_' + rawHtml[index].question_id;
                    if (currentQuestionId != rawHtml[index].question_id)
                    {
                        htmlData += '<div class="col-sm-12 question-wrap-list"><p class="mb0"><label class="mr5 f14">Q' + counter + '.</label>';
                        counter += 1;
                        htmlData += '<span class="questionidentifier" data-id="' + rawHtml[index].question_id + '" >'+ rawHtml[index].title +'</span>';
                        htmlData += '</p>';
                        currentQuestionId = rawHtml[index].question_id;
                        allFields += identityName + ',';
                    }
                    var values = rawHtml[index].value === '' || rawHtml[index].value === null ? 0 : rawHtml[index].value;
                    htmlData += addSRField(rawHtml[index].option, identityName, values, rawHtml[index].question_option_id);
                    var currentIndex = index;
                    var nextIndex = parseInt(index) + 1;

                    if (rawHtml[index].option == 'input' && rawHtml[currentIndex].option == 'paragraph') {
                        htmlData += '</div>';
                    } else {
                        if (nextIndex != rawHtml.length && rawHtml[currentIndex].question_id != rawHtml[nextIndex].question_id) {
                            htmlData += '</div>';
                        }
                    }
                }
                htmlData += '<div class="col-sm-12 mtb10" ><a href="javascript:;" class="btn blue-btn" onclick="saveLeadSR(this)"><i class="icon-plus-circle mr8"></i>Save</a></div>';
                $('#SR-list').html(htmlData);

                var lead_id = $('#hdnBranchXrefId').val();
                ajaxurl = API_URL + 'index.php/Survey/getSurveyAnswersForLead';
                params = {'lead_id': lead_id};
                commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function (response) {
                    if (response == -1 || response['status'] == false)
                    {
                        //htmlData = '';
                    }
                    else
                    {
                        var answer = response.data;
                        $(".questionidentifier").each(function (questionIndex)
                        {
                            var identity = $(this).attr('data-id');
                            for (var index in answer)
                            {
                                if (answer[index].question_id == identity)
                                {
                                    var identifierName = 'question_' + identity;
                                    var dataType = answer[index].option_type;
                                    var value = answer[index].answer;
                                    if (dataType === 'input')
                                    {
                                        $("input[name='" + identifierName + "']").val(value);
                                    }
                                    else if (dataType == 'paragraph')
                                    {
                                        $("textarea[name='" + identifierName + "']").val(value);
                                    }
                                    else if (dataType == 'radio')
                                    {
                                        $("input[name='" + identifierName + "'][value='" + value + "']").prop('checked', true);
                                    }
                                    else if (dataType == 'checkbox')
                                    {
                                        if (/,/.test(value)) {
                                            var options = value.split(',');
                                            for (var a in options) {
                                                $("input[name='" + identifierName + "'][value='" + options[a] + "']").prop('checked', true);
                                            }
                                        } else {
                                            $("input[name='" + identifierName + "'][value='" + value + "']").prop('checked', true);
                                        }
                                    }
                                }
                            }
                        });
                    }
                }});
            }
        }});
    }
    function addSRField(optionType, identityName, labelText, optionId) {
        var htmlData = '';
        var selectedOption = optionType;
        if (selectedOption == 'input') {
            htmlData += '<div class="ml15">';
            htmlData += '<input name="' + identityName + '" data-type="' + optionType + '" class="form-control p5 mb0" type="text" aria-label="Text input with radio button" placeholder="Answer"><label></label>';
            htmlData += '</div>';
        } else if (selectedOption == 'paragraph') {
            htmlData += '<div class="ml15">';
//                htmlData += '<textarea name="'+identityName+'" data-type="'+optionType+'" id="textarea1" class="p0"></textarea>';
            htmlData += '<textarea name="'+identityName+'" data-type="'+optionType+'" class="form-control table-textarea" rows="2"></textarea>';
            htmlData += '</div>';
        } else if (selectedOption == 'radio') {
//            htmlData += '<div class="ml15">';
            //htmlData += '<span class="col-sm-3 ml15">';
            htmlData += '<span>';
            htmlData += '<span class="inline-radio">';
            htmlData += '<input data-type="' + optionType + '" id="' + identityName + '_' + optionId + '" class="with-gap" name="' + identityName + '" type="radio" value="' + optionId + '" />';
            htmlData += '<label for="' + identityName + '_' + optionId + '">' + labelText.capitalizeFirstLetter() + '</label>'
            htmlData += '</span>';
            htmlData += '</span>';
//            htmlData += '</div>';
        } else if (selectedOption == 'checkbox') {
//            htmlData += '<div class="ml15">';
            htmlData += '<span>';
            htmlData += '<span class="inline-radio">';
            htmlData += '<input id="' + identityName + '_' + optionId + '" class="' + identityName + ' filled-in with-gap" data-type="' + optionType + '" type="checkbox" name="' + identityName + '" value="' + optionId + '">';
            htmlData += '<label for="' + identityName + '_' + optionId + '">' + labelText.capitalizeFirstLetter() + '</label>';
            htmlData += '</span>';
            htmlData += '</span>';
//            htmlData += '</div>';
        }
        return htmlData;
    }
    function saveLeadSR() {
        var results = [];
        var courseId = $('#hdnCoursefId').val();
        var lead_id = $('#hdnBranchXrefId').val();
        $(".questionidentifier").each(function ()
        {
            var identity = $(this).attr('data-id');
            var identifierName = 'question_' + identity;
            var dataType = $("input[name='" + identifierName + "']").attr('data-type');
            var dataValue = $("input[name='" + identifierName + "']").val();
            if (typeof dataType == 'undefined')
            {
                dataType = $("textarea[name='" + identifierName + "']").attr('data-type');
                dataValue = $("textarea[name='" + identifierName + "']").val();
            }
            else if (dataType == 'radio')
            {
                dataValue = $("input[name='" + identifierName + "']:checked").val();
            }
            else if (dataType == 'checkbox')
            {
                var sThisVal = '';
                $('input:checkbox.' + identifierName).each(function () {
                    if (this.checked) {
                        sThisVal += $(this).val() + ',';
                    }
                });
                dataValue = sThisVal.replace(/[,]$/, '');
            }
            if (typeof dataValue == 'undefined')
                dataValue = '';
            results.push({'id': identity, 'value': dataValue, 'courseId': courseId, 'leadId': lead_id});
        });
        if (results.length > 0)
        {
            notify('Processing..', 'warning', 10);
            var userId = $('#hdnUserId').val();
            var action = 'add';
            var method = 'POST';
            var ajaxurl = API_URL + 'index.php/Survey/saveSurveyAnswersForLead';
            var params = results;
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, params: params, method: method, headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function (response) {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false) {
                    var id = '';
                    if (Object.size(response.data) > 0) {
                        notify('Validation Error', 'error', 10);
                        for (var a in response.data) {
                            id = '#' + a;
                            if (a == '') {
                                $(id).parent().find('.select-dropdown').addClass("required");
                                $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else {
                                $(id).addClass("required");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            }
                        }
                    } else {
                        notify(response.message, 'error', 10);
                    }
                } else {
                    notify('Saved successfully', 'success', 10);
                }
            }});
        } else {

        }
    }
    function loadFeeDetails(This){
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};

        var ajaxurl = API_URL + 'index.php/Students/getStudentFeeDetails';
        var params = {'lead-id': $glb_lead_id,'branch_xref_lead_id':$glb_branchxref_lead_id};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function (response) {
            //
            var htmlData = '';
            if (response == -1 || response['status'] == false || typeof response.data.OverallDetails=='undefined')
            {
                htmlData = '<p class="text-center">No Details found</p>';
                $('#fee_Deatails > .tab-data > .white-bg').html(htmlData);
            }
            else
            {
                var rawData = response.data.OverallDetails;
                var total = 0;
                var amountPaid = 0;
                var dueAmount = 0;
                var dueDays = 0;
                for(var a in rawData){
                    total += parseInt(rawData[a].amountPayable);
                    amountPaid += parseInt(rawData[a].amountPaid);
                    dueDays = rawData[a].dueDays;
                }
                dueAmount = total - amountPaid;
                dueDays = dueDays==0?' No Due':dueDays+' days';
                htmlData += '<div class="col-sm-12"><div class="col-sm-4 mtb10">';
                htmlData += '<p class=" label-text">Payable amount</p>';
                htmlData += '<label class="label-data"><span class="icon-rupee"></span>'+moneyFormat(total)+'</label>';
                htmlData += '</div>';
                htmlData += '<div class="col-sm-4 mtb10">';
                htmlData += '<p class=" label-text">Paid amount</p>';
                htmlData += '<label class="label-data"><span class="icon-rupee"></span>'+moneyFormat(amountPaid)+'</label>';
                htmlData += '</div>';
                htmlData += '<div class="col-sm-4 mtb10">';
                htmlData += '<p class=" label-text">Due Amount</p>';
                htmlData += '<label class="label-data"><span class="icon-rupee"></span>'+moneyFormat(dueAmount)+'</label>';
                htmlData += '</div></div>';
                var feedetailsData='';
                var rawDataReceipt=response.data.ReceiptDetails;
                feedetailsData+="<div class=\"col-sm-12\"><table id=\"feedetailsInfoTable\" class=\"table table-responsive table-striped table-custom\">";
                feedetailsData+='<thead><tr> <th>Receipt Number</th><th>Company</th><th>Receipt Date</th><th class="text-right">Amount</th><th>Payment Status</th><th>Remarks</th></tr><tr><td class="border-none p0 lh10">&nbsp;</td></tr></thead>';
                feedetailsData+='<tbody>';
                for(var a in rawDataReceipt){
                    feedetailsData+='<tr>';
                    feedetailsData+='<td>'+rawDataReceipt[a].receipt_number+'</td>';
                    feedetailsData+='<td>'+rawDataReceipt[a].name+'</td>';
                    feedetailsData+='<td>'+rawDataReceipt[a].receipt_date+'</td>';
                    feedetailsData+='<td class="text-right"><span class="icon-rupee"></span>'+moneyFormat(parseInt(rawDataReceipt[a].amount_paid))+'</td>';
                    feedetailsData+='<td>'+rawDataReceipt[a].payment_status+'</td>';//payment_remarks
                    feedetailsData+='<td>';
                    feedetailsData+='<a href="javascript:" class="popper" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Payment Remarks"><img src="<?php echo BASE_URL; ?>assets/images/comment.png"></a>';
                    feedetailsData+='<div class="popper-content hide">';
                    feedetailsData+='<div class="plr15 pb20 mb5">';
                    var remarks='';
                    if(rawDataReceipt[a].payment_remarks == null)
                        remarks='No Remarks Found';
                    else
                        remarks=rawDataReceipt[a].payment_remarks;
                    feedetailsData+='<p class="p0">'+remarks+'</p>';
                    feedetailsData+='</div>';
                    feedetailsData+='</div>';
                    feedetailsData+='</td>';
                    feedetailsData+='</tr>';
                }

                feedetailsData+='</tbody>';
                feedetailsData+='</table></div>';
                $('#fee_Deatails > .tab-data > .white-bg').html(htmlData+feedetailsData);
                buildpopover();
            }
        }});
    }

    function loadStationaryIssue(This)
    {

        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/BooksIssue/getStudentAdhocBookDetails';
        var params = {'courseId': $("#hdnCoursefId").val(),'studentId':$glb_branchxref_lead_id};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess:adhocStudentBookIssueResponse});
    }
    function adhocStudentBookIssueResponse(response)
    {
        if(response.status == false)
        {
            $("#adhoc_stationary > .tab-data > .white-bg").html('<p class="text-center">No Details found</p>');
            notify(response.message, 'error', 10);
        }
        else
        {
            var stationaryData=response.data;
            var leadstationaryDetails='';
            leadstationaryDetails+="<div class=\"col-sm-12\"><table id=\"feedetailsInfoTable\" class=\"table table-responsive table-striped table-custom\">";
            leadstationaryDetails+='<table class="table table-responsive table-striped table-custom">';
            leadstationaryDetails+='<thead><tr>';
            leadstationaryDetails+='<th>Item</th>';
            leadstationaryDetails+='<th>Quantity</th>';
            leadstationaryDetails+='<th>Issued</th>';
            leadstationaryDetails+='<th width="20%">Adhoc Quantity</th>';
            leadstationaryDetails+='</tr></thead>';
            leadstationaryDetails+='<tbody>';
            for(var st=0;st<stationaryData.length;st++)
            {
                leadstationaryDetails+='<tr>';
                leadstationaryDetails+='<td>'+stationaryData[st].product_name+'</td>';
                leadstationaryDetails+='<td>'+stationaryData[st].available_qty+'</td>';
                leadstationaryDetails+='<td>'+stationaryData[st].issued+'</td>';
                leadstationaryDetails+='<td>';
                leadstationaryDetails+='<input type="hidden" name="hdnProductId['+st+']" value="'+stationaryData[st].product_id+'">';
                leadstationaryDetails+='<input class="modal-table-textfiled m0 f12" name="adhocquantity[]" value="'+stationaryData[st].adhoc_issue+'" onkeypress="return isNumberKey(event)" maxlength="10">';
                leadstationaryDetails+='</td>';
                leadstationaryDetails+='</tr>';
            }
            leadstationaryDetails+='</tbody>';
            leadstationaryDetails+='</table>';
            leadstationaryDetails+='<div class="col-sm-12 white-bg p0 mb10">';
            leadstationaryDetails+='<button onclick="saveStudentAdhocStationaryIssue()" class="btn blue-btn" type="button"><i class="icon-right mr8"></i>Save</button>';
            leadstationaryDetails+='</div>';
            $("#adhoc_stationary > .tab-data > .white-bg").html(leadstationaryDetails);
        }
    }

    function saveStudentAdhocStationaryIssue()
    {
        alertify.dismissAll();
        var adhoc_qty= 0,index= 0,adhocProductQty = [];
        $('input[name^="adhocquantity"]').each(function()
        {
            adhoc_qty=$(this).val();
            if($(this).val() == '')
            {
                adhoc_qty=0;
            }
            adhocProductQty.push(adhoc_qty+'@@@@@@'+$('input[name="hdnProductId['+index+']"]').val());
            index++;
        });
        var userId = $('#hdnUserId').val();

        var action = 'adhocissue';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/BooksIssue/saveStudentAdhocBookDetails';
        var params = {userID: userId,courseId: $("#hdnCoursefId").val(),studentId:$glb_branchxref_lead_id,batchId:$glb_batch_id,adhocProductQty:adhocProductQty};
        commonAjaxCall({This: this, params: params,method: 'POST', headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: adhocstationaryIssueResponse});
    }
    function adhocstationaryIssueResponse(response)
    {
        alertify.dismissAll();
        if(response.status == false)
        {
            notify(response.message, 'error', 10);
        }
        else
        {
            notify('Saved Successfully', 'success', 10);
        }
    }
    function SaveStudentResume(This)
    {
        var $parent = $('#manageResumeUpload');
        var userId = $('#hdnUserId').val();
        var LeadId = $('#leadId').val();
        var branch_xref_lead_id = '<?php echo $Id; ?>';

        var comment = $("#resumeComment").val();

        $parent.find('span.required-msg').remove();
        $parent.find('input').removeClass("required");
        $parent.find('textarea').removeClass("required");
        var flag = 0;
        var max_fileUpload_size = 100.048;
        /*For max file size for uploaded file(In MB)*/
        var files = $("#resumeAttachemnt").get(0).files;
        var selectedFile = $("#resumeAttachemnt").val();
        var extArray = ['gif','jpg','png','jpeg','doc','docz'];
        var extension = selectedFile.split('.');
//        console.log(files);
        if (files.length > 0)
        {
            if(extension[extension.length - 1] == 'exe')
            {
                $parent.find(".file-path").addClass("required");
                $parent.find(".file-path").after('<span class="required-msg">Upload valid file</span>');
                flag = 1;
            }
        }
        if (files.length <= 0) {
            $parent.find(".file-path").focus();
            $parent.find(".file-path").addClass("required");
            $parent.find(".file-path").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        /*if (files.length > 0 && ((files[0].size / 1024) / 1024) > max_fileUpload_size) {
         $(".file-path").focus();
         $(".file-path").addClass("required");
         $(".file-path").after('<span class="required-msg">File can\'t be more than 2Mb</span>');
         flag = 1;
         }*/

        if (flag == 0) {
            var action = "add";
            var headerParams = {
                action: action,
                contentType: false,
                processData: false,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: userId
            };
            var ajaxurl = API_URL + 'index.php/LeadsInfo/saveStudentResume';
            var fileUpload = document.getElementById("attachemntUpload");

            var uploadFile = new FormData();
            uploadFile.append("file", files[0]);
            uploadFile.append("image", 'resume_' + new Date().getTime() +'.'+extension[1]);
            uploadFile.append('comment', comment);
            uploadFile.append('LeadId', LeadId);
            uploadFile.append('branch_xref_lead_id', branch_xref_lead_id);

            $.ajax({
                type: "POST",
                url: ajaxurl,
                dataType: "json",
                data: uploadFile,
                contentType: false,
                processData: false,
                headers: headerParams,
                beforeSend: function () {
                    $(This).attr("disabled", "disabled");
                    alertify.dismissAll();
                    notify('Processing..', 'warning', 10);
                }, success: function (response) {
                    if(response.status){
                        loadPlacementInfo();
                        alertify.dismissAll();
                        notify(response.message, 'success', 10);
                        $(This).removeAttr("disabled");
                        $parent[0].reset();
                    }else{
                        alertify.dismissAll();
                        notify('Something went wrong. Please try again', 'error', 10);
                    }
                }, error: function (response) {
                    alertify.dismissAll();
                    notify('Something went wrong. Please try again', 'error', 10);
                }
            });

        }
    }
    function clearPlacementInfo(){
        var $parent = $('#manageResumeUpload');

        $parent.find('span.required-msg').remove();
        $parent.find('input').removeClass("required");
        $parent.find('textarea').removeClass("required");

        $parent[0].reset();
    }
    function loadPlacementInfo(){
        clearPlacementInfo();
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/LeadsInfo/getleadId';
        var params = {'branch_xref_lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
            var resume = '<p class="dark-sky-blue f12">';
            if (response.data[0].placement_file != null && response.data[0].placement_file != '') {
                resume += '<a download target="_blank" href="' + BASE_URL + 'uploads/' + response.data[0].placement_file + '"> Download resume   <i class="icon-arrow-down-circle f16 pull-left pr5"></i></a>';
            } else {
                resume += '<span class="pull-left">No resume uploaded</span>';
            }
            if (response.data[0].placement_comments != null && response.data[0].placement_comments != '') {
                resume += '<span class="pull-right">'+response.data[0].placement_comments+'</span>';
            } else {
                resume += '<span class="pull-right">No comments found</span>';
            }
            resume += '</p>';
            $('#resumeDownload').html(resume);
        }});
    }
    function clearEligibility(){
        var $parent = $('#eligibilityForm');

        $parent.find('span.required-msg').remove();
        $parent.find('input, select').removeClass("required");
        $parent.find('textarea').removeClass("required");

        $parent[0].reset();

        $parent.find('#eligibility').val('');
        $parent.find('#eligibility').material_select();
    }
    function loadEligibilityDetails(){
        clearEligibility();
        getEligibilityFileList($glb_branchxref_lead_id);
    }
    function SaveEligibility(This)
    {
        var $parent = $('#eligibilityForm');
        var userId = $('#hdnUserId').val();
        var LeadId = $('#leadId').val();
        var branch_xref_lead_id = '<?php echo $Id; ?>';

        var eligibility = $("#eligibility").val();

        $parent.find('span.required-msg').remove();
        $parent.find('input, select').removeClass("required");
        $parent.find('textarea').removeClass("required");
        var flag = 0;
        var max_fileUpload_size = 100.048;
        /*For max file size for uploaded file(In MB)*/
        var files = $("#eligibilityAttachemnt").get(0).files;
        var selectedFile = $("#eligibilityAttachemnt").val();
        //console.log(selectedFile);
        var extArray = ['gif','jpg','png','jpeg','doc','docz'];
        var extension = selectedFile.split('.');
        //console.log(files);
        if (files.length > 0)
        {
            if(extension[extension.length - 1] == 'exe')
            {
                $parent.find(".file-path").addClass("required");
                $parent.find(".file-path").after('<span class="required-msg">Upload valid file</span>');
                flag = 1;
            }
        }
        if(eligibility==""){
            $parent.find("#eligibility").parent().find('.select-dropdown').addClass("required");
            $parent.find("#eligibility").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if (flag == 0) {
            var action = "add";
            var headerParams = {
                action: action,
                contentType: false,
                processData: false,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: userId
            };
            var ajaxurl = API_URL + 'index.php/LeadsInfo/saveEligibility';
            var fileUpload = document.getElementById("attachemntUpload");

            var uploadFile = new FormData();
            if (files.length > 0) {
                uploadFile.append("file", files[0]);
                uploadFile.append("orginalFile", selectedFile);
                uploadFile.append("image", 'resume_' + new Date().getTime() +'.'+extension[1]);
            }
            uploadFile.append('eligibility', eligibility);
            uploadFile.append('LeadId', LeadId);
            uploadFile.append('branch_xref_lead_id', branch_xref_lead_id);
            uploadFile.append('userID', '<?php echo $userData['userId']; ?>');

            $.ajax({
                type: "POST",
                url: ajaxurl,
                dataType: "json",
                data: uploadFile,
                contentType: false,
                processData: false,
                headers: headerParams,
                beforeSend: function () {
                    $(This).attr("disabled", "disabled");
                    alertify.dismissAll();
                    notify('Processing..', 'warning', 10);
                }, success: function (response) {
                    if(response.status){
                        loadEligibilityDetails();
                        alertify.dismissAll();
                        notify(response.message, 'success', 10);
                        $(This).removeAttr("disabled");
                    }else{
                        alertify.dismissAll();
                        notify('Something went wrong. Please try again', 'error', 10);
                    }
                }, error: function (response) {
                    alertify.dismissAll();
                    notify('Something went wrong. Please try again', 'error', 10);
                }
            });

        }
    }
    function getEligibilityFileList(id){
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/LeadsInfo/getEligibilityFileList';
        var params = {'branch_xref_lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
            //console.log(response);
            var html = '';
            if(response.status){
                if(response.data.length>0 && response.data[0].eligibility!=''){
                    $('#eligibility').val(response.data[0].eligibility);
                    $('#eligibility').material_select();
                }
                $('#eligibility_list tbody').html('');
                if(response.data.data.length>0){
                    for(var a in response.data.data){
                        html += '<tr>';
                        html += '<td>';
                        html += '<span>'+response.data.data[a].orginal_filename+'</span>';
                        html += '</td>';
                        html += '<td>' +
                            '<a download target="_blank" href="' + BASE_URL + 'uploads/' + response.data.data[a].filepath + '"><i class="icon-arrow-down-circle f16"></i></a>' +
                            "<a href='javascript:;' class='p0 pl10' onclick='deleteEligibilityFile(&quot;"+response.data.data[a].branch_xref_lead_uploads_id+"&quot;)'><i class='fa fa-times-circle red mr8 ml5 f18'></i></a>" +
                            '</td>';
                        html += '</td>';
                        html += '</tr>';
                    }
                }else{
                    html += '<tr>';
                    html += '<td style="padding:4px 8px;" colspan=2><p class="text-center">No documents found</p>';
                    html += '</td>';
                    html += '</tr>';
                }

            }
            $('#eligibility_list tbody').html(html);
        }});
    }

    function deleteEligibilityFile(id) {
        var conf = "Are you sure want to delete?";
        customConfirmAlert('Delete file', conf);
        $("#popup_confirm #btnTrue").attr("onclick", "deleteEligibilityFileConfirm(this,'" + id + "')");
    }
    function deleteEligibilityFileConfirm(This, id){
        var userId = $('#hdnUserId').val();
        var action = 'delete';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/LeadsInfo/deleteEligibilityFile';
        var params = {'branch_xref_lead_uploads_id': id};
        commonAjaxCall({This: This, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
            //console.log(response);
            alertify.dismissAll();
            if (response == -1 || response['status'] == false) {
                notify(response.message, 'error', 10);
            } else {
                notify(response.message, 'success', 10);
                $('#popup_confirm').modal('hide');
                getEligibilityFileList(id);
            }
        }});
    }
    function resetStudentReference(){
        var $parent = $('#student_reference');

        $parent.find('label').removeClass('active');
        $parent.find('span.required-msg').remove();
        $parent.find('input, select').removeClass("required");
        $parent.find('textarea').removeClass("required");

        $parent.find('#reference_list tbody').html('');
        $parent.find('#txtLeadInfo').val('');
        $parent.find('#txtLeadInfoDetails').val('');
        $parent.find('#lead_ref_comment').val('');
    }
    function loadStudentReference()
    {
        resetStudentReference();
        var userId = $('#hdnUserId').val();
        var action = 'list';
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        var ajaxurl = API_URL + 'index.php/LeadsInfo/getStudentReference';
        var params = {'branch_xref_lead_id': '<?php echo $Id; ?>'};
        commonAjaxCall({This: this, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
            //console.log(response);
            var html = '';
            if(response.status){
                if(response.data.ref_comment!=''){
                    $('#lead_ref_comment').val(response.data.ref_comment);
                    $('#lead_ref_comment').next('label').addClass('active');
                }

                var _html = $('#reference_list tbody').html();
                if(response.data.data.length > 0){
                    for(var a in response.data.data){
                        var listData =  response.data.data[a];
                        _html += '<tr class="ref-tab-val" data-id="'+listData.lead_id+'" data-val="'+listData.lead_id+','+listData.name+'">';
                        _html += '<td>';
                        _html += listData.lead_id;
                        _html += '</td>';
                        _html += '<td>';
                        _html += listData.name;
                        _html += '</td>';
                        _html += "<td><a href='javascript:;' class='p0 pl10 tooltipped' onclick='deletedselected(this)' data-position='top' data-tooltip='Delete'><i class='fa fa-times-circle red mr8 ml5 f18' ></i></a></td>";
                        _html += '</tr>';
                    }
                }else{
                    html += '<tr>';
                    html += '<td style="padding:4px 8px;" colspan="3"><p class="text-center">No referrals found</p>';
                    html += '</td>';
                    html += '</tr>';
                }

                $('#reference_list tbody').html(_html);
                $('#reference_list').show();
            }
        }});
    }
    function SaveStudentRefrence()
    {
        var $parent = $('#manageStudentreference');
        var userId = $('#hdnUserId').val();
        var LeadId = $('#leadId').val();
        var branch_xref_lead_id = '<?php echo $Id; ?>';
        var comment = $('#lead_ref_comment').val();
        var flag = 0;

        var $table = $('#reference_list');
        var leadarray = $table.find('.ref-tab-val').map(function() {
            return $(this).attr('data-id');
        }).get();

        /*if(leadarray.length<=0 && (comment=='' || trim(comment).lrngth<=0)){
         flag = 1;
         notify('Nothing to save', 'error', 10);
         return;
         }*/

        if(leadarray.length<=0){
            flag = 1;
            notify('Atleast one lead should be selected', 'error', 10);
            return;
        }else{
            leadarray = leadarray.join('-');
        }

        if(comment==''){
            $("#lead_ref_comment").addClass("required");
            $("#lead_ref_comment").after('<span class="required-msg">'+$inputRequiredMessage+'</span>');
        }

        if(flag==0){
            var userId = $('#hdnUserId').val();

            var action = 'add';
            var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            var ajaxurl = API_URL + 'index.php/LeadsInfo/saveStudentRefrence';
            var params = {userID: userId, leads: leadarray, comment: comment, LeadId:$glb_branchxref_lead_id};
            commonAjaxCall({This: this, params: params,method: 'POST', headerParams: headerParams, requestUrl: ajaxurl, action: action, onSuccess: function(response){
                //console.log(response);
                if (response == -1 || response['status'] == false){
                    alertify.dismissAll();
                    notify('Validation Error', 'error', 10);
                }
                else
                {
                    alertify.dismissAll();
                    notify('Data Saved Successfully', 'success', 10);
                    loadStudentReference();
                }
            }});
        }
    }

    function addStudentRefrence(This)
    {
        var $parent = $('#student_reference');
        var flag = 0;
        var userId = $('#hdnUserId').val();
        var LeadId = $('#leadId').val();
        var branch_xref_lead_id = '<?php echo $Id; ?>';
        var selectedLead = $('#txtLeadInfo').val();
        var selectedLeadDetails = $('#txtLeadInfoDetails').val();

        selectedLeadDetails = selectedLeadDetails.split('|');

        $parent.find('span.required-msg').remove();
        $parent.find('input, select').removeClass("required");
        $parent.find('textarea').removeClass("required");

        if(selectedLead==""){
            $("#txtLeadInfo").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        var $table = $('#reference_list');
        if($table.find('.ref-tab-val').length>0){
            var ref_id = $table.find('.ref-tab-val').map(function() {
                return $(this).attr('data-id');
            }).get();
            if($.inArray(selectedLead , ref_id ) >= 0){
                flag = 1;
                notify('Already added', 'error', 10);
                return;
            }
        }

        if(flag == 0){
            //build table
            if($table.find('.ref-tab-val').length>0) {
                var _html = $('#reference_list tbody').html();
            } else {
                var _html ='';
            }
            if(_html == 'undefined')
                _html = '';
            _html += '<tr class="ref-tab-val" data-id="'+selectedLead+'" data-val="'+selectedLeadDetails+'">';
            _html += '<td>';
            _html += selectedLeadDetails[0];
            _html += '</td>';
            _html += '<td>';
            _html += selectedLeadDetails[1];
            _html += '</td>';
            _html += "<td><a href='javascript:;' class='p0 pl10 tooltipped' onclick='deletedselected(this)' data-position='top' data-tooltip='Delete'><i class='fa fa-times-circle red mr8 ml5 f18' ></i></a></td>";
            _html += '</tr>';
            $('#reference_list tbody').html(_html);
            $('#reference_list').show();
            $('#txtLeadInfo').val('');
            $('#txtLeadInfoDetails').val('');
        }
    }

    function deletedselected(This){
        var deletedId = $(This).parent().parent().attr('data-id');;
        /*var deletedVal = $(This).parent().parent().attr('data-val');*/

        $('.ref-tab-val').each(function(){
            var current = $(this).attr('data-id');
            if(current == deletedId){
                $(this).remove();
            }
        });
        if($('.ref-tab-val').length<=0){
            var html = '<tr>';
            html += '<td style="padding:4px 8px;" colspan="3"><p class="text-center">No referrals found</p>';
            html += '</td>';
            html += '</tr>';

            $('#reference_list tbody').html(html);
            $('#reference_list').show();
        }

    }

    $(function(){
        var headerParams = {
            action: 'list',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $('input#txtLeadInfo').jsonSuggest({
            onSelect:function(item){
                //console.log('search - '+item.id);
                $('input#txtLeadInfo').val(item.id);
                $('input#txtLeadInfoDetails').val(item.text);
//                addStudentRefrence(item);
            },
            headers:headerParams,url:API_URL + 'index.php/FeeAssign/getLeadsInformation' , minCharacters: 2});
    });
</script>