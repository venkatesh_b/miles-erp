<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
<input type="hidden" id="hdnUserId" name="hdnUserId" value="<?php echo $userData['userId']; ?>"/>
<input type="hidden" id="subjectId" name="subjectId" value="0"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Subject </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" onclick="getSubjectDetails(this, 0)" data-position="top" data-tooltip="Add Subject" class="viewsidePanel"><i class="icon-plus-circle" ></i></a></li>
                <!--<li><a></a></li>-->
            </ul>
        </div>   
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" > <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0" access-element="list">
                <table class="table table-responsive table-striped table-custom table-info" id="subject-list">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Course</th>
                            <th>Code</th>
                            <th class="border-r-none">Status</th>
                            <th class="no-sort" width="10%"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="subjectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeSubjectModal()"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="subjectModalLabel">Create Subject</h4>
                    </div>
                    <form action="/" method="post" id="subjectForm" novalidate="novalidate">
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="subjectName" type="text" name="subjectName" class="validate" class="formSubmit" required>
                                    <label for="subjectName">Subject Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="subjectCode" type="text" name="subjectCode" class="validate" class="formSubmit" required>
                                    <label for="subjectCode">Subject Code <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="course" name="course" class="validate" class="formSubmit">
                                        <option value="" selected>---Select--</option>
                                    </select>
                                    <label class="select-label">Course <em>*</em></label>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="actionButton" type="submit" class="btn blue-btn" onclick="AddSubject(this)"><i class="icon-right mr8"></i>Add</button>
                        <button type="button" class="btn blue-light-btn" onclick="closeSubjectModal()"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildSubjectDataTable();
    });

    function buildSubjectDataTable() {
        var userId = $('#hdnUserId').val();
        var ajaxurl=API_URL+'index.php/Subject/getSubjectDatatables';
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        $("#subject-list").dataTable().fnDestroy();
        $tableobj=$('#subject-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false
            }],
            columns: [
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.name;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.course;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.subject_code;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        var status='';
                        if(data.is_active == 1)
                        {
                            status="Active";
                        }
                        else
                        {
                            status="Inactive";
                        }
                        return status;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        var rawHtml = '';
                        var status = data.is_active == 1 ? 'Active' : 'Inactive';
                        var statusText = data.is_active == 1 ? 'Inactive' : 'Active';
                        rawHtml += '';
//                        rawHtml += status;
                        rawHtml += '<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        rawHtml += '<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                        rawHtml += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        rawHtml += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        rawHtml += '<li><a access-element="edit" href="javascript:;" onclick="getSubjectDetails(this,\'' + data.subject_id + '\')">Edit</a></li>';
                        rawHtml += '<li><a access-element="delete" href="javascript:;" onclick="deleteSubject(this,\'' + data.subject_id + '\', \'' + data.is_active + '\')">' + statusText + '</a></li>';
                        rawHtml += '</ul></div>';
                        return rawHtml;
                    }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    
    function getSubjectDetails(This, id){
        notify('Processing..', 'warning', 10);
        $('#subjectForm .select-label').removeClass("active");
        $('#subjectForm input').removeClass("required");
        var data = [];
        var method = 'GET';
        var action = 'edit';
        var userID = $('#hdnUserId').val();
        $('#venueModalLabel').html('Create Subject');
        var ajaxurl = API_URL + 'index.php/Courses/getAllCourseList';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({This: this, requestUrl: ajaxurl, headerParams: headerParams, action: action, onSuccess: function(response){
            selectOption = 1;
            branchCourse(response);
            selectOption = 0;
            
            if(id == 0){
                //add
                $('#subjectId').val('0');
                $('#actionButton').html('<i class="icon-right mr8"></i>Add');
                alertify.dismissAll();
                $('#subjectModal').modal('show');
            } else{
                $('#subjectId').val(id);
                //edit
                $('#subjectModalLabel').html('Edit Subject');
                $('#actionButton').html('<i class="icon-right mr8"></i>Update');
                var params = {subject_id: id};
                ajaxurl = API_URL + "index.php/Subject/getSubjectById";
                var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: method, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
                    if (response == -1 || response['status'] == false) {
                        alertify.dismissAll();
                        notify(response['message'], 'error', 10);
                    } else {
                        var rawData = response.data[0];
                        $('#subjectName').val(rawData.name);
                        $('#subjectName').next('label').addClass("active");
                        $('#subjectCode').val(rawData.subject_code);
                        $('#subjectCode').next('label').addClass("active");
                        $('#course').val(rawData.fk_course_id);
                        $('#course').material_select();
                        alertify.dismissAll();
                        $('#subjectModal').modal('show');
                    }
                }});

            }
        }});
    }
    
    function closeSubjectModal(){
        $('#subjectForm')[0].reset();

	$('#course').find('option:gt(0)').remove();
        $('#subjectId').val(0);
        $('#subjectForm label').removeClass("active");
        $('#subjectForm span.required-msg').remove();
        $('#subjectForm input').removeClass("required");

        $("select[name=course]").val(0);
        $('select[name=course]').material_select();

        $('#subjectModal').modal('toggle');

        $("#subjectModalLabel").html("Create Subject");
    }
    
    function AddSubject(This){
        notify('Processing..', 'warning', 10);
        $('#subjectForm span.required-msg').remove();
        $('#subjectForm input').removeClass("required");
        var data ,url , action = '';
        
        var id = $('#subjectId').val();
        
        var subjectName = $('#subjectName').val();
        var subjectCode = $('#subjectCode').val();
        
        var course = $("select[name=course]").val();
        
        var flag = 0;

        if (course == '' || course == null) {
            $("#course").parent().find('.select-dropdown').addClass("required");
            $("#course").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if (subjectName == '') {
            $("#subjectName").addClass("required");
            $("#subjectName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (subjectCode == '') {
            $("#subjectCode").addClass("required");
            $("#subjectCode").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (flag == 0) {
            data = {
                subjectName: subjectName,
                subjectCode: subjectCode,
                course: course
            };
            if(id == 0){
                //add
                url = API_URL + "index.php/Subject/saveSubject";
                action = 'add';
            }else{
                //edit
                url = API_URL + "index.php/Subject/editSubject/subject_id/"+id;
                action = 'edit';
            }
            var userId = $('#hdnUserId').val();
            var ajaxurl = url;
            var params = data;
            var method = 'POST';
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, method: method, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response) {
                if (response == -1 || response['status'] == false) {
                    var id = '';
                    alertify.dismissAll();
                    if (Object.size(response.data) > 0) {
                        for (var a in response.data) {
                            id = '#' + a;
                            if (a == 'course') {
                                $(id).parent().find('.select-dropdown').addClass("required");
                                $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else if (a == 'id'){
                                notify(response.data[a], 'error', 10);
                            } else{
                                $(id).addClass("required");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            }
                        }
                    }else {
                        notify(response.message, 'error', 10);
                    }
                } else {
                    alertify.dismissAll();
                    notify('Data Saved Successfully', 'success', 10);
                    buildSubjectDataTable();
                    closeSubjectModal();
                }
            }});
        }
    }
    
    function deleteSubject(This, subjectId, currentStatus) {
        var status = currentStatus == 1 ? 'inactivate' : 'activate';
        var conf = "Are you sure want to " + status + " this subject ?";
        customConfirmAlert('Subject Status', conf);
        $("#popup_confirm #btnTrue").attr("onclick", "deleteSubjectConfirm(this,'" + subjectId + "')");
    }
    function deleteSubjectConfirm(This, subjectId) {
        notify('Processing..', 'warning', 10);
        var userId = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Subject/deleteSubject/subject_id/' + subjectId;
        var method = "POST";
        var action = 'delete';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: method, requestUrl: ajaxurl, headerParams: headerParams, action: 'delete', onSuccess: deleteSubjectResponse});
    }
    function deleteSubjectResponse(response) {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false) {
            notify(response.message, 'error', 10);
        } else {
            notify(response.message, 'success', 10);
            buildSubjectDataTable();
        }
    }
</script>