<?php
/**
 * Created by PhpStorm.
 * User: VENKATESH.B
 * Date: 4/4/16
 * Time: 3:14 PM
 */
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo BASE_URL;?>assets/images/favicon.ico" type="image/x-icon">
    <title>::MILES::</title>
    <script src="<?php echo BASE_URL;?>assets/js/jquery.min.js"></script>
    <script src="<?php echo BASE_URL;?>assets/js/jquery-ui-1.11.0.js"></script>
    <script>
        var BASE_URL='<?php echo BASE_URL; ?>';
        var API_URL='<?php echo API_URL; ?>';
        $context = 'Email Template';
        $pageUrl = 'emailtemplate';
        $serviceurl = 'emailtemplate';
        $accessToken ='<?php echo $accessToken; ?>';

    </script>
    <script src="<?php echo BASE_URL;?>assets/js/commonajax.js"></script>
    
</head>
<body>
<input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
<input type="hidden" id="hdnTemplateId" value="<?php echo $Id; ?>" readonly />

<div id="templatePreview" access-element="preview">

</div>


<script>
    $(document).ready(function(){
        getFeeCollectionDetails();
    });
    function getFeeCollectionDetails()
    {
        var userId = $('#hdnUserId').val();
        var templateId = $('#hdnTemplateId').val();
        var action = 'preview';
        var ajaxurl = API_URL + 'index.php/EmailTemplates/getEmailTemplatePreviewById';
        var params = {templateID: templateId};
        var headerParams = {action: action, context: $context, serviceurl: $serviceurl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        $.ajax({
            type: "GET",
            url: ajaxurl,
            dataType: "json",
            data: params,
            headers: headerParams,
            success: function (response){
                if(response['status']==true)
                {
                    $('#templatePreview').html(response.data.content);
                }
            }
        });
        
    }
</script>
</body>
</html>