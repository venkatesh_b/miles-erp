<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
<div class="contentpanel" id="usermanagementClass"><!-- InstanceBeginEditable name="PageTitle" -->
    
<div class="content-header-wrap">
    <h3 class="content-header">User Management</h3>
    <div class="content-header-btnwrap">
        <ul>
            <li access-element="add"><a class="tooltipped" data-position="left" data-tooltip="Add User" class="viewsidePanel" onclick="ManageUser(this,0)"><i class="icon-plus-circle" ></i></a></li>
        </ul>
    </div>
</div>
<!-- InstanceEndEditable -->
<div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
<div class="fixed-wrap clearfix">
    <div class="col-sm-12 p0" access-element="List">
        <table class="table table-responsive table-striped table-custom" id="users-list">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>E-mail</th>
                    <th>Code</th>
                    <th>Role</th>
                    <th>Branches</th>
                    <th>Joining Date</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<!--Modals-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
    <div class="modal-dialog" role="document">
        <form id="ManageUserCreation" method="post">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Create User</h4>
            </div>
            <div class="modal-body modal-scroll clearfix">
                 <input type="hidden" id="hdnUserId" value="0" />
                <div class="col-sm-12">
                    <div class="input-field">
                        <input type="text" class="validate" id="txtUserName"  >
                        <label for="txtUserName">Name<em>*</em></label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                        <input  type="text" class="validate" id="txtUserPhoneNumber" onkeypress="return isPhoneNumber(event)" maxlength="15">
                        <label for="txtUserPhoneNumber"> Phone<em>*</em></label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                       <input  type="email"  id="txtUserEmail">
                        <label for="txtUserEmail"  >Email<em>*</em></label>
                    </div>
                </div>
                 <div class="col-sm-12">
                    <div class="input-field">
                       <input  type="text" class="validate" id="txtCode">
                        <label for="txtCode" data-error="wrong" data-success="right">Code<em>*</em></label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                        <label class="datepicker-label">Date of Birth</label>
                        <input type="date" class="datepicker relative enableDobDates" placeholder="dd/mm/yyyy" id="txtUserDob">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="multiple-radio">
                        <label>Gender<em>*</em></label>
                        <span class="inline-radio">
                            <input class="with-gap" name="txtUserGender" type="radio" id="male" value="1" checked />
                            <label for="male">Male</label>
                        </span>
                        <span class="inline-radio">
                            <input class="with-gap" name="txtUserGender" type="radio" value="0" id="female"  />
                            <label for="female">Female</label>
                        </span>  
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="multiple-checkbox">
                        <span class="inline-checkbox">
                            <input type="checkbox" class="filled-in" id="sCounselor" />
                            <label for="sCounselor">Is Secondary Counselor?</label>
                        </span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                        <input type="date" class="datepicker relative enableDates" placeholder="dd/mm/yyyy" id="txtUserJoin">
                        <label class="datepicker-label">Joining Date <em>*</em></label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                        <select id="txtUserRole">
                            <option value="" disabled selected>--Select--</option>
                        </select>
                        <label class="select-label">Roles<em>*</em></label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-field">
                        <select multiple id="txtUserBranch" name="txtUserBranch" class="multiple">
                            <option value="">--Select All--</option>
                        </select>
                        <label class="select-label">Branches<em>*</em></label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="actionButton" type="button" onclick="saveUser(this)" class="btn blue-btn" id="btn_submit" ><i class="icon-right mr8"></i>Add</button>
                <button type="button" class="btn blue-light-btn" data-dismiss="modal" id="btn_cancel" ><i class="icon-times mr8"></i>Cancel</button>
            </div>
           
        </div>
            
            </form>
    </div>
</div>
<!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildUsersDataTable();
    });

    function buildUsersDataTable()
    {
        var ajaxurl=API_URL+'index.php/user/getUsersList';
        var userdata='';
        var userID=<?php echo $userData['userId']; ?>;
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#users-list").dataTable().fnDestroy();
        $tableobj=$('#users-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    //return data.value;
                    userdata='<div class="image-td">';
                    if(data.image && CheckFileExists(data.image))
                    {
                        userdata += '<img src="'+data.image+'">';
                    }
                    else
                    {
                        userdata += '<img src="<?php echo BASE_URL;?>assets/images/user-icon.png">';
                    }
                    userdata += '</div>';
                    userdata+='<div class="user-name">'+data.name+'</div>';
                    return userdata;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return (data.phone == null || data.phone == '') ? "----":data.phone;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.email;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.employee_code;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.role;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    userdata='';
                    if(data.branchesCount == 0)
                    {
                        userdata+='<span>';
                        userdata+='<a href="javascript:;" class="anchor-blue p10">'+data.branchesCount+'</a>';
                        userdata+='</span>';
                    }
                    else
                    {
                        userdata+='<span class="popper p10" data-toggle="popover" data-placement="top" data-trigger="click" data-title="Branches">';
                        userdata+='<a href="javascript:;" class="anchor-blue p10">'+data.branchesCount+'</a>';
                        userdata+='</span>';
                        userdata+='<div class="popper-content hide">';
                        //var branchesList=data.branches.split(',');
                        userdata+='<p>'+data.branches+'</p>';
                        /*for(var u=0;u<branchesList.length;u++)
                        {
                            userdata+='<p>'+branchesList[u]+'</p>';
                        }*/
                        userdata+='</div>';
                    }
                    return userdata;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    userdata='';
                    var labelUserStatus='';
                    if(data.is_active==1){
                        labelUserStatus='Inactive User';
                    }
                    else if(data.is_active==0){
                        labelUserStatus='Active User';
                    }
                    userdata+='<span class="p0">'+data.joining_date+'</span><div class="dropdown feehead-list pull-right custom-dropdown-style"> <a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v f18 plr10"></i> </a>'
                            +'<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">'
                                +'<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>'
                                +'<li><a access-element="edit" href="javascript:;" onclick="ManageUser(this,'+data.user_id+')"  >Edit</a></li>'
                                +'<li><a access-element="status" href="javascript:;" onclick="ChangeUserStatus(this,'+data.user_id+','+data.is_active+')"  >'+labelUserStatus+'</a></li>'
                            +'</ul>'
                        +'</div>';
                    return userdata;
                }
                }
            ],
            "createdRow": function ( row, data, index )
            {

                if(data.is_active == 0)
                {
                    $(row).addClass('disabled-branchview');
                }
            }
        } );
        DTSearchOnKeyPressEnter();
    }
    function ChangeUserStatus(This,userId,userStatus){
        var labelUserStatus='';
        var toStatus='';
        if(userStatus==1){
            labelUserStatus='inactivate';
            toStatus=0;
        }
        else if(userStatus==0){
            labelUserStatus='activate';
            toStatus=1
        }
        //var conf=confirm("Are you sure want to "+labelUserStatus+" this user ?");
        var ModelTitle="User Status";
        var Description="Are you sure want to "+labelUserStatus+" this user ?";
        $("#popup_confirm #btnTrue").attr('onClick','ChangeUserStatusFinal(this,"'+userId+'","'+userStatus+'")');
        customConfirmAlert(ModelTitle,Description);


    }
    function ChangeUserStatusFinal(This,userId,userStatus){
        var labelUserStatus='';
        var toStatus='';
        if(userStatus==1){
            labelUserStatus='Inactive';
            toStatus=0;
        }
        else if(userStatus==0){
            labelUserStatus='Active';
            toStatus=1
        }
            alertify.dismissAll();
            notify('Processing..', 'warning', 10);
            var userID =<?php echo $userData['userId']; ?>;
            var selUserId=userId;
            var ajaxurl = API_URL + 'index.php/User/updateUserStatus';
            var params = {userID: selUserId,toStatus:toStatus};
            var action = 'status';
            var type="POST";
            var headerParams = {
                action: action,
                context: $context,
                serviceurl: $pageUrl,
                pageurl: $pageUrl,
                Authorizationtoken: $accessToken,
                user: userID
            };
            commonAjaxCall({
                This: This,
                method:'POST',
                requestUrl: ajaxurl,
                params: params,
                headerParams: headerParams,
                action: action,
                onSuccess: ChangeUserStatusResponse
            });
    }
    function ChangeUserStatusResponse(response){
        alertify.dismissAll();
        if(response.status===true){
            notify(response.message,'success',10);
            $('#popup_confirm').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildUsersDataTable();
        }
        else{
            notify(response.message,'error',10);
        }

    }
</script>
