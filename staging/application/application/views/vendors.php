<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-16
 * Time: 10:39 AM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-09
 * Time: 02:45 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Vendors</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="viewsidePanel tooltipped" data-position="top" data-target="#myVendor" data-toggle="modal" data-tooltip="Add Vendor" onclick="manageVendors(this,0)" ><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div access-element="list" class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="vendors-list">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Contact by Name</th>
                        <th>Contact by Phone</th>
                        <th class="no-sort"></th>
                    </tr>

                    </thead>

                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="myVendor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageVendorForm" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Vendor</h4>
                        </div>
                        <input type="hidden" id="hdnVendorId" value="0" />
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="vendor_name" name="vendor_name" type="text" class="validate" maxlength="200">
                                    <label for="vendor_name">Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="phone_number" name="phone_number" type="text" class="validate" maxlength="15" onkeypress="return isPhoneNumber(event)">
                                    <label for="phone_number">Phone <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="vendor_email" name="vendor_email" type="text" class="validate" maxlength="200">
                                    <label for="vendor_email">Email <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="vendor_address" name="vendor_address" class="materialize-textarea" maxlength="1000"></textarea>
                                    <label for="vendor_address">Address </label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="contact_by_name" name="contact_by_name" type="text" class="validate" maxlength="100">
                                    <label for="contact_by_name">Contact by Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="contact_by_phone" name="contact_by_phone" type="text" class="validate" maxlength="15" onkeypress="return isPhoneNumber(event)">
                                    <label for="contact_by_phone">Contact by Phone <em>*</em></label>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" id="vendor_button" class="btn blue-btn" onclick="saveVendor(this)"><i class="icon-right mr8"></i>Create Product</button>
                            <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildVendorsDataTable();
    });

    function buildVendorsDataTable()
    {
        var ajaxurl=API_URL+'index.php/Vendor/vendors';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#vendors-list").dataTable().fnDestroy();
        $tableobj=$('#vendors-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No vendors found",
                "sZeroRecords": "No vendors found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.name;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.email;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.phone;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.contactby_name;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.contactby_phone;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var vendorData='';
                    vendorData+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                    vendorData+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    vendorData+='<i class="fa fa-ellipsis-v f18 plr10"></i>';
                    vendorData+='</a>';
                    vendorData+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                    vendorData+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                    vendorData+='<li><a access-element="edit" href="javascript:;" data-target="#myVendor" data-toggle="modal" onclick=\'manageVendors(this,"'+data.inventory_vendor_id+'")\'>Edit</a></li>';
                    vendorData+='<li><a access-element="delete" href="javascript:;" onclick=\'deleteVendor(this,"'+data.inventory_vendor_id+'")\'>Delete</a></li>';
                    vendorData+='</ul>';
                    vendorData+='</div>';
                    return vendorData;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function manageVendors(This,Id)
    {
        notify('Processing..', 'warning', 10);
        $('#vendor_button').html('');
        if(Id==0)
        {
            $('#vendor_button').html('<i class="icon-right mr8"></i>Add');
        }
        else
        {
            $('#vendor_button').html('<i class="icon-right mr8"></i>Update');
        }
        $('#ManageVendorForm')[0].reset();
        Materialize.updateTextFields();
        $('#ManageVendorForm span.required-msg').remove();
        $('#ManageVendorForm input').removeClass("required");
        $('#ManageVendorForm textarea').removeClass("required");
        $("#ManageVendorForm").removeAttr("disabled");
        $("#vendor_email").attr('disabled',false);
        var userID = $("#hdnUserId").val();
        $('#hdnVendorId').val(Id);
        if (Id == 0)
        {
            $("#myModalLabel").html("Create Vendor");
        }
        else {
            var ajaxurl = API_URL + 'index.php/Vendor/getVendorDetailsById';
            var params = {vendorId: Id};
            var userID = $('#hdnUserId').val();
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: editVendorResponse});
            $("#myModalLabel").html("Edit Vendor");
            $("#vendor_button").html("<i class=\"icon-right mr8\"></i>Update");
        }
    }
    function editVendorResponse(response)
    {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var vendorData = response.data[0];
            $("#vendor_name").val(vendorData.name);
            $("#vendor_name").next('label').addClass("active");
            $('#phone_number').val(vendorData.phone);
            $("#phone_number").next('label').addClass("active");
            $("#vendor_email").val(vendorData.email);
            $("#vendor_email").next('label').addClass("active");
            $("#vendor_email").attr('disabled',true);
            $("#vendor_address").val(vendorData.address);
            $("#vendor_address").next('label').addClass("active");
            $("#contact_by_name").val(vendorData.contactby_name);
            $("#contact_by_name").next('label').addClass("active");
            $("#contact_by_phone").val(vendorData.contactby_phone);
            $("#contact_by_phone").next('label').addClass("active");
        }
    }
    function saveVendor(This)
    {
        var vendorName = $("#vendor_name").val();
        var phoneNumber = $("#phone_number").val();
        var vendorEmail = $("#vendor_email").val();
        var vendorAddress = $('#vendor_address').val().trim();
        var contactByName = $('#contact_by_name').val();
        var contactByPhone = $('#contact_by_phone').val();
        var userID = $("#hdnUserId").val();
        var vendorId = $('#hdnVendorId').val();
        $('#ManageVendorForm span.required-msg').remove();
        $('#ManageVendorForm input').removeClass("required");
        $('#ManageVendorForm textarea').removeClass("required");
        var regx_productName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
        var regx_txtVendorEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var regx_txtVendorPhone = /^([+0-9()]{2,5})?[-. ]?[0-9]{3,5}?[-. ]?[0-9]{3,5}$/;
        var flag = 0;
        if (vendorName == '')
        {
            $("#vendor_name").addClass("required");
            $("#vendor_name").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } /*else if (regx_productName.test($('#vendor_name').val()) === false) {
     $("#vendor_name").addClass("required");
     $("#vendor_name").after('<span class="required-msg">Invalid Name</span>');
     flag = 1;
     }*/
        if (phoneNumber == '')
        {
            $("#phone_number").addClass("required");
            $("#phone_number").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (phoneNumber!='' && regx_txtVendorPhone.test($('#phone_number').val()) === false) {
            $("#phone_number").addClass("required");
            $("#phone_number").after('<span class="required-msg">Invalid Phone Number</span>');
            flag = 1;
        }
        if (vendorEmail == '')
        {
            $("#vendor_email").addClass("required");
            $("#vendor_email").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (vendorEmail!='' && regx_txtVendorEmail.test($('#vendor_email').val()) === false) {
            $("#vendor_email").addClass("required");
            $("#vendor_email").after('<span class="required-msg">Invalid Email</span>');
            flag = 1;
        }
        /*if (vendorAddress == '')
         {
         $("#vendor_address").addClass("required");
         $("#vendor_address").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
         flag = 1;
         }*/
        if (contactByName == '')
        {
            $("#contact_by_name").addClass("required");
            $("#contact_by_name").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_productName.test($('#contact_by_name').val()) === false) {
            $("#contact_by_name").addClass("required");
            $("#contact_by_name").after('<span class="required-msg">Invalid Name</span>');
            flag = 1;
        }
        if (contactByPhone == '')
        {
            $("#contact_by_phone").addClass("required");
            $("#contact_by_phone").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (contactByPhone!='' && regx_txtVendorPhone.test($('#contact_by_phone').val()) === false) {
            $("#contact_by_phone").addClass("required");
            $("#contact_by_phone").after('<span class="required-msg">Invalid Phone Number</span>');
            flag = 1;
        }
        if (flag == 1) {
            return false;
        } else {
            notify('Processing..', 'warning', 10);
            if(vendorId==0)
            {
                var ajaxurl = API_URL + 'index.php/Vendor/addVendor';
                var params = {userID: userID, vendorName: vendorName, phoneNumber: phoneNumber, vendorEmail: vendorEmail, vendorAddress: vendorAddress,contactByName:contactByName,contactByPhone:contactByPhone};
                var type = "POST";
                var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveVendorResponse});
            }
            else
            {
                var ajaxurl = API_URL + 'index.php/Vendor/updateVendor';
                var params = {userID: userID,vendorId:vendorId, vendorName: vendorName, phoneNumber: phoneNumber, vendorEmail: vendorEmail, vendorAddress: vendorAddress,contactByName:contactByName,contactByPhone:contactByPhone};
                var type = "POST";
                var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
                commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveVendorResponse});
            }
        }
    }
    function SaveVendorResponse(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            $.each(response.data, function (i, item) {
                alertify.dismissAll();
                notify(item, 'error', 10);
                if (response.data['vendorName'])
                {
                    alertify.dismissAll();
                    $("#vendor_name").addClass("required");
                    $("#vendor_name").after('<span class="required-msg">' + response.data['vendorName'] + '</span>');
                }
                if (response.data['phoneNumber'])
                {
                    alertify.dismissAll();
                    $("#phone_number").addClass("required");
                    $("#phone_number").after('<span class="required-msg">' + response.data['phoneNumber'] + '</span>');
                }
                if (response.data['vendorEmail'])
                {
                    alertify.dismissAll();
                    $("#vendor_email").addClass("required");
                    $("#vendor_email").after('<span class="required-msg">' + response.data['vendorEmail'] + '</span>');
                }
                if (response.data['vendorAddress'])
                {
                    alertify.dismissAll();
                    $("#vendor_address").addClass("required");
                    $("#vendor_address").after('<span class="required-msg">' + response.data['vendorAddress'] + '</span>');
                }
                if (response.data['contactByName'])
                {
                    alertify.dismissAll();
                    $("#contact_by_name").addClass("required");
                    $("#contact_by_name").after('<span class="required-msg">' + response.data['contactByName'] + '</span>');
                }
                if (response.data['contactByPhone'])
                {
                    alertify.dismissAll();
                    $("#contact_by_phone").addClass("required");
                    $("#contact_by_phone").after('<span class="required-msg">' + response.data['contactByPhone'] + '</span>');
                }
                return false;
            });
        } else
        {
            alertify.dismissAll();
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            buildVendorsDataTable();
            $('#myVendor').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    }
    function deleteVendor(This,Id)
    {
        var ModelTitle="Delete Status";
        var Description="Are you sure want to delete this vendor ?";
        $("#popup_confirm #btnTrue").attr('onClick','deleteVendorFinal(this,"'+Id+'")');
        customConfirmAlert(ModelTitle,Description);
    }
    function deleteVendorFinal(This,vendorId){
        var userID=$('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Vendor/deleteVendor';
        var params = {vendorId: vendorId};
        var action = 'delete';
        var type="POST";
        var headerParams = {
            action: action,
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: userID
        };
        commonAjaxCall({
            This: This,
            method:'POST',
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: action,
            onSuccess: ChangeVendorResponse
        });
    }
    function ChangeVendorResponse(response)
    {
        alertify.dismissAll();
        if(response.status===true){
            notify(response.message,'success',10);
            $('#popup_confirm').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildVendorsDataTable();
        }
        else{
            notify(response.message,'error',10);
        }

    }
</script>

