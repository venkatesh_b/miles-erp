<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
<input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
<input type="hidden" id="hdnUserId" name="hdnUserId" value="<?php echo $userData['userId']; ?>"/>
<input type="hidden" id="venueId" name="venueId" value="0"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Venue </h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" onclick="getVenueDetails(this, 0)" data-position="top" data-tooltip="Add Venue" class="viewsidePanel"><i class="icon-plus-circle" ></i></a></li>
                <!--<li><a></a></li>-->
            </ul>
        </div>   
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody" > <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0" access-element="list">
                <table class="table table-responsive table-striped table-custom table-info" id="venue-list">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>City</th>
                            <th class="border-r-none">Status</th>
                            <th class="no-sort" width="10%"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="venueModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeVenueModal()"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="venueModalLabel">Create Venue</h4>
                    </div>
                    <form action="/" method="post" id="venueForm" novalidate="novalidate">
                        <div class="modal-body modal-scroll clearfix">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="venueName" type="text" name="venueName" class="validate" class="formSubmit" required>
                                    <label for="venueName">Venue Name <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="country" name="country" class="validate" class="formSubmit">
                                        <option value="" selected>--Select--</option>
                                    </select>
                                    <label class="select-label">Country <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="state" name="state" class="validate" class="formSubmit">
                                        <option value="" selected>--Select--</option>
                                    </select>
                                    <label class="select-label">State <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <select id="city" name="city" class="validate" class="formSubmit">
                                        <option value="" selected>--Select--</option>
                                    </select>
                                    <label class="select-label">City <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="address" name="address" class="materialize-textarea"></textarea>
                                    <label for="address">Address</label>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button id="actionButton" type="submit" class="btn blue-btn" onclick="AddVenue(this)"><i class="icon-right mr8"></i>Add</button>
                        <button type="button" class="btn blue-light-btn" onclick="closeVenueModal()"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        buildVenueDataTable();
    });

    function buildVenueDataTable() {
        var userId = $('#hdnUserId').val();
        var ajaxurl=API_URL+'index.php/Venue/getVenueDatatables';
//        var params = {'reference_type':$pageUrl};
        var action = 'list';
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user: userId};
        $("#venue-list").dataTable().fnDestroy();
        $tableobj=$('#venue-list').DataTable( {
            "fnDrawCallback": function() {
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false
            }],
            columns: [
                {
                    data: null, render: function (data, type, row)
                    {
                        return data.name;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        return '<span class="text-capitalize">' + data.city + '<span>';
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        var status='';
                        if(data.is_active == 1)
                        {
                            status="Active";
                        }
                        else
                        {
                            status="Inactive";
                        }
                        return status;
                    }
                },
                {
                    data: null, render: function (data, type, row)
                    {
                        var rawHtml = '';
                        var status = data.is_active == 1 ? 'Active' : 'Inactive';
                        var statusText = data.is_active == 1 ? 'Inactive' : 'Active';
                        rawHtml += '';
//                        rawHtml += status;
                        rawHtml += '<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        rawHtml += '<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                        rawHtml += '<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        rawHtml += '<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        rawHtml += '<li><a access-element="edit" href="javascript:;" onclick="getVenueDetails(this,\'' + data.venue_id + '\')">Edit</a></li>';
                        rawHtml += '<li><a access-element="delete" href="javascript:;" onclick="deleteVenue(this,\'' + data.venue_id + '\', \'' + data.is_active + '\')">' + statusText + '</a></li>';
                        rawHtml += '</ul></div>';
                        return rawHtml;
                    }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();

    }
    
    function getVenueDetails(This, id){
        notify('Processing..', 'warning', 10);
        $('#venueForm .select-label').removeClass("active");
        $('#venueForm input').removeClass("required");
        var data = [];
        var method = 'GET';
        var action = 'edit';
        var params = {name: 'country'};
        var ajaxurl = API_URL + "index.php/Referencevalues/getReferenceValuesByName";
        var userID = $('#hdnUserId').val();
        if(id == 0){
            //add
            $('#venueModalLabel').html('Create Venue');
            $('#actionButton').html('<i class="icon-right mr8"></i>Add');
            params = {name: 'country'};
            ajaxurl = API_URL + "index.php/Referencevalues/getReferenceValuesByName";
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: method, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
                alertify.dismissAll();
                branchCountry(response);
            }});
            $('#venueId').val('0');
            $('#venueModal').modal('show');
        } else{
            $('#venueId').val(id);
            //edit
            $('#venueModalLabel').html('Edit Venue');
            $('#actionButton').html('<i class="icon-right mr8"></i>Update');
            params = {venue_id: id};
            ajaxurl = API_URL + "index.php/Venue/getVenueById";
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: method, params: params, headerParams: headerParams, requestUrl: ajaxurl, action: 'list', onSuccess: function(response){
                if (response == -1 || response['status'] == false) {
                    alertify.dismissAll();
                    notify(response['message'], 'error', 10);
                } else {
                    var rawData = response.data;
                    var countryId = rawData.city.countryId;
                    var stateId = rawData.city.stateId;
                    $.when(
                        commonAjaxCall
                        (
                            {
                                This: This,
                                headerParams: headerParams,
                                requestUrl: API_URL + "index.php/Referencevalues/getReferenceValuesByName",
                                params: {'name': 'country'},
                                action: action
                            }
                        ),
                        commonAjaxCall
                        (
                            {
                                This: This,
                                headerParams: headerParams,
                                requestUrl: API_URL + "index.php/Referencevalues/getReferenceValuesUsingName",
                                params: {parent_id: countryId, name: 'state'},
                                action: action
                            }
                        ),
                        commonAjaxCall
                        (
                            {
                                This: This,
                                headerParams: headerParams,
                                requestUrl: API_URL + "index.php/Referencevalues/getReferenceValuesUsingName",
                                params: {parent_id: stateId, name: 'city'},
                                action: action
                            }
                        )
                    ).then(function (response1, response2, response3) {
                        var country = response1[0];
                        var state = response2[0];
                        var city = response3[0];

                        /*Country*/
                        branchCountry(country);
                        $('#country').material_select();
                        getStateList(state);
                        $('#state').material_select();
                        getCityDropDown(city);
                        $('#city').material_select();
                        
                        $('#country').val(rawData.city.countryId);
                        $("#state").val(rawData.city.stateId);
                        $("#city").val(rawData.city.id);
                        $('#venueName').val(rawData.details.name);
                        $('#venueName').next('label').addClass("active");
                        $('#address').val(rawData.details.address);
                        $('#address').next('label').addClass("active");
                        $('#country').material_select();
                        $('#state').material_select();
                        $('#city').material_select();
                        alertify.dismissAll();
//                        $('#countryModal').modal('toggle');
                        $('#venueModal').modal('show');
                    });
                }
            }});
            
        }
        
        
    }
    
    function closeVenueModal(){
        $('#venueForm')[0].reset();

	$('#country').find('option:gt(0)').remove();
	$('#state').find('option:gt(0)').remove();
	$('#city').find('option:gt(0)').remove();
        $('#venueId').val(0);
        $('#venueForm label').removeClass("active");
        $('#venueForm span.required-msg').remove();
        $('#venueForm input').removeClass("required");

        $("select[name=state]").val(0);
        $("select[name=country]").val(0);
        $("select[name=city]").val(0);
        $('select').material_select();

        $('#venueModal').modal('toggle');

        $("#venueModalLabel").html("Create Venue");
    }
    
    function AddVenue(This){
        notify('Processing..', 'warning', 10);
        $('#venueForm span.required-msg').remove();
        $('#venueForm input').removeClass("required");
        var data ,url , action = '';
        
        var id = $('#venueId').val();
        
        var venueName = $('#venueName').val();
        
        var country = $("select[name=country]").val();
        var state = $("select[name=state]").val();
        var city = $('select[name=city]').val();
        var address=$('#address').val();
        var flag = 0;

        if (country == '' || country == null) {
            $("#country").parent().find('.select-dropdown').addClass("required");
            $("#country").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if (state === '' || state == null) {
            $("#state").parent().find('.select-dropdown').addClass("required");
            $("#state").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        
        if (city === '' || city == null) {
            $("#city").parent().find('.select-dropdown').addClass("required");
            $("#city").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }

        if (venueName == '') {
            $("#venueName").addClass("required");
            $("#venueName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if (flag == 0) {
            data = {
                venueName: venueName,
                city: city,
                address:address
            };
            if(id == 0){
                //add
                url = API_URL + "index.php/Venue/saveVenue";
                action = 'add';
            }else{
                //edit
                url = API_URL + "index.php/Venue/editVenue/venue_id/"+id;
                action = 'edit';
            }
            var userId = $('#hdnUserId').val();
            var ajaxurl = url;
            var params = data;
            var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: function (response)
            {
                alertify.dismissAll();
                if (response == -1 || response['status'] == false)
                {
                    var id = '';
                    if (Object.size(response.data) > 0)
                    {
                        for (var a in response.data) {
                            id = '#' + a;
                            if (a == 'country' || a == 'state' || a == 'city') {
                                $(id).parent().find('.select-dropdown').addClass("required");
                                $(id).parent().after('<span class="required-msg">' + response.data[a] + '</span>');
                            } else if (a == 'id'){
                                notify(response.data[a], 'error', 10);
                            } else{
                                $(id).addClass("required");
                                $(id).after('<span class="required-msg">' + response.data[a] + '</span>');
                            }
                        }
                    }
                    else
                    {
                        notify(response.message, 'error', 10);
                    }
                }
                else
                {
                    notify('Data Saved Successfully', 'success', 10);
                    buildVenueDataTable();
                    closeVenueModal();
                }
            }});
        }
    }
    
    function deleteVenue(This, venueId, currentStatus) {
        var status = currentStatus == 1 ? 'inactivate' : 'activate';
        var conf = "Are you sure want to " + status + " this venue ?";
        customConfirmAlert('Venue Status', conf);
        $("#popup_confirm #btnTrue").attr("onclick", "deleteVenueConfirm(this,'" + venueId + "')");
    }
    function deleteVenueConfirm(This, venueId) {
        notify('Processing..', 'warning', 10);
        var userId = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/Venue/deleteVenue/venue_id/' + venueId;
        var type = "POST";
        var action = 'delete';
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userId};
        commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, headerParams: headerParams, action: 'delete', onSuccess: deleteVenueResponse});
    }
    function deleteVenueResponse(response) {
        alertify.dismissAll();
        if (response == -1 || response['status'] == false) {
            notify(response.message, 'error', 10);
        } else {
            notify(response.message, 'success', 10);
            buildVenueDataTable();
        }
    }
</script>
