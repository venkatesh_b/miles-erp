<div class="contentpanel" id="voucherview"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Voucher Management</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class="tooltipped" data-position="top" data-tooltip="Add Voucher" class="viewsidePanel"  onclick="ManageVoucher(this,0)"><i class="icon-plus-circle" ></i></a></li>
            </ul>
        </div>   
    </div>
  <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
         <div access-element="list" class="fixed-wrap clearfix"> 
                <div class="col-sm-12 p0">
                    <table class="table table-responsive table-striped table-custom" id="vouchers-list">
                      <thead>
                            <tr>
                                  <th>Voucher Number</th>
                                  <th>Branch</th>
                                  <th class="text-right">Amount</th>
                                  <th>Date</th>
                                  <th>Status</th>
                                  <th>Is active</th>
                            </tr>
                      </thead>
                    </table>
                </div>
          </div>
          <!--Modal1-->
          <div class="modal fade" id="myVoucher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="500">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="ManageVoucherForm">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                    <h4 class="modal-title" id="myModalLabel">Create Voucher</h4>
                  </div>
                  <div class="modal-body modal-scroll clearfix">
                      <input type="hidden" id="hdnVoucherId" value="0"/>
                    <div class="col-sm-12">
                    	<div class="input-field">
                          <input type="text" class="" id="txtVoucherName">
                           <label for="txtVoucherName"> Voucher Number <em>*</em></label>
                        </div>
                   </div>
                  <div class="col-sm-12">
                      <div class="input-field">
                          <select id="ddlCompanies">
                              <option value=""  selected>--Select--</option>
                          </select>
                          <label class="select-label">Company <em>*</em></label>
                      </div>
                  </div>
                    <div class="col-sm-12">
                        <div class="input-field">
                        <select id="txtCourseBranches">
                          <option value="" disabled selected>--Select--</option>
                        </select>
                        <label class="select-label">Branches <em>*</em></label>
                      </div>
                   </div>
                      <div class="col-sm-12">
                          <div class="input-field">
                              <input type="text" class="" id="txtVoucherAmount" onkeypress="return isNumberKey(event)">
                              <label for="txtVoucherName"> Amount <em>*</em></label>
                          </div>
                      </div>
                      <div class="col-sm-12">
                          <div class="input-field">
                              <input type="date" placeholder="dd/mm/yyyy" class="datepicker relative enablePastDatesWithCurrentDate" id="txtVoucherDate">
                              <label class="datepicker-label datepicker" for="txtVoucherDate"> Date <em>*</em></label>
                          </div>
                      </div>
                    <input type="hidden" readonly value="1" id="txtVoucherStatusValue"/>
                    <div class="col-sm-12">
                        <div class="input-field">
                            <textarea class="materialize-textarea" id="txtVoucherDescription"></textarea>

                            <label for="txtVoucherDescription">Description <em>*</em></label>
                         </div>
                    </div>
                      <div class="col-sm-12" access-element="status">
                          <div class="switch display-inline-block">
                              <label class="display-block">Status</label>
                              <label>
                                  Off
                                  <input type="checkbox" id="txtVoucherStatus" checked>
                                  <span class="lever"></span>
                                  On
                              </label>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn blue-btn" onclick="saveVoucher(this)" id="btnSaveVoucher"><i class="icon-right mr8"></i>Submit</button>
                     <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
    </div><!-- InstanceEndEditable -->
  </div>
<script>
docReady(function(){
    buildVouchersDataTable();
});

function buildVouchersDataTable()
{
        var userID=$("#hdnUserId").val();
        var ajaxurl=API_URL+'index.php/Voucher/getAllVouchers';
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#vouchers-list").dataTable().fnDestroy();
        $tableobj = $('#vouchers-list').DataTable( {
            "fnDrawCallback": function() {
                $("#vouchers-list thead th").removeClass("icon-rupee");
                buildpopover();
                verifyAccess();
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                        $('.dataTables_paginate').css("display", "block");
                        $('.dataTables_length').css("display", "block");
                        //$('.dataTables_filter').css("display", "block");
                } else {
                        $('.dataTables_paginate').css("display", "none");
                        $('.dataTables_length').css("display", "none");
                        //$('.dataTables_filter').css("display", "none");
                }
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No vouchers found",
                "sZeroRecords": "No vouchers found",
                "sProcessing":"<img src='<?php echo BASE_URL; ?>assets/images/preloader.gif'>"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            "order": [[ 3, "desc" ]],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                    {
                        //voucher_code
                        return data.voucher_code+'-'+data.voucher_name;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.branch_name;
                    }
                },
                {
                    data: null,className:"text-right icon-rupee", render: function ( data, type, row )
                    {
                        return moneyFormat(data.amount);
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.voucher_date;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        return data.status;
                    }
                },
                {
                    data: null, render: function ( data, type, row )
                    {
                        var status='',voucher_status='';
                        var status = ''
                        var statusText = '';
                        if(data.is_active == 1)
                        {
                            status="Active";
                            statusText = "Inactive";
                        }
                        else{
                            status="Inactive";
                            statusText = "Active";
                        }
                        voucher_status+=status;
                        voucher_status+='<div class="dropdown feehead-list pull-right custom-dropdown-style">';
                        voucher_status+='<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v f18 pr10 pl10"></i></a>';
                        voucher_status+='<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">';
                        voucher_status+='<li class="text-right pr10"><i class="fa fa-ellipsis-v f18"></i></li>';
                        voucher_status+='<li><a access-element="edit" href="javascript:;" onclick="ManageVoucher(this,\''+data.voucher_id+'\')">Edit</a></li>';
                        voucher_status+='<li><a access-element="delete" href="javascript:;" onclick="DeleteVoucher(this,\''+data.voucher_id+'\','+data.is_active+')">'+statusText+'</a></li>';
                        voucher_status+='</ul></div>';
                        return voucher_status;
                    }
                }
            ],
            "createdRow": function ( row, data, index )
            {

                if(data.is_active == 0)
                {
                    $(row).addClass('disabled-branchview');
                }
            }
        } );
        DTSearchOnKeyPressEnter();
}
function ManageVoucher(This, Id)
{
    $('#ManageVoucherForm')[0].reset();
    $('#txtCourseBranches').empty();
    $('#txtCourseBranches').material_select();
    $('#ManageVoucherForm label').removeClass("active");
    $('#ManageVoucherForm span.required-msg').remove();
    $('#ManageVoucherForm input').removeClass("required");
    $('#ManageVoucherForm select').removeClass("required");
    $('#ManageVoucherForm textarea').removeClass("required");
    $('#ddlCompanies').empty();
    $('#ddlCompanies').material_select();
    $('#txtVoucherStatusValue').val(1);
    var userID = $("#hdnUserId").val();
    AllCompanies(This, userID);
    if (Id == 0)
    {
        TotalBranches(This, userID);
        $("#myModalLabel").html("Create Voucher");
        $("#btnSaveVoucher").val("Add");
        $("#btnSaveVoucher").html("<i class=\"icon-right mr8\"></i>Add");
    }
    else
    {
        alertify.dismissAll();
        notify('Processing..', 'warning', 10);
        var headerParams1 = {context: 'default', Authorizationtoken: $accessToken, user: userID};
        var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        $.when(
                commonAjaxCall
                (
                    {
                        This: This,
                        headerParams: headerParams1,
                        requestUrl: API_URL + 'index.php/Branch/getBranchList',
                        action: 'edit'
                    }
                ),
                commonAjaxCall
                (
                    {
                        This: This,
                        headerParams: headerParams,
                        params: {voucherId: Id},
                        requestUrl: API_URL + 'index.php/Voucher/getVoucherDetails',
                        action: 'edit'
                    }
                )
            ).then(function (response1, response2)
            {
                TotalBranchesResponse(response1[0]);
                ManageVoucherResponse(response2[0]);
                $("#myModalLabel").html("Edit Voucher");
                $("#btnSaveVoucher").val("Add");
                $("#btnSaveVoucher").html("<i class=\"icon-right mr8\"></i>Update");
            });
    }

    $("#hdnVoucherId").val(Id);
    alertify.dismissAll();
    $(This).attr("data-target", "#myVoucher");
    $(This).attr("data-toggle", "modal");
}
$('#txtVoucherStatus').change(function () {
    if ($('#txtVoucherStatus').is(':checked')) {
        $('#txtVoucherStatusValue').val(1);
    } else {
        $('#txtVoucherStatusValue').val(0);
    }
});

function ManageVoucherResponse(response)
{
    var VoucherDetails = response;
    if (VoucherDetails == -1 || VoucherDetails['status'] == false)
    {
        notify('Something went wrong', 'error', 10);
    } else
    {
        var VouchersData = VoucherDetails.data[0];

        $("#txtVoucherName").val(VouchersData.name);
        $("#txtVoucherName").next('label').addClass("active");

        console.log('TEST --',VouchersData.fk_company_id);
        $('#ddlCompanies').val(VouchersData.fk_company_id);
        $('#ddlCompanies').material_select();
        $('#ddlCompanies').trigger('change');
        $("#ddlCompanies").next('label').addClass("active");

        $('#txtCourseBranches').val(VouchersData.fk_branch_id);
        $('#txtCourseBranches').material_select();
        $('#txtCourseBranches').trigger('change');
        $("#txtCourseBranches").next('label').addClass("active");

        $("#txtVoucherAmount").val(VouchersData.amount);
        $("#txtVoucherAmount").next('label').addClass("active");

        if(VouchersData.voucher_date!='0000-00-00')
        {
            setDatePicker('#txtVoucherDate', VouchersData.voucher_date);
        }
            //$("#txtVoucherDate").val(VouchersData.voucher_date);

        $("#txtVoucherDescription").val(VouchersData.description);
        $("#txtVoucherDescription").next('label').addClass("active");
    }
}

function saveVoucher(This) {
    var voucherId = $("#hdnVoucherId").val();
    var voucherName = $("#txtVoucherName").val();
    var companyId = $("#ddlCompanies").val();
    var branchId = $("#txtCourseBranches").val();
    var voucherAmount = $("#txtVoucherAmount").val();
    var voucherDate = $("#txtVoucherDate").val();
    var voucherDescription = $("#txtVoucherDescription").val();
    var voucherStatus = $('#txtVoucherStatusValue').val();
    var userID = $("#hdnUserId").val();
    var regx_txtCourseName = /^[a-zA-Z][a-zA-Z0-9-\s]*$/;

    $('#ManageVoucherForm span.required-msg').remove();
    $('#ManageVoucherForm input').removeClass("required");
    $('#ManageVoucherForm textarea').removeClass("required");
    var flag = 0;
    if (voucherName == '')
    {
        $("#txtVoucherName").addClass("required");
        $("#txtVoucherName").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }
    else if (regx_txtCourseName.test($('#txtVoucherName').val()) === false)
    {
        $("#txtVoucherName").addClass("required");
        $("#txtVoucherName").after('<span class="required-msg">Invalid Name</span>');
        flag = 1;
    }
    if (companyId == '')
    {
        $("#ddlCompanies").parent().find('.select-dropdown').addClass("required");
        $("#ddlCompanies").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if (branchId == '')
    {
        $("#txtCourseBranches").parent().find('.select-dropdown').addClass("required");
        $("#txtCourseBranches").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if (voucherAmount == '' || voucherAmount == 0)
    {
         $("#txtVoucherAmount").addClass("required");
         $("#txtVoucherAmount").after('<span class="required-msg">' + $listRequiredMessage + '</span>');
         flag = 1;
    }
    if (voucherDate == '')
    {
        $("#txtVoucherDate").addClass("required");
        $("#txtVoucherDate").after('<span class="required-msg">' + $listRequiredMessage + '</span>');
        flag = 1;
    }
    if (voucherDescription == '')
    {
        $("#txtVoucherDescription").addClass("required");
        $("#txtVoucherDescription").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
        flag = 1;
    }

    if (flag == 1)
    {
        return false;
    }
    else
    {
        if (voucherId == 0)
        {
            var ajaxurl = API_URL + 'index.php/Voucher/addVoucher';
            var params = {userID: userID, voucherName: voucherName, companyId: companyId,branchId: branchId,voucherAmount:voucherAmount, voucherDescription: voucherDescription, voucherStatus: voucherStatus, voucherDate:voucherDate };
            var type = "POST";
            var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveVoucherResponse});
        }
        else
        {
            var ajaxurl = API_URL + 'index.php/Voucher/updateVoucher';
            var params = {userID: userID,voucherId: voucherId, voucherName: voucherName, companyId: companyId,branchId: branchId,voucherAmount:voucherAmount, voucherDescription: voucherDescription, voucherStatus: voucherStatus, voucherDate:voucherDate };
            var type = "POST";
            var headerParams = {action: 'edit', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'edit', onSuccess: SaveVoucherResponse});
        }
    }
}

function SaveVoucherResponse(response) {
    var response = response;
    if (response == -1 || response.status == false)
    {
        $.each(response.data, function (i, item) {
            alertify.dismissAll();
            notify(item, 'error', 10);
            if (response.data['voucherName'])
            {
                alertify.dismissAll();
                $("#txtCourseName").addClass("required");
                $("#txtCourseName").after('<span class="required-msg">' + response.data['voucherName'] + '</span>');
            }
            return false;
        });
    }
    else
    {
        alertify.dismissAll();
        notify(response.message, 'success', 10);
        $(this).attr("data-dismiss", "modal");
        buildVouchersDataTable();
        $('#myVoucher').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

    }
}
function DeleteVoucher(This, VoucherID, currentStatus) {
    var status = currentStatus == 1 ? 'inactivate' : 'activate';
    var conf = "Are you sure want to " + status + " this voucher ?";
    customConfirmAlert('Voucher Status', conf);
    $("#popup_confirm #btnTrue").attr("onclick","ConfirmDeleteVoucher(this,'"+VoucherID+"','"+currentStatus+"')");

}
function ConfirmDeleteVoucher(This, VoucherID, status) {
    notify('Processing..', 'warning', 10);
    var userID = $("#hdnUserId").val();

    var ajaxurl = API_URL + 'index.php/Voucher/deleteVoucher/voucherId/' + VoucherID + '/userID/' + userID +'/status/'+ status;
    var params = {};
    var type = "DELETE";
    var headerParams = {action: 'delete', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
    commonAjaxCall({This: this, method: type, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'delete', onSuccess: DeleteVoucherResponse});
}
function DeleteVoucherResponse(response)
{
    $('#popup_confirm').modal('hide');
    alertify.dismissAll();
    if (response == -1 || response.status == false)
    {
        $.each(response.data, function (i, item) {
            notify(item, 'error', 10);
            return false;
        });
    } else
    {
        notify(response.message, 'success', 10);
        //renderCoursesData();
        buildVouchersDataTable();
        $('#myVoucher').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }
}
</script>
