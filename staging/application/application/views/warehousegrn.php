<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-19
 * Time: 06:11 PM
 */
?>

<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Warehouse GRN</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class=""  data-toggle="modal"><i class="icon-plus-circle" onclick="manageWarehouseGrn(this,0)"></i></a></li>
                <!--<li><a><i class="icon-filter"></i></a></li>-->

                <li><a></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="warehouse-gtn-list">
                    <thead>
                    <tr>
                        <th>GRN No.</th>
                        <th>Received By</th>
                        <th>Received On</th>
                        <th>Sent By</th>
                        <th>Sent On</th>
                        <th>No. of Items </th>
                        <th>Invoice</th>
                        <th>Status</th>
                        <th>Remarks</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="myGrnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="800">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Create GRN</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">

                        <!--<div class="col-sm-12">
                            <div class="input-field">
                                <input id="grn_no" type="text" name="grn_no" value="" class="validate">
                                <label for="grn_no">GRN No.</label>
                            </div>
                        </div>-->
                        <div class="col-sm-12">
                            <div class="multiple-radio">
                                <label>Received From</label>
                            <span class="inline-radio" >
                            <input id="txtBranch" class="with-gap" type="radio" name="receivedFrom" value="branch" checked>
                            <label for="txtBranch">Branch</label>
                            </span>
                            <span class="inline-radio">
                            <input id="txtOther" class="with-gap" type="radio" name="receivedFrom" value="vendor">
                            <label  for="txtOther">Vendor</label>
                            </span>
                            </div>
                        </div>
                        <div id="vendorInvoiceWrapper" style="display: none;">
                            <div class="col-sm-12">
                            <div class="input-field">
                                <input id="txtinvoice" name="txtinvoice" type="text" class="validate" maxlength="20" />
                                <label for="txtinvoice">Invoice Number <em>*</em></label>
                            </div>
                        </div>
                            <div class="col-sm-12 mb15">
                                <div class="file-field input-field">
                                    <div class="btn file-upload-btn">
                                        <span>Upload Invoice <em>*</em></span>
                                        <input type="file" id="txtfileinvoice">
                                    </div>
                                    <div class="file-path-wrapper custom">
                                        <input type="text" placeholder="Upload single file" class="file-path validate">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <textarea id="txtRemarks" name="txtRemarks" class="materialize-textarea"></textarea>
                                <label for="txtRemarks">Remarks</label>
                            </div>
                        </div>

                        <!-- -->
                        <div id="vendorgtnwrapper" style="display: none;">
                            <input id="hdnVendorGtn" name="hdnVendorGtn" type="hidden" class="validate">
                            <form name="warehouseGtnForm" id="warehouseGtnForm">
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <!--<input id="sent_by" name="sent_by" type="text"  class="validate">-->

                                        <div id="vendor_wrapper" class="input-field" style="display: none;">
                                            <select id="vendor" name="vendor" ></select>
                                            <label class="select-label">Vendor Name <em>*</em></label>
                                        </div>

                                    </div>
                                    <div class="col-sm-11 p0 addNewProduct">
                                        <div class="col-sm-6 pl0">
                                            <div class="input-field">
                                                <input id="txtProductCode" name="txtProductCode" type="text" class="validate" />
                                                <input id="hdnProductId" name="hdnProductId" type="hidden" class="validate" />
                                                <label for="txtProductCode" >Product Code <em>*</em></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pl0">
                                            <div class="input-field">
                                                <input id="txtProductQuantity" name="txtProductQuantity" type="text" class="validate" onkeypress="return isNumberKey(event)" />
                                                <input id="hdnProductQuantity" name="hdnProductQuantity" type="hidden" />
                                                <label for="txtProductQuantity" >Quantity <em>*</em></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 pl0">
                                        <a class="m10 ml0 display-inline-block cursor-pointer" onclick="addNewGtnProduct()"> <i class="icon-plus-circle f24 sky-blue"></i> </a>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="product-map-table p0">
                                        <table class="table table-responsive table-bordered mb0" id="warehouse-gtn">
                                            <thead>
                                            <tr>
                                                <th>Product Code</th>
                                                <th>Quantity</th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- -->
                        <div id="warehouseGtnWrapper" >
                            <input id="hdnSentBy" name="hdnSentBy" type="hidden" class="validate">
                            <input id="hdnMode" name="hdnMode" type="hidden" class="validate">
                            <div id="warehouse_grn">
                               <!-- <div class="col-sm-12 sent-field">
                                    <div class="input-field">
                                        <div id="grn_no_wrapper">
                                            <input id="grn_no" name="grn_no" type="text"  class="validate">
                                            <label for="grn_no">GDN No <em>*</em></label>
                                            <input id="stock_flow_id" type="hidden" name="stock_flow_id" >
                                        </div>
                                    </div>


                                </div>-->

                                <div class="col-sm-12 sent-field">
                                    <div class="input-field">
                                        <div id="grn_no_wrapper">
                                        <select id="grn_no" name="grn_no" class="validate" class="formSubmit">
                                            <option value="" disabled selected>--Select--</option>
                                        </select>
                                        <label class="select-label">GDN No <em>*</em></label>
                                        </div>
                                    </div>
                                </div>



                            </div>
                            <form name="warehouseGrnForm" id="warehouseGrnForm">
                                <div class="col-sm-12">
                                    <div class="product-map-table p0">
                                        <table class="table table-responsive table-bordered mb0" id="warehouse-grn-products">
                                            <thead>
                                            <tr>
                                                <th>Product Code</th>
                                                <th>Quantity</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="warehouseGrn_btn" class="btn blue-btn" onclick="saveWarehouseGrn(this)"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="whGrnProducts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="600">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="whGrnModal"></h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12 product-map-table p0" id="grnProductsWh">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save Changes</button>-->
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myGrnEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="600">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabelEdit">Edit GRN</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">

                        <!--<div class="col-sm-12">
                            <div class="input-field">
                                <input id="grn_no" type="text" name="grn_no" value="" class="validate">
                                <label for="grn_no">GRN No.</label>
                            </div>
                        </div>-->

                        <div class="col-sm-12">
                            <div class="input-field">
                                <input id="txtinvoiceEdit" name="txtinvoiceEdit" type="text" class="validate" maxlength="20" />
                                <label for="txtinvoiceEdit">Invoice Number <em>*</em></label>
                            </div>
                        </div>
                        <div class="col-sm-12" id="attachedInvoice">
                            <div class="input-field" id="attachedInvoiceInner">

                            </div>
                        </div>


                        <div class="col-sm-12 mb15">
                            <div class="file-field input-field">
                                <div class="btn file-upload-btn">
                                    <span>Upload New Invoice</span>
                                    <input type="file" id="txtfileinvoiceEdit">
                                </div>
                                <div class="file-path-wrapper custom">
                                    <input type="text" placeholder="Upload single file" class="file-path validate">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-field">
                                <textarea id="txtRemarksEdit" name="txtRemarksEdit" class="materialize-textarea" maxlength="255"></textarea>
                                <label for="txtRemarksEdit">Remarks</label>
                            </div>
                        </div>
                        <!-- -->
                        <div id="vendorgtnwrapperEdit">
                            <input id="hdnVendorGtnEdit" name="hdnVendorGtnEdit" type="hidden" class="validate">
                            <form name="warehouseGtnFormEdit" id="warehouseGtnFormEdit">
                                <div class="col-sm-12">
                                    <div class="input-field">
                                        <input id="stock_flow_idEdit" name="stock_flow_idEdit" type="hidden"  value='' class="validate">

                                        <div id="vendor_wrapperEdit" class="input-field">
                                            <select id="vendorEdit" name="vendorEdit" ></select>
                                            <label class="select-label">Vendor Name <em>*</em></label>
                                        </div>

                                    </div>
                                    <div class="col-sm-11 p0 addNewProduct">
                                        <div class="col-sm-6 pl0">
                                            <div class="input-field">
                                                <input id="txtProductCodeEdit" name="txtProductCodeEdit" type="text" class="validate" />
                                                <input id="hdnProductIdEdit" name="hdnProductIdEdit" type="hidden" class="validate" />
                                                <label for="txtProductCodeEdit" >Product Code <em>*</em></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pl0">
                                            <div class="input-field">
                                                <input id="txtProductQuantityEdit" name="txtProductQuantityEdit" type="text" class="validate" onkeypress="return isNumberKey(event)" />
                                                <input id="hdnProductQuantityEdit" name="hdnProductQuantityEdit" type="hidden" />
                                                <label for="txtProductQuantityEdit" >Quantity <em>*</em></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1 pl0">
                                        <a class="m10 ml0 display-inline-block cursor-pointer" onclick="addNewGtnProductEdit()"> <i class="icon-plus-circle f24 sky-blue"></i> </a>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="product-map-table p0">
                                        <table class="table table-responsive table-bordered mb0" id="warehouse-gtnEdit">
                                            <thead>
                                            <tr>
                                                <th>Product Code</th>
                                                <th>Quantity</th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="product-map-table p0  mt20" id="whGrnStatusChangeHistory">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="warehouseGrnEdit_btn" class="btn blue-btn" onclick="saveWarehouseGtnEdit(this)"><i class="icon-right mr8"></i>Submit for approval</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script>
    docReady(function(){
        buildWarehouseGrnDataTable();
    });
    function buildWarehouseGrnDataTable()
    {
        var ajaxurl=API_URL+'index.php/WarehouseGrn/warehouseGrnList';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#warehouse-gtn-list").dataTable().fnDestroy();
        $tableobj=$('#warehouse-gtn-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            "order": [[ 0, "desc" ]],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    if(data.is_editable==0)
                    var html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" data-target=\"#whGrnProducts\" data-toggle=\"modal\"  onclick=\"getGrnProduct(this,"+data.sequence_number+",'"+data.new_sequence_number+"')\">"+data.new_sequence_number+"</a></div>";
                    else if(data.is_editable==1)
                    var html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\"  onclick=\"manageWarehouseGrnEdit(this,"+data.stockflow_id+")\">"+data.new_sequence_number+"</a></div>";

                    //return data.sequence_number;
                    return html;
                    //return data.sequence_number;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receiveName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receivedOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.requestedName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.sentOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.total;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    if(data.invoice_number.trim()=='' || data.invoice_number==null){
                        html='----';
                    }
                    else{
                        html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" onclick=\"downloadInvoice('"+data.invoice_attachment+"')\">"+data.invoice_number+"</a></div>";
                    }
                    return html;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    if(data.approval_status===null || data.approval_status.trim()=='')
                        data.approval_status='----';
                    return data.approval_status;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    if(data.approval_remarks===null || data.approval_remarks.trim()=='')
                        data.approval_remarks='----';
                    return data.approval_remarks;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    function getProducts()
    {
        $('#txtProductCode').next('ul').remove();
        var headerParams = {
            action: 'add',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $("#txtProductCode").jsonSuggest({
            onSelect:function(item){
                var product = item.text;
                var productName = product.split(' | ');
                $("#txtProductCode").val(productName[0]);
                $("#hdnProductId").val(item.id);
            },
            headers:headerParams,url:API_URL + 'index.php/WarehouseGrn/getProducts' , minCharacters: 2});
    }

    function buildBranchGtnNumbers(response)
    {
        $('#grn_no').find('option:gt(0)').remove();
        $('#grn_no').material_select();
        if (response.data.length > 0) {

            $.each(response.data, function (key, value) {

                var o = $('<option/>', {value: value.text})
                    .text(value.text);
                o.appendTo('#grn_no');

                var req = value.id.split('|');
                $("#hdnSentBy").val(req[0]);
                $("#hdnMode").val(req[1]);

            });
        }
        $('#grn_no').material_select();

        $("#grn_no").change(function () {
            getBranchGtnProductsByNo($('#grn_no').val());
        });
    }

    function getBranchGtnNumbers()
    {
        $("#vendor").val('');
        $('#vendor').material_select();
        $("#vendor_wrapper").hide();
        $("#grn_no_wrapper").show();
        $("#grn_no").val('');
        $('#stock_flow_id').val('');

        var headerParams = {
            action: 'add',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        var ajaxurl = API_URL + 'index.php/WarehouseGrn/getBranchGtnNumbers'
        commonAjaxCall({This: this, requestUrl: ajaxurl, params: null, headerParams: headerParams, action: 'add', onSuccess: buildBranchGtnNumbers});
        return true;

        $('#grn_no').empty();
        $('#grn_no').material_select();
        var o = $('<option/>', {value: ''})
            .text('--Select--');
        o.appendTo('#grn_no');
       /* if (response.data.length == 0 || response == -1 || response['status'] == false)
        {
            //
        } else
        {
            *//*var o = $('<option/>', { value: '' })
             .text('--Select All--');
             o.appendTo('#txtUserBranch');*//*
            var branchRawData = response.data;
            for (var b = 0; b < branchRawData.length; b++) {
                var o = $('<option/>', {value: branchRawData[b]['id']})
                    .text(branchRawData[b]['name']);
                o.appendTo('#txtUserBranch');
            }
        }*/
        $('#grn_no').material_select();




        return true;

        $("#grn_no").next('ul').remove();
        $("#vendor").val('');
        $('#vendor').material_select();
        $("#vendor_wrapper").hide();
        $("#grn_no_wrapper").show();
        $("#grn_no").val('');
        $('#stock_flow_id').val('');
        var headerParams = {
            action: 'add',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $("#grn_no").jsonSuggest({
            onSelect:function(item){
                $("#grn_no").val(item.text);
                var request = item.id;
                var req = request.split('|');
                $("#hdnSentBy").val(req[0]);
                $("#hdnMode").val(req[1]);
                getBranchGtnProductsByNo(item.text);
            },
            headers:headerParams,url:API_URL + 'index.php/WarehouseGrn/getBranchGtnNumbers' , minCharacters: 2});
    }
    function getBranchGtnProductsByNo(gtnId)
    {
        var userID = $("#hdnUserId").val();
        var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall
        (
            {
                This: this,
                headerParams: headerParams,
                params: {gtnId: gtnId},
                requestUrl: API_URL + 'index.php/WarehouseGrn/getGtnProductDetails',
                action: 'add',
                onSuccess : setGtnProductResponse
            }
        )
    }

    function setGtnProductResponse(response)
    {
        $('#warehouse-grn-products tbody').html('');
        alertify.dismissAll();
        var details = response;
        if (details == -1 || details['status'] == false)
        {
            notify('Something went wrong', 'error', 10);
        } else
        {
            var productData = details.data;
            var products = '';
            $('#stock_flow_id').val(productData.stockflow_id);
            var branchGrnProducts = new Object();
            $.each(productData.products, function (key, value) {
                products +='<tr><td>'+value.code+'</td>';
                products +='<td><span id="txtQuantity_'+value.product_id+'">'+value.quantity+'</span>';
                products +='<div class="col-sm-4 pl0"><input class="modal-table-textfiled m0"  type="hidden" name="return_quantity" id="txtReturn_'+value.product_id+'"/></div></td>';
                products +='</tr>';
            });
            $('#warehouse-grn-products tbody').append(products);
        }
    }
    function getGrnVendors()
    {
        $("#vendor").val('');
        $("#vendor_wrapper").show();
        $("#grn_no_wrapper").hide();
        $("#grn_no").val('');
        $('#stock_flow_id').val('');
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/WarehouseGrn/getVendors';
        var action = 'add';
        var params = {};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getGrnVendorsResponse});
    }
    function getGrnVendorsResponse(response)
    {
        $('#vendor').empty();
        $('#vendor').append($('<option  selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#vendor').append($('<option></option>').val(value.id).html(value.text));
                });
            }
        }
        $('#vendor').material_select();
    }

    $(document).ready(function() {
        $('input[type=radio][name=receivedFrom]').change(function() {
            $("#hdnSentBy").val('');
            $("#hdnMode").val('');
            $('#stock_flow_id').val('');
            $('#warehouse_grn span.required-msg').remove();
            $('#warehouse_grn input').removeClass("required");
            $("#warehouse_grn").removeAttr("disabled");
            $('span.required-msg').remove();
            $('#vendor').parent().find('.select-dropdown').removeClass('required');
            $("#vendor").parent().find(".required-msg").remove();
            $('#grn_no').parent().find('.select-dropdown').removeClass('required');
            $("#grn_no").parent().find(".required-msg").remove();
            $('#warehouseGrnForm span.required-msg').remove();
            $('#warehouseGrnForm input').removeClass("required");
            $("#warehouseGrnForm").removeAttr("disabled");
            $('#warehouseGtnForm span.required-msg').remove();
            $('#warehouseGtnForm input').removeClass("required");
            $("#warehouseGtnForm").removeAttr("disabled");
            $('#txtinvoice').removeClass("required");
            $("#txtinvoice").removeAttr("disabled");
            if (this.value == 'branch') {
                $('#vendorInvoiceWrapper').hide();
                $('#vendorgtnwrapper').hide();
                $('#warehouseGtnWrapper').show();
                $('#warehouseGrn_btn').removeAttr('onclick');
                $('#warehouseGrn_btn').attr('onclick',"saveWarehouseGrn(this)");
                getBranchGtnNumbers();
                $('#warehouseGtnForm span.required-msg').remove();
                $('#warehouseGtnForm input').removeClass("required");
                $("#warehouseGtnForm").removeAttr("disabled");
                $('#warehouse-gtn tbody').html('');
                gtnProducts = new Object();
                rowNum =0;
            }
            if (this.value == 'vendor') {
                $('#warehouseGtnWrapper').hide();
                $('#vendorgtnwrapper').show();
                $('#vendorInvoiceWrapper').show();
                $('#warehouseGrn_btn').removeAttr('onclick');
                $('#warehouseGrn_btn').attr('onclick',"saveWarehouseGtn(this)");
                getGrnVendors();
                //$('#warehouse_grn')[0].reset();
                $('#warehouse_grn span.required-msg').remove();
                $('#warehouse_grn input').removeClass("required");
                $("#warehouse_grn").removeAttr("disabled");
                $('#warehouseGrnForm')[0].reset();
                Materialize.updateTextFields();
                $('#warehouseGrnForm span.required-msg').remove();
                $('#warehouseGrnForm input').removeClass("required");
                $("#warehouseGrnForm").removeAttr("disabled");
                $('#warehouse-grn-products tbody').html('');
            }
        });
        getProducts();
    });
    function saveWarehouseGrn(This)
    {
        $('#warehouse_grn span.required-msg').remove();
        $('#warehouse_grn input').removeClass("required");
        $("#warehouse_grn").removeAttr("disabled");
        $('span.required-msg').remove();
        $('#vendor').parent().find('.select-dropdown').removeClass('required');
        $("#vendor").parent().find(".required-msg").remove();

        $('#grn_no').parent().find('.select-dropdown').removeClass('required');
        $("#grn_no").parent().find(".required-msg").remove();

        $('#warehouseGrnForm span.required-msg').remove();
        $('#warehouseGrnForm input').removeClass("required");
        $("#warehouseGrnForm").removeAttr("disabled");
        $('#warehouseGtnForm span.required-msg').remove();
        $('#warehouseGtnForm input').removeClass("required");
        $("#warehouseGtnForm").removeAttr("disabled");
        $('#txtinvoice').removeClass("required");
        $("#txtinvoice").removeAttr("disabled");
        var sentBy = $('#sent_by').val();
        var sentById = $('#request_mode_id').val();
        var stock_flow_id = $('#stock_flow_id').val();
        var grnInvoice = $('#txtinvoice').val().trim();
        var grnRemarks = $('#txtRemarks').val().trim();
        var flag = 0;
        var files = $("#txtfileinvoice").get(0).files;
        var selectedFile = $("#txtfileinvoice").val();
        var extArray = ['gif', 'jpg', 'jpeg', 'png'];
        var extension = selectedFile.split('.');
        var fileUpload = document.getElementById("txtfileinvoice");
        var InValidExtensions = ['exe']; //array of in valid extensions
        $('#warehouse_grn span.required-msg').remove();
        $('#warehouse_grn input').removeClass("required");
        var branchId = $("#hdnSentBy").val();
        var reqMode = $("#hdnMode").val();
        var userID = $("#hdnUserId").val();
        var receivedFrom = $("input[name=receivedFrom]:checked").val();
        var returnProducts = [];
        var totalProducts = 0;
        $('input[name="return_quantity"]').each(function() {
            totalProducts = totalProducts+1;
            var currentVal = parseInt($(this).val());
            var res = $(this).attr('id').split("_");
            var actualQuantity = parseInt($('#txtQuantity_'+res[1]).html());
            returnProducts.push(res[1] + '|' +actualQuantity);
        });


        if(receivedFrom == 'branch')
        {
            var grnNo = $('#grn_no').val();

            if (grnNo == '' || grnNo == null)
            {
                $('#grn_no').parent().find('.select-dropdown').addClass('required');
                $("#grn_no").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                //$("#grn_no").addClass("required");
                //$("#grn_no").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag = 1;
            }
            else if(totalProducts ==0 )
            {
                //$("#grn_no").addClass("required");
                //$("#grn_no").after('<span class="required-msg">No products for this GDN</span>');
                notify('No products for this GDN','error', 10);
                flag=1;
            }
        }
        if(receivedFrom == 'vendor')
        {
            if (grnInvoice == '')
            {
                $("#txtinvoice").addClass("required");
                $("#txtinvoice").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag = 1;
            }


            if (fileUpload.value == null || fileUpload.value == '') {
                alertify.dismissAll();
                notify('Upload the invoice', 'error', 10);
                flag = 1;
            }
            else{
                var fileName = selectedFile;
                var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
                if ($.inArray(fileNameExt, InValidExtensions) == -1){

                }
                else{
                    alertify.dismissAll();
                    notify('Upload valid invoice', 'error', 10);
                    flag = 1;
                }
            }

            var grnNo = $('#hdnVendorGtn').val();
            var vendor = $('#vendor').val();
            if(vendor == '')
            {
                $('#vendor').parent().find('.select-dropdown').addClass('required');
                $("#vendor").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;
            }
            else if(vendor !='' && totalProducts ==0)
            {
                $('#vendor').parent().find('.select-dropdown').addClass('required');
                notify('No products for this GTN','error', 10);
                flag = 1;
            }
        }
        if(flag == 1)
        {
            return false;
        }
        else {
            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/WarehouseGrn/addWarehouseGrn';
            var params = {userID: userID,grnNo:grnNo, receiverMode: 'warehouse',receiverObjectId:1, requestMode: reqMode ,requestObjectId :branchId, returnProducts : returnProducts,stock_flow_id:stock_flow_id};
            var type = "POST";
            var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            //commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveWarehouseGrnResponse});
            if (fileUpload.value != null) {
                var uploadFile = new FormData();
                if (files.length > 0) {
                    uploadFile.append("file", files[0]);
                    uploadFile.append("image", 'invoice_' + new Date().getTime());
                }
                uploadFile.append('userID', userID);
                uploadFile.append('grnNo', grnNo);
                uploadFile.append('receiverMode', 'warehouse');
                uploadFile.append('receiverObjectId', 1);
                uploadFile.append('requestMode', reqMode);
                uploadFile.append('requestObjectId', branchId);
                uploadFile.append('grnRemarks', grnRemarks);
                uploadFile.append('products', returnProducts);
                uploadFile.append('grnInvoice', grnInvoice);
                uploadFile.append('stock_flow_id', stock_flow_id);
                $.ajax({
                    type: type,
                    url: ajaxurl,
                    dataType: "json",
                    data: uploadFile,
                    contentType: false,
                    processData: false,
                    headers: headerParams,
                    beforeSend: function () {
                        $(This).attr("disabled", "disabled");
                    }, success: function (response) {
                        alertify.dismissAll();
                        $(This).removeAttr("disabled");
                        SaveWarehouseGrnResponse(response);
                    }, error: function (response) {
                        alertify.dismissAll();
                        notify('Something went wrong. Please try again', 'error', 10);
                    }
                });
//            }
            }
            else {
                alertify.dismissAll();
                notify('Validation Error', 'error', 10);
            }
        }
    }
    function SaveWarehouseGrnResponse(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
            if(response.data.invoiceNumber){
                $("#txtinvoice").addClass("required");
                $("#txtinvoice").after('<span class="required-msg">'+response.data.invoiceNumber+'</span>');
                flag = 1;
            }
        }
        else
        {
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            $('#myGrnModal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildWarehouseGrnDataTable();
        }
    }
    var rowNum=0;
    var gtnProducts = new Object();
    function addNewGtnProduct()
    {
        var productCode = $("#txtProductCode").val();
        var productId = $("#hdnProductId").val();
        var quantity = $("#txtProductQuantity").val();
        if(quantity!='')
            var quantity = parseInt($("#txtProductQuantity").val());

        $('#warehouseGtnForm span.required-msg').remove();
        $('#warehouseGtnForm input').removeClass("required");
        var flag = 0;
        var regx_productName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
        if (productCode == '')
        {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_productName.test($('#txtProductCode').val()) === false) {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (productCode != '' && productId == '')
        {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (quantity == '')
        {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (quantity <= 0) {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">Should be >0</span>');
            flag = 1;
        }
        if (flag == 1) {
            return false;
        } else {
            var products = "";
            if(typeof gtnProducts[productId]!='undefined')
            {
                $("#txtProductCode").addClass("required");
                $("#txtProductCode").after('<span class="required-msg">Product already added</span>');
            }
            else
            {
                var obj = new Object();
                obj.quantity=quantity;
                gtnProducts[productId] = obj;
                products +='<tr id=rownum'+rowNum+'><td>'+productCode+'</td>';
                products +='<td><div class="col-sm-11 p0">'+quantity+'</div><div class="col-sm-1 p0"> <span class="filter-selectall-wrap pull-right"><a href="javascript:;" class="reportFilter-unselectall mr0" onclick="removeProduct('+rowNum+',\''+productId+'\')"><i class="icon-times red"></i></a></span> </div></td>';
                products +='</tr>';
                $('#warehouse-gtn tbody').append(products);
                $('#txtProductCode').val('');
                $('#txtProductQuantity').val('');
                $('#hdnProductId').val('');
                $('#hdnProductQuantity').val('');
                $(".addNewProduct label").removeClass("active");
                rowNum = rowNum+1;
            }
        }
    }
    function removeProduct(rowNum,pcode)
    {
        delete gtnProducts[pcode];
        $('#rownum'+rowNum).remove();
    }
    function saveWarehouseGtn(This)
    {
        alertify.dismissAll();
        $('#warehouse_grn span.required-msg').remove();
        $('#warehouse_grn input').removeClass("required");
        $("#warehouse_grn").removeAttr("disabled");
        $('span.required-msg').remove();
        $('#vendor').parent().find('.select-dropdown').removeClass('required');
        $("#vendor").parent().find(".required-msg").remove();
        $('#warehouseGrnForm span.required-msg').remove();
        $('#warehouseGrnForm input').removeClass("required");
        $("#warehouseGrnForm").removeAttr("disabled");
        $('#warehouseGtnForm span.required-msg').remove();
        $('#warehouseGtnForm input').removeClass("required");
        $("#warehouseGtnForm").removeAttr("disabled");
        $('#txtinvoice').removeClass("required");
        var sentBy = $('#vendor').val();
        var flag = 0;
        var grnInvoice = $('#txtinvoice').val().trim();
        var grnRemarks = $('#txtRemarks').val().trim();
        var files = $("#txtfileinvoice").get(0).files;
        var selectedFile = $("#txtfileinvoice").val();
        var extArray = ['gif', 'jpg', 'jpeg', 'png'];
        var extension = selectedFile.split('.');
        var fileUpload = document.getElementById("txtfileinvoice");
        var InValidExtensions = ['exe']; //array of in valid extensions
        var finalProducts = [];
        var userID = $("#hdnUserId").val();
        $.each(gtnProducts, function (i, item)
        {
            finalProducts.push(i+'||'+item.quantity);
        });
        finalProducts= finalProducts.join(',');

        var sentByVendor = $('#vendor').val();
        if (fileUpload.value == null || fileUpload.value == '') {
            notify('Upload the invoice', 'error', 10);
            flag = 1;
        }
        else{
            var fileName = selectedFile;
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            if ($.inArray(fileNameExt, InValidExtensions) == -1){

            }
            else{
                notify('Upload valid invoice', 'error', 10);
                flag = 1;
            }
        }
        if (grnInvoice == '')
        {
            $("#txtinvoice").addClass("required");
            $("#txtinvoice").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if(sentByVendor == '')
        {
            $('#vendor').parent().find('.select-dropdown').addClass('required');
            $("#vendor").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        else if(sentByVendor!='' && sentBy==''){
            $('#vendor').parent().find('.select-dropdown').addClass('required');
            $("#vendor").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if(finalProducts == '')//products mapped
        {
            notify('No products added','error', 10);
            flag=1;
        }
        if(flag == 1)
        {
            return false;
        }
        else
        {
            var ajaxurl = API_URL + 'index.php/WarehouseGrn/addWarehouseGrnFromVendor';
            var params = {userID: userID, requestMode : 'vendor',requestObjectId :sentBy, receiverMode: 'warehouse' ,receiverObjectId :1, products : finalProducts};
            var type = "POST";
            var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            //commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveWarehouseGtnResponse});
            if (fileUpload.value != null) {
                var uploadFile = new FormData();
                if (files.length > 0) {
                    uploadFile.append("file", files[0]);
                    uploadFile.append("image", 'invoice_' + new Date().getTime());
                }
                uploadFile.append('userID', userID);
                uploadFile.append('receiverMode', 'warehouse');
                uploadFile.append('receiverObjectId', 1);
                uploadFile.append('requestMode', 'vendor');
                uploadFile.append('requestObjectId', sentBy);
                uploadFile.append('grnRemarks', grnRemarks);
                uploadFile.append('products', finalProducts);
                uploadFile.append('grnInvoice', grnInvoice);
                $.ajax({
                    type: type,
                    url: ajaxurl,
                    dataType: "json",
                    data: uploadFile,
                    contentType: false,
                    processData: false,
                    headers: headerParams,
                    beforeSend: function () {
                        $(This).attr("disabled", "disabled");
                    }, success: function (response) {
                        alertify.dismissAll();
                        $(This).removeAttr("disabled");
                        SaveWarehouseGtnResponse(response);
                    }, error: function (response) {
                        alertify.dismissAll();
                        notify('Something went wrong. Please try again', 'error', 10);
                    }
                });
//            }
            }
            else {
                alertify.dismissAll();
                notify('Validation Error', 'error', 10);
            }
        }
    }
    function SaveWarehouseGtnResponse(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
            if(response.data.invoiceNumber){
                $("#txtinvoice").addClass("required");
                $("#txtinvoice").after('<span class="required-msg">'+response.data.invoiceNumber+'</span>');
                flag = 1;
            }

        }
        else
        {
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            $('#myGrnModal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildWarehouseGrnDataTable();
        }
    }
    function getGrnProduct(This,seqNum,newseqNum)
    {
        $('#whGrnModal').html('GRN No. '+newseqNum);
        $('#grnProductsWh').html('');
        var ajaxurl = API_URL + 'index.php/WarehouseGrn/getWareHouseGrnProducts';
        var params = {sequenceNumber:seqNum};
        var userID = $('#hdnUserId').val();
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({
            This: this,
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: 'list',
            onSuccess: function (response) {
                var stockhtml = '';
                stockhtml += "<table class=\"table table-responsive table-bordered mb0\">";
                stockhtml += "<thead>";
                stockhtml += "<tr class=\"\">";
                stockhtml += "<th>Product Name</th>";
                stockhtml += "<th>Quantity</th>";
                stockhtml += "</tr>";
                stockhtml += "</thead>";
                stockhtml += "<tbody>";
                if (response.status === true && response.data.length > 0) {
                    $.each(response.data, function (key, value) {
                        stockhtml += "<tr class=\"\">";
                        stockhtml += "<td>" + value.productName + "</td>";
                        stockhtml += "<td>" + value.quantity + "</td>";
                        stockhtml += "</tr>";
                    });
                }
                else {
                    stockhtml = "<tr colspan='2'>No records found</tr>";
                }
                stockhtml += "</tbody>";
                stockhtml += "</table>";
                $('#grnProductsWh').html(stockhtml);
            }
        });
    }
    function getProductsEdit()
    {
        $('#txtProductCodeEdit').next('ul').remove();
        var headerParams = {
            action: 'add',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $("#txtProductCodeEdit").jsonSuggest({
            onSelect:function(item){
                var product = item.text;
                var productName = product.split(' | ');
                $("#txtProductCodeEdit").val(productName[0]);
                $("#hdnProductIdEdit").val(item.id);
            },
            headers:headerParams,url:API_URL + 'index.php/WarehouseGrn/getProducts' , minCharacters: 2});
    }
    var rowNumEdit=0;
    var gtnProductsEdit = new Object();
    function addNewGtnProductEdit()
    {
        var productCode = $("#txtProductCodeEdit").val();
        var productId = $("#hdnProductIdEdit").val();
        var quantity = $("#txtProductQuantityEdit").val();
        if(quantity!='')
            var quantity = parseInt($("#txtProductQuantityEdit").val());

        $('#warehouseGtnFormEdit span.required-msg').remove();
        $('#warehouseGtnFormEdit input').removeClass("required");
        var flag = 0;
        var regx_productName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
        if (productCode == '')
        {
            $("#txtProductCodeEdit").addClass("required");
            $("#txtProductCodeEdit").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_productName.test($('#txtProductCodeEdit').val()) === false) {
            $("#txtProductCodeEdit").addClass("required");
            $("#txtProductCodeEdit").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (productCode != '' && productId == '')
        {
            $("#txtProductCodeEdit").addClass("required");
            $("#txtProductCodeEdit").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (quantity == '')
        {
            $("#txtProductQuantityEdit").addClass("required");
            $("#txtProductQuantityEdit").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (quantity <= 0) {
            $("#txtProductQuantityEdit").addClass("required");
            $("#txtProductQuantityEdit").after('<span class="required-msg">Should be >0</span>');
            flag = 1;
        }
        if (flag == 1) {
            return false;
        } else {
            var products = "";
            if(typeof gtnProductsEdit[productId]!='undefined')
            {
                $("#txtProductCodeEdit").addClass("required");
                $("#txtProductCodeEdit").after('<span class="required-msg">Product already added</span>');
            }
            else
            {
                var obj = new Object();
                obj.quantity=quantity;
                gtnProductsEdit[productId] = obj;
                products +='<tr id="rownumEdit'+rowNumEdit+'"><td>'+productCode+'</td>';
                products +='<td><div class="col-sm-11 p0">'+quantity+'</div><div class="col-sm-1 p0"> <span class="filter-selectall-wrap pull-right"><a href="javascript:;" class="reportFilter-unselectall mr0" onclick="removeProduct('+rowNum+',\''+productId+'\')"><i class="icon-times red"></i></a></span> </div></td>';
                products +='</tr>';
                $('#warehouse-gtnEdit tbody').append(products);
                $('#txtProductCodeEdit').val('');
                $('#txtProductQuantityEdit').val('');
                $('#hdnProductIdEdit').val('');
                $('#hdnProductQuantityEdit').val('');
                $(".addNewProduct label").removeClass("active");
                rowNumEdit = rowNumEdit+1;
            }
        }
    }
    function removeProductEdit(rowNum,pcode)
    {
        delete gtnProductsEdit[pcode];
        $('#rownumEdit'+rowNum).remove();
    }
    function manageWarehouseGrnEdit(This,Id)
    {
        //$('#warehouse_grn')[0].reset();
        $('#myGrnEditModal span.required-msg').remove();
        $('#myGrnEditModal input').removeClass("required");
        $("#myGrnEditModal").removeAttr("disabled");
        $('#warehouseGtnFormEdit')[0].reset();
        $('#txtinvoiceEdit').val('');
        $('span.required-msg').remove();
        $('#vendorEdit').parent().find('.select-dropdown').removeClass('required');
        $("#vendorEdit").parent().find(".required-msg").remove();
        $('#txtinvoiceEdit').removeClass("required");
        $("#txtinvoiceEdit").removeAttr("disabled");
        $('#whGrnStatusChangeHistory').html('');
        $('#txtRemarksEdit').val('');
        $('#attachedInvoiceInner').html("");
        $('#txtfileinvoiceEdit').val('');
        $('#txtfileinvoiceEdit').trigger('change');
        Materialize.updateTextFields();
        $('#warehouseGtnFormEdit span.required-msg').remove();
        $('#warehouseGtnFormEdit input').removeClass("required");
        $("#warehouseGtnFormEdit").removeAttr("disabled");
        $('#warehouse-grn-products tbody').html('');
        $('#warehouseGtnForm span.required-msg').remove();
        $('#warehouseGtnForm input').removeClass("required");
        $("#warehouseGtnForm").removeAttr("disabled");
        $('#hdnProductIdEdit').val('');
        $('#stock_flow_idEdit').val('');
        rowNumEdit=0;
        gtnProductsEdit = new Object();
        $('#warehouse-gtnEdit tbody').html('');
        var userID = $("#hdnUserId").val();
        $("#myModalLabelEdit").html("Edit GRN");
        getProductsEdit();
        var userID = $('#hdnUserId').val();
        var action = 'add';
        var params = {stockFlowId:Id};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        $.when(
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/WarehouseGrn/getVendors',
                    action: action
                }
            ),
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/WarehouseGrn/getGrnDetails',
                    action: action,
                    params:params
                }
            ),
            commonAjaxCall
            (
                {
                    This: This,
                    headerParams: headerParams,
                    requestUrl: API_URL + 'index.php/GrnApproval/getWareHouseGrnProducts',
                    action: action,
                    params:{sequenceNumber:Id,stockflowId:Id}
                }
            )
        ).then(function (response1, response2,response3) {
            var vendorList = response1[0];
            var grndetails = response2[0];
            var grndetailsHistory = response3[0];
            $("#myModalLabelEdit").html("Edit GRN - "+grndetails.data.sequence_number);
            $('#stock_flow_idEdit').val(Id);
            $('#vendorEdit').empty();
            $('#vendorEdit').append($('<option  selected></option>').val('').html('--Select--'));
            if (vendorList.status == true) {

                if (vendorList.data.length > 0) {

                    $.each(vendorList.data, function (key, value) {

                        $('#vendorEdit').append($('<option></option>').val(value.id).html(value.text));
                    });
                }
            }
            $('#vendorEdit').val(grndetails.data.fk_requested_id);
            $('#vendorEdit').material_select();

            $('#txtinvoiceEdit').val(grndetails.data.invoice_number);
            $("#txtinvoiceEdit").next('label').addClass("active");

            if(grndetails.data.invoice_attachment && grndetails.data.invoice_attachment!==null && grndetails.data.invoice_attachment.trim()!='') {
                $('#attachedInvoiceInner').html("Attached Invoice: <a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" onclick=\"downloadInvoice('"+grndetails.data.invoice_attachment+"')\">"+grndetails.data.invoice_attachment+"</a>");
                $('#attachedInvoice').show();
            }
            else
                $('#attachedInvoice').hide();
            if(grndetails.data.remarks!='')
            {
                $('#txtRemarksEdit').val(grndetails.data.remarks);
                $("#txtRemarksEdit").next('label').addClass("active");
            }

            if (grndetails.data.products.length > 0) {

                $.each(grndetails.data.products, function (key, value) {

                    $('#vendorEdit').append($('<option></option>').val(value.id).html(value.text));

                    var obj = new Object();
                    obj.quantity=value.quantity;
                    gtnProductsEdit[value.fk_product_id] = obj;
                    var productsHtml='';
                    productsHtml +='<tr id="rownumEdit'+rowNumEdit+'"><td>'+value.code+'</td>';
                    productsHtml +='<td><div class="col-sm-11 p0">'+value.quantity+'</div><div class="col-sm-1 p0"> <span class="filter-selectall-wrap pull-right"><a href="javascript:;" class="reportFilter-unselectall mr0" onclick="removeProductEdit('+rowNumEdit+',\''+value.fk_product_id+'\')"><i class="icon-times red"></i></a></span> </div></td>';
                    productsHtml +='</tr>';
                    $('#warehouse-gtnEdit tbody').append(productsHtml);
                    rowNumEdit = rowNumEdit+1;

                });
            }

            var stockhtml = '';

            stockhtml += "<table class=\"table table-responsive table-bordered mb0\">";
            stockhtml += "<thead>";
            stockhtml += "<tr class=\"\">";
            stockhtml += "<th>Date</th>";
            stockhtml += "<th>User</th>";
            stockhtml += "<th>Comments</th>";
            stockhtml += "<th>Status</th>";
            stockhtml += "</tr>";
            stockhtml += "</thead>";
            stockhtml += "<tbody>";
            if (grndetailsHistory.status === true && grndetailsHistory.data.history.length > 0) {
                //approval_status=response.data.history[0].approval_status;
                $.each(grndetailsHistory.data.history, function (key, value) {
                    stockhtml += "<tr class=\"\">";
                    stockhtml += "<td>" + value.created_on + "</td>";
                    stockhtml += "<td>" + value.name + "</td>";
                    stockhtml += "<td>" + value.remarks + "</td>";
                    stockhtml += "<td>" + value.approval_status + "</td>";
                    stockhtml += "</tr>";
                });
            }
            else {
                stockhtml += "<tr><td colspan='4' class='text-center'>No history found</td></tr>";
            }
            stockhtml += "</tbody>";
            stockhtml += "</table>";
            $('#whGrnStatusChangeHistory').html(stockhtml);



        });
        $('#myGrnEditModal').modal('show');
    }
    function saveWarehouseGtnEdit(This)
    {
        alertify.dismissAll();
        $('span.required-msg').remove();
        $('#vendorEdit').parent().find('.select-dropdown').removeClass('required');
        $("#vendorEdit").parent().find(".required-msg").remove();
        $('#warehouseGtnFormEdit span.required-msg').remove();
        $('#warehouseGtnFormEdit input').removeClass("required");
        $("#warehouseGtnFormEdit").removeAttr("disabled");
        $('#warehouseGtnFormEdit span.required-msg').remove();
        $('#warehouseGtnFormEdit input').removeClass("required");
        $("#warehouseGtnFormEdit").removeAttr("disabled");
        $('#txtinvoiceEdit').removeClass("required");
        var sentBy = $('#vendorEdit').val();
        var stockFlowId=$('#stock_flow_idEdit').val();
        var flag = 0;
        var grnInvoice = $('#txtinvoiceEdit').val().trim();
        var grnRemarks = $('#txtRemarksEdit').val().trim();
        var files = $("#txtfileinvoiceEdit").get(0).files;
        var selectedFile = $("#txtfileinvoiceEdit").val();
        var extArray = ['gif', 'jpg', 'jpeg', 'png'];
        var extension = selectedFile.split('.');
        var fileUpload = document.getElementById("txtfileinvoiceEdit");
        var InValidExtensions = ['exe']; //array of in valid extensions
        var finalProducts = [];
        var userID = $("#hdnUserId").val();
        $.each(gtnProductsEdit, function (i, item)
        {
            finalProducts.push(i+'||'+item.quantity);
        });
        finalProducts= finalProducts.join(',');

        var sentByVendor = $('#vendorEdit').val();
        if (fileUpload.value == null || fileUpload.value == '') {
            //notify('Upload the invoice', 'error', 10);
            //flag = 1;
        }
        else{
            var fileName = selectedFile;
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            if ($.inArray(fileNameExt, InValidExtensions) == -1){

            }
            else{
                notify('Upload valid invoice', 'error', 10);
                flag = 1;
            }
        }
        if (grnInvoice == '')
        {
            $("#txtinvoiceEdit").addClass("required");
            $("#txtinvoiceEdit").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }
        if(sentByVendor == '')
        {
            $('#vendorEdit').parent().find('.select-dropdown').addClass('required');
            $("#vendorEdit").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        else if(sentByVendor!='' && sentBy==''){
            $('#vendorEdit').parent().find('.select-dropdown').addClass('required');
            $("#vendorEdit").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
            flag = 1;
        }
        if(finalProducts == '')//products mapped
        {
            notify('No products added','error', 10);
            flag=1;
        }
        if(flag == 1)
        {
            return false;
        }
        else
        {
            var ajaxurl = API_URL + 'index.php/WarehouseGrn/editWarehouseGrnFromVendor';
            var params = {userID: userID, requestMode : 'vendor',requestObjectId :sentBy, receiverMode: 'warehouse' ,receiverObjectId :1, products : finalProducts};
            var type = "POST";
            var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            //commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveWarehouseGtnResponse});

                var uploadFile = new FormData();
                if (files.length > 0) {
                    uploadFile.append("file", files[0]);
                    uploadFile.append("image", 'invoice_' + new Date().getTime());
                }
                uploadFile.append('userID', userID);
                uploadFile.append('receiverMode', 'warehouse');
                uploadFile.append('receiverObjectId', 1);
                uploadFile.append('requestMode', 'vendor');
                uploadFile.append('requestObjectId', sentBy);
                uploadFile.append('grnRemarks', grnRemarks);
                uploadFile.append('products', finalProducts);
                uploadFile.append('grnInvoice', grnInvoice);
                uploadFile.append('stockFlowId', stockFlowId);
                $.ajax({
                    type: type,
                    url: ajaxurl,
                    dataType: "json",
                    data: uploadFile,
                    contentType: false,
                    processData: false,
                    headers: headerParams,
                    beforeSend: function () {
                        $(This).attr("disabled", "disabled");
                    }, success: function (response) {
                        alertify.dismissAll();
                        $(This).removeAttr("disabled");
                        SaveWarehouseGtnResponseEdit(response);
                    }, error: function (response) {
                        alertify.dismissAll();
                        notify('Something went wrong. Please try again', 'error', 10);
                    }
                });
//            }


        }
    }
    function SaveWarehouseGtnResponseEdit(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
            if(response.data.invoiceNumber){
                $("#txtinvoiceEdit").addClass("required");
                $("#txtinvoiceEdit").after('<span class="required-msg">'+response.data.invoiceNumber+'</span>');

            }

        }
        else
        {
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            $('#myGrnEditModal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildWarehouseGrnDataTable();
        }
    }
</script>