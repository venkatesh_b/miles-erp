<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-17
 * Time: 12:30 PM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="hdnUserId" value="<?php echo $userData['userId']; ?>" readonly />
    <div class="content-header-wrap">
        <h3 class="content-header">Warehouse GDN</h3>
        <div class="content-header-btnwrap">
            <ul>
                <li access-element="add"><a class=""  data-toggle="modal"><i class="icon-plus-circle" onclick="manageWarehouseGtn(this,0)"></i></a></li>
                <!--<li><a><i class="icon-filter"></i></a></li>-->

                <li><a></a></li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-responsive table-striped table-custom" id="warehouse-gtn-list">
                    <thead>
                    <tr>
                        <th>GDN No.</th>
                        <th>Received By</th>
                        <th>Received On</th>
                        <th>Sent By</th>
                        <th>Sent On</th>
                        <th>No. of Items </th>
                        <th>Tracking / Consignee Number</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <!--Modal-->
        <div class="modal fade" id="myWarehouseGtnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="800">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Create GDN</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <form name="warehouse_gtn" id="warehouse_gtn">
                            <!--<div class="col-sm-12">
                                <div class="input-field">
                                    <input id="grn_no" type="text" name="grn_no" value="" class="validate">
                                    <label for="grn_no">GRN No.</label>
                                </div>
                            </div>-->
                            <div class="col-sm-12">
                                <div class="multiple-radio">
                                    <!--<label>Received From</label>-->
                            <span class="inline-radio" >
                            <input id="txtBranch" class="with-gap" type="radio" name="receivedFrom" value="branch" checked>
                            <label for="txtBranch">Branch</label>
                            </span>
                            <span class="inline-radio">
                            <input id="txtOther" class="with-gap" type="radio" name="receivedFrom" value="vendor">
                            <label  for="txtOther">Vendor</label>
                            </span>
                                </div>
                            </div>
                            <div class="col-sm-12 ">
                                <div class="input-field">
                                    <!--<input id="sent_by" name="sent_by" type="text"  class="validate">-->
                                    <div id="brachdiv">
                                        <select id="sent_to_branch" name="sent_to_branch">

                                        </select>
                                    </div>
                                    <div id="vendordiv" style="display: none;">
                                        <select id="sent_to_vendor" name="sent_to_vendor" ></select></div>
                                    <label class="select-label">Sent To <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <input id="txtinvoice" name="txtinvoice" type="text" class="validate" maxlength="20" />
                                    <label for="txtinvoice" id="lblNumberText">Tracking Number <em>*</em></label>
                                </div>
                            </div>
                            <div class="col-sm-12" id="dvBranchWeight">
                                <div class="input-field">
                                    <input id="txtweight" name="txtweight" type="text" class="validate" maxlength="20" />
                                    <label for="txtweight">Weight <em>*</em></label>
                                </div>
                            </div>
                           <!-- <div class="col-sm-12 mb15">
                                <div class="file-field input-field">
                                    <div class="btn file-upload-btn">
                                        <span>Upload Invoice <em>*</em></span>
                                        <input type="file" id="txtfileinvoice">
                                    </div>
                                    <div class="file-path-wrapper custom">
                                        <input type="text" placeholder="Upload single file" class="file-path validate">
                                    </div>
                                </div>
                            </div>-->
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="txtRemarks" name="txtRemarks" class="materialize-textarea" maxlength="255"></textarea>
                                    <label for="txtRemarks">Remarks</label>
                                </div>
                            </div>
                        </form>
                        <form name="warehouseGtnForm" id="warehouseGtnForm">
                            <div class="col-sm-12">
                                <div class="col-sm-11 p0 addNewProduct">
                                    <div class="col-sm-6 pl0">
                                        <div class="input-field">
                                            <input id="txtProductCode" name="txtProductCode" type="text" class="validate" />
                                            <input id="hdnProductId" name="hdnProductId" type="hidden" class="validate" />
                                            <label for="txtProductCode" >Product Code <em>*</em></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pl0">
                                        <div class="input-field">
                                            <input id="txtProductQuantity" name="txtProductQuantity" type="text" class="validate" onkeypress="return isNumberKey(event)" />
                                            <input id="hdnProductQuantity" name="hdnProductQuantity" type="hidden" />
                                            <label for="txtProductQuantity" >Quantity <em>*</em></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1 pl0">
                                    <a class="m10 ml0 display-inline-block cursor-pointer" onclick="addNewGtnProduct()"> <i class="icon-plus-circle f24 sky-blue"></i> </a>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="product-map-table p0">
                                    <table class="table table-responsive table-bordered mb0" id="warehouse-gtn">
                                        <thead>
                                        <tr>
                                            <th>Product Code</th>
                                            <th>Quantity</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="warehouseGtn_btn" class="btn blue-btn" onclick="saveWarehouseGtn(this)"><i class="icon-right mr8"></i>Save</button>
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="whGtnProducts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="600">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="whGtnModal"></h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12 product-map-table p0" id="gtnProductsWh">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save Changes</button>-->
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script>
    docReady(function(){
        buildwareHouseGtnDataTable();
    });

    function buildwareHouseGtnDataTable()
    {
        var ajaxurl=API_URL+'index.php/WarehouseGtn/warehouseGtnList';
        var userID=$('#hdnUserId').val();
        var headerParams = {action:'list',context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#warehouse-gtn-list").dataTable().fnDestroy();
        $tableobj=$('#warehouse-gtn-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                buildpopover();
                verifyAccess();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": true,
            "bProcessing": true,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No records found",
                "sZeroRecords": "No records found"
            },
            ajax: {
                url:ajaxurl,
                type:'GET',
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false
            } ],
            "order": [[ 0, "desc" ]],
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" data-target=\"#whGtnProducts\" data-toggle=\"modal\"  onclick=\"getGtnProduct(this,"+data.sequence_number+",'"+data.new_sequence_number+"')\">"+data.new_sequence_number+"</a></div>";
                    //return data.sequence_number;
                    return html;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receiveName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.receivedOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.requestedName;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.sentOn;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.total;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    if(data.invoice_number.trim()=='' || data.invoice_number==null){
                        html='----';
                    }
                    else{
                        return data.invoice_number;
                        html="<div><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" onclick=\"downloadInvoice('"+data.invoice_attachment+"')\">"+data.invoice_number+"</a></div>";
                    }
                    return html;
                }
                }
            ]
        } );
        DTSearchOnKeyPressEnter();
    }
    var rowNum=0;
    var gtnProducts = new Object();
    function manageWarehouseGtn(This,Id)
    {
        $('#warehouse_gtn')[0].reset();
        $('#warehouse_gtn span.required-msg').remove();
        $('#warehouse_gtn input').removeClass("required");
        $("#warehouse_gtn").removeAttr("disabled");
        $('#warehouseGtnForm')[0].reset();
        Materialize.updateTextFields();
        $('#warehouseGtnForm span.required-msg').remove();
        $('#warehouseGtnForm input').removeClass("required");
        $("#warehouseGtnForm").removeAttr("disabled");
        $('#warehouse-gtn tbody').html('');
        gtnProducts = new Object();
        rowNum =0;
        $('#warehouseGtn_btn').html('');
        if(Id==0)
        {
            $('#warehouseGtn_btn').html('<i class="icon-right mr8"></i>Add');
        }
        else
        {
            $('#warehouseGtn_btn').html('<i class="icon-right mr8"></i>Update');
        }
        $('#hdnSentBy').val('');
        $('#hdnProductId').val('');
        $('#hdnProductQuantity').val('');
        var userID = $("#hdnUserId").val();
        if (Id == 0)
        {
            $("#myModalLabel").html("Create GDN");
            //getBranches();
            getGtnBranches();
        }
    }
    function getGtnBranches()
    {
        $("#sent_to_vendor").val('');
        $("#vendordiv").hide();
        $("#brachdiv").show();
        $("#sent_to_branch").val('');
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/WarehouseGtn/getBranches';
        var action = 'add';
        var params = {};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getGtnBranchesResponse});
    }
    function getGtnBranchesResponse(response)
    {
        $('#sent_to_branch').empty();
        $('#sent_to_branch').append($('<option  selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#sent_to_branch').append($('<option></option>').val(value.id).html(value.text));
                });
            }
        }
        $('#sent_to_branch').material_select();
        $('#myWarehouseGtnModal').modal('show');
    }
    function getGtnVendors()
    {
        $("#sent_to_vendor").val('');
        $("#vendordiv").show();
        $("#brachdiv").hide();
        $("#sent_to_branch").val('');
        var userID = $('#hdnUserId').val();
        var ajaxurl = API_URL + 'index.php/WarehouseGtn/getVendors';
        var action = 'add';
        var params = {};
        var headerParams = {action: action, context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        var response =
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: action, onSuccess: getGtnVendorsResponse});
    }
    function getGtnVendorsResponse(response)
    {
        $('#sent_to_vendor').empty();
        $('#sent_to_vendor').append($('<option  selected></option>').val('').html('--Select--'));
        if (response.status == true) {

            if (response.data.length > 0) {

                $.each(response.data, function (key, value) {

                    $('#sent_to_vendor').append($('<option></option>').val(value.id).html(value.text));
                });
            }
        }
        $('#sent_to_vendor').material_select();
    }
    $(document).ready(function() {
        $('input[type=radio][name=receivedFrom]').change(function() {
            $("#dvBranchWeight").hide();
            $('#warehouse_gtn span.required-msg').remove();
            $('#warehouse_gtn input').removeClass("required");
            $("#warehouse_gtn").removeAttr("disabled");
            $('#warehouseGtnForm span.required-msg').remove();
            $('#warehouseGtnForm input').removeClass("required");
            $("#warehouseGtnForm").removeAttr("disabled");
            $('#warehouse_gtn span.required-msg').remove();
            $('#warehouse_gtn select').removeClass("required");
            if (this.value == 'branch') {
                $("#dvBranchWeight").show();
                $('#lblNumberText').html('Tracking Number <em>*</em>');
                getGtnBranches();
            }
            if (this.value == 'vendor') {
                $('#lblNumberText').html('Consignee Number <em>*</em>');
                getGtnVendors();
            }
        });
        getProducts();
    });
    function getProducts()
    {
        var headerParams = {
            action: 'add',
            context: $context,
            serviceurl: $pageUrl,
            pageurl: $pageUrl,
            Authorizationtoken: $accessToken,
            user: '<?php echo $userData['userId']; ?>'
        };
        $("#txtProductCode").jsonSuggest({
            onSelect:function(item){
                var product = item.text;
                var productId = item.id;
                var productIdQuantity = productId.split(' | ');
                var productName = product.split(' | ');
                $("#txtProductCode").val(productName[0]);
                $("#hdnProductId").val(productIdQuantity[0]);
                $("#hdnProductQuantity").val(parseInt(productIdQuantity[1]));
            },
            headers:headerParams,url:API_URL + 'index.php/WarehouseGtn/getProducts' , minCharacters: 2});
    }
    function addNewGtnProduct()
    {
        var productCode = $("#txtProductCode").val();
        var productId = $("#hdnProductId").val();
        var productQuantity = $("#hdnProductQuantity").val();
        var quantity = $("#txtProductQuantity").val();
        if(quantity!='')
            var quantity = parseInt($("#txtProductQuantity").val());
        if(productQuantity!='')
            var productQuantity = parseInt($("#hdnProductQuantity").val());
        $('#warehouseGtnForm span.required-msg').remove();
        $('#warehouseGtnForm input').removeClass("required");
        var flag = 0;
        var regx_productName = /^[a-zA-Z][a-zA-Z0-9\s]*$/;
        if (productCode == '')
        {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (regx_productName.test($('#txtProductCode').val()) === false) {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (productCode != '' && productId == '')
        {
            $("#txtProductCode").addClass("required");
            $("#txtProductCode").after('<span class="required-msg">Invalid Code</span>');
            flag = 1;
        }
        if (quantity == '')
        {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        } else if (quantity <= 0) {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">Should be >0</span>');
            flag = 1;
        }else if(quantity>productQuantity) {
            $("#txtProductQuantity").addClass("required");
            $("#txtProductQuantity").after('<span class="required-msg">Insufficient Quantity</span>');
            flag = 1;
        }
        if (flag == 1) {
            return false;
        } else {
            var products = "";
            if(typeof gtnProducts[productId]!='undefined')
            {
                $("#txtProductCode").addClass("required");
                $("#txtProductCode").after('<span class="required-msg">Product already added</span>');
            }
            else
            {
                var obj = new Object();
                obj.quantity=quantity;
                gtnProducts[productId] = obj;
                products +='<tr id=rownum'+rowNum+'><td>'+productCode+'</td>';
                products +='<td><div class="col-sm-11 p0">'+quantity+'</div><div class="col-sm-1 p0"> <span class="filter-selectall-wrap pull-right"><a href="javascript:;" class="reportFilter-unselectall mr0" onclick="removeProduct('+rowNum+',\''+productId+'\')"><i class="icon-times red"></i></a></span> </div></td>';
                //products +='<td><div class="col-sm-11 p0">'+productRemarks+'</div><div class="col-sm-1 p0"> <span class="filter-selectall-wrap pull-right"><a href="#" class="reportFilter-unselectall mr0" onclick="removeProduct('+rowNum+',\''+productId+'\')"><i class="icon-times red"></i></a></span> </div></td></tr>';
                products +='</tr>';
                $('#warehouse-gtn tbody').append(products);
                $('#txtProductCode').val('');
                $('#txtProductQuantity').val('');
                $('#hdnProductId').val('');
                $('#hdnProductQuantity').val('');
                $(".addNewProduct label").removeClass("active");
                rowNum = rowNum+1;
            }
        }
    }
    function removeProduct(rowNum,pcode)
    {
        delete gtnProducts[pcode];
        $('#rownum'+rowNum).remove();
    }
    function saveWarehouseGtn(This) {
        alertify.dismissAll();
        $('#warehouse_gtn span.required-msg').remove();
        $('#warehouse_gtn input').removeClass("required");
        $("#warehouse_gtn").removeAttr("disabled");
        $('#warehouseGtnForm span.required-msg').remove();
        $('#warehouseGtnForm input').removeClass("required");
        $("#warehouseGtnForm").removeAttr("disabled");
        var receivedFrom = $("input[name=receivedFrom]:checked").val();
        var sentBy = 0;
        var flag = 0;
       /* var files = $("#txtfileinvoice").get(0).files;
        var selectedFile = $("#txtfileinvoice").val();
        var extArray = ['gif', 'jpg', 'jpeg', 'png'];
        var extension = selectedFile.split('.');
        var fileUpload = document.getElementById("txtfileinvoice");*/
        var InValidExtensions = ['exe']; //array of in valid extensions
        var finalProducts = [];
        var gtnRemarks = $('#txtRemarks').val().trim();
        var gtnInvoice = $('#txtinvoice').val().trim();
        var gtnWeight = $('#txtweight').val().trim();
        var userID = $("#hdnUserId").val();
        $.each(gtnProducts, function (i, item) {
            finalProducts.push(i + '||' + item.quantity);
        });
        finalProducts = finalProducts.join(',');
        if (receivedFrom == 'branch') {

            if (gtnWeight == '')
            {
                $("#txtweight").addClass("required");
                $("#txtweight").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
                flag = 1;
            }

            var sentByBranch = $('#sent_to_branch').val();
            sentBy = sentByBranch;
            if (sentByBranch == '' || sentByBranch == null) {
                $("#sent_to_branch").parent().find('.select-dropdown').addClass("required");
                $("#sent_to_branch").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;
            }
        }
        if (receivedFrom == 'vendor') {
            var sentByVendor = $('#sent_to_vendor').val();
            sentBy = sentByVendor;
            if (sentByVendor == '' || sentByVendor == null) {
                $("#sent_to_vendor").parent().find('.select-dropdown').addClass("required");
                $("#sent_to_vendor").parent().after('<span class="required-msg">' + $listRequiredMessage + '</span>');
                flag = 1;
            }
        }

        if (gtnInvoice == '')
        {
            $("#txtinvoice").addClass("required");
            $("#txtinvoice").after('<span class="required-msg">' + $inputRequiredMessage + '</span>');
            flag = 1;
        }

        /*if (fileUpload.value == null || fileUpload.value == '') {

            notify('Upload the invoice', 'error', 10);
            flag = 1;
        }
        else{
            var fileName = selectedFile;
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            if ($.inArray(fileNameExt, InValidExtensions) == -1){

            }
            else{

                notify('Upload valid invoice', 'error', 10);
                flag = 1;
            }
        }*/
        if(finalProducts == '')//products mapped
        {

            notify('No products added','error', 10);
            flag=1;
        }
        if(flag == 1)
        {
            return false;
        }
        else
        {
            notify('Processing..', 'warning', 10);
            var ajaxurl = API_URL + 'index.php/WarehouseGtn/addWarehouseGtn';
            var params = {userID: userID, requestMode : 'warehouse',requestObjectId :1, receiverMode: receivedFrom ,receiverObjectId :sentBy,gtnRemarks:gtnRemarks,gtnWeight:gtnWeight, products : finalProducts};
            var type = "POST";
            var headerParams = {action: 'add', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            //commonAjaxCall({This: this, method: 'POST', requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'add', onSuccess: SaveWarehouseGtnResponse});
            var uploadFile = new FormData();
            uploadFile.append('userID', userID);
            uploadFile.append('requestMode', 'warehouse');
            uploadFile.append('requestObjectId', 1);
            uploadFile.append('receiverMode', receivedFrom);
            uploadFile.append('receiverObjectId', sentBy);
            uploadFile.append('gtnRemarks', gtnRemarks);
            uploadFile.append('products', finalProducts);
            uploadFile.append('gtnInvoice', gtnInvoice); //gtnWeight
            uploadFile.append('gtnWeight', gtnWeight);
            $.ajax({
                /*type: 'POST',*/
                type: type,
                url: ajaxurl,
                dataType: "json",
                data: uploadFile,
                contentType: false,
                processData: false,
                headers: headerParams,
                beforeSend: function () {
                    $(This).attr("disabled", "disabled");
                }, success: function (response) {
                    alertify.dismissAll();
                    $(This).removeAttr("disabled");
                    SaveWarehouseGtnResponse(response);
                }, error: function (response) {
                    alertify.dismissAll();
                    notify('Something went wrong. Please try again', 'error', 10);
                }
            });

        }
    }
    function SaveWarehouseGtnResponse(response)
    {
        alertify.dismissAll();
        var response = response;
        if (response == -1 || response.status == false)
        {
            notify(response.message, 'error', 10);
            if(response.data.invoiceNumber){
                $("#txtinvoice").addClass("required");
                $("#txtinvoice").after('<span class="required-msg">'+response.data.invoiceNumber+'</span>');
                flag = 1;
            }
        }
        else
        {
            notify(response.message, 'success', 10);
            $(this).attr("data-dismiss", "modal");
            $('#myWarehouseGtnModal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            buildwareHouseGtnDataTable();
        }
    }
    function getGtnProduct(This,seqNum,newseqNum)
    {
        $('#whGtnModal').html('GDN No. '+newseqNum);
        $('#gtnProductsWh').html('');
        var ajaxurl = API_URL + 'index.php/WarehouseGtn/getWareHouseGtnProducts';
        var params = {sequenceNumber:seqNum};
        var userID = $('#hdnUserId').val();
        var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
        commonAjaxCall({
            This: this,
            requestUrl: ajaxurl,
            params: params,
            headerParams: headerParams,
            action: 'list',
            onSuccess: function (response) {
                var stockhtml = '';
                stockhtml += "<table class=\"table table-responsive table-bordered mb0\">";
                stockhtml += "<thead>";
                stockhtml += "<tr class=\"\">";
                stockhtml += "<th>Product Name</th>";
                stockhtml += "<th>Quantity</th>";
                stockhtml += "</tr>";
                stockhtml += "</thead>";
                stockhtml += "<tbody>";
                if (response.status === true && response.data.length > 0) {
                    $.each(response.data, function (key, value) {
                        stockhtml += "<tr class=\"\">";
                        stockhtml += "<td>" + value.productName + "</td>";
                        stockhtml += "<td>" + value.quantity + "</td>";
                        stockhtml += "</tr>";
                    });
                }
                else {
                    stockhtml = "<tr colspan='2'>No records found</tr>";
                }
                stockhtml += "</tbody>";
                stockhtml += "</table>";
                $('#gtnProductsWh').html(stockhtml);
            }
        });
    }
</script>
