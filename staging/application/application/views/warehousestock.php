<?php
/**
 * Created by PhpStorm.
 * User: THRESHOLD
 * Date: 2016-05-17
 * Time: 10:27 AM
 */
?>
<div class="contentpanel"><!-- InstanceBeginEditable name="PageTitle" -->
    <input type="hidden" id="userId" name="userId" value="<?php echo $userData['userId']; ?>"/>
    <div class="content-header-wrap">
        <h3 class="content-header">Warehouse Stock</h3>
        <div class="content-header-btnwrap">
            <ul>
                <!--<li><a class="viewsidePanel"  data-target="#myModal" data-toggle="modal"><i class="icon-plus-circle" ></i></a></li>-->
                <li>

                </li>
            </ul>
        </div>
    </div>
    <!-- InstanceEndEditable  -->
    <div class="content-body" id="contentBody"> <!-- InstanceBeginEditable name="contentBody" -->
        <div class="fixed-wrap clearfix">
            <div class="col-sm-12 p0">
                <table class="table table-inside table-custom branchwise-report-table" id="warehouseStock-list">
                    <thead>
                    <tr class="">
                        <th>Product Name</th>
                        <th>Product Type</th>
                        <th class="text-right">Stock Received</th>
                        <th class="text-right">Stock Dispatched</th>
                        <th class="text-right">In Transit</th>
                        <th class="text-right">In Hand</th>
                        <!--<th>Comments</th>-->
                    </tr>
                    </thead>

                </table>
            </div>
        </div>
        <!--Modal1-->
        <div class="modal fade" id="myInward" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="800">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">In Ward</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12 product-map-table p0" id="stockInwardWrapper">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save Changes</button>-->
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal2-->
        <div class="modal fade" id="myOutward" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-modal="right" modal-width="800">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon-times strong"></i></span></button>
                        <h4 class="modal-title" id="myModalLabel">Out Ward</h4>
                    </div>
                    <div class="modal-body modal-scroll clearfix">
                        <div class="col-sm-12 product-map-table p0" id="stockOutwardWrapper">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--<button type="button" class="btn blue-btn"><i class="icon-right mr8"></i>Save Changes</button>-->
                        <button type="button" class="btn blue-light-btn" data-dismiss="modal"><i class="icon-times mr8"></i>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- InstanceEndEditable --></div>
</div>
<script>
    docReady(function () {

        getWareHouseStock();
    });
    function getProductWiseStock(This,productId,type,productName){
        $('#stockInwardWrapper').html('');
        $('#stockOutwardWrapper').html('');
        $('#myInward').find('#myModalLabel').html('');
        $('#myOutward').find('#myModalLabel').html('');
        var stockhtml='';
        if(type=='inward'){
            var ajaxurl = API_URL + 'index.php/WarehouseGrn/getWareHouseStockProductWise';
            var params = {type:type,productId:productId};
            var userID = $('#userId').val();
            var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: function (response){
                var stockhtml='';
                stockhtml+="<table class=\"table table-responsive table-bordered mb0\">";
                stockhtml+="<thead>";
                stockhtml+="<tr class=\"\">";
                stockhtml+="<th>Date</th>";
                stockhtml+="<th>Quantity</th>";
                stockhtml+="<th>Received From</th>";
                stockhtml+="<th>Received By</th>";
                stockhtml+="<th>Comments</th>";
                stockhtml+="</tr>";
                stockhtml+="</thead>";
                stockhtml+="<tbody>";
                if(response.status===true && response.data.length>0){
                    $.each(response.data, function (key, value) {
                        if(value.remarks.trim()=='' || value.remarks==null)
                            value.remarks='----';
                        stockhtml+="<tr class=\"\">";
                        stockhtml+="<td>"+value.created_date+"</td>";
                        stockhtml+="<td>"+value.quantity+"</td>";
                        stockhtml+="<td>"+value.requestedName+"</td>";
                        stockhtml+="<td>"+value.receiveName+"</td>";
                        stockhtml+="<td>"+value.remarks+"</td>";
                        stockhtml+="</tr>";
                    });
                }
                else{
                    stockhtml="<tr colspan='5'>No records found</tr>";
                }
                stockhtml+="</tbody>";
                stockhtml+="</table>";
                $('#myInward').find('#myModalLabel').html(productName+" - In Ward");
                $('#stockInwardWrapper').html(stockhtml);

            }});


        }
        else if(type=='outward'){

            var ajaxurl = API_URL + 'index.php/WarehouseGrn/getWareHouseStockProductWise';
            var params = {type:type,productId:productId};
            var userID = $('#userId').val();
            var headerParams = {action: 'list', context: $context, serviceurl: $pageUrl, pageurl: $pageUrl, Authorizationtoken: $accessToken, user: userID};
            commonAjaxCall({This: this, requestUrl: ajaxurl, params: params, headerParams: headerParams, action: 'list', onSuccess: function (response){
                var stockhtml='';
                stockhtml+="<table class=\"table table-responsive table-bordered mb0\">";
                stockhtml+="<thead>";
                stockhtml+="<tr class=\"\">";
                stockhtml+="<th>Date</th>";
                stockhtml+="<th>Quantity</th>";
                stockhtml+="<th>Sent To</th>";
                stockhtml+="<th>Sent By</th>";
                stockhtml+="<th>Comments</th>";
                stockhtml+="<th>Status</th>";
                stockhtml+="</tr>";
                stockhtml+="</thead>";
                if(response.status===true && response.data.length>0){
                    $.each(response.data, function (key, value) {
                        if(value.remarks.trim()=='' || value.remarks==null)
                            value.remarks='----';
                        stockhtml+="<tr class=\"\">";
                        stockhtml+="<td>"+value.created_date+"</td>";
                        stockhtml+="<td>"+value.quantity+"</td>";
                        stockhtml+="<td>"+value.receiveName+"</td>";
                        stockhtml+="<td>"+value.requestedName+"</td>";
                        stockhtml+="<td>"+value.remarks+"</td>";
                        stockhtml+="<td>"+value.deliverStatus+"</td>";
                        stockhtml+="</tr>";
                    });
                }
                else{
                    stockhtml="<tr colspan='5'>No records found</tr>";
                }
                stockhtml+="</tbody>";
                stockhtml+="</table>";
                $('#myOutward').find('#myModalLabel').html(productName+" - Out Ward");
                $('#stockOutwardWrapper').html(stockhtml);

            }});


        }
    }
    function getWareHouseStock(This){
        var action = 'list';
        var ajaxurl=API_URL+'index.php/WarehouseGrn/getWareHouseStock';
        var userID=$('#userId').val();
        var params={};
        var headerParams = {action:action,context:$context,serviceurl:$pageUrl,pageurl:$pageUrl,Authorizationtoken:$accessToken,user:userID};
        $("#warehouseStock-list").dataTable().fnDestroy();
        $tableobj = $('#warehouseStock-list').DataTable( {
            "fnDrawCallback": function() {
                var $api = this.api();
                var pages = $api.page.info().pages;
                if(pages > 1)
                {
                    $('.dataTables_paginate').css("display", "block");
                    $('.dataTables_length').css("display", "block");
                    //$('.dataTables_filter').css("display", "block");
                } else {
                    $('.dataTables_paginate').css("display", "none");
                    $('.dataTables_length').css("display", "none");
                    //$('.dataTables_filter').css("display", "none");
                }
                verifyAccess();
                buildpopover();
            },
            dom: "Bfrtip",
            bInfo: false,
            "serverSide": false,
            "oLanguage":
            {
                "sSearch": "<span class='icon-search f16'></span>",
                "sEmptyTable": "No Records Found",
                "sZeroRecords": "No Records Found"
            },
            "bProcessing": true,
            ajax: {
                url:ajaxurl,
                type:'GET',
                data:params,
                headers:headerParams,
                error:function(response) {
                    DTResponseerror(response);
                }
            },
            columns: [
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    html="<a class=\"anchor-blue text-dec-none\" href=\"javascript:\">"+data.productName+"</a>";
                    return html;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    return data.producType;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    if(parseInt(data.received)>0){
                        html="<div class='text-right'><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\" data-target=\"#myInward\" data-toggle=\"modal\"  onclick=\"getProductWiseStock(this,"+data.productId+",'inward','"+data.productName+"')\">"+data.received+"</a></div>";
                    }
                    else{
                        html="<div class='text-right'><a href=\"javascript:\" class=\"viewsidePanel anchor-blue\">"+data.received+"</a></div>";
                    }

                    return html;
                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    if(parseInt(data.dispatched)>0){
                        html="<div class='text-right'><a href=\"javascript:;\" class=\"viewsidePanel anchor-blue\" data-target=\"#myOutward\" data-toggle=\"modal\"  onclick=\"getProductWiseStock(this,"+data.productId+",'outward','"+data.productName+"')\">"+data.dispatched+"</a></div>";
                    }
                    else{
                        html="<div class='text-right'><a href=\"javascript:;\" class=\"viewsidePanel anchor-blue\">"+data.dispatched+"</a></div>";
                    }

                    return html;

                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    html="<div class='text-right'>"+data.inTransit+"</div>";
                    return html;

                }
                },
                {
                    data: null, render: function ( data, type, row )
                {
                    var html='';
                    html="<div class='text-right'>"+data.inHand+"</div>";
                    return html;

                }
                }
                /*{
                    data: null, render: function ( data, type, row )
                {
                    if(data.remarks==null || data.remarks.trim()=='' )
                        data.remarks='----';
                    return data.remarks;
                }
                }*/
            ]
        } );
        DTSearchOnKeyPressEnter();

    }


</script>
