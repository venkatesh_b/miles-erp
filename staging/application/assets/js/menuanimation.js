/**
 * Created by VENKATESH.B on 9/2/16.
 */

$body = $(document.body);
if($("#left-panel-main-menus-ul").length > 0)
{
    var _menusUl = $('#left-panel-main-menus-ul'), _searchLi, _menuSearchBox, _inputSearch;
    $(function(){
        var list =[], obj={};


       $('.leftpanel .nav-parent > a').click(function () {
            var parent = $(this).parent();
            var sub = parent.find('> ul');
            if (!$('body').hasClass('leftpanel-collapsed')) {
                if (sub.is(':visible')) {
                    sub.slideUp(200, function () {
                        parent.removeClass('nav-active active');
                        $('.mainpanel').css({ height: '' });
                    });
                } else {
                    $('.leftpanel .nav-parent').each(function () {
                        var t = $(this);
                        if (t.hasClass('nav-active')) {
                            t.find('> ul').slideUp(200, function () {
                                t.removeClass('nav-active');
                            });
                        }
                    });
                    $('.nav-bracket > li').removeClass('nav-active active');
                    parent.addClass('nav-active');
                    sub.slideDown(200);
                }
            }
        });

    });
}
