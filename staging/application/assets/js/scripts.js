//GLOBAL VARIABLE (DECLARE variable in caps ex: var MY_VAR = '';)
$('body').hide();
//$(window).on('load', function(){$('body').show()});
//LOAD / RESIZE / SCROLL
$(window).on('load resize', function(){
    $('body').show();
    $('#contentBody').height($(window).height()-140);
    $('.home-page-parent').height($(window).height()-155);
    $('.masters-menu-style').height($(window).height()-125);
    //collapsed menu active 1024width
    if($(window).width()<=1024){
        $("body").addClass('leftpanel-collapsed');
        $('.nav-parent').removeClass('nav-active');
        if ($body.hasClass('leftpanel-collapsed'))
            $('.leftpanel-collapsed').find('ul.children').hide();

    }else{
        $("body").removeClass('leftpanel-collapsed');
    }


});
//DOCUMENT READY
$(function(){
    //adding animation to dropdowns
    $('.table tr').addClass('animated fadeInUp');
    $('.fixed-wrap').addClass('animated fadeIn');
    $('ul.dropdown-menu').addClass('animated zoomIn');
    $('.batch-info-wrap,.col-5,.contact-wrap').addClass('animated zoomIn');
    //textarea
    /*$('#textarea1').val('New Text');
     $('#textarea1').trigger('autoresize');*/
    //scroll Tabs
    $('#tabs2').scrollTabs();
    $('body').on('.nav-tabs li a','click',function(){
        alert();
        $(".nav-tabs li").removeClass("active");
    });
    $('.showAddCompanyWrap-btn').on('click',function(){
        //console.log($(this).parent().next().slideDown());
        $(this).parent().next().slideToggle();
    });
    $('.showAddInstituteWrap-btn').on('click',function(){
        $(this).parent().next().slideToggle();
    });
    $('.company-icon-cancel').on('click',function(){
        $(".coldCall-addCompanyWrap").slideUp();
    });
    //cold call status
    $('#callStatus').on('change', function() {
        var coldCallStatusVal = $(this).val();
        var contacttypeVal = $(this).val();

        if ($("input[name='callStatusInfo-radio']:checked").val() == 'callStatusInfoIntrested') {
            $(".coldcall-info").slideDown();
        }

        if ($("input[name='contacttype']:checked").val() == 'contacttype-corp') {
            $("#contactType-corporate-wrap").slideDown();
        }

        if(coldCallStatusVal=='answered'){
            $('#callStatusInfo').slideDown();
        }
        if(coldCallStatusVal!='answered'){
            $('#callStatusInfo').slideUp();
            $(".coldcall-info").slideUp();
        }
    });
    //
    $("input[name='callStatusInfo-radio']").click(function(){
        $(".coldcall-info").hide();
        if ($("input[name='callStatusInfo-radio']:checked").val() == 'callStatusInfoIntrested') {
            $(".coldcall-info").slideDown();
            $(".coldcall-info").find('#coldcalling-branch').hide();
        }
        if ($("input[name='callStatusInfo-radio']:checked").val() == 'callStatusInfoIntrestedTransfer') {
            $(".coldcall-info").slideDown();
            $(".coldcall-info").find('#coldcalling-branch').slideDown();
        }
    });
    $("input[name='contacttype']").click(function(){
        var contacttypeVal = $(this).val();
        if(contacttypeVal=='contacttype-corp'){
            $("#contactType-Institution-wrap").hide();
            $("#contactType-corporate-wrap").slideDown();
        }
        if(contacttypeVal=='contacttype-insti'){
            $("#contactType-corporate-wrap").hide();
            $("#contactType-Institution-wrap").slideDown();
        }
        if(contacttypeVal=='contacttype-se'){
            $("#contactType-corporate-wrap").hide();
            $("#contactType-Institution-wrap").hide();
        }
    });


    $(".branch-head-data").click(function(){
        $(".student-custom").removeClass("addpaid");
        $(".paid-info").hide();
    });
    $(".super-admin-data").click(function(){
        $(".student-custom").addClass("addpaid");
        $(".paid-info").show();
    });
    //super admin widgets and branch head widgets
    $(".super-admin").click(function(){
        $("body").addClass("batch-hideshow");
    });
    $(".branch-head").on('click',function(){
        $("body").removeClass("batch-hideshow");
    });
    //tooltip
    /*$('[data-toggle="tooltip"]').tooltip({
     container: 'body'
     })*/
    $('.tooltipped').tooltip({delay: 50});
    //funnel chart
    $('#stageFunnel').highcharts({
        chart: {
            type: 'funnel',
            marginRight: 100
        },
        title: {
            text: 'Leads Life Cycle',
            x: -50
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b> ({point.y:,.0f})',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '28%',
                neckHeight: '25%'

                //-- Other available options
                // height: pixels or percent
                // width: pixels or percent
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Leads',
            data: [
                ['M1', 25],
                ['M2', 15],
                ['M3', 23],
                ['M4', 56],
                ['M5', 23],
                ['M6', 53],
                ['M7', 80]
            ]
        }]
    });
    //fee structure create
    $('#includeFeetype').on('change', function() {
        var mystructureVal = $(this).val();
        // alert( myVal ); // or $(this).val()
        if(mystructureVal=='group1'){
            $('.feeStructureCreate-companyname').hide();
            $('.feeStructureCreate-groupname').slideDown();
        }
        if(mystructureVal=='corporate1'){
            $('.feeStructureCreate-groupname').hide();
            $('.feeStructureCreate-companyname').slideDown();
        }
        if(mystructureVal=='retail1'){
            $('.feeStructureCreate-groupname').hide();
            $('.feeStructureCreate-companyname').hide();
        }

    });


    //receipt view
    //$('#feecollectionCreate-tform').on('click','#finalizee',function(){
    $('#finalize').click(function(){
        $('#feecollectionCreate-tform').hide();
        $('.receipt-printable').show();
    });

    $('#proceed').on('click',function(){
        $('#proceedWrapper').hide();
        $('#feeCollectionCreate-studentDetails').slideDown();
    });
    //reports select all and un select all
    $('.reportFilter-selectall').click(function(){
        $(this).closest('.reports-filter-wrapper').find('.boxed-tags a').addClass('active');
        if($(this).attr("data-value")=='batch-filter')
        {
            getBatchesByBranchCourse();
        }
    });
    $('.reportFilter-unselectall').click(function(){
        $(this).closest('.reports-filter-wrapper').find('.boxed-tags a').removeClass('active');
        if($(this).attr("data-value")=='batch-filter')
        {
            getBatchesByBranchCourse();
        }
    });


    $('.boxed-tags').on( 'click', 'a', function () {
        if($(this).parent().hasClass('radio-tags'))
        {
                $(this).parent().find('a').removeClass("active");
        }
        $(this).toggleClass('active');
        if($(this).attr("data-value")=='batch-filter')
        {
            getBatchesByBranchCourse();
        }
    });


    var modalBodyHeight = $(window).height()-111;
    $('[data-modal="right"]').on('show.bs.modal', function () {
        $(this).find('.modal-dialog').css({
            width:$(this).attr('modal-width') ? $(this).attr('modal-width') : 'auto'
        });
        //modal-scrool height
        $(this).find('.modal-scroll').css({
            /*'height':'auto',
             'max-height':modalBodyHeight*/
            'height':modalBodyHeight,
            'overflow-y':'auto'
        });
    });
    //filters
    $('.accordian-heading').on('click',function(){
        $(this).toggleClass("active");
        //$('.accordian-content').slideUp();
        $(this).next().slideToggle()
    });
    //Materialize select
    $('select').material_select();
    //Materialize Date picker



    $( ".select-dropdown" ).change(function() {
        $(this).parent('.select-wrapper').find('.select-label').show();
    });

    $('.datepicker').click(function(){

        var datePickerValue = $('.datepicker').val();
        if(datePickerValue !== ""){
            $('.datepicker').prev().addClass('active');
        }
    });

    //Manage users- add users working add
    $(".slide-down").click(function(){
        $(".hide-data").slideToggle();
    });
    //warehouse grn create inventory recieved from
    $(".others").click(function(){
        if($("input.others:checked")){
            $(".sent-field").slideToggle();
        }
    });
    $("#branch").click(function(){
        $(".sent-field").slideUp();
    });

    // hide all other popovers
    $('body').popover({
        selector: '[rel=popover]',
        trigger: "click"
    }).on("show.bs.popover", function(e){
        // hide all other popovers
        $("[rel=popover]").not(e.target).popover("destroy");
        $(".popover").remove();
    });


    //malihu scroll
    $(window).load(function(){
        $("#contentBody").mCustomScrollbar({
            theme:"dark",
            autoHideScrollbar: true,
            autoExpandScrollbar: true
        });

        $(".modal-scrol").mCustomScrollbar({
            theme:"dark",
            autoHideScrollbar: true,
            autoExpandScrollbar: true,
            updateOnContentResize: true
        });

        $(".call-schedule-scroll").mCustomScrollbar({
            theme:"dark",
            axis:"x",
            autoHideScrollbar: true,
            autoExpandScrollbar: true,
            updateOnContentResize: true
        });
        $(".branch-visit-scroll").mCustomScrollbar({
            theme:"dark",
            axis:"x",
            autoHideScrollbar: true,
            autoExpandScrollbar: true,
            updateOnContentResize: true
        });
        enableBranchInfoScrollBar();
        enableDatePicker();
    });
});
//cliseHideData
function closeHideData(){
    $(".hide-data").slideUp();
}
function enableBranchInfoScrollBar()
{
    $(".branch-info-right").mCustomScrollbar({
        theme:"dark",
        autoHideScrollbar: true,
        autoExpandScrollbar: true,
        updateOnContentResize: true
    });
}

function enableDatePicker()
{
    enableDates();
    enablePastDates();
    enableFutureDates();
    enableDobDates();
    enablePast7Dates();
    enablePastDatesWithCurrentDate();
}
function enableFutureDates()
{
    if($('.enableFutureDates').length>0) {
        $('.enableFutureDates').each(function(i, obj) {
            var $input = $(obj).pickadate({
                min: new Date(),
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 90, // Creates a dropdown of 60 years to control year
                onSet: function (arg)
                {
                    if ('select' in arg)
                    { //prevent closing on selecting month/year
                        this.close();
                    }
                }
            });
            // Use the picker object directly.
            var picker = $input.pickadate('picker');
            picker.set('select', new Date());
        });
    }
}
function enablePastDates()
{
    if($('.enablePastDates').length>0)
    {
        $('.enablePastDates').each(function(i, obj)
        {
            var $input = $(obj).pickadate({
                max: new Date(),
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 90, // Creates a dropdown of 60 years to control year
                onSet: function (arg)
                {
                    if ('select' in arg)
                    { //prevent closing on selecting month/year
                        this.close();
                    }
                }
            });
            // Use the picker object directly.
            var picker = $input.pickadate('picker');
            picker.set('select', new Date());
        });
    }
}
function enableDates()
{
    if($('.enableDates').length>0) {
        $('.enableDates').each(function(i, obj)
        {
            var $input = $(obj).pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 30, // Creates a dropdown of 15 years to control year
                onSet: function (arg) {
                    if ('select' in arg) { //prevent closing on selecting month/year
                        this.close();
                    }
                }
            });
            // Use the picker object directly.
            var picker = $input.pickadate('picker');
            picker.set('select', new Date());
        });
    }
}
function enableDobDates(){
    if($('.enableDobDates').length>0) {
        var today = new Date();
        var restrictedYears = 15;//years
        var startDate = new Date(today.getTime() - restrictedYears * 365 * 24 * 60 * 60 * 1000);
        $('.enableDobDates').pickadate({
            max: new Date(startDate),
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 30, // Creates a dropdown of 15 years to control year
            onSet: function (arg) {
                if ('select' in arg) { //prevent closing on selecting month/year
                    this.close();
                }
            }
        });
    }

}
function enablePast7Dates(){
    if($('.enablePast7Dates').length>0) {
        var today = new Date();
        var restrictedDays = 6;//days
        var startDate = new Date(today.getTime() - restrictedDays * 24 * 60 * 60 * 1000);
        $('.enablePast7Dates').pickadate({
            min: -6,
            max: new Date(today),
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 30, // Creates a dropdown of 15 years to control year
            onSet: function (arg) {
                if ('select' in arg) { //prevent closing on selecting month/year
                    this.close();
                }
            }
        });
    }

}
function enablePastDatesWithCurrentDate()
{
    if($('.enablePastDatesWithCurrentDate').length>0) {
        $('.enablePastDatesWithCurrentDate').each(function(i, obj) {
            var $input = $(obj).pickadate({
                max: new Date(),
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 90, // Creates a dropdown of 60 years to control year
                onSet: function (arg) {
                    if ('select' in arg) { //prevent closing on selecting month/year
                        this.close();
                    }
                }
            });
            // Use the picker object directly.
            //if ($('.enablePastDatesWithCurrentDate').length > 0) {
            var picker = $input.pickadate('picker');
            picker.set('select', new Date());
            //}
        });
    }

}
//USER DEFINED FUNCTIONS
//notifications
function notify(message, type , duration){
    var defaultMessage = 'Message Not Given', defaultType='normal';
    if(message=='' || message == undefined){
        message=defaultMessage;
    }
    if(type=='' || type == undefined){
        type=defaultType;
    }
    message = message + '<div class="timer-wrapper"><div class="pie spinner"></div><div class="pie filler"></div><div class="mask"></div></div>'

    alertify.notify(message, type, duration);
    $('.filler').css({
        'animation': 'fill ' + duration + 's steps(1, end) infinite'
    });
    $('.spinner').css({
        'animation': 'rota ' + duration + 's linear infinite'
    });
    $('.mask').css({
        'animation': 'mask ' + duration + 's steps(1, end) infinite'
    })
}

function buildpopover() {
    $('.popper').popover({
        container: 'body',
        html: true,
        content: function () {
            return $(this).next('.popper-content').html();
        },
        title: function () {
            return jQuery(this).data('title') + '<span class="close ml15 -mt2">&times;</span>';
        }
    }).on('shown.bs.popover', function (e) {
        var popover = jQuery(this);
        $('div.popover .popover-title .close').on('click', function (e) {
            popover.popover('hide');
        });
    }).on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
    });
    $('.tooltipped').tooltip({delay: 50});
    $('.table tr').addClass('animated fadeInUp');
    $('.modal .table tr').removeClass('animated fadeInUp');
    $('ul.dropdown-menu').addClass('animated zoomIn');
}

// Global Regular expressions starts here //
// Expression to allow alphanumeric and space //
$rgx_allow_alpha_numeric_space=/^[a-zA-Z0-9\-\s]+$/;
$rgx_allow_alpha_numeric_space_message='Accepts only alphanumeric with spaces';
//------------//


// Global Regular expressions ends here //
