<?php
date_default_timezone_set('Asia/Kolkata');
//define('ENVIRONMENT', 'development');
define('ENVIRONMENT', 'production');
ini_set('max_execution_time', 0);
if (defined('ENVIRONMENT'))
{
    switch (ENVIRONMENT)
    {
        case 'development':
            error_reporting(E_ALL);
            $base_host = sprintf('%s://%s/',$_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http',$_SERVER['SERVER_NAME'])."miles-erp/staging/application/";

            $api_host = sprintf('%s://%s/',$_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http',$_SERVER['SERVER_NAME'])."miles-erp/staging/api/";
            /* site urls starts */
            define("BASE_URL",$base_host);
            define("API_URL",$api_host);
            define("APP_REDIRECT",$base_host.'app/');

            define("ENCRYPTION_KEY",'MILESERP');
            define("GOOGLE_API_KEY","AIzaSyAup94iNs9ytz-zmxOH56-hlEDvP5VnSPs");
            define("GOOGLE_CLIENT_ID",'472244376060-29v5udskvifbcj1fqdevm71ffblet4qh.apps.googleusercontent.com');
            define("GOOGLE_CLIENT_SECRET",'hMyKECP6Hm9_mHZVTrXgb5ID');
            define("GOOGLE_REDIRECT_URI",$base_host);
            break;
        case 'production':
            error_reporting(0);
            $base_host = sprintf('%s://%s/',$_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http',$_SERVER['SERVER_NAME'])."staging/application/";
            $api_host = sprintf('%s://%s/',$_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http',$_SERVER['SERVER_NAME'])."staging/api/";
            define("BASE_URL",$base_host);
            define("API_URL",$api_host);
            define("APP_REDIRECT",$base_host.'app/');


            define("ENCRYPTION_KEY",'MILESERP');
            define("GOOGLE_API_KEY","AIzaSyAup94iNs9ytz-zmxOH56-hlEDvP5VnSPs");
            define("GOOGLE_CLIENT_ID",'472244376060-29v5udskvifbcj1fqdevm71ffblet4qh.apps.googleusercontent.com');
            define("GOOGLE_CLIENT_SECRET",'hMyKECP6Hm9_mHZVTrXgb5ID');
            define("GOOGLE_REDIRECT_URI",$base_host);
            break;
        default:
            exit('The application environment is not set correctly.');
    }
}
?>